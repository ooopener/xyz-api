<?php

namespace xyz\ooopener\controllers\veterinarians;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\livestocks\LivestocksController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;
use xyz\ooopener\things\Veterinarian;
use xyz\ooopener\validations\VeterinarianValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;

use Exception ;

class VeterinariansController extends CollectionsController
{
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path , new VeterinarianValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'identifier'    => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,

        'authority'     => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,

        'medicalSpecialties' => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ]  ] ,
        'livestocks'         => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ]  ]
    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Veterinarian::FILTER_LIVESTOCKS :
                    {
                        if( property_exists( $init , Veterinarian::FILTER_LIVESTOCKS ) && is_array( $init->{ Veterinarian::FILTER_LIVESTOCKS } ) && count( $init->{ Veterinarian::FILTER_LIVESTOCKS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Veterinarian::FILTER_LIVESTOCKS } as $item )
                            {
                                array_push( $sub , $this->container
                                                        ->get( LivestocksController::class )
                                                        ->create( (object) $item ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Veterinarian::FILTER_AUTHORITY :
                    {
                        // get authority
                        $authority = $this->container
                                          ->get( 'veterinarianAuthorityController' )
                                          ->all( NULL , NULL , [ 'id' => (string) $init->id , 'skin' => 'normal' ] ) ;

                        $init->{ $key } = $authority && is_array( $authority ) && count( $authority ) > 0
                                        ? $authority[0]
                                        : NULL ;
                        break;
                    }
                }
            }
        }
        return $init ;
    }

    public function deleteDependencies( $id )
    {
        $this->deleteInEdgeModel( 'livestockVeterinarians'         , $id        ) ; // livestock.veterinarians
        $this->deleteInEdgeModel( 'userFavorites'                  , $id        ) ; // favorites
        $this->deleteInEdgeModel( 'veterinarianOrganizations'      , $id , 'to' ) ; // veterinarian.organization
        $this->deleteInEdgeModel( 'veterinarianPeople'             , $id , 'to' ) ; // veterinarian.people
        $this->deleteInEdgeModel( 'veterinarianMedicalSpecialties' , $id , 'to' ) ; // veterinarian.medicalSpecialities
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' );
        }

        $params = $request->getParsedBody();

        $organization = NULL ;
        $person       = NULL ;

        $item               = [];
        $item['active']     = 1;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']       = $this->path;

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'];
        }

        $conditions = [] ;

        if( ( isset( $params['organization'] ) && !empty( $params['organization'] ) && isset( $params['person'] ) && !empty( $params['person'] ) )  || ( !isset( $params['organization'] ) && !isset( $params['person'] ) ) || ( empty( $params['organization'] ) && empty( $params['person'] ) ) )
        {
            return $this->error( $response , [ 'person' => 'person is required' , 'organization' => 'organization is required' ] , "400" ) ;
        }

        if( isset( $params['organization'] ) && !empty( $params['organization'] ) )
        {
            $organization = $params['organization'];
            $conditions['organization'] = [ $organization   , 'required|organization' ] ;
        }

        if( isset( $params['person'] ) && !empty( $params['person'] ) )
        {
            $person = $params['person'];
            $conditions['person'] = [ $person   , 'required|person' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $latest = $this->model->insert( $item );

                if( $organization )
                {
                    $edge = $this->container->get( 'veterinarianOrganizations' ) ;
                    $edge->insertEdge( $edge->from['name'] . '/' . $organization , $latest->_id ) ;
                }
                elseif( $person )
                {
                    $edge   = $this->container->get( 'veterinarianPeople' ) ;
                    $edge->insertEdge( $edge->from['name'] . '/' . $person , $latest->_id ) ;
                }

                if( $latest )
                {
                    return $this->success
                    (
                        $response ,
                        $this->model->get( $latest->_key , [ 'queryFields' => $this->getFields() ] )
                    ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'];
        }

        $conditions = [] ;

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success
                    (
                        $response ,
                        $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] )
                    );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }
}
