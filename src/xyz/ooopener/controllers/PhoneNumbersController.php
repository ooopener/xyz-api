<?php

namespace xyz\ooopener\controllers;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\IdentifierValidator;

/**
 * The generic phone numbers controller.
 */
class PhoneNumbersController extends ThingsEdgesController
{
    /**
     * Creates a new PhoneNumbersController instance.
     * @param ContainerInterface $container
     * @param ?Model             $model
     * @param ?Collections       $owner
     * @param ?Edges             $edge
     * @param ?string            $path
     */
    public function __construct
    (
        ContainerInterface $container    ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $owner ,
            $edge ,
            $path ,
            new IdentifierValidator( $container , 'phoneNumbersTypes' )
        );
        $this->helper = $this->container->get( PhoneNumberUtil::class ) ;
    }

    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' => Thing::FILTER_ID        ] ,
        'value'          => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'created'        => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'modified'       => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_JOIN      ]
    ];

    protected $helper ;

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;

        $item       = [];
        $conditions = [] ;

        if( isset( $params['additionalType'] )  )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) )
        {
            $params['value'] = $item['value'] = $this->formatPhoneNumber( $params['value'] , isset( $params['regionCode'] ) ?: NULL ) ;
        }

        $conditions['value']           = [ $params['value']          , 'required'                ] ;
        $conditions['additionalType' ] = [ $params['additionalType'] , 'required|additionalType' ] ;

        $this->conditions = $conditions ;
        $this->item       = $item ;
    }

    /**
     * @private
     * @param $value
     * @param null $regionCode
     * @return string|null
     */
    private function formatPhoneNumber( $value , $regionCode = NULL ) :?string
    {
        try
        {
            $number = $this->helper->parse( $value , $regionCode ) ;
            return $this->helper->isValidNumber( $number )
                   ? $this->helper->format( $number , PhoneNumberFormat::E164 )
                   : NULL ;
        }
        catch( NumberParseException $exception )
        {
            return NULL ;
        }
    }
}

/**
 * The enumeration of all properties to filtering when we create a new instance.
 * @OA\Schema(
 *     schema="PhoneNumber",
 *     type="object",
 *     @OA\Property(type="integer",property="id",description="Resource identification"),
 *     @OA\Property(type="string",property="name",description="The name of the resource"),
 *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
 *     @OA\Property(type="string",property="value",description="A valid phone number in international format"),
 *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
 *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
 * )
 * @OA\RequestBody(
 *     request="postPhoneNumber",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="value",type="string",description="The phone number in international format"),
 *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
 *             required={"value","additionalType"},
 *             @OA\Property(property="name",type="string",description="The name of the resource"),
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 */