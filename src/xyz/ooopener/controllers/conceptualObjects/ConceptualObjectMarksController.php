<?php

namespace xyz\ooopener\controllers\conceptualObjects;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\ConceptualObjectMarkValidator;

/**
 * The object materials controller.
 */
class ConceptualObjectMarksController extends ThingsEdgesController
{
    /**
     * Creates a new ConceptualObjectMarksController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new ConceptualObjectMarkValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     * @OA\Schema(
     *     schema="ConceptualObjectMark",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="position",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="text",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="language",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="technique",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID        ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_JOIN     ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'position'      => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'language'      => [ 'filter' =>  Thing::FILTER_JOIN     ] ,
        'technique'     => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;
        $set    = $this->config['conceptualObjects-marks'];

        $item = [];

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['language'] ) )
        {
            $item['language'] = (int) $params['language'] ;
        }

        if( isset( $params['technique'] ) )
        {
            $item['technique'] = (int) $params['technique'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['position'] ) )
        {
            $item['position'] = $this->filterLanguages( $params['position'] ) ;
        }

        if( isset( $params['text'] ) )
        {
            $item['text'] = $this->filterLanguages( $params['text'] ) ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'additionalType' => [ $params['additionalType'] , 'int|additionalType' ] ,
            'language'       => [ $params['language']       , 'int|language'       ] ,
            'technique'      => [ $params['technique']      , 'int|technique'      ] ,
            'name'           => [ $params['name']           , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ]
        ];

        $this->conditions = $conditions ;
        $this->item       = $item ;
    }
}

/**
 * @OA\RequestBody(
 *     request="postConceptualObjectMark",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="integer",property="language"),
 *             @OA\Property(type="integer",property="technique"),
 *             required={"additionalType","language","technique"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="position",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="text",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putConceptualObjectMark",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="integer",property="language"),
 *             @OA\Property(type="integer",property="technique"),
 *             required={"additionalType","language","technique"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="position",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="text",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 */
