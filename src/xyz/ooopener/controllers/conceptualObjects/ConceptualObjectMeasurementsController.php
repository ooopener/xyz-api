<?php

namespace xyz\ooopener\controllers\conceptualObjects;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\MeasurementValidator;

/**
 * The object measurements controller.
 */
class ConceptualObjectMeasurementsController extends ThingsEdgesController
{
    /**
     * Creates a new ConceptualObjectMeasurementsController instance.
     *
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new MeasurementValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="ConceptualObjectMeasurement",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
     *     @OA\Property(type="number",property="value",description="The name of the resource"),
     *     @OA\Property(type="string",property="dimension",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="unit",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID        ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,
        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE  ] ,
        'dimension'     => [ 'filter' =>  Thing::FILTER_JOIN      ] ,
        'unit'          => [ 'filter' =>  Thing::FILTER_JOIN      ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;
        $set    = $this->config['conceptualObjects-measurements'];
        $item   = [];

        if( isset( $params['dimension'] ) )
        {
            $item['dimension'] = (int) $params['dimension'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['unit'] ) )
        {
            $item['unit'] = (int) $params['unit'] ;
        }

        if( isset( $params['value'] ) )
        {
            $item['value'] = $params['value'] ;
        }

        $conditions =
        [
            'dimension'   => [ $params['dimension'] , 'required|int|dimension' ] ,
            'unit'        => [ $params['unit']      , 'int|unit' ] ,
            'value'       => [ $params['value']     , 'number'   ]
        ];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ;
        }

        $this->conditions = $conditions ;
        $this->item       = $item ;
    }
}

/**
 * @OA\RequestBody(
 *     request="postConceptualObjectMeasurement",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="dimension"),
 *             @OA\Property(type="integer",property="unit"),
 *             required={"dimension","unit"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
 *             @OA\Property(type="number",property="value",description=""),
 *         )
 *     ),
 *     required=true
 * )
 *
 * @OA\RequestBody(
 *     request="putConceptualObjectMeasurement",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="dimension"),
 *             @OA\Property(type="integer",property="unit"),
 *             required={"dimension","unit"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
 *             @OA\Property(type="number",property="value",description=""),
 *         )
 *     ),
 *     required=true
 * )
 */