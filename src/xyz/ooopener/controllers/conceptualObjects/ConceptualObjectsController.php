<?php

namespace xyz\ooopener\controllers\conceptualObjects;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\places\PlacesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Stages;
use xyz\ooopener\validations\ConceptualObjectValidator;

use Exception ;

use Psr\Container\ContainerInterface ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\things\ConceptualObject;
use xyz\ooopener\things\Thing;

/**
 * The object collection controller.
 */
class ConceptualObjectsController extends CollectionsController
{
    /**
     * Creates a new ConceptualObjectsController instance.
     * @param ContainerInterface $container
     * @param ?Collections $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = 'conceptualObjects' )
    {
        parent::__construct( $container , $model , $path , new ConceptualObjectValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="ConceptualObjectList",
     *     description="A conceptual object like artwork, sculture, painting, etc",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="category",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="movement",ref="#/components/schemas/Thesaurus"),
     * )
     *
     * @OA\Schema(
     *     schema="ConceptualObject",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/ConceptualObjectList")},
     *     @OA\Property(property="temporal",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="marks",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMark")),
     *     @OA\Property(property="materials",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMaterial")),
     *     @OA\Property(property="measurements",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMeasurement")),
     *     @OA\Property(property="numbers",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectNumber")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="productions",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="conceptualObjectResponse",
     *     description="Result of the conceptualObject",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/ConceptualObject")
     *     )
     * )
     *
     * @OA\Response(
     *     response="conceptualObjectListResponse",
     *     description="Result list of conceptualObjects",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        =>  [ 'filter' => Thing::FILTER_BOOL          ] ,
        'withStatus'    =>  [ 'filter' => Thing::FILTER_DEFAULT       ] ,
        'id'            =>  [ 'filter' => Thing::FILTER_ID            ] ,
        'name'          =>  [ 'filter' => Thing::FILTER_DEFAULT       ] ,
        'url'           =>  [ 'filter' => Thing::FILTER_URL           ] ,
        'created'       =>  [ 'filter' => Thing::FILTER_DATETIME      ] ,
        'modified'      =>  [ 'filter' => Thing::FILTER_DATETIME      ] ,

        'isBasedOn'     =>  [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,
        'image'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,
        'video'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,

        'alternateName' =>  [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'   =>  [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'text'          =>  [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'location'      =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'numAudios'     => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'     => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'     => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'audios'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,

        'brand'         => [ 'filter' => ConceptualObject::FILTER_BRAND ] ,

        'category'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE            ] ,
        'movement'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE            ] ,
        'marks'         => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'materials'     => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'measurements'  => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'temporal'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE            , 'skins' => [ 'full' , 'list' , 'extend' ]] ,
        'numbers'       => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'keywords'      => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'productions'   => [ 'filter' => ConceptualObject::FILTER_PRODUCTIONS , 'skins' => [ 'full' , 'extend' ] ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE              , 'skins' => [ 'full' , 'extend' ] ] ,
        'websites'      => [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ]
    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case ConceptualObject::FILTER_BRAND :
                    {
                        $init->{ $key } = $this->container
                                   ->get( 'conceptualObjectHasBrandController' )
                                   ->all( NULL , NULL , [ 'id' => (string) $init->id ] ) ;
                        break ;
                    }
                    case ConceptualObject::FILTER_LOCATION :
                    {
                        if( property_exists( $init , ConceptualObject::FILTER_LOCATION ) && $init->{ ConceptualObject::FILTER_LOCATION } != NULL )
                        {
                            $init->{ $key } =  $this->container
                                                    ->get( PlacesController::class )
                                                    ->create( (object) $init->{ ConceptualObject::FILTER_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case ConceptualObject::FILTER_PRODUCTIONS :
                    {
                        if( property_exists( $init , ConceptualObject::FILTER_PRODUCTIONS ) )
                        {
                            $init->{$key} = $this->container
                                                 ->get( 'conceptualObjectProductionsController' )
                                                 ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] );
                        }
                        break;
                    }
                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'conceptualObjectKeywordsController'            , $id ) ; // keywords
        $this->deleteAllInController( ConceptualObjectMarksController::class          , $id ) ; // marks
        $this->deleteAllInController( ConceptualObjectMaterialsController::class      , $id ) ; // materials
        $this->deleteAllInController( ConceptualObjectMeasurementsController::class   , $id ) ; // measurements
        $this->deleteAllInController( 'conceptualObjectArtMovementsController'        , $id ) ; // movement
        $this->deleteAllInController( ConceptualObjectNumbersController::class        , $id ) ; // numbers
        $this->deleteAllInController( 'conceptualObjectArtTemporalEntitiesController' , $id ) ; // temporal
        $this->deleteAllInController( 'conceptualObjectWebsitesController'            , $id ) ; // websites

        $this->deleteInController( 'conceptualObjectAudioController' , $id ) ; // audio
        $this->deleteInController( 'conceptualObjectImageController' , $id ) ; // image
        $this->deleteInController( 'conceptualObjectVideoController' , $id ) ; // video

        $this->deleteAllInController( 'conceptualObjectAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'conceptualObjectPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'conceptualObjectVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'userFavorites' , $id ) ; // favorites

        $this->deleteInEdgeModel( 'conceptualObjectConceptualObjectsCategories' , $id , 'to' ) ; // conceptualObject.category
        $this->deleteInEdgeModel( 'conceptualObjectHasBrand'                    , $id , 'to' ) ; // conceptualObject.brand
        $this->deleteInEdgeModel( 'conceptualObjectPlaces'                      , $id , 'to' ) ; // conceptualObject.location
        $this->deleteInEdgeModel( 'conceptualObjectProductionsOrganizations'    , $id , 'to' ) ; // conceptualObject.productions
        $this->deleteInEdgeModel( 'conceptualObjectProductionsPeople'           , $id , 'to' ) ; // conceptualObject.productions

        $this->deleteInEdgeModel( 'eventWorksFeatured' , $id /*from*/ ) ; // event.workFeatured

        $this->deleteAllArrayPropertyValuesInModel( Courses::class , 'discover' , $id ) ; // courses.discover
        $this->deleteAllArrayPropertyValuesInModel( Stages::class  , 'discover' , $id ) ; // stages.discover
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(255)' ]
        ] ;

        if( isset( $params['category'] ) )
        {
            $category = $params['category'] ;
            $conditions['category'] = [ $params['category'] , 'category' ] ;
        }

        if( isset( $params['location'] ) )
        {
            $location = $params['location'] ;
            $conditions['location'] = [ $params['location'] , 'location' ] ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                // insert category edge
                $this->insertEdge
                (
                    $category ,
                    $result->_id ,
                    $this->container->get( 'conceptualObjectConceptualObjectsCategories' )
                ) ;

                // insert location edge
                $this->insertEdge
                (
                    $location ,
                    $result->_id ,
                    $this->container->get( 'conceptualObjectPlaces' )
                ) ;

                return $this->success
                (
                    $response ,
                    $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] )
                ) ;
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @throws \ArangoDBClient\Exception
     * @OA\RequestBody(
     *     request="patchConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="movement",type="integer",description="The place ID"),
     *             @OA\Property(property="temporal",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;
        $movement = NULL ;
        $temporal = NULL ;

        $item = [] ;
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(255)' ] ;
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params[ 'location' ];
                $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ];
            }
            else
            {
                $location = FALSE ;
            }
        }

        if( isset( $params['category'] ) )
        {
            if( $params['category'] != '' )
            {
                $category = $params['category'] ;
                $conditions['category'] = [ $params['category'] , 'category' ] ;
            }
            else
            {
                $category = FALSE ;
            }
        }

        if( isset( $params['movement'] ) )
        {
            if( $params['movement'] != '' )
            {
                $movement = $params['movement'] ;
                $conditions['movement'] = [ $params['movement'] , 'movement' ] ;
            }
            else
            {
                $movement = FALSE ;
            }
        }

        if( isset( $params['temporal'] ) )
        {
            if( $params['temporal'] != '' )
            {
                $temporal = $params['temporal'] ;
                $conditions['temporal'] = [ $params['temporal'] , 'temporal' ] ;
            }
            else
            {
                $temporal = FALSE ;
            }
        }

        ////// --------Security - removes sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $category === NULL && $location === NULL && $movement === NULL && $temporal == NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $idTo = $this->model->table . '/' . $id ;

                $this->updateEdge
                (
                    $location ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectPlaces' )
                ) ;

                $this->updateEdge
                (
                    $category ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectConceptualObjectsCategories' )
                ) ;

                $this->updateEdge
                (
                    $movement ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectArtMovements' )
                ) ;

                $this->updateEdge
                (
                    $temporal ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectArtTemporalEntities' )
                ) ;

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('extend') ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="movement",type="integer",description="The place ID"),
     *             @OA\Property(property="temporal",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;
        $movement = NULL ;
        $temporal = NULL ;

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(255)' ]
        ] ;

        if( isset( $params['category'] ) && $params['category'] != '' )
        {
            $category = $params['category'] ;
            $conditions[ 'category' ] = [ $params[ 'category' ] , 'category' ] ;
        }
        else
        {
            $category = FALSE ;
        }

        if( isset( $params['location'] ) && $params['location'] != '' )
        {
            $location = $params[ 'location' ] ;
            $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ] ;
        }
        else
        {
            $location = FALSE ;
        }

        if( isset( $params['movement'] ) )
        {
            $movement = $params['movement'] ;
            $conditions[ 'movement' ] = [ $params[ 'movement' ] , 'movement' ] ;
        }
        else
        {
            $movement = FALSE ;
        }

        if( isset( $params['temporal'] ) )
        {
            $temporal = $params['temporal'] ;
            $conditions[ 'temporal' ] = [ $params[ 'temporal' ] , 'temporal' ] ;
        }
        else
        {
            $temporal = FALSE ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // Security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                $this->updateEdge
                (
                    $location ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectPlaces' )
                ) ;

                $this->updateEdge
                (
                    $category ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectConceptualObjectsCategories' )
                ) ;

                $this->updateEdge
                (
                    $movement ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectArtMovements' )
                ) ;

                $this->updateEdge
                (
                    $temporal ,
                    $idTo ,
                    $this->container->get( 'conceptualObjectArtTemporalEntities' )
                ) ;

                $this->model->update( $item , $id );

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }
}


