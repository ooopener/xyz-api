<?php

namespace xyz\ooopener\controllers\livestocks ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\WorkshopValidator;

use Psr\Container\ContainerInterface ;

use Exception ;

class WorkshopsController extends ThingsEdgesController
{
    /**
     * Creates a new WorkshopsController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new WorkshopValidator( $this->container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' =>  Thing::FILTER_BOOL        ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT      ] ,
        'id'         => [ 'filter' =>  Thing::FILTER_ID          ] ,
        'name'       => [ 'filter' =>  Thing::FILTER_DEFAULT     ] ,
        'url'        => [ 'filter' =>  Thing::FILTER_URL         ] ,
        'created'    => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,
        'modified'   => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,
        'isBasedOn'  => [ 'filter' => Thing::FILTER_DEFAULT      , 'skins' => [ 'full' ] ] ,
        'identifier' => [ 'filter' =>  Thing::FILTER_JOIN        ],
        'breeding'   => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],
        'production' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],
        'water'      => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],
        'workplaces' => [ 'filter' =>  Thing::FILTER_EDGE        ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        try
        {
            $this->container->get( WorkplacesController::class )->deleteAll( NULL , NULL , [ 'owner' =>  $id ] ) ;
            $this->container->get('workshopsBreedingsTypesController')->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;
            $this->container->get('workshopsProductionsTypesController')->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;
            $this->container->get('workshopsWaterOriginsController')->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'lang'  => $lang ,
            'owner' => $owner ,
            'skin'  => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = $this->config[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $livestock     = NULL ;
                $edgeLivestock = $this->edge->getEdge( (string) $result->id ) ;
                if( $edgeLivestock && property_exists( $edgeLivestock , Thing::FILTER_EDGE ) && is_array( $edgeLivestock->edge ) && count( $edgeLivestock->edge ) == 1 )
                {
                    $livestock = $edgeLivestock->edge[0] ;
                }
                $options[ 'object' ] = $livestock ;

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT = [ 'id' => NULL ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $ev = $this->owner->get($id , [ 'fields' => '_id' ]);
            if( !$ev )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [];

            $item['active'] = 1 ;
            $item['withStatus'] = Status::DRAFT ;

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)' ] ,
                'breeding'   => [ $params['breeding']   , 'required|int|breeding' ] ,
                'production' => [ $params['production'] , 'required|int|production' ] ,
                'identifier' => [ $params['identifier'] , 'required|int|identifier' ] ,
                'water'      => [ $params['water']      , 'required|int|water' ]
            ];

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                $check = $this->container
                              ->get('breedingsTypesProductionsTypesController')
                              ->exists( NULL , NULL , [ 'from' => $params['production'] , 'to' => $params['breeding'] ] ) ;

                if( !$check )
                {
                    return $this->error( $response , 'breedings types and productions types are not linked' , '400' );
                }

                $item['active'] = 1 ;
                $item['path']   = $this->owner->table . '/' . $this->path ;

                $result = $this->model->insert( $item ) ;

                if( $result )
                {
                    $this->edge->insertEdge( $result->_id , $ev->_id ) ; // save workshops livestocks edge

                    $workshopsBreedingsTypes   = $this->container->get('workshopsBreedingsTypes') ;
                    $workshopsProductionsTypes = $this->container->get('workshopsProductionsTypes') ;
                    $workshopsWaterOrigins     = $this->container->get('workshopsWaterOrigins') ;

                    $workshopsBreedingsTypes->insertEdge
                    (
                        $workshopsBreedingsTypes->from['name'] . '/' . $params['breeding'] , $result->_id
                    ) ;

                    $workshopsProductionsTypes->insertEdge
                    (
                        $workshopsProductionsTypes->from['name'] . '/' . $params['production'] , $result->_id
                    ) ;

                    $workshopsWaterOrigins->insertEdge
                    (
                        $workshopsWaterOrigins->from['name'] . '/' . $params['water'] , $result->_id
                    ) ;

                    return $this->success
                    (
                        $response ,
                        $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $result->_key ] )
                    ) ;
                }
                else
                {
                    return $this->error( $response ) ;
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id,
            'owner' => $owner
        ]
        = array_merge(self::ARGUMENTS_POST_DEFAULT , $args);

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $owner . ',' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)' ],
                'breeding'   => [ $params['breeding']   , 'required|int|breeding' ] ,
                'production' => [ $params['production'] , 'required|int|production' ]
            ];

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
                $conditions['identifier'] = [ $params['identifier'] , 'required|int|identifier' ] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            if( isset( $params['water'] ) )
            {
                $conditions['water'] = [ $params['water'] , 'required|int|water' ] ;
            }


            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                if( isset( $params['breeding'] ) && isset( $params['production'] ) )
                {
                    $check = $this->container
                                  ->get('breedingsTypesProductionsTypesController')
                                  ->exists( NULL , NULL , [ 'from' => $params['production'] , 'to' => $params['breeding'] ] ) ;

                    if( !$check )
                    {
                        return $this->error( $response , 'breedings types and productions types are not linked' , '400' );
                    }
                }

                $result = $this->model->update( $item , $id ) ;

                // update edges

                if( isset( $params['breeding'] ) && isset( $params['production'] ) )
                {
                    $workshopsBreedingsTypes   = $this->container->get('workshopsBreedingsTypes') ;
                    $workshopsProductionsTypes = $this->container->get('workshopsProductionsTypes') ;

                    $existWBTE = $workshopsBreedingsTypes->existEdge
                    (
                        $workshopsBreedingsTypes->from['name'] . '/' . $params['breeding'] ,
                        $workshopsBreedingsTypes->to['name']   . '/' . $id
                    ) ;

                    if( !$existWBTE )
                    {
                        $workshopsBreedingsTypes->delete( $workshopsBreedingsTypes->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $workshopsBreedingsTypes->insertEdge($workshopsBreedingsTypes->from['name'] . '/' . $params['breeding'] , $workshopsBreedingsTypes->to['name'] . '/' . $id ) ;
                    }

                    // save workshops productionTypes edge
                    $existWPTE = $workshopsProductionsTypes->existEdge( $workshopsProductionsTypes->from['name'] . '/' . $params['production'] , $workshopsBreedingsTypes->to['name'] . '/' . $id ) ;
                    if( !$existWPTE )
                    {
                        $workshopsProductionsTypes->delete( $workshopsProductionsTypes->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $workshopsProductionsTypes->insertEdge( $workshopsProductionsTypes->from['name'] . '/' . $params['production'] , $workshopsBreedingsTypes->to['name'] . '/' . $id ) ;
                    }
                }

                if( isset( $params['water'] ) )
                {
                    $workshopsWaterOrigins = $this->container->get('workshopsWaterOrigins') ;
                    $existWWOE = $workshopsWaterOrigins->existEdge( $workshopsWaterOrigins->from['name'] . '/' . $params['water'] , $workshopsWaterOrigins->to['name'] . '/' . $id ) ;
                    if( !$existWWOE )
                    {
                        $workshopsWaterOrigins->delete( $workshopsWaterOrigins->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $workshopsWaterOrigins->insertEdge( $workshopsWaterOrigins->from['name'] . '/' . $params['water'] , $workshopsWaterOrigins->to['name'] . '/' . $id ) ;
                    }
                }

                return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $owner , 'id' => $id ] ) ) ;
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
