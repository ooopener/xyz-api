<?php

namespace xyz\ooopener\controllers\livestocks;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\IdentifierValidator;

/**
 * The livestock numbers controller.
 */
class LivestockNumbersController extends ThingsEdgesController
{
    /**
     * Creates a new LivestockNumbersController instance.
     * @param ContainerInterface $container
     * @param ?Model             $model
     * @param ?Collections       $owner
     * @param ?Edges             $edge
     * @param ?string            $path
     */
    public function __construct
    (
        ContainerInterface $container    ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $owner ,
            $edge ,
            $path ,
            new IdentifierValidator( $container , 'livestocksNumbersTypes' )
        );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;
        $set    = $this->config[ 'livestocks-numbers' ] ;

        $item = [];

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['value'] ) )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'required|min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'additionalType' => [ $params['additionalType'] , 'int|required|additionalType' ] ,
            'value'          => [ $params['value']          , 'required' ] ,
        ];

        $this->conditions = $conditions ;
        $this->item       = $item ;
    }
}
