<?php

namespace xyz\ooopener\controllers\livestocks ;

use Exception ;

use Psr\Container\ContainerInterface ;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\WorkplaceValidator;

class WorkplacesController extends ThingsEdgesController
{
    /**
     * Creates a new WorkshopsController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new WorkplaceValidator( $this->container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'identifier' => [ 'filter' => Thing::FILTER_JOIN     ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'isBasedOn'  => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        //'location'      => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE    ] ,
        'sectors' => [ 'filter' =>  Thing::FILTER_EDGE    ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $this->deleteDependencies( $id ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( SectorsController::class , $id ) ; // sectors
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        try
        {
            $this->conditions = [] ;
            $this->item       = [] ;

            $bearer = $this->owner->get( $id , [ 'fields' => '_id' ] );
            if( !$bearer )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            $this->prepare( $request ) ;

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;
                if( $this->validator->passes() )
                {
                    $this->item['active']     = 1 ;
                    $this->item['withStatus'] = Status::DRAFT ;
                    $this->item['path']       = 'livestocks/' . $bearer->_id . '/' . $this->path ;

                    $new = $this->model->insert( $this->item ) ;

                    if( $new )
                    {
                        $this->edge->insertEdge( $new->_id , $bearer->_id ) ;

                        $this->owner->updateDate( $id ) ;

                        return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $new->_key ] ) ) ;
                    }
                    else
                    {
                        return $this->error( $response , 'error') ;
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;

        if( isset( $params['identifier'] ) )
        {
            $this->item['identifier'] = (int) $params['identifier'] ;
        }

        if( isset( $params['name'] ) )
        {
            $this->item['name'] = $params['name'] ;
        }

        $this->conditions[ 'name' ]       = [ $params['name']       , 'required|max(70)'        ] ;
        $this->conditions[ 'identifier' ] = [ $params['identifier'] , 'required|int|identifier' ] ;

        if( isset( $params['capacity'] ) )
        {
            $this->item['capacity']       = (int) $params['capacity'] ;
            $this->conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
        }

        if( isset( $params['numAttendee'] ) )
        {
            $this->item['numAttendee']       = (int) $params['numAttendee'] ;
            $this->conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
        }

        if( isset( $params['remainingAttendee'] ) )
        {
            $this->item['remainingAttendee']       = (int) $params['remainingAttendee'] ;
            $this->conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
        }
    }
}
