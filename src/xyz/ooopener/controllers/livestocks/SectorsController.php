<?php

namespace xyz\ooopener\controllers\livestocks ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\models\Workshops;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\SectorValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface ;

class SectorsController extends ThingsEdgesController
{
    /**
     * Creates a new SectorsController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Model|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Model $owner = NULL , Edges $edge = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new SectorValidator( $this->container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT   ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        try
        {
            $this->container->get('sectorSectorsTypesController')
                 ->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * The post request
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge(self::ARGUMENTS_POST_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        try
        {
            $this->conditions = [] ;
            $this->item       = [] ;

            $workplace = $this->owner->get( $id , [ 'fields' => '_id,_key' ]);
            if( !$workplace )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ') failed, the workplace reference is not found.' ] , NULL , 404);
            }

            $workshop = $this->container->get( Workshops::class )->get( $owner , [ 'fields' => '_id' ] );
            if( !$workshop )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . '), the workshop reference is not found.' ] , NULL , 404);
            }

            $this->prepare( $request ) ;

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;
                if( $this->validator->passes() )
                {
                    $this->item['active']     = 1 ;
                    $this->item['withStatus'] = Status::DRAFT ;
                    $this->item['path']       = 'livestocks/' . $workshop->_id . '/' . $workplace->_id . '/' . $this->path ;

                    $new = $this->model->insert( $this->item );

                    if( $new )
                    {
                        $this->edge->insertEdge( $new->_id , $workplace->_id ) ;

                        $sectorTypes = $this->container->get( 'sectorsSectorsTypes' ) ;
                        $sectorTypes->insertEdge
                        (
                            $sectorTypes->from['name'] . '/' . $this->getBodyParam( $request , 'additionalType' ) ,
                            $new->_id
                        ) ;

                        return $this->success
                        (
                            $response ,
                            $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $new->_key ] )
                        ) ;
                    }
                    else
                    {
                        return $this->error( $response , 'error' ) ;
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;

        if( isset( $params['name'] ) )
        {
            $this->item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $this->item['additionalType'] = (int) $params['additionalType'] ;
        }

        $this->conditions[ 'name' ]           = [ $params['name']           , 'required|max(70)'        ] ;
        $this->conditions[ 'additionalType' ] = [ $params['additionalType'] , 'required|additionalType' ] ;

        if( isset( $params['capacity'] ) )
        {
            $this->item['capacity'] = (int) $params['capacity'] ;
            $this->conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
        }

        if( isset( $params['numAttendee'] ) )
        {
            $this->item['numAttendee'] = (int) $params['numAttendee'] ;
            $this->conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
        }

        if( isset( $params['remainingAttendee'] ) )
        {
            $this->item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
            $this->conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge(self::ARGUMENTS_POST_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $owner . ',' . $id . ')');
        }

        try
        {
            $this->conditions = [] ;
            $this->item       = [] ;

            $bearer = $this->owner->exist( $owner , [ 'fields' => '_key' ] ) ;
            if( !$bearer )
            {
                return $this->formatError( $response ,'404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $this->prepare( $request ) ;

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {
                    $result = $this->model->update( $this->item , $id ) ;

                    // -------- additionalType

                    $edge = $this->container->get( 'sectorsSectorsTypes' ) ;

                    $from = $edge->from['name'] . '/' . $this->getBodyParam( $request , 'additionalType' ) ;
                    $to   = $this->model->table . '/' . $id ;

                    $exist = $edge->existEdge( $from , $to ) ;
                    if( !$exist )
                    {
                        $edge->deleteEdgeTo( $to ) ;
                        $edge->insertEdge( $from , $to ) ;
                    }

                    // -------- return

                    return $this->success
                    (
                        $response ,
                        $this->get( NULL , NULL , [ 'owner' => $owner , 'id' => $id ] )
                    ) ;
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
