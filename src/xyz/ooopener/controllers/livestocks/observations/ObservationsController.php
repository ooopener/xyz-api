<?php

namespace xyz\ooopener\controllers\livestocks\observations;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\livestocks\WorkshopsController;
use xyz\ooopener\controllers\users\UsersController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Livestocks;
use xyz\ooopener\things\Observation;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\ObservationValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;

use DateTime;

use Exception;

class ObservationsController extends CollectionsController
{
    /**
     * Creates a new ObservationsController instance.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path , new ObservationValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'startDate'     => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'endDate'       => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'alternateName' => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'notes'         => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'text'          => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,

        'about'          => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,
        'subject'        => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,
        'eventStatus'    => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,

        'audio'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'audios'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' ] ] ,
        'photos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' ] ] ,

        'actor'     => [ 'filter' => Thing::FILTER_EDGE ] ,
        'attendee'  => [ 'filter' => Thing::FILTER_EDGE ] ,
        'authority' => [ 'filter' => Observation::FILTER_AUTHORITY ] ,
        'location'  => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'owner'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'review'    => [ 'filter' => Thing::FILTER_DEFAULT ]
    ];

    public function allLivestock( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['facets'] = [ 'livestock' => $args['id'] ] ;
        return parent::all( $request , $response , $args );
    }

    public function allWorkshop( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['facets'] = [ 'workshop' => $args['id'] ] ;
        return parent::all( $request , $response , $args );
    }

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Observation::FILTER_AUTHORITY :
                    {
                        $init->{ $key } = $this->container
                                               ->get( 'observationAuthorityController' )
                                               ->all( NULL , NULL , [ 'id' => (string) $init->id ] ) ;
                        break;
                    }
                    case Observation::FILTER_FIRST_OWNER :
                    {
                        if( property_exists( $init , Observation::FILTER_FIRST_OWNER) )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( UsersController::class )
                                                   ->create( (object)$init->{ Observation::FILTER_FIRST_OWNER } ) ;
                        }
                        break;
                    }
                    case Observation::FILTER_OWNER :
                    {
                        if( property_exists( $init , Observation::FILTER_OWNER) )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( UsersController::class )
                                                   ->create( (object)$init->{ Observation::FILTER_OWNER } ) ;
                        }
                        break;
                    }
                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $controllers =
        [
            'observationActorsController'             ,
            'observationAttendeesController'          ,
            'observationLivestocksController'         ,
            'observationWorkshopsController'          ,
            'observationPlacesController'             ,
            'observationObservationsStatusController' ,
            'observationObservationsTypesController'  ,
            'observationOwnersController'             ,
            'observationPeopleController'
        ];

        foreach( $controllers as $ref )
        {
            $this->deleteAllInController( $ref , $id ) ;
        }

        $this->deleteAllInController( 'observationAuthorityController' , $id , NULL , 'id' ) ;

        $this->deleteInController( 'observationAudioController' , $id ) ; // audio
        $this->deleteInController( 'observationImageController' , $id ) ; // image
        $this->deleteInController( 'observationVideoController' , $id ) ; // video

        $this->deleteAllInController( 'observationAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'observationPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'observationVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'userFavorites' , $id ) ; // favorites
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] ): ?Response
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' );
        }

        $old = $this->model->get( $id , [ 'fields' => 'endDate,startDate' ] ) ;
        if( !$old )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        // check
        $params = $request->getParsedBody();

        $location = NULL ;
        $status = NULL ;

        $testEndDate   = $old->endDate ;
        $testStartDate = $old->startDate ;

        $item = [] ;
        $conditions = [] ;

        if( isset( $params['review'] ) )
        {
            try
            {
                $item['review'] = json_decode( $params['review'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ')' ] , NULL , 400 );
            }
        }

        if( isset( $params['endDate'] ) )
        {
            if( $params['endDate'] != '' )
            {
                $item['endDate'] = $testEndDate = $params['endDate'] ;
                $conditions['endDate'] = [ $params['endDate'] , 'datetime' ] ;
            }
            else
            {
                $item['endDate'] = $testEndDate = NULL ;
            }
        }

        if( isset( $params['startDate'] ) )
        {
            $item['startDate'] = $testStartDate = $params['startDate'] ;
            $conditions['startDate'] = [ $params['startDate'] , 'datetime' ] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            if( $params['eventStatus'] != '' )
            {
                $status = $params['eventStatus'] ;
                $conditions['eventStatus'] = [ $params['eventStatus'] , 'required|eventStatus' ] ;
            }
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params['location'] ;
                $conditions['location'] = [ $params['location'] , 'location' ] ;
            }
            else
            {
                $location = FALSE ;
            }
        }

        /// check dates

        if( $testStartDate && $testEndDate )
        {
            $after = (boolean) (strtotime($testStartDate) < strtotime($testEndDate)) ;
            $conditions['after']  = [ $after , 'true' ] ;
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $status !== NULL )
                {
                    $edge   = $this->container->get('observationObservationsStatus') ;
                    $idFrom = $edge->from['name'] . '/' . $status ;
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $location !== NULL )
                {
                    $locEdge = $this->container->get('observationPlaces') ;
                    $idFrom  = $locEdge->from['name'] . '/' . $location ;
                    if( !$locEdge->existEdge( $idFrom , $idTo ) )
                    {
                        $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        $locEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( array_key_exists( 'review' , $item ) )
                {
                    $result = $this->model->update( [ 'review' => '' ] , $id );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] ): ?Response
    {
        try
        {
            [
                'id'    => $id ,
                'owner' => $owner
            ]
            = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

            if( $response )
            {
                $this->logger->debug( $this . ' post(' . $id . ')' );
            }

            $workshop = $this->container->get( WorkshopsController::class )->get( NULL , NULL , [ 'id' => $id , 'skin' => 'full' ] ) ;
            if( !$workshop )
            {
                return $this->formatError( $response , '404', [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            $livestock = $this->container->get('livestocksWorkshops')->getEdge( (string) $workshop->id ) ;
            if( !$livestock && !property_exists( 'edge' , $livestock ) && !is_array( $livestock->edge ) && ( count( $livestock->edge ) != 1 ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            $livestock = (object) $livestock->edge[0] ;

            $item   = [];
            $params = $request->getParsedBody();

            $additionalType = isset( $params['additionalType'] ) ? (string) $params['additionalType'] : NULL ;

            if( isset( $params['alternateName'] ) )
            {
                $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
            }

            if( isset( $params['review'] ) )
            {
                try
                {
                    $item['review'] = json_decode( $params['review'] , true ) ;
                }
                catch(Exception $e)
                {
                    return $this->formatError( $response , '400', [ 'post(' . $id . ') failed, the review reference is not a valid JSON string representation' ] , NULL , 400 );
                }
            }
            else
            {
                try
                {
                    $item['review'] = [ 'about' => json_decode( json_encode( $workshop ) , true ) ] ;
                }
                catch(Exception $e)
                {
                    return $this->formatError( $response , '400', [ 'post(' . $id . ') failed, impossible to json encode the workshop reference of this new observation' ] , NULL , 400 );
                }
            }

            $eventStatus = $this->getDefaultObservationStatus() ;

            $conditions =
            [
                'eventStatus'    => [ $eventStatus    , 'required'                ] ,
                'additionalType' => [ $additionalType , 'required|additionalType' ]
            ] ;

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                $item['active']     = 1 ;
                $item['withStatus'] = Status::DRAFT ;
                $item['path']       = 'livestocks/' . $this->path ;
                $item['audios']     = [] ;
                $item['photos']     = [] ;
                $item['videos']     = [] ;

                $user = $this->container->get('auth') ;

                $item['firstOwner'] = $user ? (int) $user->id : null ;

                ///// TODO check if user is a veterinarian and authorized to post observation

                $new = $this->model->insert( $item );
                $id  = $new->_key ;

                // get type name
                $observationTpe = $this->container->get('observationsTypesController')->get( NULL , NULL , [ 'id' => $additionalType , 'fields' => 'name' ] ) ;

                $date = new DateTime();
                $name = strtoupper( str_replace( '/' , '-' , $observationTpe->alternateName ) ) . '-' . $date->format( 'Ymd' ) . '-' . $new->_key ;

                $update = $this->model->update( [ 'name' => $name ] , $new->_key ) ;

                ///// ---------------------- edges

                $this->container->get( 'observationLivestocks' )
                                ->insertEdge( 'livestocks/' . $livestock->id , $new->_id ) ;

                $this->container->get( 'observationWorkshops' )
                                ->insertEdge( 'workshops/' . $workshop->id , $new->_id ) ;

                $this->setEdgeFrom( $id , $eventStatus    , 'observationObservationsStatus' ) ;
                $this->setEdgeFrom( $id , $additionalType , 'observationObservationsTypes'  ) ;

                $edge = $this->container->get('observationOwners') ;
                $edge->insertEdge( $edge->from['name'] . '/' . $user->id , $new->_id ) ;

                if(
                       property_exists( $livestock , 'organization' )
                    && array_key_exists( 'location' , $livestock->organization )
                    && $livestock->organization['location'] != NULL
                )
                {
                    $this->container->get( 'observationPlaces' )
                                    ->insertEdge( 'places/' . $livestock->organization['location']['id'] , $new->_id ) ;
                }

                return $this->success
                (
                    $response ,
                    $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] )
                ) ;
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'       => NULL ,
        'owner'    => NULL ,
        'workshop' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [
            'id'       => $id ,
            'owner'    => $owner ,
            'workshop' => $workshop
        ]
        = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $workshop . ',' . $id . ')' );
        }

        $params = $request->getParsedBody();

        $status = NULL ;

        $item = [];

        if( isset( $params['endDate'] ) && !empty( $params['endDate'] ) )
        {
            $item['endDate'] = $params['endDate'] ;
        }

        if( isset( $params['startDate'] ) && !empty( $params['startDate'] ) )
        {
            $item['startDate'] = $params['startDate'] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            $status = $params['eventStatus'] ;
        }

        if( isset( $params['review'] ) )
        {
            try
            {
                $item['review'] = json_decode( $params['review'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'put(' . $id . ')' ] , NULL , 400 );
            }
        }

        $conditions =
        [
            'name'         => [ $params['name']        , 'required|max(70)'    ] ,
            'endDate'      => [ $params['endDate']     , 'simpleDate'          ] ,
            'startDate'    => [ $params['startDate']   , 'simpleDate' ] ,
            'status'       => [ $status                , 'required|status'     ]
        ] ;

        if( isset( $params['startDate'] ) && isset( $params['endDate'] ) )
        {
            $after = (boolean) (strtotime($params['startDate']) <= strtotime($params['endDate'])) ;
            $conditions['after']  = [ $after , 'after' ] ;
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                // check livestock
                $livestock = $this->container->get( Livestocks::class )->get( $owner , [ 'fields' => '_id' ] ) ;
                if( !$livestock )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' ] , NULL , 404 );
                }

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                if( $status )
                {
                    $edge   = $this->container->get('observationObservationsStatus') ;
                    $idFrom = $edge->from['name'] . '/' . $status ;
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success
                    (
                        $response ,
                        $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) )
                    );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError
                (
                    $response ,
                    '500',
                    [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' ,
                    $e->getMessage() ] ,
                    NULL ,
                    500
                );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    protected function getDefaultObservationStatus( string $model = 'observationsStatus' , string $identifier = 'open' ) :?int
    {
        if( $this->container->get( $model ) )
        {
            $thing = $this->container
                          ->get( $model )
                          ->get( $identifier , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;

            if( $thing )
            {
                return (int) $thing->_key ;
            }
        }
        return NULL ;
    }
}
