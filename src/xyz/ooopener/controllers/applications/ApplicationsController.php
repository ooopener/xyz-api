<?php

namespace xyz\ooopener\controllers\applications;

use Psr\Container\ContainerInterface ;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\HttpCache\CacheProvider;

use Exception;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\courses\CoursesController;
use xyz\ooopener\controllers\events\EventsController;
use xyz\ooopener\controllers\games\ApplicationGamesController;
use xyz\ooopener\controllers\users\UsersController;
use xyz\ooopener\helpers\Hash;
use xyz\ooopener\helpers\Version;
use xyz\ooopener\models\Collections;

use xyz\ooopener\things\Application;
use xyz\ooopener\validations\setting\AmbuloSettingValidator;
use xyz\ooopener\validations\ApplicationValidator;

use xyz\ooopener\things\Thing;

/**
 * The applications collection controller.
 */
class ApplicationsController extends CollectionsController
{
    /**
     * Creates a new ApplicationsController instance.
     *
     * @param ContainerInterface $container
     * @param Collections|null $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = 'applications' )
    {
        parent::__construct( $container , $model , $path ,  new ApplicationValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="ApplicationList",
     *     description="An application",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="slogan",ref="#/components/schemas/text"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     * )
     *
     * @OA\Schema(
     *     schema="Application",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/ApplicationList")},
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="producer",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="publisher",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="sponsor",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website")),
     * )
     *
     * @OA\Response(
     *     response="applicationResponse",
     *     description="Result of the application",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Application")
     *     )
     * )
     *
     * @OA\Response(
     *     response="applicationListResponse",
     *     description="Result list of applications",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/ApplicationList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'         => [ 'filter' =>  Thing::FILTER_BOOL        ] ,
        'id'             => [ 'filter' =>  Thing::FILTER_ID          ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT     ] ,
        'url'            => [ 'filter' =>  Thing::FILTER_URL         ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'audio' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'image' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'video' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,

        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'notes'               => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'slogan'              => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'                => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'version'   => [ 'filter' => Thing::FILTER_DEFAULT     ] ,
        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'owner'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE     , 'skins' => [ 'full' , 'compact' ] ] ,
        'oAuth'     => [ 'filter' => Thing::FILTER_DEFAULT         , 'skins' => [ 'full' , 'compact' ] ] ,
        'audios'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY      , 'skins' => [ 'full' ] ] ,
        'photos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY      , 'skins' => [ 'full' ] ] ,
        'videos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY      , 'skins' => [ 'full' ] ] ,
        'producer'  => [ 'filter' => Application::FILTER_PRODUCER  , 'skins' => [ 'extend' , 'full' , 'compact' ] ],
        'publisher' => [ 'filter' => Application::FILTER_PUBLISHER , 'skins' => [ 'extend' , 'full' , 'compact' ] ],
        'sponsor'   => [ 'filter' => Application::FILTER_SPONSOR   , 'skins' => [ 'extend' , 'full' , 'compact' ] ] ,
        'setting'   => [ 'filter' => Application::FILTER_SETTING   , 'skins' => [ 'extend' , 'full' , 'compact' ] ] ,
        'websites'  => [ 'filter' => Thing::FILTER_EDGE            , 'skins' => [ 'extend' , 'full' , 'compact' ] ]

    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                try
                {
                    switch( $key )
                    {
                        case Application::FILTER_OWNER :
                        {
                            if( property_exists( $init , Application::FILTER_OWNER) && $init->{ Application::FILTER_OWNER } != null )
                            {
                                $usersController = $this->container->get( UsersController::class ) ;
                                $init->{ $key } = $usersController->create( (object)$init->{ Application::FILTER_OWNER } , $lang ) ;
                            }
                            break;
                        }
                        case Application::FILTER_PRODUCER :
                        {
                            if( property_exists( $init , Application::FILTER_PRODUCER) )
                            {
                                $init->{ $key } = $this->container
                                                ->get( 'applicationProducerController' )
                                                ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;
                            }
                            break;
                        }
                        case Application::FILTER_PUBLISHER :
                        {
                            if( property_exists( $init , Application::FILTER_PUBLISHER) )
                            {
                                $init->{ $key } = $this->container
                                                ->get( 'applicationPublisherController' )
                                                ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;
                            }
                            break;
                        }
                        case Application::FILTER_SETTING :
                        {
                            if( property_exists( $init , Application::FILTER_SETTING ) && $init->{ Application::FILTER_SETTING } != null )
                            {
                                if( property_exists( $init , Thing::FILTER_ADDITIONAL_TYPE ) && array_key_exists( Thing::FILTER_ALTERNATE_NAME , $init->{ Thing::FILTER_ADDITIONAL_TYPE } ) )
                                {
                                    switch( $init->{ Thing::FILTER_ADDITIONAL_TYPE }[ Thing::FILTER_ALTERNATE_NAME ] )
                                    {
                                        case 'ambulo' :
                                        {
                                            $courses = $this->container->get( CoursesController::class ) ;
                                            if( array_key_exists( 'courses' , $init->{ Application::FILTER_SETTING } ) && $init->{ Application::FILTER_SETTING }['courses'] != NULL && is_array( $init->{ Application::FILTER_SETTING }['courses'] ) && count( $init->{ Application::FILTER_SETTING }['courses'] ) > 0 )
                                            {
                                                $init->{ Application::FILTER_SETTING }['courses'] = $courses->all
                                                (
                                                    NULL ,
                                                    NULL ,
                                                    [
                                                        'facets' => [ 'idsSorted' => implode( ',' , $init->{ Application::FILTER_SETTING }['courses'] ) ] ,
                                                        'lang'   => $lang
                                                    ]
                                                ) ;
                                            }

                                            $events = $this->container->get( EventsController::class ) ;
                                            if( array_key_exists( 'events' , $init->{ Application::FILTER_SETTING } ) && $init->{ Application::FILTER_SETTING }['events'] != NULL && is_array( $init->{ Application::FILTER_SETTING }['events'] ) && count( $init->{ Application::FILTER_SETTING }['events'] ) > 0 )
                                            {
                                                $init->{ Application::FILTER_SETTING }['events'] = $events->all
                                                (
                                                    NULL ,
                                                    NULL ,
                                                    [
                                                        'facets' => [ 'idsSorted' => implode( ',' , $init->{ Application::FILTER_SETTING }['events'] ) ] ,
                                                        'lang'   => $lang
                                                    ]
                                                ) ;
                                            }

                                            $games = $this->container->get( ApplicationGamesController::class ) ;
                                            if( array_key_exists( 'game' , $init->{ Application::FILTER_SETTING } ) && $init->{ Application::FILTER_SETTING }['game'] != NULL )
                                            {
                                                $init->{ Application::FILTER_SETTING }['game'] = $games->get
                                                (
                                                    NULL ,
                                                    NULL ,
                                                    [
                                                        'id'   => (string) $init->{ Application::FILTER_SETTING }['game'] ,
                                                        'skin' => 'full' ,
                                                        'lang' => $lang
                                                    ]
                                                ) ;
                                            }
                                            break ;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case Application::FILTER_SPONSOR :
                        {
                            if( property_exists( $init , Application::FILTER_SPONSOR) )
                            {
                                $init->{ $key } = $this->container
                                                ->get( 'applicationSponsorController' )
                                                ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;
                            }
                            break;
                        }
                    }
                }
                catch( Exception $exception )
                {

                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'applicationApplicationsTypesController' , $id ) ; // additionalType
        $this->deleteAllInController( 'applicationProducerController'          , $id ) ; // producer
        $this->deleteAllInController( 'applicationPublisherController'         , $id ) ; // publisher
        $this->deleteAllInController( 'applicationSponsorController'           , $id ) ; // sponsor
        $this->deleteAllInController( 'applicationUsersController'             , $id ) ; // user
        $this->deleteAllInController( 'applicationWebsitesController'          , $id ) ; // websites

        $this->deleteInController( 'applicationAudioController' , $id ) ; // audio
        $this->deleteInController( 'applicationImageController' , $id ) ; // image
        $this->deleteInController( 'applicationVideoController' , $id ) ; // video

        $this->deleteAllInController( 'applicationAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'applicationPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'applicationVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'id'      => NULL ,
        'item'    => NULL ,
        'key'     => '_key' ,
        'lang'    => NULL ,
        'params'  => NULL ,
        'skin'    => NULL
    ] ;

    /**
     * Get a specific item reference.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return mixed
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active'     => $active ,
            'conditions' => $conditions ,
            'id'         => $id ,
            'item'       => $item ,
            'key'        => $key ,
            'lang'       => $lang ,
            'params'     => $params ,
            'skin'       => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ init

        $api = $this->config['api'] ;
        $set = $this->config[$this->path] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            // ----- active

            if( isset( $params[ 'active' ] ) )
            {
                if
                (
                    $params[ 'active' ] == "0" ||
                    $params[ 'active' ] == 'false' ||
                    $params[ 'active' ] == 'FALSE'
                )
                {
                    $active = NULL;
                }
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ----------------

        try
        {
            $item = $this->model->get
            (
                $id ,
                [
                    'active'      => $active ,
                    'conditions'  => $conditions ,
                    'key'         => $key ,
                    'lang'        => $lang ,
                    'queryFields' => $this->getFields( $skin )
                ]
            ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang , $skin , $params ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                $cache = $this->container->get( CacheProvider::class ) ;
                $response = $cache->withETag( $response , $item->modified );
                $response = $cache->withLastModified( $response , $item->modified );
                return $this->success( $response, $item, $this->getCurrentPath( $request , $params ) );
            }

            return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;

        }

        return $item ;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return mixed|object|null
     */
    public function getApp( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $jwt = $this->container->get('jwt') ;

        $this->logger->info( $this . ' ' . __FUNCTION__ );

        if( $jwt && property_exists( $jwt , 'aud') && $jwt->aud )
        {
            $args['id']  = $jwt->aud ;
            $args['key'] = 'oAuth.client_id' ;
        }
        else
        {
            return $this->formatError( $response , '403' , NULL , NULL , 403 ) ;
        }
        $args['skin'] = 'extend' ;
        return $this->get( $request , $response , $args ) ;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postApplication",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        $params = $request->getParsedBody();

        $additionalType = NULL ;

        $item           = [];
        $item['active'] = 1 ;
        $item['path']   = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']  , 'required|min(2)|max(70)' ] ,
            'additionalType' => [ $additionalType  , 'required|additionalType' ]
        ] ;

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $hash = $this->container->get( Hash::class ) ;

                // generate client_id and client_secret

                $oAuth = [] ;
                $oAuth['client_id']     = $hash->generateUUID() ;
                $oAuth['client_secret'] = $hash->generateSecret() ;
                $oAuth['url_redirect']  = '' ;

                $item['oAuth']   = $oAuth ;
                $item['setting'] = [] ;

                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $latest = $this->model->insert( $item );

                ///// edges

                // add user edge
                $user     = $this->container->get( 'auth' ) ;
                $userEdge = $this->container->get( 'applicationUsers' ) ;
                $userEdge->insertEdge( $userEdge->from['name'] . '/' . $user->id , $latest->_id ) ;

                // add additionalType edge
                $addTypeEdge = $this->container->get( 'applicationApplicationsTypes' ) ;
                $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $latest->_id ) ;

                if( $latest )
                {
                    return $this->success( $response , $this->model->get( $latest->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT = [ 'id' => NULL ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     * @OA\RequestBody(
     *     request="patchApplication",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType")
     *         )
     *     ),
     *     required=true
     * )
     * @throws \ArangoDBClient\Exception
     */
    public function patch( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        $params = $request->getParsedBody() ;

        $item       = [] ;
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        $additionalType = NULL ;
        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        if( isset( $params['version'] ) )
        {
            $item['version'] = Version::fromString( $params['version'] ) ;
            $conditions['version'] = [ $params['version'] , 'version' ] ;
        }

        // check if there is at least one param
        if( empty( $item ) )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }


        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->get( 'applicationApplicationsTypes' ) ;
                    $idFrom      = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                        $addTypeEdge->insertEdge( $idFrom , $idTo ) ; // add edge
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) ) ) ;
                }
                else
                {
                    return $this->error( $response ,'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchApplicationOAuth",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url_redirect",type="string",description="The url redirect")
     *         )
     *     ),
     *     required=true
     * )
     * @throws \ArangoDBClient\Exception
     */
    public function patchOAuth( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' patchOAuth(' . $id . ')');
        }

        $conditions   = [] ;
        $params       = $request->getParsedBody() ;
        $url_redirect = NULL ;

        if( isset( $params['url_redirect'] ) )
        {
            $url_redirect = $params['url_redirect'] ;
            $conditions['url_redirect'] = [ $params['url_redirect'] , 'redirect' ] ;
        }

        // check if there is at least one param
        if( !isset( $url_redirect ) )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        $thing = $this->model->get( $id , [ 'fields' => 'oAuth' ] ) ;
        if( !$thing )
        {
            return $this->formatError( $response , '404', [ 'patchOAuth(' . $id . ')' ] , NULL , 404 );
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $oAuth = $thing->oAuth ;
                $oAuth['url_redirect'] = $url_redirect ;

                $result = $this->model->update( [ 'oAuth' => $oAuth ] , $id );

                if( $result )
                {
                    return $this->success
                    (
                        $response ,
                        $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) )
                    ) ;
                }
                else
                {
                    return $this->error( $response ,'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patchOAuth(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * Patch the secret value of the application.
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @throws \ArangoDBClient\Exception
     */
    public function patchSecret( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' patchSecret(' . $id . ')');
        }

        $thing = $this->model->get( $id , [ 'fields' => 'oAuth' ] ) ;
        if( !$thing )
        {
            return $this->formatError( $response , '404', [ 'patchSecret(' . $id . ')' ] , NULL , 404 );
        }

        try
        {
            $hash = $this->container->get( Hash::class ) ;

            $oAuth = $thing->oAuth ;
            $oAuth['client_secret'] = $hash->generateSecret() ;

            $result = $this->model->update( [ 'oAuth' => $oAuth ] , $id );

            if( $result )
            {
                return $this->success( $response , $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) ) ) ;
            }
            else
            {
                return $this->error( $response ,'error' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchSecret(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchApplicationSetting",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="setting",type="object",description="The setting of the application")
     *         )
     *     ),
     *     required=true
     * )
     * @throws \ArangoDBClient\Exception
     */
    public function patchSetting( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' patchSetting(' . $id . ')');
        }

        // check if resource exists
        $thing = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
        if( !$thing )
        {
            return $this->formatError( $response , '404', [ 'patchSetting(' . $id . ')' ] , NULL , 404 );
        }

        // check
        $params = $request->getParsedBody() ;

        $item       = [] ;
        $conditions = [] ;
        $validator  = NULL ;

        if( isset( $params['setting'] ) )
        {
            try
            {
                $setting = json_decode( $params['setting'] , true ) ;

                if( property_exists( $thing , Thing::FILTER_ADDITIONAL_TYPE ) && array_key_exists( Thing::FILTER_ALTERNATE_NAME , $thing->{ Thing::FILTER_ADDITIONAL_TYPE } ) )
                {
                    switch( $thing->{ Thing::FILTER_ADDITIONAL_TYPE }[ Thing::FILTER_ALTERNATE_NAME ] )
                    {
                        case 'ambulo' :
                        {
                            $validator = new AmbuloSettingValidator( $this->container ) ;

                            $item['setting'] = null ;

                            // courses
                            if( array_key_exists( 'courses' , $setting ) && is_array( $setting['courses'] ) )
                            {
                                $conditions['courses'] = [ $setting['courses'] , 'courses' ] ;
                                $item['setting']['courses'] = $setting['courses'] ;
                            }

                            // events
                            if( array_key_exists( 'events' , $setting ) && is_array( $setting['events'] ) )
                            {
                                $conditions['events'] = [ $setting['events'] , 'events' ] ;
                                $item['setting']['events'] = $setting['events'] ;
                            }

                            // game
                            if( array_key_exists( 'game' , $setting ) && is_string( $setting['game'] ) )
                            {
                                $conditions['game'] = [ $setting['game'] , 'game' ] ;
                                $item['setting']['game'] = $setting['game'] ;
                            }

                            // agenda
                            if( array_key_exists( 'agenda' , $setting ) && is_bool( $setting['agenda'] ) )
                            {
                                $conditions['agenda'] = [ $setting['agenda'] , 'bool' ] ;
                                $item['setting']['agenda'] = $setting['agenda'] ;
                            }

                            // version
                            if( array_key_exists( 'version' , $setting ) && is_string( $setting['version'] ) )
                            {
                                $conditions['version'] = [ $setting['version'] , 'version' ] ;
                                $item['setting']['version'] = $setting['version'] ;
                            }

                            break;
                        }
                    }
                }
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'patchSetting(' . $id . ')' ] , NULL , 400 );
            }
        }

        // check if there is at least one param
        if( empty( $item ) )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        ////// validator

        if( $validator != null  )
        {
            $validator->validate($conditions);

            if( $validator->passes() )
            {
                try
                {
                    if( array_key_exists('setting' , $item) )
                    {
                        $result = $this->model->update([ 'setting' => '' ] , $id);
                    }

                    $result = $this->model->update($item , $id);

                    if( $result )
                    {
                        return $this->success($response , $this->create($this->model->get($id , [ 'queryFields' => $this->getFields('full') ])));
                    }
                    else
                    {
                        return $this->error($response , 'error');
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError($response , '500' , [ 'patchSetting(' . $id . ')' , $e->getMessage() ] , NULL , 500);
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
    }
}
