<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Model;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

use Exception ;

use system\date\ISO8601 ;


/**
 * The active controller class.
 *
 * @OA\Schema(
 *     schema="successActive",
 *     allOf={},
 *     @OA\Property(property="active",type="boolean"),
 *     @OA\Property(property="modified",type="string",format="date-time"),
 *     @OA\Property(property="created",type="string",format="date-time"),
 *     @OA\Property(property="id",type="integer"),
 *     @OA\Property(property="url",type="string",format="uri")
 * )
 */
class ActiveController extends Controller
{
    /**
     * Creates a new ActiveController instance.
     * @param ContainerInterface $container
     * @param ?Model $model
     * @param ?string $path
     */
    public function __construct( ContainerInterface $container , ?Model $model = NULL , ?string $path = NULL )
    {
        parent::__construct( $container );
        $this->model   = $model ;
        $this->iso8601 = $this->container->get( ISO8601::class ) ;
        if( !empty( $path ) )
        {
            $this->path     = $path ;
            $this->fullPath = '/' . $path ;
        }
    }

    /**
     * The full path expression.
     */
    public string $fullPath;

    protected ISO8601 $iso8601 ;

    /**
     * The model reference.
     */
    public ?Model $model;

    /**
     * The path reference.
     */
    public ?string $path;

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Returns if the specific item is active.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => '_key,active,modified,created' ] ) ;

            if( !$item )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            return $this->success( $response , $this->populate( $item , $request ) ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'   => NULL ,
        'bool' => NULL
    ] ;

    /**
     * Switch the activity of the specific item.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] ):Response
    {
        [
            'id'   => $id ,
            'bool' => $bool
        ]
        = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            if( is_string( $bool ) )
            {
                $bool = strtolower( $bool ) ;
            }

            if( $bool != 'true' && $bool != 'false' )
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ',' . $bool . ')' ] );
            }

            $active = $bool == 'true' ;

            $result = $this->model->update( [ 'active' => (int) $active ] , $id ) ;

            if( $result )
            {
                $thing = $this->model
                              ->get( $id , [ 'fields' => '_key,active,modified,created' ] ) ;

                return $this->success( $response , $this->populate( $thing , $request ) ) ;
            }

            return $this->error( $response , 'error' ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function populate( object $thing , Request $request = NULL ) :object
    {
        if( $thing )
        {
            $thing->id       = $thing->_key ;
            $thing->created  = $this->iso8601->formatTimeToISO8601( $thing->created , TRUE ) ;
            $thing->modified = $this->iso8601->formatTimeToISO8601( $thing->modified , TRUE ) ;
            $thing->active   = (bool) $thing->active ;
            $thing->url      = $this->getCurrentPath( $request ) ;

            unset( $thing->_key ) ;
        }
        return $thing ;
    }
}


