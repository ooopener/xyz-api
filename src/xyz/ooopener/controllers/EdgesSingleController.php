<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use Exception;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

class EdgesSingleController extends EdgesController
{
    /**
     * Creates a new EdgesSingleController instance.
     * @param ContainerInterface $container
     * @param Model|NULL         $model
     * @param Collections|NULL   $owner
     * @param Edges|NULL         $edge
     * @param array|NULL         $conditions
     * @param string|NULL        $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        Model              $model      = NULL ,
        Collections        $owner      = NULL ,
        ?Edges             $edge       = NULL ,
        ?array             $conditions = NULL ,
        ?string            $path       = NULL
    )
    {
        parent::__construct ( $container , $model , $owner , $edge , $path ) ;

        $this->conditions = $conditions ;
    }

    protected function getEdgeMock() :?array
    {
        if( $this->owner )
        {
            $table = $this->owner->table ;
            return
            [
                'controller' => $table . 'Controller' ,
                'name'       => $table
            ];
        }
        return NULL ;
    }

    /**
     * @param ?Request $request
     * @param ?Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function delete( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug($this . ' delete(' . $id . ')');
        }

        try
        {
            $o = $this->owner->get($id , [ 'fields' => '_key' ]);
            if( !$o )
            {
                return $this->formatError($response , '404' , [ 'delete(' . $id . ')' ] , NULL , 404);
            }

            $idTo = $this->owner->table . '/' . $id ;

            $this->edge->delete( $idTo , [ 'key' => '_to' ] ) ; // delete all edges to be sure
            $this->owner->updateDate( $id ) ; // update owner date

            return $this->success( $response , (int) $id ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError($response , '500' , [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500);
        }
    }

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'skin'  => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        try
        {
            $result = parent::get( NULL , NULL , $args ) ;

            // get first result
            if( is_array( $result ) && count( $result ) > 0 )
            {
                $result = $result[0] ;
            }
            else
            {
                $result = NULL ;
            }

            return $response ? $this->success( $response , $result , $this->getCurrentPath( $request ) )
                             : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id,
            'owner' => $owner
        ]
        = array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $owner . ',' . $id . ')');
        }

        try
        {
            if( !$this->owner->exist( $owner ) )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $edgeFromController = $this->container->get( $this->edge->from['controller'] ) ;

            $item = $this->model->get
            (
                $id ,
                [
                    'conditions'  => $this->conditions ,
                    'queryFields' => $edgeFromController->getFields()
                ]
            ) ;

            if( $item == NULL )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $from = $this->model->table . '/' . $id ;
            $to   = $this->owner->table . '/' . $owner ;

            $this->edge->delete( $to , [ 'key' => '_to' ] ) ; // delete all edges to be sure
            $this->edge->insertEdge( $from , $to ) ;
            $this->owner->updateDate( $owner ) ;

            return $this->success( $response , $edgeFromController->create( $item ) ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
