<?php

namespace xyz\ooopener\controllers\oauth;


use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\Sessions;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\Controller ;

use Firebase\JWT\JWT;
use Psr\Container\ContainerInterface;

use Exception ;

/**
 * The revoke controller.
 */
class RevokeController extends Controller
{
    /**
     * Creates a new class RevokeController instance.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container );
    }

    const ARGUMENTS_REVOKE_DEFAULT = [ 'token' => NULL ];

    /**
     * Revoke token from GET and POST request
     * @param Request|NULL $request The request
     * @param Response|NULL $response The response
     * @param array $args The arguments
     * @return mixed
     */
    public function revoke( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'token' => $token ] = array_merge( self::ARGUMENTS_REVOKE_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' revoke' ) ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
            if( !$params )
            {
                $params = $request->getParsedBody() ;
            }
            $token = isset( $params['token'] ) ? $params['token'] : NULL ;
        }
        else
        {
            $token = $args['token'] ;
        }

        //// check required parameters

        if( $token == NULL )
        {
            return $this->error( $response , "The request is missing a required parameter " , "400" );
        }

        try //// check if token exists and not expired
        {
            $jwt = JWT::decode
            (
                $token ,
                $this->config['token']['key'] ,
                $this->config['token']['algorithm']
            ) ;

            if( $jwt && $jwt->jti )
            {
                $this->logger->debug( $this . " revoke, token id: " . $jwt->jti ) ;

                $agent = $this->container
                              ->get( UserInfos::class )
                              ->getUserAgent() ;

                $check = $this->container
                              ->get( Sessions::class )
                              ->check( $jwt->jti , $agent ) ;

                if( $check )
                {
                    $this->container->get( Sessions::class )->revoke( $jwt->jti ) ; // revoke token
                    return $this->success( $response , "ok" ) ;
                }
            }

            return $this->formatError( $response , "500" , NULL , NULL , 500 ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ] , NULL , 500 ) ;
        }
    }
}


