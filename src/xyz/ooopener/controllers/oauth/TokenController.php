<?php

namespace xyz\ooopener\controllers\oauth;

use xyz\ooopener\controllers\users\UsersController;
use xyz\ooopener\helpers\Hash;
use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\Applications;
use xyz\ooopener\models\AuthorizationCodes;
use xyz\ooopener\models\Sessions;
use xyz\ooopener\things\TokenPayload;
use Firebase\JWT\JWT;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\Controller ;
use xyz\ooopener\helpers\token\GrantType ;
use xyz\ooopener\things\Session ;

use Psr\Container\ContainerInterface;

use DateTime ;
use DateTimeZone ;
use Exception ;

/**
 * The token controller.
 */
class TokenController extends Controller
{
    /**
     * Creates a new TokenController instance.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container );
    }

    const ARGUMENTS_TOKEN_DEFAULT =
    [
        'client_id'     => NULL ,
        'client_secret' => NULL ,
        'grant_type'    => NULL ,
        'code'          => NULL ,
        'redirect_uri'  => NULL ,
        'refresh_token' => NULL ,
        'scope'         => NULL ,
        'state'         => NULL
    ];

    /**
     * Handle token from GET and POST request
     *
     * @param Request|NULL $request The request
     * @param Response|NULL $response The response
     * @param array $args The arguments
     *
     * @return mixed
     */
    public function token( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' token' );

        [
            'client_id'     => $client_id ,
            'client_secret' => $client_secret ,
            'grant_type'    => $grant_type ,
            'code'          => $code ,
            'redirect_uri'  => $redirect_uri ,
            'refresh_token' => $refresh_token ,
            'scope'         => $scope ,
            'state'         => $state
        ]
        = array_merge( self::ARGUMENTS_TOKEN_DEFAULT , $args ) ;

        $format = $request->getAttribute('format') ;

        // $this->logger->debug( $this . ' token #1' );

        // check get params
        if( isset( $request ) )
        {
            $params = $request->getQueryParams();
            if( !$params )
            {
                $params = $request->getParsedBody();
            }

            $client_id     = isset($params['client_id'])     ? $params['client_id']     : NULL;
            $client_secret = isset($params['client_secret']) ? $params['client_secret'] : NULL;
            $grant_type    = isset($params['grant_type'])    ? $params['grant_type']    : NULL;
            $code          = isset($params['code'])          ? $params['code']          : NULL;
            $redirect_uri  = isset($params['redirect_uri'])  ? $params['redirect_uri']  : NULL;
            $refresh_token = isset($params['refresh_token']) ? $params['refresh_token'] : NULL;
            $scope         = isset($params['scope'])         ? $params['scope']         : NULL;
            $state         = isset($params['state'])         ? $params['state']         : NULL;
        }
        else
        {
            $client_id     = $args['client_id'];
            $client_secret = $args['client_secret'];
            $grant_type    = $args['grant_type'];
            $redirect_uri  = $args['redirect_uri'];
            $refresh_token = $args['refresh_token'];
            $code          = $args['code'];
            $scope         = $args['scope'];
            $state         = $args['state'];
        }

        // $this->logger->debug( $this . ' token #2' );

        //// check required parameters

        try
        {
            if( $grant_type == NULL )
            {
                throw new Exception('grant_type');
            }

            if( $client_id == NULL )
            {
                throw new Exception('client_id');
            }

            if( $client_secret == NULL )
            {
                throw new Exception('client_secret');
            }

            switch( $grant_type )
            {
                case GrantType::AUTHORIZATION_CODE :
                {
                    if( $code == NULL )
                    {
                        throw new Exception('code');
                    }
                    break;
                }

                case GrantType::CLIENT_CREDENTIALS :
                {
                    break;
                }

                case GrantType::REFRESH_TOKEN :
                {
                    if( $refresh_token == NULL )
                    {
                        throw new Exception('refresh_token');
                    }
                    break;
                }

                default:
                {
                    throw new Exception('grant_type');
                }
            }
        }
        catch( Exception $error )
        {
            return $this->error( $response ,"The request is missing a required parameter : " . $error->getMessage() , "400" );
        }

        $user_id         = NULL ;
        $user_ip_address = NULL ;

        $user_agent = $this->container
                           ->get( UserInfos::class )
                           ->getUserAgent() ;

        try
        {
            switch( $grant_type )
            {
                case GrantType::AUTHORIZATION_CODE :
                {
                    //// check application
                    $application = $this->container
                                        ->get( Applications::class )
                                        ->getByClientCredentials( $client_id , $client_secret ) ;

                    if( !$application )
                    {
                        return $this->error( $response ,"Invalid client id or client_secret" , "401" , null , 401 );
                    }

                    // valid the code
                    $authCode = $this->container
                                     ->get( AuthorizationCodes::class )
                                     ->check( $code );

                    if( !$authCode )
                    {
                        return $this->error( $response , "Invalid authorization code" , "400" ) ; // invalid authorization code
                    }

                    $client_id       = $authCode->client_id ;
                    $user_id         = $authCode->user_id ;
                    $user_ip_address = $authCode->ip ;

                    break;
                }

                case GrantType::REFRESH_TOKEN :
                {
                    //// check application
                    $application = $this->container
                                        ->get( Applications::class )
                                        ->getByClientCredentials( $client_id , $client_secret ) ;

                    if( !$application )
                    {
                        return $this->error( $response ,"Invalid client id or client_secret" , "401" , null , 401 );
                    }

                    $user_ip_address = $this->container
                                            ->get( UserInfos::class )
                                            ->getIp() ;

                    // check refresh token
                    $refresh = $this->container
                                    ->get( Sessions::class )
                                    ->checkRefresh( $client_id , $refresh_token ) ;

                    if( !$refresh )
                    {
                        return $format === 'json'
                             ? $this->error( $response , "Invalid refresh token" , "400" )
                             : $this->redirectFor(  $response , "api.logout" ) ;

                    }

                    // check ip and user agent
                    if( $refresh->ip != $user_ip_address || $refresh->agent != $user_agent )
                    {
                        // revoke token to avoid exploit
                        $this->container->get( Sessions::class )->revokeID( $refresh->_key ) ;

                        return $this->error( $response , "token revoked" , "401" , NULL , 401 ) ;
                    }

                    $user_id = $refresh->user ;

                    break;
                }

                case GrantType::CLIENT_CREDENTIALS :
                {
                    //// check application exists
                    $application = $this->container
                                        ->get( Applications::class )
                                        ->getByClientCredentials( $client_id , $client_secret ) ;

                    if( !$application )
                    {
                        return $this->error( $response ,"Invalid client id or client_secret" , "401" , null , 401 );
                    }

                    $user = $this->container
                                 ->get( 'applicationUsers' )
                                 ->getEdge( NULL , $application->_key ) ;

                    // $this->logger->debug( $this . ' token grant_type '  . json_encode( $user ) );

                    if( property_exists( $user , 'edge' ) && is_array( $user->edge ) && count( $user->edge ) == 1 )
                    {
                        $user = (object)$user->edge[0] ;
                        $user_id = $user->uuid ;
                    }
                    else
                    {
                        return $this->formatError( $response ,"500", [ $this . ' post' ]  , NULL , 500 );
                    }
                    $user_ip_address = $this->container->get( UserInfos::class )->getIp() ;
                    break;
                }
            }

            $scopeToken = null ;

            // get team/user permissions
            $user = $this->container
                         ->get( UsersController::class )
                         ->getUuid( NULL , NULL , [ 'id' => $user_id , 'skin' => 'full' ] ) ;

            // application scope asked
            if( $grant_type == GrantType::CLIENT_CREDENTIALS && $scope != null && $scope != '' && $scope != 'all' )
            {
                $appScope = explode( ',' , preg_replace( '/\s+/' , '', $scope ) ) ;

                $scopeToken = [] ;

                foreach( $appScope as $asked )
                {
                    if( array_key_exists( $asked , $user->scope ) )
                    {
                        $scopeToken[ $asked ] = $user->scope[ $asked ] ;
                    }
                }

            }
            else
            {
                $scopeToken = $user->scope ;
            }

            // generate token
            $now    = new DateTime();
            $future = new DateTime( "now +" . $this->config['token']['live_access_token'] . " seconds" , new DateTimeZone( 'UTC' ) ) ;

            $tokenUid = $this->container
                             ->get( Hash::class )
                             ->generateUUID() ;

            $payload = new TokenPayload();

            $payload->jti = $tokenUid ;
            $payload->iss = $this->config['app']['url'] ;
            $payload->aud = $client_id ;
            $payload->sub = $user_id ;
            $payload->exp = $future->getTimestamp() ;
            $payload->iat = $now->getTimeStamp() ;

            $payload->scope = json_encode( $scopeToken ) ;

            $tokenJWT = JWT::encode( $payload->jsonSerialize() , $this->config['token']['key'] , $this->config['token']['algorithm'][0] ) ;

            // save token infos
            $session = new Session();

            $session->type       = "access_token" ;
            $session->user       = $user_id ;
            $session->app        = $client_id ;
            $session->client_id  = $client_id ;
            $session->token_id   = $tokenUid ;
            $session->ip         = $user_ip_address ;
            $session->agent      = $user_agent ;
            $session->refresh    = $refresh_token ;

            $result =
            [
                "access_token" => $tokenJWT ,
                "expires_in"   => $this->config['token']['live_access_token'] ,
                "token_type"   => "Bearer" ,
                "scope"        => "all"
            ];

            switch( $grant_type )
            {
                case GrantType::AUTHORIZATION_CODE:
                {
                    $refresh_token = bin2hex( random_bytes( 16 ) ) ;

                    $session->refresh = $refresh_token ;
                    $session->expired = $future->format( "Y-m-d\TH:i:s.v\Z" ) ;

                    $this->container->get( Sessions::class )->insert( $session );

                    $result["refresh_token"] = $refresh_token;
                    break;
                }
                case GrantType::CLIENT_CREDENTIALS:
                {
                    $session->expired = $future->format( "Y-m-d\TH:i:s.v\Z" ) ;
                    $this->container->get( Sessions::class )->insert( $session );
                    break;
                }
                case GrantType::REFRESH_TOKEN:
                {
                    $session->expired = $future->format( "Y-m-d\TH:i:s.v\Z" ) ;
                    $this->container->get( Sessions::class )->updateToken( $refresh_token , $session );
                    $result["refresh_token"] = $refresh_token;
                    break;
                }
            }

            if( isset( $state ) )
            {
                $result["state"] = $state ;
            }

            return $this->jsonResponse( $response , $result );

        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
        }
    }

}


