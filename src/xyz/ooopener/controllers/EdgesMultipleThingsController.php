<?php

namespace xyz\ooopener\controllers;

use Exception ;

use Psr\Container\ContainerInterface ;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Model;
use xyz\ooopener\models\Edges;

/**
 * The edges create a relation with a specific resource and multiple things with different types.
 */
class EdgesMultipleThingsController extends Controller
{
    /**
     * Creates a new EdgesMultipleThingsController instance.
     * @param ContainerInterface $container
     * @param Collections        $owner
     * @param Edges              $edges
     * @param ?array             $things
     */
    public function __construct( ContainerInterface $container , Collections $owner , Edges $edges , ?array $things )
    {
        parent::__construct( $container );
        $this->edges  = $edges ;
        $this->owner  = $owner ;
        $this->things = $things ;
    }

    /**
     * The things definitions
     * @var ?array
     */
    public ?array $things ;

    /**
     * The edges model reference.
     * @var Edges
     */
    public Edges $edges ;

    /**
     * The owner collection model reference.
     */
    public ?Collections $owner ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'facets'  => NULL ,
        'id'      => NULL ,
        'items'   => NULL ,
        'params'  => NULL ,
        'skin'    => NULL ,
        'sort'    => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'facets' => $facets ,
            'id'     => $id ,
            'items'  => $items ,
            'params' => $params ,
            'skin'   => $skin ,
            'sort'   => $sort
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            // ----- initialize

            if( $request )
            {
                $params = isset( $params ) ? $params : $request->getQueryParams();
            }

            // ----- facets

            if( isset( $params[ 'facets' ] ) )
            {
                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = array_merge
                        (
                            json_decode( $params[ 'facets' ] , TRUE ) ,
                            isset( $facets ) ? $facets : []
                        );
                    }
                    else
                    {
                        $facets = $params['facets'] ;
                    }
                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                }
            }

            $result = [] ;

            try
            {
                $query = <<<EOD
                FOR vertex, edge IN 1..1 INBOUND @start @@edge
                RETURN { _id : vertex._id , _key : vertex._key , _type : edge.type }
                EOD;

               $binds =
               [
                   '@edge' => $this->edges->table ,
                   'start' => $this->owner->table . '/' . $id
               ] ;

                $database = $this->edges->getDatabase() ;

                $database->prepare
                ([
                    'query'    => $query ,
                    'bindVars' => $binds
                ]);

                $database->execute() ;

                $result = $database->getResult() ;

                $keys = is_array( $this->things ) ? array_keys( $this->things ) : [] ;

                if( is_countable($result) && count($result) && count($keys) > 0 )
                {
                    $things = [] ;
                    $types  = [] ;

                    foreach( $keys as $key )
                    {
                        $types[$key] = [] ;
                    }

                    foreach( $result as $item )
                    {
                        $key  = $item['_key'] ;
                        $type = $item['_type'] ;
                        if( isset( $this->things[$type] ) )
                        {
                            $set = is_array( $types[$type] ) ? $types[$type]  : [] ;
                            $set = [ ...$set , $item['_key'] ] ;
                            $types[$type] = $set ;
                        }
                    }

                    foreach( $types as $type => $keys )
                    {
                        if( is_array($keys) && count($keys) > 0 )
                        {
                            $elements = $this->things[ $type ][ Model::CONTROLLER ]->all( NULL , NULL , [ 'active' => NULL , 'facets' => [ 'ids' => implode( ',' , array_values( $keys ) ) ] ] ) ;
                            foreach( $elements as $thing )
                            {
                                $things[ $thing->id ] = $thing ;
                            }
                        }
                    }

                    $result = array_map( fn( $item ) => $things[$item['_key']] , $result ) ;
                }

                return $response ? $this->success
                       (
                           $response ,
                           $result ,
                           $this->getCurrentPath( $request , $params ) ,
                           count( $result )
                       ) : $result ;
            }
            catch( Exception $error )
            {
                $this->logger->warning( $this . ' all(' . $id . ') failed, ' . $error->getMessage() ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function count( Request $request = NULL , Response $response = NULL , array $args = [] ) :?Response
    {
        return $this->success( $response , 0 ) ; // TODO not implemented yet
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL ,
        'type'  => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] ) :Response
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;

        try
        {
            $exist = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            $type  = $this->getParam( $request , 'type' ) ;

            if( $exist && is_string( $type ) && is_array( $this->things ) && array_key_exists( $type , $this->things ) )
            {
                $edges = $this->edges;
                $model = $this->things[ $type ][ Model::MODEL      ] ;

                $from = $model->table . '/' . $id ;
                $to   = $exist->_id ;

                if( $edges->existEdge( $from , $to ) )
                {
                    $item = $edges->deleteEdge( $from , $to ) ;
                    if( $item )
                    {
                        $this->owner->updateDate( $owner ) ;
                        return $response ? $this->success( $response , (int) $id ) : $id ;
                    }
                }
            }

            return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT = [ 'id' => NULL ] ;

    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args );

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $id . ')' ) ;
        }

        try
        {
            $exist = $this->owner->exist( $id , ['fields' => '_id']);
            if ( !$exist )
            {
                return $this->formatError( $response, '404', [ 'deleteAll(' . $id . ')' ] , NULL , 404 ) ;
            }

            $count = 0 ;

            $removed = $this->edges->deleteEdgeTo( $this->owner->table . '/' . $id ) ;
            if( is_array( $removed ))
            {
                $count = count( $removed ) ;
            }

            if( $count > 0 )
            {
                $this->owner->updateDate( $id ) ;
            }

             return $response ? $this->success( $response , $count ) : $count ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'type'    => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response|object
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $thing = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$thing )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $insert = NULL ;
            $result = NULL ;

            if( $request )
            {
                $params = $request->getParsedBody() ;
                if( isset( $params['type'] ) )
                {
                    $type = $params['type'] ;
                }
            }

            if( is_string( $type ) && is_array( $this->things ) && array_key_exists( $type , $this->things ) )
            {
                $edges      = $this->edges;
                $controller = $this->things[ $type ][ Model::CONTROLLER ] ;
                $model      = $this->things[ $type ][ Model::MODEL      ] ;

                if( !$model->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $from = $model->table . '/' . $id ;
                $to   = $thing->_id ;

                if( $from == $to || $edges->existEdge( $from , $to ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                $insert = $edges->insertEdge( $from , $to , [ 'type' => $type ] ) ;

                if( $insert )
                {
                    $this->owner->updateDate( $owner ) ; // update owner
                }

                $fields = $controller->getFields() ;
                $result = $model->get( $id , [ 'queryFields' => $fields ] ) ;
                $result = $controller->create( $result ) ;
            }

            return $response
                 ? $this->success( $response , $result , $this->getCurrentPath( $request ) )
                 : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
