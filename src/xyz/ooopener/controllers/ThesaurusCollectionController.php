<?php

namespace xyz\ooopener\controllers;

use Exception ;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use xyz\ooopener\data\ThesaurusCollector;
use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\ThesaurusCollectionValidator;

class ThesaurusCollectionController extends CollectionsController
{
    /**
     * Creates a new ThesaurusCollectionController instance.
     * @param ContainerInterface $container
     * @param ?Collections       $model
     * @param ?string            $path
     */
    public function __construct(
        ContainerInterface $container ,
        ?Collections       $model = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $path ,
            new ThesaurusCollectionValidator( $container , $model , $path )
        );

        $this->collector = $this->container->get( ThesaurusCollector::class ) ;
    }

    public function initCollector()
    {
         $this->collector->build( $this->all( NULL , NULL , [ 'active' => NULL , 'limit' => 0 ] ) ) ;
    }

    public ThesaurusCollector $collector ;

    /**
     * Deletes the thesaurus definition if the user is a super administrator only.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        try
        {
            if( $this->isSuperAdmin() )
            {
                $result = parent::delete( $request , $response , $args ) ;

                $this->initCollector() ;

                return $result ;
            }
            else
            {
                [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;
                $this->logger->debug( $this . ' delete(' . $id . ')' ) ;
                return $this->formatError( $response , '401', [ 'delete() failed, the current user is not a superadmin' ] , NULL , 401 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteDependencies( $id )
    {
        $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
        if( $item )
        {
            $this->model->collectionDrop( $item->identifier ) ; // Remove the collection in the database
        }
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     * @OA\Schema(
     *     schema="Thesauri",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/status")},
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="name",description="The name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="url",description="The url of the resource"),
     *     @OA\Property(type="string",property="category",description="The category of the resource"),
     *     @OA\Property(type="string",property="identifier",description="The unique identifier of the resource"),
     *     @OA\Property(type="string",property="path",description="The unique path of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     *
     * @OA\Response(
     *     response="thesauriResponse",
     *     description="Result list of thesauri",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/Thesauri"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' =>  Thing::FILTER_BOOL          ] ,
        'id'         => [ 'filter' =>  Thing::FILTER_ID            ] ,
        'url'        => [ 'filter' =>  Thing::FILTER_THESAURUS_URL ] ,
        'name'       => [ 'filter' =>  Thing::FILTER_TRANSLATE     ] ,
        'category'   => [ 'filter' =>  Thing::FILTER_DEFAULT       ] ,
        'identifier' => [ 'filter' =>  Thing::FILTER_DEFAULT       ] ,
        'path'       => [ 'filter' =>  Thing::FILTER_DEFAULT       ] ,
        'created'    => [ 'filter' =>  Thing::FILTER_DATETIME      ] ,
        'modified'   => [ 'filter' =>  Thing::FILTER_DATETIME      ]
    ];

    public function isSuperAdmin() :bool
    {
        return $this->container->get('auth')->team->name == 'superadmin' ;
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = []) : ?Response
    {
        try
        {
            if( $response )
            {
                $this->logger->debug( $this . ' post()' ) ;
            }

            if( $this->isSuperAdmin() )
            {
                if( isset( $request ) )
                {
                    [ $conditions , $init ] = $this->prepare( $request ) ;

                    $this->validator->validate( $conditions ) ;

                    if( $this->validator->passes() )
                    {
                        $result = $this->model->insert( $init ) ;

                        $this->model->collectionCreate( $result->identifier ) ; // Creates the collection in the database

                        $this->initCollector() ; // Init the thesaurus cache in the slim application : controller / model / router

                        return $this->success
                        (
                            $response ,
                            $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] )
                        ) ;
                    }
                    else
                    {
                        return $this->getValidatorError( $response ) ;
                    }
                }
            }
            else
            {
                return $this->formatError( $response , '401', [ 'post() failed, the current user is not a superadmin' ] , NULL , 401 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function prepare( Request $request = NULL , $old = NULL ) :array
    {
        $conditions = [] ;
        $init       = [] ;

        if( $request )
        {
            $method = $request->getMethod();

            $isPost  = $method == 'POST' ;
            $isValid = $isPost || $this->isSuperAdmin() ;

            $category   = $this->getParam( $request , 'category'   ) ;
            $identifier = $this->getParam( $request , 'identifier' ) ;
            $name       = $this->getParam( $request , 'name'       ) ;
            $path       = $this->getParam( $request , 'path'       ) ;

            $oldIdentifier = isset($old) ? $old->identifier : NULL ;
            $oldPath       = isset($old) ? $old->path       : NULL ;

            if( $isPost )
            {
                $init['active'] = 1 ;
            }

            $params = $this->getParams( $request ) ;

            if( is_array( $params ) )
            {
                $init['name']     = isset( $name ) ? $this->filterLanguages( $name ) : NULL ;
                $init['category'] = isset( $category ) ? $category : NULL ;

                if( $isValid )
                {
                    $init['identifier'] = isset( $identifier ) ? $identifier : NULL ;
                    $init['path']       = isset( $path )       ? $path       : NULL ;
                }
            }

            $conditions['category'] = [ $category   , 'required|min(2)|max(255)' ] ;

            if( $isValid )
            {
                $currentPath = $this->path . '/' ;
                $len         = ( strlen( $this->path . '/' ) + 2 ) . '' ;
                $conditions['identifier'] = [ $identifier , "required|min($len)|max(255)|uniqueIdentifier($oldIdentifier)|startWithIdentifier($currentPath)" ] ;
                $conditions['path']       = [ $path       , "required|min(2)|max(255)|uniquePath($oldPath)" ] ;
            }
        }

        return [ $conditions , $init ] ;
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT = [ 'id' => NULL ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = []) : ?Response
    {
        try
        {
            [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

            if( $response )
            {
                $this->logger->debug( $this . ' put(' . $id . ')' ) ;
            }

            $old = $this->model->get( $id , [ 'fields' => '_key,identifier,path' ] ) ;
            if( !$old  )
            {
                return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
            }

            $jwt   = $this->container->get('jwt') ;
            $scope = json_decode( $jwt->scope ) ;

            if( property_exists( $scope ,$this->path ) && $scope->{ $this->path } == 'admin' )
            {
                if( isset( $request ) )
                {
                    [ $conditions , $init ] = $this->prepare( $request , $old ) ;

                    $this->validator->validate( $conditions ) ;

                    if( $this->validator->passes() )
                    {
                        $result = $this->model->update( $init , $id ) ;

                        // ------------- Manage the collection of the thesaurus in the database

                        if( $this->model->collectionExists( $old->identifier ) )
                        {
                            if( $old->identifier !== $result->identifier )
                            {
                                $this->model->collectionRename( $old->identifier , $result->identifier ) ; // Rename the collection
                            }
                        }
                        else if( !$this->model->collectionExists( $result->identifier ) )
                        {
                            $this->model->collectionCreate( $result->identifier ) ;
                        }

                        // ------------- Initialize the thesaurus cache in the Slim application

                        $this->initCollector() ;

                        return $this->success
                        (
                            $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] )
                        ) ;
                    }
                    else
                    {
                        return $this->getValidatorError( $response ) ;
                    }
                }
            }
            else
            {
                return $this->formatError( $response , '401', [ 'put() failed, the current user is not an admin with this resource' ] , NULL , 401 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
