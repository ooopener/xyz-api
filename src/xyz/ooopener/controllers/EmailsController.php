<?php

namespace xyz\ooopener\controllers;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\EmailValidator ;

/**
 * The generic emails controller.
 */
class EmailsController extends ThingsEdgesController
{
    /**
     * Creates a new EmailsController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new EmailValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Email",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",description="The alternate name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="value",description="The email value",format="email"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;

        if( isset( $params['additionalType'] )  )
        {
            $this->item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $this->item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) && $params['value'] != '' )
        {
            $this->item['value'] = $params['value'] ;
        }

        $this->conditions['value']          = [ $params['value']          , 'required|email'     ] ;
        $this->conditions['additionalType'] = [ $params['additionalType'] , 'required|emailType' ] ;

        if( isset( $params['name'] ) )
        {
            $this->item['name'] = $params['name'] ;
            $this->conditions['name'] = [ $params['name'] , 'max(70)' ] ;
        }
    }
}

/**
 * @OA\RequestBody(
 *     request="postEmail",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="value",type="string",format="email",description="The email"),
 *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
 *             required={"value","additionalType"},
 *             @OA\Property(property="name",type="string",description="The name of the resource"),
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putEmail",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="value",type="string",format="email",description="The email"),
 *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
 *             required={"value","additionalType"},
 *             @OA\Property(property="name",type="string",description="The name of the resource"),
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 */