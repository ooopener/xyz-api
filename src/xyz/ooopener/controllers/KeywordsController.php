<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use xyz\ooopener\validations\KeywordValidator ;

/**
 * The generic keywords controller.
 */
class KeywordsController extends ThingsEdgesController
{
    /**
     * Creates a new KeywordsController instance.
     *
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path ) ;
        $this->validator = new KeywordValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'href'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
    ];

    /**
     * Post new keyword
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['href'] ) )
        {
            $item['href'] = $params['href'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(50)' ] ,
            'url'  => [ $params['href'] , 'url' ]
        ] ;

        $this->conditions = $conditions ;
        $this->item       = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * Put specific keyword
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['href'] ) )
        {
            $item['href'] = $params['href'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(50)' ] ,
            'url'  => [ $params['href'] , 'url' ]
        ] ;

        $this->conditions = $conditions ;
        $this->item       = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
