<?php

namespace xyz\ooopener\controllers ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;

use Exception ;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

class MediaObjectsOrderController extends EdgesController
{
    /**
     * Creates a new MediaObjectsOrderController instance.
     * @param ContainerInterface $container
     * @param ?Model             $model
     * @param ?Collections       $owner
     * @param ?Edges             $edge
     * @param ?array             $conditions
     * @param ?string            $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        Model              $model      = NULL ,
        Collections        $owner      = NULL ,
        Edges              $edge       = NULL ,
        array              $conditions = NULL ,
        string             $path       = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->conditions = $conditions ;
    }

    // --------------------------------

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'         => $id ,
            'limit'      => $limit ,
            'offset'     => $offset ,
            'params'     => $params
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        $api = $this->config['api'] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- limit

            $limit = intval( isset( $limit ) ? $limit : $api[ 'limit_default' ] );
            if( isset( $params[ 'limit' ] ) )
            {
                $limit             = filter_var
                (
                    $params[ 'limit' ] ,
                    FILTER_VALIDATE_INT ,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api[ 'minlimit' ] ) ,
                            "max_range" => intval( $api[ 'maxlimit' ] )
                        ]
                    ]
                );
                $params[ 'limit' ] = intval( ( $limit !== FALSE ) ? $limit : $api[ 'limit_default' ] );
            }

            // ----- offset

            $offset = intval( isset( $offset ) ? $offset : $api[ 'offset_default' ] );
            if( isset( $params[ 'offset' ] ) )
            {
                $offset = filter_var
                (
                    $params[ 'offset' ] ,
                    FILTER_VALIDATE_INT ,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api[ 'minlimit' ] ) ,
                            "max_range" => intval( $api[ 'maxlimit' ] )
                        ]
                    ]
                );
                $params[ 'offset' ] = intval( ( $offset !== FALSE ) ? $offset : $api[ 'offset_default' ] );
            }
        }

        $joinFields = $this->container->get( $this->edge->from['controller'] )->getFields( 'extend' ) ;

        $result = $this->owner->getArrayJoinAll
        (
            $id ,
            $this->model->table,
            $joinFields ,
            [
                'field'  => $this->path,
                'limit'  => $limit,
                'offset' => $offset
            ]
        ) ;

        $items = $result->{ $this->path } ;

        if( $response )
        {
            return $this->success
            (
                $response ,
                $items,
                $this->getCurrentPath( $request , $params ) ,
                is_array($items) ? count($items) : NULL,
                [
                    'limit'  => $limit,
                    'offset' => $offset,
                    'total'  => $result->{ 'num' . ucfirst($this->path) }
                ]
            ) ;
        }

        return $items ;
    }

    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' count(' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $id , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'count(' . $id . ')' ] , NULL , 404 );
            }

            $count = count( $o->{ $this->path } ) ;

            if( $response )
            {
                return $this->success( $response , $count , $this->getCurrentPath( $request ) ) ;
            }

            return $count ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'count(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return object|Response|null
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;

            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $this->model->table . '/' . $id ;
            $idTo   = $this->owner->table . '/' . $owner;

            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                $this->edge->deleteEdge( $idFrom , $idTo ) ; // delete edge
            }

            $result = $this->deleteInOwner( $id , $o ) ;

            if( $response )
            {
                return $this->success( $response , (int) $id );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'id'   => NULL ,
        'list' => NULL
    ] ;

    public function deleteAll( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [
            'id'   => $id ,
            'list' => $list
        ]
        = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ;

        if( $request )
        {
            $params = $request->getParsedBody();
            if( isset($params) && isset( $params['list'] ) )
            {
                $list = $params['list'] ;
            }
        }

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $id . ') list: ' . $list ) ;
        }

        try
        {
            $exist = $this->owner->exist( $id ) ;
            if( $exist )
            {
                if( is_string( $list ) )
                {
                    if( $list = '*' )
                    {
                        $to = $this->edge->to[ Model::NAME ] . '/' . $id ;

                        // $this->logger->info( $this . '::' . __FUNCTION__ . ' to:'   . $to ) ;

                        $result = $this->edge->deleteEdgeTo( $to ) ;

                        if( is_array( $result ) )
                        {
                            $result = array_map( fn( $item ) => $item['_from']  , $result) ;
                        }

                        if( $response )
                        {
                            return $this->success( $response , $result ) ;
                        }

                        return $list ;
                    }

                    $count = 0 ;
                    $items = [] ;

                    if( $list != '' )
                    {
                        $items = explode( ',' , $list ) ;
                        $items = array_unique( $items ) ; // remove duplicate value
                        $count = $this->container
                                      ->get( $this->edge->from[ Model::CONTROLLER ] )
                                      ->model
                                      ->existAll( $items , [ 'conditions' => $this->conditions ] ) ;
                    }

                    if( $count == count( $items ) )
                    {
                        foreach( $items as $item )
                        {
                            $this->delete( NULL , NULL , [ 'id' => $item , 'owner' => $id ] ) ;
                        }

                        if( $response )
                        {
                            return $this->success( $response , $list ) ;
                        }

                        return $list ;
                    }
                    else
                    {
                        return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                    }
                }
                else
                {
                    return $this->formatError( $response , '400' , [ 'deleteAll(' . $id . ')' , 'the parameter list is needed' ] );
                }
            }
            else
            {
                return $this->formatError( $response , '404' , [ 'deleteAll(' . $id . ')' ] , NULL , 404 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

   /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'     => NULL ,
        'owner'  => NULL ,
        'params' => NULL ,
        'skin'   => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return object|Response|null
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'     => $id ,
            'owner'  => $owner ,
            'params' => $params ,
            'skin'   => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;

            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $this->model->table . '/' . $id ;
            $idTo   = $this->owner->table . '/' . $owner;

            $result = NULL ;

            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                $controller = $this->container->get( $this->edge->from['controller' ] ) ;
                $fields     = $controller->getFields() ;
                $result     = $controller->create( $this->model->get( $id , [ 'queryFields' => $fields ] ) ) ;
            }

            return $response ? $this->success( $response , $result , $this->getCurrentPath( $request , $params ))
                             : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch position' methods options.
     */
    const ARGUMENTS_PATCH_POSITION_DEFAULT =
    [
        'id'       => NULL ,
        'owner'    => NULL ,
        'position' => NULL
    ] ;

    public function patchPosition( Request $request = NULL , Response $response = NULL , array $args = [] ) :?Response
    {
        [
            'id'       => $id ,
            'owner'    => $owner ,
            'position' => $position
        ]
        = array_merge( self::ARGUMENTS_PATCH_POSITION_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            // check
            if( in_array( (int)$id , $o->{$this->path} , TRUE ) === FALSE )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            $length = count( $o->{$this->path} ) ;


            $newArray = array_diff( $o->{$this->path} , [ (int)$id ] ) ; // remove id in array

            // check position
            if( $position < 0 )
            {
                $position = 0 ;
            }
            else if( $position > $length - 1 )
            {
                $position = $length - 1 ;
            }

            // put id in the new position
            array_splice( $newArray , $position , 0 , (int)$id ) ;

            // update
            $this->owner->update( [ $this->path => $newArray ] , $owner ) ;

            $items = $this->all( NULL , NULL , [ 'id' => $owner , 'limit' => 1 , 'offset' => (int) $position ] ) ;

            return $this->success
            (
                $response ,
                $items[0] ,
                '' ,
                NULL ,
                [
                    'position' => $position ,
                    'total'    => $length
                ]
            ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT = [ 'id' => NULL ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $id , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $params['list'] ) )
            {
                $list = $params['list'] ;

                if( $list != "" )
                {
                    $items = explode( ',' , $list ) ;
                    $items = array_unique( $items ) ;
                    $count = $this->container
                                  ->get( $this->edge->from['controller'] )
                                  ->model
                                  ->existAll( $items , [ 'conditions' => $this->conditions ] ) ;
                }
                else
                {
                    $items = [] ;
                    $count = 0 ;
                }

                if( $count == count( $items ) )
                {
                    // get current edges of 'to'
                    $currentEdges = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $currentEdges = $currentEdges->edge ;

                    $currents = [] ;

                    foreach( $currentEdges as $edge )
                    {
                        array_push( $currents , (string) $edge['id'] ) ;
                    }

                    // compare lists (currents and items)
                    $addItems = array_diff( $items , $currents ) ;

                    ///////  ADD edges
                    $idTo = $this->edge->to['name'] . '/' . $id ;
                    foreach( $addItems as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;
                        $exist  = $this->edge->existEdge( $idFrom , $idTo ) ;
                        if( !$exist )
                        {
                            $this->edge->insertEdge( $idFrom , $idTo ) ; // save to edge
                        }
                    }

                    $position = NULL ;

                    if( isset( $params['position'] ) )
                    {
                        $position = (int) $params['position'] ;
                        if( $position < 0 )
                        {
                            $position = 0 ;
                        }
                    }

                    $insert = $this->insertInOwner( $items , $o , $position ) ;

                    if( $insert == NULL )
                    {
                        return $this->formatError( $response , '500', [ 'post(' . $id . ')' ] , NULL , 500 );
                    }

                    $position = $insert['position'] ;
                    $total    = $insert['total'] ;

                    $result = $this->all( NULL , NULL , [ 'id' => $id , 'limit' => $count , 'offset' => $position ] ) ;

                    // $this->logger->info( $this . ' ' . __FUNCTION__ . ' $result : ' . json_encode( $result)  ) ;

                    return $this->success
                    (
                        $response ,
                        $result ,
                        '',
                        NULL ,
                        [
                            'position' => $position,
                            'total'    => $total
                        ]
                    ) ;
                }
                else
                {
                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '400' , [ 'post(' . $id . ')' , 'the parameter list is needed' ] );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    //////////////////////////////////

    protected function deleteInOwner( $id , $owner )
    {
        $array = $owner->{$this->path} ;
        $pos = array_search( (int)$id , $array , TRUE ) ;
        if( $pos !== FALSE )
        {
            // $this->logger->debug( $this . ' pos => ' . $pos ) ;
            array_splice( $array , $pos , 1 ) ;
            // $this->logger->debug( $this . ' new array => ' . json_encode( $array ) ) ;
            // $this->logger->debug( $this . ' owner key => ' . $owner->_key ) ;
            return $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;
        }

        return null ;
    }

    protected function getOwner( $modelId , $skin = NULL )
    {
        $name = $this->owner->table . 'Controller' ;
        if( $this->container->has( $name ) )
        {
            $ownerController = $this->container->get( $name ) ;
            $result = $this->owner->all
            ([
                'active'      => NULL ,
                'conditions'  => [ 'POSITION( doc.' . $this->path . ' , ' . $modelId . ' ) == true' ],
                'queryFields' => $ownerController->getFields( $skin ),
                'limit'       => 1
            ]) ;

            if( is_array( $result ) && count( $result ) == 1 )
            {
                return $result[0] ;
            }
        }
        return null ;
    }

    protected function insertInOwner( $list , $owner , $position = NULL )
    {
        $toInt = function( $value )
        {
            return (int) $value ;
        };

        $list   = array_map( $toInt , $list ) ; // make sure it is an array of integer
        $array  = $owner->{ $this->path } ; // get existing array
        $length = count( $array ) ;

        if( $position === NULL || $position > $length )
        {
            $position = $length ;
        }

        $duplicate = array_keys( array_intersect( $array , $list ) ) ; // remove duplicate but in existing array

        if( $duplicate && count( $duplicate ) > 0 )
        {
            $diff = 0 ;
            $duplicate = array_reverse( $duplicate ) ;
            foreach( $duplicate as $pos ) // remove them
            {
                array_splice( $array , $pos , 1 ) ;
                if( $pos < $position )
                {
                    $diff++ ;
                }
            }
            $position -= $diff ;
        }

        array_splice( $array , $position , 0 , $list ) ;

        $update = $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;

        if( $update )
        {
            return
            [
                'position' => $position ,
                'total'    => count( $array )
            ] ;
        }
        else
        {
            return NULL ;
        }
    }
}
