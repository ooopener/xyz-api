<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface ;

/**
 * The edges medical authorities controller.
 */
class EdgesMedicalAuthoritiesController extends Controller
{
    /**
     * Creates a new EdgesAuthoritiesController instance.
     * @param ContainerInterface $container
     * @param Collections        $owner
     * @param array|NULL         $authorities
     */
    public function __construct( ContainerInterface $container , Collections $owner , array $authorities )
    {
        parent::__construct( $container );
        $this->owner       = $owner ;
        $this->authorities = $authorities ;
    }

    /**
     * The authorities definitions
     * @var array|NULL
     */
    public ?array $authorities ;

    /**
     * The owner model reference.
     */
    public ?Collections $owner ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'facets'  => NULL ,
        'id'      => NULL ,
        'items'   => NULL ,
        'params'  => NULL ,
        'skin'    => NULL ,
        'sort'    => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'facets' => $facets ,
            'id'     => $id ,
            'items'  => $items ,
            'params' => $params ,
            'skin'   => $skin ,
            'sort'   => $sort
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            // ----- initialize

            if( $request )
            {
                $params = isset( $params ) ? $params : $request->getQueryParams();
            }

            // ----- facets

            if( isset( $params[ 'facets' ] ) )
            {
                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = array_merge
                        (
                            json_decode( $params[ 'facets' ] , TRUE ) ,
                            isset( $facets ) ? $facets : []
                        );
                    }
                    else
                    {
                        $facets = $params['facets'] ;
                    }
                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                }
            }

            // ----- authorities

            $items = [] ;

            if( is_array( $this->authorities ) && count( $this->authorities ) > 0 )
            {
                $authorities = [] ;

                if( is_array($facets) && array_key_exists( 'authorities' , $facets ) )
                {
                    $filters = $facets['authorities'] ;

                    if( is_string($filters) )
                    {
                        $filters = explode( ',' , $filters ) ;
                    }

                    if( is_array( $filters ) )
                    {
                        $keys = array_keys( $this->authorities ) ;

                        $filters = array_filter( $filters , fn( $value ) => in_array( $value , $keys ) ) ;

                        $authorities = array_filter( $this->authorities, fn( $key ) => in_array( $key , $filters ) , ARRAY_FILTER_USE_KEY ) ;

                        if( count( $authorities ) > 0 )
                        {
                            $tmp = [] ;
                            foreach ( $filters as $value )
                            {
                                $tmp[ $value ] = $authorities[$value] ;
                            }
                            $authorities = $tmp ;
                        }
                    }
                }
                else
                {
                    $authorities = $this->authorities ;
                }

                $authorities = array_values( $authorities ) ;
                foreach ( $authorities as $edge )
                {
                    $result = $this->allAuthority( $id , $edge , $skin ) ;
                    if( count($result) > 0 )
                    {
                        $items = array_merge( $items , $result ) ;
                    }
                }
            }

            if( $response )
            {
                return $this->success
                (
                    $response ,
                    $items ,
                    $this->getCurrentPath( $request , $params ) ,
                    count( $items )
                ) ;
            }
            else
            {
                return $items ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function count( Request $request = NULL , Response $response = NULL , array $args = [] ) :?Response
    {
        return $this->success( $response , 0 ) ; // TODO not implemented yet
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE ,
        'type'    => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] ) :Response
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;

        try
        {
            if( $request )
            {
                $params = $request->getParsedBody() ;

                if( isset( $params['type'] ) )
                {
                    $type = $params['type'] ;
                }
                else
                {
                    $params = $request->getQueryParams() ;
                    if( isset( $params['type'] ) )
                    {
                        $type = $params['type'] ;
                    }
                }
            }

            $exist = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;

            if( $exist && is_string( $type ) && is_array( $this->authorities ) && array_key_exists( $type , $this->authorities ) )
            {
                $edge       = $this->container->get( $this->authorities[ $type ] ) ;
                $controller = $this->container->get( $edge->from[Edges::CONTROLLER] ) ;
                $model      = $controller->model ;

                $idFrom = $model->table . '/' . $id ;
                $idTo   = $exist->_id ;

                if( $edge->existEdge( $idFrom , $idTo ) )
                {
                    $item = $edge->deleteEdge( $idFrom , $idTo ) ;
                    if( $item )
                    {
                        $this->owner->updateDate( $owner ) ;
                        return $response ? $this->success( $response , (int) $id ) : $id ;
                    }
                }
            }

            return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'id'      => NULL ,
        'reverse' => FALSE
    ] ;

    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id,
            'reverse' => $reverse
        ]
        = array_merge(self::ARGUMENTS_DELETE_ALL_DEFAULT, $args);

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $id . ')' ) ;
        }

        try
        {
            $exist = $this->owner->get( $id , ['fields' => '_id']);

            if (! $exist )
            {
                return $this->formatError( $response, '404', [ 'deleteAll(' . $id . ')' ] , NULL , 404 ) ;
            }

            $count = 0 ;

            if( is_array( $this->authorities ) && ( count( $this->authorities ) > 0 ) )
            {
                foreach( $this->authorities as $type )
                {
                    $edge       = $this->container->get( $type ) ;
                    $controller = $this->container->get( $edge->to[Edges::CONTROLLER] ) ;
                    $model      = $controller->model ;
                    $table      = $model->table ;
                    $removed    = $edge->deleteEdgeTo( $table . '/' . $id ) ;
                    if( is_array($removed))
                    {
                        $count += count( $removed ) ;
                    }
                }
            }

            if( $count > 0 )
            {
                $this->owner->updateDate( $id ) ;
            }

             return $response ? $this->success( $response , $count ) : $count ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE ,
        'type'    => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $insert = NULL ;
            $result = NULL ;

            if( $request )
            {
                $params = $request->getParsedBody() ;
                if( isset( $params['type'] ) )
                {
                    $type = $params['type'] ;
                }
            }

            if( is_string( $type ) && is_array( $this->authorities ) && array_key_exists( $type , $this->authorities ) )
            {
                $edge       = $this->container->get( $this->authorities[ $type ] ) ;
                $controller = $this->container->get( $edge->from[Edges::CONTROLLER] ) ;
                $model      = $controller->model ;

                if( $this->owner->table == $model->table && ( $owner == $id ) ) // ???? important or not
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                if( !$model->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $idFrom = $model->table . '/' . $id ;
                $idTo   = $o->_id ;

                if( $edge->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                $insert = $edge->insertEdge( $idFrom , $idTo ) ;
                if( $insert )
                {
                    $this->owner->updateDate( $owner ) ; // update owner
                }

                $fields = $controller->getFields() ;
                $result = $model->get( $id , [ 'queryFields' => $fields ] ) ;
                $result = $controller->create( $result ) ;
            }

            if( $response )
            {
                return $this->success
                (
                    $response ,
                    $result ,
                    $this->getCurrentPath( $request )
                );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    protected function allAuthority( $id , string $edge , $skin = NULL ) :array
    {
        $result = [] ;

        try
        {
            $edge        = $this->container->get( $edge  ) ;
            $controller  = $this->container->get( $edge->from[ Edges::CONTROLLER ] ) ;

            $authorities = $edge->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;

            if( $authorities && $authorities->edge && is_array( $authorities->edge ) && count( $authorities->edge ) > 0 )
            {
                $authorities = array_values( $authorities->edge ) ;
                foreach( $authorities as $value )
                {
                    $result[] = $controller->create( (object) $value ) ;
                }
            }

        }
        catch( Exception $error )
        {
            $this->logger->warning( $this . ' getAuthorities(' . $id . ') failed, ' . $error->getMessage() ) ;
        }

        return $result ;
    }
}
