<?php

namespace xyz\ooopener\controllers\medicalLaboratories ;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\livestocks\LivestocksController;
use xyz\ooopener\controllers\organizations\OrganizationsController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;

use xyz\ooopener\things\MedicalLaboratory;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\MedicalLaboratoryValidator;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface ;

class MedicalLaboratoriesController extends CollectionsController
{
    /**
     * Creates a new MedicalLaboratoriesController instance.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path , new MedicalLaboratoryValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,
        'organization'  => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'livestocks'    => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ]  ]
    ];

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case MedicalLaboratory::FILTER_LIVESTOCKS :
                    {
                        if(
                               property_exists( $init , MedicalLaboratory::FILTER_LIVESTOCKS )
                            &&        is_array( $init->{ MedicalLaboratory::FILTER_LIVESTOCKS } )
                            &&           count( $init->{ MedicalLaboratory::FILTER_LIVESTOCKS } ) > 0
                        )
                        {
                            $sub = [] ;
                            foreach( $init->{ MedicalLaboratory::FILTER_LIVESTOCKS } as $item )
                            {
                                array_push( $sub , $this->container
                                                        ->get( LivestocksController::class )
                                                        ->create( (object) $item ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case MedicalLaboratory::FILTER_ORGANIZATION :
                    {
                        $organizationController = $this->container->get( OrganizationsController::class ) ;
                        if( property_exists( $init , MedicalLaboratory::FILTER_ORGANIZATION ) && $init->{ MedicalLaboratory::FILTER_ORGANIZATION } != NULL )
                        {
                            $init->{ $key } = $organizationController->create( (object) $init->{ MedicalLaboratory::FILTER_ORGANIZATION } ) ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    public function deleteDependencies( $id )
    {
        // TODO $this->deleteInEdgeModel( 'medicalLaboratoriesIdentifierEdge' , $id , 'to' ) ; // identifier

        $this->deleteInEdgeModel( 'livestockMedicalLaboratories'   , $id        ) ; // livestock.medicalLaboratories
        $this->deleteInEdgeModel( 'medicalLaboratoryOrganizations' , $id , 'to' ) ; // medicalLaboratory.organization
        $this->deleteInEdgeModel( 'userFavorites'                  , $id        ) ; // favorites
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        $params = $request->getParsedBody() ;

        $organization = NULL ;

        $item = [];
        $item['active']     = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']       = $this->path ;

        if( isset( $params['organization'] ) )
        {
            $organization = $params['organization']  ;
        }

        $conditions =
        [
            'organization' => [ $organization , 'required|organization' ]
        ] ;

        ////// security - remove sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                $latest = $this->model->insert( $item );

                if( $organization )
                {
                    $edge   = $this->container->get('medicalLaboratoryOrganizations') ;
                    $idFrom = $edge->from['name'] . '/' . $organization ;
                    $edge->insertEdge( $idFrom , $latest->_id ) ;
                }

                if( $latest )
                {
                    return $this->success( $response , $this->model->get( $latest->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $organization = NULL;

        $item = [];

        if( isset( $params['organization'] ) )
        {
            $organization = $params['organization']  ;
        }

        $conditions =
        [
            'organization' => [ $organization   , 'required|organization' ]
        ] ;

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                if( $organization )
                {
                    $edge = $this->container->get('medicalLaboratoryOrganizations') ;

                    $idFrom = $edge->from['name'] . '/' . $organization ;

                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {

                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                        $edge->insertEdge( $idFrom , $idTo ) ; // add edge
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

}
