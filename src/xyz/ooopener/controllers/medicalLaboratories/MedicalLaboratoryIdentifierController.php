<?php

namespace xyz\ooopener\controllers\medicalLaboratories;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\MedicalLaboratoryIdentifierValidator;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;
use Psr\Container\ContainerInterface ;


/**
 * The medical laboratory identifiers controller.
 */
class MedicalLaboratoryIdentifierController extends ThingsEdgesController
{
    /**
     * Creates a new MedicalLaboratoryIdentifierController instance.
     *
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct(
        ContainerInterface $container ,
        ?Model              $model = NULL ,
        ?Collections        $owner = NULL ,
        ?Edges              $edge  = NULL ,
        ?string             $path  = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN   ]
    ];

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param ?Request $request
     * @param ?Response $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        $set = $this->config['medical-laboratories-identifier'];

        $params = $request->getParsedBody();

        $container = $this->container ;

        try
        {
            $ev = $this->owner->get( $id , [ 'fields' => 'location' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            if( isset( $params['additionalType'] ) )
            {
                $item['additionalType'] = (int)$params['additionalType'] ;
            }

            if( isset( $params['value'] ) )
            {
                $item['value'] = $params['value'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'  => [ $params['name']            , 'required|min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
                'type'  => [ $params['additionalType']  , 'int|type'                                                           ] ,
                'value' => [ $params['value']           , 'value'                                                              ]
            ];

            $validator = new MedicalLaboratoryIdentifierValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {

                // model

                $new = $this->model->insert( $item ) ;

                if( $new )
                {
                    // edge
                    $newEdge = $this->edge->insertEdge( $new->_id , $this->owner->table . '/' . $id ) ;

                    $item = $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] ) ;
                    if( $item )
                    {
                        return $this->success
                        (
                            $response,
                            $item
                        ) ;
                    }
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $id . ')' ) ;
        }

        $set = $this->config['medical-laboratories-identifier'];

        $params = $request->getParsedBody();

        $container = $this->container ;

        try
        {
            $ev = $this->owner->get( $owner , [ 'fields' => 'location' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            if( isset( $params['additionalType'] ) )
            {
                $item['additionalType'] = (int)$params['additionalType'] ;
            }

            if( isset( $params['value'] ) )
            {
                $item['value'] = $params['value'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'  => [ $params['name']           , 'required|min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
                'type'  => [ $params['additionalType'] , 'int|type'                                                           ] ,
                'value' => [ $params['value']         , 'value'                                                              ]
            ];

            $validator = new MedicalLaboratoryIdentifierValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                $update = $this->model->update
                (
                    $item ,
                    $id
                ) ;

                if( $update )
                {
                    $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
                    if( $item )
                    {
                        return $this->success
                        (
                            $response,
                            $item
                        ) ;
                    }
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
