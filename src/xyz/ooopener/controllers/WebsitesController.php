<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\WebsiteValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

/**
 * The websites controller.
 */
class WebsitesController extends ThingsEdgesController
{
    /**
     * Creates a new WebsitesController instance.
     *
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new WebsiteValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Website",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="href",format="uri",description="The href value",format="uri"),
     *     @OA\Property(type="integer",property="order"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'href'           => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'order'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_JOIN      ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;

        if( isset( $params['alternateName'] ) )
        {
            $this->item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['order'] ) )
        {
            $this->item['order'] = $params['order'] ;
        }

        if( isset( $params['href'] ) )
        {
            $this->item['href'] = $params['href'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $this->item['additionalType'] = (int) $params['additionalType'] ;
        }

        $this->conditions['additionalType'] = [ $params['additionalType'] , 'required|websiteType' ] ;
        $this->conditions['url']            = [ $params['href']           , 'required|url'         ] ;

        if( isset( $params['name'] ) )
        {
            $this->item['name'] = $params['name'] ;
            $this->conditions['name'] = [ $params['name'] , 'min(2)|max(50)' ] ;
        }
    }
}

/**
 * @OA\RequestBody(
 *     request="postWebsite",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="href",type="string",format="uri",description="The url of the website"),
 *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
 *             required={"href","additionalType"},
 *             @OA\Property(property="name",type="string",description="The name of the resource"),
 *             @OA\Property(property="order",type="integer",description=""),
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putWebsite",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="href",type="string",format="uri",description="The url of the website"),
 *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
 *             required={"href","additionalType"},
 *             @OA\Property(property="name",type="string",description="The name of the resource"),
 *             @OA\Property(property="order",type="integer",description=""),
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *         )
 *     ),
 *     required=true
 * )
 */
