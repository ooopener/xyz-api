<?php

namespace xyz\ooopener\controllers ;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

use Exception ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\validations\Validation;

/**
 * Class DiscoverController
 */
class DiscoverController extends CollectionsController
{
    public function __construct
    (
        ContainerInterface    $container ,
        Collections           $model     = NULL ,
        string                $path      = NULL ,
        string                $owner     = NULL ,
        Validation            $validator = NULL
    )
    {
        parent::__construct( $container , $model , $path , $validator );
        $this->owner = $container->get( $owner ) ;
    }

    public ?CollectionsController $owner ;

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT = [ 'id' => NULL ] ;

    /**
     * Deletes the specific url resource in the discover collection of the specific thing (id).
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed|Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $id . ')' );
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 ) ;
            }

            $url = $this->getParam( $request , 'url' ) ;

            $conditions =
            [
                'url' => [ $url , 'required|url' ]
            ] ;

            $this->validator->validate( $conditions ) ;
            if( $this->validator->passes() )
            {
                $item = $this->model->get( $id ) ;

                if(
                    !$item || !property_exists( $item , $this->path ) ||
                    !is_array( $item->{ $this->path } )
                )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $id . ') | The discover array collection not exist' ] , NULL , 404) ;
                }

                $path  = substr( $url , strlen( $this->config['app']['url'] ) ) ;
                $array = $item->{ $this->path } ;

                $pos = array_search( $path , $array , TRUE ) ;

                if( $pos === FALSE )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $id . ') | The resource ' . $path . ' is not register in the discover collection' ] , NULL , 404) ;
                }

                array_splice( $array , $pos , 1 ) ;

                $this->model->update( [ $this->path => $array ] , $id ) ;

                $args['active'] = NULL ;
                $args['skin'  ] = $this->path ;

                $result = $this->owner->get( NULL , NULL , $args ) ;

                $result = ( $result && property_exists( $result , $this->path ) ) ? $result->{ $this->path } : [] ;

                if( isset( $result ) )
                {
                    return $this->success
                    (
                        $response ,
                        $result,
                        $this->getCurrentPath( $request ) ,
                        is_array( $result ) ? count( $result ) : NULL
                    ) ;
                }
                else
                {
                    return $this->formatError( $response , '500' , [ 'delete(' . $id . ')' ] , NULL , 500 );
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' );
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 ) ;
            }

            $args['active'] = NULL ;
            $args['skin'  ] = $this->path ;

            $thing = $this->owner->get( NULL , NULL , $args ) ;

            if( $thing && property_exists( $thing , $this->path ) )
            {
                $thing = $thing->{ $this->path } ;
            }
            else
            {
                $thing = [] ;
            }

            if( $response )
            {
                return $this->success
                (
                    $response ,
                    $thing ,
                    $this->getCurrentPath( $request ) ,
                    is_array($thing) ? count( $thing ) : NULL
                ) ;
            }

            return $thing ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postCourseDiscover",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url",type="string",description="The url of the resource"),
     *             required={"url"},
     *             @OA\Property(property="position",type="integer",description="The position of course discover"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' );
        }

        try
        {
            if ( !$this->model->exist($id) )
            {
                return $this->formatError($response, '404', ['get(' . $id . ')'], NULL, 404) ;
            }

            $url      = $this->getParam( $request , 'url' ) ;
            $position = $this->getParam( $request , 'position' ) ;

            $conditions = [ 'url' => [ $url , 'required|url' ] ] ;

            if( isset( $position ) )
            {
                $position = (int) $position ;
                $conditions['position'] = [ $position , 'int|min(0)'   ] ;
            }

            $this->validator->validate( $conditions ) ;
            if( $this->validator->passes() )
            {
                $item  = $this->model->get( $id ) ;
                $array = ( property_exists( $item , $this->path ) && is_array( $item->{ $this->path } ) )
                       ? $item->{ $this->path }
                       : [] ; // create the array if not exist !

                $urlPath = substr( $url , strlen( $this->config['app']['url'] ) ) ;

                if( in_array( $urlPath , $array ) )
                {
                    return $this->formatError
                    (
                        $response ,
                        '409' ,
                        [ 'post(' . $id . ')' ] ,
                        [ 'errors' => [ 'discover' => 'The resource already exist with the url: ' . $urlPath  ] ] ,
                        409
                    ) ;
                }

                $length = count( $array ) ;

                if( $position === NULL || $position > $length )
                {
                    $position = $length ;
                }

                array_splice( $array , $position , 0 , $urlPath ) ;

                $this->model->update( [ $this->path => $array ] , $id ) ;

                $args['active'] = NULL ;
                $args['skin'  ] = $this->path ;

                $result = $this->owner->get( NULL , NULL , $args ) ;

                $result = ( $result && property_exists( $result , $this->path ) )
                        ? $result->{ $this->path }
                        : [] ;

                if( $result )
                {
                    return $this->success
                    (
                        $response ,
                        $result,
                        $this->getCurrentPath( $request ) ,
                        is_array($result) ? count( $result ) : NULL
                    ) ;
                }
                else
                {
                    return $this->formatError( $response , '500' , [ 'post(' . $id . ')' ] , NULL , 500 );
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Patch the position value of the specific ressource.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchCourseDiscover",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url",type="string",description="The url of the resource"),
     *             @OA\Property(property="position",type="integer",description="The position of course discover"),
     *             required={"url","position"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' );
        }

        try
        {
            if ( !$this->model->exist($id) )
            {
                return $this->formatError($response, '404', ['get(' . $id . ')'], NULL, 404) ;
            }

            $url      = $this->getParam( $request , 'url' ) ;
            $position = $this->getParam( $request , 'position' ) ;

            $conditions =
            [
                'url'      => [ $url      , 'required|url' ] ,
                'position' => [ $position , 'int|min(0)'   ]
            ] ;

            $this->validator->validate( $conditions ) ;
            if( $this->validator->passes() )
            {
                $item  = $this->model->get( $id ) ;

                if( property_exists( $item , $this->path ) && is_array( $item->{ $this->path } ) )
                {
                    $array  = $item->{ $this->path } ;
                    $length = count( $array ) ;

                    if( $length === 0 )
                    {
                        return $this->formatError
                        (
                            $response ,
                            '409' ,
                            [ 'patch(' . $id . ')' ] ,
                            [ 'errors' => [ 'discover' => 'The discover collection is empty.' ] ]  ,
                            409
                        ) ;
                    }

                    $urlPath = substr( $url , strlen( $this->config['app']['url'] ) ) ;
                    if( !in_array( $urlPath , $array ) )
                    {
                        return $this->formatError
                        (
                            $response ,
                            '409' ,
                            [ 'patch(' . $id . ')' ] ,
                            [ 'errors' => [ 'discover' => 'The resource already exist with the url: ' . $urlPath  ] ] ,
                            409
                        ) ;
                    }

                    $array = array_diff( $array , [ $urlPath ] ) ; // remove ref

                    if( $position < 0 )
                    {
                        $position = 0 ;
                    }
                    else if( $position > $length - 1 )
                    {
                        $position = $length - 1 ;
                    }

                    array_splice( $array , $position , 0 , $urlPath ) ; // put ref in the new position

                    $this->model->update( [ $this->path => $array ] , $id ) ;

                    $args['active'] = NULL ;
                    $args['skin'  ] = $this->path ;

                    $result = $this->owner->get( NULL , NULL , $args ) ;

                    if( $result && property_exists( $result , $this->path ) )
                    {
                        $result = $result->{ $this->path } ;
                    }
                    else
                    {
                        $result = [] ;
                    }

                    if( $result )
                    {
                        return $this->success
                        (
                            $response ,
                            $result,
                            $this->getCurrentPath( $request ) ,
                            is_array($result) ? count( $result ) : NULL
                        ) ;
                    }
                    else
                    {
                        return $this->formatError( $response , '500' , [ 'patch(' . $id . ')' ] , NULL , 500 );
                    }
                }
                else
                {
                    return $this->formatError
                    (
                        $response ,
                        '409' ,
                        [ 'patch(' . $id . ')' ] ,
                        [ 'errors' => [ 'discover' => 'The discover collection not exist.' ] ]  ,
                        409
                    ) ;
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
