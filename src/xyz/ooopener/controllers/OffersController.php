<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\OfferValidator;

use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Container\ContainerInterface ;

/**
 * The offers controller.
 * @example ../places/165/offers?prettyprint=true&category=full
 */
class OffersController extends ThingsEdgesController
{
    /**
     * Creates a new OffersController instance.
     *
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        Model              $model = NULL ,
        Collections        $owner = NULL ,
        Edges              $edge = NULL ,
        ?string            $path = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path , new OfferValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Offer",
     *     description="An offer to transfer some rights to an item or to provide a service",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",ref="#/components/schemas/text"),
     *     @OA\Property(property="category",description="The category of the offer",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="number",property="price",description="The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes"),
     *     @OA\Property(type="string",property="priceCurrency",description="The currency of the price, or a price component when attached to PriceSpecification and its subtypes.Use standard formats: ISO 4217 currency format"),
     *     @OA\Property(type="string",property="validFrom",format="date-time",description="The date when the item becomes valid"),
     *     @OA\Property(type="string",property="validThrough",format="date-time",description="The date after when the item is not valid"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' => Thing::FILTER_ID        ] ,
        'price'         => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'priceCurrency' => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'validFrom'     => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'validThrough'  => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'category'      => [ 'filter' => Thing::FILTER_JOIN      ]
    ];


    public function prepare( Request $request = NULL , $params = NULL )
    {
        $set = $this->config['offer'] ;

        $this->item = [ 'priceCurrency' => "EUR" ];

        $alternateName = $this->getBodyParam( $request , 'alternateName' );
        $category      = $this->getBodyParam( $request , 'category'      );
        $description   = $this->getBodyParam( $request , 'description'   );
        $price         = $this->getBodyParam( $request , 'price'         );
        $validFrom     = $this->getBodyParam( $request , 'validFrom'     );
        $validThrough  = $this->getBodyParam( $request , 'validThrough'  );

        if( isset( $category ) )
        {
            $category = $this->item['category'] = (int) $category ;
        }

        if( isset( $price ) )
        {
            $price = $this->item['price'] = ( $price != '' ) ? (float) $price : 0 ;
        }

        $this->conditions['category'] = [ $category , 'required|category' ] ;
        $this->conditions['price']    = [ $price    , 'price'             ] ;

        if( isset( $alternateName ) )
        {
            $this->item['alternateName'] = $this->filterLanguages( $alternateName ) ;
        }

        if( isset( $description ) )
        {
             $this->item['description'] = $this->filterLanguages( $description ) ;
        }
        // ----------

        if( isset( $validFrom ) && $validFrom != '' )
        {
            $this->item['validFrom'] = $validFrom ;
            $this->conditions['validFrom'] = [ $validFrom , 'date' ] ;
        }

        if( isset( $validThrough ) && $validThrough != '' )
        {
            $this->item['validThrough'] = $validThrough ;
            $this->conditions['validThrough'] = [ $validThrough , 'date' ] ;
        }

        if( isset( $validFrom ) && isset( $validThrough ) )
        {
            $validPeriod = (boolean) (strtotime($validFrom) <= strtotime($validThrough)) ;
            $this->conditions['validPeriod'] = [ $validPeriod , 'true' ] ;
        }
    }
}


/**
 * @OA\RequestBody(
 *     request="postOffer",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="category",type="integer",description="The id of the category"),
 *             required={"name","additionalType"},
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(property="description",ref="#/components/schemas/text"),
 *             @OA\Property(property="price",type="string",description="The price of the offer"),
 *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
 *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putOffer",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="category",type="integer",description="The id of the category"),
 *             required={"name","additionalType"},
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(property="description",ref="#/components/schemas/text"),
 *             @OA\Property(property="price",type="string",description="The price of the offer"),
 *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
 *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
 *         )
 *     ),
 *     required=true
 * )
 */