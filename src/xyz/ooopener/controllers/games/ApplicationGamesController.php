<?php

namespace xyz\ooopener\controllers\games;

use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Applications;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\GameValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\CollectionsController;

use xyz\ooopener\models\Games;
use Psr\Container\ContainerInterface;

use Exception ;

class ApplicationGamesController extends CollectionsController
{
    /**
     * Creates a new ApplicationGamesController instance.
     * @param ContainerInterface $container
     * @param Games|NULL $model
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        Games              $model ,
        string             $path = 'games'
    )
    {
        parent::__construct( $container , $model , $path , new GameValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'additionalType'  => [ 'filter' => Thing::FILTER_JOIN  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,

        'item'  => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' , 'normal' ] ] ,
        'quest' => [ 'filter' => Thing::FILTER_EDGE    , 'skins' => [ 'full' ] ]
    ];

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $gameCourses = $this->container
                            ->get( CourseGamesController::class )
                            ->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        $this->updateApplications( $id ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        $params = $request->getParsedBody();

        $item = [];
        $conditions = [] ;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
            $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType'  ] ;
        }

        $this->validator->validate( $conditions ) ;
        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    $this->updateApplications( $id ) ;
                    return $this->success
                    (
                        $response ,
                        $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] )
                    );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        $params = $request->getParsedBody();

        $item = [];

        $item['active']     = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']       = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $additionalType = $this
                        ->container
                        ->get('gamesTypes')
                        ->get( 'application' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;

        if( $additionalType )
        {
            $item['additionalType'] = (int) $additionalType->_key ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|max(70)' ]
        ] ;


        $this->validator->validate( $conditions ) ;
        if( $this->validator->passes() )
        {
            try
            {
                $result = $this->model->insert( $item );
                if( $result )
                {
                    return $this->success
                    (
                        $response ,
                        $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ])
                    );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    public function update( $id )
    {
        $this->model->updateDate( $id ) ;
        $this->updateApplications( $id ) ;
    }

    private function updateApplications( $id )
    {
        if( $this->container->has( Applications::class ) )
        {
            $this->container->get( Applications::class )->updateDate( $id , [ 'key' => 'setting.game' ] );
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the Application identifier is not registered in the DI container.');
        }
    }
}
