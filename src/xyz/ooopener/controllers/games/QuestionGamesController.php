<?php

namespace xyz\ooopener\controllers\games;

use xyz\ooopener\controllers\Controller;
use xyz\ooopener\controllers\ThingsEdgesController;

use xyz\ooopener\helpers\Status;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;

use xyz\ooopener\validations\GameValidator;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface;

use Exception ;

class QuestionGamesController extends ThingsEdgesController
{
    /**
     * Creates a new QuestionGamesController instance.
     * @param ContainerInterface $container
     * @param ?Model             $model
     * @param ?Collections       $owner
     * @param ?Edges             $edge
     * @param ?Controller        $controller
     * @param ?string            $path
     */
    public function __construct
    (
        ContainerInterface $container  ,
        ?Model             $model      = NULL ,
        ?Collections       $owner      = NULL ,
        ?Edges             $edge       = NULL ,
        ?Controller        $controller = NULL ,
        ?string            $path       = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $owner ,
            $edge ,
            $path ,
            new GameValidator( $container )
        );
        $this->controller = $controller ;
    }

    public ?Controller $controller ;

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'about'          => [ 'filter' => Thing::FILTER_JOIN      ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_JOIN      ] ,
        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'    => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'notes'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,
        'text'           => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,

        'item'  => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' , 'normal' ] ] ,
        'quest' => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ]
    ];

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $edgeOwner = $this->edge->getEdge( $id ) ;
        if(
            $edgeOwner
            && property_exists( $edgeOwner , Thing::FILTER_EDGE )
            && is_array( $edgeOwner->edge )
            && count( $edgeOwner->edge ) == 1
        )
        {
            $args['owner'] = (string) $edgeOwner->edge[0]['id'] ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'   => $id,
            'skin' => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = $this->config[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $courseGame     = NULL ;
                $edgeCourseGame = $this->edge->getEdge( (string) $result->id ) ;

                if(
                    $edgeCourseGame && property_exists( $edgeCourseGame , Thing::FILTER_EDGE )
                    && is_array( $edgeCourseGame->edge )
                    && count( $edgeCourseGame->edge ) == 1
                )
                {
                    $courseGame = $edgeCourseGame->edge[0] ;
                }

                $options[ 'object' ] = $courseGame ;

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        $params = $request->getParsedBody();

        $item = [];
        $conditions = [] ;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ]     = $params[ 'name' ];
            $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType']       = (int) $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType'  ] ;
        }

        if( isset( $params['quest'] ) )
        {
            try
            {
                $item['quest'] = json_decode( $params['quest'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ')' ] , NULL , 400 );
            }
        }


        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                if( array_key_exists( 'quest' , $item ) )
                {
                    $this->model->update( [ 'quest' => '' ] , $id );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    $owner = $this->edge->getEdge( $id );

                    if( $owner && property_exists( $owner , 'edge' ) && is_array($owner->edge) && count($owner->edge) == 1 )
                    {
                        $owner = (object) $owner->edge[0] ;
                        $this->controller->update( (string)$owner->id );
                    }

                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'full' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge(self::ARGUMENTS_POST_DEFAULT , $args) ;

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $ev = $this->owner->get($id , [ 'fields' => '_id' ]);
            if( !$ev )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [];

            $item['active']     = 1 ;
            $item['withStatus'] = Status::DRAFT ;
            $item['path']       = $this->path ;

            if( isset( $params['about'] ) )
            {
                $item['about'] = (int) $params['about'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $additionalType = $this->container
                                   ->get('gamesTypes')
                                   ->get( 'question' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;

            if( $additionalType )
            {
                $item['additionalType'] = (int) $additionalType->_key ;
            }

            $conditions =
            [
                'name'  => [ $params['name']  , 'required|max(70)' ] ,
                'about' => [ $params['about'] , 'required|step|uniqueStep(' . $id . ',' . $this->edge->table . ')'    ]
            ] ;

            $this->validator->validate( $conditions ) ;
            if( $this->validator->passes() )
            {
                $result = $this->model->insert( $item );
                if( $result )
                {
                    $this->edge->insertEdge( $result->_id , $ev->_id ) ;

                    $this->controller->update( $id ) ;

                    return $this->success
                    (
                        $response ,
                        $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ])
                    );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function update( $id )
    {
        $this->model->updateDate( $id ) ;
        $owner = $this->edge->getEdge( $id );
        if( $owner && key_exists( 'edge' , (array) $owner) && is_array($owner->edge) && count($owner->edge) == 1 )
        {
            $owner = (object) $owner->edge[0] ;
            $this->controller->update( (string) $owner->id );
        }
    }
}
