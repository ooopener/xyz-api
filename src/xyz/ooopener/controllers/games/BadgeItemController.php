<?php

namespace xyz\ooopener\controllers\games;

use xyz\ooopener\controllers\Controller;
use xyz\ooopener\models\Collections;

use xyz\ooopener\validations\BadgeItemValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface;

class BadgeItemController extends Controller
{
    /**
     * Creates a new BadgeItemController instance.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param Controller|NULL $controller
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , Controller $controller = NULL )
    {
        parent::__construct( $container , new BadgeItemValidator( $container ) );
        $this->controller = $controller ;
        $this->model      = $model ;
    }

    public ?Controller $controller ;

    /**
     * The model reference.
     */
    public ?Collections $model ;

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'  => NULL ,
        'key' => '_key'
    ] ;

    /**
     * Returns the badge item reference of the specific item reference.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return object the badge item reference of the specific item reference.
     */
    public function get( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        [
            'id'  => $id ,
            'key' => $key
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $item = NULL ;

        try
        {
            $item = $this->model->get
            (
                $id ,
                [ 'key' => $key , 'fields' => 'item' ]
            ) ;

            if( $item )
            {
                $item = $item->item ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError
            (
                $response ,
                '500' ,
                [ 'get(' . $id . ')' , $e->getMessage() ] ,
                NULL ,
                500
            );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getCurrentPath( $request )
                );
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item  ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'  => NULL ,
        'key' => '_key'
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'  => $id ,
            'key' => $key
        ]
        = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $owner = $this->model->get( $id , [ 'fields' => 'item' ] );
            if( !$owner )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            if( property_exists( $owner , 'item' ) )
            {
                $item = $owner->item ;
            }
            else
            {
                $item = [] ;
            }

            $additionalType = NULL ;
            $conditions     = [] ;

            if( isset($params[ 'name' ]) )
            {
                $item[ 'name' ] = $params[ 'name' ];
                $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
            }

            if( isset( $params['alternateName'] ) )
            {
                $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
            }

            if( isset( $params['alternativeHeadline'] ) )
            {
                $item['alternativeHeadline'] = $this->filterLanguages( $params['alternativeHeadline'] ) ;
            }

            if( isset( $params['description'] ) )
            {
                $item['description'] = $this->filterLanguages( $params['description'] ) ;
            }

            if( isset( $params['headline'] ) )
            {
                $item['headline'] = $this->filterLanguages( $params['headline'] ) ;
            }

            if( isset( $params['additionalType'] ) )
            {
                if( $params['additionalType'] != '' )
                {
                    $additionalType = (string) $params['additionalType'] ;
                    $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
                }
                else
                {
                    // clean
                    $item = null ;
                }
            }

            // -------- colors

            if( isset( $params['color'] ) )
            {
                if( $params['color'] != '' )
                {
                    $item['color'] = $params['color'] ;
                    $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
                }
                else
                {
                    $item['color'] = null ;
                }
            }

            if( isset( $params['bgcolor'] ) )
            {
                if( $params['bgcolor'] != '' )
                {
                    $item['bgcolor'] = $params['bgcolor'] ;
                    $conditions['bgcolor'] = [ $params['bgcolor'] , 'min(3)|max(6)|isColor' ] ;
                }
                else
                {
                    $item['bgcolor'] = null ;
                }
            }

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {

                if( $additionalType )
                {
                    $add = $this->container
                                ->get('badgeItemsTypesController')
                                ->get( NULL , NULL , [ 'id' => $additionalType ] ) ;

                    if( $add )
                    {
                        $item['additionalType'] = (array)$add ;
                    }
                }

                if( is_array( $item ) && ( count( $item ) == 0 || !array_key_exists( 'additionalType' , $item ) ) )
                {
                    $item = null ;
                }
                $badge = [ 'item' => $item ] ;
                $update = $this->model->update
                (
                    $badge,
                    $id,
                    [ 'key' => $key ]
                ) ;

                if( $update )
                {
                    $this->controller->update( $id ) ;

                    $add = $this->model->get
                    (
                        $id ,
                        [
                            'key'    => $key ,
                            'fields' => 'item,created,modified'
                        ]
                    ) ;

                    if( $add )
                    {
                        return $this->success
                        (
                            $response,
                            [
                                "item"     => $add->item ,
                                "created"  => $add->created ,
                                "modified" => $add->modified
                            ]
                        ) ;
                    }
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
