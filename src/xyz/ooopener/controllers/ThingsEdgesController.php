<?php

namespace xyz\ooopener\controllers ;

use Exception ;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\Validation;

class ThingsEdgesController extends EdgesController
{
    /**
     * Creates a new ThingsEdgesController instance.
     * @param ContainerInterface $container
     * @param ?Model             $model
     * @param ?Collections       $owner
     * @param ?Edges             $edge
     * @param ?string            $path
     * @param ?Validation        $validator The optional validator of the controller (by default use a basic Validation instance).
     */
    public function __construct
    (
        ContainerInterface $container    ,
        ?Model             $model     = NULL ,
        ?Collections       $owner     = NULL ,
        ?Edges             $edge      = NULL ,
        ?string            $path      = NULL ,
        ?Validation        $validator = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path , $validator );
    }

    protected ?array $item ;

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ]
    ];

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ref = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;

            if( !$ref )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $this->edge->delete( $this->model->table . '/' . $id ) ; // delete edge

            $result = $this->model->delete( $id ) ; // delete model

            $this->owner->updateDate( $owner ) ; // update owner

            return $response ? $this->success( $response , (int) $result->_key ) : $result ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner' => NULL
    ] ;

    /**
     * Delete all specific things.
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'owner' => $owner ] = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
            }

            $all = $this->edge->getEdge( NULL , $owner ) ;

            if( $all && $all->edge )
            {
                // delete them all
                foreach( $all->edge as $edge )
                {
                    // delete
                    $result = $this->delete( NULL , $response , [ 'owner' => $owner , 'id' => (string) $edge['id'] ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            return $response ? $this->success( $response , 'ok' ) : TRUE ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'lang'  => NULL ,
        'owner' => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner ,
            'skin'  => $skin
        ] = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $owner . ',' . $id . ')' ) ;
        }

        $set = $this->config[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            $ref = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$ref )
            {
                return $this->formatError( $response ,'404' , [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            return $response ? $this->success( $response , $result ) : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Post
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        try
        {
            $this->conditions = [] ;
            $this->item       = [] ;

            $bearer = $this->owner->get( $id , [ 'fields' => '_key' ]  ) ;
            if( !$bearer )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            $this->prepare( $request ) ;

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {
                    $result = $this->model->insert( $this->item ) ;

                    if( $result )
                    {
                        $this->edge->insertEdge( $result->_id , $this->owner->table . '/' . $id ) ; // edge
                        $this->owner->updateDate( $id ) ; // update owner
                        $item = $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function prepare( Request $request = NULL , $params = NULL )
    {

    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * Put specific keyword
     *
     * @param Request|NULL  $request
     * @param Response|NULL $response
     * @param array         $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $this->conditions = [] ;
            $this->item       = [] ;

            $bearer = $this->owner->get( $owner , [ 'fields' => '_key' ]  ) ;
            if( !$bearer || !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $this->prepare( $request ) ;

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;
                if( $this->validator->passes() )
                {
                    $success = $this->model->update( $this->item , $id ) ;
                    if( $success )
                    {
                        $this->owner->updateDate( $owner ) ; // update owner
                        $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

}
