<?php

namespace xyz\ooopener\controllers;

use Psr\Container\ContainerInterface;

use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;

class SettingsListController extends CollectionsController
{
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Setting",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/status")},
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="name",description="The name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="url",description="The url of the resource"),
     *     @OA\Property(type="string",property="category",description="The category of the resource"),
     *     @OA\Property(type="string",property="icon",description=""),
     *     @OA\Property(type="string",property="from",description="The from url of the resource"),
     *     @OA\Property(type="string",property="to",description="The to url of the resource"),
     *     @OA\Property(type="string",property="put",description="The put url of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     *
     * @OA\Response(
     *     response="settingsResponse",
     *     description="Result list of thesauri",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/Setting"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL          ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID            ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_THESAURUS_URL ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_TRANSLATE     ] ,
        'category'      => [ 'filter' =>  Thing::FILTER_DEFAULT       ] ,
        'icon'          => [ 'filter' =>  Thing::FILTER_DEFAULT       ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME      ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME      ] ,
        'from'          => [ 'filter' =>  Thing::FILTER_URL_API       ] ,
        'to'            => [ 'filter' =>  Thing::FILTER_URL_API       ] ,
        'put'           => [ 'filter' =>  Thing::FILTER_URL_API       ]
    ];
}
