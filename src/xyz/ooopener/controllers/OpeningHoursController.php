<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;

use Psr\Http\Message\ServerRequestInterface as Request ;

use DateTime ;
use Exception ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\validations\OpeningHoursSpecificationValidator;

/**
 * The opening hours controller.
 */
class OpeningHoursController extends ThingsEdgesController
{
    /**
     * Creates a new OpeningHoursController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container    ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new OpeningHoursSpecificationValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="OpeningHoursSpecification",
     *     description="A structured value providing information about the opening hours of a place or a certain service inside a place",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="dayOfWeek",description="The day of the week for which these opening hours are valid"),
     *     @OA\Property(type="string",property="opens",description="The opening hour of the place or service on the given day(s) of the week"),
     *     @OA\Property(type="string",property="closes",description="The closing hour of the place or service on the given day(s) of the week"),
     *     @OA\Property(type="string",property="validFrom",format="date-time",description="The date when the item becomes valid"),
     *     @OA\Property(type="string",property="validThrough",format="date-time",description="The date after when the item is not valid"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' => Thing::FILTER_ID        ] ,
        'dayOfWeek'     => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'opens'         => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'closes'        => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'validFrom'     => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'validThrough'  => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME  ] ,
        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody();

        if( isset( $params['alternateName'] ) )
        {
            $this->item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $this->item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['dayOfWeek'] ) && !empty( $params['dayOfWeek'] ) )
        {
            $this->item['dayOfWeek'] = $params['dayOfWeek'] ;
            $params['dayOfWeek'] = explode( ',' , $params['dayOfWeek'] ) ;
        }

        if( isset( $params['opens'] ) )
        {
            $this->item['opens'] = $params['opens'] ;
        }

        if( isset( $params['closes'] ) )
        {
            $this->item['closes'] = $params['closes'] ;
        }

        if( isset( $params['validFrom'] ) && $params['validFrom'] != '' )
        {
            $this->item['validFrom'] = $params['validFrom'] ;
        }

        if( isset( $params['validThrough'] ) && $params['validThrough'] != '' )
        {
            $this->item['validThrough'] = $params['validThrough'] ;
        }

        // ----------

        $equals = true ;
        if( $params['opens'] && $params['closes'] )
        {
            $aCloses = explode( ',' , $params['closes'] ) ;
            $aOpens  = explode( ',' , $params['opens']  ) ;
            $equals = ( count($aCloses) == count($aOpens) ) ;
        }

        // ----------

        $timeStart = NULL ;
        $timeEnd   = NULL ;

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validFrom']) ;
            if( $date )
            {
                $timeStart = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warning( $this . " post failed, " . $e->getMessage() ) ;
        }

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validThrough']);
            if( $date )
            {
                $timeEnd = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warning( $this . " post failed, " . $e->getMessage() ) ;
        }

        $validPeriod = FALSE ;

        if( ($timeStart == NULL) && ($timeEnd == NULL) )
        {
            $validPeriod = TRUE ;
        }
        else if( ($timeStart != NULL) && ($timeEnd != NULL) && ($timeStart <= $timeEnd) )
        {
            $validPeriod = TRUE ;
        }

        $this->conditions =
        [
            'dayOfWeek'        => [ $params['dayOfWeek']    , 'dayOfWeek'          ] ,
            'closes'           => [ $params['closes']       , 'required|times'     ] ,
            'opens'            => [ $params['opens']        , 'required|times'     ] ,
            'openCloseEquals'  => [ $equals                 , 'true'               ] ,
            'validFrom'        => [ $params['validFrom']    , 'date'               ] ,
            'validThrough'     => [ $params['validThrough'] , 'date'               ] ,
            'validPeriod'      => [ $validPeriod            , 'true'               ]
        ];
    }
}


/**
 * @OA\RequestBody(
 *     request="postOpeningHoursSpecification",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="opens",type="integer",description="The opening hour of the place or service on the given day(s) of the week"),
 *             @OA\Property(property="closes",type="integer",description="The closing hour of the place or service on the given day(s) of the week"),
 *             required={"opens","closes"},
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(property="description",ref="#/components/schemas/text"),
 *             @OA\Property(property="dayOfWeek",type="string",description="The day of the week for which these opening hours are valid"),
 *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
 *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putOpeningHoursSpecification",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(property="opens",type="integer",description="The opening hour of the place or service on the given day(s) of the week"),
 *             @OA\Property(property="closes",type="integer",description="The closing hour of the place or service on the given day(s) of the week"),
 *             required={"opens","closes"},
 *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
 *             @OA\Property(property="description",ref="#/components/schemas/text"),
 *             @OA\Property(property="dayOfWeek",type="string",description="The day of the week for which these opening hours are valid"),
 *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
 *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
 *         )
 *     ),
 *     required=true
 * )
 */