<?php

namespace xyz\ooopener\controllers\courses ;

use Psr\Container\ContainerInterface;

use xyz\ooopener\models\Collections;
use xyz\ooopener\validations\CourseValidator;
use xyz\ooopener\controllers\DiscoverController;

/**
 * Class CourseDiscoverController
 */
class CourseDiscoverController extends DiscoverController
{
    public function __construct
    (
        ContainerInterface    $container ,
        Collections           $model     = NULL ,
        string                $path      = NULL ,
        string                $owner     = NULL ,
        ?array                $types     = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $path ,
            $owner ,
            new CourseValidator( $container , $types )
        );
    }
}
