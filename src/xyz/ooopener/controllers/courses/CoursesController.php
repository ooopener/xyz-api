<?php

namespace xyz\ooopener\controllers\courses ;

use xyz\ooopener\controllers\articles\ArticlesController;
use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController;
use xyz\ooopener\controllers\events\EventsController;
use xyz\ooopener\controllers\organizations\OrganizationsController;
use xyz\ooopener\controllers\people\PeopleController;
use xyz\ooopener\controllers\places\PlacesController;
use xyz\ooopener\controllers\stages\StagesController;
use xyz\ooopener\controllers\steps\StepsController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Stages;
use xyz\ooopener\things\Course;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\CourseValidator;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

class CoursesController extends CollectionsController
{
    public function __construct( ContainerInterface $container , Courses $model , $path = 'courses' )
    {
        parent::__construct( $container , $model , $path , new CourseValidator( $container )  );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="CourseList",
     *     description="Course",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="distance",type="string",description="The total course distance in meters"),
     *     @OA\Property(property="elevation",type="object",description="The course elevation"),
     *     @OA\Property(property="level",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="status",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="numSteps",type="number",description="Number of steps"),
     * )
     *
     * @OA\Schema(
     *     schema="Course",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/CourseList")},
     *     @OA\Property(property="openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="audience",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="path",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="steps",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="transportation",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="discover",type="array",items=@OA\Items(ref="#/components/schemas/discovers")),
     * )
     *
     * @OA\Response(
     *     response="courseResponse",
     *     description="Result of the course",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Course")
     *     )
     * )
     *
     * @OA\Response(
     *     response="courseListResponse",
     *     description="Result list of courses",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/CourseList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'distance'       => [ 'filter' => Thing::FILTER_DEFAULT     ] ,
        'elevation'      => [ 'filter' => Thing::FILTER_DEFAULT     ] ,

        'alternativeHeadline'       => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'               => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'headline'                  => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'level'                     => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,
        'status'                    => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,

        'openingHoursSpecification' => [ 'filter' => Thing::FILTER_EDGE        ] ,

        'audible'    => [ 'filter' => Thing::FILTER_BOOL ] ,
        'geofencing' => [ 'filter' => Thing::FILTER_BOOL ] ,

        'numSteps'   => [ 'filter' => Thing::FILTER_JOIN_COUNT  ],
        'numAudios'  => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'  => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'  => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'notes'           => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'text'            => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'audios'          => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'          => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'          => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,
        'audience'        => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'normal' , 'compact' ] ],
        'path'            => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'normal' , 'compact' ] ],
        'steps'           => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'steps' , 'full'  , 'compact' ] ],
        'transportations' => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'compact' ] ],
        'discover'        => [ 'filter' => Thing::FILTER_DEFAULT     , 'skins' => [ 'full' , 'discover' , 'compact' ] ]
    ];

    public const DISCOVER_CONTROLLERS =
    [
        'articles'          => ArticlesController::class ,
        'conceptualObjects' => ConceptualObjectsController::class ,
        'courses'           => CoursesController::class ,
        'events'            => EventsController::class ,
        'organizations'     => OrganizationsController::class ,
        'people'            => PeopleController::class ,
        'places'            => PlacesController::class ,
        'stages'            => StagesController::class
    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Course::FILTER_DISCOVER :
                    {
                        if( property_exists( $init , Course::FILTER_DISCOVER ) )
                        {
                            $items = $init->{ Course::FILTER_DISCOVER } ;

                            if( is_array( $items ) )
                            {
                                $items = array_map
                                (
                                    function ( $path )
                                    {
                                        $array = explode( '/' , $path ) ;
                                        $id    = array_pop( $array ) ;
                                        $type  = implode( '/' , $array ) ;

                                        if( isset( self::DISCOVER_CONTROLLERS[$type] ) )
                                        {
                                            $controller = self::DISCOVER_CONTROLLERS[$type] ;
                                            if( $this->container->has( $controller )  )
                                            {
                                                return $this->container->get( $controller )->get( NULL , NULL , [ 'id' => $id ] ) ;
                                            }
                                        }
                                        return NULL ;
                                    }
                                    ,
                                    $items
                                ) ;

                                $items = array_filter( $items , fn( $item ) => $item != NULL  ) ;
                            }

                             $init->{ Course::FILTER_DISCOVER } = $items ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'courseCoursesTypesController'         , $id ) ; // additionalType
        $this->deleteAllInController( 'courseCoursesAudiencesController'     , $id ) ; // audience
        $this->deleteAllInController( 'courseCoursesLevelsController'        , $id ) ; // level
        $this->deleteAllInController( 'courseOpeningHoursController'         , $id ) ; // openingHours
        $this->deleteAllInController( 'courseCoursesPathsController'         , $id ) ; // paths
        $this->deleteAllInController( 'courseCoursesStatusController'        , $id ) ; // status
        $this->deleteAllInController( StepsController::class                 , $id ) ; // steps
        $this->deleteAllInController( CourseTransportationsController::class , $id ) ; // transportations

        $this->deleteInController( 'courseAudioController' , $id ) ; // audio
        $this->deleteInController( 'courseImageController' , $id ) ; // image
        $this->deleteInController( 'courseVideoController' , $id ) ; // video

        $this->deleteAllInController( 'courseAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'coursePhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'courseVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'userFavorites' , $id ) ; // favorites

        $this->deleteAllArrayPropertyValuesInModel( Courses::class , 'discover' , $id ) ; // courses.discover
        $this->deleteAllArrayPropertyValuesInModel( Stages::class  , 'discover' , $id ) ; // stages.discover
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postCourse",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' );
        }

        try
        {
            $params = $request->getParsedBody() ;

            [
                'additionalType' => $additionalType ,
                'conditions'     => $conditions ,
                'init'           => $init
            ]
            = $this->prepare( $request ) ;

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                $result = $this->model->insert( $init );

                if( $additionalType )
                {
                    $this->insertEdge
                    (
                        $additionalType ,
                        $result->_id ,
                        $this->container->get( 'courseCoursesTypes' )
                    );
                }

                return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function prepare( Request $request = NULL , $old = NULL ) :array
    {
        $conditions = [] ;
        $init       = [] ;

        $additionalType = FALSE ;
        $audience       = FALSE ;
        $level          = FALSE ;
        $path           = FALSE ;
        $status         = FALSE ;

        if( $request )
        {
            $method = $request->getMethod();
            $isPost = $method == 'POST' ;
            $isPut  = $method == 'PUT'  ;
            $params = $request->getParsedBody();

            if( isset( $params['name'] ) )
            {
                $init['name'] = $params['name'] ;
            }

            if( $isPost )
            {
                $init[ 'active'     ] = 1 ;
                $init[ 'audios'     ] = [] ;
                $init[ 'photos'     ] = [] ;
                $init[ 'videos'     ] = [] ;
                $init[ 'discover'   ] = [] ;
                $init[ 'steps'      ] = [] ;
                $init[ 'path'       ] = $this->path ;
                $init[ 'withStatus' ] = Status::DRAFT ;

                if( isset( $params['additionalType'] ) )
                {
                    $additionalType = $params['additionalType'] ;
                    $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
                }
            }
            else if( $isPut )
            {
                if( isset( $params[ 'distance' ] ) )
                {
                    $init[ 'distance' ] = ( $params['distance'] != '' ) ? (int) $params[ 'distance' ] : NULL ;
                }

                if( isset( $params['additionalType'] ) )
                {
                    if( $params['additionalType'] != '' )
                    {
                        $additionalType = $params['additionalType'] ;
                        $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
                    }
                    else
                    {
                        $additionalType = FALSE ;
                    }
                }

                if( isset( $params['audience'] ) )
                {
                    if( $params['audience'] != '' )
                    {
                        $audience = $params['audience'] ;
                        $conditions['audience'] = [ $params['audience'] , 'audience' ] ;
                    }
                    else
                    {
                        $audience = FALSE ;
                    }
                }

                if( isset( $params['level'] ) )
                {
                    if( $params['level'] != '' )
                    {
                        $level = $params['level'] ;
                        $conditions['level'] = [ $params['level'] , 'level' ] ;
                    }
                    else
                    {
                        $level = FALSE ;
                    }
                }

                if( isset( $params['path'] ) )
                {
                    if( $params['path'] != '' )
                    {
                        $path = $params['path'] ;
                        $conditions['path'] = [ $params['path'] , 'path' ] ;
                    }
                    else
                    {
                        $path = FALSE ;
                    }
                }

                if( isset( $params['status'] ) )
                {
                    $status = $params['status'] ;
                    $conditions[ 'status' ] = [ $params['status'] , 'status' ] ;
                }

                $conditions =
                [
                    'audience' => [ $audience           , 'audience' ] ,
                    'distance' => [ $params['distance'] , 'int'      ] ,
                    'level'    => [ $level              , 'level'    ] ,
                    'path'     => [ $path               , 'path'     ]
                ] ;
            }

            if( isset( $params['audible'] ) )
            {
                $init['audible'] = $audible = (bool) $params['audible'] ;
                $conditions['audible'] = [ $audible , 'bool' ] ;
            }
            else
            {
                $init['audible'] = FALSE ;
            }

            if( isset( $params['geofencing'] ) )
            {
                $init['geofencing'] = $geofencing = (bool) $params['geofencing'] ;
                $conditions['geofencing'] = [ $geofencing , 'bool' ] ;
            }
            else
            {
                $init['geofencing'] = FALSE ;
            }

            $conditions[ 'name'   ] = [ $params['name']   , 'required|min(2)|max(70)' ] ;

        }

        return
        [
            'additionalType' => $additionalType ,
            'conditions'     => $conditions ,
            'init'           => $init  ,
            'audience'       => $audience ,
            'level'          => $level ,
            'path'           => $path ,
            'status'         => $status
        ] ;
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT = [ 'id' => NULL ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putCourse",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge(self::ARGUMENTS_PUT_DEFAULT , $args) ;

        try
        {
            if( $response )
            {
                $this->logger->debug($this . ' put(' . $id . ')');
            }

            [
                'additionalType' => $additionalType ,
                'audience'       => $audience ,
                'conditions'     => $conditions ,
                'init'           => $init ,
                'level'          => $level ,
                'path'           => $path ,
                'status'         => $status
            ]
            = $this->prepare( $request ) ;

            if( $this->validator->passes() )
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $to = $this->model->table . '/' . $id ;

                $this->updateEdge
                (
                    $additionalType ,
                    $to ,
                    $this->container->get( 'courseCoursesTypes' )
                ) ;

                $this->updateEdge
                (
                    $audience ,
                    $to ,
                    $this->container->get( 'courseCoursesAudiences' )
                ) ;

                $this->updateEdge
                (
                    $level ,
                    $to ,
                    $this->container->get( 'courseCoursesLevels' )
                ) ;

                $this->updateEdge
                (
                    $path ,
                    $to ,
                    $this->container->get( 'courseCoursesPaths' )
                ) ;

                $this->updateEdge
                (
                    $status ,
                    $to ,
                    $this->container->get( 'courseCoursesStatus' )
                ) ;

                $this->model->update( $init , $id );

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put()' , $e->getMessage() ] , NULL , 500 );
        }
    }
}

