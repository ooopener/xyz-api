<?php

namespace xyz\ooopener\controllers\courses;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\TransportationValidator;

class CourseTransportationsController extends ThingsEdgesController
{
    public function __construct( ContainerInterface $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->validator = new TransportationValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="CourseTransportation",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="integer",property="duration",description="The duration (in minutes) of the tranportation"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'duration'       => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'comment'        => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody();

        $this->conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        $this->conditions['duration']       = [ $params['duration']       , 'required|int'            ] ;

        if( isset( $params['duration'] ) )
        {
            $this->item['duration'] = (int) $params['duration'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $this->item['additionalType'] = (int) $params['additionalType'] ;
        }
    }
}

/**
 * @OA\RequestBody(
 *     request="postCourseTransportation",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="integer",property="duration"),
 *             required={"additionalType","language","technique"},
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putCourseTransportation",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="integer",property="duration"),
 *             required={"additionalType","language","technique"},
 *         )
 *     ),
 *     required=true
 * )
 */