<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use BadMethodCallException;
use Exception ;

use Psr\Container\ContainerInterface ;
/**
 * The translation controller.
 */
class TranslationController extends Controller
{
    /**
     * Creates a new TranslationController instance.
     * @param ContainerInterface $container
     * @param ?Collections $model
     * @param ?string $path
     * @param string $fields
     */
    public function __construct( ContainerInterface $container , ?Collections $model = NULL , ?string $path = NULL , string $fields = 'description' )
    {
        parent::__construct( $container );
        $this->model    = $model ;
        $this->path     = empty($path) ? '' : $path ;
        $this->fullPath = '/' . $path ;
        $this->fields   = ( isset($fields) && !empty($fields) ) ? $fields : '' ;
    }

    /**
     * The table fields to control.
     */
    public string $fields;

    /**
     * The full path expression.
     */
    public string $fullPath;

    /**
     * The path expression.
     */
    public ?string $path ;

    /**
     * The model reference.
     */
    public ?Collections $model;

    // --------------------------------

    /**
     * The default 'translation' method options.
     */
    const ARGUMENTS_TRANSLATION_DEFAULT =
    [
        'id'     => NULL ,
        'lang'   => NULL ,
        'fields' => NULL ,
        'key'    => '_key',
        'params' => NULL
    ] ;

    // --------------------------------

    /**
     * Returns the translation elements of the specific indexed item.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response|object the translation elements of the specific indexed item.
     */
    public function translation( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [
            'id'     => $id ,
            'lang'   => $lang ,
            'fields' => $fields ,
            'key'    => $key,
            'params' => $params ,
        ]
        = array_merge( self::ARGUMENTS_TRANSLATION_DEFAULT , $args ) ;

        if( is_null($fields) || empty($fields) )
        {
            $fields = $this->fields ;
        }

        if( $response )
        {
            $this->logger->debug( $this . ' translation(' . $id . ',' . $lang . ',' . $fields . ')' ) ;
        }

        $api = $this->config['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        try
        {
            $results = $this->model->get( $id , [ "fields" => $fields , 'key' => $key , 'lang' => $lang ] ) ;
            $results = $results->{ $fields } ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'translation(' . $id . ',' . $lang . ',' . $fields . ')', $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $results ,
                $this->getCurrentPath( $request , $params )
            );
        }

        return $results  ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'   => NULL,
        'html' => NULL,
        'key'  => '_key'
    ] ;

    public function patchElement( Request $request , Response $response , array $args = [] , $field = 'description' )
    {
        [
            'id'   => $id,
            'html' => $html,
            'key'  => $key
        ]
        = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchElement(' . $id . ',' . $field . ')' ) ;
        }

        try
        {
            $params = $request->getParsedBody();

            $element = $this->model->exist( $id , [ 'key' => $key ] ) ;

            if( $element == NULL )
            {
                return $this->formatError( $response ,'404', [ $this . ' patchElement(' . $id . ',' . $field . ')' ] , NULL , 404 );
            }

            $item = $this->filterLanguages( $params , $html ) ;

            $item = [ $field => $item ] ;

            $success = $this->model->update( $item , $id , [ 'key' => $key ] ) ;

            if( $success )
            {
                return $this->success( $response , $this->translation( NULL , NULL , [ 'id' => $id , 'key' => $key , 'fields' => $field ] ) );
            }
            else
            {
                return $this->formatError( $response ,'400' , [ $this . ' patchElement(' . $id . ',' . $field . ')' ] );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchElement(' . $id . ',' . $field . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    // -------------------------------- Magick

    public function __call( $name , $arguments )
    {
        if
        (
            is_string( $this->fields )
            && !empty( $this->fields )
            && strpos( $this->fields , $name ) !== FALSE
        )
        {
            if( isset($arguments[2]) && is_array($arguments[2]) )
            {
                $arguments[2]['fields'] = $name ;
            }
            return call_user_func_array( [ $this , 'translation' ] , $arguments );
        }
        else
        {
            throw new BadMethodCallException( "The method '$name' not exist." ) ;
        }
    }

    // --------------------------------
}
