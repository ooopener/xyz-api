<?php

namespace xyz\ooopener\controllers\articles ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\things\CreativeWork;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\TranslationController;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class ArticleTranslationController extends TranslationController
{
    /**
     * ArticleTranslationController constructor.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string|NULL $path
     * @param string $fields
     */
    public function __construct( ContainerInterface $container , Collections $model , string $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function alternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = CreativeWork::FILTER_ALTERNATIVEHEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function headline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = CreativeWork::FILTER_HEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function notes( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_NOTES ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function text( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_TEXT ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , CreativeWork::FILTER_ALTERNATIVEHEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchHeadline( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        return $this->patchElement( $request , $response , $args , CreativeWork::FILTER_HEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchNotes( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_NOTES ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchText( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        return $this->patchElement( $request , $response , $args , Thing::FILTER_TEXT ) ;
    }
}
