<?php

namespace xyz\ooopener\controllers\steps ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Stage;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\TranslationController;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class StepTranslationController extends TranslationController
{
    /**
     * StepTranslationController constructor.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string|NULL $path
     * @param string $fields
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function alternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_ALTERNATIVEHEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function headline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_HEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function notes( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_NOTES ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function text( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_TEXT ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_ALTERNATIVEHEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_HEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchNotes( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_NOTES ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchText( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_TEXT ) ;
    }
}
