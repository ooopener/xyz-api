<?php

namespace xyz\ooopener\controllers\steps;

use xyz\ooopener\controllers\CollectionsController;
use Exception ;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\places\PlacesController;
use xyz\ooopener\controllers\stages\StagesController;
use xyz\ooopener\controllers\ThingsOrderController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Model;
use xyz\ooopener\models\Places;
use xyz\ooopener\models\Stages;
use xyz\ooopener\things\Step;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\StepValidator;
use Slim\HttpCache\CacheProvider;

class StepsController extends ThingsOrderController
{
    /**
     * Creates a new StepsController instance.
     * @param ContainerInterface $container
     * @param ?Collections $model
     * @param ?Collections $owner
     * @param ?CollectionsController $ownerController
     * @param ?string $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?Collections $model                     = NULL ,
        ?Collections $owner                     = NULL ,
        ?CollectionsController $ownerController = NULL,
        ?string $path                           = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $ownerController , $path , new StepValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="StepList",
     *     description="Step",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="position",type="integer"),
     * )
     *
     * @OA\Schema(
     *     schema="Step",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/StepList")},
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="stage",ref="#/components/schemas/StageList")
     * )
     *
     * @OA\Response(
     *     response="stepResponse",
     *     description="Result of the step",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Step")
     *     )
     * )
     *
     * @OA\Response(
     *     response="stepListResponse",
     *     description="Result list of course steps",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/StepList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'audio' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'position'  => [ 'filter' => Thing::FILTER_UNIQUE_NAME , 'skins' => [ 'full' , 'normal' ] ] ,
        'geoRadius' => [ 'filter' => Thing::FILTER_INT ] ,

        'audible'    => [ 'filter' => Thing::FILTER_BOOL ] ,
        'geofencing' => [ 'filter' => Thing::FILTER_BOOL ] ,
        'listable'   => [ 'filter' => Thing::FILTER_BOOL ] ,
        'mappable'   => [ 'filter' => Thing::FILTER_BOOL ] ,

        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'audios'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'audios' ] ] ,
        'photos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'photos' ] ] ,
        'videos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'videos' ] ] ,

        //'markup'        => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' ] ] ,

        'location'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'compact' ] ] ,
        'stage'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Step::FILTER_LOCATION :
                    {
                        if( property_exists( $init , Step::FILTER_LOCATION ) && $init->{ Step::FILTER_LOCATION } != NULL )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( PlacesController::class )
                                                   ->create( (object) $init->{ Step::FILTER_LOCATION } ) ;
                        }
                        break ;
                    }

                    case Step::FILTER_STAGE :
                    {
                        if( property_exists( $init , Step::FILTER_STAGE ) && $init->{ Step::FILTER_STAGE } != NULL )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( StagesController::class )
                                                   ->create( (object) $init->{ Step::FILTER_STAGE } ) ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    public function delete( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $owner = $this->getOwner( $id ) ; // find the owner reference

        if( $owner )
        {
            $args['owner'] = (string) $owner->id ;
        }

        $this->deleteDependencies( $id ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
         $this->deleteInEdgeModel( 'stepLocations' , $id , 'to' ) ; // step.location
         $this->deleteInEdgeModel( 'stepStages'    , $id , 'to' ) ; // step.stage
    }

    /**
     * @param Request|NULL  $request
     * @param Response|NULL $response
     * @param array|NULL    $args
     *
     * @return Response|object
     */
    public function get( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'lang'  => $lang ,
            'skin'  => $skin
        ] = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->config['api'] ;
        $set = $this->config[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( $params['lang'] , $api['languages'] ) )
                {
                    $params['lang'] = $lang = $params['lang'] ;
                }
                else if( $params['lang'] == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'], $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $extraQuery = '' ;

            $objectSkin = NULL ;
            if( $skin == 'extra' )
            {
                $skin       = 'full' ;
                $objectSkin = 'full' ;
            }

            $fields = $this->getFields( $skin ) ;

            // get position
            if( array_key_exists( Model::POSITION , $fields ) )
            {
                $extraQuery = 'LET ' . $fields[ Model::POSITION ][ Model::UNIQUE ]
                            . ' = ( '
                            . 'FOR doc_owner IN ' . $this->owner->table . ' '
                            . 'FILTER POSITION( doc_owner.' . $this->path . ' , TO_NUMBER( @value ) ) == true '
                            . 'RETURN POSITION( doc_owner.' . $this->path . ' , TO_NUMBER( @value ) , true ) '
                            . ')[0]' ;
            }

            $result = $this->model->get
            (
                $id ,
                [
                    'extraQuery'  => $extraQuery ,
                    'queryFields' => $fields ,
                    'lang'        => $lang
                ]
            ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $options = [] ;

                // get owner

                $ownerItem = $this->getOwner( $id , $objectSkin ) ;
                if( $ownerItem )
                {
                    $options[ 'object' ] = $ownerItem ;
                }

                // add headers

                $cache    = $this->container->get( CacheProvider::class ) ;
                $response = $cache->withETag( $response , $result->modified );
                $response = $cache->withLastModified( $response , $result->modified );

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postStep",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="stage",type="integer",description="The stage ID"),
     *             required={"name","stage"},
     *             @OA\Property(property="position",type="integer",description="The position of the step"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        try
        {
            [
                'id'       => $id,
                'owner'    => $owner,
                'position' => $position
            ]
            = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

            if( $response )
            {
                $this->logger->debug( $this . ' post(' . $id . ')' ) ;
            }

            [
                $conditions ,
                $init ,
                $isValid ,
                $location ,
                $stage
            ]
            = $this->prepare( $request ) ;

            $this->validator->validate( $conditions ) ;

            if( $isValid && $this->validator->passes() )
            {
                    $owner = $this->owner->get( $id , [ 'fields' => '_key,' . $this->path ]  ) ;

                    if( !$owner )
                    {
                        return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
                    }

//                    $this->logger->debug( $this . ' post(' . $id . ') item :: ' . json_encode( $init , JSON_PRETTY_PRINT ) ) ;

                    $result = $this->model->insert( $init );

                    if( $result )
                    {
                        if( $location )
                        {
                            $edge = $this->container->get( 'stepLocations' ) ;
                            $edge->insertEdge( $edge->from['name'] . '/' . $location , $result->_id ) ;
                        }

                        if( $stage )
                        {
                            $edge = $this->container->get( 'stepStages' ) ;
                            $edge->insertEdge( $edge->from['name'] . '/' . $stage , $result->_id ) ;
                        }

                        $this->insertInOwner( $result->_key , $owner , $position ) ; // update owner

                        return $this->success
                        (
                            $response ,
                            $this->get( NULL , NULL , [ 'id' => $result->_key , 'skin' => 'full' ] )
                        ) ;
                    }
                    else
                    {
                        return $this->error( $response , 'error' ) ;
                    }
            }
            else
            {
                $errors = [] ;
                if( !$isValid )
                {
                    $errors['location'] = 'The location not must be null, or the stage is not null' ;
                    $errors['stage']    = 'The stage not must be null, or the location is not null' ;
                }
                return $this->getValidatorError( $response , $errors ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function prepare( Request $request = NULL , $old = NULL ) :array
    {
        $conditions = [] ;
        $init       = [] ;
        $isValid    = FALSE ;
        $location   = NULL ;
        $stage      = NULL ;

        if( $request )
        {
            $method = $request->getMethod();
            $params = $request->getParsedBody();
            $isPost = $method == 'POST' ;

            if( $isPost )
            {
                $init['active']     = 1 ;
                $init['withStatus'] = Status::DRAFT ;
                $init['path']       = 'courses/' . $this->path ;
                $init['audios']     = [] ;
                $init['photos']     = [] ;
                $init['videos']     = [] ;
            }

            if( isset( $params['name'] ) )
            {
                $init['name'] = $params['name'] ;
            }

            if( isset( $params['location'] ) )
            {
                $location = $params['location'] ;
            }

            if( isset( $params['stage'] ) )
            {
                $stage = $params['stage'] ;
            }

            if( $location )
            {
                $isValid = is_null($stage)
                        && $this->container
                                ->get( Places::class )
                                ->exist( (string) $location );
            }

            if( $stage )
            {
                $isValid = is_null($location)
                        && $this->container
                                ->get( Stages::class )
                                ->exist( (string) $stage );
            }

            if( isset( $params['headline'] ) )
            {
                $init['headline'] = $this->filterLanguages( $params['headline'] ) ;
            }

            $conditions =
            [
                'name' => [ $params['name']  , 'required|max(70)' ]
            ] ;

            if( isset( $params['position'] ) )
            {
                $position = (int) $params['position'] ;
                $conditions['position'] = [ $params['position'] , 'int|min(0,number)' ] ;
            }

            if( isset( $params['geoRadius'] ) )
            {
                $init['geoRadius'] = $geoRadius = (int) $params['geoRadius'] ;
                $conditions['geoRadius'] = [ $geoRadius , 'min(0,number)|max(1000,number)' ] ;
            }
            else
            {
                $init['geoRadius'] = NULL ;
            }

            if( isset( $params['audible'] ) )
            {
                $init['audible'] = $audible = (bool) $params['audible'] ;
                $conditions['audible'] = [ $audible , 'bool' ] ;
            }
            else
            {
                $init['audible'] = FALSE ;
            }

            if( isset( $params['geofencing'] ) )
            {
                $init['geofencing'] = $geofencing = (bool) $params['geofencing'] ;
                $conditions['geofencing'] = [ $geofencing , 'bool' ] ;
            }
            else
            {
                $init['geofencing'] = FALSE ;
            }

            if( isset( $params['listable'] ) )
            {
                $init['listable'] = $listable = (bool) $params['listable'] ;
                $conditions['listable'] = [ $listable , 'bool' ] ;
            }
            else
            {
                $init['listable'] = TRUE ;
            }

            if( isset( $params['mappable'] ) )
            {
                $init['mappable'] = $mappable = (bool) $params['mappable'] ;
                $conditions['mappable'] = [ $mappable , 'bool' ] ;
            }
            else
            {
                $init['mappable'] = TRUE ;
            }
        }

        return [ $conditions , $init , $isValid , $location , $stage ] ;
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putStep",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="stage",type="integer",description="The stage ID"),
     *             required={"name","stage"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        try
        {

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
            }

            [
                $conditions ,
                $init ,
                $isValid ,
                $location ,
                $stage
            ]
            = $this->prepare( $request ) ;

            $this->validator->validate( $conditions ) ;

            if( $isValid && $this->validator->passes() )
            {
                $to = $this->model->table . '/' . $id ;

                if( $stage )
                {
                    $edge = $this->container->get( 'stepStages' ) ;
                    $from = $edge->from['name'] . '/' . $stage ;

                    if( !$edge->existEdge( $from , $to ) )
                    {
                        $edge->delete( $to , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                        $edge->insertEdge( $from , $to ) ; // add edge
                    }
                }

                $result = $this->model->update( $init , $id );

                if( $result )
                {
                    $owner = $this->getOwner( $id ) ;
                    if( $owner )
                    {
                        $this->owner->updateDate( $owner->id ) ;
                    }

                    return $this->success
                    (
                        $response ,
                        $this->get( NULL , NULL , [ 'id' => $id , 'skin' => 'full' ] )
                    ) ;
                }
                else
                {
                    return $this->error( $response , 'the put method failed with no result after the update of the database resource' ) ;
                }
            }
            else
            {
                $errors = [] ;
                if( !$isValid )
                {
                    $errors['location'] = 'The location not must be null, or the stage is not null' ;
                    $errors['stage']    = 'The stage not must be null, or the location is not null' ;
                }
                return $this->getValidatorError( $response , $errors ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function updateLocation( $location )
    {
        if( $this->container->has( 'stepLocations' ) )
        {
            $steps = $this->container
                          ->get( 'stepLocations' )
                          ->getEdge( (string) $location ) ;

            if( $steps && property_exists( $steps , 'edge' ) && is_array( $steps->edge ) && count( $steps->edge ) > 0 )
            {
                $steps = $steps->edge ;

                foreach( $steps as $step )
                {
                    $step  = (object) $step ;
                    $owner = $this->getOwner( $step->id ) ;

                    $this->model->updateDate( $step->id  ) ; // Steps
                    $this->owner->updateDate( $owner->id ) ; // Courses
                }
            }
        }
    }

    public function updateStage( $stage )
    {
        if( $this->container->has( 'stepStages' ) )
        {
            $steps = $this->container
                          ->get( 'stepStages' )
                          ->getEdge( (string) $stage ) ;

            if( $steps && property_exists( $steps , 'edge' ) && is_array( $steps->edge ) && count( $steps->edge ) > 0 )
            {
                $steps = $steps->edge ;

                foreach( $steps as $step )
                {
                    $step  = (object) $step ;
                    $owner = $this->getOwner( $step->id ) ;

                    $this->model->updateDate( $step->id ) ;
                    $this->owner->updateDate( $owner->id ) ;
                }
            }
        }
    }
}
