<?php

namespace xyz\ooopener\controllers ;

use ArangoDBClient\Exception;
use Psr\Container\ContainerInterface ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Log\LoggerInterface ;

use Slim\App;
use Slim\Interfaces\RouteParserInterface ;

use Slim\Views\Twig ;

use xyz\ooopener\models\Edges ;

use core\Strings ;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use xyz\ooopener\validations\Validation;

/**
 * The abstract Controller class.
 *
 * @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Resource ID",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Parameter(
 *     name="owner",
 *     in="path",
 *     description="Resource ID",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Response(
 *     response="BadRequest",
 *     description="Bad request",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error"),
 *         @OA\Property(property="code", type="string",example="400"),
 *         @OA\Property(property="result", type="string",example="Bad request")
 *     )
 * )
 *
 * @OA\Response(
 *     response="Unauthorized",
 *     description="Not authorized",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error JWT"),
 *         @OA\Property(property="message", type="string",example="Token not found")
 *     )
 * )
 *
 * @OA\Response(
 *     response="Unexpected",
 *     description="Unexpected error",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error"),
 *         @OA\Property(property="code", type="string",example="500"),
 *         @OA\Property(property="result", type="string",example="Internal Server Error")
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="success",
 *     description="Request success",
 *     @OA\Property(property="status", type="string",description="The request status",example="success"),
 *     @OA\Property(property="url", type="string",format="uri",description="URL of the request")
 * )
 *
 */
abstract class Controller
{
    /**
     * Creates a new Controller instance.
     * @param ContainerInterface $container The DI container reference to initialize the controller.
     * @param ?Validation $validator The optional validator of the controller (by default use a basic Validation instance).
     */
    public function __construct( ContainerInterface $container , ?Validation $validator = NULL )
    {
        $this->container = $container ;
        if( $this->container )
        {
            $this->app     = $this->container->get( App::class ) ;
            $this->config  = $this->container->get( 'settings' ) ;
            $this->logger  = $this->container->get( LoggerInterface::class ) ;
            $this->router  = $this->container->get( RouteParserInterface::class ) ;
            $this->tracker = $this->container->get( 'tracker' ) ;
            $this->setValidator( $validator ) ;
        }
    }

    /**
     * The app reference.
     * @private
     */
    protected App $app ;

    /**
     * The optional conditions settings to validate things.
     */
    public ?array $conditions ;

    /**
     * The config reference.
     */
    public array $config ;

    /**
     * The container reference.
     * @private
     */
    protected ContainerInterface $container ;

    /**
     * @var ?array
     */
    protected ?array $item ;

    /**
     * The logger reference.
     */
    protected LoggerInterface $logger ;

    /**
     * @var RouteParserInterface
     */
    protected RouteParserInterface $router ;

    /**
     * The Google Analytics tracker reference.
     */
    public $tracker ;

    /**
     * The internal validator reference of this controller.
     * @var ?Validation
     */
    protected ?Validation $validator ;

    /**
     * Outputs an error message.
     * Ex:
     * return $this->error( $response , 'bad request' , '405' );
     *
     * @param ?Response $response
     * @param string $message
     * @param string $code
     * @param array|null $options
     * @param int $status
     *
     * @return Response
     */
    public function error( ?Response $response , $message = '' , $code = '' , $options = NULL , $status = 400 )
    {
        if( isset($response) )
        {
            $output = [ 'status' => 'error' ];

            if( !empty( $code ) )
            {
                $output[ 'code' ] = $code;
            }

            $output[ 'message' ] = $message ;

            if( $options && is_array($options) )
            {
                $output = array_merge( $output , $options ) ;
            }

            //return $response->withJson( $output , $status , $this->container->jsonOptions );
            return $this->jsonResponse( $response , $output , $status ) ;
        }
        return NULL ;
    }

    public function filterLanguages( $field , $html = NULL ) :array
    {
        $item = [];
        if( $field )
        {
            $languages = $this->config['api']['languages'] ;
            foreach( $languages as $lang )
            {
                if( isset( $field[$lang] ) )
                {
                    $item[$lang] = $html == TRUE ? preg_replace('/(<[^>]+) style=".*?"/i', '$1', $field[$lang] ) : $field[$lang]  ; // if html remove all styles
                }
            }
        }
        return $item ;
    }

    /**
     * Formats a specific error message with a code.
     * Ex:
     * return $this->formatError( $response , '405' , [ 'get(' . $args['name'] . ')' ] , [ 'errors' => ['test' => 2] ] ) ;
     * @param ?Response $response
     * @param string $code
     * @param array|null $args
     * @param array|null $errors
     * @param integer $status
     *
     * @return Response
     */
    public function formatError( ?Response $response , string $code = '', array $args = NULL , array $errors = NULL , int $status = 400 )
    {
        $conf = $this->config ;

        if( !array_key_exists( $code , $conf['errors'] ) )
        {
            $code = $conf['errors']['code_default'] ;
        }

        $message = $conf['errors'][ $code ];

        if( isset( $args ) )
        {
            $str = Strings::fastformat( $message , json_encode( $args ) ) ;
            if( $str != $message )
            {
                $message = $str ;
            }
        }

        if( $conf['useLogging'] && $this->logger )
        {
            $this->logger->error( $this . ' ' . $code . ' : ' . $message . ' -- ' . json_encode( $args ) );
        }

        if( isset( $this->tracker ) )
        {
            $pre = (bool) $conf['analytics']['useVersion']
                 ? '/' . $conf['version']
                 : '' ;

            $this->tracker->pageview( $pre . '/errors/' . $code , $message );
        }

        return $this->error( $response , $message , $code , $errors , $status );
    }

    /**
     * The default 'image' method options.
     */
    const ARGUMENTS_IMAGE_DEFAULT =
    [
        'id'      => NULL  ,
        'check'   => TRUE  ,
        'gray'    => FALSE ,
        'format'  => 'jpg' ,
        'image'   => NULL ,
        'quality' => 70    ,
        'params'  => NULL  ,
        'strip'   => FALSE
    ] ;

    /**
     * Indicates the base path of the application.
     * @return ?string
     */
    public function getBasePath()
    {
        return $this->app->getBasePath() ;
    }

    /**
     * Indicates the base url of the application.
     * @return string
     */
    public function getBaseURL()
    {
        return $this->config['app']['url'] ;
    }

    /**
     * Get a parameter in the body of the current request.
     * @param Request $request
     * @param string $name
     * @return mixed
     */
    public function getBodyParam( Request $request , string $name )
    {
        if( $request )
        {
            $params = (array) $request->getParsedBody();
            if( isset( $params[$name] ) )
            {
                return $params[$name] ;
            }
        }
        return NULL  ;
    }

    /**
     * Get the parameters in the body of the current request.
     * @param Request $request
     * @param array $names
     * @return ?array
     */
    public function getBodyParams( Request $request , array $names ) :?array
    {
        if( $request )
        {
            $variables = [] ;
            $params = (array) $request->getParsedBody();
            foreach( $names as $name )
            {
                if( isset( $params[$name] ) )
                {
                    $variables[$name] = $params[$name] ;
                }
            }
            return $variables ;
        }
        return NULL  ;
    }

    public function getCurrentPath( Request $request = NULL , $params = NULL , $useNow = FALSE ) :string
    {
        $path = isset( $request )
              ? $request->getUri()->getPath()
              : parse_url( $_SERVER["REQUEST_URI"] , PHP_URL_PATH ) ;

        return $this->config['app']['path']
             . $path
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Generates the current application url.
     *
     * @param null $params
     * @param boolean $useNow
     *
     * @return string
     */
    public function getFullPath( $params = NULL , $useNow = FALSE ) :string
    {
        return $this->config['app']['path']
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Generates the current application url.
     * @param string $path
     * @param null $params
     * @param boolean $useNow
     * @return string
     */
    public function getPath( $path = '' , $params = NULL , $useNow = FALSE ) :string
    {
        return $this->config['app']['path']
             . $path
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Get the parameters in the query or body of the current request.
     * @param Request $request
     * @param string $name
     * @return mixed
     */
    public function getParam( Request $request , string $name )
    {
        if( $request )
        {
            $params = (array) $request->getQueryParams();
            if( isset( $params[ $name ] ) )
            {
                return $params[ $name ] ;
            }

            $params = (array) $request->getParsedBody();
            if( isset($params[$name]) )
            {
                return $params[$name];
            }
        }
        return NULL  ;
    }

    /**
     * Get the parameters in the query or body of the current request.
     * @param Request $request
     * @return ?array
     */
    public function getParams( Request $request ) :?array
    {
        if( $request )
        {
            $params = $request->getQueryParams();
            if( isset( $params ) )
            {
                return (array) $params ;
            }

            return (array) $request->getParsedBody();
        }
        return NULL  ;
    }

    /**
     * Get the parameters in the body of the current request.
     * @param Request $request
     * @param string $name
     * @return mixed
     */
    public function getQueryParam( Request $request , string $name )
    {
        if( $request )
        {
            $params = (array) $request->getQueryParams();
            if( isset( $params[ $name ] ) )
            {
                return $params[ $name ] ;
            }
        }
        return NULL  ;
    }

    /**
     * Generates the current application url.
     * @param string $path
     * @param ?array $params
     * @param boolean $useNow
     * @return string
     */
    public function getUrl( $path = '' , ?array $params = NULL , $useNow = FALSE ) :string
    {
        return $this->config['app']['path']
             . $this->app->getBasePath()
             . $path
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * @param Response $response
     * @param $data
     * @param $status
     * @return Response
     */
    public function jsonResponse( Response $response , $data = NULL , $status = 200 ): Response
    {
        $body = $response->getBody() ;

        $body->write( json_encode( $data , $this->container->get('jsonOptions') ) );

        //$this->logger->info( $this . '::success output:' . json_encode( $output , JSON_PRETTY_PRINT ) ) ;

        return $response->withStatus( $status )
                        ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function noBodyResponse( Request $request , Response $response , array $args ) :Response
    {
        //$this->logger->debug( $this . ' noBodyResponse' );
        return $response ;
    }

    /**
     * Redirect to a specific route identifier defines with the setName() method.
     *
     * @param Response $response
     * @param string $name
     * @param array $params
     * @param int $status
     * @return Response
     */
    public function redirectFor( Response $response , string $name , array $params = [] , int $status = 302 ) : Response
    {
        return $this->redirectResponse
        (
            $response ,
            $this->router->urlFor( $name , $params ),
            $status
        ) ;
    }

    /**
     * Redirect to a specific URL target.
     * @param Response $response
     * @param string $url
     * @param int $status
     * @return Response
     */
    public function redirectResponse( Response $response , string $url , int $status = 302 ) : Response
    {
        return $response->withHeader( 'Location' , $url )
                        ->withStatus( $status ) ;
    }

    /**
     * Render the specific view with the current template engine.
     * @param Response $response
     * @param string   $template
     * @param array    $args
     *
     * @return Response
     * @throws LoaderError  When the template cannot be found
     * @throws SyntaxError  When an error occurred during compilation
     * @throws RuntimeError When an error occurred during rendering
     */
    public function render( Response $response , string $template , array $args = [] ) : ?Response
    {
        if( isset( $response ) )
        {
            $view = $this->container->get( Twig::class ) ;
            return $view->render( $response , $template , is_array($args) ? $args : [] );
        }
        return NULL ;
    }

    /**
     * Sets the current internal validator of the controller. By default, creates a new Validation instance.
     * @param ?Validation $validator
     */
    public function setValidator( ?Validation $validator = NULL )
    {
        $this->validator = isset( $validator ) ? $validator : new Validation( $this->container ) ;
    }

    /**
     * Outputs the sucess message.
     * Ex:
     * return $this->success
     * (
     *     $response ,
     *     $data ,
     *     $this->getCurrentPath( $request , $request->getParams()  )
     * );
     * @param ?Response $response The HTTP Response reference.
     * @param mixed $data The data object to returns (output a JSON object).
     * @param string $url The current uri of the service.
     * @param mixed $count An optional count value.
     * @param mixed $options An Array with the optional custom key/value elements to merge into the output object.
     * @param int $status The status code of the response
     *
     * @return Response|NULL
     */
    public function success( ?Response $response , $data , $url = '' , $count = NULL , $options = NULL , $status = 200 ) : ?Response
    {
        if( isset( $response ) )
        {
            $output = [ 'status' => 'success' ];

            if( !empty($url) )
            {
                $output['url'] = $url;
            }

            if( !empty($count) || $count === 0 )
            {
                $output['count'] = $count;
            }

            if( $options && is_array($options) )
            {
                $output = array_merge( $output , $options ) ;
            }

            $output['result'] = $data ;

            return $this->jsonResponse( $response , $output , $status ) ;
        }

        return $response ;
    }

    /**
     * Build a full url from uri and parameters
     * @param $uri
     * @param $params
     *
     * @return string
     */
    public function buildUri( $uri , $params ) :string
    {
        $parse_url = parse_url( $uri ) ;

        foreach( $params as $key => $value )
        {
            if( isset( $parse_url[ $key ] ) )
            {
                $parse_url[ $key ] .= "&" . http_build_query( $value , '' , '&' ) ;
            }
            else
            {
                $parse_url[ $key ] = http_build_query( $value , '' , '&' ) ;
            }
        }

        return
              ( ( isset( $parse_url[ "scheme"   ] ) ) ? $parse_url[ "scheme" ] . "://" : "" )
            . ( ( isset( $parse_url[ "user"     ] ) ) ? $parse_url[ "user" ]
            . ( ( isset( $parse_url[ "pass"     ] ) ) ? ":" . $parse_url[ "pass" ] : "" ) . "@" : "" )
            . ( ( isset( $parse_url[ "host"     ] ) ) ? $parse_url[ "host" ] : "" )
            . ( ( isset( $parse_url[ "port"     ] ) ) ? ":" . $parse_url[ "port" ] : "" )
            . ( ( isset( $parse_url[ "path"     ] ) ) ? $parse_url[ "path" ] : "" )
            . ( ( isset( $parse_url[ "query"    ] ) && !empty( $parse_url[ 'query' ] ) ) ? "?" . $parse_url[ "query" ] : "" )
            . ( ( isset( $parse_url[ "fragment" ] ) ) ? "#" . $parse_url[ "fragment" ] : "" )
            ;
    }

    public function translate( $texts , $lang = NULL )
    {
        $languages = $this->config['api']['languages'] ;

        if( $lang == NULL )
        {
            return $texts ;
        }
        else
        {
            if( array_key_exists( $lang , $texts ) )
            {
                return $texts[ $lang ] ;
            }
            else if( array_key_exists( $languages[0] , $texts ) )
            {
                return $texts[ $languages[0] ] ;
            }
        }
    }

    /**
     * @param string $fieldId
     * @param string $to
     * @param Edges $edge
     * @return ?object
     * @throws Exception
     */
    public function insertEdge( string $fieldId , string $to , Edges $edge ) :?object
    {
        if( $fieldId !== NULL )
        {
            return $edge->insertEdge( $edge->from['name'] . '/' . $fieldId , $to ) ;
        }
        return NULL ;
    }

    /**
     * @param $fieldId
     * @param string $to
     * @param Edges $edge
     * @throws Exception
     */
    public function updateEdge( $fieldId , string $to , Edges $edge )
    {
        if( $fieldId !== NULL )
        {
            if( $fieldId === FALSE )
            {
                $edge->delete( $to , [ 'key' => '_to' ] ) ; // delete all edges to be sure
            }
            else
            {
                $idFrom = $edge->from['name'] . '/' . $fieldId ;

                if( !$edge->existEdge( $idFrom , $to ) )
                {
                    $edge->delete( $to , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                    $edge->insertEdge( $idFrom , $to ) ; // add
                }
            }
        }
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return '[' . get_class($this) . ']' ;
    }

    // ----- helpers

    protected function deleteAllInController( $controllerID , $id , ?array $args = [] , string $key = 'owner' )
    {
        if( $this->container->has( $controllerID ) )
        {
            if( !is_array( $args ) )
            {
                $args = [] ;
            }
            $this->container->get( $controllerID )->deleteAll( NULL , NULL , array_merge( [ $key => $id ] , $args ) ) ;
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $controllerID . '" definition not exist in the DI container.') ;
        }
    }

    protected function deleteAllArrayPropertyValuesInModel( $modelID , $name , $id )
    {
        if( $this->container->has( $modelID ) )
        {
            $this->container->get( $modelID )->deleteAllArrayPropertyValues( $name , [ $this->path . '/' . $id ] ) ;
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $modelID . '" definition not exist in the DI container.') ;
        }
    }

    protected function deleteInController( $controllerID , $id , ?array $args = []  )
    {
        if( $this->container->has( $controllerID ) )
        {
            if( !is_array( $args ) )
            {
                $args = [] ;
            }
            $this->container->get( $controllerID )->delete( NULL , NULL , array_merge( [ 'id' => $id ] , $args ) ) ;
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $controllerID . '" definition not exist in the DI container.') ;
        }
    }

    protected function deleteInEdgeModel( $modelID , $id , $direction = 'from' )
    {
        $result = NULL ;

        if( $this->container->has( $modelID ) )
        {
            $model = $this->container->get( $modelID ) ;
            if( $model instanceof Edges )
            {
                $id = $this->model->table . '/' . $id ;

                switch( $direction )
                {
                    case $direction === 'both' :
                    {
                        $result = $model->deleteEdgeBoth( $id ) ;
                        break ;
                    }
                    case $direction === 'to'   :
                    {
                        $result = $model->deleteEdgeTo( $id ) ;
                        break ;
                    }
                    default : // from
                    {
                        $result = $model->deleteEdgeFrom( $id ) ;
                    }
                }
            }
            else
            {
                $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the edge model reference is not a valid Edges instance.') ;
            }
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $modelID . '" definition not exist in the DI container.') ;
        }

        return $result ;
    }

    /**
     * Removes an element in an edge model and remove the opposite resources in the specific controller.
     * @param $id
     * @param string $edgeModel The edge model reference.
     * @param string $controllerID The identifier of the controller to remove the opposite elements defines in the edge model.
     * @param string $direction The direction of the element to vertex to remove in the edge model (from or to)
     */
    protected function deleteInEdgeAndInController( $id , string $edgeModel , string $controllerID , $direction = 'from' )
    {
        if( $this->container->has( $controllerID ) && $this->container->has( $edgeModel ) )
        {
            $removed = $this->deleteInEdgeModel( $edgeModel , $id , $direction ) ;
            if( is_array( $removed ) && count( $removed ) > 0 )
            {
                $controller = $this->container->get( $controllerID ) ;
                $table      = $controller->model->table ;
                foreach( $removed as $edge )
                {
                    if( is_array($edge) && isset( $edge['_to'] ) )
                    {
                        if( isset( $edge['_to'] ) )
                        {
                            $controller->delete
                            (
                                NULL ,
                                NULL ,
                                [ 'id' => str_replace( $table . '/' , '' , $edge['_to'] ) ] // remove the prefix of the edge['_to'] reference (keep only the key)
                            ) ;
                        }
                        else
                        {
                            $this->logger->warning( $this . '::' . __FUNCTION__ . '(' . $id . ',' . $edgeModel . ',' . $controller . ') failed, the current edge array not contains the "_to" property : ' . json_encode( $edge )) ;
                        }
                    }
                    else
                    {
                        $this->logger->warning( $this . '::' . __FUNCTION__ . '(' . $id . ',' . $edgeModel . ',' . $controller . ') failed, the current edge is not an array reference : ' . json_encode( $edge )) ;
                    }
                }
            }
        }
    }

    protected function getValidatorError( ?Response $response  , ?array $errors = [] ) : ?Response
    {
        if( !is_array( $errors ) )
        {
            $errors = [] ;
        }

        if( $this->validator )
        {
            $messages = $this->validator->errors() ;
            $keys     = $messages->keys() ;
            foreach( $keys as $key )
            {
                $errors[$key] = $messages->first( $key ) ;
            }
        }

        return $this->error( $response , $errors , '400' ) ;
    }
}


