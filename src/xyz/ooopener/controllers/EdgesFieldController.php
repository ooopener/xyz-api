<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use Exception;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

/**
 * The edges field controller.
 */
class EdgesFieldController extends Controller
{
    /**
     * Creates a new EdgesFieldController instance.
     * @param ContainerInterface $container
     * @param ?Model $model
     * @param ?string $field
     * @param bool $single
     */
    public function __construct( ContainerInterface $container , ?Model $model = NULL , ?string $field = NULL , bool $single = FALSE )
    {
        parent::__construct( $container );
        $this->model  = $model ;
        $this->field  = $field ;
        $this->single = $single ;
    }

    /**
     * The eventStatus database field name.
     */
    public ?string $field ;

    /**
     * The model reference.
     */
    public ?Model $model;

    /**
     * The edge if is a single record.
     */
    public bool $single;

    /**
     * The default 'all' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'     => NULL ,
        'lang'   => NULL ,
        'params' => NULL
    ] ;

    /**
     * Returns specific thesaurus field value.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return object the collection of all types of the specific indexed event.
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [
            'id'     => $id ,
            'lang'   => $lang ,
            'params' => $params
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $api    = $this->config['api'] ;
            $params = $request->getQueryParams();
            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $field =
            [
                $this->field =>
                [
                    'filter' => $this->single ? Thing::FILTER_EDGE_SINGLE : Thing::FILTER_EDGE ,
                    'unique' => $this->field
                ]
            ] ;

            $item = $this->model->get( $id , [ 'queryFields' => $field ] ) ;

            if( $item )
            {
                $item = $item->{ $this->field } ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getCurrentPath( $request , $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }
}
