<?php

namespace xyz\ooopener\controllers;

use Exception ;
use Imagick ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use Slim\HttpCache\CacheProvider ;

use xyz\ooopener\data\Builder ;

use xyz\ooopener\helpers\Strings ;

use xyz\ooopener\models\Thesaurus ;

use xyz\ooopener\things\Thing ;
use xyz\ooopener\things\Word ;

use xyz\ooopener\validations\ThesaurusValidator ;

/**
 * The thesaurus controller class.
 */
class ThesaurusController extends Controller implements Builder
{
    /**
     * Creates a new ThesaurusController instance.
     * @param ContainerInterface            $container
     * @param Thesaurus                     $model
     * @param string                        $path
     * @param ThesaurusCollectionController $ownerController
     */
    public function __construct
    (
        ContainerInterface            $container ,
        Thesaurus                     $model ,
        string                        $path ,
        ThesaurusCollectionController $ownerController
    )
    {
        parent::__construct( $container , new ThesaurusValidator( $container , $model ) );
        $this->ownerController = $ownerController ;
        $this->model           = $model ;
        $this->setPath( $path ) ;
    }

    /**
     * The full path expression.
     */
    public string $fullPath = '';

    /**
     * The icon path expression.
     */
    public string $iconPath = '' ;

    /**
     * The model reference.
     */
    public Thesaurus $model;

    /**
     * The thesaurus collection owner controller reference.
     */
    public ThesaurusCollectionController $ownerController ;

    public string $path = '' ;

    /**
     * The word entry url pattern expression.
     */
    public string $pattern = '';

    /**
     * The settings object.
     */
    public ?array $settings;

    // --------------------------------

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Thesaurus",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/status")},
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="name",description="The name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="alternateName",description="The name of the resource"),
     *     @OA\Property(property="description",description="The description of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="format",description="Format of the resource"),
     *     @OA\Property(type="string",property="pattern",description="Pattern of the resource"),
     *     @OA\Property(type="string",property="validator",description="Validator of the resource"),
     *     @OA\Property(type="string",property="image",description="Image of the resource",format="uri"),
     *     @OA\Property(type="string",property="color",description="Color of the resource"),
     *     @OA\Property(type="string",property="bgcolor",description="Background color of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     *
     * @OA\Response(
     *     response="thesaurusResponse",
     *     description="Thesaurus result",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Thesaurus")
     *     )
     * )
     *
     * @OA\Response(
     *     response="thesaurusCollectionResponse",
     *     description="Result list of thesaurus",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/Thesaurus"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL            ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID              ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL             ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_TRANSLATE       ] ,
        'alternateName' => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE       ] ,
        'format'        => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'pattern'       => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'validator'     => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'image'         => [ 'filter' =>  Thing::FILTER_THESAURUS_IMAGE ] ,
        'color'         => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'bgcolor'       => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME        ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME        ]
    ];

    public function getFields( $skin = NULL ) :array
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins  = array_key_exists( 'skins'  , $options ) ? $options['skins']  : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            //// set unique value for edge, edgeSingle and join filters
            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                {
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                }
                case Thing::FILTER_JOIN :
                {
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
                }
            }
        }

        return $fields ;
    }

    public function sortAfter( $items )
    {
        $sortable = $this->model->sortable ;

        if( $sortable && array_key_exists( 'after' , $sortable ) )
        {
            $after = explode( '.' , $sortable['after'] ) ;

            if( $after && is_array( $after ) && count( $after ) == 2 )
            {
                usort( $items , function ( $a , $b ) use ( $after )
                {
                    return strcmp( $a->{$after[0]}->{$after[1]} , $b->{$after[0]}->{$after[1]} ) ;
                }) ;
            }
        }
        return $items ;
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'lang'    => NULL ,
        'limit'   => NULL ,
        'offset'  => NULL ,
        'search'  => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns of all the words.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed of all the words.
     */
    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active'  => $active ,
            'facets'  => $facets ,
            'groupBy' => $groupBy ,
            'lang'    => $lang ,
            'limit'   => $limit ,
            'offset'  => $offset ,
            'search'  => $search ,
            'sort'    => $sort ,
            'params'  => $params
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $api = $this->config['api'] ;
        $set = $this->getSettings() ;

        $format = $request->getAttribute( 'format' ) ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- limit

            $limit = intval( isset($limit) ? $limit : $api['limit_default'] ) ;
            if( isset($params['limit']) )
            {
                $limit = filter_var
                (
                    $params['limit'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['limit'] = intval( ($limit !== FALSE) ? $limit : $api['limit_default'] ) ;
            }

            // ----- offset

            $offset = intval( isset($offset) ? $offset : $api['offset_default'] ) ;
            if( isset($params['offset']) )
            {
                $offset = filter_var
                (
                    $params['offset'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['offset'] = intval( ($offset !== FALSE) ? $offset : $api['offset_default'] ) ;
            }

            // ----- facets

            if( isset( $params[ 'facets' ] ) )
            {
                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = array_merge
                        (
                            json_decode( $params[ 'facets' ] , TRUE ) ,
                            isset( $facets ) ? $facets : []
                        );
                    }
                    else
                    {
                        $facets = $params['facets'] ;
                    }

                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                }
            }

            // ----- sort

            if( isset($params['sort']) )
            {
                $sort = $params['sort'] ;
            }
            else if( is_null($sort) )
            {
                if( isset($set['sort_default']) )
                {
                    $sort = $set['sort_default'];
                }
                else if( isset($this->config['thesaurus-item']) && isset($this->config['thesaurus-item']['sort_default']) )
                {
                    $sort = $this->config['thesaurus-item']['sort_default'] ;
                }
            }

            // ----- groupBy

            if( isset($params['groupBy']) )
            {
                $groupBy = $params['groupBy'] ;
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }

            // ----- active

            if( isset($params['active']) )
            {
                if
                (
                    $params['active'] == "0"  ||
                    $params['active'] == 'false'  ||
                    $params['active'] == 'FALSE'
                )
                {
                    $active = NULL  ;
                }
            }
        }

        // ------------

        $result  = NULL ;
        $options = NULL ;

        try
        {
            $init =
            [
                'active'      => $active ,
                'facets'      => $facets ,
                'search'      => $search
            ];

            $result = $this->model->all
            ([
                'active'      => $active ,
                'facets'      => $facets ,
                'limit'       => $limit ,
                'offset'      => $offset ,
                'sort'        => $sort,
                'groupBy'     => $groupBy,
                'search'      => $search,
                'lang'        => $lang,
                'queryFields' => $this->getFields()
            ]) ;

            $options =
            [
                'total'  => $this->model->count( $init ) ,
                'object' => $this->getOwner( $request )
            ] ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            switch( $format )
            {
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $result,
                        $this->getCurrentPath( $request , $params ),
                        is_array($result) ? count($result) : NULL ,
                        $options
                    ) ;
                }
            }
        }

        return $result ;
    }

    public function getOwner( Request $request ) :?object
    {
        $path = ltrim
        (
            $request->getUri()->getPath() ,
            $this->getBasePath()
        ) ;

        return $this->ownerController->model->get
        (
            $path ,
            [
                'key'         => 'path' ,
                'queryFields' => $this->ownerController->getFields()
            ]
        );
    }

    // --------------------------------


    /**
     * The default 'build' method options.
     */
    const ARGUMENTS_BUILD_DEFAULT = [ 'lang' => NULL ] ;

    /**
     * Build a new object.
     *
     * @param null $init
     * @param array $options
     *
     * @return Word|NULL
     */
    public function build( $init = NULL , array $options = [] )
    {
        [ 'lang' => $lang ] = array_merge( self::ARGUMENTS_BUILD_DEFAULT , is_array($options) ? $options : [] ) ;
        return $this->create( $init , $lang ) ;
    }

    /**
     * Creates and populates a new item.
     *
     * @param object $init
     * @param string|null $lang
     *
     * @return object|NULL
     */
    public function create( object $init , string $lang = NULL )
    {
        return $init ;
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active' => TRUE
    ] ;

    /**
     * Returns the number of items.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return integer|Response the number of items.
     */
    public function count( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        [ 'active' => $active ] = array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
        }

        try
        {
            $count = $this->model->count( [ 'active' => $active ] ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'count' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success( $response , $count , $this->getCurrentPath( $request ) ) ;
        }

        return $count > 0 ? $count : 0 ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT = [ 'id' => NULL ] ;

    /**
     * Delete the item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $id . ')' ) ;
        }

        try
        {
            $result = $this->model->delete( $id ) ;

            if( $result )
            {

                $icon = $this->getImagePath( $id ) ;
                if( file_exists($icon) )
                {
                    $flag = unlink( $icon ) ;
                }

                return $this->success( $response , $id );
            }
            else
            {
                return $this->error( $response , 'error' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * Delete all thesaurus
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' deleteAll()' ) ;

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    $result = $this->model->deleteAll( $items );

                    if( $result )
                    {
                        return $this->success( $response , "ok" );
                    }
                    else
                    {
                        return $this->error( $response , "not deleted" ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'deleteAll()' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    // --------------------------------

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'  => TRUE ,
        'id'      => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns the specific item by id.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active'  => $active ,
            'id'      => $id ,
            'item'    => $item ,
            'lang'    => $lang ,
            'params'  => $params
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->config['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        // ----------------

        $item = NULL ;

        try
        {
            $item = $this->model->get( $id , [ 'lang' => $lang , 'queryFields' => $this->getFields() ] ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                $cache    = $this->container->get( CacheProvider::class ) ;
                $response = $cache->withLastModified( $response , $item->modified ); // add header

                return $this->success
                (
                    $response,
                    $item,
                    $this->getCurrentPath( $request , $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    // --------------------------------

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_BY_ALTERNATE_NAME_DEFAULT =
    [
        'active'  => TRUE ,
        'name'    => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns the specific item by the alternateName.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return object the specific item by the alternateName.
     */
    public function getByAlternateName( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active'  => $active ,
            'name'    => $name,
            'item'    => $item,
            'lang'    => $lang,
            'params'  => $params
        ]
        = array_merge( self::ARGUMENTS_GET_BY_ALTERNATE_NAME_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' getByAlternateName(' . $name . ')' ) ;
        }

        $api = $this->config['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        // ----------------

        $item = NULL ;

        try
        {
            $item = $this->model->getByAlternateName( $name ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'getByAlternateName(' . $name . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getCurrentPath( $request , $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    // --------------------------------

    /**
     * The default 'get method options.
     */
    const ARGUMENTS_GET_IMAGE_DEFAULT =
    [
        'id'      => NULL ,
        'bgcolor' => NULL ,
        'color'   => NULL ,
        'margin'  => 2    ,
        'params'  => NULL ,
        'render'  => NULL ,
        'shadow'  => NULL ,
        'w'       => NULL ,
        'h'       => NULL
    ] ;

    /**
     * Display the image of the specific item.
     * Ex: ../image?shadow=true
     * Ex: ../image?render=map&bgcolor=ff0000&color=ffffff&margin=4&shadow=40,2,20,20
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return Response the image of the specific item
     */
    public function getImage( Request $request = NULL , Response $response = NULL , array $args = [] ) // FIXME remove this method
    {
        [
            'id'      => $id ,
            'bgcolor' => $bgcolor ,
            'color'   => $color ,
            'margin'  => $margin   ,
            'params'  => $params ,
            'render'  => $render ,
            'shadow'  => $shadow ,
            'w'       => $w ,
            'h'       => $h
        ]
        = array_merge( self::ARGUMENTS_GET_IMAGE_DEFAULT , $args ) ;

        $icons = $this->config['icons'] ;
        $w     = $icons['width'] ;
        $h     = $icons['height'] ;

        if( $response )
        {
            $this->logger->debug( $this . ' getImage(' . $id . ')' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            if( isset($params['w']) )
            {
                $w = filter_var
                (
                    $params['w'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minrange'], "max_range" => $icons['maxrange'] ] ]
                ) ;
                $w = ($w !== FALSE) ? $w : $icons['width'] ;
            }

            if( isset($params['h']) )
            {
                $h = filter_var
                (
                    $params['h'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minrange'], "max_range" => $icons['maxrange'] ] ]
                ) ;
                $h = ($h !== FALSE) ? $h : $icons['height'] ;
            }

            if( isset($params['color']) )
            {
                $color = filter_var
                (
                    strtolower($params['color']),
                    FILTER_VALIDATE_REGEXP,
                    [ 'options' => [ 'regexp' => '/^[a-f0-9]{6}$/i' ] ]
                ) ;
                $color = ($color !== FALSE) ? $color : NULL ;
            }

            if( isset($params['bgcolor']) )
            {
                $bgcolor = filter_var
                (
                    strtolower($params['bgcolor']),
                    FILTER_VALIDATE_REGEXP,
                    [ 'options' => [ 'regexp' => '/^[a-f0-9]{6}$/i' ] ]
                ) ;
                $bgcolor = ($bgcolor !== FALSE) ? $bgcolor : NULL ;
            }

            if( !empty($params['render']) )
            {
                if( in_array( strtolower($params['render']) , $icons['renders'] ) )
                {
                    $params['render'] = $render
                                      = strtolower($params['render']) ;
                }
            }

            if( isset($params['margin']) )
            {
                $margin = filter_var
                (
                    $params['margin'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minmargin'], "max_range" => $icons['maxmargin'] ] ]
                ) ;
                $margin = ($margin !== FALSE) ? $margin : $icons['margin'] ;
            }

            if( !empty($params['shadow']) )
            {
                $shadow = $params['shadow'] ;
            }
        }

        // -------

        $url = $this->getImagePath( $id ) ;

        if( file_exists( $url ) )
        {
            // add header
            $cache = $this->container->get( CacheProvider::class ) ;
            $response = $cache->withLastModified( $response , filemtime( $url ) );

            $response = $this->container->get('image')->icon
            (
                $response , $url ,
                [
                    'w'       => $w ,
                    'h'       => $h ,
                    'bgcolor' => $bgcolor ,
                    'color'   => $color ,
                    'render'  => $render,
                    'margin'  => $margin,
                    'shadow'  => $shadow
                ]
            ) ;

            return $response ;
        }

        return $this->formatError( $response , '204' , [ 'getImage(' . $id . ')' ] , NULL , 204 ) ;
    }

    /**
     * Returns the specific image path.
     * @param int $id The id of the image.
     * @param bool $lazy Indicates if the path is returned without a checking if the file exist.
     * @return string
     */
    public function getImagePath( int $id , $lazy = FALSE )
    {
        try
        {
            if( !is_dir( $this->iconPath ) )
            {
                if( !mkdir( $this->iconPath , 0755 , TRUE ) )
                {
                    $this->logger->warning( $this . ' getImagePath(' . $id . ') failed, can\'t create the image path:[' . $this->iconPath . ']' ) ;
                }
            }

            $iconPrefix = '' ;
            $iconSuffix = '.png' ;

            $set = $this->getSettings() ;

            if( $set )
            {
                if( isset( $set['iconPrefix'] ) )
                {
                    $iconPrefix = $set['iconPrefix'] ;
                }

                if( isset( $set['iconSuffix'] ) )
                {
                    $iconSuffix = $set['iconSuffix'] ;
                }
            }

            $icon = $this->iconPath . '/' . $iconPrefix . str_pad( $id , 4, "0", STR_PAD_LEFT ) . $iconSuffix  ;

            if( $lazy || file_exists($icon) )
            {
                return $icon ;
            }
        }
        catch( Exception $e )
        {
            $this->logger->error( $this . ' getImagePath(' . $id . ') failed, the following Exception occurred: ' . $e->getMessage() ) ;
        }
    }

    /**
     * Returns the current thesaurus settings
     * @return ?array
     */
    public function getSettings() :?array
    {
        if( isset( $this->settings ) )
        {
            return $this->settings ;
        }

        try
        {
            $this->settings = $this->config[ $this->path ] ;
        }
        catch( Exception $e )
        {
            $this->settings = NULL ;
        }

        return $this->settings ;
    }

    /**
     * Add new thesaurus
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getParsedBody() ;

            $init = [] ;
            $conditions = [] ;

            $init['active'] = 1 ;
            $init['path'] = 'thesaurus/' . $this->path ;

            if( isset( $params['name'] ) )
            {
                $init['name'] = $this->filterLanguages( $params['name'] ) ;
            }

            if( isset( $params['description'] ) )
            {
                $init['description'] = $this->filterLanguages( $params['description'] ) ;
            }

            // -------- alternateName

            if( isset($params['alternateName']) )
            {
                $init['alternateName'] = strtolower( Strings::latinize( $params['alternateName'] ) ) ;
                $conditions['alternateName'] = [ $init['alternateName'] , 'required|alnumSlash|uniqueAlternateName|max(150)' ] ;
            }

            // -------- colors

            if( isset( $params['color'] ) )
            {
                $init['color'] = $params['color'] ;
                $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
            }

            if( isset( $params['bgcolor'] ) )
            {
                $init['bgcolor'] = $params['bgcolor'] ;
                $conditions['bgcolor'] = [ $params['bgcolor'] , 'min(3)|max(6)|isColor' ] ;
            }

            if( isset( $params['pattern'] ) )
            {
                $init['pattern'] = $params['pattern'] ;
            }

            if( isset( $params['format'] ) )
            {
                $init['format'] = $params['format'] ;
            }

            if( isset( $params['validator'] ) )
            {
                $init['validator'] = $params['validator'] ;
            }

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                try
                {
                    $index = $this->model->insert( $init ) ;

                    if( $index )
                    {
                        return $this->success( $response , $this->model->get( $index->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'post' , $e->getMessage() ] , NULL , 500 );
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }

        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT = [ 'id' => NULL ] ;

    /**
     * Put new values for the item
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     * @throws \ArangoDBClient\Exception
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $item = $this->model->get( $id , [ 'fields' => '_key,alternateName' ] ) ;

        if( !isset( $item ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ], NULL , 404 );
        }

        if( isset( $request ) )
        {
            $params = $request->getParsedBody() ;

            $init       = [] ;
            $conditions = [] ;

            if( isset( $params['name'] ) )
            {
                $init['name'] = $this->filterLanguages( $params['name'] ) ;
            }

            if( isset( $params['description'] ) )
            {
                $init['description'] = $this->filterLanguages( $params['description'] ) ;
            }

            // -------- alternateName

            if( isset($params['alternateName']) )
            {
                if( $params['alternateName'] != "" )
                {
                    $init['alternateName'] = strtolower( Strings::latinize( $params['alternateName'] ) ) ;
                    $conditions['alternateName'] =
                    [
                        $init['alternateName'] ,
                        'alnumSlash|uniqueAlternateName(' . $item->alternateName . ')|max(150)'
                    ] ;
                }
                else
                {
                    $init['alternateName'] = NULL ;
                }

            }

            // -------- colors

            if( isset( $params['color'] ) && $params['color'] != '' )
            {
                $init['color'] = $params['color'] ;
                $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
            }
            else
            {
                $init['color'] = NULL ;
            }

            if( isset( $params['bgcolor'] ) && $params['bgcolor'] != '' )
            {
                $init['bgcolor'] = $params['bgcolor'] ;
                $conditions['bgcolor'] = [ $params['bgcolor'] , 'min(3)|max(6)|isColor' ] ;
            }
            else
            {
                $init['bgcolor'] = NULL ;
            }

            if( isset( $params['pattern'] ) )
            {
                $init['pattern'] = $params['pattern'] ;
            }

            if( isset( $params['format'] ) )
            {
                $init['format'] = $params['format'] ;
            }

            if( isset( $params['validator'] ) )
            {
                $init['validator'] = $params['validator'] ;
            }

            $this->validator->validate( $conditions ) ;

            if( $this->validator->passes() )
            {
                try
                {
                    $result = $this->model->update( $init ,$id ) ;

                    if( $result )
                    {
                        return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'put' , $e->getMessage() ] , NULL , 500 );
                }
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }

        }
    }

    /**
     * The default 'patchActive' methods options.
     */
    const ARGUMENTS_PATCH_ACTIVE_DEFAULT =
    [
        'id'   => NULL ,
        'bool' => NULL
    ] ;

    /**
     * Updates the activity of the item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchActive( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'   => $id ,
            'bool' => $bool
        ]
        = array_merge( self::ARGUMENTS_PATCH_ACTIVE_DEFAULT , $args )  ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchActive(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patchActive(' . $id . ')' ], NULL , 404 );
            }

            if( is_string( $bool ) )
            {
                $bool = strtolower( $bool ) ;
            }

            if( $bool != 'true' && $bool != 'false' )
            {
                return $this->formatError( $response , '400', [ 'patchActive(' . $id . ',' . $bool . ')' ] );
            }

            $active = $bool == 'true' ;


            $result = $this->model->update( [ 'active' => (int) $active ] , $id ) ;

            if( $result )
            {
                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
            }
            else
            {
                return $this->error( $response , 'error' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchActive(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * The default 'deleteImage' methods options.
     */
    const ARGUMENTS_DELETE_IMAGE_DEFAULT = [ 'id'   => NULL ] ;

    public function deleteImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_IMAGE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteImage(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'deleteImage(' . $id . ')' ], NULL , 404 );
            }

            $flag = FALSE ;

            $icon = $this->getImagePath( $id ) ;
            if( file_exists($icon) )
            {
                $flag = unlink( $icon ) ;
            }

            $update = $this->model->update( [ "image" => false ] , $id ) ;

            return $this->success( $response , (int) $id ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }

    /**
     * The default 'postImage' methods options.
     */
    const ARGUMENTS_POST_IMAGE_DEFAULT = [ 'id' => NULL ] ;

    public function postImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_POST_IMAGE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' postImage(' . $id . ')' ) ;
        }

        try
        {
            if( !( $this->model->exist( $id ) )  )
            {
                return $this->formatError( $response , '404', [ 'postImage(' . $id . ')' ], NULL , 404 );
            }

            if( $request )
            {
                $filename = NULL ;
                $files    = $request->getUploadedFiles() ;

                if ( empty( $files ) )
                {
                    return $this->error( $response , ' postImage(' . $id . ') failed, the $files is empty.' , "400" ) ;
                }
                else if ( !isset( $files['icon'] ) )
                {
                    return $this->error( $response , ' postImage(' . $id . ') failed, the $files["file"] is not set.' , "400" ) ;
                }
                else
                {
                    $file  = $files['icon'] ;
                    $error = $file->getError() ;
                    if( $error === UPLOAD_ERR_OK )
                    {
                        $icons = $this->config['icons'] ;

                        $filename = $file->getClientFilename() ;
                        $size     = $file->getSize() ;
                        $type     = $file->getClientMediaType() ;

                        // check filesize
                        if( isset( $icons['maxsize' ]) )
                        {
                            $limitSize = $icons['maxsize'] * 1048576 ; // in bytes
                            if( $size > $limitSize )
                            {
                                return $this->formatError( $response , '400', NULL , [ "message" => [ 'size' => 'The size of the file is too big.' ] ] ) ;
                            }
                        }

                        $uri = $file->getStream()->getMetadata('uri') ;


                        $finfo = finfo_open( FILEINFO_MIME_TYPE ) ;
                        $check = finfo_file( $finfo , $uri ) ;

                        finfo_close( $finfo ) ;

                        if( $type !== $check )
                        {
                            return $this->formatError( $response , '400', NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' ] ] ) ;
                        }

                        $typesAvailable = NULL ;

                        if( isset( $icons['formats'] ) )
                        {
                            $typesAvailable = $icons['formats'] ;
                        }

                        $maxwidth  = 0 ;
                        $minwidth  = 0 ;

                        $maxheight = 0 ;
                        $minheight = 0 ;

                        if( isset( $icons['maxwidth'] ) && isset( $icons['maxheight'] ) )
                        {
                            $maxwidth  = $icons[ 'maxwidth' ] ;
                            $maxheight = $icons[ 'maxheight' ] ;
                        }

                        if( isset( $icons['minwidth'] ) && isset( $icons['minheight'] ) )
                        {
                            $minwidth  = $icons[ 'minwidth' ] ;
                            $minheight = $icons[ 'minheight' ] ;
                        }

                        if( $typesAvailable && in_array( $type , $typesAvailable ) )
                        {
                            $imgUpload = new Imagick( $uri ) ; // check resolution of the image

                            $width  = $imgUpload->getImageWidth() ;
                            $height = $imgUpload->getImageHeight() ;

                            if( $width < $minwidth || $height < $minheight )
                            {
                                return $this->formatError( $response , '400' , NULL , [ 'message' => [ 'resolution' => 'The resolution of the file is too small.' ] ] );
                            }
                            else if( $width > $maxwidth || $height > $maxheight )
                            {
                                return $this->formatError( $response , '400' , NULL , [ 'message' => [ 'resolution' => 'The resolution of the file is too large.' ] ] );
                            }

                            if( $filename )
                            {
                                $output = $this->getImagePath( $id , TRUE ) ; // lazy calling

                                $file->moveTo( $output ) ;

                                $update = $this->model->update( [ "image" => true ] , $id ) ;

                                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );

                            }
                            else
                            {
                                return $this->formatError( $response , '601' , [ 'patchImage(' . $id . ')' , 'unknow error' ] ) ;
                            }
                        }
                        else
                        {
                            return $this->formatError( $response , '400', NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' ] ] ) ;
                        }
                    }
                    else
                    {
                        return $this->formatError( $response , '400', NULL , [ "message" => [ 'upload' => 'The uploaded file is not valid.' ] ] ) ;
                    }
                }
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'postImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }

    /**
     * Sets the path of the controller.
     * @param string|NULL $value
     */
    public function setPath( string $value = NULL )
    {
        if( isset( $value ) )
        {
            $this->path     = $value ;
            $this->fullPath = '/thesaurus/' . $value ;
            $this->iconPath = $this->config['icons']['root'] . $value ;
            $this->pattern  = '/thesaurus/' . $value . '/%u' ;
        }
        else
        {
            $this->path     = '' ;
            $this->fullPath = '' ;
            $this->iconPath = '' ;
            $this->pattern  = '' ;
        }
    }
}


