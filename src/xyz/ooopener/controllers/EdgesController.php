<?php

namespace xyz\ooopener\controllers ;

use Exception ;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\Validation;

class EdgesController extends Controller
{
    /**
     * Creates a new EdgesController instance.
     * @param ContainerInterface $container
     * @param ?Model         $model
     * @param ?Collections   $owner
     * @param ?Edges         $edge
     * @param ?string        $path
     * @param ?Validation    $validator The optional validator of the controller (by default use a basic Validation instance).
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL ,
        ?Validation        $validator = NULL
    )
    {
        parent::__construct ( $container , $validator );
        $this->edge  = $edge  ;
        $this->model = $model ;
        $this->owner = $owner ;
        $this->path  = $path  ;
    }

    const CREATE_PROPERTIES = [] ;

    /**
     * The edge reference.
     */
    public ?Edges $edge ;

    /**
     * The model reference.
     */
    public ?Model $model ;

    /**
     * The owner model reference.
     */
    public ?Collections $owner ;

    /**
     * The path reference.
     */
    public ?string $path ;

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return mixed
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        return $init ;
    }


    public function getFields( $skin = NULL )
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins  = array_key_exists( 'skins'  , $options ) ? $options['skins']  : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                {
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                }
                case Thing::FILTER_JOIN :
                {
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
                }
            }
        }

        //$this->logger->info( $this . ' getFields ::: ' . json_encode( $fields , JSON_PRETTY_PRINT ) ) ;

        return $fields ;
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'     => TRUE ,
        'conditions' => [] ,
        'facets'     => NULL ,
        'groupBy'    => NULL ,
        'items'      => NULL ,
        'lang'       => NULL ,
        'limit'      => NULL ,
        'offset'     => NULL ,
        'options'    => NULL ,
        'search'     => NULL ,
        'sort'       => NULL ,
        'params'     => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'         => $id ,
            'lang'       => $lang ,
            'sort'       => $sort ,
            'params'     => $params
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
        }

        try
        {
            if( !$this->owner->exist( $id , [ 'fields' => '_key' ] ) )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $api = $this->config['api'] ;
            $set = array_key_exists( $this->path , $this->config ) ? $this->config[ $this->path ] : NULL ;

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            $skin = NULL ;

            // ----- sort

            if( isset( $params[ 'sort' ] ) )
            {
                $sort = $params[ 'sort' ];
            }
            else if( is_null( $sort ) && is_array($set) && array_key_exists( 'sort_default' , $set ) )
            {
                $sort = $set[ 'sort_default' ];
            }

            // ----- skin

            if( !isset($skin) && is_array($set) && array_key_exists( 'skin_default' , $set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( is_array($set) && in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            $items = NULL ;

            try
            {
                $result = $this->edge->getEdge
                (
                    NULL ,
                    $id ,
                    [
                        'skin' => $skin ,
                        'lang' => $lang ,
                        'sort' => $sort
                    ]
                ) ;

                $items = $result->edge ;

                // $this->logger->debug( $this . ' all(' . $id . ') ::: ' . json_encode( $items , JSON_PRETTY_PRINT ) ) ;
            }
            catch( Exception $error )
            {
                $this->logger->warning( $this . ' ' . __FUNCTION__ . ' failed to get the edge result, error: ' . $error->getMessage() ) ;
            }

            if( $items )
            {
                foreach( $items as $key => $value )
                {
                    $items[$key] = $this->container->get( $this->edge->from['controller'] )->create( (object) $value , $lang , $skin ) ;
                }
                $this->container->get( $this->edge->from['controller'] )->sortAfter( $items ) ;
            }

            return $this->success
            (
                $response ,
                $items ,
                $this->getCurrentPath( $request , $params ) ,
                is_array( $items ) ? count( $items ) : NULL ,
                [ 'total' => is_array( $items ) ? count( $items ) : 0 ]
            ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() , $e->getFile() , $e->getLine() ] , NULL , 500 );
        }
    }

    public function allReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'         => $id ,
            'lang'       => $lang ,
            'sort'       => $sort ,
            'params'     => $params
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' allReverse(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
        }

        try
        {
            $model = $this->model->get( $id , [ 'fields' => '_key' ] ) ;

            if( !$model )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $api = $this->config['api'] ;
            $set = array_key_exists( $this->path , $this->config ) ? $this->config[ $this->path ] : NULL ;

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            $skin = NULL ;

            // ----- sort

            if( isset( $params[ 'sort' ] ) )
            {
                $sort = $params[ 'sort' ];
            }
            else if( is_array($set) && is_null( $sort ) )
            {
                $sort = $set[ 'sort_default' ];
            }

            // ----- skin

            if( !isset($skin) && is_array($set) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( is_array($set) && in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            $result = $this->edge->getEdge
            (
                $id ,
                null ,
                [ 'skin' => $skin , 'lang' => $lang , 'sort' => $sort ]
            ) ;

            $items = $result->edge ;

            if( $items )
            {
                foreach( $items as $key => $value )
                {
                    $items[$key] = $this->container
                                        ->get( $this->edge->to['controller'] )
                                        ->create( (object) $value , $lang , $skin ) ;
                }

                $this->container->get( $this->edge->to['controller'] )->sortAfter( $items ) ;
            }

            return $this->success
            (
                $response ,
                $items ,
                $this->getCurrentPath( $request , $params ) ,
                is_array( $items ) ? count( $items ) : NULL ,
                [ 'total' => is_array( $items ) ? count( $items ) : 0 ]
            ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'allReverse(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'count' methods options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'id'      => NULL ,
        'reverse' => FALSE
    ] ;

    // FIXME count return always 0 for the moment
    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $count = 0 ;
        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
            return $this->success( $response , $count , $this->getCurrentPath( $request ) ) ;
        }
        return $count ;
    }

    // FIXME countReverse return always 0 for the moment
    public function countReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $count = 0 ;
        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
            return $this->success( $response , $count , $this->getCurrentPath( $request ) ) ;
        }
        return $count ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ref = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;

            if( !$ref )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            /*if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }*/

            $del    = NULL ;
            $idFrom = $this->model->table . '/' . $id ;
            $idTo   = $this->owner->table . '/' . $owner;

            // delete edge

            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                $del = $this->edge->deleteEdge( $idFrom , $idTo ) ;
            }

            // update owner

            $this->owner->updateDate( $owner ) ;

            // update child

            $this->model->updateDate( $id ) ;

            if( $response )
            {
                return $this->success( $response , $reverse ? (int)$owner : (int)$id );
            }
            else
            {
                return $del ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        //
    }

    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $args['id']      = $owner ;
        $args['owner']   = $id ;
        $args['reverse'] = TRUE ;

        return $this->delete( $request , $response , $args ) ;
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            if( $reverse === FALSE )
            {
                $ref = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$ref )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }
                $all = $this->edge->getEdge( NULL , $owner ) ;
            }
            else
            {
                $ref = $this->model->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$ref )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }
                $all = $this->edge->getEdge( $owner ) ;
            }

            if( $all && $all->edge )
            {
                $unref = FALSE ;

                foreach( $all->edge as $edge ) // delete them all
                {
                    if( $edge['id'] != 0 )
                    {
                        if( $reverse === FALSE )
                        {
                            $setOwner = $owner ;
                            $setID    = (string) $edge['id'] ;
                        }
                        else
                        {
                            $setOwner = (string) $edge['id'] ;
                            $setID    = $owner ;
                        }

                        $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID ] ) ;

                        if( $result instanceof Response && $result->getStatusCode() != 200 )
                        {
                            return $result ;
                        }
                    }
                    else
                    {
                        $unref = TRUE ;
                    }
                }

                if( $unref === TRUE ) // remove all edges
                {
                    if( $reverse === FALSE )
                    {
                        $this->edge->deleteEdgeTo( $this->owner->table . '/' . $owner ) ;
                    }
                    else
                    {
                        $this->edge->deleteEdgeFrom( $this->model->table . '/' . $owner ) ;
                    }
                }
            }

            if( $response )
            {
                return $this->success( $response , 'ok' );
            }
            else
            {
                return true ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'skin'  => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = array_key_exists( $this->path , $this->config) ? $this->config[$this->path] : NULL ;

        $params = NULL ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) && is_array($set) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) && is_array($set) )
            {
                if( in_array( strtolower($params['skin']) , $set['skins'] ) )
                {
                    $params['skin'] = $skin = strtolower($params['skin']) ;
                }
            }
        }

        try
        {
            if( !$this->owner->exist( $id , [ 'fields' => '_key' ] ) )
            {
                return $this->formatError( $response ,'404' , [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->edge->getEdge( NULL , $id , [ 'skin' => $skin ] , $this->getEdgeMock() ) ;

            return $response ? $this->success( $response , $result->edge , $this->getCurrentPath( $request , $params ) )
                             : $result->edge ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ref = $this->owner->get( $owner , [ 'fields' => '_id' ] ) ;
            if( !$ref )
            {
                return $this->formatError( $response ,'404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $this->model->table . '/' . $id ;
            $idTo   = $ref->_id ;

            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            $this->edge->insertEdge( $idFrom , $idTo ) ; // insert edge
            $this->owner->updateDate( $owner ) ; // update owner
            $this->model->updateDate( $id ) ; // update child

            if( $reverse )
            {
                $edgeToController = $this->container->get( $this->edge->to['controller'] ) ;
                $item = $this->owner->get( $owner, [ 'queryFields' => $edgeToController->getFields() ] ) ;
                $result = $edgeToController->create( $item ) ;
            }
            else
            {
                $edgeFromController = $this->container->get( $this->edge->from['controller'] ) ;
                $item = $this->model->get( $id, [ 'queryFields' => $edgeFromController->getFields() ] ) ;
                $result = $edgeFromController->create( $item ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function postReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        $args['id']      = $owner ;
        $args['owner']   = $id ;
        $args['reverse'] = TRUE ;

        return $this->post( $request , $response , $args ) ;
    }

    public function sortAfter( $items )
    {
        $sortable = $this->model->sortable ;

        if( $sortable && array_key_exists( 'after' , $sortable ) )
        {
            $after = explode( '.' , $sortable['after'] ) ;

            if( $after && is_array( $after ) && count( $after ) == 2 )
            {
                usort( $items , function ( $a , $b ) use ( $after )
                {
                    return strcmp( $a->{$after[0]}->{$after[1]} , $b->{$after[0]}->{$after[1]} ) ;
                }) ;
            }
        }
        return $items ;
    }

    // -------- helpers

    protected function deleteAllInController( $controllerID , $id , ?array $args = [] , string $key = 'owner' )
    {
        if( $this->container->has( $controllerID ) )
        {
            if( !is_array( $args ) )
            {
                $args = [] ;
            }
            $this->container->get( $controllerID )->deleteAll( NULL , NULL , array_merge( [ $key => $id ] , $args ) ) ;
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $controllerID . '" definition not exist in the DI container.') ;
        }
    }



    protected function deleteInController( $controllerID , $id , ?array $args = []  )
    {
        if( $this->container->has( $controllerID ) )
        {
            if( !is_array( $args ) )
            {
                $args = [] ;
            }
            $this->container->get( $controllerID )->delete( NULL , NULL , array_merge( [ 'id' => $id ] , $args ) ) ;
        }
        else
        {
            $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed, the "' . $controllerID . '" definition not exist in the DI container.') ;
        }
    }

    protected function getEdgeMock() :?array
    {
        return NULL ;
    }
}
