<?php

namespace xyz\ooopener\controllers\teams ;

use xyz\ooopener\models\Collections;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\TranslationController;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class TeamTranslationController extends TranslationController
{
    /**
     * TeamTranslationController constructor.
     *
     * @param ContainerInterface $container
     * @param Collections $model
     * @param null $path
     * @param string $fields
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function alternateName( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_ALTERNATE_NAME ;
        $args['key'] = 'name' ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        $args['key'] = 'name' ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternateName( Request $request , Response $response , array $args = [] )
    {
        $args['key'] = 'name' ;
        return $this->patchElement( $request , $response , $args , Thing::FILTER_ALTERNATE_NAME ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        $args['key'] = 'name' ;
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }
}
