<?php

namespace xyz\ooopener\controllers\places;

use Exception ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

use core\Maths ;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController;
use xyz\ooopener\controllers\events\EventsController;
use xyz\ooopener\controllers\steps\StepsController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Places;
use xyz\ooopener\models\Stages;
use xyz\ooopener\things\GeoCoordinates;
use xyz\ooopener\things\Place;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\PlaceValidator;

/**
 * The place collection controller.
 */
class PlacesController extends CollectionsController
{
    /**
     * Creates a new PlacesController instance.
     * @param ContainerInterface $container
     * @param ?Places            $model
     * @param string             $path
     */
    public function __construct( ContainerInterface $container , Places $model = NULL , $path = 'places' )
    {
        parent::__construct( $container , $model , $path , new PlaceValidator( $container )  );
    }

    ///////////////////////////

    /**
     * The default 'near' method options.
     */
    const ARGUMENTS_NEAR_DEFAULT =
    [
        'active'    => TRUE ,
        'distance'  => 0 ,
        'facets'    => NULL ,
        'id'        => NULL ,
        'latitude'  => NULL ,
        'longitude' => NULL ,
        'lang'      => NULL ,
        'limit'     => NULL ,
        'offset'    => NULL ,
        'order'     => 'ASC' ,
        'search'    => NULL ,
        'skin'      => NULL ,
        'sort'      => NULL ,
        'timezone'  => NULL ,
        'params'    => NULL
    ] ;

    /**
     * Returns all the elements near a specific geo position (latitude and longitude).
     * Ex: ../places/near
     * Ex: ../places/near/100?skin=main
     * Ex: ../places/near?latitude=43.530961&longitude=5.447295
     * Ex: ../places/near?latitude=43.530961&longitude=5.447295&distance=10000 (10km around the position)
     *
     * @param ?Request $request
     * @param ?Response $response
     * @param array $args array init ex: [ 'limit' => 0 ]
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function near( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [
            'active'    => $active ,
            'distance'  => $distance ,
            'facets'    => $facets ,
            'id'        => $id ,
            'latitude'  => $latitude ,
            'longitude' => $longitude ,
            'lang'      => $lang ,
            'limit'     => $limit ,
            'offset'    => $offset ,
            'order'     => $order ,
            'search'    => $search ,
            'skin'      => $skin ,
            'sort'      => $sort ,
            'timezone'  => $timezone ,
            'params'    => $params
        ]
        = array_merge( self::ARGUMENTS_NEAR_DEFAULT , $args ) ;

        $conf = $this->config ;

        $api = $conf[ 'api' ] ;
        $set = $conf[ $this->path ] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = isset($params) ? $params : $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default', $set) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            // ----- limit

            $limit = intval( isset($limit) ? $limit : $api['limit_default'] ) ;
            if( isset($params['limit']) )
            {
                $limit = filter_var
                (
                    $params['limit'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['limit'] = intval( ($limit !== FALSE) ? $limit : $api['limit_default'] ) ;
            }

            // ----- offset

            $offset = intval( isset($offset) ? $offset : $api['offset_default'] ) ;
            if( isset($params['offset']) )
            {
                $offset = filter_var
                (
                    $params['offset'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['offset'] = intval( ($offset !== FALSE) ? $offset : $api['offset_default'] ) ;
            }

            // ----- facets

            if( isset($params['facets']) )
            {
                try
                {
                    $facets = array_merge
                    (
                        json_decode( $params['facets'] , TRUE ) ,
                        isset($facets) ? $facets : []
                    ) ;
                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' from failed, the facets params failed to decode the json expression: ' . $params['facets'] ) ;
                }
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }

            // ----- order

            if( !empty($params['order']) )
            {
                if( in_array( strtoupper($params['order']) , $api['orders'] ) )
                {
                    $params['order'] = $order = strtoupper($params['order']) ;
                }
            }

            // ----- latitude

            if( isset($params['latitude']) )
            {
                $params['latitude'] = $latitude = filter_var
                (
                    $params['latitude'],
                    FILTER_VALIDATE_FLOAT ,
                    [
                        'options' => [ 'min_range' => 0, 'max_range' => 90 ]
                    ]
                ) ;
                if( $latitude === FALSE )
                {
                    $latitude = NULL ;
                    unset($params['latitude']) ;
                }
            }

            // ----- longitude

            if( isset($params['latitude']) )
            {
                $params['longitude'] = $longitude = filter_var
                (
                    $params['longitude'],
                    FILTER_VALIDATE_FLOAT ,
                    [
                        'options' => [ 'min_range' => -180, 'max_range' => 180 ]
                    ]
                ) ;
                if( $longitude === FALSE )
                {
                    $longitude = NULL ;
                    unset($params['longitude']) ;
                }
            }

            // ----- distance

            if( isset($params['distance']) )
            {
                $params['distance'] = $distance = filter_var
                (
                    $params['distance'],
                    FILTER_VALIDATE_INT ,
                    [
                        'options' => [ 'min_range' => 1 ]
                    ]
                ) ;
                if( $distance === FALSE )
                {
                    $distance = NULL ;
                    unset($params['distance']) ;
                }
            }
        }

        // ------------ ID

        try
        {
            $item = $this->model->get
            (
                $id ,
                [
                    'active' => $active  ,
                    'fields' => 'id,latitude,longitude'
                ]
            ) ;
            if( $item )
            {
                $latitude = floatval($item->latitude) ;
                if( isset($params['latitude']) )
                {
                    unset($params['latitude']) ;
                }

                $longitude = floatval($item->longitude) ;
                if( isset($params['longitude']) )
                {
                    unset($params['longitude']) ;
                }
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'near(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        // ------------

        if( !isset( $latitude ) || $latitude < 0 )
        {
            $latitude = floatval($api['latitude_default']) ;
        }

        if( !isset( $longitude ) || $longitude < -180 || $longitude > 180 )
        {
            $longitude = floatval($api['longitude_default']) ;
        }

        if( !isset( $distance ) || $distance < 0 )
        {
            $longitude = intval($api['distance_default']) ;
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ------------

        if( $response )
        {
            $this->logger->debug( $this . ' near(' . $latitude . ',' . $longitude . ',' . $distance . ')' )  ;
        }

        // ------------

        $items   = NULL  ;
        $options = NULL ;

        // ------------

        try
        {
            $items = $this->model->near
            ([
                'active'      => $active,
                'facets'      => $facets,
                'distance'    => $distance,
                'latitude'    => $latitude,
                'longitude'   => $longitude,
                'limit'       => $limit ,
                'offset'      => $offset ,
                'order'       => $order ,
                'search'      => $search ,
                'queryFields' => $this->getFields( $skin )
            ]) ;

            $options =
            [
                'latitude'  => $latitude ,
                'longitude' => $longitude ,
                'distance'  => $distance ,
                'total'     => (int) $this->model->foundRows()
            ] ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'from' , $e->getMessage() ] , NULL , 500 );
        }

        return $this->outputAll( $request , $response , $items , $params , $options ) ;
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="PlaceList",
     *     description="Entities that have a somewhat fixed, physical extension",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="slogan",ref="#/components/schemas/text"),
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="logo",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="geo",ref="#/components/schemas/GeoCoordinates"),
     *     @OA\Property(property="status",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="capacity",type="integer",description="The total number of individuals that may attend an event or venue"),
     *     @OA\Property(property="numAttendee",type="integer",description="The number of attendee"),
     *     @OA\Property(property="remainingAttendee",type="integer",description="The number of remaining attendee"),
     *     @OA\Property(property="publicAccess",type="boolean",description="A flag to signal that the Place is open to public visitors")
     * )
     *
     * @OA\Schema(
     *     schema="Place",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/PlaceList")},
     *     @OA\Property(property="email",type="array",items=@OA\Items(ref="#/components/schemas/Email")),
     *     @OA\Property(property="telephone",type="array",items=@OA\Items(ref="#/components/schemas/PhoneNumber")),
     *     @OA\Property(property="events",type="array",items=@OA\Items(ref="#/components/schemas/EventList")),
     *     @OA\Property(property="conceptualObjects",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="offers",type="array",items=@OA\Items(ref="#/components/schemas/Offer")),
     *     @OA\Property(property="openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="activities",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="permits",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="prohibitions",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="services",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="containsPlace",type="array",items=@OA\Items(ref="#/components/schemas/PlaceList")),
     *     @OA\Property(property="containedInPlace",type="array",items=@OA\Items(ref="#/components/schemas/PlaceList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     *  )
     *
     * @OA\Response(
     *     response="placeResponse",
     *     description="Result of the place",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Place")
     *     )
     * )
     *
     * @OA\Response(
     *     response="placeListResponse",
     *     description="Result list of places",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/PlaceList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'logo'          => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType'            => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'alternateName'             => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'               => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'slogan'                    => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'text'                      => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,

        'geo'                       => [ 'filter' => Thing::FILTER_DEFAULT     ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT            ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT            ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT            ] ,

        'publicAccess'      => [ 'filter' => Thing::FILTER_DEFAULT            ] ,
        'status'            => [ 'filter' => Thing::FILTER_JOIN               ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'numEvents'                 => [ 'filter' => Thing::FILTER_EDGE_COUNT  ] ,
        'numConceptualObjects'      => [ 'filter' => Thing::FILTER_EDGE_COUNT  ] ,

        'address'                   => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'email'                     => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'telephone'                 => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,

        'notes'                     => [ 'filter' => Thing::FILTER_TRANSLATE , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        //'events'                    => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' ] ] ,
        //'keywords'                  => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' ] ] ,
        //'conceptualObjects'         => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' ] ] ,
        'offers'                    => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'openingHoursSpecification' => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,

        'audios'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'videos' ] ] ,

        'brand'                     => [ 'filter' => Place::FILTER_BRAND     ] ,
        'activities'                => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'activities' , 'compact' ] ] ,
        'permits'                   => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'permits' , 'compact' ] ] ,
        'prohibitions'              => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'prohibitions' , 'compact' ] ] ,
        'services'                  => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'services' , 'compact' ] ] ,
        'containsPlace'             => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'compact' ] ] ,
        'containedInPlace'          => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'compact' ] ] ,
        'websites'                  => [ 'filter' => Thing::FILTER_EDGE      , 'skins' => [ 'full' , 'normal' , 'compact' ] ]
    ];

   /**
    * Creates a new instance.
    * @param ?object $init A generic object to create and populate the new thing.
    * @param ?string $lang The lang optional lang iso code.
    * @param ?string $skin The optional skin mode.
    * @param ?array $params The optional params object.
    * @return ?object
    */
   public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
   {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Place::FILTER_BRAND :
                    {
                        $init->{ $key } = $this->container
                                   ->get( 'placeHasBrandController' )
                                   ->all( NULL , NULL , [ 'id' => (string) $init->id ] ) ;
                        break ;
                    }
                    case Place::FILTER_EVENTS :
                    {
                        if( property_exists( $init , Place::FILTER_EVENTS ) && is_array( $init->{ Place::FILTER_EVENTS } ) && count( $init->{ Place::FILTER_EVENTS } ) > 0 )
                        {
                            $sub = [] ;
                            $events = $this->container->get( EventsController::class ) ;
                            foreach( $init->{ Place::FILTER_EVENTS } as $item )
                            {
                                array_push( $sub , $events->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Place::FILTER_CONTAINS_PLACE :
                    {
                        if( property_exists( $init , Place::FILTER_CONTAINS_PLACE ) && is_array( $init->{ Place::FILTER_CONTAINS_PLACE } ) && count( $init->{ Place::FILTER_CONTAINS_PLACE } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Place::FILTER_CONTAINS_PLACE } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Place::FILTER_CONTAINED_IN_PLACE :
                    {
                        if( property_exists( $init , Place::FILTER_CONTAINED_IN_PLACE ) && is_array( $init->{ Place::FILTER_CONTAINED_IN_PLACE } ) && count( $init->{ Place::FILTER_CONTAINED_IN_PLACE } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Place::FILTER_CONTAINED_IN_PLACE } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Place::FILTER_CONCEPTUAL_OBJECTS :
                    {
                        if( property_exists( $init , Place::FILTER_CONCEPTUAL_OBJECTS ) && is_array( $init->{ Place::FILTER_CONCEPTUAL_OBJECTS } ) && count( $init->{ Place::FILTER_CONCEPTUAL_OBJECTS } ) > 0 )
                        {
                            $sub = [] ;
                            $conceptualObjects = $this->container->get( ConceptualObjectsController::class ) ;
                            foreach( $init->{ Place::FILTER_CONCEPTUAL_OBJECTS } as $item )
                            {
                                array_push( $sub , $conceptualObjects->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'placeEmailsController'       , $id ) ; // emails
        $this->deleteAllInController( 'placeKeywordsController'     , $id ) ; // keywords
        $this->deleteAllInController( 'placeOffersController'       , $id ) ; // offers
        $this->deleteAllInController( 'placeOpeningHoursController' , $id ) ; // openingHours
        $this->deleteAllInController( 'placePhoneNumbersController' , $id ) ; // telephone
        $this->deleteAllInController( 'placeWebsitesController'     , $id ) ; // websites

        // -------- medias

        $this->deleteInController( 'placeAudioController' , $id ) ; // audio
        $this->deleteInController( 'placeImageController' , $id ) ; // image
        $this->deleteInController( 'placeLogoController'  , $id ) ; // logo
        $this->deleteInController( 'placeVideoController' , $id ) ; // video

        $this->deleteAllInController( 'placeAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'placePhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'placeVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        // -------- discover

        $this->deleteAllArrayPropertyValuesInModel( Courses::class , 'discover' , $id ) ; // course.discover
        $this->deleteAllArrayPropertyValuesInModel( Stages::class  , 'discover' , $id ) ; // stage.discover

        // -------- edges

        $this->deleteInEdgeModel( 'placeActivities'              , $id , 'to' ) ; // place.activities
        $this->deleteInEdgeModel( 'placeContainsPlaces'          , $id , 'to' ) ; // place.containsPlace
        $this->deleteInEdgeModel( 'placeHasBrand'                , $id , 'to' ) ; // place.brand
        $this->deleteInEdgeModel( 'placePermits'                 , $id , 'to' ) ; // place.permits
        $this->deleteInEdgeModel( 'placePlacesTypes'             , $id , 'to' ) ; // place.additionalType
        $this->deleteInEdgeModel( 'placeProhibitions'            , $id , 'to' ) ; // place.prohibitions
        $this->deleteInEdgeModel( 'placeServices'                , $id , 'to' ) ; // place.services

        $this->deleteInEdgeModel( 'organizationPlaces'           , $id /*from*/ ) ; // organization.location
        $this->deleteInEdgeModel( 'organizationFoundingLocation' , $id /*from*/ ) ; // organization.foundingLocation
        $this->deleteInEdgeModel( 'placeContainsPlaces'          , $id /*from*/ ) ; // place.containedInPlace
        $this->deleteInEdgeModel( 'stageLocations'               , $id /*from*/ ) ; // stage.location
        $this->deleteInEdgeModel( 'stepLocations'                , $id /*from*/ ) ; // step.location
        $this->deleteInEdgeModel( 'conceptualObjectPlaces'       , $id /*from*/ ) ; // conceptualObject.location
        $this->deleteInEdgeModel( 'eventPlaces'                  , $id /*from*/ ) ; // event.location

        $this->deleteInEdgeModel( 'userFavorites' , $id /*from*/ ) ; // favorites

        // $this->deleteInEdgeAndInController( $id , 'stepLocations'  , StepsController::class  ) ; // steps
    }

    /**
     * @param  ?Request  $request
     * @param  ?Response $response
     * @param  array     $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postPlace",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="status",type="integer",description="The id of the place status"),
     *             required={"name","additionalType","status"},
     *             allOf={
     *                 @OA\Schema(ref="#/components/schemas/PostalAddress"),
     *                 @OA\Schema(ref="#/components/schemas/GeoCoordinates")
     *             },
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = []) :Response
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        $params = $request->getParsedBody() ;

        $item = [];

        $item['active']     = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']       = $this->path ;

        $geo = [] ;
        $address = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['streetAddress'] ) && $params['streetAddress'] != '' )
        {
            $address['streetAddress'] = $params['streetAddress'] ;
        }

        if( isset( $params['additionalAddress'] ) && $params['additionalAddress'] != '' )
        {
            $address['additionalAddress'] = $params['additionalAddress'] ;
        }

        if( isset( $params['addressLocality'] ) && $params['addressLocality'] != '' )
        {
            $address['addressLocality'] = $params['addressLocality'] ;
        }

        if( isset( $params['addressCountry'] ) && $params['addressCountry'] != '' )
        {
            $address['addressCountry'] = $params['addressCountry'] ;
        }

        if( isset( $params['addressDepartment'] ) && $params['addressDepartment'] != '' )
        {
            $address['addressDepartment'] = $params['addressDepartment'] ;
        }

        if( isset( $params['addressRegion'] ) && $params['addressRegion'] != '' )
        {
            $address['addressRegion'] = $params['addressRegion'] ;
        }

        if( isset( $params['postOfficeBoxNumber'] ) )
        {
            $address['postOfficeBoxNumber'] = $params['postOfficeBoxNumber'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'required|max(70)'        ],
            'additionalType' => [ $params['additionalType'] , 'required|additionalType' ],
        ] ;

        if( isset( $params['status'] ) )
        {
            $item['status']       = (int) $params['status'] ;
            $conditions['status'] = [ $params['status'] , 'required|status' ] ;
        }
        else
        {
            // get open status
            $openStatus = $this->container
                               ->get('placesStatusTypes')
                               ->get( 'open' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
            if( $openStatus )
            {
                $item['status'] = (int) $openStatus->_key ;
            }
        }

        if( isset( $params['latitude'] ) )
        {
            $geo['latitude'] = (float) $params['latitude'] ;
            $conditions['latitude'] = [ $params['latitude'] , 'min(-90, number)|max(90,number)' ] ;
        }

        if( isset( $params['longitude'] ) && $params['longitude'] != '' )
        {
            $geo['longitude'] = (float) $params['longitude'] ;
            $conditions['longitude'] = [ $params['longitude'] , 'min(-180,number)|max(180,number)' ];
        }

        if( isset( $params['elevation'] ) && $params['elevation'] != '' )
        {
            $geo['elevation'] = (float) $params['elevation'] ;
            $conditions['elevation'] = [ $params['elevation'] , 'min(-11500,number)|max(8900,number)' ] ;
        }

        if( isset( $params['postalCode'] ) && $params['postalCode'] != '' )
        {
            $address['postalCode'] = $params['postalCode'] ;
            $conditions['postalCode'] = [ $params['postalCode']      , 'postalCode' ] ;
        }

        if( isset( $params['postalCode'] ) && $params['postalCode'] != '' )
        {
            $address['postalCode'] = $params['postalCode'] ;
            $conditions['postalCode'] = [ $params['postalCode']      , 'postalCode' ] ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // Security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $item['address'] = !empty( $address ) ? $address : NULL ;
                $item['geo']     = !empty( $geo )     ? $geo     : NULL ;

                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                $edge = $this->container->get('placePlacesTypes') ;
                $edge->insertEdge( $edge->from['name'] . '/' . $params['additionalType'] , $result->_id ) ;

                return $this->success
                (
                    $response ,
                    $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ])
                );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param ?Request $request
     * @param ?Response $response
     * @param  array $args
     * @return Response
     *
     * @throws \ArangoDBClient\Exception
     * @OA\RequestBody(
     *     request="patchPlace",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="status",type="integer",description="The id of the place status"),
     *
     *             @OA\Property(property="capacity",type="integer",description="The capacity"),
     *             @OA\Property(property="numAttendee",type="integer",description="The number of attendee"),
     *             @OA\Property(property="remainingAttendee",type="integer",description="The remaining of attendee"),
     *             @OA\Property(property="publicAccess",type="boolean",description="A flag to signal that the Place is open to public visitors")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        $additionalType = NULL ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        if( isset( $params['capacity'] ) )
        {
            if( $params['capacity'] != '')
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }
            else
            {
                $item['capacity'] = null;
            }
        }

        if( isset( $params['numAttendee'] ) )
        {
            if( $params['numAttendee'] != '' )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }
            else
            {
                $item['numAttendee'] = null ;
            }
        }

        if( isset( $params['remainingAttendee'] ) )
        {
            if( $params['remainingAttendee'] != '' )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }
            else
            {
                $item['remainingAttendee'] = null ;
            }
        }

        if( isset( $params['publicAccess'] ) )
        {
            if( $params['publicAccess'] !== '' )
            {
                $item['publicAccess'] = (bool) $params['publicAccess'] ;
                $conditions['publicAccess'] = [ $params['publicAccess'] , 'bool' ] ;
            }
            else
            {
                $item['publicAccess'] = null ;
            }
        }

        if( isset( $params['status'] ) )
        {
            $item['status'] = (int) $params['status'] ;
            $conditions['status'] = [ $params['status'] , 'required|status' ] ;
        }


        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edge

                if( $additionalType !== NULL )
                {
                    $edge = $this->container->get('placePlacesTypes') ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                        $edge->insertEdge( $idFrom , $idTo ) ; // add edge
                    }
                }

                $this->model->update( $item , $id );

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param ?Request  $request
     * @param ?Response $response
     * @param array     $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putPlace",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="status",type="integer",description="The id of the place status"),
     *             required={"name","additionalType","status"},
     *             @OA\Property(property="capacity",type="integer",description="The capacity"),
     *             @OA\Property(property="numAttendee",type="integer",description="The number of attendee"),
     *             @OA\Property(property="remainingAttendee",type="integer",description="The remaining of attendee"),
     *             @OA\Property(property="publicAccess",type="boolean",description="A flag to signal that the Place is open to public visitors")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];


        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['status'] ) )
        {
            $item['status'] = (int) $params['status'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']            , 'required|max(70)'        ],
            'additionalType' => [ $params['additionalType']  , 'required|additionalType' ],
            'status'         => [ $params['status']          , 'required|status'         ] ,
        ] ;

        if( isset( $params['capacity'] ) )
        {
            if( $params['capacity'] != '')
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }
            else
            {
                $item['capacity'] = null;
            }
        }

        if( isset( $params['numAttendee'] ) )
        {
            if( $params['numAttendee'] != '' )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }
            else
            {
                $item['numAttendee'] = null ;
            }
        }

        if( isset( $params['remainingAttendee'] ) )
        {
            if( $params['remainingAttendee'] != '' )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }
            else
            {
                $item['remainingAttendee'] = null ;
            }
        }

        if( isset( $params['publicAccess'] ) )
        {
            if( $params['publicAccess'] !== '' )
            {
                $item['publicAccess'] = (bool) $params['publicAccess'] ;
                $conditions['publicAccess'] = [ $params['publicAccess'] , 'bool' ] ;
            }
            else
            {
                $item['publicAccess'] = null ;
            }
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                // update edge
                $edge = $this->container->get('placePlacesTypes') ;

                $idFrom = $edge->from['name'] . '/' . $params['additionalType'] ;
                $idTo = $this->model->table . '/' . $id ;

                if( !$edge->existEdge( $idFrom , $idTo ) )
                {
                    $edge->delete( $idTo , [ 'key' => '_to' ] ) ; // delete all edges to be sure
                    $edge->insertEdge( $idFrom , $idTo ) ; // add edge
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    // ----- Special case to update the steps dependencies

                    if( $this->container->has( StepsController::class ) )
                    {
                        $this->container
                             ->get( StepsController::class )
                             ->updateLocation( $id ) ;
                    }

                    // -----

                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * Outputs all the elements.
     *
     * @param ?Request $request
     * @param ?Response $response
     * @param ?array $items
     * @param ?array $params
     * @param ?array $options
     * @return Response|array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $config = $this->config ;

        $api = $config['api'] ;
        $set = $config[ $this->path ] ;

        $format    = NULL ;
        $lang      = NULL ;
        $latitude  = NULL ;
        $longitude = NULL ;
        $lang      = NULL ;
        $marker    = FALSE ;
        $provider  = NULL ;
        $skin      = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- format

        if( !empty( $params[ 'format' ] ) )
        {
            $format =  $params[ 'format' ] ;
        }

        // ----- lang

        if( !empty( $params['lang'] ) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
        }

        // ----- skin

        if( !isset($skin) && array_key_exists( 'skin_default' , $set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( in_array( strtolower($params['skin']) , $set['skins'] ) )
            {
                $params['skin'] = $skin = strtolower($params['skin']) ;
            }
        }

        if( $skin == 'main' || !in_array( strtolower($skin) , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        if( $format == 'map' )
        {
            $skin = 'map' ;
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            switch( $format )
            {
                case "map" :
                {
                    $map = $config['map'] ;

                    // ----- marker

                    if( isset($params['marker']) )
                    {
                        if( $params['marker'] == 'true' || $params['marker'] == 'TRUE' )
                        {
                            $params['marker'] = $marker = TRUE ;
                        }
                    }

                    // ----- provider

                    $provider = $map['provider_default'] ;
                    if( isset($params['provider']) && !empty($params['provider']) )
                    {
                        if( in_array( $params['provider'] , $map['providers'] ) )
                        {
                            $params['provider'] = $provider = $params['provider'] ;
                        }
                    }

                    // ----- latitude and longitude

                    if( is_array($options)  )
                    {
                        if( array_key_exists('latitude',$options) )
                        {
                            $latitude = $options['latitude'] ;
                            unset($options['latitude']) ;
                        }

                        if( array_key_exists('longitude',$options) )
                        {
                            $longitude = $options['longitude'] ;
                            unset($options['longitude']) ;
                        }
                    }

                    $center = $this->getCenter( $items ) ;

                    if( !isset($latitude) && !isset($longitude) )
                    {
                        if( $center )
                        {
                            $latitude  = $center->latitude ;
                            $longitude = $center->longitude ;
                        }
                        else
                        {
                            $latitude  = $api['latitude_default'] ;
                            $longitude = $api['longitude_default'] ;
                        }
                    }

                    $zoom = $this->getMapZoomLevel
                    (
                        $this->getMaximum( $center , $items ) , $map['zoom_default']
                    );

                    $settings =
                    [
                        'api'         => $config['app']['url'] ,
                        'analytics'   => (bool) $config['useAnalytics'] ,
                        'extra'       => TRUE , // indicates if the extra mode is enabled (gds application only)
                        'interactive' => TRUE ,
                        'latitude'    => $latitude,
                        'longitude'   => $longitude,
                        'distance'    => isset( $options['distance'] ) ? $options['distance'] : 0 ,
                        'marker'      => $marker,
                        'places'      => $items,
                        'provider'    => $provider,
                        'version'     => $config['version'],
                        'zoom'        => $zoom
                    ] ;

                    if( isset($options) )
                    {
                        $settings = array_merge( $settings , $options ) ;
                    }

                    return $this->render( $response , $map['template'] , $settings );
                }
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $items,
                        $this->getUrl( $this->fullPath , $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }

    /**
     * Determinates the center position of all passed-in places.
     *
     * @param $places
     *
     * @return GeoCoordinates|FALSE
     */
    private function getCenter( $places )
    {
        if ( !is_array($places) )
        {
            return FALSE;
        }

        $count = count($places);

        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        foreach( $places as $place )
        {
            $lat = deg2rad( is_array( $place->geo ) ? $place->geo['latitude'] : $place->geo->latitude ) ;
            $lon = deg2rad( is_array( $place->geo ) ? $place->geo['longitude'] : $place->geo->longitude );

            $a = cos($lat) * cos($lon);
            $b = cos($lat) * sin($lon);
            $c = sin($lat);

            $X += $a;
            $Y += $b;
            $Z += $c;
        }

        $X /= $count;
        $Y /= $count;
        $Z /= $count;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        $geo = new GeoCoordinates() ;

        $geo->latitude  = rad2deg($lat) ;
        $geo->longitude = rad2deg($lon) ;

        return $geo ;
    }


    /**
     * Determinates the maximum place distance.
     *
     * @param $center
     * @param $places
     *
     * @return float
     */
    private function getMaximum( $center , $places )
    {
        if ( !is_array($places) )
        {
            return FALSE;
        }

        $distances = [0];

        foreach( $places as $place )
        {
            $distances[] = Maths::haversine( $center->latitude , $center->longitude , is_array( $place->geo ) ? $place->geo['latitude'] : $place->geo->latitude , is_array( $place->geo ) ? $place->geo['longitude'] : $place->geo->longitude ) ;
        }

        return max( $distances ) ;
    }

    /**
     * Indicates the zoom level with a specific distance.
     *
     * @param $distance
     * @param $defaultZoom
     *
     * @return integer
     */
    private function getMapZoomLevel( $distance , $defaultZoom = 0 )
    {
        $max = $distance / 1000 ; // km

        $zoom = $defaultZoom ;

        if( $max < 50 )
        {
            $zoom = 10 ;
        }
        else if( ($max > 50) && ($max <= 200) )
        {
            $zoom = 9 ;
        }
        else if( ($max > 200) && ($max <= 600) )
        {
            $zoom = 8 ;
        }
        else if( ($max > 600) && ($max <= 1000) )
        {
            $zoom = 6 ;
        }
        else if( $max > 1000 )
        {
            $zoom = 5 ;
        }

        return $zoom;
    }
}


