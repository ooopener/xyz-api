<?php

namespace xyz\ooopener\controllers\login ;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\Controller ;
use xyz\ooopener\helpers\CookieHelper ;
use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\Sessions;

use DateTime ;
use Exception ;

use Slim\HttpCache\CacheProvider ;

/**
 * The logout controller
 * @package xyz\ooopener\controllers\oauth
 */
class LogoutController extends Controller
{
    /**
     * Creates a new LogoutController instance.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container );
    }

    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $client_id    = NULL ;
        $redirect_uri = NULL ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;

            $redirect_uri = isset( $params['redirect_uri'] ) ? $params['redirect_uri'] : NULL;

            if( isset( $params['client_id'] ) )
            {
                $client_id = $params['client_id'] ;
            }
        }
        else
        {
            $redirect_uri  = $args['redirect_uri'];
        }

        if( isset( $_SESSION[ $this->config['auth']['session'] ] ) )
        {
            $sessionsModel = $this->container->get( Sessions::class ) ;
            $userInfos     = $this->container->get( UserInfos::class ) ;
            $ip            = $userInfos->getIp() ;
            $user_agent    = $userInfos->getUserAgent() ;
            $user_id       = $_SESSION[ $this->config['auth']['session'] ] ;

            if( isset( $client_id ) )
            {
                try
                {
                    $sessionsModel->logoutAccessToken( $user_id , $client_id , $ip , $user_agent ) ; // logout access token
                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . " " . $e->getMessage() ) ;
                }
            }

            try
            {
                $sessionsModel->logoutIDToken( $user_id , $ip , $user_agent ) ; // logout id token
            }
            catch( Exception $e )
            {
                $this->logger->warning( $this . " " . $e->getMessage() ) ;
            }

            unset( $_SESSION[ $this->config['auth']['session'] ] ); // remove session
        }

        // remove cookie
        $helper = $this->container->get( CookieHelper::class ) ;
        $cookie = $helper->get( $request , $this->config['token']['cookie_name'] ) ;

        if( $cookie ) # expire cookie and web browser will remove it
        {
            $response = $helper->set
            (
                $response ,
                $this->config['token']['cookie_name'] ,
                null ,
                new DateTime( '-5 years' ) ,
                '/'
            );
        }

        $cache    = $this->container->get( CacheProvider::class ) ;
        $firebase = $this->container->get('firebaseConfig') ;
        $response = $cache->denyCache( $response ) ;

        return $this->render
        (
            $response ,
            'login/logout.twig' ,
            [
                'apiKey'            => $firebase['apiKey'],
                'authDomain'        => $firebase['authDomain'],
                'databaseURL'       => $firebase['databaseURL'],
                'storageBucket'     => $firebase['storageBucket'],
                'messagingSenderId' => $firebase['messagingSenderId'],
                'loginUri'          => $this->config['app']['path'] . $this->router->urlFor( 'api.login' ) ,
                'redirectUri'       => $redirect_uri
            ]
        ) ;
    }
}
