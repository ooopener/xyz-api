<?php

namespace xyz\ooopener\controllers\login;

use xyz\ooopener\helpers\Hash;
use xyz\ooopener\helpers\Mailer;
use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\InvitationCodes;
use xyz\ooopener\validations\InviteValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use xyz\ooopener\controllers\Controller ;

use Psr\Container\ContainerInterface;

/**
 * The invite controller.
 */
class InviteController extends Controller
{
    /**
     * Creates a new InviteController instance.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container , new InviteValidator( $container ) );
    }

    /**
     * Get invite
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        try
        {
            $params = $request->getQueryParams() ;

            if( isset( $params['code'] ) )
            {
                $checkInvitation = $this->container
                                        ->get( InvitationCodes::class )
                                        ->check( $params['code'] ) ;

                if( $checkInvitation )
                {
                    $_SESSION['invitation'] = $checkInvitation ;

                    if( $checkInvitation->redirect && $checkInvitation->redirect != '' )
                    {
                        $_SESSION['urlRedirect'] = $checkInvitation->redirect ;
                    }

                    return $this->redirectFor( $response , 'api.login' ) ;
                }
            }

            return $this->render
            (
                $response ,
                'login/invite.twig'
            ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' get', $e->getMessage() ]  , NULL , 500 );
        }

    }

    /**
     * Post invite
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $item = [];

        $container = $this->container ;
        $params    = $request->getParsedBody() ;

        if( isset( $params['team'] ) )
        {
            $item['team'] = $params['team'] ;
        }

        if( isset( $params['email'] ) )
        {
            $item['email'] = $params['email'] ;
        }

        $conditions =
        [
            'team'    => [ $params['team']    , 'required|team' ] ,
            'email'   => [ $params['email']   , 'required|email|userEmail' ]
        ];

        if( isset( $params['person'] ) && $params['person'] != '' )
        {
            $item['person'] = $params['person'] ;
            $conditions['person'] = [ $params['person']  , 'person' ] ;
        }

        if(
            isset( $params['redirect'] )
            && $params['redirect'] != ''
            && property_exists( $container->get('jwt') , 'aud' )
        )
        {
            $item['redirect'] = $params['redirect'] ;
            $conditions['redirect'] = [ $params['redirect']  , 'redirect' ] ;
        }


        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $expire = new \DateTime( "now +" . $this->config['token']['invite_code'] . " seconds" , new \DateTimeZone( 'UTC' ) ) ;

                $item['user']    = $container->get('jwt')->sub ; // get user id
                $item['ip']      = $container->get( UserInfos::class )->getIp() ; // get user ip
                $item['code']    = $container->get(Hash::class)->generateUUID() ; // generate code
                $item['expired'] = $expire->format( "Y-m-d\TH:i:s.v\Z" ) ;
                $item['used']    = 0 ;

                $result = $container->get( InvitationCodes::class )
                                    ->insert( $item );

                if( $result )
                {
                    $item['name'] = $container->get('auth')->getFullNameOrUsername() ; // set name

                    $sending = $container->get( Mailer::class )
                                        ->sendInvitation( $item ) ; // send email

                    if( $sending )
                    {
                        return $this->success( $response , 'ok' );
                    }
                    else
                    {
                        $container->get( InvitationCodes::class )->delete( $result->_key ) ;
                    }

                }

                return $this->error( $response , '' ) ;

            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }
}


