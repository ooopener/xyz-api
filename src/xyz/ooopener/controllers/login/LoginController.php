<?php

namespace xyz\ooopener\controllers\login;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\users\UsersController;
use xyz\ooopener\helpers\CookieHelper;
use xyz\ooopener\helpers\Hash;
use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\InvitationCodes;
use xyz\ooopener\models\People;
use xyz\ooopener\models\Sessions;
use xyz\ooopener\models\Teams;
use xyz\ooopener\models\UserPermissions;
use xyz\ooopener\models\Users;

use Exception ;

use xyz\ooopener\controllers\Controller ;
use xyz\ooopener\things\Session ;
use xyz\ooopener\things\TokenPayload ;

use Firebase\JWT\JWT;
// use Firebase\Auth\Token\Exception\ExpiredToken ;
// use Firebase\Auth\Token\Exception\IssuedInTheFuture ;
// use Firebase\Auth\Token\Exception\InvalidToken ;

use Slim\HttpCache\CacheProvider ;

use DateTime ;
use DateTimeZone ;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * The login controller.
 */
class LoginController extends Controller
{
    /**
     * Creates a new LoginController instance.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container );
    }

    /**
     * Get login
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' get' ) ;

        $cache    = $this->container->get( CacheProvider::class ) ;
        $response = $cache->denyCache( $response ) ; // force remove browser cache

        $firebase = $this->container->get('firebaseConfig');

        return $this->render
        (
            $response ,
            'login/login.twig' ,
            [
                'apiKey'            => $firebase['apiKey'],
                'authDomain'        => $firebase['authDomain'],
                'databaseURL'       => $firebase['databaseURL'],
                'storageBucket'     => $firebase['storageBucket'],
                'messagingSenderId' => $firebase['messagingSenderId']
            ]
        ) ;
    }

    /**
     * Register user and/or create id token
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] ): Response
    {
        $container = $this->container ;

        $params = $request->getParsedBody() ;

        if( !isset( $params['token'] ) || !isset( $params['provider'] ) )
        {
            return $this->formatError( $response , '400' ) ;
        }

        $firebaseToken = $params['token'] ;
        $provider      = json_decode( $params['provider'] ) ;

        try
        {
            $auth = $container->get('firebase') ;

            $verifiedIdToken = $auth->verifyIdToken( $firebaseToken ) ;

            $uid = $verifiedIdToken->claims()->get('sub') ;

            if( $verifiedIdToken->claims()->has( 'name' ) )
            {
                $name = $verifiedIdToken->claims()->get('name') ;
            }
            else if( $provider && property_exists( $provider , 'displayName' ) )
            {
                $name = $provider->displayName ;
            }
            else
            {
                $name = '' ;
            }

            if( $verifiedIdToken->claims()->has( 'picture' ) )
            {
                $picture = $verifiedIdToken->claims()->get('picture') ;
            }
            else
            {
                $picture = null ;
            }

            $email         = $verifiedIdToken->claims()->get('email') ;
            $emailVerified = $verifiedIdToken->claims()->get('email_verified') ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '401', [ 'post()' , $e->getMessage() ]  , NULL , 401 ) ;
        }

        if( $provider->providerId != 'password' || $emailVerified == true )
        {
            try
            {
                $name = explode( ' ' , $name ) ;

                $givenName  = $name[0] ;
                $familyName = '' ;

                if( count( $name ) > 1 )
                {
                    // handle family name
                    array_shift( $name );
                    $name = implode( ' ' , $name );
                    $familyName = $name ;
                }

                $init =
                [
                    'givenName'    => $givenName ,
                    'familyName'   => $familyName ,
                    'email'        => $email ,
                    'uuid'         => $uid ,
                    'provider'     => $provider->providerId,
                    'provider_uid' => $provider->uid,
                    'active'       => 1
                ];

                if( $picture )
                {
                    $init['image'] = $picture ;
                }

                $users = $container->get( Users::class ) ;

                if( !$users->exist( $uid , [ 'key' => 'uuid' ] ) )
                {
                    $invitation        = null ;
                    $invitation_used   = false ;
                    $invitation_person = false ;

                    if( isset( $_SESSION['invitation'] ) )
                    {
                        $invitation = $_SESSION['invitation'] ;

                        if( $invitation )
                        {
                            if( property_exists( $invitation , 'team' ) )
                            {
                                $teamsModel = $container->get( Teams::class ) ;
                                $team = $teamsModel->get( $invitation->team ) ;
                                if( $team && $team->name )
                                {
                                    $init['team'] = $team->name ;
                                    $invitation_used = true ;
                                }
                            }

                            $peopleModel = $container->get( People::class ) ;
                            if( property_exists( $invitation , 'person' ) && $peopleModel->exist( $invitation->person ) )
                            {
                                $init['person'] = $invitation->person ;
                                $invitation_person = true ;
                            }

                        }
                    }

                    $users->insert( $init ) ;

                    if( $invitation && property_exists( $invitation , 'person' ) )
                    {
                        $up =
                        [
                            'user'       => $uid ,
                            'module'     => 'people' ,
                            'resource'   => $invitation->person ,
                            'permission' => 'W'
                        ];

                        // add user permission for the specific person
                        $userPermissionsModel = $container->get( UserPermissions::class ) ;
                        $userPermissionsModel->insert( $up ) ;
                        $invitation_used = true ;
                    }

                    if( $invitation && $invitation_used )
                    {
                        $invitationCodesModel = $container->get( InvitationCodes::class ) ;
                        $invitationCodesModel->used( $invitation->id ) ;
                    }
                }

                if( isset( $_SESSION['invitation'] ) )
                {
                    unset( $_SESSION['invitation'] ) ;
                }

                if( $uid )
                {
                    $_SESSION[ $this->config['auth']['session'] ] = $uid ;

                    // get team/user permissions
                    $user = $container->get( UsersController::class )
                                      ->getUuid( NULL , NULL , [ 'id' => $uid , 'skin' => 'full' ] ) ;

                    $scope = $user->scope ;

                    # create token

                    $now    = new DateTime();
                    $future = new DateTime('now +' . $this->config['token']['live_id_token'] . ' seconds');

                    $hash     = $container->get( Hash::class ) ;
                    $tokenUid = $hash->generateUUID() ;

                    $payload = new TokenPayload();

                    $payload->jti   = $tokenUid ;
                    $payload->iss   = $this->config['app']['url'] ;
                    $payload->sub   = $user->uuid ;
                    $payload->exp   = $future->getTimestamp() ;
                    $payload->iat   = $now->getTimeStamp() ;
                    $payload->scope = json_encode( $scope );

                    $jwtToken = JWT::encode
                    (
                        $payload ,
                        $this->config['token']['key'] ,
                        $this->config['token']['algorithm'][0]
                    );

                    $cookie = $this->container->get( CookieHelper::class ) ;
                    $response = $cookie->set
                    (
                        $response ,
                        $this->config['token']['cookie_name'] ,
                        $jwtToken ,
                        $future ,
                        '/'
                    );

                    $future->setTimezone( new DateTimeZone( 'Etc/UCT' ) ) ;

                    $userInfos = $container->get( UserInfos::class ) ;

                    // save session infos
                    $session = new Session();

                    $session->type     = 'id_token' ;
                    $session->user     = $user->uuid ;
                    $session->token_id = $tokenUid ;
                    $session->ip       = $userInfos->getIp() ;
                    $session->agent    = $userInfos->getUserAgent() ;
                    $session->expired  = $future->format( 'Y-m-d\TH:i:s.v\Z' ) ;

                    $sessionsModel = $container->get( Sessions::class ) ;
                    $sessionsModel->insert( $session );

                    $args = [];
                    if ( array_key_exists( 'urlRedirect' , $_SESSION ) )
                    {
                        $args['redirect'] = $_SESSION['urlRedirect'];
                        unset( $_SESSION['urlRedirect'] );
                    }

                    return $this->success
                    (
                        $response ,
                        TRUE ,
                        $this->getCurrentPath($request) , NULL , $args
                    );
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
            }
        }
        else
        {
            $this->logger->debug( $this . ' email not verified!' ) ;

            return $this->formatError( $response ,"401", NULL  , NULL , 401 );
        }

    }
}