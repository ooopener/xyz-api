<?php

namespace xyz\ooopener\controllers\brands;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\validations\BrandValidator;

use Exception ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Model;
use xyz\ooopener\models\Brands;
use xyz\ooopener\things\Thing;

/**
 * The brands controller.
 */
class BrandsController extends CollectionsController
{
    /**
     * Creates a new BrandsController instance.
     * @param ContainerInterface $container
     * @param Brands $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Brands $model , $path = 'brands' )
    {
        parent::__construct( $container , $model , $path , new BrandValidator( $container ) );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="OrganizationList",
     *     description="An brand such as a school, NGO, corporation, club, etc",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="slogan",ref="#/components/schemas/text"),
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="logo",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="taxID",type="string"),
     *     @OA\Property(property="vatID",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="Organization",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/OrganizationList")},
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="dissolutionDate",type="string",format="date"),
     *     @OA\Property(property="foundingDate",type="string",format="date"),
     *     @OA\Property(property="foundingLocation",ref="#/components/schemas/PlaceList"),
     *     @OA\Property(property="legalForm",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="ape",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="numbers",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationNumber")),
     *     @OA\Property(property="email",type="array",items=@OA\Items(ref="#/components/schemas/Email")),
     *     @OA\Property(property="telephone",type="array",items=@OA\Items(ref="#/components/schemas/PhoneNumber")),
     *     @OA\Property(property="employees",type="array",items=@OA\Items(ref="#/components/schemas/PersonList")),
     *     @OA\Property(property="members",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="providers",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="ows",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="subOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="parentOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="department",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="memberOf",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="brandResponse",
     *     description="Result of the brand",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Organization")
     *     )
     * )
     *
     * @OA\Response(
     *     response="brandListResponse",
     *     description="Result list of brands",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/OrganizationList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'isBasedOn'  => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'logo'  => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,
        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'slogan'         => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'    => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'notes'          => [ 'filter' => Thing::FILTER_TRANSLATE    , 'skins' => [ 'full' , 'compact' ] ] ,
        'text'           => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,

        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'audios'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,
        'websites'  => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'normal' , 'compact' ] ]
    ];

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteInEdgeModel( 'brandHasType' , $id , 'to' ) ; // brand.additionalType

        $this->deleteInController( 'brandAudioController' , $id ) ; // audio
        $this->deleteInController( 'brandImageController' , $id ) ; // image
        $this->deleteInController( 'brandLogoController'  , $id ) ; // logo
        $this->deleteInController( 'brandVideoController' , $id ) ; // video

        $this->deleteAllInController( 'brandAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'brandPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'brandVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'organizationHasBrand' , $id ) ; // organization.brand
        $this->deleteInEdgeModel( 'userFavorites'        , $id ) ; // favorites
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param ?Request $request
     * @param ?Response
     * @param array $args
     * @return Response
     *
     * @throws \ArangoDBClient\Exception
     * @OA\RequestBody(
     *     request="patchOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="legalForm",type="integer",description="The legal form ID"),
     *             @OA\Property(property="ape",type="integer",description="The APE ID"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *             @OA\Property(property="dissolutionDate",type="string",description="The dissolution date",format="date"),
     *             @OA\Property(property="foundingDate",type="string",description="The founding date",format="date"),
     *             @OA\Property(property="foundingLocation",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType   = NULL ;

        $item       = [];
        $conditions =
        [
            'name' => [ $params['name'] , 'required|max(70)' ] ,
        ] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string) $params['additionalType'] ;
            $conditions['additionalType'] = [ $additionalType , 'additionalType' ] ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        if( empty( $item ) )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $to      = $this->model->table . '/' . $id ;
                $hasType = $this->container->get( 'brandHasType' ) ;

                if( $hasType->existEdgeTo( $to ) )
                {
                   $hasType->deleteEdgeTo( $to ) ;
                }

                if( $additionalType !== NULL )
                {
                    $hasType->insertEdge
                    (
                        $hasType->from['name'] . '/' . $additionalType ,
                        $to
                    ) ;
                }

                $result = $this->model->update( $item , $id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'extend' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        $params = $request->getParsedBody() ;

        $additionalType = NULL ;

        $item       = [];
        $conditions = [ 'name' => [ $params['name'] , 'required|max(70)' ] ] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string) $params['additionalType'] ;
            $conditions['additionalType'] = [ $additionalType , 'additionalType' ] ;
        }

        ////// security - remove sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        $this->logger->debug( $this . ' post() :::: ' . json_encode( $conditions , JSON_PRETTY_PRINT ) ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $item['active']     = 1 ;
                $item['withStatus'] = Status::DRAFT ;
                $item['path']       = $this->path ;

                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                if( isset( $additionalType ) )
                {
                    $hasTypes = $this->container->get( 'brandHasType' ) ;
                    $hasTypes->insertEdge( $hasTypes->from['name'] . '/' . $additionalType , $result->_id ) ;
                }

                return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $params         = $request->getParsedBody() ;
        $additionalType = NULL ;
        $item           = [];

        $item       = [];
        $conditions =
        [
            'name' => [ $params['name'] , 'required|max(70)' ] ,
        ] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string) $params['additionalType'] ;
            $conditions['additionalType'] = [ $additionalType , 'additionalType' ] ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;
        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $to      = $this->model->table . '/' . $id ;
                $hasType = $this->container->get( 'brandHasType' ) ;

                if( $hasType->existEdgeTo( $to ) )
                {
                   $hasType->deleteEdgeTo( $to ) ;
                }

                if( $additionalType !== NULL )
                {
                    $hasType->insertEdge
                    (
                        $hasType->from[ Model::NAME ] . '/' . $additionalType ,
                        $to
                    ) ;
                }

                $result = $this->model->update( $item , $id ) ;

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ) ;
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }
}


