<?php

namespace xyz\ooopener\controllers\organizations;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ThingsEdgesController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\IdentifierValidator;


/**
 * The organization numbers controller.
 */
class OrganizationNumbersController extends ThingsEdgesController
{
    /**
     * Creates a new OrganizationNumbersController instance.
     * @param ContainerInterface $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct(
        ContainerInterface $container ,
        ?Model             $model = NULL ,
        ?Collections       $owner = NULL ,
        ?Edges             $edge  = NULL ,
        ?string            $path  = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path , new IdentifierValidator( $container , 'organizationsNumbersTypes' ) );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="OrganizationNumber",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="value",description="The value of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN   ]
    ];

    public function prepare( Request $request = NULL , $params = NULL )
    {
        $params = is_array($params) ? $params : $request->getParsedBody() ;
        $set    = $this->config['organizations-numbers'];

        $item = [];

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['value'] ) )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name'  => [ $params['name']            , 'required|min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'type'  => [ $params['additionalType']  , 'int|required|additionalType'                                        ] ,
            'value' => [ $params['value']           , 'required'                                                           ]
        ];

        $this->conditions = $conditions ;
        $this->item       = $item ;
    }
}

 /**
 * @OA\RequestBody(
 *     request="postOrganizationNumber",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="string",property="value",description=""),
 *             required={"additionalType","value"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *         )
 *     ),
 *     required=true
 * )
 * @OA\RequestBody(
 *     request="putOrganizationNumber",
 *     @OA\MediaType(
 *         mediaType="application/x-www-form-urlencoded",
 *         @OA\Schema(
 *             @OA\Property(type="integer",property="additionalType"),
 *             @OA\Property(type="string",property="value",description=""),
 *             required={"additionalType","value"},
 *             @OA\Property(type="string",property="name",description="The name of the resource"),
 *         )
 *     ),
 *     required=true
 * )
 */