<?php

namespace xyz\ooopener\controllers\organizations;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController;
use xyz\ooopener\controllers\people\PeopleController;
use xyz\ooopener\controllers\places\PlacesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Stages;
use xyz\ooopener\validations\OrganizationValidator;

use Exception ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use core\Maths ;

use xyz\ooopener\things\GeoCoordinates;
use xyz\ooopener\models\Organizations;
use xyz\ooopener\things\Organization;
use xyz\ooopener\things\Thing;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * The organization collection controller.
 */
class OrganizationsController extends CollectionsController
{
    /**
     * Creates a new OrganizationsController instance.
     * @param ContainerInterface $container
     * @param Organizations $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Organizations $model , $path = 'organizations' )
    {
        parent::__construct( $container , $model , $path , new OrganizationValidator( $container ) );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="OrganizationList",
     *     description="An organization such as a school, NGO, corporation, club, etc",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="slogan",ref="#/components/schemas/text"),
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="logo",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="taxID",type="string"),
     *     @OA\Property(property="vatID",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="Organization",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/OrganizationList")},
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="dissolutionDate",type="string",format="date"),
     *     @OA\Property(property="foundingDate",type="string",format="date"),
     *     @OA\Property(property="foundingLocation",ref="#/components/schemas/PlaceList"),
     *     @OA\Property(property="legalForm",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="ape",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="numbers",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationNumber")),
     *     @OA\Property(property="email",type="array",items=@OA\Items(ref="#/components/schemas/Email")),
     *     @OA\Property(property="telephone",type="array",items=@OA\Items(ref="#/components/schemas/PhoneNumber")),
     *     @OA\Property(property="employees",type="array",items=@OA\Items(ref="#/components/schemas/PersonList")),
     *     @OA\Property(property="members",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="providers",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="ows",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="subOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="parentOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="department",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="memberOf",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="organizationResponse",
     *     description="Result of the organization",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Organization")
     *     )
     * )
     *
     * @OA\Response(
     *     response="organizationListResponse",
     *     description="Result list of organizations",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/OrganizationList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'identifier' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'name'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'legalName'  => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'isBasedOn'  => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'logo'  => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,
        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'slogan'         => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'    => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'notes'          => [ 'filter' => Thing::FILTER_TRANSLATE    , 'skins' => [ 'full' , 'compact' ] ] ,
        'text'           => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,

        'location' => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,

        'ape'   => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'taxID' => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'vatID' => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'audios'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,

        'address'   => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'email'     => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' , 'normal' ] ] ,
        'telephone' => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' , 'normal' ] ] ,
        'websites'  => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'numbers'   => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,

        'brand'              => [ 'filter' => Organization::FILTER_BRAND ] ,
        'department'         => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'dissolutionDate'    => [ 'filter' => Thing::FILTER_DATETIME         , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'founder'            => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'foundingDate'       => [ 'filter' => Thing::FILTER_DATETIME         , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'foundingLocation'   => [ 'filter' => Thing::FILTER_EDGE_SINGLE      , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'legalForm'          => [ 'filter' => Thing::FILTER_EDGE_SINGLE      , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'employees'          => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        //'keywords'           => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'members'            => [ 'filter' => Organization::FILTER_MEMBERS   , 'skins' => [ 'full' , 'compact' ] ] ,
        'providers'          => [ 'filter' => Organization::FILTER_PROVIDERS , 'skins' => [ 'full' , 'compact' ] ] ,
        'owns'               => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'subOrganization'    => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'parentOrganization' => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'memberOf'           => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ],
        'sponsor'            => [ 'filter' => Organization::FILTER_SPONSOR   , 'skins' => [ 'full' ] ],

    ];

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Organization::FILTER_BRAND :
                    {
                        $init->{ $key } = $this->container
                                   ->get( 'organizationHasBrandController' )
                                   ->all( NULL , NULL , [ 'id' => (string) $init->id ] ) ;
                        break ;
                    }
                    case Organization::FILTER_EMPLOYEES :
                    {
                        if( property_exists( $init , Organization::FILTER_EMPLOYEES ) && is_array( $init->{ Organization::FILTER_EMPLOYEES } ) && count( $init->{ Organization::FILTER_EMPLOYEES } ) > 0 )
                        {
                            $employees = [] ;
                            $controller = $this->container->get( PeopleController::class ) ;
                            foreach( $init->{ Organization::FILTER_EMPLOYEES } as $item )
                            {
                                array_push( $employees , $controller->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $employees ;
                        }
                        break ;
                    }
                    case Organization::FILTER_FOUNDER :
                    {
                        if( property_exists( $init , Organization::FILTER_FOUNDER ) && is_array( $init->{ Organization::FILTER_FOUNDER } ) && count( $init->{ Organization::FILTER_FOUNDER } ) > 0 )
                        {
                            $founder = [] ;
                            $controller = $this->container->get( PeopleController::class ) ;
                            foreach( $init->{ Organization::FILTER_FOUNDER } as $item )
                            {
                                array_push( $founder , $controller->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $founder ;
                        }
                        break ;
                    }
                    case Organization::FILTER_FOUNDING_LOCATION :
                    {
                        if( property_exists( $init , Organization::FILTER_FOUNDING_LOCATION ) && $init->{ Organization::FILTER_FOUNDING_LOCATION } != NULL )
                        {
                            $controller = $this->container->get( PlacesController::class ) ;
                            $init->{ $key } = $controller->create( (object) $init->{ Organization::FILTER_FOUNDING_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case Organization::FILTER_LOCATION :
                    {
                        if( property_exists( $init , Organization::FILTER_LOCATION ) && $init->{ Organization::FILTER_LOCATION } != NULL )
                        {
                            $controller = $this->container->get( PlacesController::class ) ;
                            $init->{ $key } = $controller->create( (object) $init->{ Organization::FILTER_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case Organization::FILTER_MEMBER_OF :
                    {
                        if( property_exists( $init , Organization::FILTER_MEMBER_OF ) && is_array( $init->{ Organization::FILTER_MEMBER_OF } ) && count( $init->{ Organization::FILTER_MEMBER_OF } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_MEMBER_OF } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_MEMBERS :
                    {
                        if( property_exists( $init , Organization::FILTER_MEMBERS ) )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( 'organizationMembersController' )
                                                   ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;
                        }
                        break;
                    }
                    case Organization::FILTER_OWNS :
                    {
                        if( property_exists( $init , Organization::FILTER_OWNS ) && is_array( $init->{ Organization::FILTER_OWNS } ) && count( $init->{ Organization::FILTER_OWNS } ) > 0 )
                        {
                            $sub = [] ;
                            $conceptualObjectsController = $this->container->get( ConceptualObjectsController::class  ) ;
                            foreach( $init->{ Organization::FILTER_OWNS } as $item )
                            {
                                array_push( $sub , $conceptualObjectsController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_PARENT_ORGANIZATION :
                    {
                        if( property_exists( $init , Organization::FILTER_PARENT_ORGANIZATION ) && is_array( $init->{ Organization::FILTER_PARENT_ORGANIZATION } ) && count( $init->{ Organization::FILTER_PARENT_ORGANIZATION } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_PARENT_ORGANIZATION } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_PROVIDERS :
                    {
                        if( property_exists( $init , Organization::FILTER_PROVIDERS ) )
                        {
                            $init->{ $key } = $this->container
                                                   ->get( 'organizationProvidersController' )
                                                   ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;
                        }
                        break;
                    }
                    case Organization::FILTER_SUB_ORGANIZATION :
                    {
                        if( property_exists( $init , Organization::FILTER_SUB_ORGANIZATION ) && is_array( $init->{ Organization::FILTER_SUB_ORGANIZATION } ) && count( $init->{ Organization::FILTER_SUB_ORGANIZATION } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_SUB_ORGANIZATION } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'organizationEmailsController'       , $id ) ; // emails
        $this->deleteAllInController( 'organizationKeywordsController'     , $id ) ; // keywords
        $this->deleteAllInController( OrganizationNumbersController::class , $id ) ; // numbers
        $this->deleteAllInController( 'organizationPhoneNumbersController' , $id ) ; // telephone
        $this->deleteAllInController( 'organizationWebsitesController'     , $id ) ; // websites

        $this->deleteInController( 'organizationAudioController' , $id ) ; // audio
        $this->deleteInController( 'organizationImageController' , $id ) ; // image
        $this->deleteInController( 'organizationLogoController'  , $id ) ; // logo
        $this->deleteInController( 'organizationVideoController' , $id ) ; // video

        $this->deleteAllInController( 'organizationAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'organizationPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'organizationVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'userFavorites' , $id ) ; // favorites

        $this->deleteInEdgeModel( 'organizationApe'                          , $id , 'to'   ) ; // organization.ape
        $this->deleteInEdgeModel( 'organizationDepartment'                   , $id , 'to'   ) ; // organization.department
        $this->deleteInEdgeModel( 'organizationEmployees'                    , $id , 'to'   ) ; // organization.employees
        $this->deleteInEdgeModel( 'organizationFounder'                      , $id , 'to'   ) ; // organization.founder
        $this->deleteInEdgeModel( 'organizationFoundingLocation'             , $id , 'to'   ) ; // organization.foundingLocation
        $this->deleteInEdgeModel( 'organizationHasBrand'                     , $id , 'to'   ) ; // organization.brand
        $this->deleteInEdgeModel( 'organizationLegalForm'                    , $id , 'to'   ) ; // organization.legalForm
        $this->deleteInEdgeModel( 'organizationMembersOrganizations'         , $id , 'to'   ) ; // organization.members
        $this->deleteInEdgeModel( 'organizationOrganizationsTypes'           , $id , 'to'   ) ; // organization.additionalType
        $this->deleteInEdgeModel( 'organizationPlaces'                       , $id , 'to'   ) ; // organization.location
        $this->deleteInEdgeModel( 'organizationProvidersOrganizations'       , $id , 'to'   ) ; // organization.providers
        $this->deleteInEdgeModel( 'organizationSubOrganizations'             , $id , 'to'   ) ; // organization.parentOrganization

        $this->deleteInEdgeModel( 'organizationHasBrand'                     , $id /*from*/ ) ; // organization.brand
        $this->deleteInEdgeModel( 'organizationMembersOrganizations'         , $id /*from*/ ) ; // organization.membersOf
        $this->deleteInEdgeModel( 'conceptualObjectProductionsOrganizations' , $id /*from*/ ) ; // conceptualObject.productions
        $this->deleteInEdgeModel( 'eventOrganizersOrganizations'             , $id /*from*/ ) ; // event.organizer
        $this->deleteInEdgeModel( 'livestockOrganizations'                   , $id /*from*/ ) ; // livestock.organization
        $this->deleteInEdgeModel( 'medicalLaboratoryOrganizations'           , $id /*from*/ ) ; // medicalLaboratory.organization
        $this->deleteInEdgeModel( 'organizationProvidersOrganizations'       , $id /*from*/ ) ; // organization.providers
        $this->deleteInEdgeModel( 'organizationSubOrganizations'             , $id /*from*/ ) ; // organization.subOrganization

        $this->deleteAllArrayPropertyValuesInModel( Courses::class , 'discover' , $id ) ; // courses.discover
        $this->deleteAllArrayPropertyValuesInModel( Stages::class  , 'discover' , $id ) ; // stages.discover
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;
        $location = NULL ;

        $item = [];

        $item['active']     = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']       = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        if( isset( $params['identifier'] ) )
        {
            if( $params['identifier'] == '' ) $params['identifier'] = NULL ;

            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['location'] ) )
        {
            $location = $params['location'] != '' ? (string)$params['location'] : NULL  ;
        }

        $conditions =
        [
            'name'            => [ $params['name']       , 'required|max(70)'        ] ,
            'additionalType'  => [ $additionalType       , 'required|additionalType' ] ,
            'location'        => [ $location             , 'location'                ]
        ] ;

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
            $conditions['legalName'] = [ $params['legalName'] , 'max(70)' ] ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                // add additionalType edge

                $addTypeEdge = $this->container->get( 'organizationOrganizationsTypes' ) ;
                $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $result->_id ) ;

                // add location edge

                if( $location != NULL )
                {
                    $locEdge = $this->container->get( 'organizationPlaces' ) ;
                    $locEdge->insertEdge( $locEdge->from['name'] . '/' . $location , $result->_id ) ;
                }

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @throws \ArangoDBClient\Exception
     * @OA\RequestBody(
     *     request="patchOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="legalForm",type="integer",description="The legal form ID"),
     *             @OA\Property(property="ape",type="integer",description="The APE ID"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *             @OA\Property(property="dissolutionDate",type="string",description="The dissolution date",format="date"),
     *             @OA\Property(property="foundingDate",type="string",description="The founding date",format="date"),
     *             @OA\Property(property="foundingLocation",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType   = NULL ;
        $foundingLocation = NULL ;
        $legalForm        = NULL ;
        $location         = NULL ;
        $ape              = NULL ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params['location'] ;
                $conditions['location'] = [ $params['location'] , 'location' ] ;
            }
            else
            {
                $location = FALSE ;
            }
        }

        // legal

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
            $conditions['legalName'] = [ $params['legalName'] , 'max(70)' ] ;
        }

        if( isset( $params['legalForm'] ) )
        {
            if( $params['legalForm'] != '' )
            {
                $legalForm = $params['legalForm'] ;
                $conditions['legalForm'] = [ $params['legalForm'] , 'legalForm' ] ;
            }
            else
            {
                $legalForm = FALSE ;
            }
        }

        if( isset( $params['ape'] ) )
        {
            if( $params['ape'] != '' )
            {
                $ape = $params['ape'] ;
                $conditions['ape'] = [ $params['ape'] , 'ape' ] ;
            }
            else
            {
                $ape = FALSE ;
            }
        }

        if( isset( $params['taxID'] ) )
        {
            $item['taxID'] = $params['taxID'] ;
        }

        if( isset( $params['vatID'] ) )
        {
            $item['vatID'] = $params['vatID'] ;
        }

        if( isset( $params['dissolutionDate'] ) )
        {
            if( $params['dissolutionDate'] != '' )
            {
                $item['dissolutionDate'] = $params['dissolutionDate'] ;
                $conditions['dissolutionDate'] = [ $params['dissolutionDate'] , 'date' ] ;
            }
            else
            {
                $item['dissolutionDate'] = null ;
            }
        }

        if( isset( $params['foundingDate'] ) )
        {
            if( $params['foundingDate'] != '' )
            {
                $item['foundingDate'] = $params['foundingDate'] ;
                $conditions['foundingDate'] = [ $params['foundingDate'] , 'date' ] ;
            }
            else
            {
                $item['foundingDate'] = null ;
            }
        }

        if( isset( $params['foundingLocation'] ) )
        {
            if( $params['foundingLocation'] != '' )
            {
                $foundingLocation = $params['foundingLocation'] ;
                $conditions['foundingLocation'] = [ $params['foundingLocation'] , 'location' ] ;
            }
            else
            {
                $foundingLocation = FALSE ;
            }
        }


        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL && $legalForm === NULL && $location === NULL && $foundingLocation === NULL && $ape === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->get( 'organizationOrganizationsTypes' ) ;

                    $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $foundingLocation !== NULL )
                {
                    $fLocEdge = $this->container->get( 'organizationFoundingLocation' ) ;

                    if( $foundingLocation === FALSE )
                    {
                        // delete all edges
                        $fLocEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $fLocEdge->from['name'] . '/' . $foundingLocation ;

                        if( !$fLocEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $fLocEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $fLocEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $legalForm !== NULL )
                {
                    $legalFormEdge = $this->container->get( 'organizationLegalForm' ) ;

                    if( $legalForm === FALSE )
                    {
                        // delete all edges
                        $legalFormEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $legalFormEdge->from['name'] . '/' . $legalForm ;

                        if( !$legalFormEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $legalFormEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $legalFormEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $location !== NULL )
                {
                    $locEdge = $this->container->get( 'organizationPlaces' ) ;

                    if( $location === FALSE )
                    {
                        // delete all edges
                        $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $locEdge->from['name'] . '/' . $location ;

                        if( !$locEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $locEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $ape !== NULL )
                {
                    $apeEdge = $this->container->get( 'organizationApe' ) ;

                    if( $ape === FALSE )
                    {
                        // delete all edges
                        $apeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $apeEdge->from['name'] . '/' . $ape ;

                        if( !$apeEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $apeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $apeEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                $result = $this->model->update( $item , $id ) ;

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'extend' ) ] ) );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;
        $location = NULL ;

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
        }

        if( isset( $params['identifier'] ) )
        {
            if( $params['identifier'] == '' ) $params['identifier'] = NULL ;

            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['taxID'] ) )
        {
            $item['taxID'] = $params['taxID'] ;
        }

        if( isset( $params['vatID'] ) )
        {
            $item['vatID'] = $params['vatID'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']       , 'required|max(70)'        ] ,
            'additionalType' => [ $additionalType       , 'required|additionalType' ]
        ] ;

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
            $conditions['legalName'] = [ $params['legalName'] , 'max(70)'] ;
        }

        if( isset( $params['location'] ) && $params['location'] != '' )
        {
            $location = $params[ 'location' ] ;
            $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ] ;
        }
        else
        {
            $location = FALSE ;
        }

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ; // security - remove sensible fields
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }


                $addTypeEdge = $this->container->get( 'organizationOrganizationsTypes' ) ; // update edges

                $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;
                $idTo = $this->model->table . '/' . $id ;

                if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                {
                    // delete all edges to be sure
                    $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    // add edge
                    $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                }

                $this->updateEdge( $location , $idTo , $this->container->get( 'organizationPlaces' ) ) ;

                $result = $this->model->update( $item , $id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * Outputs all the elements.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param null $items
     * @param null $params
     * @param array|null $options
     *
     * @return Response|array
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->config['api'] ;
        $set = $this->config[$this->path] ;

        $lang = NULL ;
        $skin = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- lang

        if( !empty($params['lang']) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
        }

        // ----- skin

        if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( in_array( $params['skin'] , $set['skins'] ) )
            {
                $params['skin'] = $skin = $params['skin'] ;
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            $format = isset( $request ) ? $request->getAttribute( 'format' ) : null ;

            switch( $format )
            {
                case "map" :
                {
                    $map  = $this->config['map'] ;
                    $skin = 'map' ;

                    $latitude  = NULL ;
                    $longitude = NULL ;
                    $marker    = FALSE ;
                    $provider  = NULL ;

                    // ----- marker

                    if( isset($params['marker']) )
                    {
                        if( $params['marker'] == 'true' || $params['marker'] == 'TRUE' )
                        {
                            $params['marker'] = $marker = TRUE ;
                        }
                    }

                    // ----- provider

                    $provider = $map['provider_default'] ;
                    if( isset($params['provider']) && !empty($params['provider']) )
                    {
                        if( in_array( $params['provider'] , $map['providers'] ) )
                        {
                            $params['provider'] = $provider = $params['provider'] ;
                        }
                    }

                    // ----- latitude and longitude

                    if( is_array($options)  )
                    {
                        if( array_key_exists('latitude',$options) )
                        {
                            $latitude = $options['latitude'] ;
                            unset($options['latitude']) ;
                        }

                        if( array_key_exists('longitude',$options) )
                        {
                            $longitude = $options['longitude'] ;
                            unset($options['longitude']) ;
                        }
                    }

                    $center = $this->getCenter( $items ) ;

                    if( !isset($latitude) && !isset($longitude) )
                    {
                        if( $center )
                        {
                            $latitude  = $center->latitude ;
                            $longitude = $center->longitude ;
                        }
                        else
                        {
                            $latitude  = $api['latitude_default'] ;
                            $longitude = $api['longitude_default'] ;
                        }
                    }

                    $zoom = $this->getMapZoomLevel
                    (
                        $this->getMaximum( $center , $items ) , $map['zoom_default']
                    );

                    $settings =
                    [
                        'api'           => $this->config['app']['url'] ,
                        'analytics'     => (bool) $this->config['useAnalytics'] ,
                        'extra'         => TRUE , // indicates if the extra mode is enabled (gds application only)
                        'interactive'   => TRUE ,
                        'latitude'      => $latitude,
                        'longitude'     => $longitude,
                        'distance'      => isset( $options['distance'] ) ? $options['distance'] : 0 ,
                        'marker'        => $marker,
                        'organizations' => $items,
                        'provider'      => $provider,
                        'version'       => $this->config['version'],
                        'zoom'          => $zoom
                    ] ;

                    if( isset($options) )
                    {
                        $settings = array_merge( $settings , $options ) ;
                    }

                    return $this->render( $response , $map['template'] , $settings );
                }
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $items,
                        $this->getUrl( $this->fullPath , $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }


    /**
     * Determines the center position of all passed-in organizations.
     * @param $organizations
     * @return GeoCoordinates|FALSE
     */
    private function getCenter( $organizations )
    {
        if ( !is_array($organizations) )
        {
            return FALSE;
        }

        $count = count($organizations);

        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        foreach( $organizations as $organization )
        {
            $lat = deg2rad( $organization->geo->latitude ) ;
            $lon = deg2rad( $organization->geo->longitude );

            $a = cos($lat) * cos($lon);
            $b = cos($lat) * sin($lon);
            $c = sin($lat);

            $X += $a;
            $Y += $b;
            $Z += $c;
        }

        $X /= $count;
        $Y /= $count;
        $Z /= $count;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        $geo = new GeoCoordinates() ;

        $geo->latitude  = rad2deg($lat) ;
        $geo->longitude = rad2deg($lon) ;

        return $geo ;
    }


    /**
     * Determines the maximum organization distance.
     *
     * @param $center
     * @param $organizations
     *
     * @return float
     */
    private function getMaximum( $center , $organizations )
    {
        if ( !is_array($organizations) )
        {
            return FALSE;
        }

        $distances = [0];

        foreach( $organizations as $organization )
        {
            $distances[] = Maths::haversine( $center->latitude , $center->longitude , $organization->geo->latitude , $organization->geo->longitude ) ;
        }

        return max( $distances ) ;
    }

    /**
     * Indicates the zoom level with a specific distance.
     *
     * @param $distance
     * @param $defaultZoom
     *
     * @return integer
     */
    private function getMapZoomLevel( $distance , $defaultZoom = 0 )
    {
        $max = $distance / 1000 ; // km

        $zoom = $defaultZoom ;

        if( $max < 50 )
        {
            $zoom = 10 ;
        }
        else if( ($max > 50) && ($max <= 200) )
        {
            $zoom = 9 ;
        }
        else if( ($max > 200) && ($max <= 600) )
        {
            $zoom = 8 ;
        }
        else if( ($max > 600) && ($max <= 1000) )
        {
            $zoom = 6 ;
        }
        else if( $max > 1000 )
        {
            $zoom = 5 ;
        }

        return $zoom;
    }
}


