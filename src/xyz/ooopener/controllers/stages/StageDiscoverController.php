<?php

namespace xyz\ooopener\controllers\stages ;

use xyz\ooopener\controllers\DiscoverController;
use xyz\ooopener\models\Collections;

use xyz\ooopener\validations\StageValidator;

use Psr\Container\ContainerInterface;

class StageDiscoverController extends DiscoverController
{
    public function __construct
    (
        ContainerInterface    $container ,
        Collections           $model     = NULL ,
        string                $path      = NULL ,
        string                $owner     = NULL ,
        ?array                $types     = NULL
    )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $path ,
            $owner ,
            new StageValidator( $container , $types )
        );
    }
}
