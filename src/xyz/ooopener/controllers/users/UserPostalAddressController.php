<?php

namespace xyz\ooopener\controllers\users ;

use xyz\ooopener\controllers\PostalAddressController;
use xyz\ooopener\models\Users;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

class UserPostalAddressController extends PostalAddressController
{
    /**
     * Creates a new UserPostalAddressController instance.
     * @param ContainerInterface $container
     * @param Users|null $model
     */
    public function __construct( ContainerInterface $container , Users $model = NULL )
    {
        parent::__construct( $container , $model );
    }

    public function getForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->get( $request , $response , $args ) ;
    }

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['key'] = 'uuid' ;
        return parent::get( $request , $response , $args ) ;
    }

    public function patchForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $jwt = $this->container->get('jwt') ;
        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }
        return $this->patch( $request , $response , $args ) ;
    }

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['key'] = 'uuid' ;
        return parent::patch( $request , $response , $args ) ;
    }
}
