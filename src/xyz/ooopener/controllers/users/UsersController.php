<?php

namespace xyz\ooopener\controllers\users;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\livestocks\LivestocksController;
use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController;
use xyz\ooopener\controllers\people\PeopleController;
use xyz\ooopener\controllers\teams\TeamsController;
use xyz\ooopener\controllers\technicians\TechniciansController;
use xyz\ooopener\controllers\veterinarians\VeterinariansController;

use xyz\ooopener\models\Articles ;
use xyz\ooopener\models\Brands ;
use xyz\ooopener\models\ConceptualObjects ;
use xyz\ooopener\models\Courses ;
use xyz\ooopener\models\Diseases;
use xyz\ooopener\models\Events ;
use xyz\ooopener\models\Livestocks;
use xyz\ooopener\models\MedicalLaboratories;
use xyz\ooopener\models\Model;
use xyz\ooopener\models\Observations ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;
use xyz\ooopener\models\Stages ;
use xyz\ooopener\models\Sessions ;
use xyz\ooopener\models\Teams;
use xyz\ooopener\models\Technicians;
use xyz\ooopener\things\Thing;
use xyz\ooopener\models\Users;
use xyz\ooopener\models\Veterinarians;

use xyz\ooopener\validations\UserValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;

use Exception ;
use Slim\HttpCache\CacheProvider;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * The admin users controller.
 */
class UsersController extends CollectionsController
{
    /**
     * Creates a new UsersController instance.
     * @param ContainerInterface $container
     * @param Users $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Users $model , $path = 'users' )
    {
        parent::__construct
        (
            $container ,
            $model ,
            $path ,
            new UserValidator( $container , self::FAVORITES )
        );
    }

    /**
     * The enumeration of all favorites url types to find the good things in the database with a specific model.
     * This enumeration is passed-in the UserValidator constructor.
     */
    const FAVORITES =
    [
        'articles'                => [ Model::NAME => Articles::class            , Model::TABLE => 'articles'            ] ,
        'brands'                  => [ Model::NAME => Brands::class              , Model::TABLE => 'brands'              ] ,
        'conceptualObjects'       => [ Model::NAME => ConceptualObjects::class   , Model::TABLE => 'conceptualObjects'   ] ,
        'courses'                 => [ Model::NAME => Courses::class             , Model::TABLE => 'courses'             ] ,
        'diseases'                => [ Model::NAME => Diseases::class            , Model::TABLE => 'diseases'            ] ,
        'events'                  => [ Model::NAME => Events::class              , Model::TABLE => 'events'              ] ,
        'medicalLaboratories'     => [ Model::NAME => MedicalLaboratories::class , Model::TABLE => 'medicalLaboratories' ] ,
        'livestocks/observations' => [ Model::NAME => Observations::class        , Model::TABLE => 'observations'        ] ,
        'livestocks'              => [ Model::NAME => Livestocks::class          , Model::TABLE => 'livestocks'          ] ,
        'organizations'           => [ Model::NAME => Organizations::class       , Model::TABLE => 'organizations'       ] ,
        'people'                  => [ Model::NAME => People::class              , Model::TABLE => 'people'              ] ,
        'places'                  => [ Model::NAME => Places::class              , Model::TABLE => 'places'              ] ,
        'stages'                  => [ Model::NAME => Stages::class              , Model::TABLE => 'stages'              ] ,
        'technicians'             => [ Model::NAME => Technicians::class         , Model::TABLE => 'technicians'         ] ,
        'veterinarians'           => [ Model::NAME => Veterinarians::class       , Model::TABLE => 'veterinarians'       ]
    ];

    /**
     * The enumeration of all special favorites things to populate with a specific controller.
     */
    const SPECIAL_FAVORITES =
    [
        'livestocks'          => LivestocksController::class ,
        'medicalLaboratories' => MedicalLaboratoriesController::class ,
        'people'              => PeopleController::class ,
        'technicians'         => TechniciansController::class ,
        'veterinarians'       => VeterinariansController::class
    ];

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="Resource UUID",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Schema(
     *     schema="UserList",
     *     description="An user",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="uuid",type="string"),
     *
     *     @OA\Property(property="team",ref="#/components/schemas/TeamList"),
     *     @OA\Property(property="person",ref="#/components/schemas/PersonList"),
     *
     *     @OA\Property(property="givenName",type="string"),
     *     @OA\Property(property="familyName",type="string"),
     *
     *     @OA\Property(property="gender",ref="#/components/schemas/Thesaurus"),
     *
     *     @OA\Property(property="email",type="string"),
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="provider",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="User",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/UserList")},
     * )
     *
     * @OA\Response(
     *     response="userResponse",
     *     description="Result of the user",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/User")
     *     )
     * )
     *
     * @OA\Response(
     *     response="userListResponse",
     *     description="Result list of users",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/UserList"))
     *     )
     * )
     *
     */
    const CREATE_PROPERTIES =
    [
        'id'       => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'     => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'      => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'  => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified' => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'image'    => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,

        'uuid'        => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'team'        => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'person'      => [ 'filter' =>  Thing::FILTER_JOIN     ] ,
        'permissions' => [ 'filter' =>  Thing::FILTER_DEFAULT  , 'skins' => [ 'full' ] ] ,

        'familyName'   => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,
        'givenName'    => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,
        'gender'       => [ 'filter' =>  Thing::FILTER_JOIN    ] ,
        'address'      => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,
        'email'        => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,
        'provider'     => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,
        'provider_uid' => [ 'filter' =>  Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ]
    ];

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            $jwt = $this->container->get('jwt') ;

            if( $jwt && $jwt->sub && $jwt->sub == $init->uuid )
            {
                $init->url = $this->getBaseURL() . 'me';

                if( $skin == 'full' )
                {
                    $favorites = $this->getFavorites() ;

                    if( is_array( $favorites ) && !empty( $favorites ) )
                    {
                        $init->favorites = $favorites ;
                    }
                }
            }
            else
            {
                $init->url = $this->getBaseURL() . 'users/' . $init->uuid;
            }

            $skinFull = $skin == 'full';

            $team = $this->container
                         ->get( TeamsController::class )
                         ->getTeam( NULL , NULL ,
                         [
                             'id'   => $init->team ,
                             'lang' => $lang ,
                             'skin' => $skinFull ? 'full' : ''
                         ]);

            $init->team = $team;

            //////////////////////

            if( $skinFull )
            {
                $scope =
                [
                    "app" => "read" , // get default permission for app
                    "me"  => "admin" // get default permission for account
                ];

                // get team permissions
                if( property_exists($team , 'permissions') && is_countable($team->permissions) && count($team->permissions) > 0 )
                {
                    foreach( $team->permissions as $permission )
                    {
                        $resource = ( $permission[ 'resource' ] != "0" ) ? '/' . $permission[ 'resource' ] : '' ;
                        $path     = $permission[ 'module' ] . $resource ;

                        switch( $permission[ 'permission' ] )
                        {
                            case 'A':
                                $scope[ $path ] = 'admin';
                                break;
                            case 'W':
                                $scope[ $path ] = 'write';
                                break;
                            case 'R':
                                $scope[ $path ] = 'read';
                                break;
                        }
                    }
                }

                // get user permissions
                if( property_exists($init , 'permissions') && is_countable($init->permissions) && count($init->permissions) > 0 )
                {
                    foreach( $init->permissions as $permission )
                    {
                        $resource = ( $permission[ 'resource' ] != "0" ) ? '/' . $permission[ 'resource' ] : '' ;
                        $path     = $permission[ 'module' ] . $resource ;

                        switch( $permission[ 'permission' ] ) // FIXME create a function
                        {
                            case 'A':
                                $scope[ $path ] = 'admin';
                                break;
                            case 'W':
                                $scope[ $path ] = 'write';
                                break;
                            case 'R':
                                $scope[ $path ] = 'read';
                                break;
                        }
                    }
                }

                $init->scope = $scope;

                //unset($init->permissions);
            }
        }

        return $init ;
    }

    public function deleteFavorites( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' deleteFavorites()' ) ;
        }

        $jwt = $this->container->get('jwt') ; // get current user

        if( $jwt && $jwt->sub )
        {
            $uuid = $jwt->sub;

            $params = $request->getParsedBody();

            $url = NULL;

            if( isset($params[ 'url' ]) )
            {
                $url = $params[ 'url' ];
            }

            $conditions = [ 'url' => [ $params[ 'url' ] , 'required|url' ] ];

            $this->validator->validate( $conditions );

            if( $this->validator->passes() )
            {
                try
                {
                    $appUrl        = $this->config[ 'app' ][ 'url' ];
                    $userFavorites = $this->container->get( 'userFavorites' ) ;
                    $item          = $this->model->get( $uuid , [ 'key' => 'uuid' , 'fields' => '_id' ] ) ;

                    $urlPath = substr($url , strlen($appUrl)); // get url path
                    $array   = explode( '/' , $urlPath ) ;
                    $id      = array_pop( $array ) ;
                    $type    = implode( '/' , $array ) ;

                    if(
                           isset( self::FAVORITES[$type] )
                        && isset( self::FAVORITES[$type][Model::TABLE] )
                    )
                    {
                        $type = self::FAVORITES[$type][Model::TABLE] ;
                    }

                    $path = $type . '/' . $id ;

                    if( $userFavorites->existEdge( $path , $item->_id ) )
                    {
                        $userFavorites->deleteEdge( $path , $item->_id ) ;
                    }

                    if( $response )
                    {
                        $items = $this->getFavorites() ;
                        return $this->success( $response , $items ) ;
                    }

                }
                catch( Exception $e )
                {
                    return $this->formatError($response , '500' , [ 'deleteFavorites()' , $e->getMessage() ] , NULL , 500);
                }

            }
            else
            {
                $errors = [];

                $err = $this->validator->errors();
                $keys = $err->keys();

                foreach( $keys as $key )
                {
                    $errors[ $key ] = $err->first($key);
                }

                return $this->error($response , $errors , "400");
            }
        }

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;
    }

    const ARGUMENTS_FAVORITES_DEFAULT =
    [
        'facets'  => NULL ,
        'lang'    => NULL ,
        'limit'   => 0 ,
        'offset'  => NULL ,
        'params'  => NULL ,
        'search'  => NULL ,
        'sort'    => NULL
    ] ;

    public function getFavorites( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'facets'  => $facets ,
            'lang'    => $lang ,
            'limit'   => $limit ,
            'offset'  => $offset ,
            'params'  => $params ,
            'search'  => $search ,
            'sort'    => $sort
        ]
        = array_merge( self::ARGUMENTS_FAVORITES_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' ' . __FUNCTION__ ) ;
        }

        $jwt = $this->container->get('jwt') ; // get current user

        if( $jwt && $jwt->sub )
        {
            $id = $jwt->sub ;

            $api = $this->config['api'] ;
            $set = $this->config['users/favorites'] ;

            if( isset( $request ) )
            {
                $params = isset( $params ) ? $params : $request->getQueryParams();

                // ----- lang

                if( !empty($params['lang']) )
                {
                    if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                    {
                        $params['lang'] = $lang = strtolower($params['lang']) ;
                    }
                    else if( strtolower($params['lang']) == 'all' )
                    {
                        $lang = NULL ;
                    }

                }

                // ----- limit

                $limit = intval( isset( $limit ) ? $limit : $api[ 'limit_default' ] );
                if( isset( $params[ 'limit' ] ) )
                {
                    $limit             = filter_var
                    (
                        $params[ 'limit' ] ,
                        FILTER_VALIDATE_INT ,
                        [
                            'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                        ]
                    );
                    $params[ 'limit' ] = intval( ( $limit !== FALSE ) ? $limit : $api[ 'limit_default' ] );
                }

                // ----- offset

                $offset = intval( isset( $offset ) ? $offset : $api[ 'offset_default' ] );
                if( isset( $params[ 'offset' ] ) )
                {
                    $offset = filter_var
                    (
                        $params[ 'offset' ] ,
                        FILTER_VALIDATE_INT ,
                        [
                            'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                        ]
                    );
                    $params[ 'offset' ] = intval( ( $offset !== FALSE ) ? $offset : $api[ 'offset_default' ] );
                }

                // ----- facets

                if( isset( $params[ 'facets' ] ) )
                {

                    try
                    {
                        if( is_string( $params['facets'] ) )
                        {
                            $facets = array_merge
                            (
                                json_decode( $params[ 'facets' ] , TRUE ) ,
                                isset( $facets ) ? $facets : []
                            );
                        }
                        else
                        {
                            $facets = $params['facets'] ;
                        }

                    }
                    catch( Exception $e )
                    {
                        $this->logger->warning( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                    }
                }

                // ----- sort

                if( isset( $params[ 'sort' ] ) )
                {
                    $sort = $params[ 'sort' ];
                }
                else if( is_null( $sort ) )
                {
                    $sort = $set[ 'sort_default' ];
                }

                // ----- search

                if( isset( $params[ 'search' ] ) )
                {
                    $search = $params[ 'search' ];
                }
            }

            try
            {
                $init =
                [
                    'facets'      => $facets ,
                    'key'         => 'uuid' ,
                    'lang'        => $lang ,
                    'limit'       => $limit ,
                    'offset'      => $offset ,
                    'search'      => $search ,
                    'sort'        => $sort
                ] ;

                $result = $this->model->getFavorites( $id , $init ) ;

                $items = $this->populateSpecialFavorites( $result->favorites ) ;
                $total = $result->total ;

                if( $response )
                {
                    $options = [ 'total' => $total ] ;

                    if( $limit != 0 )
                    {
                        $options['limit']  = $limit ;
                        $options['offset'] = $offset ;
                    }

                    return $this->success
                    (
                        $response ,
                        $items ,
                        $this->getCurrentPath( $request , $params ) ,
                        is_array( $items ) ? count( $items ) : NULL ,
                        $options
                    ) ;
                }

                return $items ;
            }
            catch( Exception $e )
            {
                if( $response )
                {
                    return $this->formatError( $response , '500', [ 'getFavorites()' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;
    }

    public function getForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        $args['skin'] = 'full' ;

        return $this->getUuid( $request , $response , $args ) ;
    }

    /**
     * Get the current user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     *
     */
    public function getUser( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            try
            {
                $user = $this->model->get( $jwt->sub , [ 'key' => 'uuid' , 'queryFields' => $this->getFields() ] ) ;

                if( $user )
                {
                    $user = $this->create( $user ) ;

                    return $this->success( $response , $user ) ;
                }
                else
                {
                    return $this->error( $response , "no user" , '404' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->error( $response , "error with token" , '400' ) ;
        }
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_GET_USERNAME_DEFAULT =
    [
        'active'   => TRUE ,
        'id'       => NULL ,
        'item'     => NULL ,
        'lang'     => NULL ,
        'params'   => NULL ,
        'skin'     => NULL
    ] ;

    /**
     * Get a specific item reference.
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return mixed
     */
    public function getUuid( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active'   => $active ,
            'id'       => $id ,
            'item'     => $item ,
            'lang'     => $lang ,
            'params'   => $params ,
            'skin'     => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_USERNAME_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ init

        $api = $this->config['api'] ;
        $set = $this->config[$this->path] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ----------------

        try
        {
            $init =
            [
                'key'         => 'uuid' ,
                'active'      => $active ,
                'queryFields' => $this->getFields( $skin )
            ] ;

            $item = $this->model->get( $id , $init ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang , $skin , $params ) ;
            }
        }
        catch( Exception $e )
        {
            if( $response )
            {
                return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
            else
            {
                return null ;
            }
        }

        if( $response )
        {
            if( $item )
            {
                // add header
                $cache = $this->container->get( CacheProvider::class ) ;
                $response = $cache->withLastModified( $response , $item->modified );

                return $this->success( $response, $item, $this->getCurrentPath( $request , $params ) );
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    public function postFavorites( Request $request = NULL , Response $response = NULL , array $args = [] ): ?Response
    {
        $url = NULL;

        $params = $request->getParsedBody();
        if( isset($params[ 'url' ]) )
        {
            $url = $params[ 'url' ];
        }

        if( $response )
        {
            $this->logger->debug( $this . ' postFavorites(' . $url . ')' );
        }

        // get current user
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
           $uuid = $jwt->sub;

            $conditions = [ 'url' => [ $url , 'required|url' ] ];

            $this->validator->validate( $conditions );

            if( $this->validator->passes() )
            {
                try
                {
                    $appUrl        = $this->config[ 'app' ][ 'url' ];
                    $userFavorites = $this->container->get( 'userFavorites' ) ;
                    $item          = $this->model->get( $uuid , [ 'key' => 'uuid' , 'fields' => '_id' ] ) ;

                    $urlPath = substr($url , strlen($appUrl)); // get url path
                    $array   = explode( '/' , $urlPath ) ;
                    $id      = array_pop( $array ) ;
                    $type    = implode( '/' , $array ) ;

                    if(
                           isset( self::FAVORITES[$type] )
                        && isset( self::FAVORITES[$type][Model::TABLE] )
                    )
                    {
                        $type = self::FAVORITES[$type][Model::TABLE] ;
                    }

                    $path = $type . '/' . $id ;

                    if( !$userFavorites->existEdge( $path , $item->_id ) )
                    {
                        $userFavorites->insertEdge( $path , $item->_id ) ;
                    }

                    if( $response )
                    {
                        $items = $this->getFavorites() ;

                        return $this->success( $response , $items ) ;
                    }

                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' postFavorites() url:' . $url. ' failed | ' . $e->getMessage() ) ;
                    if( $response )
                    {
                        return $this->formatError( $response , '500' , [ 'postFavorites()' , $e->getMessage() ] , NULL , 500);
                    }
                }

            }
            else
            {
                $errors = [];

                $err = $this->validator->errors();
                $keys = $err->keys();

                foreach( $keys as $key )
                {
                    $errors[ $key ] = $err->first($key);
                }

                return $this->error($response , $errors , "400");
            }
        }

        $this->logger->warning( $this . ' postFavorites(' . $url . ') failed' ) ;

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;

    }

    public function putForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->put( $request , $response , $args ) ;
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'active'   => TRUE ,
        'id'       => NULL
    ] ;

    /**
     * Put user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     *
     * @OA\RequestBody(
     *     request="putUser",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="givenName",type="string",description="The name of the resource"),
     *             @OA\Property(property="familyName",type="string",description="The name of the resource"),
     *             required={"givenName","familyName"},
     *             @OA\Property(property="gender",type="integer",description="The id of the gender"),
     *             @OA\Property(property="person",type="integer",description="The id of the person"),
     *             @OA\Property(property="team",type="integer",description="The id of the team"),
     *         )
     *     ),
     *     required=true
     * )
     * @throws \ArangoDBClient\Exception
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'active' => $active ,
            'id'     => $id
        ]
        = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // user exists
        $user = $this->model->get( $id , [ 'key' => 'uuid' ] ) ;
        if( !$user )
        {
            return $this->error( $response , null , '404' , null , 404 ) ;
        }

        $user = $this->create( $user ) ;
        $auth = $this->container->get('auth') ;

        // SECURITY - check if user team can change selected user
        if( $auth->team->identifier > $user->team->identifier )
        {
            return $this->formatError( $response , '401' , null , null , 401 ) ;
        }

        // is you
        $isYou = false ;
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub && $jwt->sub == $id )
        {
            $isYou = true ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];

        if( isset( $params['givenName'] ) )
        {
            $item['givenName'] = $params['givenName'] ;
        }

        if( isset( $params['familyName'] ) )
        {
            $item['familyName'] = $params['familyName'] ;
        }

        if( isset( $params['gender'] ) )
        {
            $gender = $this->container->get('genders')->exist( $params['gender'] ) ;
            if( $gender )
            {
                $item['gender'] = (int)$params['gender'] ;
            }
        }

        $conditions =
        [
            'givenName'  => [ $params['givenName']  , 'required|max(50)' ] ,
            'familyName' => [ $params['familyName'] , 'required|max(50)' ]
        ] ;

        $change_team = FALSE ;

        if( !$isYou )
        {
            // check team
            if( isset( $params['team'] ) && $params['team'] != '' )
            {
                if( $params['team'] != $user->team->name )
                {
                    $team = $this->container
                                 ->get( Teams::class )
                                 ->get( $params['team'] ) ;

                    // SECURITY - check if user auth can change team with selected team
                    if( $team )
                    {
                        $teamsController = $this->container->get( TeamsController::class ) ;
                        $team = $teamsController->create( $team ) ;
                        if( $auth->team->identifier <= $team->identifier )
                        {
                            $item['team'] = $team->name ;

                            $change_team = TRUE ;
                        }

                    }
                }
            }

            // check person
            if( isset( $params['person'] ) )
            {
                if( $params['person'] != '' || $params['person'] != null )
                {
                    $item['person']       = (int)$params['person'] ;
                    $conditions['person'] = [ $params['person'] , 'person'    ] ;
                }
                else
                {
                    $item['person'] = null ;
                }
            }
            else
            {
                $item['person'] = null ;
            }
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }
        if( isset( $params['uuid'] ) )
        {
            unset( $params['uuid'] ) ;
        }
        if( isset( $params['email'] ) )
        {
            unset( $params['email'] ) ;
        }
        if( isset( $params['image'] ) )
        {
            unset( $params['image'] ) ;
        }
        if( isset( $params['provider'] ) )
        {
            unset( $params['provider'] ) ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                $result = $this->model->update( $item , $id , [ 'key' => 'uuid' , 'active' => $active ] );

                if( $result )
                {
                    if( $change_team === TRUE )
                    {
                        // revoke user sessions to apply new team immediately
                        $revoke = $this->container
                                       ->get( Sessions::class )
                                       ->revokeAll( $id ) ;
                    }
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'key' => 'uuid' , 'queryFields' => $this->getFields() ] ) ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    public function deleteForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->delete( $request , $response , $args ) ;
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Delete user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     * @throws \ArangoDBClient\Exception
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args )  ;

        $this->logger->debug( $this . ' delete(' . $id . ')' ) ;

        $user = $this->container
                     ->get( Users::class )
                     ->get( $id , [ 'key' => 'uuid' ] ) ;

        if( !$user )
        {
            return $this->formatError( $response , '404' , null , null , 404 ) ;
        }

        $user = $this->create( $user ) ;
        $auth = $this->container->get('auth') ;

        // SECURITY - check if user team can delete selected user
        if( $auth->team->identifier > $user->team->identifier )
        {
            return $this->formatError( $response , '401' , null , null , 401 ) ;
        }

        try
        {
            // remove user in firebase
            try
            {
                $this->container
                     ->get('firebase')
                     ->getAuth()
                     ->deleteUser( $user->uuid ) ;
            }
            catch( Exception $e )
            {

            }

            $result = $this->model->delete( $id , [ 'key' => 'uuid' ] );

            if( $result )
            {
                // remove sessions
                $rt = $this->container
                           ->get( Sessions::class )
                           ->revokeAll( $id );

                return $this->success( $response , "ok" );
            }
            else
            {
                return $this->error( $response , 'not deleted' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * Output users
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param null $items
     * @param null $params
     * @param array|NULL $options
     * @return NULL
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function outputAll( Request $request = NULL , Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->config[ 'api' ] ;
        $set = array_key_exists( $this->path , $this->config ) ? $this->config[ $this->path ] : NULL ;

        $format = $request->getAttribute('format') ;

        $lang   = NULL ;
        $skin   = NULL ;
        $count  = NULL ;
        $limit  = NULL ;
        $offset = NULL ;
        $total  = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- lang

        if( !empty($params['lang']) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
            else
            {
                unset($params['lang']) ;
            }
        }

        // ----- skin

        if( !isset( $skin ) && is_array( $set ) && array_key_exists( 'skin_default' , $set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( is_array($set) && in_array( $params['skin'] , $set['skins'] ) )
            {
                $params['skin'] = $skin = $params['skin'] ;
            }
            else
            {
                unset($params['skin']) ;
            }
        }


        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            switch( $format )
            {
                case "html" :
                {
                    if( is_array( $options ) )
                    {
                        if( array_key_exists( 'count' , $options ) )
                        {
                            $count = $options['count'] ;
                            unset( $options['count'] ) ;
                        }

                        if( array_key_exists( 'limit' , $options ) )
                        {
                            $limit = $options['limit'] ;
                            unset( $options['limit'] ) ;
                        }

                        if( array_key_exists( 'offset' , $options ) )
                        {
                            $offset = $options['offset'] ;
                            unset( $options['offset'] ) ;
                        }

                        if( array_key_exists( 'total' , $options ) )
                        {
                            $total = $options['total'] ;
                            unset( $options['total'] ) ;
                        }
                    }

                    return $this->render
                    (
                        $response ,
                        'users/all.twig' ,
                        [
                            //'api'    => $api['url'],
                            'limit'  => $limit,
                            'offset' => $offset,
                            'count'  => $count,
                            'total'  => $total,
                            'users'  => $items
                        ]
                    ) ;
                }
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response ,
                        $items ,
                        $this->getUrl( $this->fullPath , $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }

    /**
     * Returns
     * @param array $favorites
     * @return array
     */
    private function populateSpecialFavorites( array $favorites ) : array
    {
        $cache = [] ;

        foreach( $favorites as $thing )
        {
            foreach( self::SPECIAL_FAVORITES as $key => $value )
            {
                if( str_contains( $thing['url'] , '/' . $key . '/' ) )
                {
                    if( !isset($cache[$key] ) )
                    {
                        $cache[$key] = [] ;
                    }
                    $cache[ $key ][] = $thing['id'] ;
                    break ;
                }
            }
        }

        foreach( $cache as $key => $value )
        {
            $favorites = $this->populateFavorites
            (
                $favorites ,
                $value ,
                $this->container->get( self::SPECIAL_FAVORITES[ $key ] )
            ) ;
        }

        return $favorites ;
    }

    /**
     * @param array $favorites
     * @param array $ids
     * @param CollectionsController $controller
     * @return array
     */
    private function populateFavorites( array $favorites , array $ids , CollectionsController $controller ) : array
    {
        if(
            is_countable( $favorites )      &&
            is_countable( $ids       )      &&
                   count( $favorites ) > 0  &&
                   count( $ids       ) > 0
        )
        {
            $items = $controller->all( NULL , NULL , [ 'active' => NULL , 'facets' => [ 'ids' => implode( ',' , array_values( $ids ) ) ] ] ) ;
            if( is_array( $items ) )
            {
                $cache = [] ;
                foreach( $items as $key => $thing )
                {
                    $cache[ $thing->id ] = $thing ;
                }

                $len = count( $favorites ) ;
                foreach( $favorites as $key => $value )
                {
                    $index = NULL ;

                    if( is_object($value) )
                    {
                        $index = $value->id ;
                    }
                    else if( is_array( $value ) )
                    {
                        $index = $value['id']  ;
                    }

                    if( !is_null($index) && isset( $cache[ $index ] ) )
                    {
                        $favorites[ $key ] = $cache[ $index ] ;
                    }
                }
            }
        }

        return $favorites ;
    }
}


