<?php

namespace xyz\ooopener\controllers\users ;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\things\Thing;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\models\Collections;
use Psr\Container\ContainerInterface;

class UserActivityLogsController extends CollectionsController
{
    /**
     * ActivityLogs constructor.
     *
     * @param ContainerInterface $container
     * @param ?Collections|NULL $model
     * @param ?string $path
     */
    public function __construct( ContainerInterface $container , ?Collections $model = NULL , ?string $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="method",
     *     type="string",
     *     description="The request method",
     *     enum={
     *         "DELETE",
     *         "PATCH",
     *         "POST",
     *         "PUT"
     *     }
     * )
     *
     * @OA\Schema(
     *     schema="ActivityLog",
     *     description="The activity log",
     *     type="object",
     *     @OA\Property(property="id",type="string",description="Resource ID"),
     *     @OA\Property(property="url",type="string",description="The resource url"),
     *     @OA\Property(property="agent",type="string",description="The user agent"),
     *     @OA\Property(property="ip",type="string",description="The user ip"),
     *     @OA\Property(property="method",ref="#/components/schemas/method"),
     *     @OA\Property(property="resource",type="string",format="uri",description="The resource url logged"),
     *     @OA\Property(property="created",type="string",format="date-time",description="The creation date"),
     *     @OA\Property(property="user",ref="#/components/schemas/UserList"),
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'       => [ 'filter' => Thing::FILTER_ID       ] ,
        'url'      => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'  => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'agent'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'ip'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'method'   => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'resource' => [ 'filter' => Thing::FILTER_URL_API  ] ,
        'user'     => [ 'filter' => Thing::FILTER_JOIN     ]
    ];

    public function allForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $jwt = $this->container->get('jwt') ;

        if( $jwt && $jwt->sub )
        {
            $args['facets'] = [ 'user' => $jwt->sub ] ;
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }

    public function allByUser( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        if( array_key_exists( 'id' , $args ) )
        {
            $args['facets'] = [ 'user' => $args['id'] ] ;
            unset( $args['id'] );
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }

    /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        $init->url = $this->getFullPath()
                   . parse_url( $_SERVER["REQUEST_URI"] , PHP_URL_PATH )
                   . '/'
                   . $init->id ;
        return $init ;
    }

    protected function getAllUrl( $params ) :string
    {
        return $this->getFullPath() . parse_url( $_SERVER["REQUEST_URI"] , PHP_URL_PATH )  ;
    }
}
