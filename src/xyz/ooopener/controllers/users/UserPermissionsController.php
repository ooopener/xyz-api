<?php

namespace xyz\ooopener\controllers\users ;

use xyz\ooopener\controllers\Controller;
use xyz\ooopener\models\Model;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface;

use Exception ;

class UserPermissionsController extends Controller
{
    /**
     * UserPermissions constructor.
     *
     * @param ContainerInterface $container
     * @param Model $owner
     */
    public function __construct( ContainerInterface $container , Model $owner )
    {
        parent::__construct( $container );
        $this->owner      = $owner ;
    }

    /**
     * The owner reference.
     */
    public Model $owner;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'id'  => NULL
    ] ;

    public function all( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $items = NULL ;

        try
        {
            $user = $this->owner->get( $id , [ 'key' => 'uuid' , 'fields' => 'permissions' ] ) ;
            if( !$user )
            {
                return $this->formatError( $response , '404', [ 'all(' . $id . ')' ] , NULL , 404 );
            }

            $items = [] ;

            if( property_exists( $user , 'permissions' ) && is_countable($user->permissions) && count( $user->permissions ) > 0 )
            {
                foreach( $user->permissions as $permission )
                {
                    $resource = ( $permission['resource'] != "0" ) ? '/' . $permission['resource'] : '' ;
                    $path = $permission['module'] . $resource ;

                    switch( $permission['permission'] )
                    {
                        case 'A':
                            $items[ $path ] = 'admin';
                            break;
                        case 'W':
                            $items[ $path ] = 'write';
                            break;
                        case 'R':
                            $items[ $path ] = 'read';
                            break;
                    }
                }
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $items,
                $this->getCurrentPath( $request )
            );
        }

        return $items ;
    }
}
