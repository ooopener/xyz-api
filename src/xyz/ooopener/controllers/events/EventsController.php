<?php

namespace xyz\ooopener\controllers\events;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController;
use xyz\ooopener\controllers\places\PlacesController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Stages;
use xyz\ooopener\validations\EventValidator;

use DateTime;
use DateTimeZone;
use Exception;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Container\ContainerInterface ;

use xyz\ooopener\things\Event;
use xyz\ooopener\things\Thing;

/**
 * The event collection controller.
 */
class EventsController extends CollectionsController
{
    /**
     * Creates a new EventsController instance.
     * @param ContainerInterface $container
     * @param Collections|null $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = 'events' )
    {
        parent::__construct( $container , $model , $path , new EventValidator( $container ) );
    }

    /**
     * The default 'from' method options.
     */
    const ARGUMENTS_FROM_DEFAULT =
    [
        'active'   => TRUE ,
        'facets'   => NULL ,
        'from'     => 'now' ,
        'to'       => NULL ,
        'interval' => 365 ,
        'lang'     => NULL ,
        'limit'    => NULL ,
        'offset'   => NULL ,
        'search'   => NULL ,
        'skin'     => NULL ,
        'sort'     => NULL ,
        'timezone' => NULL ,
        'params'   => NULL
    ] ;

    /**
     * Returns all the elements from a specific date.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args array init ex: [ 'limit' => 0 ]
     * @return Response|array
     */
    public function from( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [
            'active'   => $active ,
            'facets'   => $facets ,
            'from'     => $from ,
            'to'       => $to ,
            'interval' => $interval ,
            'lang'     => $lang ,
            'limit'    => $limit ,
            'offset'   => $offset ,
            'search'   => $search ,
            'skin'     => $skin ,
            'timezone' => $timezone ,
            'params'   => $params
        ]
        = array_merge( self::ARGUMENTS_FROM_DEFAULT , $args ) ;

        $api  = $this->config['api'] ;
        $set  = $this->config[$this->path] ;
        $time = $this->config['time'] ;

        $format = $request->getAttribute('format') ;

        if( !isset($from) )
        {
            $from = $time['from_default'] ;
        }

        if( $response )
        {
            $this->logger->debug( $this . ' from(' . $from . ')' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = isset($params) ? $params : $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default', $set) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            // ----- limit

            $limit = intval( isset($limit) ? $limit : $api['limit_default'] ) ;
            if( isset($params['limit']) )
            {
                $limit = filter_var
                (
                    $params['limit'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['limit'] = intval( ($limit !== FALSE) ? $limit : $api['limit_default'] ) ;
            }

            // ----- offset

            $offset = intval( isset($offset) ? $offset : $api['offset_default'] ) ;
            if( isset($params['offset']) )
            {
                $offset = filter_var
                (
                    $params['offset'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['offset'] = intval( ($offset !== FALSE) ? $offset : $api['offset_default'] ) ;
            }

            // ----- facets

            if( isset($params['facets']) )
            {
                try
                {
                    $facets = array_merge
                    (
                        json_decode( $params['facets'] , TRUE ) ,
                        isset($facets) ? $facets : []
                    ) ;
                }
                catch( Exception $e )
                {
                    $this->logger->warning( $this . ' from failed, the facets params failed to decode the json expression: ' . $params['facets'] ) ;
                }
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }

            // ----- timezone

            if( isset($params['timezone']) )
            {
                try
                {
                    $timezone = new DateTimeZone($params['timezone']) ;
                }
                catch ( Exception $e )
                {
                    unset($params['timezone']) ;
                    return $this->formatError( $response , '010' , [ 'from' , $e->getMessage() ] );
                }
            }

            // ----- interval

            if( isset($params['interval']) )
            {
                $interval = filter_var
                (
                    $params['interval'],
                    FILTER_VALIDATE_INT ,
                    [
                        'options' =>
                        [
                            "min_range" => 1,
                            "max_range" => intval( $time["maxinterval"] )
                        ]
                    ]
                ) ;
                if( $interval === FALSE )
                {
                    unset($params['interval']) ;
                }
                else
                {
                    $params['interval'] = $interval ;
                }
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        if( is_null($interval) || $interval === FALSE )
        {
            $interval = (int) $time["interval_default"] ;
        }

        if( !isset( $timezone ) )
        {
            $timezone = new DateTimeZone( $time['timezone_default'] ) ;
        }

        // ------------

        try
        {
            $from = new DateTime( $from , $timezone ) ;
        }
        catch ( Exception $e )
        {
            return $this->formatError( $response , '400' , [ 'from' ] );
        }

        // ------------

        if( isset($to) )
        {
            try
            {
                $to = new DateTime( $to , $timezone ) ;
            }
            catch ( Exception $e )
            {
                return $this->formatError( $response , '400' , [ 'to' ] );
            }

            if( isset($from) && ($from instanceof DateTime) &&  isset($to) && ($to instanceof DateTime) )
            {
                if( $to >= $from )
                {
                    $dateInterval /*DateInterval*/ = $to->diff( $from , TRUE ) ;
                    $interval = $dateInterval->days ;
                }
                else
                {
                    return $this->formatError( $response , '606' , [ $from->format('Y-m-d') , '>' , $to->format('Y-m-d') ] );
                }
            }
        }

        // ------------

        $items   = NULL  ;
        $options = NULL ;

        // ------------

        try
        {
            // convert from date to UTC
            $from->setTimezone( new DateTimeZone( 'UTC' ) ) ;

            $init =
            [
                'active'   => $active,
                'facets'   => $facets,
                'from'     => $from->format( "Y-m-d\TH:i:s.v\Z" ),
                'interval' => $interval,
                'search'   => $search
            ];

            $items = $this->model->from
            ([
                'active'      => $active,
                'facets'      => $facets,
                'from'        => $from->format( "Y-m-d\TH:i:s.v\Z" ),
                'interval'    => $interval,
                'limit'       => $limit ,
                'offset'      => $offset ,
                'search'      => $search ,
                'lang'        => $lang,
                'queryFields' => $this->getFields( $skin )
            ]) ;

            //$options = [ 'total' => (int) $this->model->foundRows() ] ;
            $options = [ 'total' => $limit != 0 ? $this->model->countFrom( $init ) : count( $items ) ] ;

            if( $items )
            {
                foreach( $items as $key => $value )
                {
                    $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
                }
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'from' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            switch( $format )
            {
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $items ,
                        $this->getCurrentPath( $request , $params ) ,
                        is_array($items) ? count($items) : NULL ,
                        $options
                    );
                }
            }
        }

        return $items ;
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="EventList",
     *     description="An event happening at a certain time and location, such as a concert, lecture, or festival",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="startDate",type="string",format="date-time",description="The start date and time of the item (in ISO 8601 date format)"),
     *     @OA\Property(property="endDate",type="string",format="date-time",description="The end date and time of the item (in ISO 8601 date format)"),
     *
     *     @OA\Property(property="eventStatus",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="capacity",type="integer"),
     *     @OA\Property(property="numAttendee",type="integer"),
     *     @OA\Property(property="remainingAttendee",type="integer")
     * )
     *
     * @OA\Schema(
     *     schema="Event",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/EventList")},
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="offers",type="array",items=@OA\Items(ref="#/components/schemas/Offer")),
     *     @OA\Property(property="organizer",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="subEvent",type="array",items=@OA\Items(ref="#/components/schemas/EventList")),
     *     @OA\Property(property="superEvent",type="array",items=@OA\Items(ref="#/components/schemas/EventList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website")),
     *     @OA\Property(property="workFeatured",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList"))
     * )
     *
     * @OA\Response(
     *     response="eventResponse",
     *     description="Result of the event",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Event")
     *     )
     * )
     *
     * @OA\Response(
     *     response="eventListResponse",
     *     description="Result list of events",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/EventList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'     => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'       => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn' => [ 'filter' => Thing::FILTER_DEFAULT  , 'skins' => [ 'full' ] ] ,
        'startDate' => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'endDate'   => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'audio' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'numAudios' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos' => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'additionalType'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'alternateName'       => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'text'                => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'eventStatus'         => [ 'filter' => Thing::FILTER_JOIN        ] ,
        'location'            => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'notes'          => [ 'filter' => Thing::FILTER_TRANSLATE  , 'skins' => [ 'full' , 'normal' ] ] ,
        'keywords'       => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' ] ] ,
        'audios'         => [ 'filter' => Event::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'         => [ 'filter' => Event::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'         => [ 'filter' => Event::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'videos' ] ] ,
        'offers'         => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'normal' ] ] ,
        'organizer'      => [ 'filter' => Event::FILTER_ORGANIZER  , 'skins' => [ 'full' , 'extend' ] ] ,
        'subEvent'       => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' ] ] ,
        'superEvent'     => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' ] ] ,
        'workFeatured'   => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' ] ] ,
        'websites'       => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'extend' ] ]
    ];

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Event::FILTER_ORGANIZER :
                    {
                        if( property_exists( $init , Event::FILTER_ORGANIZER ) )
                        {
                            // get organizers
                            $organizers = $this->container
                                               ->get('eventOrganizersController')
                                               ->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;

                            $init->{ $key } = $organizers ;
                        }
                        break;
                    }
                    case Event::FILTER_LOCATION :
                    {
                        // get image
                        if( property_exists( $init , Event::FILTER_LOCATION ) && $init->{ Event::FILTER_LOCATION } != NULL )
                        {
                            $init->{ $key } = $this->container
                                                   ->get(PlacesController::class)
                                                   ->create( (object) $init->{ Event::FILTER_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case Event::FILTER_SUB_EVENT :
                    {
                        if( property_exists( $init , Event::FILTER_SUB_EVENT ) && is_array( $init->{ Event::FILTER_SUB_EVENT } ) && count( $init->{ Event::FILTER_SUB_EVENT } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Event::FILTER_SUB_EVENT } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang , 'normal' ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Event::FILTER_SUPER_EVENT :
                    {
                        if( property_exists( $init , Event::FILTER_SUPER_EVENT ) && is_array( $init->{ Event::FILTER_SUPER_EVENT } ) && count( $init->{ Event::FILTER_SUPER_EVENT } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Event::FILTER_SUPER_EVENT } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang , 'normal' ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Event::FILTER_WORK_FEATURED :
                    {
                        if( property_exists( $init , Event::FILTER_WORK_FEATURED ) && is_array( $init->{ Event::FILTER_WORK_FEATURED } ) && count( $init->{ Event::FILTER_WORK_FEATURED } ) > 0 )
                        {
                            $works = [] ;
                            foreach( $init->{ Event::FILTER_WORK_FEATURED } as $item )
                            {
                                array_push( $works , $this->container
                                                          ->get(ConceptualObjectsController::class)
                                                          ->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $works ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    /**
     * Remove all linked resources before the final delete process.
     * @param $id
     */
    public function deleteDependencies( $id )
    {
        $this->deleteAllInController( 'eventKeywordsController'      , $id ) ; // keywords
        $this->deleteAllInController( 'eventOffersController'        , $id ) ; // offers
        $this->deleteAllInController( 'eventWebsitesController'      , $id ) ; // websites
        $this->deleteAllInController( 'eventWorksFeaturedController' , $id ) ; // workFeatured

        $this->deleteInController( 'eventAudioController' , $id ) ; // audio
        $this->deleteInController( 'eventImageController' , $id ) ; // image
        $this->deleteInController( 'eventVideoController' , $id ) ; // video

        $this->deleteAllInController( 'eventAudiosController' , $id , [ 'list' => '*' ] , 'id' ) ; // audios
        $this->deleteAllInController( 'eventPhotosController' , $id , [ 'list' => '*' ] , 'id' ) ; // photos
        $this->deleteAllInController( 'eventVideosController' , $id , [ 'list' => '*' ] , 'id' ) ; // videos

        $this->deleteInEdgeModel( 'userFavorites' , $id ) ; // favorites

        $this->deleteInEdgeModel( 'eventEventsTypes'             , $id , 'to' ) ; // event.additionalType
        $this->deleteInEdgeModel( 'eventPlaces'                  , $id , 'to' ) ; // event.location
        $this->deleteInEdgeModel( 'eventOrganizersOrganizations' , $id , 'to' ) ; // event.organizer
        $this->deleteInEdgeModel( 'eventOrganizersPeople'        , $id , 'to' ) ; // event.organizer
        $this->deleteInEdgeModel( 'eventSubEvents'               , $id        ) ; // event.subEvent
        $this->deleteInEdgeModel( 'eventSubEvents'               , $id , 'to' ) ; // event.superEvent
        $this->deleteInEdgeModel( 'eventWorksFeatured'           , $id , 'to' ) ; // event.workFeatured

        $this->deleteAllArrayPropertyValuesInModel( Courses::class , 'discover' , $id ) ; // courses.discover
        $this->deleteAllArrayPropertyValuesInModel( Stages::class  , 'discover' , $id ) ; // stages.discover
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postEvent",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="startDate",type="string",description="The start date",format="date-time"),
     *             @OA\Property(property="endDate",type="string",description="The end date",format="date-time"),
     *             required={"name","additionalType","startDate","endDate"},
     *             @OA\Property(property="eventStatus",type="integer",description="The id of the event status"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        $item       = [] ;
        $params     = $request->getParsedBody() ;
        $conditions =
        [
            'name'           => [ $params['name']           , 'required|min(2)|max(140)' ] ,
            'endDate'        => [ $params['endDate']        , 'datetime|required'        ] ,
            'startDate'      => [ $params['startDate']      , 'datetime|required'        ] ,
            'additionalType' => [ $params['additionalType'] , 'required|additionalType'  ] ,
            'location'       => [ $params['location']       , 'location'                 ]
        ] ;

        $additionalType = isset( $params['additionalType'] ) ? (string) $params['additionalType'] : NULL ;
        $location       = isset( $params['location']       ) ? (string) $params['location']       : NULL ;

        $item['endDate']   = isset( $params['endDate'] )   && !empty( $params['endDate'] )   ? $params['endDate']   : NULL ;
        $item['startDate'] = isset( $params['startDate'] ) && !empty( $params['startDate'] ) ? $params['startDate'] : NULL ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            $item['eventStatus']       = (int) $params['eventStatus'] ;
            $conditions['eventStatus'] = [ $params['eventStatus'] , 'required|eventStatus' ] ;
        }
        else
        {
            $item['eventStatus'] = $this->getDefaultEventStatus()  ;
        }

        if( isset( $params['startDate'] ) && isset( $params['endDate'] ) )
        {
            $conditions['after']  =
            [
                (boolean) ( strtotime($params['startDate'] ) < strtotime($params['endDate'] ) )  ,
                'true'
            ] ;
        }

        ////// security - remove sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;
        if( $this->validator->passes() )
        {
            try
            {
                $item['active']     = 1 ;
                $item['withStatus'] = Status::DRAFT ;
                $item['path']       = $this->path ;

                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $new = $this->model->insert( $item ) ;
                $id  = $new->_key ;

                $this->setEdgeFrom( $id , $additionalType , 'eventEventsTypes' ) ;
                $this->setEdgeFrom( $id , $location       , 'eventPlaces'      ) ;

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchEvent",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="startDate",type="string",description="The start date",format="date-time"),
     *             @OA\Property(property="endDate",type="string",description="The end date",format="date-time"),
     *             @OA\Property(property="eventStatus",type="integer",description="The id of the event status"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *
     *             @OA\Property(property="capacity",type="integer"),
     *             @OA\Property(property="numAttendee",type="integer"),
     *             @OA\Property(property="remainingAttendee",type="integer")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $old = $this->model->get( $id , [ 'fields' => 'endDate,startDate' ] ) ;
        if( !$old )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;
        $location = NULL ;

        $testEndDate   = $old->endDate ;
        $testStartDate = $old->startDate ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(140)' ] ;
        }

        if( isset( $params['endDate'] ) )
        {
            if( $params['endDate'] != '' )
            {
                $item['endDate'] = $testEndDate = $params['endDate'] ;
                $conditions['endDate'] = [ $params['endDate'] , 'required|datetime' ] ;
            }
            else
            {
                $item['endDate'] = $testEndDate = NULL ;
            }
        }

        if( isset( $params['startDate'] ) )
        {
            $item['startDate'] = $testStartDate = $params['startDate'] ;
            $conditions['startDate'] = [ $params['startDate'] , 'required|datetime' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            $item['eventStatus'] = (int) $params['eventStatus'] ;
            $conditions['eventStatus'] = [ $params['eventStatus'] , 'required|eventStatus' ] ;
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params['location'] ;
                $conditions['location'] = [ $params['location'] , 'location' ] ;
            }
            else
            {
                $location = FALSE ;
            }
        }

        if( isset( $params['capacity'] ) )
        {
            $item['capacity'] = (int) $params['capacity'] ;
            $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
        }

        if( isset( $params['numAttendee'] ) )
        {
            $item['numAttendee'] = (int) $params['numAttendee'] ;
            $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
        }

        if( isset( $params['remainingAttendee'] ) )
        {
            $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
            $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
        }

        /// check dates

        if( $testStartDate && $testEndDate )
        {
            $after = (boolean) (strtotime($testStartDate) < strtotime($testEndDate)) ;
            $conditions['after']  = [ $after , 'true' ] ;
        }


        ////// security - remove sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        //////

        $this->validator->validate( $conditions ) ;
        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $this->setEdgeFrom( $id , $additionalType , 'eventEventsTypes' ) ;
                $this->setEdgeFrom( $id , $location       , 'eventPlaces'      ) ;

                $this->model->update( $item , $id );

                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ));
            }
            catch( Exception $exception )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $exception->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putEvent",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="startDate",type="string",description="The start date",format="date-time"),
     *             @OA\Property(property="endDate",type="string",description="The end date",format="date-time"),
     *             @OA\Property(property="eventStatus",type="integer",description="The id of the event status"),
     *             required={"name","additionalType","startDate","endDate","eventStatus"},
     *             @OA\Property(property="location",type="integer",description="The place ID")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        $additionalType = isset( $params['additionalType'] ) ? (string) $params['additionalType'] : NULL ;
        $location       = isset( $params['location']       ) ? (string) $params['location']       : NULL ;

        $item = [];

        $item['endDate']   = isset( $params['endDate'] )   && !empty( $params['endDate'] )   ? $params['endDate']   : NULL ;
        $item['startDate'] = isset( $params['startDate'] ) && !empty( $params['startDate'] ) ? $params['startDate'] : NULL ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            $item['eventStatus'] = (int) $params['eventStatus'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'required|min(2)|max(140)' ] ,
            'endDate'        => [ $params['endDate']        , 'required|datetime'        ] ,
            'startDate'      => [ $params['startDate']      , 'required|datetime'        ] ,
            'additionalType' => [ $params['additionalType'] , 'required|additionalType'  ] ,
            'eventStatus'    => [ $params['eventStatus']    , 'required|eventStatus'     ] ,
            'location'       => [ $params['location']       , 'location'                 ]
        ] ;

        if( isset( $params['startDate'] ) && isset( $params['endDate'] ) )
        {
            $conditions['after'] =
            [
                (boolean) (strtotime($params['startDate']) < strtotime($params['endDate'])) ,
                'true'
            ] ;
        }

        ////// security - remove sensible fields

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        //////

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $this->setEdgeFrom( $id , $additionalType , 'eventEventsTypes' ) ;
                $this->setEdgeFrom( $id , $location       , 'eventPlaces'      ) ;

                $result = $this->model->update( $item , $id );

                return $this->success( $response ,$this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
            }
            catch( Exception $exception )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $exception->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

    protected function getDefaultEventStatus( string $model = 'eventsStatusTypes' , string $identifier = 'scheduled' ) :?int
    {
        if( $this->container->get( $model ) )
        {
            $thing = $this->container
                          ->get( $model )
                          ->get( $identifier , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;

            if( $thing )
            {
                return (int) $thing->_key ;
            }
        }
        return NULL ;
    }
}


