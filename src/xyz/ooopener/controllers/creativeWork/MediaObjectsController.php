<?php

namespace xyz\ooopener\controllers\creativeWork;

use xyz\ooopener\helpers\Hash;
use Exception ;

use Psr\Container\ContainerInterface ;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use FFMpeg\FFProbe;
use FFMpeg\FFMpeg ;
use FFMpeg\Format\Audio\Mp3 ;

use ForceUTF8\Encoding;

use system\date\ISO8601;

use xyz\ooopener\helpers\FFMpeg\Format\Audio\Aac;

use xyz\ooopener\controllers\CollectionsController;

use xyz\ooopener\helpers\Status;
use xyz\ooopener\helpers\Xmp;

use xyz\ooopener\models\Collections;

use xyz\ooopener\things\creativeWork\mediaObject\AudioObject;
use xyz\ooopener\things\creativeWork\mediaObject\ImageObject;
use xyz\ooopener\things\creativeWork\MediaObject;
use xyz\ooopener\things\creativeWork\mediaObject\VideoObject;

use xyz\ooopener\validations\MediaObjectValidator;

/**
 * The mediaObject controller.
 *
 * @OA\Schema(
 *     schema="MediaObject",
 *     allOf={
 *         @OA\Schema(ref="#/components/schemas/Thing"),
 *         @OA\Schema(ref="#/components/schemas/headlineText"),
 *         @OA\Schema(ref="#/components/schemas/longText")
 *     },
 *     @OA\Property(type="string",property="author",description="The author of this content"),
 *     @OA\Property(type="string",property="editor",description="Specifies the Person who edited the CreativeWork"),
 *     @OA\Property(type="string",property="encoding",description="A media object that encodes this CreativeWork"),
 *     @OA\Property(type="string",property="encodingFormat",description="Media type typically expressed using a MIME format (see IANA site and MDN reference) e.g. application/zip for a SoftwareApplication binary, audio/mpeg for .mp3 etc.)"),
 *     @OA\Property(type="string",property="inLanguage",description="The language of the content or performance or used in an action"),
 *     @OA\Property(type="string",property="keywords",description="Keywords or tags used to describe this content"),
 *     @OA\Property(type="string",property="license",description="A license document that applies to this content, typically indicated by URL"),
 *     @OA\Property(type="string",property="mentions",description="Indicates that the CreativeWork contains a reference to, but is not necessarily about a concept"),
 *     @OA\Property(type="string",property="publisher",description="The publisher of the creative work"),
 *     @OA\Property(type="string",property="thumbnailUrl",description="A thumbnail image relevant to the Thing"),
 *     @OA\Property(type="string",property="review",description="A review of the item"),
 *
 *     @OA\Property(type="string",property="bitrate",description="The bitrate of the media object"),
 *     @OA\Property(type="string",property="contentSize",description="File size in (mega/kilo) bytes"),
 *     @OA\Property(type="string",property="contentUrl",description="Actual bytes of the media object, for example the image file or video file"),
 *     @OA\Property(type="string",property="duration",description="The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format"),
 *     @OA\Property(type="string",property="embedUrl",description="A URL pointing to a player for a specific video"),
 *     @OA\Property(type="integer",property="height",description="The height of the item"),
 *     @OA\Property(type="string",property="playerType",description="Player type required"),
 *     @OA\Property(type="integer",property="width",description="The width of the item")
 * )
 *
 * @OA\Response(
 *     response="mediaObject",
 *     description="Result of the mediaObject",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",description="The request status",example="success"),
 *         @OA\Property(property="result",ref="#/components/schemas/MediaObject")
 *     )
 * )
 */
class MediaObjectsController extends CollectionsController
{
    /**
     * Creates a new MediaObjectController instance.
     * @param ContainerInterface $container
     * @param Collections|null   $model
     * @param string|null        $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $path );
        $this->ffprobe = $this->container->get( FFProbe::class );
        $this->ffmpeg  = $this->container->get( FFMpeg::class  );
        $this->root    = $this->config['files']['root'] ;
    }

    protected FFMpeg $ffmpeg ;

    protected FFProbe $ffprobe ;

    /**
     * The image root.
     */
    public string $root = '' ;

    // --------------------------------

    const CREATE_PROPERTIES = [] ;

    /**
     * The default 'image' method options.
     */
    const ARGUMENTS_IMAGE_DEFAULT =
    [
        'id'      => NULL  ,
        'check'   => TRUE  ,
        'gray'    => FALSE ,
        'format'  => 'jpg' ,
        'quality' => 70    ,
        'params'  => NULL  ,
        'strip'   => FALSE
    ] ;

    /**
     * The default 'exists' methods options.
     */
    const ARGUMENTS_EXISTS_DEFAULT =
    [
        'list'           => NULL ,
        'encodingFormat' => NULL
    ] ;

    /**
     * Exists all items
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function exists( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'list'           => $list ,
            'encodingFormat' => $encodingFormat
        ]
        = array_merge( self::ARGUMENTS_EXISTS_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' exists()' ) ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;

            if( !empty($params['list']) )
            {
                $list = $params['list'] ;
            }

            if( !empty($params['encodingFormat']) )
            {
                $encodingFormat = $params['encodingFormat'] ;
            }
        }

        if( isset( $list ) )
        {
            try
            {
                $items = explode( ',' , $list ) ;

                $options = [] ;
                if( isset( $encodingFormat ) )
                {
                    $options['conditions'] = [ 'doc.encodingFormat =~ "' . $encodingFormat . '"' ] ;
                }

                $count = $this->model->existAll( $items , $options ) ;

                if( $count == count( $items ) )
                {
                    if( $response )
                    {
                        return $this->success( $response , $count );
                    }
                    return $count ;
                }
                else
                {
                    if( $response )
                    {
                        return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                    }
                    return null ;
                }

            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'exists()' , $e->getMessage() ] , NULL , 500 );
            }
        }

        if( $response )
        {
            return $this->error( $response , 'no list' ) ;
        }
        return null ;
    }


    /**
     * Delete photo
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [
            'id'         => $id,
            'conditions' => $conditions
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' delete(' . $id . ')' ) ;

        try
        {
            $media = $this->model->delete( $id );

            $this->logger->info( $this . ' delete(' . $id . ') result:' . json_encode( $media , JSON_PRETTY_PRINT ) ) ;

            if( $media )
            {
                $this->deleteMediaDependencies( $id , $media ) ;
                $this->deleteMediaContent( $media ) ;

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->error( $response , 'not deleted' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteAll( ?Request $request, ?Response $response , array $args = [] )
    {
        $this->logger->debug( $this . ' deleteAll()' ) ;

        $list = $this->getParam( $request , 'list' ) ;

        if( $list )
        {
            try
            {
                $items = explode( ',' , $list ) ;

                $count = $this->model->existAll( $items ) ;

                if( $count == count( $items ) )
                {
                    $result = [] ;

                    foreach( $items as $id )
                    {
                        $media = $this->model->delete( $id );
                        if( $media )
                        {
                            $this->deleteMediaDependencies( $id , $media );
                            $this->deleteMediaContent( $media ) ;
                        }
                    }

                    $this->logger->info( $this . ' deleteAll(' . $list . ')' . ' count:' . $count ) ;

                    return $response ? $this->success( $response , $list ) : $list ;
                }
                else
                {
                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                }
            }
            catch( Exception $e )
            {
                $this->logger->warning( $this . ' deleteAll() failed, '  .$e->getMessage() ) ;
                return $this->formatError( $response , '500', [ 'deleteAll()' , $e->getMessage() ] , NULL , 500 );
            }
        }

        return $this->error( $response , 'The list is empty or null.' ) ;
    }

    /**
     * Returns the local uri of the specific photo image.
     * @param string $filename
     * @return string
     */
    public function uri( string $filename ) :string
    {
        $root = $this->root ;

        if( !is_dir( $root ) )
        {
            mkdir( $root , 0755 , TRUE ) ;
        }

        return $root
            . '/'
            . $filename ;
    }

    // --------------------------------

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     *
     * @OA\RequestBody(
     *     request="postMediaObject",
     *     @OA\MediaType(
     *         mediaType="multipart/form-data",
     *         @OA\Schema(
     *             oneOf={
     *                 @OA\Schema(
     *                     @OA\Property(property="file",type="string",format="binary",description=API_UPLOAD_MAXSIZE),
     *                     @OA\Property(property="name",type="string",description="The name of the resource"),
     *                     required={"file","name"},
     *                 ),
     *                 @OA\Schema(
     *                     @OA\Property(property="encodingFormat",type="string",description="The media type of the link"),
     *                     @OA\Property(property="embedUrl",type="string",format="uri",description="The embed url of the link"),
     *                     @OA\Property(property="name",type="string",description="The name of the resource"),
     *                     required={"name","encodingFormat","embedUrl"},
     *                     @OA\Property(property="thumbnailUrl",type="string",format="uri",description="The thumbnail url of the link"),
     *                 )
     *             },
     *             @OA\Property(property="description",ref="#/components/schemas/text"),
     *             @OA\Property(property="author",type="string",description="The author of the resource"),
     *             @OA\Property(property="alternativeHeadline",ref="#/components/schemas/text"),
     *             @OA\Property(property="editor",type="string",description="The editor of the resource"),
     *             @OA\Property(property="headline",ref="#/components/schemas/text"),
     *             @OA\Property(property="inLanguage",type="string",description="The inLanguage of the resource"),
     *             @OA\Property(property="license",type="string",description="The license of the resource"),
     *             @OA\Property(property="mentions",type="string",description="The mentions of the resource"),
     *             @OA\Property(property="publisher",type="string",description="The publisher of the resource"),
     *             @OA\Property(property="review",type="string",description="The review of the resource"),
     *             @OA\Property(property="text",ref="#/components/schemas/text"),
     *         ),
     *         encoding={"file"={"contentType"=API_UPLOAD_CONTENT_TYPE}}
     *     ),
     *     required=true
     * )
     *
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        try
        {
            $this->logger->debug( $this . ' post()' ) ;

            $container = $this->container ;

            $appId = $container->get('jwt')->aud ;

            if( $request )
            {
                $files  = $request->getUploadedFiles() ;
                $params = $request->getParsedBody() ;

                $filename = NULL ;

                if ( empty( $files ) )
                {
                    // check if it is a link media

                    if( isset( $params['encodingFormat'] ) && isset( $params['embedUrl'] ) )
                    {
                        $item = [];
                        $item['active'] = 1 ;
                        $item['withStatus'] = Status::PUBLISHED ;
                        $item['path'] = $this->path ;

                        $item['name'] = $params['name'] ;
                        $item['encodingFormat'] = $type = $params['encodingFormat'] ;
                        $item['embedUrl'] = $params['embedUrl'] ;

                        $availableMediaTypes = $this->getAvailableMediaTypes() ;

                        if( $availableMediaTypes && in_array( $type , $availableMediaTypes ) )
                        {
                            $conditions =
                            [
                                'name'           => [ $params['name']           , 'required|min(2)' ] ,
                                'encodingFormat' => [ $params['encodingFormat'] , 'required'        ] ,
                                'embedUrl'       => [ $params['embedUrl']       , 'required|url'    ]
                            ] ;

                            if( isset( $params['thumbnailUrl'] ) )
                            {
                                $item['thumbnailUrl'] = $params['thumbnailUrl'] ;
                                $conditions['thumbnailUrl'] = [ $params['thumbnailUrl'] , 'url'] ;
                            }

                            $mainType = explode( '/' , $type ) ;
                            $props = $this->getProperties( $mainType[0] , $item , $params , $conditions ) ;

                            $item = $props->item ;
                            $conditions = $props->conditions ;

                            $validator = new MediaObjectValidator( $container ) ;

                            $validator->validate( $conditions ) ;

                            if( $validator->passes() )
                            {
                                $inserted = $this->model->insert( $item ) ;

                                return $this->success( $response , $this->create( $inserted ) );
                            }
                            else
                            {
                                return $this->getValidatorError( $response ) ;
                            }

                        }
                        else
                        {
                            $this->logger->debug( $this . ' post() failed! Mismatch link type with authorized formats : ' . $type ) ;
                            return $this->formatError( $response , '400' , NULL , [ "message" => [ 'type' => 'The link type mismatch with authorized formats.' ] ] );
                        }
                    }

                    return $this->error( $response , ' post() failed, the $files is empty.' ) ;
                }
                else if ( !isset( $files['file'] ) )
                {
                    return $this->error( $response , ' post() failed, the $files["file"] is not set.' ) ;
                }
                else
                {
                    $file = $files['file'] ;
                    $error = $file->getError() ;

                    if( $error === UPLOAD_ERR_OK )
                    {
                        $filename = $file->getClientFilename();
                        $size     = $file->getSize();
                        $type     = $file->getClientMediaType();

                        // Really check the type of the file
                        $finfo     = finfo_open( FILEINFO_MIME_TYPE );
                        $checkType = finfo_file( $finfo , $file->getFilePath() );
                        finfo_close( $finfo );

                        // check filesize
                        $mainType = explode( '/' , $type ) ;

                        $getUploadMaxSize = $this->getUploadMaxSize( $mainType[0] ) ;

                        if( $getUploadMaxSize )
                        {
                            // in bytes
                            $limitSize = $getUploadMaxSize * 1048576 ;
                            if( $size > $limitSize )
                            {
                                return $this->formatError( $response , '400' , NULL , [ "message" => [ 'size' => 'The size of the file is too big.' ] ] );
                            }
                        }

                        // check if we need to transcode audio
                        $transcodedFile = $this->transcodeAudioToMp3( $response , $checkType , $file ) ;
                        if( $transcodedFile )
                        {
                            if( $transcodedFile instanceof Response )
                            {
                                return $transcodedFile ;
                            }

                            $type = 'audio/mpeg' ;
                        }

                        $availableMediaTypes = $this->getAvailableMediaTypes() ;

                        if( $availableMediaTypes && in_array( $type , $availableMediaTypes ) )
                        {
                            $item = [];
                            $item['active'] = 1 ;
                            $item['withStatus'] = Status::PUBLISHED ;
                            $item['path'] = $this->path ;

                            $conditions =
                            [
                                'name'   => [ $params['name'] , 'required|min(2)' ]
                            ] ;

                            if( isset( $params['name'] ) )
                            {
                                $item['name'] = $params['name'] ;
                            }

                            $props = $this->getProperties( $mainType[0] , $item , $params , $conditions ) ;

                            $item = $props->item ;
                            $conditions = $props->conditions ;

                            $validator = new MediaObjectValidator( $container ) ;

                            $validator->validate( $conditions ) ;

                            if( $validator->passes() )
                            {
                                if( $filename )
                                {
                                    $item['contentSize'] = $size ;
                                    $item['encodingFormat']  = $type ;

                                    $inserted = $this->model->insert( $item ) ;

                                    $newFilename = $container->get( Hash::class )
                                                             ->hash( $appId . $inserted->_key ) . '.' . strtolower( pathinfo( $filename , PATHINFO_EXTENSION ) ) ;

                                    $output = $this->uri( $newFilename ) ;

                                    if( $transcodedFile )
                                    {
                                        rename( $transcodedFile , $output ) ;
                                    }
                                    else
                                    {
                                        $file->moveTo( $output ) ;
                                    }

                                    $item = [] ;

                                    $item['contentUrl'] = $newFilename ;

                                    // get file metadata and other
                                    $item = $this->getFileMetadata( $mainType , $output , $item ) ;

                                    $updated = $this->model->update( $item , $inserted->_key ) ;

                                    return $this->success( $response , $this->create( $updated ) );

                                }
                                else
                                {
                                    return $this->formatError( $response , '404' , [ 'post()' , 'unknown error' ] , NULL , 404 );
                                }
                            }
                            else
                            {
                                return $this->getValidatorError( $response ) ;
                            }

                        }
                        else
                        {
                            $this->logger->debug( $this . ' post() failed! Mismatch type with authorized formats : ' . $type  . ' / ' . $checkType ) ;
                            return $this->formatError( $response , '400' , NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' , 'mime' => $checkType ] ] );
                        }
                    }
                    else
                    {
                        return $this->formatError( $response , '500', [ 'post()' , $this->getError( $error ) ] , NULL , 500 ) ;
                    }
                }
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchMediaObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="description",type="string",description="The name of the resource"),
     *             @OA\Property(property="author",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternativeHeadline",ref="#/components/schemas/text"),
     *             @OA\Property(property="editor",type="string",description="The name of the resource"),
     *             @OA\Property(property="headline",ref="#/components/schemas/text"),
     *             @OA\Property(property="inLanguage",type="string",description="The name of the resource"),
     *             @OA\Property(property="license",type="string",description="The name of the resource"),
     *             @OA\Property(property="mentions",type="string",description="The name of the resource"),
     *             @OA\Property(property="publisher",type="string",description="The name of the resource"),
     *             @OA\Property(property="review",type="string",description="The name of the resource"),
     *             @OA\Property(property="text",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' patch(' . $id . ')' ) ;

        $container = $this->container ;

        try
        {
            $media = $this->model->get( $id ) ;

            if( $media == null )
            {
                return $this->formatError( $response , '404', [ $this . ' patch(' . $id . ')' ] , NULL , 404 );
            }

            if( $request )
            {
                $params = $request->getParsedBody();


                $item = [] ;
                $conditions = [] ;

                if( isset( $params[ 'name' ] ) )
                {
                    $item[ 'name' ] = $params[ 'name' ] ;
                    $conditions[ 'name' ] = [ $params[ 'name' ] , 'required|min(2)' ] ;
                }

                $mainType = explode( '/' , $media->encodingFormat ) ;

                $props = $this->getProperties( $mainType[0] , $item , $params ,$conditions ) ;

                $item = $props->item ;
                $conditions = $props->conditions ;

                $validator = new MediaObjectValidator( $container ) ;

                $validator->validate( $conditions ) ;

                if( $validator->passes() )
                {
                    $update = $this->model->update( $item , $id );

                    if( $update )
                    {
                        $this->model->updateAllThings( $id , $mainType[0] ) ;
                        return $this->success( $response , $this->create( $update ) );

                    }
                    else
                    {
                        return $this->formatError( $response , '606' , [ 'patch(' . $id . ')' , 'unknown error' ] );
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }

            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    // --------------------------------

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        $iso8601  = $this->container->get( ISO8601::class ) ;
        $mediaUrl = $this->config['app']['mediaUrl'] ;
        $type     = explode( '/' , $init->encodingFormat ) ;

        switch( $type[0] )
        {
            case 'audio' :
            {
                $item = AudioObject::create( $init , $iso8601 , NULL , $lang ) ;
                break ;
            }
            case 'image' :
            {
                $item = ImageObject::create( $init , $iso8601 , NULL , $lang ) ;
                break ;
            }
            case 'video' :
            {

                $item = VideoObject::create( $init , $iso8601 , NULL , $lang ) ;
                break ;
            }
            default:
            {
                $item = MediaObject::create( $init , $iso8601 , NULL , $lang ) ;
                break;
            }
        }


        $item->url = $this->getUrl( $this->fullPath . '/' . $init->_key )  ;

//        $this->logger->debug  ( '---------------------' );
//        $this->logger->debug( $this . '::getBasePath : ' . $this->getBasePath() ); // /1.0
//        $this->logger->debug( $this . '::getBaseURL  : ' . $this->getBaseURL()  ) ;
//        $this->logger->debug( $this . '::getFullPath : ' . $this->getFullPath()     ) ;
//        $this->logger->debug( $this . '::getPath     : ' . $this->getPath()     ) ;
//        $this->logger->debug( $this . '::url       : ' . $item->url           ) ;

        //// set API url

        if( property_exists( $init , 'contentUrl' ) )
        {
            $item->contentUrl = $mediaUrl . $init->contentUrl ;
        }

        if( property_exists( $init , 'thumbnail') && array_key_exists( 'contentUrl' , $init->thumbnail ) )
        {
            $item->thumbnail['contentUrl'] = $mediaUrl . $init->thumbnail['contentUrl'] ;
        }

        if( property_exists( $init , 'source' ) && is_array( $init->source ) && count( $init->source ) > 0 )
        {
            for( $i = 0 ; $i < count( $init->source ) ; $i++ )
            {
                if( array_key_exists( 'contentUrl' , $init->source[$i] ) )
                {
                    $item->source[$i]['contentUrl'] = $mediaUrl . $init->source[$i]['contentUrl'] ;
                }
            }
        }

        return $item ;
    }

    private function convertToAac( $ffmpeg , $source , $output )
    {
        $codec = 'aac' ;

        $codecConfig = 'codec-' . $codec ;
        $codecSetting = isset( $this->config[$codecConfig] ) ? $this->config[$codecConfig] : [] ;

        $format = new Aac() ;

        // remove video, playback and set format
        //$format->setAdditionalParameters( [ '-vn' , '-movflags' , '+faststart' , '-f' , $codec ] ) ;

        // default rate 44100
        $defaultRate = array_key_exists('defaultSampleRate' , $codecSetting ) ? $codecSetting['defaultSampleRate'] : 44100 ;
        // max rate 48000 for Android
        $maxRate = array_key_exists('maxSampleRate' , $codecSetting ) ? $codecSetting['maxSampleRate'] : 48000 ;

        if( $source->has( 'sample_rate' ) )
        {
            if( $source->get( 'sample_rate' ) > $maxRate )
            {
                $ffmpeg->filters()->resample( $maxRate ) ;
            }
        }
        else
        {
            $ffmpeg->filters()->resample( $defaultRate ) ;
        }

        $this->logger->debug( $this . ' convertToAac command : ' . $ffmpeg->getFinalCommand( $format , $output ) ) ;

        $ffmpeg->save( $format , $output ) ;
    }

    private function convertToMp3( $ffmpeg , $source , $output )
    {
        $codec = 'mp3' ;

        $codecConfig = 'codec-' . $codec ;
        $codecSetting = isset( $this->config[$codecConfig] ) ? $this->config[$codecConfig] : [] ;

        $format = new Mp3();

        // max 2 channels
        $maxChannels = 2 ;
        if( $source->has( 'channels' ) )
        {
            if( $source->get( 'channels' ) > $maxChannels )
            {
                $format->setAudioChannels( $maxChannels ) ;
            }
        }
        else
        {
            $format->setAudioChannels( $maxChannels ) ;
        }

        // default bit rate 128
        $defaultBitRate = array_key_exists('defaultBitRate' , $codecSetting ) ? $codecSetting['defaultBitRate'] : 128 ;
        // max 320
        $maxBitRate = array_key_exists('maxBitRate' , $codecSetting ) ? $codecSetting['maxBitRate'] : 320 ;

        if( $source->has( 'bit_rate' ) )
        {
            if( $source->get( 'bit_rate' ) / 1000 > $maxBitRate )
            {
                $format->setAudioKiloBitrate( $maxBitRate ) ;
            }
        }
        else
        {
            $format->setAudioKiloBitrate( $defaultBitRate ) ;
        }

        // default rate 44100
        $defaultRate = array_key_exists('defaultSampleRate' , $codecSetting ) ? $codecSetting['defaultSampleRate'] : 44100 ;
        // max rate 48000 for Android
        $maxRate = array_key_exists('maxSampleRate' , $codecSetting ) ? $codecSetting['maxSampleRate'] : 48000 ;

        if( $source->has( 'sample_rate' ) )
        {
            if( $source->get( 'sample_rate' ) > $maxRate )
            {
                $ffmpeg->filters()->resample( $maxRate ) ;
            }
        }
        else
        {
            $ffmpeg->filters()->resample( $defaultRate ) ;
        }

        $this->logger->debug( $this . ' convertToMp3 command : ' . $ffmpeg->getFinalCommand( $format , $output ) ) ;

        $ffmpeg->save( $format , $output ) ;
    }

    private function deleteMediaDependencies( $id , $media )
    {
        try
        {
            if( $media )
            {
                $format = explode( '/' , $media->encodingFormat ) ;
                switch( $format[0] )
                {
                    case 'audio':
                    {
                        $this->model->deleteAudio( $id ) ;
                        $this->model->deleteAudios( $id ) ;
                        break ;
                    }
                    case 'image':
                    {
                        $this->model->deleteImage( $id ) ;
                        $this->model->deleteLogo( $id ) ;
                        $this->model->deletePhotos( $id ) ;
                        break ;
                    }
                    case 'video' :
                    {
                        $this->model->deleteVideo( $id ) ;
                        $this->model->deleteVideos( $id ) ;
                        break ;
                    }
                }
            }
        }
        catch( Exception $e )
        {
            $this->logger->warning( $this . ' deleteMediaDependencies(' . $id . ') failed, ' . $e->getMessage() ) ;
        }
    }

    private function deleteMediaContent( $media )
    {
        if( $media && isset( $media->contentUrl ) )
        {
            $file = $this->uri( $media->contentUrl );
            if( file_exists($file) )
            {
                unlink($file);
                array_map( 'unlink' , glob($file . ".*") ); // remove all source files
            }
        }
    }

    private function getAudioInfos( $file ) :array
    {
        $infos = [] ;

        try
        {
            $valid = $this->ffprobe->isValid( $file ) ;

            if( $valid )
            {
                $audios = $this->ffprobe->streams( $file )->audios();
                if( $audios->count() > 0 && $audios->first()->isAudio() )
                {
                    $audio = $audios->first() ;

                    // get bitrate
                    if( $audio->has( 'bit_rate' ) )
                    {
                        $infos['bitrate'] = (int) $audio->get( 'bit_rate' ) ;
                    }

                    // get channel
                    if( $audio->has( 'channel_layout' ) )
                    {
                        $infos['channels'] = $audio->get( 'channel_layout' ) ;
                    }

                    // get codec
                    if( $audio->has( 'codec_name' ) )
                    {
                        $infos['audioCodec'] = $audio->get( 'codec_name' ) ;
                    }

                    // get duration
                    if( $audio->has( 'duration' ) )
                    {
                        $infos['duration'] = (float) $audio->get( 'duration' ) ;
                    }

                    // get sample rate
                    if( $audio->has( 'sample_rate' ) )
                    {
                        $infos['sampleRate'] = (int) $audio->get( 'sample_rate' ) ;
                    }

                    // get bit per sample
                    if( $audio->has( 'bits_per_raw_sample' ) )
                    {
                        $infos['bitsPerSample'] = (int) $audio->get( 'bits_per_raw_sample' ) ;
                    }
                }
            }
            else
            {
                $this->logger->error( $this . ' getAudioInfos - Invalid audio file' ) ;
            }
        }
        catch( Exception $e )
        {
            $this->logger->error( $this . ' error reading audio infos - ' . $e->getMessage() ) ;
        }

        return $infos ;
    }

    /**
     * @return array
     */
    private function getAvailableMediaTypes()
    {
        $audios = $this->config['audios'] ;
        $files  = $this->config['files'] ;
        $images = $this->config['images'] ;
        $videos = $this->config['videos'] ;

        $availableMediaTypes = [] ;

        if( array_key_exists( 'formats' , $audios ) )
        {
            $availableMediaTypes = array_merge( $availableMediaTypes , $audios[ 'formats' ] ) ;
        }

        if( array_key_exists( 'formats' , $files ) )
        {
            $availableMediaTypes = array_merge( $availableMediaTypes , $files[ 'formats' ] ) ;
        }

        if( array_key_exists( 'formats' , $images ) )
        {
            $availableMediaTypes = array_merge( $availableMediaTypes , $images[ 'formats' ] ) ;
        }

        if( array_key_exists( 'formats' , $videos ) )
        {
            $availableMediaTypes = array_merge( $availableMediaTypes , $videos[ 'formats' ] ) ;
        }

        return $availableMediaTypes ;
    }

    /**
     * @param int $error
     *
     * @return string
     */
    private function getError( int $error )
    {
        switch( $error )
        {
            case 1 :
                $msg = "The uploaded file exceeds the upload_max_filesize directive in php.ini" ;
                break ;
            case 2 :
                $msg = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form" ;
                break ;
            case 3 :
                $msg = "The uploaded file was only partially uploaded" ;
                break ;
            case 4 :
                $msg = "No file was uploaded" ;
                break ;
            case 6 :
                $msg = "Missing a temporary folder" ;
                break ;
            case 7 :
                $msg = "Failed to write file to disk" ;
                break ;
            case 8 :
                $msg = "A PHP extension stopped the file upload" ;
                break ;
            default :
                $msg = "Unknown error" ;
                break ;
        }

        return $msg ;
    }

    private function getFileMetadata( $mainType , $output , $item )
    {
        $filename = $item['contentUrl'] ;

        $filetype = $mainType[0] . '/' . $mainType[1] ;

        switch( $mainType[0] )
        {
            case 'audio':

                $configAudios = isset( $this->config['audios'] ) ? $this->config['audios'] : [] ;

                $thumbnailFile = $output . '.png' ;

                $waveformHeight = 120 ;
                $waveformWidth  = 640 ;

                $audioAacExt    = '.m4a' ;
                $audioMp3Ext    = '.mp3' ;

                // get source infos
                $infos = $this->getAudioInfos( $output ) ;

                if( is_array( $infos ) && count( $infos ) > 0 )
                {
                    $item = array_merge( $item , $infos ) ;
                }

                try
                {
                    $valid = $this->ffprobe->isValid( $output ) ;

                    if( $valid )
                    {
                        $audios = $this->ffprobe->streams( $output )->audios() ;
                        if( $audios->count() > 0 && $audios->first()->isAudio() )
                        {
                            $audioFile = $this->ffmpeg->open( $output ) ;

                            // check if allowed to generate waveform
                            if( array_key_exists( 'generateWaveForm' , $configAudios ) && $configAudios['generateWaveForm'] == true )
                            {
                                // Create the waveform
                                $waveform = $audioFile->waveform( $waveformWidth , $waveformHeight ) ;
                                $waveform->save( $thumbnailFile );

                                // save thumbnail
                                $item['thumbnail'] =
                                [
                                    'encodingFormat' => 'image/png',
                                    'contentSize'    => filesize( $thumbnailFile ),
                                    'contentUrl'     => $filename . '.png',
                                    'height'         => $waveformHeight,
                                    'width'          => $waveformWidth
                                ] ;
                            }


                            // remove all metadata
                            $audioFile->filters()->addMetadata();

                            $source = [] ;

                            if( ( array_key_exists( 'convertToAac' , $configAudios ) && $configAudios['convertToAac'] == true )
                                && $filetype != 'audio/aac' && $filetype != 'audio/mp4' )
                            {
                                try
                                {
                                    // save aac

                                    $audioAac = $output . $audioAacExt;

                                    $this->convertToAac( $audioFile , $audios->first() , $audioAac ) ;

                                    $infos = $this->getAudioInfos( $audioAac ) ;

                                    $content =
                                    [
                                        'encodingFormat' => 'audio/mp4' ,
                                        'contentSize'    => filesize( $audioAac ) ,
                                        'contentUrl'     => $filename . $audioAacExt
                                    ];

                                    if( is_array( $infos ) && count( $infos ) > 0 )
                                    {
                                        $content = array_merge( $content , $infos ) ;
                                    }

                                    // save source
                                    $source[] = $content ;
                                }
                                catch( Exception $e )
                                {
                                    $this->logger->error( $this . ' error converting audio to aac - ' . $e->getMessage() ) ;
                                }
                            }

                            if( ( array_key_exists( 'convertToMp3' , $configAudios ) && $configAudios['convertToMp3'] == true )
                                && $filetype != 'audio/mpeg' )
                            {
                                try
                                {
                                    // save mp3

                                    $audioMp3 = $output . $audioMp3Ext;

                                    $this->convertToMp3($audioFile , $audios->first() , $audioMp3 ) ;

                                    $infos = $this->getAudioInfos( $audioMp3 ) ;

                                    $content =
                                    [
                                        'encodingFormat' => 'audio/mpeg' ,
                                        'contentSize'    => filesize($audioMp3) ,
                                        'contentUrl'     => $filename . $audioMp3Ext
                                    ];

                                    if( is_array( $infos ) && count( $infos ) > 0 )
                                    {
                                        $content = array_merge( $content , $infos ) ;
                                    }

                                    // save source
                                    $source[] = $content ;
                                }
                                catch( Exception $e )
                                {
                                    $this->logger->error( $this . ' error converting audio to mp3 - ' . $e->getMessage() ) ;
                                }

                            }

                            if( is_array( $source ) && count( $source ) > 0 )
                            {
                                $item['source'] = $source ;
                            }
                        }
                    }
                }
                catch( Exception $e )
                {
                    $this->logger->error( $this . ' error creating audio - ' . $e->getMessage() ) ;
                }

                break ;
            case 'image':

                if( $filetype != 'image/webp' )
                {
                    // create webP
                    try
                    {
                        $webpExt = '.webp' ;
                        $webpFile = $output . $webpExt ;

                        exec("cwebp " . $output . " -o " . $webpFile ) ;

                        $webpSize = getimagesize( $webpFile ) ;

                        // save to source
                        $item['source'] =
                        [
                            [
                                'encodingFormat' => 'image/webp',
                                'contentSize'    => filesize( $webpFile ),
                                'contentUrl'     => $filename . $webpExt,
                                'height'         => is_array( $webpSize ) ? $webpSize[1] : null ,
                                'width'          => is_array( $webpSize ) ? $webpSize[0] : null ,
                            ]
                        ] ;
                    }
                    catch( Exception $e )
                    {
                        $this->logger->error( $this . ' error creating webp file - ' . $e->getMessage() ) ;
                    }
                }
                else
                {
                    // convert wepb image to png
                    try
                    {
                        $pngExt = '.png' ;
                        $pngFile = $output . $pngExt ;

                        exec("dwebp " . $output . " -o " . $pngFile ) ;

                        $pngSize = getimagesize( $pngFile ) ;

                        // save to source
                        $item['source'] =
                        [
                            [
                                'encodingFormat' => 'image/png',
                                'contentSize'    => filesize( $pngFile ),
                                'contentUrl'     => $filename . $pngExt,
                                'height'         => is_array( $pngSize ) ? $pngSize[1] : null ,
                                'width'          => is_array( $pngSize ) ? $pngSize[0] : null ,
                            ]
                        ] ;
                    }
                    catch( Exception $e )
                    {
                        $this->logger->error( $this . ' error creating png file from webp - ' . $e->getMessage() ) ;
                    }
                }


                // get image size
                $size = getimagesize( $output ) ;

                if( $size && is_array( $size ) )
                {
                    $item['height'] = $size[1] ;
                    $item['width']  = $size[0] ;
                }

                // get exif
                if( $filetype == 'image/jpeg' )
                {
                    $exif = exif_read_data( $output , 0 , true ) ;

                    if( $exif )
                    {
                        // CLEAN characters
                        foreach( $exif as $key1 => $value1 )
                        {
                            foreach( $value1 as $key2 => $value2 )
                            {
                                //
                                $exif[$key1][$key2] = Encoding::toUTF8( $value2 ) ;
                            }
                        }

                        $item['exifData'] = $exif ;

                        if( array_key_exists( 'IFD0' , $exif ) )
                        {
                            if( array_key_exists( 'Artist' , $exif['IFD0'] ) )
                            {
                                $item['author'] = $exif['IFD0']['Artist'] ;
                            }

                            if( array_key_exists( 'Copyright' , $exif['IFD0'] ) )
                            {
                                $item['license'] = $exif['IFD0']['Copyright'] ;
                            }
                        }
                    }
                }

                break ;
            default:

                break ;
        }

        try
        {
            // check XMP metadata
            $xmp = new Xmp() ;
            $xmpData = $xmp->extractFromFile( $output ) ;

            if( $xmpData && array_key_exists( 'rdf:RDF' , $xmpData ) )
            {
                $rdf = $xmpData['rdf:RDF'] ;

                if( array_key_exists( 'rdf:Description' , $rdf ) )
                {
                    $desc = $rdf['rdf:Description'] ;

                    if( array_key_exists( 'dc:creator' , $desc ) )
                    {
                        $item['author'] = $desc['dc:creator'] ;
                    }

                    if( array_key_exists( 'dc:rights' , $desc ) && array_key_exists( '@content' , $desc['dc:rights'] ) )
                    {
                        $item['license'] = $desc['dc:rights']['@content'] ;
                    }

                    if( array_key_exists( '@attributes' , $desc ) )
                    {
                        $attr = $desc['@attributes'] ;

                        if( array_key_exists( 'WebStatement' , $attr ) )
                        {
                            $item['license'] = $attr['WebStatement'] ;
                        }
                    }
                }
            }
        }
        catch( Exception $e )
        {
            $this->logger->error( $this . ' error extracting XMP metadata - ' . $e->getMessage() ) ;
        }

        return $item ;
    }

    /**
     * @param string $mainType
     *
     * @return int|null
     */
    private function getUploadMaxSize( string $mainType )
    {
        $maxUploadSize = NULL ;

        $setting = $this->config ;

        if( isset( $setting['upload'] )
            && array_key_exists( 'maxsize' , $setting['upload'] ) )
        {
            $maxUploadSize = $setting['upload']['maxsize'] ;
        }

        switch( $mainType )
        {
            case 'audio' :
                if( isset( $setting['audios'] )
                    && array_key_exists( 'maxsize' , $setting['audios'] ) )
                {
                    $maxUploadSize = $setting['audios']['maxsize'] ;
                }
                break ;
            case 'image' :
                if( isset( $setting['images'] )
                    && array_key_exists( 'maxsize' , $setting['images'] ) )
                {
                    $maxUploadSize = $setting['images']['maxsize'] ;
                }
                break ;
            case 'video' :
                if( isset( $setting['videos'] )
                    && array_key_exists( 'maxsize' , $setting['videos'] ) )
                {
                    $maxUploadSize = $setting['videos']['maxsize'] ;
                }
                break ;
        }

        return $maxUploadSize ;
    }

    /**
     * @param string $mainType
     * @param array $item
     * @param array $params
     * @param array $conditions
     *
     * @return object
     */
    private function getProperties( string $mainType , array $item , array $params , array $conditions )
    {
        if( isset( $params[ 'description' ] ) )
        {
            $item[ 'description' ] = $this->translate( $params[ 'description' ] ) ;
        }

        if( isset( $params[ 'author' ] ) )
        {
            $item[ 'author' ] = $params[ 'author' ] ;
            $conditions[ 'author' ] = [ $params[ 'author' ] , 'min(2)' ] ;
        }

        if( isset( $params[ 'alternativeHeadline' ] ) )
        {
            $item[ 'alternativeHeadline' ] = $this->translate( $params[ 'alternativeHeadline' ] ) ;
        }

        if( isset( $params[ 'editor' ] ) )
        {
            $item[ 'editor' ] = $params[ 'editor' ] ;
        }

        if( isset( $params[ 'headline' ] ) )
        {
            $item[ 'headline' ] = $this->translate( $params[ 'headline' ] ) ;
        }

        if( isset( $params[ 'inLanguage' ] ) )
        {
            $item[ 'inLanguage' ] = $params[ 'inLanguage' ] ;
        }

        if( isset( $params[ 'keywords' ] ) )
        {
            $keywordsIn = explode( ',' , $params[ 'keywords' ] ) ;
            $keywords = [] ;
            foreach( $keywordsIn as $keyword )
            {
                $keyword = strtolower( trim( $keyword ) ) ;
                if( $keyword != '' ) $keywords[] = $keyword ;
            }
            $item[ 'keywords' ] = $keywords ;
        }

        if( isset( $params[ 'license' ] ) )
        {
            $item[ 'license' ] = $params[ 'license' ] ;
            $conditions[ 'license' ] = [ $params[ 'license' ] , 'min(2)' ] ;
        }

        if( isset( $params[ 'mentions' ] ) )
        {
            $item[ 'mentions' ] = $params[ 'mentions' ] ;
        }

        if( isset( $params[ 'publisher' ] ) )
        {
            $item[ 'publisher' ] = $params[ 'publisher' ] ;
            $conditions[ 'publisher' ] = [ $params[ 'publisher' ] , 'min(2)' ] ;
        }

        if( isset( $params[ 'review' ] ) )
        {
            $item[ 'review' ] = $params[ 'review' ] ;
        }

        if( isset( $params[ 'text' ] ) )
        {
            $item[ 'text' ] = $this->translate( $params[ 'text' ] ) ;
        }

        switch( $mainType )
        {
            case 'audio' :

                if( isset( $params[ 'transcript' ] ) )
                {
                    $item[ 'transcript' ] = $params[ 'transcript' ] ;
                }

                break ;
            case 'image' :

                if( isset( $params[ 'caption' ] ) )
                {
                    $item[ 'caption' ] = $params[ 'caption' ] ;
                }

                break ;
            case 'video' :

                if( isset( $params[ 'actor' ] ) )
                {
                    $item[ 'actor' ] = $params[ 'actor' ] ;
                }

                if( isset( $params[ 'caption' ] ) )
                {
                    $item[ 'caption' ] = $params[ 'caption' ] ;
                }

                if( isset( $params[ 'director' ] ) )
                {
                    $item[ 'director' ] = $params[ 'director' ] ;
                }

                if( isset( $params[ 'musicBy' ] ) )
                {
                    $item[ 'musicBy' ] = $params[ 'musicBy' ] ;
                }

                if( isset( $params[ 'transcript' ] ) )
                {
                    $item[ 'transcript' ] = $params[ 'transcript' ] ;
                }

                break ;
            default :
                break ;
        }

        return (object)[ 'item' => $item , 'conditions' => $conditions ] ;
    }

    /**
     * @param Response $response
     * @param string $checkType
     * @param $file
     *
     * @return Response|string|null
     */
    private function transcodeAudioToMp3( Response $response , string $checkType , $file )
    {
        $fil = $this->config['files'] ;

        $filename = $file->getClientFilename();
        $type     = $file->getClientMediaType();

        $transcodedFile = NULL ;

        if( ( ( $type == 'audio/mp3' || $type == 'audio/mpeg' || $type == 'audio/x-mpeg-3' )
                && $checkType == 'application/octet-stream' ) || ( $type == 'audio/mp3' && $checkType == 'audio/mpeg' ) )
        {
            try
            {
                $valid = $this->ffprobe->isValid( $file->file ) ;

                if( $valid )
                {
                    $audios = $this->ffprobe->streams( $file->file )->audios() ;
                    if( $audios->count() > 0 && $audios->first()->isAudio() )
                    {
                        $audio = $audios->first() ;

                        $audioFile = $this->ffmpeg->open( $file->file ) ;

                        // remove all metadata
                        $audioFile->filters()->addMetadata();

                        if( !file_exists( $fil['tmp'] ) )
                        {
                            mkdir( $fil['tmp'] ) ;
                        }

                        $transcodedFile = $fil['tmp'] . '/transcoded_' . $filename ;

                        $this->convertToMp3( $audioFile , $audio , $transcodedFile ) ;
                    }
                    else
                    {
                        return $this->formatError( $response , '400' , NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' , 'mime' => $checkType ] ] );
                    }
                }
                else
                {
                    return $this->formatError( $response , '400' , NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' , 'mime' => $checkType ] ] );
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500' , [ 'transcodeAudioToMp3()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else if( ( $type == 'audio/x-flac' && $checkType == 'audio/flac' ) || ( $type == 'audio/flac' && $checkType == 'audio/x-flac' ) )
        {
            $this->logger->debug( $this . ' transcodeAudioToMp3() - flac type difference') ; // do nothing
        }
        else if( ( $type == 'audio/x-m4a' && $checkType == 'audio/mp4' ) || ( $type == 'audio/mp4' && $checkType == 'audio/x-m4a' ) )
        {
            $this->logger->debug( $this . ' transcodeAudioToMp3() - acc type difference') ; // do nothing
        }
        else if( ( $type == 'audio/x-wav' && $checkType == 'audio/wav' ) || ( $type == 'audio/wav' && $checkType == 'audio/x-wav' ) )
        {
            $this->logger->debug( $this . ' transcodeAudioToMp3() - wav type difference') ; // do nothing
        }
        else if( $type !== $checkType )
        {
            $this->logger->debug( $this . ' transcodeAudioToMp3() failed! Mismatch type : ' . $type . ' / ' . $checkType ) ;
            return $this->formatError( $response , '400' , NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' , 'mime' => $checkType ] ] );
        }

        return $transcodedFile ;
    }
}
