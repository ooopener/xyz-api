<?php

namespace xyz\ooopener\controllers\creativeWork\mediaObject;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;



use Psr\Container\ContainerInterface;

class ImageObjectsController extends CollectionsController
{
    /**
     * Creates a new ImageObjectsController instance.
     *
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = 'imageObjects' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * @OA\Schema(
     *     schema="ImageObject",
     *     allOf={@OA\Schema(ref="#/components/schemas/MediaObject")},
     *     @OA\Property(type="string",property="caption",description="The caption for this object"),
     *     @OA\Property(type="string",property="exifData",description="exif data for this object"),
     *     @OA\Property(type="string",property="representativeOfPage",description="Indicates whether this image is representative of the content of the page"),
     *     @OA\Property(type="string",property="thumbnail",description="Thumbnail image for an image or video")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            =>  [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          =>  [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           =>  [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       =>  [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      =>  [ 'filter' => Thing::FILTER_DATETIME ] ,

        'position'      => [ 'filter' => Thing::FILTER_UNIQUE_NAME , 'skins' => [ 'extend' ] ] ,

        'headline'            =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'alternativeHeadline' =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'         =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'                =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'author'              =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encoding'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encodingFormat'      =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'inLanguage'          =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'keywords'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'license'             =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'mentions'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'publisher'           =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'review'              =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'source'              =>  [ 'filter' => Thing::FILTER_MEDIA_SOURCE ] ,
        'thumbnailUrl'        =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'contentSize'         =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'contentUrl'          =>  [ 'filter' => Thing::FILTER_MEDIA_URL ] ,
        'editor'              =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'width'               =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'height'              =>  [ 'filter' => Thing::FILTER_INT       ] ,

        'caption'              =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'exifData'             =>  [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ],
        'representativeOfPage' =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'thumbnail'            =>  [ 'filter' => Thing::FILTER_MEDIA_THUMBNAIL ]
    ];
}
