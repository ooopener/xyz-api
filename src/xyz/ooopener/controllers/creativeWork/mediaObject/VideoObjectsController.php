<?php

namespace xyz\ooopener\controllers\creativeWork\mediaObject;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;



use Psr\Container\ContainerInterface;

class VideoObjectsController extends CollectionsController
{
    /**
     * Creates a new VideoObjectsController instance.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = 'videoObjects' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * @OA\Schema(
     *     schema="VideoObject",
     *     allOf={@OA\Schema(ref="#/components/schemas/MediaObject")},
     *     @OA\Property(type="string",property="actor",description="An actor, e.g. in tv, radio, movie, video games etc., or in an event"),
     *     @OA\Property(type="string",property="caption",description="The caption for this object"),
     *     @OA\Property(type="string",property="director",description="A director of e.g. tv, radio, movie, video gaming etc. content, or of an event"),
     *     @OA\Property(type="string",property="musicBy",description="The composer of the soundtrack"),
     *     @OA\Property(type="string",property="thumbnail",description="Thumbnail image for an image or video"),
     *     @OA\Property(type="string",property="transcript",description="If this MediaObject is an AudioObject or VideoObject, the transcript of that object"),
     *     @OA\Property(type="string",property="videoFrameSize",description="The frame size of the video"),
     *     @OA\Property(type="string",property="videoQuality",description="The quality of the video")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'       => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'     => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'      => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'  => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified' => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'position' => [ 'filter' => Thing::FILTER_UNIQUE_NAME , 'skins' => [ 'extend' ] ] ,

        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'                => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'author'         => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encoding'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encodingFormat' => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'inLanguage'     => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'keywords'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'license'        => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'mentions'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'publisher'      => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'review'         => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'source'         => [ 'filter' => Thing::FILTER_MEDIA_SOURCE ] ,
        'thumbnailUrl'   => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'bitrate'     => [ 'filter' => Thing::FILTER_INT       ] ,
        'contentSize' => [ 'filter' => Thing::FILTER_INT       ] ,
        'contentUrl'  => [ 'filter' => Thing::FILTER_MEDIA_URL ] ,
        'duration'    => [ 'filter' => Thing::FILTER_FLOAT     ] ,
        'editor'      => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'embedUrl'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'playerType'  => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'width'       => [ 'filter' => Thing::FILTER_INT       ] ,
        'height'      => [ 'filter' => Thing::FILTER_INT       ] ,

        'actor'          => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'audioCodec'     => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'bitsPerSample'  => [ 'filter' => Thing::FILTER_INT       ] ,
        'caption'        => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'channels'       => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'director'       => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'musicBy'        => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'sampleRate'     => [ 'filter' => Thing::FILTER_INT       ] ,
        'thumbnail'      => [ 'filter' => Thing::FILTER_MEDIA_THUMBNAIL   ] ,
        'transcript'     => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'videoFrameSize' => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'videoQuality'   => [ 'filter' => Thing::FILTER_DEFAULT   ]
    ];
}
