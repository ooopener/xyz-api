<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\helpers\Status;
use xyz\ooopener\models\Collections;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;

use Exception ;
use xyz\ooopener\things\Thing;

/**
 * The status controller class.
 *
 * @OA\Schema(
 *     schema="successWithStatus",
 *     allOf={},
 *     @OA\Property(property="withStatus",ref="#/components/schemas/withStatus"),
 *     @OA\Property(property="modified",type="string",format="date-time"),
 *     @OA\Property(property="created",type="string",format="date-time"),
 *     @OA\Property(property="id",type="integer"),
 *     @OA\Property(property="url",type="string",format="uri")
 * )
 *
 * @OA\Schema(
 *     schema="withStatus",
 *     type="string",
 *     description="Status of the resource",
 *     enum={
 *         "anonymized",
 *         "archived",
 *         "draft",
 *         "published",
 *         "rejected",
 *         "unpublished",
 *         "under_review",
 *     },
 *     default="draft"
 * )
 *
 * @OA\Parameter(
 *     name="status",
 *     in="path",
 *     description="status of the resource",
 *     required=true,
 *     @OA\Schema(ref="#/components/schemas/withStatus")
 * )
 *
 */
class ThingStatusController extends CollectionsController
{
    /**
     * Creates a new ThingStatusController instance.
     * @param ContainerInterface $container The DI container reference of this controller.
     * @param Collections|NULL $model The model reference of this controller.
     * @param string|NULL $path The path expression of this controller.
     * @param string|NULL $statusName The name of the status property (default withStatus)
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , string $path = NULL , string $statusName = 'withStatus' )
    {
        parent::__construct( $container , $model , $path );
        $this->statusName = $statusName ;
    }


    /**
     * The name of the status property.
     */
    public string $statusName ;

    const CREATE_PROPERTIES =
    [
        'id'         => [ 'filter' => Thing::FILTER_ID       ] ,
        'url'        => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'    => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'   => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'withStatus' => [ 'filter' => Thing::FILTER_DEFAULT  ]
    ];

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Returns if the specific item is active.
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] ):Response
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ') ' . $_SERVER['REQUEST_URI'] ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;

            if( !$item )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            return $this->success( $response , $item ,  $this->getUrl( $this->fullPath . '/' . $item->id . '/' . $this->statusName ) ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }


    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'     => NULL ,
        'status' => NULL
    ] ;

    /**
     * Switch the activity of the specific item.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] ):Response
    {
        [
            'id'     => $id ,
            'status' => $status
        ]
        = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ',' . $status . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => '_key,withStatus' ] ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $id . ',' . $status . ')' ] , NULL , 404 );
            }

            $allowed = [] ;

            switch( $item->withStatus )
            {
                case Status::ANONYMIZED : // can't modify
                {
                    break ;
                }
                case Status::ARCHIVED :
                {
                    $allowed = [ Status::ANONYMIZED , Status::UNPUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                }
                case Status::DRAFT :
                {
                    $allowed = [ Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                }
                case Status::PUBLISHED :
                {
                    $allowed = [ Status::ANONYMIZED , Status::ARCHIVED , Status::DRAFT , Status::UNPUBLISHED ] ;
                    break ;
                }
                case Status::REJECTED :
                {
                    $allowed = [ Status::DRAFT , Status::UNDER_REVIEW ] ;
                    break ;
                }
                case Status::UNDER_REVIEW :
                {
                    $allowed = [ Status::DRAFT , Status::PUBLISHED , Status::REJECTED ] ;
                    break ;
                }
                case Status::UNPUBLISHED :
                {
                    $allowed = [ Status::ARCHIVED , Status::DRAFT , Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                }
                default :
                {
                    $allowed = [ Status::DRAFT , Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                }
            }

            $withStatus = strtolower( $status ) ;

            if( !in_array( $withStatus , $allowed ) )
            {
                return $this->formatError( $response , '403', [ 'patch(' . $id . ',' . $status . ')' ] );
            }

            $result = $this->model->update( [ 'withStatus' => $withStatus ] , $id ) ;

            if( $result )
            {
                $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
                return $this->success( $response , $item , $this->getUrl( $this->fullPath . '/' . $item->id . '/' . $this->statusName ) ) ;
            }

            return $this->error( $response , 'error' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ',' . $status . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}


