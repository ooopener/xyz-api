<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Collections;

use xyz\ooopener\things\Thing;
use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Container\ContainerInterface;
use Slim\HttpCache\CacheProvider;

class MultiFieldController extends CollectionsController
{
    /**
     * Creates a new CollectionsController instance.
     *
     * @param ContainerInterface $container
     * @param Collections|null $model
     * @param null $path
     * @param null $children
     * @param null $parent
     * @param null $num
     * @param null $tables
     */
    public function __construct( ContainerInterface $container , Collections $model = NULL , $path = NULL , $children = NULL , $parent = NULL , $num = NULL , $tables = NULL )
    {
        parent::__construct( $container , $model , $path );
        $this->num      = $num ;
        $this->children = $children ;
        $this->parent   = $parent ;
        $this->tables   = $tables ;
    }

    public $children ;

    public $num ;

    public $parent ;

    public $tables ;

    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ];

    /**
     * Delete item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ', ' . $this->children . ',' . $id  . ')' ) ;
        }

        try
        {
            // checks owner exists and contains id
            if( !$this->model->existsInMultiField( $owner , $id , [ 'field' => $this->children ] ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ', ' . $this->children . ',' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->deleteInMultiField( $owner , $id , [ 'field' => $this->children , 'num' => $this->num ] ) ;

            if( $result )
            {
                // update parent
                $updateParent = $this->model->updateDateParentMultiField( $this->path . '/' . $owner , [ 'key' => $this->children ] ) ;

                return $this->success( $response , $this->get( NULL , NULL , [ 'id' => $owner ] ) );
            }

            return $this->error( $response , 'error' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ', ' . $this->children . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteReverse(' . $id . ', ' . $this->children . ')' ) ;
        }

        try
        {
            $result = $this->model->deleteReverseInMultiField( $id , [ 'field' => $this->children , 'num' => $this->num ] ) ;

            if( $result )
            {
                if( $response )
                {
                    return $this->success( $response , 'ok' );
                }

                return true ;
            }

            if( $response )
            {
                return $this->error( $response , 'error' ) ;
            }

            return false ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteReverse(' . $id . ', ' . $this->children . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'conditions' => [] ,
        'id'      => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL
    ] ;

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'conditions' => $conditions ,
            'id'         => $id ,
            'item'       => $item ,
            'lang'       => $lang ,
            'params'     => $params
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ', ' . $this->children . ')' ) ;
        }

        // ------------ init

        $api = $this->config['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }

            }

        }

        try
        {
            $fields =
            [
                'modified' => [ 'filter' => Thing::FILTER_DATETIME ],
                $this->children => [ 'filter' => Thing::FILTER_JOIN_MULTIPLE ]
            ] ;

            $item = $this->model->get( $id , [ 'conditions' => $conditions , 'lang' => $lang , 'queryFields' => $fields ] ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ', ' . $this->children . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                // add header
                $cache = $this->container->get( CacheProvider::class ) ;
                $response = $cache->withETag( $response , $item->modified );
                $response = $cache->withLastModified( $response , $item->modified );

                return $this->success
                (
                    $response,
                    $item->{ $this->children },
                    $this->getCurrentPath( $request , $params )
                );
            }

            return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;

        }

        if( $item )
        {
            $item = $item->{ $this->children } ;
        }

        return $item ;
    }

    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'       => NULL,
        'owner'    => NULL,
        'position' => 0
    ];

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'       => $id,
            'owner'    => $owner,
            'position' => $position
        ]
        = array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $owner . ', ' . $this->children . ',' . $id  . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        try
        {
            // checks owner exists and contains id
            //if( !$this->model->existsInMultiField( $owner , $id , [ 'field' => $this->children ] ) )
            if( !$this->model->exist( $owner ) )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $owner . ', ' . $this->children . ',' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $params['position'] ) && (int) $params['position'] >= 0 )
            {
                $position = (int) $params['position'] ;
            }

            $result = $this->model->updateInMultiField( $owner , $id , [ 'field' => $this->children , 'num' => $this->num , 'position' => $position ] ) ;

            if( $result )
            {
                return $this->success( $response , $this->get( NULL , NULL , [ 'id' => $owner ] ) );
            }

            return $this->error( $response , 'error' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $owner . ', ' . $this->children . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    const ARGUMENTS_PATCH_REVERSE_DEFAULT =
    [
        'from'     => NULL,
        'owner'    => NULL,
        'position' => 0,
        'to'       => NULL
    ];

    public function patchReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'from'     => $from,
            'owner'    => $owner,
            'position' => $position,
            'to'       => $to
        ] = array_merge(self::ARGUMENTS_PATCH_REVERSE_DEFAULT , $args);

        if( $response )
        {
            $this->logger->debug( $this . ' patchReverse(' . $owner . ', ' . $this->parent . ',' . $from . ',' . $to . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        try
        {
            //
            $fromAr = explode( '/' , $from ) ;
            $toAr   = explode( '/' , $to ) ;

            if( count($fromAr) != 2 || count($toAr) != 2 )
            {
                return $this->formatError( $response , '400', [ 'patchReverse(' . $owner . ', ' . $this->parent . ',' . $from . ',' . $to . ')' ] , NULL , 400 );
            }

            $item = $this->path . '/' . $owner ;

            // checks from exists and contains owner
            if( !$this->container->get( $fromAr[0] )->existsInMultiField( $fromAr[1] , $item , [ 'field' => $this->children ] ) )
            {
                return $this->formatError( $response , '404', [ 'patchReverse(' . $owner . ', ' . $this->parent . ',' . $from . ',' . $to . ')' ] , NULL , 404 );
            }

            // checks to exists
            if( !$this->container->get( $toAr[0] )->exist( $toAr[1] ) )
            {
                return $this->formatError( $response , '404', [ 'patchReverse(' . $owner . ', ' . $this->parent . ',' . $from . ',' . $to . ')' ] , NULL , 404 );
            }

            $resultDelete = $this->container->get( $fromAr[0] )->deleteInMultiField( $fromAr[1] , $item , [ 'field' => $this->children , 'num' => $this->num ] ) ;

            $result = $this->container->get( $toAr[0] )->updateInMultiField( $toAr[1] , $item , [ 'field' => $this->children , 'num' => $this->num , 'position' => $position ] ) ;

            if( $resultDelete && $result )
            {
                return $this->success( $response , 'ok' );
            }

            return $this->error( $response , 'error' ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchReverse(' . $owner . ', ' . $this->parent . ',' . $from . ',' . $to . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    const ARGUMENTS_POST_DEFAULT =
    [
        'id'       => NULL,
        'owner'    => NULL,
        'side'     => 'left'
    ];

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'       => $id,
            'owner'    => $owner,
            'side'     => $side
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ', ' . $this->children . ',' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        try
        {
            // check id exists
            $res = explode('/' , $id ) ;
            if( !$res || count($res) != 2 || !$this->container->has( $res[0] ) )
            {
                return $this->formatError( $response , '400', [ 'post(' . $owner . ', ' . $this->children . ',' . $id . ')' ] , NULL , 400 );
            }

            if( !$this->container->get( $res[0] )->exist( $res[1] ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ', ' . $this->children . ',' . $id . ')' ] , NULL , 404 );
            }

            // check owner exists and not contains id
            $o = $this->model->get( $owner , [ 'fields' => 'numPart' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ', ' . $this->children . ',' . $id . ')' ] , NULL , 404 );
            }

            // check side
            if( isset( $params['side'] ) && $params['side'] == 'right' )
            {
                $side = 'right' ;
            }

            // insert id in multi field

            $result = $this->model->insertInMultiField( $owner , $id , [ 'field' => $this->children , 'num' => $this->num , 'side' => $side ] ) ;

            if( $result )
            {
                // update parent
                $updateParent = $this->model->updateDateParentMultiField( $this->path . '/' . $owner , [ 'key' => $this->children ] ) ;

                return $this->success( $response , $this->get( NULL , NULL , [ 'id' => $owner ] ) );
            }

            return $this->error( $response , 'error' ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ', ' . $this->children . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
