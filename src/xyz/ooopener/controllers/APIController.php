<?php

namespace xyz\ooopener\controllers;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * The application controller class.
 */
class APIController extends Controller
{
    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index( Request $request = NULL, Response $response = NULL , array $args = [] ) : ?Response
    {
        $this->logger->debug( $this . ' index' ) ;
        return $this->view( $response , 'index' , 'REST API - version ' . __VERSION__ ) ;
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed
     */
    public function info( Request $request = NULL, Response $response = NULL , array $args = [] ) : ?Response
    {
        $this->logger->debug( $this . ' info()' ) ;
        phpinfo();
        return $response ;
    }

    public function love( Request $request = NULL, Response $response = NULL , array $args = [] ) : ?Response
    {
        $this->logger->debug( $this . ' love()' ) ;
        return $response ;
    }

    /**
     * Indicates the version of the API.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function version( Request $request = NULL , Response $response = NULL, array $args = [] ) : ?Response
    {
        $this->logger->debug( $this . ' version ' . __VERSION__ ) ;
        return $this->success( $response , __VERSION__ , $this->getCurrentPath( $request ) );
    }

    ////////////////////////////////

    /**
     * Display a specific view interface.
     *
     * @param Response|NULL $response
     * @param string $name
     * @param string $title
     * @param ?array $options
     * @param string $template
     *
     * @return mixed
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function view( Response $response = NULL, $name = '', $title = '' , array $options = NULL , $template = 'view.twig' ) : ?Response
    {
        $this->logger->debug( $this . ' view name:' . $name . " title:" . $title ) ;

        $params =
        [
            'name'    => $name ,
            'title'   => $title,
            'theme'   => $this->config['strapdownjs']['theme'] ,
            'version' => __VERSION__
        ] ;

        if( isset( $options ) )
        {
            $params = array_merge( $params , $options ) ;
        }

        return $this->render( $response , $template , $params );
    }
}

