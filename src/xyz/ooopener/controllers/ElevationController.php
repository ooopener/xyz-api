<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\models\Model;
use xyz\ooopener\validations\GeoValidator;


use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface ;

/**
 * The geo coordinates controller.
 *
 * @OA\Schema(
 *     schema="Elevation",
 *     description="The elevation attribute of a thing",
 *     type="object",
 *     allOf={},
 *     @OA\Property(property="minValue",type="number",description="The minimum elevation value"),
 *     @OA\Property(property="maxValue",type="number",description="The maximum elevation value"),
 *     @OA\Property(property="gain",type="number",description="The gain value")
 *     @OA\Property(property="loss",type="number",description="The loss value")
 * )
 */
class ElevationController extends Controller
{
    /**
     * Creates a new ElevationController instance.
     * @param ContainerInterface $container
     * @param ?Model $model
     * @param string $name
     */
    public function __construct( ContainerInterface $container , ?Model $model = NULL , string $name = 'elevation' )
    {
        parent::__construct( $container );
        $this->model = $model ;
        $this->name  = $name ;
    }

    /**
     * The model reference.
     */
    public ?Model $model;

    /**
     * The name of the thing attribute.
     */
    public string $name ;

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Returns the Elevation reference of the specific item reference.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object the Elevation reference of the specific item reference.
     */
    public function get( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_GET_DEFAULT , is_array($args) ? $args : [] ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ') ' ) ;
        }

        $item = NULL ;

        try
        {

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $item = $this->model->get( $id , [ 'fields' => $this->name ] ) ;

            $item = ( $item && property_exists( $item , $this->name  ) ) ? $item->{ $this->name } : NULL ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return $response
             ? $this->success( $response, $item, $this->getCurrentPath( $request ) )
             : $item ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL ,
    ] ;

    /**
     * Update the Elevation reference of the specific item reference.
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object the Elevation reference of the specific item reference.
     *
     * @OA\RequestBody(
     *     request="patchGeo",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(ref="#/components/schemas/Elevation")
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PATCH_DEFAULT , is_array($args) ? $args : [] ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            $params = $request->getParsedBody() ;

            $item       = [];
            $conditions = [] ;

            if( isset( $params[ 'minValue' ] ) )
            {
                if(  $params['minValue'] != '' )
                {
                    $item[ 'minValue' ]  = (int) $params[ 'minValue' ];
                    $conditions[ 'minValue' ] = [ $params['minValue'] , 'int' ] ;
                }
//                else
//                {
//                    $item[ 'minValue' ] = NULL ;
//                }
            }

            if( isset($params[ 'maxValue' ]) )
            {
                if( $params['maxValue'] != '' )
                {
                    $item[ 'maxValue' ] = (int) $params[ 'maxValue' ];
                    $conditions['maxValue'] = [ $params['maxValue'] , 'int'  ] ;
                }
//                else
//                {
//                    $item['maxValue'] = NULL ;
//                }
            }

            if( isset($params[ 'gain' ]) )
            {
                if( $params['gain'] != '' )
                {
                    $item[ 'gain' ] = (int) $params[ 'gain' ];
                    $conditions['gain'] = [ $params['gain'] , 'int'  ] ;
                }
//                else
//                {
//                    $item['gain'] = NULL ;
//                }
            }

            if( isset($params[ 'loss' ]) )
            {
                if( $params['loss'] != '' )
                {
                    $item[ 'loss' ] = (int) $params[ 'loss' ];
                    $conditions['loss'] = [ $params['loss'] , 'int'  ] ;
                }
//                else
//                {
//                    $item['loss'] = null ;
//                }
            }

            $this->validator->validate( $conditions );

            if( $this->validator->passes() )
            {
                $item = empty( $item ) ? NULL : $item ;

                $this->model->update( [ $this->name => $item ] , $id );
                return $this->success( $response , $item );
            }
            else
            {
                return $this->getValidatorError( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

}
