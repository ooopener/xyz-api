<?php

namespace xyz\ooopener\controllers;

use xyz\ooopener\controllers\organizations\OrganizationsController;
use xyz\ooopener\controllers\people\PeopleController;
use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Organizations;
use xyz\ooopener\models\People;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Psr\Container\ContainerInterface ;

/**
 * The edges authorities controller.
 *
 * @OA\Schema(
 *     schema="authority",
 *     description="The authority - Organization or Person",
 *     type="object",
 *     allOf={},
 *     anyOf={
 *         @OA\Schema(ref="#/components/schemas/OrganizationList"),
 *         @OA\Schema(ref="#/components/schemas/PersonList")
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="authorities",
 *     description="List of authority",
 *     type="array",
 *     items=@OA\Items(ref="#/components/schemas/authority")
 * )
 *
 * @OA\Schema(
 *     schema="authorityType",
 *     description="Authority type",
 *     type="string",
 *     enum={
 *         "organization",
 *         "person"
 *     },
 *     default="organization"),
 *     required=true
 * )
 */
class EdgesAuthoritiesController extends EdgesController
{
    /**
     * Creates a new EdgesAuthoritiesController instance.
     *
     * @param ContainerInterface $container
     * @param People|NULL $people
     * @param Organizations|NULL $organizations
     * @param Collections|NULL $owner
     * @param Edges|NULL $edgePeople
     * @param Edges|NULL $edgeOrganizations
     * @param string|NULL $path
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?People            $people            = NULL ,
        ?Organizations     $organizations     = NULL ,
        Collections        $owner             = NULL ,
        ?Edges             $edgePeople        = NULL ,
        ?Edges             $edgeOrganizations = NULL ,
        ?string            $path              = NULL
    )
    {
        parent::__construct( $container , $people , $owner , $edgePeople , $path );

        $this->organizations = $organizations ;
        $this->people        = $people ;

        $this->edgeOrganizations = $edgeOrganizations ;
        $this->edgePeople        = $edgePeople ;

        if( $owner->table == $organizations->table )
        {
            $this->isOwnerOrganizations = TRUE ;
        }
        elseif( $owner->table == $people->table )
        {
            $this->isOwnerPeople = TRUE ;
        }
    }

    /**
     * The organizations edge reference.
     */
    public ?Edges $edgeOrganizations ;

    /**
     * The people edge reference.
     */
    public ?Edges $edgePeople ;

    /**
     * Indicates if the owner table is the owner organisation table.
     * @var bool
     */
    public bool $isOwnerOrganizations = FALSE;

    /**
     * Indicates if the owner table is the owner people table.
     * @var bool
     */
    public bool $isOwnerPeople = FALSE;

    /**
     * The organizations reference.
     */
    public ?Organizations $organizations ;

    /**
     * The people reference.
     */
    public ?People $people ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'id'      => NULL ,
        'items'   => NULL ,
        'lang'    => NULL ,
        'limit'   => NULL ,
        'offset'  => NULL ,
        'options' => NULL ,
        'search'  => NULL ,
        'skin'    => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return array|Response|null
     */
    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'   => $id ,
            'skin' => $skin ,
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $items = [] ;

            $people = $this->edgePeople->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;

            if( $people && $people->edge && is_array( $people->edge ) && count( $people->edge ) > 0 )
            {
                $itemsPeople      = NULL ;
                $peopleController = $this->container->get( PeopleController::class ) ;

                foreach( $people->edge as $key => $value)
                {
                    $itemsPeople[] = $peopleController->create( (object) $value ) ;
                }

                $items = array_merge( $items , $itemsPeople ) ;
            }

            $organizations = $this->edgeOrganizations->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;

            if( $organizations && $organizations->edge && is_array( $organizations->edge ) && count( $organizations->edge ) > 0 )
            {
                $itemsOrganization       = NULL ;
                $organizationsController = $this->container->get( OrganizationsController::class ) ;

                foreach( $organizations->edge as $key => $value)
                {
                    $itemsOrganization[] = $organizationsController->create( (object) $value ) ;
                }

                $items = array_merge( $items , $itemsOrganization ) ;
            }

            if( $response )
            {
                return $this->success( $response , $items ) ;
            }
            else
            {
                return $items ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    // FIXME code the count method
    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        return NULL ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE ,
        'type'    => 'person'
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="deleteAuthority",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="type",ref="#/components/schemas/authorityType"),
     *             required={"type"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;

        if( $request )
        {
            $params = $request->getParsedBody() ;

            if(
                   ( $params && isset( $params['type'] ) && $params['type'] == 'organization' )
                || ( isset( $args['type'] )  && $args['type'] == 'organization' ) )
            {
                $type = 'organization' ;
            }
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( $type == 'organization' )
            {
                $idFrom = $this->organizations->table . '/' . $id ;
                $idTo = $o->_id ;

                if( !$this->edgeOrganizations->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $success = $this->edgeOrganizations->deleteEdge( $idFrom , $idTo ) ;
            }
            else
            {
                $idFrom = $this->people->table . '/' . $id ;
                $idTo = $o->_id ;

                if( !$this->edgePeople->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $success = $this->edgePeople->deleteEdge( $idFrom , $idTo ) ;
            }

            if( $success )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;

                // update child
                if( $type == 'organization' )
                {
                    $this->organizations->updateDate( $id ) ;
                }
                else
                {
                    $this->people->updateDate( $id ) ;
                }

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return mixed|Response|null
     */
    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' deleteReverse(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->organizations->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->organizations->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $o->_id ;
            $idTo   = $this->organizations->table . '/' . $id ;

            if( !$this->edgeOrganizations->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $success = $this->edgeOrganizations->deleteEdge( $idFrom , $idTo ) ;

            if( $success )
            {
                $this->owner->updateDate( $id ) ; // update owner
                $this->organizations->updateDate( $owner ) ; // update child
                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteReverse(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|null
     */
    public function deleteReversePeople( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse ,
            'type'    => $type
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' deleteReversePeople(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->people->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'deleteReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->organizations->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $o->_id ;
            $idTo = $this->organizations->table . '/' . $id ;

            if( !$this->edgePeople->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $success = $this->edgePeople->deleteEdge( $idFrom , $idTo ) ;

            if( $success )
            {
                // update owner
                $this->owner->updateDate( $id ) ;

                // update child
                $this->people->updateDate( $owner ) ;

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'deleteReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteReversePeople(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return bool|Response
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            if( $reverse === FALSE )
            {
                $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }

                // get all people
                $allPeople = $this->edgePeople->getEdge( NULL , $owner ) ;

                $allOrganizations = $this->edgeOrganizations->getEdge( NULL , $owner ) ;
            }
            else
            {
                /*$o = $this->model->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }*/

                // get all people
                $allPeople = $this->edgePeople->getEdge( $owner ) ;

                // get all organizations
                $allOrganizations = $this->edgeOrganizations->getEdge( $owner ) ;
            }

            if( $allPeople && $allPeople->edge )
            {
                // delete them all
                foreach( $allPeople->edge as $edge )
                {
                    // delete
                    if( $reverse === FALSE )
                    {
                        $setOwner = $owner ;
                        $setID    = (string) $edge['id'] ;
                    }
                    else
                    {
                        $setOwner = (string) $edge['id'] ;
                        $setID    = $owner ;
                    }
                    $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            if( $allOrganizations && $allOrganizations->edge )
            {
                // delete them all
                foreach( $allOrganizations->edge as $edge )
                {
                    // delete
                    if( $reverse === FALSE )
                    {
                        $setOwner = $owner ;
                        $setID    = (string) $edge['id'] ;
                    }
                    else
                    {
                        $setOwner = (string) $edge['id'] ;
                        $setID    = $owner ;
                    }
                    $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID , 'type' => 'organization' ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object
     *
     * @OA\RequestBody(
     *     request="postAuthority",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="type",ref="#/components/schemas/authorityType"),
     *             required={"type"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();
        $organization = FALSE ;

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $result = NULL ;

            if( isset( $params['type'] ) && $params['type'] == 'organization' )
            {
                $organization = TRUE ;
                if( $this->isOwnerOrganizations && ( $owner == $id ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                if( !$this->organizations->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $idFrom = $this->organizations->table . '/' . $id ;
                $idTo = $o->_id ;

                // test if already exist
                if( $this->edgeOrganizations->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                // insert edge
                $insert = $this->edgeOrganizations->insertEdge( $idFrom , $idTo ) ;

                $fields = $this->container->get( $this->edgeOrganizations->from['controller'] )->getFields() ;
                $result = $this->organizations->get( $id , [ 'queryFields' => $fields ] ) ;
            }
            else
            {
                if( $this->isOwnerPeople && ( $owner == $id ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                if( !$this->people->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $idFrom = $this->people->table . '/' . $id ;
                $idTo = $o->_id ;

                // test if already exist
                if( $this->edgePeople->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                // insert edge
                $insert = $this->edgePeople->insertEdge( $idFrom , $idTo ) ;

                $fields = $this->container->get( $this->edgePeople->from['controller'] )->getFields() ;
                $result = $this->people->get( $id , [ 'queryFields' => $fields ] ) ;
            }

            if( $insert )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;

                // update child
                if( $organization === TRUE )
                {
                    $this->organizations->updateDate( $id ) ;
                }
                else
                {
                    $this->people->updateDate( $id ) ;
                }
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|null
     */
    public function postReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' postReverse(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->organizations->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->organizations->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( $this->isOwnerOrganizations && ( $owner == $id ) )
            {
                return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            $idFrom = $o->_id ;
            $idTo   = $this->organizations->table . '/' . $id ;

            if( $this->edgeOrganizations->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            $insert = $this->edgeOrganizations->insertEdge( $idFrom , $idTo ) ;

            $controller = $this->container->get( $this->edgeOrganizations->from['controller'] ) ;

            $fields = $controller->getFields() ;
            $result = $controller->create( $this->organizations->get( $id , [ 'queryFields' => $fields ] ) ) ;

            if( $insert )
            {
                // update owner
                $this->owner->updateDate( $id ) ;

                // update child
                $this->organizations->updateDate( $owner ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'postReverse(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|null
     */
    public function postReversePeople( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' postReversePeople(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $p = $this->people->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$p )
            {
                return $this->formatError( $response , '404' , [ 'postReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->organizations->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'postReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $p->_id ;
            $idTo  = $this->organizations->table . '/' . $id ;

            // test if already exist
            if( $this->edgePeople->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409' , [ 'postReversePeople(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            $insert = $this->edgePeople->insertEdge( $idFrom , $idTo ) ;

            $controller = $this->container->get( $this->edgePeople->to['controller'] ) ;

            $fields = $controller->getFields() ;
            $result = $controller->create( $this->organizations->get( $id , [ 'queryFields' => $fields ] ) ) ;

            if( $insert )
            {
                $this->owner->updateDate( $id ) ; // update owner
                $this->people->updateDate( $owner ) ; // update child
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError
            (
                $response ,
                '500',
                [ 'postReversePeople(' . $owner . ',' . $id . ')' , $e->getMessage() ] ,
                NULL ,
                500
            );
        }
    }
}
