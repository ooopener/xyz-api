<?php

namespace xyz\ooopener\controllers\lifes;

use xyz\ooopener\controllers\steps\StepsController;
use xyz\ooopener\helpers\Status;
use xyz\ooopener\things\Thing;
use xyz\ooopener\validations\StageValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\controllers\CollectionsController;
use xyz\ooopener\models\Collections;

use Exception ;
use Psr\Container\ContainerInterface;

class LifesController extends CollectionsController
{
    public function __construct(ContainerInterface $container, Collections $model = null, string $path = null)
    {
        parent::__construct($container, $model, $path);
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =

    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL    ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID      ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL     ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME    ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME    ] ,

        'taxon'         => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'birthDate'     => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'deathDate'     => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'birthPlace'    => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'deathPlace'    => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,

        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'list', 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'list', 'normal' ] ] ,

        'events'        => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'events' ] ] ,
        'photos'        => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'photos' ] ]
    ];

   /**
     * Creates a new instance.
     * @param ?object $init A generic object to create and populate the new thing.
     * @param ?string $lang The lang optional lang iso code.
     * @param ?string $skin The optional skin mode.
     * @param ?array $params The optional params object.
     * @return ?object
     */
    public function create( object $init = NULL , string $lang = NULL , string $skin = NULL , array $params = NULL ) :?object
    {
        /*if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {

                }
            }
        }*/
        return $init;
    }

    ///////////////////////////

    public function delete(Request $request = null, Response $response = null, array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        //// remove all linked resources

        return parent::delete($request, $response, $args);
    }

    public function post(Request $request = null , Response $response = null , array $args = [])
    {
        if($response ) {
            $this->logger->debug($this . ' post()');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];

        $item['active'] = 1;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path;

        if(isset($params['name']) ) {
            $item['name'] = $params['name'];
        }

        // get open status
        $status = $this->container->coursesStatus->get('open', [ 'key' => 'alternateName' , 'fields' => '_key' ]);
        if($status ) {
            $status = $status->_key ;
        }

        $conditions =
        [
            'name'     => [ $params['name']     , 'required|min(2)|max(255)' ],
            'location' => [ $params['location'] , 'required|location'        ],
            'status'   => [ $status             , 'required|status'          ]
        ];

        //////
        ////// security - remove sensible fields
        //////

        if(isset($params['id']) ) {
            unset($params['id']);
        }

        ////// validator

        $validator = new StageValidator($this->container);

        $validator->validate($conditions);

        if ($validator->passes()) {
            //////

            try
            {
                $result = $this->model->insert($item);

                // add location edge
                $po = $this->container->stageLocations;
                $poi = $po->insertEdge($po->from['name'] . '/' . $params['location'], $result->_id);

                // status edge
                if($status ) {
                    $edge = $this->container->stageCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // add edge
                    $edge->insertEdge($idFrom, $result->_id);
                }

                if($result ) {
                    return $this->success($response, $this->model->get($result->_key, ['queryFields' => $this->getFields()]));
                }
                else
                {
                    return $this->error($response, 'error');
                }
            }
            catch( Exception $e )
            {
                return $this->formatError($response, '500', ['post()', $e->getMessage()], null, 500);
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }


    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => null
    ] ;

    public function put( Request $request = null , Response $response = null , array $args = [])
    {
        [ 'id' => $id ] = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if($response ) {
            $this->logger->debug($this . ' put(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];
        $location = null;
        $status   = null;

        if(isset($params[ 'name' ]) ) {
            $item[ 'name' ] = $params[ 'name' ];
        }

        if(isset($params[ 'location' ]) ) {
            $location = $params[ 'location' ];
        }

        if(isset($params['status']) ) {
            $status = $params['status'] ;
        }

        $conditions =
        [
            'name'     => [ $params['name'] , 'required|min(2)|max(255)' ],
            'location' => [ $location       , 'required|location'        ],
            'status'   => [ $status         , 'required|status'          ]
        ];

        ////// validator

        $validator = new StageValidator($this->container);

        $validator->validate($conditions);

        if($validator->passes() ) {
            try
            {
                if(!$this->model->exist($id) ) {
                    return $this->formatError($response, '404', [ 'put(' . $id . ')' ], null, 404);
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if($location ) {
                    $edge = $this->container->stageLocations ;

                    $idFrom = $edge->from['name'] . '/' . $location ;

                    // check exists
                    if(!$edge->existEdge($idFrom, $idTo) ) {
                        // delete all edges to be sure
                        $edge->delete($idTo, [ 'key' => '_to' ]);
                        // add edge
                        $edge->insertEdge($idFrom, $idTo);
                    }
                }

                if($status ) {
                    $edge = $this->container->stageCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if(!$edge->existEdge($idFrom, $idTo) ) {
                        // delete all edges to be sure
                        $edge->delete($idTo, [ 'key' => '_to' ]);
                        // add edge
                        $edge->insertEdge($idFrom, $idTo);
                    }
                }

                $result = $this->model->update($item, $id);

                if($result )
                {
                    // update steps
                    if( $this->container->has( StepsController::class ) )
                    {
                        $stepsController = $this->container->get( StepsController::class ) ;
                        $stepsController->updateStage( $id ) ;
                    }

                    return $this->success($response, $this->model->get($id, [ 'queryFields' => $this->getFields('list') ]));
                }
                else
                {
                    return $this->error($response, 'error');
                }
            }
            catch( Exception $e )
            {
                return $this->formatError($response, '500', [ 'put()' , $e->getMessage() ], null, 500);
            }
        }
        else
        {
            return $this->getValidatorError( $response ) ;
        }
    }

}
