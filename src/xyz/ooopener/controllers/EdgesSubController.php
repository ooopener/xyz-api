<?php

namespace xyz\ooopener\controllers;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use xyz\ooopener\models\Collections;
use xyz\ooopener\models\Edges;
use xyz\ooopener\models\Model;

use Psr\Container\ContainerInterface;

use Exception ;

class EdgesSubController extends EdgesController
{
    /**
     * EdgesSubController constructor.
     * @param ContainerInterface $container
     * @param ?Model         $model
     * @param ?Collections   $owner
     * @param ?Edges         $edge
     * @param bool           $strict
     */
    public function __construct
    (
        ContainerInterface $container ,
        ?Model             $model  = NULL ,
        ?Collections       $owner  = NULL ,
        ?Edges             $edge   = NULL ,
        bool               $strict = TRUE
    )
    {
        parent::__construct( $container , $model , $owner , $edge );
        $this->strict = $strict ;
    }

    /**
     * Indicates if a new resource must be an unique resource in all from/to edges.
     * @var bool
     */
    public bool $strict ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'      => $id ,
            'owner'   => $owner ,
            'reverse' => $reverse
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            if( $owner == $id )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , [ 'The two resources are equals' ] , 409 );
            }

            $parent = $this->owner->get( $owner , [ 'fields' => '_id' ] ) ;
            if( !$parent )
            {
                return $this->formatError( $response ,'404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $from = $this->model->table . '/' . $id ;
            $to   = $parent->_id ;

            if( $this->edge->existEdge( $from , $to ) )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , [ 'error' => 'already exists' ] , 409 );
            }

            if( $this->strict && $this->edge->existEdge( $to , $from ) ) // check if invert edge exists
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , [ 'error' => 'Invert exists' ] , 409 );
            }

            $insert = $this->edge->insertEdge( $from , $to ) ;

            $this->owner->updateDate( $owner ) ;

            if( $reverse )
            {
                $controller = $this->container->get( $this->edge->to['controller'] ) ;
                $item       = $this->owner->get( $owner, [ 'queryFields' => $controller->getFields() ] ) ;
                $result     = $controller->create( $item ) ;
            }
            else
            {
                $controller = $this->container->get( $this->edge->from['controller'] ) ;
                $item       = $this->model->get( $id, [ 'queryFields' => $controller->getFields() ] ) ;
                $result     = $controller->create( $item ) ;
            }

            return $response ? $this->success( $response , $result ) : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
