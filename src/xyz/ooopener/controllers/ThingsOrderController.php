<?php

namespace xyz\ooopener\controllers ;

use Exception ;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Container\ContainerInterface;
use Slim\HttpCache\CacheProvider;
use Violin\Violin;

use xyz\ooopener\models\Collections;
use xyz\ooopener\validations\Validation;

class ThingsOrderController extends CollectionsController
{
    /**
     * Create a new ThingsOrderController constructor.
     * @param ContainerInterface $container
     * @param Collections|NULL $model
     * @param Collections|NULL $owner
     * @param CollectionsController|NULL $ownerController
     * @param ?string $path
     * @param ?Validation $validator The optional validator of the controller (by default use a basic Validation instance).
     */
    public function __construct
    (
        ContainerInterface    $container ,
        Collections           $model           = NULL ,
        Collections           $owner           = NULL ,
        CollectionsController $ownerController = NULL ,
        ?string               $path            = NULL ,
        ?Validation           $validator       = NULL

    )
    {
        parent::__construct( $container , $model , $path , $validator ) ;
        $this->owner           = $owner ;
        $this->ownerController = $ownerController ;
    }

    public ?Collections $owner ;

    public ?CollectionsController $ownerController ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'     => $id ,
            'params' => $params
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();
        }

        if( $this->ownerController )
        {
            try
            {
                $result = $this->ownerController->get( NULL, NULL,
                [
                    'active' => NULL ,
                    'id'     => $id,
                    'skin'   => $this->path
                ]) ;

                $items = $result->{$this->path} ;

                if( $response )
                {
                    return $this->success
                    (
                        $response ,
                        $items,
                        $this->getCurrentPath( $request , $params )
                    ) ;
                }

                return $items ;
            }
            catch( Exception $error )
            {
                if( $response )
                {
                    return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $error->getMessage() ] , NULL , 500 );
                }
                else
                {
                    $this->logger->error( $this . ' get(' . $id . ') failed, error : ' . $error->getMessage() ) ;
                }
            }
        }
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ref = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$ref )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $this->deleteInOwner( $id , $ref ) ;

            $result = $this->model->delete( $id );

            return $response ? $this->success( $response , (int) $id ) : $result ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner' => NULL
    ] ;

    /**
     * @param ?Request $request
     * @param ?Response $response
     * @param array $args
     * @return mixed
     */
    public function deleteAll( ?Request $request, ?Response $response , array $args = [] )
    {
        [ 'owner' => $owner ] = array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
            }

            if( $o->{$this->path} )
            {
                foreach( $o->{$this->path} as $id )
                {
                    $result = $this->delete( NULL , $response , [ 'owner' => $owner , 'id' => (string) $id ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            return $response ? $this->success( $response , 'ok' ) : TRUE ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'lang'  => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( ?Request $request = NULL , ?Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'lang'  => $lang ,
            'skin'  => $skin
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->config[ 'api' ] ;
        $set = array_key_exists( $this->path , $this->config ) ? $this->config[ $this->path ] : NULL ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( $params['lang'] , $api['languages'] ) )
                {
                    $params['lang'] = $lang = $params['lang'] ;
                }
                else if( $params['lang'] == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) )
            {
                if( is_array($set) && array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( is_array($set) && array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( is_array($set) && in_array( $params['skin'], $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $extraQuery = '' ;
            $fields = $this->getFields( $skin ) ;

            // get position
            if( array_key_exists( 'position' , $fields ) )
            {
                $extraQuery = 'LET ' . $fields['position']['unique'] . ' = ( ' .
                                'FOR doc_coll IN ' . $this->model->table . ' ' .
                                'FILTER POSITION( doc_coll.' . $this->path . ' , TO_NUMBER( @value ) ) == true ' .
                                'RETURN POSITION( doc_coll.' . $this->path . ' , TO_NUMBER( @value ) , true ) ' .
                                ')[0]' ;
            }

            $result = $this->owner->get
            (
                $id ,
                [
                    'extraQuery'  => $extraQuery ,
                    'queryFields' => $fields ,
                    'lang'        => $lang
                ]
            ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $options = [] ;

                // get owner
                $ownerItem = $this->getOwner( $id ) ;
                if( $ownerItem )
                {
                    $options[ 'object' ] = $ownerItem ;
                }

                // add headers
                $cache = $this->container->get( CacheProvider::class ) ;

                $response = $cache->withETag( $response , $result->modified );
                $response = $cache->withLastModified( $response , $result->modified );

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch position' methods options.
     */
    const ARGUMENTS_PATCH_POSITION_DEFAULT =
    [
        'id'       => NULL ,
        'owner'    => NULL ,
        'position' => NULL
    ] ;

    public function patchPosition( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'       => $id ,
            'owner'    => $owner ,
            'position' => $position
        ]
        = array_merge( self::ARGUMENTS_PATCH_POSITION_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            // check step in course
            if( in_array( (int)$id , $o->{$this->path} , TRUE ) === FALSE )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            $length = count( $o->{$this->path} ) ;


            $newArray = array_diff( $o->{$this->path} , [ (int)$id ] ) ; // remove id in array

            // check position

            if( $position < 0 )
            {
                $position = 0 ;
            }
            else if( $position > $length - 1 )
            {
                $position = $length - 1 ;
            }

            // put id in the new position

            array_splice( $newArray , $position , 0 , (int)$id ) ;

            // update

            $this->owner->update( [ $this->path => $newArray ] , $owner ) ;

            $item = $this->all( NULL , NULL , [ 'id' => $owner ] ) ;

            return $this->success( $response , $item ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'       => NULL,
        'owner'    => NULL,
        'position' => NULL
    ] ;

    /**
     * Post
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'       => $id,
            'owner'    => $owner,
            'position' => $position
        ]
        = array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ', ' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ', ' .  $id . ')' ] , NULL , 404 );
            }

            $item = $this->model->get( $id , [ 'fields' => '_key' ]  ) ;
            if( !$item )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ', ' .  $id . ')' ] , NULL , 404 );
            }

            $this->conditions = [] ;

            if( isset( $params['position'] ) )
            {
                $position = (int) $params['position'] ;
                $this->conditions['position'] = [ $params['position'] , 'int|min(0,number)' ] ;
            }

            if( isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {
                    $this->insertInOwner( $item->_key , $o , $position ) ;
                    $result = $this->get( NULL , NULL , [ 'id' => $o->_key ] ) ;
                    if( $result )
                    {
                        return $this->success
                        (
                            $response,
                            $result
                        ) ;
                    }

                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * Put specific keyword
     *
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        [
            'id'    => $id ,
            'owner' => $owner
        ]
        = array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ev = $this->owner->get( $owner , [ 'fields' => '_key' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {
                    $success = $this->model->update( $this->item , $id ) ;

                    if( $success )
                    {
                        $this->owner->updateDate( $owner ) ;
                        $item = $this->get( NULL , NULL , [ 'id' => $id ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    return $this->getValidatorError( $response ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    //////////////////////////////////

    protected function deleteInOwner( $id , $owner )
    {
        $array = $owner->{$this->path} ;

        $pos = array_search( (int)$id , $array , TRUE ) ;

        if( $pos !== FALSE )
        {
            array_splice( $array , $pos , 1 ) ;
            return $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;
        }

        return NULL ;
    }

    protected function getOwner( $modelId , $skin = NULL )
    {
        $result = $this->owner->all
        ([
            'active'      => NULL ,
            'conditions'  => [ 'POSITION( doc.' . $this->path . ' , ' . $modelId . ' ) == true' ],
            'queryFields' => $this->ownerController->getFields( $skin ),
            'limit'       => 1
        ]) ;

        if( is_array( $result ) && count( $result ) == 1 )
        {
            return $result[0] ;
        }

        return NULL ;
    }

    protected function insertInOwner( $id , $owner , $position = NULL ) : object
    {
        $array = $owner->{$this->path} ;
        $length = count( $array ) ;
        if( $position === NULL || $position > $length )
        {
            $position = $length ;
        }
        array_splice( $array , $position , 0 , (int)$id ) ;
        return $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;
    }
}
