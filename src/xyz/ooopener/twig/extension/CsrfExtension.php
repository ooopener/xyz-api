<?php

namespace xyz\ooopener\twig\extension ;

use Slim\Csrf\Guard ;

use Twig\Extension\AbstractExtension ;
use Twig\Extension\GlobalsInterface ;

class CsrfExtension extends AbstractExtension implements GlobalsInterface
{
    /**
     * CsrfExtension constructor.
     * @param Guard $csrf
     */
    public function __construct( Guard $csrf )
    {
        $this->csrf = $csrf;
    }

    /**
     * @var Guard
     */
    protected Guard $csrf;

    /**
     * @return array[]
     */
    public function getGlobals() : array
    {
        return
        [
            'csrf' =>
            [
                'keys' =>
                [
                    'name'  => $this->csrf->getTokenNameKey(),
                    'value' => $this->csrf->getTokenValueKey()
                ],
                'name'  => $this->csrf->getTokenName(),
                'value' => $this->csrf->getTokenValue()
            ]
        ];
    }
}
