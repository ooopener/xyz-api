<?php

namespace xyz\ooopener\handlers ;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use Slim\Handlers\NotFound ;
use Slim\Views\Twig;

use system\net\GoogleAnalytics;

final class NotFoundHandler extends NotFound
{
    /**
     * NotFoundHandler constructor.
     *
     * @param ContainerInterface $container
     * @param null $page
     */
    public function __construct( ContainerInterface $container , $page = NULL )
    {
        $this->container = $container ;
        $this->config  = $container->get('settings') ;
        $this->logger  = $container->get( LoggerInterface::class ) ;
        $this->tracker = $container->get('tracker') ;
        $this->page = $page ;
    }

    protected $config ;

    protected ContainerInterface $container ;

    /**
     * The LoggerInterface reference.
     * @var LoggerInterface
     */
    protected LoggerInterface $logger ;

    /**
     * The optional '404' template page view.
     */
    public ?string $page ;

    protected GoogleAnalytics $tracker ;

    /**
     * Invoked when a not found handler is catched.
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function __invoke( Request $request, Response $response )
    {
        parent::__invoke( $request , $response ) ;

        $config  = $this->container->get('settings') ;

        $message = $config['errors']['404'] ;

        if( (bool) $config['useLogging'] && isset($this->logger) )
        {
            $this->logger->error( $message . ' - ' . $request->getUri() );
        }

        if( isset($this->tracker) )
        {
            $pre = (bool) $config['analytics']['useVersion']
                 ? '/' . $config['version']
                 : '' ;

            $this->tracker->pageview( $pre . '/errors/404' , $message . ' - ' . $request->getUri() );
        }

        if( isset($this->page) )
        {
            return $this->container->get( Twig::class )->render
            (
                $response->withStatus(404) ,
                $this->page
            );
        }
        else
        {
            return $response->withJson
            (
                [
                    'status' => 'error' ,
                    'code'   => '404' ,
                    'message' => $message
                ]
                ,
                404 , $this->container->get('jsonOptions')
            );
        }
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString()
    {
        return '[' . get_class($this) . ']' ;
    }
}
