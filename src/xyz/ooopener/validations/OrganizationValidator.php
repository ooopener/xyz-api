<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class OrganizationValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'addressLocality' =>
            [
                'commune' => "Invalid addressLocality, it's 'commune' don't exist."
            ],
            'dissolutionDate' =>
            [
                'date' => 'The dissolution date is invalid.'
            ],
            'foundingDate' =>
            [
                'date' => 'The founding date is invalid.'
            ],
            'foundingLocation' =>
            [
                'location' => 'The founding location don\'t exist.'
            ],
            'postalCode' =>
            [
                'postalCode' => 'Invalid postal code.'
            ]
            ,
            'telephone' =>
            [
                'telephone' => 'Invalid phone number.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
            ,
            'legalForm' =>
            [
                'legalForm' => 'The legalForm don\'t exist.'
            ],
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ],
            'ape' =>
            [
                'ape' => 'The NAF don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific legalForm exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_legalForm( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('businessEntityTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if a name is unique in the users table.
     * @param $value
     * @param $input
     * @param $args
     * @return false|int
     */
    public function validate_postalCode( $value , $input , $args )
    {
        return preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
    }

    /**
     * Validates if the event type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('organizationsTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the specific naf exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_ape( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('organizationsNaf')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


