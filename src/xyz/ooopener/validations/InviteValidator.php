<?php

namespace xyz\ooopener\validations ;

use xyz\ooopener\models\Applications;
use xyz\ooopener\models\People;
use xyz\ooopener\models\Teams;
use xyz\ooopener\models\Users ;

use Psr\Container\ContainerInterface ;

use Exception ;

class InviteValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'team' =>
            [
                'team' => 'Select a valid team.'
            ],
            'person' =>
            [
                'person' => 'Select a valid person'
            ],
            'email' =>
            [
                'email' => 'email must be a valid email address.',
                'userEmail' => 'email already used'
            ],
            'redirect' =>
            [
                'redirect' => 'redirect is not valid'
            ]
        ]);
    }

    /**
     * Validates if the email is registered.
     * @param string $value
     * @param string $input
     * @param array $args
     * @return bool
     */
    public function validate_userEmail( string $value , string $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return !$this->container
                         ->get( Users::class )
                         ->exist( $value , [ 'key' => 'email' ] ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the person is registered.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_person( string $value , string $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( People::class )
                        ->exist( $value , 'id' ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the redirect is registered.
     * @param string $value
     * @param string $input
     * @param array $args
     * @return bool
     */
    public function validate_redirect( string $value , string $input , array $args ) : bool
    {
        try
        {
            $jwt = $this->container->get('jwt') ;
            if( property_exists( $jwt , 'aud' ) )
            {
                $application = $this->container
                                    ->get( Applications::class )
                                    ->getByProperty( 'client_id' , $jwt->aud ) ;

                if( $application )
                {
                    $urls = explode( ',' , $application->url_redirect ) ;

                    $pattern = "@^" . preg_quote( $value ) . "@" ;
                    $search = preg_grep( $pattern , $urls ) ;

                    if( !empty($search) )
                    {
                        return TRUE ;
                    }
                }
            }
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the team is registered.
     * @param string $value
     * @param string $input
     * @param array $args
     * @return bool
     */
    public function validate_team( string $value , string $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( Teams::class )
                        ->exist( $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


