<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class ConceptualObjectMarkValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'language' =>
            [
                'language' => 'The language reference don\'t exist.'
            ],
            'technique' =>
            [
                'technique' => 'The technique reference don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the event additionalType exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('marksTypes')
                        ->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the language exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_language( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('languages')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the object technique exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_technique( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('conceptualObjectsTechniques')
                        ->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


