<?php

namespace xyz\ooopener\validations\setting;

use xyz\ooopener\helpers\Version;

use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Events;

use xyz\ooopener\validations\Validation;

use Exception;

use Psr\Container\ContainerInterface;

class AmbuloSettingValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'courses' =>
            [
                'courses' => 'Some courses don\'t exist.'
            ]
            ,
            'events' =>
            [
                'events' => 'Some events don\'t exist.'
            ]
            ,
            'game' =>
            [
                'game' => 'The game don\'t exist.'
            ],
            'version' =>
            [
                'version' => 'The version is invalid'
            ]
        ]);
    }

    /**
     * Validates if the courses exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_courses( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( Courses::class )
                        ->existAll( $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the events exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_events( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( Events::class )
                        ->existAll( $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the game exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_game( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( 'applicationsGames' )
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * @param string $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_version( string $value , array $input , array  $args ) : bool
    {
        $version = Version::fromString( $value ) ;
        if( $version == NULL )
        {
            return FALSE ;
        }
        return TRUE ;
    }
}
