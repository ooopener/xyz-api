<?php

namespace xyz\ooopener\validations\setting;

use xyz\ooopener\validations\Validation;

use Exception;

use Psr\Container\ContainerInterface;

class SupportSettingValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'topics' =>
            [
                'topics' => 'Some topics don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the topics exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_topics( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('topics')->existAll( $value ) ;
        }
        catch (Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}
