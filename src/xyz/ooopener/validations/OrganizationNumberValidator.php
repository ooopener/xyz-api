<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

/**
 * Class OrganizationNumberValidator
 * @package xyz\ooopener\validations
 * @deprecated
 */
class OrganizationNumberValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'value' =>
            [
                'value' => 'The value is not valid.'
            ]
            ,
            'type' =>
            [
                'type' => 'The type is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the value is valid with the pattern.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_value( $value , $input , $args )
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }

        // check pattern if set
        if( array_key_exists( 'type' , $input ) && $input['type'] != '' )
        {
            $word = $this->container
                         ->get('organizationsNumbersTypes')
                         ->get( (string)$input['type'] ) ;
            if( $word )
            {
                if( property_exists( $word , 'pattern' ) && $word->pattern !== null && $word->pattern !== '' )
                {
                    if( preg_match( '/' . $word->pattern  . '/' , $value ) === 1  )
                    {
                        if
                        (
                               property_exists( $word , 'validator' )
                            && $word->validator !== null
                            && $word->validator !== ''
                            && method_exists( $this , $word->validator )
                        )
                        {
                            return $this->{ $word->validator }( $value ) ;
                        }
                        else
                        {
                            return TRUE ;
                        }
                    }
                    else
                    {
                        return false ;
                    }
                }
            }
        }

        return true ;
    }

    public function luhn( string $number ): bool
    {
        $sum  = 0 ;
        $flag = 0 ;

        for( $i = strlen($number) - 1 ; $i >= 0 ; $i-- )
        {
            $add = $flag++ & 1 ? $number[$i] * 2 : $number[$i] ;
            $sum += $add > 9 ? $add - 9 : $add ;
        }

        return $sum % 10 === 0 ;
    }

    /**
     * Validates if the number type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_type( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('organizationsNumbersTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }


        return FALSE ;
    }

}


