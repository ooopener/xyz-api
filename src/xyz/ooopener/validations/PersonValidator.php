<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class PersonValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'birthDate' =>
            [
                'simpleDate' => 'The birth date is invalid.'
            ]
            ,
            'deathDate' =>
            [
                'simpleDate' => 'The death date is invalid.'
            ],
            'prefix' =>
            [
                'prefix' => 'The prefix don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific prefix exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_prefix( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('honorificPrefix')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


