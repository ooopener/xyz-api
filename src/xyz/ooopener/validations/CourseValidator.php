<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

use Exception ;

class CourseValidator extends Validation
{
    public function __construct( ContainerInterface $container , array $types = null )
    {
        parent::__construct( $container ) ;
        $this->types = $types ;
        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'audience' =>
            [
                'audience' => 'The audience doesn\'t exist'
            ],
            'level' =>
            [
                'level' => 'The level doesn\'t exist'
            ],
            'path' =>
            [
                'path' => 'The path doesn\'t exist'
            ],
            'status' =>
            [
                'status' => "The status doesn't exist"
            ],
            'url' =>
            [
                'url' => 'The URL is not a valid API resource'
            ]
        ]);
    }

    const TEST_PATH = "/^[a-zA-Z]+\/[0-9]+$/" ;

    public ?array $types ;

    /**
     * Validates if the course additionalType exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('coursesTypes')
                                   ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the audience exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_audience( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('coursesAudiences')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the level exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_level( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('coursesLevels')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the path exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_path( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('coursesPaths')
                        ->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the status exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_status( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty( $value ) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('coursesStatus')
                        ->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    public function validate_url( $value , $input , $args )
    {
        if( empty($value) )
        {
            return FALSE ;
        }

        $appUrl  = $this->config['app']['url'] ;
        $urlPath = substr( $value , strlen( $appUrl ) ) ;

        if
        (
            $this->startsWith( $value , $appUrl )
            && preg_match( self::TEST_PATH , $urlPath ) === 1
        )
        {
            [ $type , $name ] = explode( '/' , $urlPath ) ;
            if( is_array( $this->types ) && array_key_exists( $type , $this->types ) )
            {
                return $this->container->get( $this->types[$type] )->exist( $name ) ;
            }
        }

        return FALSE ;
    }
}
