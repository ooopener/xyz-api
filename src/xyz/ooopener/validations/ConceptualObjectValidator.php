<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class ConceptualObjectValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'category' =>
            [
                'category' => 'The category is not valid.'
            ]
            ,
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ]
            ,
            'movement' =>
            [
                'category' => 'The movement is not valid.'
            ]
            ,
            'temporal' =>
            [
                'category' => 'The temporal entitt is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the category exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_category( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('conceptualObjectsCategories')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the art movement exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_movement( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('artMovements')->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }

    /**
     * Validates if the art movement exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_temporal( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('artTemporalEntities')->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


