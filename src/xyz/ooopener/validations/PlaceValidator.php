<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class PlaceValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'addressLocality' =>
            [
                'commune' => "Invalid addressLocality, it's 'commune' don't exist."
            ]
            ,
            'postalCode' =>
            [
                'postalCode' => 'Invalid postal code.'
            ]
            ,
            'telephone' =>
            [
                'telephone' => 'Invalid phone number.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'status' =>
            [
                'status' => 'The place status is invalid.'
            ]
        ]);
    }

    /**
     * Validates if a name is unique in the users table.
     * @param $value
     * @param $input
     * @param $args
     * @return false|int
     */
    public function validate_postalCode( $value , $input , $args )
    {
        return preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
    }

    /**
     * Validates if the event additionalType exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('placesTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the place status exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_status( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('placesStatusTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


