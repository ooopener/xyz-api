<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class OfferValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'category' =>
            [
                'category' => 'The category is not valid.'
            ]
            ,
            'price' =>
            [
                'price' => 'The price is not valid.'
            ]
            ,
            'validFrom' =>
            [
                'date' => 'The \'validFrom\' date is invalid.'
            ]
            ,
            'validPeriod' =>
            [
                'true' => 'The validFrom and validThrough fields are not valids.'
            ]
            ,
            'validThrough' =>
            [
                'date' => 'The \'validThrough\' date is invalid.'
            ]
        ]);
    }

    /**
     * Validates the category name (if exist in the data table).
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_category( ?string $value , array $input , array $args ) : bool
    {
        if( empty( $value ) )
        {
            return FALSE ;
        }

        try
        {
            return $this->container->get('offersCategories')->exist( ( string ) $value ) === TRUE ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the value is a valid price.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_price( $value , array $input , array $args ) : bool
    {
        return ($value === '0') || (is_numeric($value) && (($value + 0) >= 0)) ;
    }
}
