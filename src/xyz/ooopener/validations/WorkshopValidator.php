<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\LivestockNumbers;
use Exception ;

use Psr\Container\ContainerInterface;

class WorkshopValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'breeding' =>
            [
                'breeding' => 'The breeding is not valid.'
            ]
            ,
            'production' =>
            [
                'production' => 'The production is not valid.'
            ]
            ,
            'identifier' =>
            [
                'identifier' => 'The identifier is not valid.'
            ]
            ,
            'water' =>
            [
                'water' => 'The water is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the breeding type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_breeding( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('breedingsTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the production type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_production( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('productionsTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the identifier exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_identifier( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get(LivestockNumbers::class)
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the water origin exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_water( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('waterOrigins')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


