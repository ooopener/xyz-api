<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

use Slim\Interfaces\RouteCollectorInterface;

use xyz\ooopener\models\Collections;

class ThesaurusCollectionValidator extends Validation
{
    /**
     * ThesaurusValidator constructor.
     * @param ContainerInterface $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( ContainerInterface $container , Collections $model , string $path = 'thesaurus' )
    {
        parent::__construct( $container ) ;
        $this->model = $model ;
        $this->path  = $path ;
        $this->addFieldMessages
        ([
            'identifier' =>
            [
                'startWithIdentifier' => 'The identifier prefix is not valid' ,
                'uniqueIdentifier'    => 'That identifier is already in use.'
            ],
            'path' =>
            [
                'uniquePath' => 'That path is already in use.'
            ]
        ]);
    }

    public Collections $model ;

    public string $path ;

    /**
     * Validates if the identifier is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_startWithIdentifier( $value , $input , $args ) :bool
    {
        try
        {
            return str_starts_with
            (
                $value ,
                ( is_array($args) && count( $args ) == 1 ) ? $args[0] : ''
            ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }

    /**
     * Validates if the identifier is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_uniqueIdentifier( $value , $input , $args ) :bool
    {
        try
        {
            if( is_array($args) )
            {
                if( isset($args[0]) && ( $args[0] !== '' ) && $args[0] === $value )
                {
                    return TRUE ;
                }
            }
            return $value !== NULL && $this->model->collectionExists( $value ) === FALSE
                && $this->model->exist( $value , [ 'key' => 'identifier' ] ) === FALSE ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }

    /**
     * Validates if the path is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_uniquePath( $value , $input , $args ) :bool
    {
        try
        {
            if( is_array($args) )
            {
                if( isset($args[0]) && ( $args[0] !== '' ) && $args[0] === $value )
                {
                    return TRUE ;
                }
            }

            $routes = $this->container
                           ->get( RouteCollectorInterface::class )
                           ->getRoutes() ;

            foreach ( $routes as $route )
            {
                if( str_starts_with( $route->getPattern() , '/' . $value ) )
                {
                    $this->warning( __FUNCTION__ , $value , $args , new Exception( 'The route pattern is already registered') ) ;
                    return FALSE ;
                }
            }

            return $this->model->exist( $value , [ 'key' => 'path' ] ) === FALSE ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }
}


