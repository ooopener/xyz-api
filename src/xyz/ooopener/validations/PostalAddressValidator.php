<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

class PostalAddressValidator extends Validation
{
    public function __construct( ContainerInterface $container , $strict = FALSE )
    {
        parent::__construct( $container ) ;
        $this->strict = $strict ;
        $this->addFieldMessages
        ([
            'addressLocality' =>
            [
                'commune' => "Invalid addressLocality, it's 'commune' don't exist."
            ]
            ,
            'postalCode' =>
            [
                'postalCode' => 'Invalid postal code.'
            ]
        ]);
    }

    /**
     * Indicates if the validation is strict.
     */
    public bool $strict ;

    /**
     * Validates if a name is unique in the users table.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_commune( $value , $input , $args )
    {
        if( $this->strict )
        {
            return $this->container->get('insee')->existCommune( $value ) ;
        }
        else
        {
            return ($value == NULL)
                || ($value == '')
                || $this->container->get('insee')->existCommune( $value ) ;
        }
    }

    /**
     * Validates if a name is unique in the users table.
     * @param $value
     * @param $input
     * @param $args
     * @return bool|int
     */
    public function validate_postalCode( $value , $input , $args )
    {
        if( $this->strict )
        {
            return preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
        }
        else
        {
            return ($value == NULL)
                || ($value == '')
                || preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
        }
    }
}


