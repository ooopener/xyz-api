<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

use Exception;

class StageValidator extends Validation
{
    public function __construct( ContainerInterface $container , array $types = null )
    {
        parent::__construct( $container ) ;
        $this->types = $types ;
        $this->addFieldMessages
        ([
            'url' =>
            [
                'url' => 'The URL is not a valid API resource URL'
            ],
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ],
            'status' =>
            [
                'status' => 'The status don\'t exist.'
            ]
        ]);
    }

    const TEST_PATH = "/^[a-zA-Z]+\/[0-9]+$/" ;

    public ?array $types ;

    /**
     * Validates if the status exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_status( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('coursesStatus')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    public function validate_url( $value , $input , $args )
    {
        if( empty($value) )
        {
            return FALSE ;
        }

        $appUrl  = $this->config['app']['url'] ;
        $urlPath = substr( $value , strlen( $appUrl ) ) ;

        if
        (
            $this->startsWith( $value , $appUrl )
            && preg_match( self::TEST_PATH , $urlPath ) === 1
        )
        {
            [ $type , $name ] = explode( '/' , $urlPath ) ;
            if( is_array( $this->types ) && array_key_exists( $type , $this->types ) )
            {
                return $this->container->get( $this->types[$type] )->exist( $name ) ;
            }
        }

        return FALSE ;
    }
}
