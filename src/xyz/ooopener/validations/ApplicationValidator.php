<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\helpers\Version;
use Psr\Container\ContainerInterface;

use Exception ;

class ApplicationValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
            ,
            'url_redirect' =>
            [
                'redirect' => 'Invalid URL in string'
            ],
            'version' =>
            [
                'version' => 'The version is invalid'
            ]
        ]);
    }

    /**
     * Validates if the type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('applicationsTypes')
                        ->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the url redirect value is string of URL.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_redirect( $value , $input , $args )
    {
        try
        {
            if( $value != '' )
            {
                $list = explode( ',' , $value ) ;
                foreach( $list as $url )
                {
                    if( filter_var( $url , FILTER_VALIDATE_URL ) === false )
                    {
                        return FALSE ;
                    }
                }
                return TRUE ;
            }

            return TRUE ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
    }
}
