<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Teams;
use Psr\Container\ContainerInterface;

use Exception ;

class TeamValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'color' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ],
            'name' =>
            [
                'uniqueName' => 'The name must be unique.'
            ]
        ]);
    }

    /**
     * Validates if a name is unique in the table.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_uniqueName( $value , $input , $args )
    {
        try
        {
            $teams = $this->container->get( Teams::class ) ;
            if( $args && count($args) > 0 )
            {
                if( $teams->exist( $args[0] , [ 'key' => 'name' ] ) && ($args[0] == $value) )
                {
                    return TRUE ;
                }
            }
            return $teams->exist( $value , [ 'key' => 'name' ] ) === FALSE ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }
}
