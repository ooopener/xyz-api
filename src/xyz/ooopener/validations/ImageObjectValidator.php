<?php

namespace xyz\ooopener\validations;

use Imagick ;

use Psr\Container\ContainerInterface;

class ImageObjectValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addRuleMessages
        ([
            'required' => 'Le champ {field} est requis.',
            'max'      => 'Le champ {field} nécessite {$0} caractère(s) max.',
            'min'      => 'Le champ {field} nécessite {$0} caractère(s) min.'
        ]);
        $this->addFieldMessages
        ([
            'height' =>
            [
                'min' => 'La hauteur de l\'image est trop petite !'
            ]
            ,
            'width' =>
            [
                'min' => 'La largeur de l\'image est trop petite !'
            ]
            ,
            'image' =>
            [
                'image' => "Une image au format jpeg ou png est requise."
            ]
        ]);
    }

    /**
     * Validates if a image is valid.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_image( $value , $input , $args )
    {
        return ($value != NULL) && ($value instanceof Imagick) ;
    }
}


