<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

use Exception;

class DiseaseAnalysisMethodValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('analysisMethods')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}
