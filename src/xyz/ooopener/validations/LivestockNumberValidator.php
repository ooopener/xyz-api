<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class LivestockNumberValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'value' =>
            [
                'value' => 'The value is not valid.'
            ]
            ,
            'type' =>
            [
                'type' => 'The type is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the value is valid with the pattern .
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_value( $value , $input , $args )
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }
        return TRUE ;
    }

    /**
     * Validates if the number type exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_type( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('livestocksNumbersTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


