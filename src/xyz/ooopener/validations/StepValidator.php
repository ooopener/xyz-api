<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

class StepValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
    }
}
