<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Courses;
use xyz\ooopener\models\Steps;
use Exception ;
use Psr\Container\ContainerInterface;

class GameValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'about' =>
            [
                'course'       => 'The course doesn\'t exist',
                'step'         => 'The step doesn\'t exist',
                'uniqueCourse' => 'The course already taken',
                'uniqueStep'   => 'The step already taken'
            ]
        ]);
    }

    /**
     * Validates if the course exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_course( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get(Courses::class)->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the step exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_step( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( Steps::class )
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the course is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_uniqueCourse( $value , $input , $args )
    {
        try
        {
            if( $args && count($args) == 2 )
            {
                return !$this->container
                             ->get('applicationsGames')
                             ->existAbout( (int)$value , (string)$args[0] , (string)$args[1] ) ;
            }
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the step is unique.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_uniqueStep( $value , $input , $args )
    {
        try
        {
            if( $args && count($args) == 2 )
            {
                return !$this->container
                             ->get('coursesGames')
                             ->existAbout( (int)$value , (string)$args[0] , (string)$args[1] ) ;
            }
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}
