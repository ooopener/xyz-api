<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class WebsiteValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'required'    => 'A valid website type is required.',
                'websiteType' => 'A valid website type is required.'
            ],
            'url' =>
            [
                'required' => 'A valid url is required.',
                'url'      => 'A valid url is required.'
            ]
            ,
            'rel' =>
            [
                'required'    => 'A valid website type is required.',
                'websiteType' => 'A valid website type is required.'
            ]
        ]);
    }

    /**
     * Validates if the alternateName value is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return false
     */
    public function validate_websiteType( $value , $input , $args ) :bool
    {
        try
        {
            return $this->container->get( 'websitesTypes' )->exist( (string) $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
    }
}


