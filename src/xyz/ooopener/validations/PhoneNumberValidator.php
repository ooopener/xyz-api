<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class PhoneNumberValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'value' =>
            [
                'required' => 'The phoneNumber is required.'
            ],
            'additionalType' =>
            [
                'required'        => 'The additionalType is required.',
                'phoneNumberType' => 'The additionalType don\'t exist.'
            ]

        ]);
    }

    /**
     * Validates if the phone number type value is valid.
     * @param $value
     * @param $input
     * @param $args
     * @return false
     */
    public function validate_phoneNumberType( $value , $input , $args ) :bool
    {
        try
        {
            return $this->container->get('phoneNumbersTypes')->exist( (string) $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
    }
}


