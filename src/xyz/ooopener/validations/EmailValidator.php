<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class EmailValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'value' =>
            [
                'required' => 'The email is required.'
            ],
            'additionalType' =>
            [
                'required'  => 'The additionalType is required.',
                'emailType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the email type value is valid.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_emailType( $value , $input , $args ) :bool
    {
        try
        {
            return $this->container->get('emailsTypes')->exist( ( string ) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
    }
}


