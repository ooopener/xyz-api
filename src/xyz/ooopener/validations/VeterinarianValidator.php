<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Organizations;
use xyz\ooopener\models\People;
use Exception ;
use Psr\Container\ContainerInterface;

class VeterinarianValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'organization' =>
            [
                'organization' => 'The organization don\'t exist.'
            ],
            'person' =>
            [
                'person' => 'The person don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific organization exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_organization( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get(Organizations::class)
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the specific person exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_person( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get(People::class)
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


