<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class IdentifierValidator extends Validation
{
    public function __construct( ContainerInterface $container , ?string $additionalTypeModel = null )
    {
        parent::__construct( $container ) ;
        $this->additionalTypeModel = $additionalTypeModel ;
        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType is not valid.'
            ],
            'value' =>
            [
                'value' => 'The value is not valid.'
            ]
        ]);
    }

    protected ?string $additionalTypeModel ;

    /**
     * Validates if the identifier additionalType exist and is valid.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args ):bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required && ( $value == NULL || empty($value) ) )
        {
            return TRUE ;
        }

        try
        {
            return $this->container->get( $this->additionalTypeModel )->exist( ( string ) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the value is valid with the pattern .
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_value( $value , $input , $args ) :bool
    {
        if( empty( $value ) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }
        return TRUE ;
    }
}


