<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Users;
use Psr\Container\ContainerInterface;

use Exception ;

class EventValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'after' =>
            [
                'true' => 'The end date is not after the start date.'
            ]
            ,
            'endDate' =>
            [
                'datetime' => 'The end date is invalid.'
            ]
            ,
            'eventStatus' =>
            [
                'eventStatus' => 'The event status is invalid.'
            ]
            ,
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ]
            ,
            'startDate' =>
            [
                'datetime' => 'The start date is invalid.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the event additionalType exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('eventsTypes')->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the event status exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_eventStatus( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get('eventsStatusTypes')->exist( (string) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the event name is unique in the users table.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_uniqueEventStatusName( $value , $input , $args ) :bool
    {
        return !$this->container->get( Users::class )->existName( $value ) ;
    }
}


