<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\helpers\DayOfWeek;

use Exception ;

use Psr\Container\ContainerInterface;

class OpeningHoursSpecificationValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'name' =>
            [
                //
            ]
            ,
            'closes' =>
            [
                'times' => 'The closes hours are invalid.'
            ]
            ,
            'dayOfWeek' =>
            [
                'dayOfWeek' => 'The \'dayOfWeek\' enumeration not must be empty.'
            ]
            ,
            'opens' =>
            [
                'times' => 'The \'opens\' hours are invalid.'
            ]
            ,
            'openCloseEquals' =>
            [
                'true' => "The opens and closes fields are not equals."
            ]
            ,
            'validFrom' =>
            [
                'date' => 'The \'validFrom\' date is invalid.'
            ]
            ,
            'validPeriod' =>
            [
                'true' => 'The validFrom and validThrough fields are not valids.'
            ]
            ,
            'validThrough' =>
            [
                'date' => 'The \'validThrough\' date is invalid.'
            ]
        ]);
    }

    /**
     * Validates if the dayofweek elements in an Array collection.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_dayOfWeek( ?array $value , array $input , array $args ) : bool
    {
        if( $value == '' )
        {
            return TRUE ;
        }

        try
        {
            if( !is_array($value) )
            {
                return FALSE ;
            }

            foreach( $value as $day )
            {
                if
                (
                       array_search( $day , DayOfWeek::days     ) === FALSE
                    && array_search( $day , DayOfWeek::specials ) === FALSE
                )
                {
                    return FALSE ;
                }
            }

            return TRUE ;

        }
        catch (Exception $e)
        {
            return FALSE ;
        }
    }
}


