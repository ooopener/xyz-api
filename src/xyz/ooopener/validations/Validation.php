<?php

namespace xyz\ooopener\validations ;

use xyz\ooopener\models\Places;
use xyz\ooopener\helpers\Version;

use DateTime ;
use Exception ;

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface ;

use Violin\Violin ;

/**
 * Class Validator
 * @package xyz\ooopener\validations
 */
class Validation extends Violin
{
    /**
     * The Validation constructor.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container ;
        $this->config    = $container->get( 'settings' ) ;
        $this->logger    = $container->get( LoggerInterface::class ) ;
    }

    /**
     * The config reference.
     */
    public array $config ;

    /**
     * @var ContainerInterface
     */
    protected ContainerInterface $container;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger ;

    /**
     * Validates if a context name is unique.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_alnumSlash( ?string $value , array $input , array $args ) : bool
    {
        return (bool) preg_match( '/^[\pL\pM\pN\/]+$/u' , $value );
    }

    /**
     * Validates if the date is valid with the pattern 'yyyy-mm-dd'.
     *
     * @param string|null $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_date( ?string $value , array $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        if( $this->validateDate( $value ) )
        {
            return TRUE ;
        }

        return FALSE  ;
    }

    /**
     * Validates if the date is valid .
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_datetime( ?string $value , array $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        return $this->validateISO8601Date( $value )  ;
    }

    /**
     * Validates if the value is a color expression.
     *
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_isColor( ?string $value , array $input , array $args ) : bool
    {
        return (bool) preg_match( '/^[a-f0-9]{6}$/i' , $value ) ;
    }

    /**
     * Validates if the specific location exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_location( ?string $value , array $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->get( Places::class )->exist( ( string ) $value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if a name is unique in the thesaurus table.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_notEmpty( ?string $value , array $input , array $args ) : bool
    {
        return isset($value) && !empty($value) ;
    }

    /**
     * Validates if the date is valid with the pattern 'yyy-mm-dd'.
     *
     * @param string|null $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_simpleDate( ?string $value , array $input , array $args ) : bool
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }

        $d1 = DateTime::createFromFormat( 'Y-m-d' , $value );
        $d2 = DateTime::createFromFormat( 'Y-m'   , $value );
        $d3 = DateTime::createFromFormat( 'Y'     , $value );

        $b1 = $d1 && $d1->format('Y-m-d') === $value ;
        $b2 = $d2 && $d2->format('Y-m')   === $value ;
        $b3 = $d3 && $d3->format('Y')     === $value ;

        return $b1 || $b2 || $b3 ;
    }

    /**
     * Validates the team.
     *
     * @param string $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_team( string $value , array $input , array $args ) : bool
    {
        $accept = ['admin', 'guest'] ;

        if( $this->container )
        {
            $auth = $this->container->get('auth') ;
            if( $auth->team->name == 'superadmin' )
            {
                $accept[] = 'superadmin' ;
            }
        }

        return in_array( $value , $accept ) ;
    }

    /**
     * Validates if the hours are valid 'hh:mm,hh:mm,....'.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_times( ?string $value , array $input , array $args ) : bool
    {
        if( is_string($value) )
        {
            $times = explode( ',' , $value ) ;
            foreach( $times as $time )
            {
                if( !$this->validateDate( $time , 'H:i' ) )
                {
                    return FALSE ;
                }
            }
            return TRUE ;
        }

        return FALSE  ;
    }

    /**
     * Validates if the value is a boolean.
     * @param bool $value
     * @param array $input
     * @param array $args
     * @return bool
     */
    public function validate_bool( bool $value , array $input , array $args ) : bool
    {
        return is_bool($value) ;
    }

    /**
     * Validates if the value false.
     * @param bool $value
     * @param array $input
     * @param array $args
     * @return bool
     */
    public function validate_false( bool $value , array $input , array $args ) : bool
    {
        return $value === FALSE ;
    }

    /**
     * Validates if the value true.
     * @param bool $value
     * @param array $input
     * @param array $args
     * @return bool
     */
    public function validate_true( bool $value , array $input , array $args ) : bool
    {
        return $value === TRUE ;
    }

    protected function startsWith( $haystack , $needle ) :bool
    {
        return $haystack[0] === $needle[0]
            ? strncmp( $haystack, $needle, strlen( $needle ) ) === 0
            : false;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() : string
    {
        return '[' . get_class( $this ) . ']' ;
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return bool
     */
    protected function validateDate( string $date , string $format = 'Y-m-d' ) : bool
    {
        $d = DateTime::createFromFormat( $format , $date ) ;
        return $d && $d->format($format) == $date ;
    }

    /**
     * @param string $dateStr
     *
     * @return bool
     */
    protected function validateISO8601Date( string $dateStr ) : bool
    {
        if( preg_match('/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/', $dateStr) > 0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * @param string $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_version( string $value , array $input , array $args ) : bool
    {
        $v = Version::fromString( $value ) ;
        if( $v == NULL )
        {
            return FALSE ;
        }
        return TRUE ;
    }

    /**
     * @param string $label
     * @param mixed $value
     * @param array $args
     * @param Exception $error
     */
    protected function warning( string $label , $value , array $args , Exception $error )
    {
        if( $this->logger )
        {
            $this->logger->warning
            (
                $this
                . ' '
                . $label
                . ' value:' . json_encode($value)
                . ' args:'  . json_encode($args)
                . ' error:' . $error->getMessage()
            ) ;
        }
    }
}


