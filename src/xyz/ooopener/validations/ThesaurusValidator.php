<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Thesaurus;

use Psr\Container\ContainerInterface;

use Exception ;

class ThesaurusValidator extends Validation
{
    /**
     * ThesaurusValidator constructor.
     * @param ContainerInterface $container
     * @param Thesaurus $model
     */
    public function __construct( ContainerInterface $container , Thesaurus $model )
    {
        parent::__construct( $container ) ;
        $this->model = $model ;
        $this->addFieldMessages
        ([
            'bgcolor' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ],
            'color' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ],
            'alternateName' =>
            [
                'alnumSlash'          => 'The alternate name must be alphanumeric with slashes, dashes and underscores permitted.' ,
                'notEmpty'            => 'That alternate name is not empty.' ,
                'uniqueAlternateName' => 'That alternate name is already in use.'
            ]
        ]);
    }

    protected Thesaurus $model ;

    /**
     * Validates if a name is unique in the thesaurus table.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_uniqueAlternateName( $value , $input , $args )
    {
        try
        {
            if( $args && count($args) > 0 )
            {
                if
                (
                    $this->model->existByAlternateName( $args[0] )
                    && ($args[0] == $value)
                )
                {
                    return TRUE ;
                }
            }
            return $this->model->existByAlternateName( $value ) === FALSE ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


