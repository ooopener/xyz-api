<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

use Exception ;

class ObservationValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'after' =>
            [
                'after' => 'The end date is not after the start date.'
            ],
            'endDate' =>
            [
                'datetime' => 'The end date is invalid.'
            ],
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ],
            'startDate' =>
            [
                'datetime' => 'The start date is invalid.'
            ],
            'eventStatus' =>
            [
                'eventStatus' => 'The status don\'t exist.'
            ],
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if a endDate is after after the startDate.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_after( $value , $input , $args ) :bool
    {
        return $value === true ;
    }

    /**
     * Validates if the specific status exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_eventStatus( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('observationsStatus')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the specific type exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('observationsTypes')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}
