<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\LivestockNumbers;
use Exception ;

use Psr\Container\ContainerInterface;

class WorkplaceValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'identifier' =>
            [
                'identifier' => 'The identifier is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the identifier exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_identifier( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( LivestockNumbers::class )
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


