<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\People ;
use xyz\ooopener\models\Teams ;

use Psr\Container\ContainerInterface ;

use Exception ;

class UserValidator extends Validation
{
    public function __construct( ContainerInterface $container , array $types )
    {
        parent::__construct( $container ) ;
        $this->types = $types ;
        $this->addFieldMessages
        ([
            'person' =>
            [
                'person' => 'Select a valid person.'
            ],
            'team' =>
            [
                'team' => 'Select a valid team.'
            ],
            'url' =>
            [
                'url' => 'The URL is not a valid API resource URL'
            ]
        ]);
    }

    const TEST_PATH = "/^([a-zA-Z]+\/)+[0-9]+$/" ;

    public array $types ;

    /**
     * Validates if the person is registered.
     *
     * @param string $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_person( string $value , array $input , array $args ) : bool
    {
        try
        {
            return $this->container
                        ->get( People::class )
                        ->exist( $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }
        return FALSE ;
    }

    /**
     * Validates if the team is registered.
     *
     * @param string $value
     * @param array $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_team( string $value , array $input , array $args ) : bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            $teamsModel = $this->container->get( Teams::class ) ;
            return $teamsModel->exist( $value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validate the specific url.
     * @param string $value
     * @param array $input
     * @param array $args
     * @return bool
     */
    public function validate_url( string $value , array $input , array $args ) : bool
    {
        $appUrl  = $this->container->get( 'settings' )['app']['url'] ;
        $urlPath = substr( $value , strlen( $appUrl ) ) ;

        // $this->logger->debug( 'validate_url value:' . $value . ' urlPath:' . $urlPath . ' :::: ' . preg_match( "/^([a-zA-Z]+\/)+[0-9]+$/" , $urlPath ) ) ;

        if
        (
            $this->startsWith( $value , $appUrl ) &&
            preg_match( self::TEST_PATH , $urlPath ) === 1 )
        {
            $array = explode( '/' , $urlPath ) ;
            $id    = array_pop( $array ) ;
            $type  = implode( '/' , $array ) ;

            // $this->logger->debug( 'validate_url type:' . $type . ' id:' . $id ) ;

            if( is_array( $this->types ) && isset( $this->types[$type] ) )
            {
                [ 'name' => $name ] = $this->types[$type] ;
                $model = $this->container->get( $name ) ;

                if( $model )
                {
                    return $model->exist( $id ) ;
                }

                // $this->logger->debug( $this . ' validate_url failed, no model with the type:' . $type . ' name:' . $name ) ;
            }
        }

        return false ;
    }
}


