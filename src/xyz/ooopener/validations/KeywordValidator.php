<?php

namespace xyz\ooopener\validations;

use Psr\Container\ContainerInterface;

class KeywordValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'name' =>
            [
                'required' => 'Un nom est requis.'
            ]
            ,
            'url' =>
            [
                'required' => 'Une url valide est requise.',
                'url'      => 'Une url valide est requise.'
            ]
        ]);
    }
}


