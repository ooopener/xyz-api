<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class ConceptualObjectMaterialValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'material' =>
            [
                'material' => 'The material reference don\'t exist.'
            ]
            ,
            'technique' =>
            [
                'technique' => 'The technique reference don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the material exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_material( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('materials')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the object technique exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_technique( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('conceptualObjectsTechniques')
                        ->exist( (string)$value ) ;
        }
        catch( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


