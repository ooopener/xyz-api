<?php

namespace xyz\ooopener\validations;

use xyz\ooopener\models\Organizations;
use Exception ;
use Psr\Container\ContainerInterface;

class MedicalLaboratoryValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'organization' =>
            [
                'organization' => 'The organization don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific organization exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_organization( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get( Organizations::class )
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


