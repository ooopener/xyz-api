<?php

namespace xyz\ooopener\validations;

use Imagick ;

use Psr\Container\ContainerInterface;

class MediaObjectValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;

        $this->addRuleMessages
        ([
            'required' => 'The field {field} is required.'
        ]);

        $this->addFieldMessages
        ([
            'height' =>
            [
                'min' => 'The height is too low!'
            ]
            ,
            'width' =>
            [
                'min' => 'The width is too low!'
            ]
            ,
            'image' =>
            [
                'image' => "Need a correct mime type."
            ]
        ]);
    }

    /**
     * Validates if a image is valid.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_image( $value , $input , $args )
    {
        return ($value != NULL) && ($value instanceof Imagick) ;
    }
}


