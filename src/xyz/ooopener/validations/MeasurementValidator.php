<?php

namespace xyz\ooopener\validations;

use Exception ;

use Psr\Container\ContainerInterface;

class MeasurementValidator extends Validation
{
    public function __construct( ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->addFieldMessages
        ([
            'dimension' =>
            [
                'dimension' => 'The dimension don\'t exist.'
            ]
            ,
            'unit' =>
            [
                'unit' => 'The unit don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the measurement dimension exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_dimension( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('measurementsDimensions')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the measurement unit exist.
     * @param $value
     * @param $input
     * @param $args
     * @return bool
     */
    public function validate_unit( $value , $input , $args ) :bool
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container
                        ->get('measurementsUnits')
                        ->exist( (string)$value ) ;
        }
        catch ( Exception $error )
        {
            $this->warning( __FUNCTION__ , $value , $args , $error ) ;
        }

        return FALSE ;
    }
}


