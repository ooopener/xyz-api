<?php

namespace xyz\ooopener\rules ;

use Psr\Http\Message\ServerRequestInterface ;

interface RuleInterface
{
    public function __invoke( ServerRequestInterface $request ) : bool ;
}
