<?php

namespace xyz\ooopener\rules ;

use Psr\Http\Message\ServerRequestInterface ;
use Psr\Log\LoggerInterface;

/**
 * Rule to decide by request path whether the request should be authenticated or not.
 */

final class RequestPathRule implements RuleInterface
{
    /**
     * Stores all the options passed to the rule
     * @var mixed[]
     */
    private $options =
    [
        "path"   => ["/"],
        "ignore" => []
    ];

    protected string $basePath;

    /**
     * @param string $basePath
     * @param mixed[] $options
     */
    public function __construct( string $basePath , array $options = [] )
    {
        $this->basePath = $basePath ;
        $this->options = array_merge( $this->options , $options ) ;
    }

    public function __invoke( ServerRequestInterface $request ) : bool
    {
        $uri = "/" . $request->getUri()->getPath() ;
        $uri = preg_replace("#/+#" , "/" , $uri ) ;

        /* If request path is matches ignore should not authenticate. */
        foreach( (array) $this->options["ignore"] as $ignore )
        {
            $ignore = rtrim( $this->basePath . $ignore, "/") ;
            if( !!preg_match("@^{$ignore}(/.*)?$@" , (string) $uri ) )
            {
                return false ;
            }
        }

        /* Otherwise check if path matches and we should authenticate. */
        foreach( (array)$this->options["path"] as $path )
        {
            $path = rtrim($path, "/") ;
            if( !!preg_match("@^{$path}(/.*)?$@" , (string) $uri ) )
            {
                return true ;
            }
        }
        return false ;
    }
}
