<?php


namespace xyz\ooopener\data;

use mysql_xdevapi\Exception;
use RuntimeException;
use Psr\Log\LoggerInterface;

use xyz\ooopener\core\Strings;

class ThesaurusCollector implements Builder
{
    /**
     * Creates a new ThesaurusCollector instance.
     * @param ?string          $cacheFile
     * @param ?LoggerInterface $logger
     * @param ?string          $routesFile
     */
    public function __construct( ?string $cacheFile = NULL , ?LoggerInterface $logger = NULL , ?string $routesFile = NULL )
    {
        $this->logger     = $logger ;
        $this->routesFile = $routesFile ;
        if( $cacheFile )
        {
            $this->setCacheFile( $cacheFile );
        }
    }

    /**
     * Path to fast cache the thesaurus definitions. Set to null to disable the thesaurus caching
     * @var ?string
     */
    protected ?string $cacheFile ;

    /**
     * The logger reference.
     */
    protected ?LoggerInterface $logger ;

    /**
     * Path to fast cache the router.
     * @var ?string
     */
    protected ?string $routesFile ;

    public function build( $init = NULL, array $options = [] , string $prefix = 'thesaurus/' )
    {
        try
        {
            $controllers = [] ;
            $models      = [] ;
            $routes      = [] ;

            if( is_array( $init ) && count( $init ) > 0 )
            {
                foreach( $init as $item )
                {
                    $key  = Strings::toCamelCase( $item->identifier ) ;
                    $path = $item->path ;

                    if( str_starts_with( $path , $prefix) )
                    {
                        $path = substr( $path, strlen($prefix)).'';

                    }

                    $models[ $key ] = $item->identifier ;

                    $controllers[ $key . 'Controller' ] =
                    [
                        'MODEL' => $key ,
                        'PATH'  => $path
                    ];

                    $routes[] = $key ;
                }
            }

            $definitions =
            [
                'CONTROLLERS' => $controllers ,
                'MODELS'      => $models ,
                'ROUTES'      => $routes
            ];

            file_put_contents
            (
                $this->cacheFile , '<?php return ' . var_export( $definitions , true ) . ';'
            );

            $this->clearRouteCache() ;
        }
        catch( Exception $exception )
        {
            if( $this->logger )
            {
                $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed ' . $exception->getMessage() );
            }
        }
    }

    public function clear() :bool
    {
        if( file_exists( $this->cacheFile ) )
        {
            unlink( $this->cacheFile ) ;
            return TRUE ;
        }
        return FALSE ;
    }

    public function clearRouteCache() :bool
    {
        try
        {
            if( is_string( $this->routesFile ) && file_exists( $this->routesFile ) )
            {
                unlink( $this->routesFile ) ;
                return TRUE ;
            }
        }
        catch( Exception $exception )
        {
            if( $this->logger )
            {
                $this->logger->warning( $this . '::' . __FUNCTION__ . ' failed ' . $exception->getMessage() );
            }
        }
        return FALSE ;
    }

    public function exists():bool
    {
        return is_string($this->cacheFile) ? file_exists( $this->cacheFile ) : FALSE ;
    }

    /**
     * Get the path of the thesaurus cache file.
     * @return ?string
     */
    public function getCacheFile(): ?string
    {
        return $this->cacheFile;
    }

    /**
     * Sets the path of the thesaurus cache file.
     * @param ?string $cacheFile The file path.
     * @return ThesaurusCollector
     */
    public function setCacheFile( string $cacheFile ):ThesaurusCollector
    {
        if ( file_exists( $cacheFile ) && !is_readable($cacheFile) )
        {
            throw new RuntimeException( sprintf( $this . '::' . __FUNCTION__ . ' cache file `%s` is not readable' , $cacheFile ) );
        }

        if ( !file_exists($cacheFile) && !is_writable(dirname( $cacheFile ) ) )
        {
            throw new RuntimeException( sprintf('Route collector cache file directory `%s` is not writable', dirname($cacheFile)) );
        }

        $this->cacheFile = $cacheFile ;

        return $this;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return '[' . get_class($this) . ']' ;
    }

}