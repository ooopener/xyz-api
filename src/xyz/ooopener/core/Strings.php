<?php

namespace xyz\ooopener\core ;

class Strings
{
    /**
     * Returns the camelcase representation of the specific expression.
     * @param string $word The expression to format.
     * @param array $separators The enumeration of separators to replace in the word expression.
     * @return string The new camelCase representation.
     */
    public static function toCamelCase(string $word, array $separators = ['_', '-', '/']): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace($separators, ' ', $word))));
    }
}