<?php

namespace xyz\ooopener\core ;

class Objects
{
    /**
     * Gets the value at `path` of `object`.
     * @param mixed $object
     * @param array $path
     * @return mixed|null
     */
    public static function getProperty( $object , array $path )
    {
        return array_reduce( $path , fn($o,$p) => $o->$p , $object ) ;
    }
}



