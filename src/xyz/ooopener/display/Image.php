<?php

namespace xyz\ooopener\display;

use Exception ;
use Imagick;
use ImagickException;
use ImagickDraw ;
use ImagickPixel ;

use graphics\geom\AspectRatio ;
use Psr\Log\LoggerInterface;

/**
 * The image class.
 */
class Image
{
    /**
     * Creates a new Image instance.
     * @param array|NULL $init
     * @param LoggerInterface|NULL $logger
     */
    public function __construct( array $init = NULL , LoggerInterface $logger = NULL )
    {
        $this->logger = $logger ;
        if( isset( $init ) )
        {
            foreach( $init as $key => $value )
            {
                if( property_exists( $this , $key ) )
                {
                    $this->$key = $value ;
                }
            }
        }
    }

    /**
     * The 'circle' overlay constant.
     */
    const OVERLAY_CIRCLE = 'circle' ;

    /**
     * The 'rounded' overlay constant.
     */
    const OVERLAY_ROUNDED = 'rounded' ;

    /**
     * The default image compression of the output image.
     */
    public int $compression = Imagick::COMPRESSION_JPEG ;

    /**
     * The logger reference.
     */
    public ?LoggerInterface $logger ;

    /**
     * The default circle settings.
     */
    public string $circleMask = '/assets/circle.svg' ;

    /**
     * The max height of the image.
     */
    public int $maxheight = 1080 ;

    /**
     * The max width of the image.
     */
    public int $maxwidth = 1920 ;

    /**
     * The root path of the image (server side).
     */
    public string $root ;

    /**
     * Indicates if the Content-Type header is added when the image is displayed.
     * @var bool
     */
    public bool $useContentType = FALSE ;

    // ---------------------------------------

    /**
     * Check the minimum resolution of an image.
     * @param string $path
     * @param int $w
     * @param int $h
     * @param null|object $crop
     * @return bool
     * @throws ImagickException
     */
    public function checkResolution( string $path , int $w , int $h , object $crop = NULL )
    {
        if( isset( $crop ) )
        {
            if( $crop->width < $w || $crop->height < $h )
            {
                return false ;
            }
        }
        else
        {
            $img = new Imagick( $path ) ;
            if( $img->getImageWidth() < $w || $img->getImageHeight() < $h )
            {
                return false ;
            }
        }
        return true ;
    }

    public function createIcon( $url , $w = 30 , $h = 30 , $color = NULL )
    {
        try
        {
            $tmp = new Imagick() ;

            $tmp->setBackgroundColor( new ImagickPixel( "transparent" ) ) ;

            $tmp->readImage( $url ) ;

            $geo = $tmp->getImageGeometry() ;

            $width  = $geo["width"] ;
            $height = $geo["height"] ;

            $ratio = min( $w / $width , $h / $height );

            $width  = round( floatval( $width * $ratio ), 3 );
            $height = round( floatval( $height * $ratio ), 3 );

            if( !empty( $color ) )
            {
                if( substr( $color , 0 , 1 ) != '#' )
                {
                    $color = '#' . $color ;
                }

                $tmp->colorizeImage( $color , new ImagickPixel('rgba(255, 255, 255, 255)') ) ;
            }

            $tmp->thumbnailImage( $width , $height , true ) ;

            return $tmp ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' createIcon(' . $url . ') failed, throwing an error:' . $e->getMessage() ) ;
            http_response_code(500) ;
            die() ;
        }
    }

    // ---------------------------------------

    /**
     * Crop the image and resize if necessary
     * @param string $path
     * @param object $crop
     * @param string $destination
     * @param int $w
     * @param int $h
     * @return bool
     */
    public function crop( string $path , object $crop , string $destination , int $w = 0 , int $h = 0 )
    {
        try
        {
            $img = new Imagick() ;

            $img->readImage( $path ) ;
            $img->cropImage( $crop->width , $crop->height , $crop->x , $crop->y ) ;

            if( $w != 0 || $h != 0 )
            {
                $img->thumbnailImage( $w , $h , true ) ;
            }

            $img->setImageFormat( 'png' ) ;
            $img->writeImage( $destination ) ;

            return true ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' crop() failed, throwing an error:' . $e->getMessage() ) ;
            return false ;
        }
    }

    // ---------------------------------------

    /**
     * Display a specific image.
     * @param Imagick $image
     * @param int $quality
     * @param bool $gray
     * @param string $format
     * @param bool $strip
     * @throws Exception
     */
    public function display( Imagick $image , int $quality = 70 , bool $gray = FALSE , string $format = 'jpg' , bool $strip = FALSE ):void
    {
        try
        {
            if ( (bool) $gray )
            {
                $image->modulateImage(100, 0, 100);
            }

            if ( !empty($format) )
            {
                $image->setImageFormat($format);
            }

            $image->setImageCompression( $this->compression );
            $image->setImageCompressionQuality( $quality );

            if ( (bool) $strip )
            {
                $image->stripImage();
            }

            if( $this->useContentType )
            {
                header( 'Content-Type: ' . 'image/' . $image->getImageFormat() );
            }

            header( 'Content-Length: ' . strlen( $image->__toString() ) );

            echo $image->getImageBlob();

            $image->clear();
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' display failed, error: ' . $e->getMessage() ) ;
            http_response_code( 500 ) ;
            die() ;
        }
    }

    // ---------------------------------------

    /**
     * The default resize method arguments.
     */
    const DEFAULT_GET_OPTIONS =
    [
        'format'  => 'jpg' ,
        'quality' => 70    ,
        'gray'    => FALSE ,
        'strip'   => TRUE
    ] ;

    /**
     * Display a basic image.
     * @param string|NULL $url
     * @param array $init
     * @return mixed
     */
    public function get( $url = NULL , array $init = [] )
    {
        [
            'format'  => $format ,
            'quality' => $quality   ,
            'gray'    => $gray ,
            'strip'   => $strip
        ]
        = array_merge( self::DEFAULT_GET_OPTIONS , $init ) ;

        try
        {
            $image = new Imagick( $url ) ;
            $this->display( $image , $quality , $gray , $format , $strip ) ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' get failed, could not load the image with the url:[' . $url . '], the following Exception occurred: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default icon method arguments.
     */
    const DEFAULT_ICON_OPTIONS =
    [
        'background' => NULL ,
        'bgcolor'    => NULL , // 'ffffff'
        'color'      => NULL , // '000000'
        'format'     => 'png' ,
        'quality'    => 70 ,
        'gray'       => FALSE ,
        'w'          => 30 ,
        'h'          => 30 ,
        'margin'     =>  2 ,
        'pinColor'   => '#ffffff' , // The default color of the pin
        'pinPath'    => NULL ,      // The pin path
        'render'     => NULL ,      // 'icon', 'map', etc.
        'strip'      => FALSE ,
        'shadow'     => NULL        // ex: '60,2,20,20'
    ] ;

    /**
     * Display an icon with the specific svg file.
     * Ex: ../icon?render=map&bgcolor=ff0000&color=ffffff&margin=4&shadow=40,2,20,20
     * @param string|NULL $url
     * @param array $init
     */
    public function icon( $url = NULL , array $init = [] ):void
    {
        [
            'background' => $background ,
            'bgcolor'    => $bgcolor ,
            'color'      => $color ,
            'format'     => $format ,
            'quality'    => $quality ,
            'gray'       => $gray ,
            'w'          => $w ,
            'h'          => $h ,
            'margin'     => $margin ,
            'pinColor'   => $pinColor ,
            'pinPath'    => $pinPath ,
            'render'     => $render ,
            'strip'      => $strip ,
            'shadow'     => $shadow
        ]
        = array_merge( self::DEFAULT_ICON_OPTIONS , $init ) ;

        // $this->logger->info( 'icon ::: ' . $url ) ;

        try
        {
            $background = NULL ;

            $icon = new Imagick() ;
            $icon->readImage( $url ) ;

            if( $color )
            {
                if( substr( $color , 0 , 1 ) != '#' )
                {
                    $color = '#' . $color ;
                }
                $icon->colorizeImage( $color , new ImagickPixel($color) ) ;
            }

            if( $bgcolor )
            {
                if( substr( $bgcolor , 0 , 1 ) != '#' )
                {
                    $bgcolor = '#' . $bgcolor ;
                }

                $bg = new Imagick();
                $bg->newImage
                (
                    $icon->getImageWidth() ,
                    $icon->getImageHeight() ,
                    new ImagickPixel( $bgcolor )
                ) ;

                $bg->compositeImage( $icon , Imagick::COMPOSITE_OVER, 0, 0 ) ;
                $icon = $bg ;
            }

            if( $render == 'map' && !empty($pinPath) )
            {
                $background = $this->createIcon
                (
                    $pinPath , $w , $h ,
                    !empty($bgcolor) ? $bgcolor : $pinColor
                ) ;

                if( $margin > 0 )
                {
                    $w -= ($margin * 2) ;
                    $h -= ($margin * 2) ;
                }
            }

            $icon->thumbnailImage( $w , $h , true ) ;

            if( isset( $background ) )
            {
                $background->compositeImage
                (
                    $icon,
                    Imagick::COMPOSITE_DEFAULT,
                    ( $background->getImageWidth() - $icon->getImageWidth()  ) / 2 ,
                    ( $background->getImageWidth() - $icon->getImageHeight() ) / 2
                );

                $background->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN ) ;
                $icon = $background ;
            }

            if( !empty($shadow) )
            {
                $shadow = explode( ',' , $shadow ) ;
                if( is_array( $shadow ) && ( count($shadow) == 4 ) )
                {
                    try
                    {
                        [ $opacity, $sigma, $x, $y ] = $shadow ;

                        $shadowIcon = clone $icon ;
                        $shadowIcon->setImageBackgroundColor( new ImagickPixel( 'black' ) ) ;
                        $shadowIcon->shadowImage( $opacity , $sigma , $x , $y ) ;
                        $shadowIcon->compositeImage( $icon, Imagick::COMPOSITE_OVER, 0, 0 );
                        $icon = $shadowIcon ;
                    }
                    catch( Exception $e  )
                    {
                        $this->warning( $this . ' icon(' . $url . ') failed with the shadow rendering, the following exception occurred: ' . $e->getMessage() );
                        http_response_code( 500 ) ;
                        echo $e ;
                        die() ;
                    }
                }
            }

            $this->display
            (
                $icon,
                $quality ,
                $gray ,
                $format,
                $strip
            ) ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following Exception occurred: ' . $e->getMessage() );
            header( "Content-Length: " . filesize( $url ) ) ;
            readfile( $url ) ;
        }
    }

    // ---------------------------------------

    /**
     * The default icon method arguments.
     */
    const DEFAULT_PHOTO_OPTIONS =
    [
        'format'     => 'jpeg'   ,
        'gray'       => FALSE    ,
        'quality'    => 70       ,
        'mode'       => 'normal' ,
        'w'          => 128      ,
        'h'          => 128      ,
        'strip'      => FALSE
    ] ;

    /**
     * Display an image with some photo rendering (gray color, resize, strip, thumb mode, etc.)
     * @param string|NULL $url The url of the image to resize.
     * @param array $init The optional setting object.
     */
    public function photo( $url = NULL , array $init = [] ):void
    {
        [
            'format'     => $format ,
            'gray'       => $gray ,
            'quality'    => $quality ,
            'mode'       => $mode ,
            'w'          => $w ,
            'h'          => $h ,
            'strip'      => $strip
        ]
        = array_merge( self::DEFAULT_ICON_OPTIONS , $init ) ;

        try
        {
            $image = new Imagick() ;
            $image->readImage( $url ) ;

            if( $mode == 'thumb' )
            {
                $image->thumbnailImage( $w , $h , true ) ;
            }

            $this->display
            (
                $image,
                $quality ,
                $gray ,
                $format ,
                $strip
            ) ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following Exception occurred: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default resize method arguments.
     */
    const DEFAULT_RESIZE_OPTIONS =
    [
        'format'  => 'jpg' ,
        'quality' => 70    ,
        'gray'    => FALSE ,
        'h'       => NULL  ,
        'w'       => NULL  ,
        'strip'   => TRUE
    ] ;

    /**
     * Resize a specific image.
     * @param string|NULL $url The url of the image to resize.
     * @param array $init The optional setting object.
     */
    public function resize( string $url = NULL , array $init = [] ):void
    {
        [
            'format'  => $format ,
            'quality' => $quality ,
            'gray'    => $gray ,
            'h'       => $h  ,
            'w'       => $w  ,
            'strip'   => $strip
        ]
        = array_merge( self::DEFAULT_RESIZE_OPTIONS , $init ) ;

        try
        {
            $image     = new Imagick( $url ) ;
            $geo       = $image->getImageGeometry() ;
            $width     = $geo['width'];
            $height    = $geo['height'];

            if( ( $width > $this->maxwidth ) || ( $height > $this->maxheight ) )
            {
                $image->resizeImage
                (
                    $this->maxwidth, $this->maxheight, Imagick::FILTER_LANCZOS, 1.1, TRUE
                ) ;
            }

            $checkW = is_int($w) && ( (int) $w > 0 ) ;
            $checkH = is_int($h) && ( (int) $h > 0 ) ;

            if( $checkW && $checkH )
            {
                $image->resizeImage( $w, $h, Imagick::FILTER_LANCZOS, 1.1, TRUE );
            }
            else if( $checkW || $checkH )
            {
                $ratio = new AspectRatio( $width , $height , TRUE ) ;

                if( $checkW > 0 )
                {
                    $ratio->setWidth( $w ) ;
                }
                else if( $checkH > 0 )
                {
                    $ratio->setHeight( $h ) ;
                }

                $image->resizeImage
                (
                    $ratio->getWidth(),
                    $ratio->getHeight(),
                    Imagick::FILTER_LANCZOS,
                    1.1,
                    TRUE
                );
            }

            $this->display( $image , $quality , $gray , $format , $strip ) ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' resize failed, could not load the image with the url:[' . $url . '], the following Exception occurred: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default thumb method arguments.
     */
    const DEFAULT_THUMB_OPTIONS =
    [
        'corners' => [ TRUE , TRUE , TRUE , TRUE ] , // array
        'h'       => 0 , // int
        'format'  => 'jpg' , // string
        'quality' => 70 , // int
        'gray'    => FALSE , // bool
        'overlay' => NULL , // string
        'padding' => 0 , // int
        'radius'  => 24 , // int
        'w'       => 0 , // int
        'strip'   => TRUE // bool
    ] ;

    /**
     * Creates the thumb of a specific image.
     * @param string|NULL $url
     * @param array $init
     * @return mixed
     */
    public function thumb( string $url = NULL , array $init = [] )
    {
        [
            'corners' => $corners,
            'h'       => $h ,
            'format'  => $format ,
            'quality' => $quality ,
            'gray'    => $gray ,
            'overlay' => $overlay , // string
            'padding' => $padding ,
            'radius'  => $radius ,
            'w'       => $w ,
            'strip'   => $strip
        ]
        = array_merge( self::DEFAULT_THUMB_OPTIONS , $init ) ;

        // -------

        switch( $overlay )
        {
            case self::OVERLAY_CIRCLE :
            case self::OVERLAY_ROUNDED :
            {
                break ; // do nothing
            }
            default :
            {
                $overlay = NULL ;
            }
        }

        if( !is_int($w) )       $w = 0 ;
        if( !is_int($h) )       $h = 0 ;
        if( !is_int($padding) ) $padding = 0 ;
        if( !is_int($radius)  ) $radius = 0 ;

        // -------

        try
        {
            $image  = new Imagick( $url ) ;
            $geo    = $image->getImageGeometry() ;
            $width  = $geo['width'];
            $height = $geo['height'];

            if( $w > 0 && $h > 0 )
            {
                $image->resizeImage( $w, $h, Imagick::FILTER_LANCZOS, 1.1, TRUE );
            }

            $mask = NULL ;

            $image->cropThumbnailImage( $w , $h );

            switch( $overlay )
            {
                case self::OVERLAY_CIRCLE : // ?w=300&h=300&overlay=circle
                {
                    $format   = 'png' ;
                    $maskFile = $this->root . $this->circleMask ;
                    if( file_exists( $maskFile ) )
                    {
                        $mask = $this->createIcon( $maskFile , $w , $h ) ;
                    }
                    break ;
                }

                case self::OVERLAY_ROUNDED : // ?w=716&h=500&overlay=rounded&radius=42&corners=false,TRUE,false,false
                {
                    $format = 'png' ;

                    $mask = new Imagick();
                    $mask->newImage( $w , $h , new ImagickPixel('transparent') );

                    $draw = new ImagickDraw();
                    $draw->setFillColor( new ImagickPixel('black') );

                    $padding = 0 ;

                    $draw->roundRectangle( $padding, $padding, $w+$padding, $h+$padding, $radius, $radius );

                    if( $corners[0] == FALSE ) // tl
                    {
                        $draw->rectangle( $padding, $padding , $radius + $padding , $radius + $padding ) ;
                    }

                    if( $corners[1] == FALSE ) // tr
                    {
                        $draw->rectangle( $w - $padding - $radius , $padding , $w - $padding , $radius - $padding ) ;
                    }

                    if( $corners[2] == FALSE ) // br
                    {
                        $draw->rectangle( $w - $padding - $radius , $h - $padding - $radius, $w - $padding , $h - $padding ) ;
                    }

                    if( $corners[3] == FALSE ) // bl
                    {
                        $draw->rectangle( $padding , $h - $padding - $radius, $radius + $padding , $h - $padding ) ;
                    }

                    $mask->drawImage( $draw );

                    break ;
                }
            }

            if( $mask )
            {
                $image->compositeImage( $mask , Imagick::COMPOSITE_DSTIN , 0, 0, Imagick::CHANNEL_ALPHA);
                $mask->clear() ;
            }

            $this->display( $image , $quality , $gray , $format , $strip ) ;
        }
        catch( Exception $e )
        {
            $this->warning( $this . ' thumb failed, could not load the image with the url:[' . $url . '], the following Exception occurred: ' . $e->getMessage() );
        }
    }

    /**
    * Returns a String representation of the object.
    * @return string A string representation of the object.
    */
    public function __toString():string
    {
        return '[' . get_class($this) . ']' ;
    }

    /**
     * Log a warning message if the logger property exist.
     * @param string|NULL $message
     */
    public function warning( string $message = NULL )
    {
        if( $this->logger )
        {
            $this->logger->warning( $message );
        }
        else
        {
            error_log( $message ) ;
        }
    }
}


