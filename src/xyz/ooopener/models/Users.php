<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Users model.
 */
class Users extends Collections
{
    /**
     * Creates a new Users instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'   => Thing::FILTER_DEFAULT,
        'name' => Thing::FILTER_DEFAULT,
        'uuid' => Thing::FILTER_DEFAULT,

        'person' => Thing::FILTER_DEFAULT,
        'team'   => Thing::FILTER_DEFAULT,

        'gender'     => Thing::FILTER_DEFAULT,
        'givenName'  => Thing::FILTER_DEFAULT,
        'familyName' => Thing::FILTER_DEFAULT,

        'active'       => Thing::FILTER_DEFAULT,
        'image'        => Thing::FILTER_DEFAULT,
        'provider'     => Thing::FILTER_DEFAULT,
        'provider_uid' => Thing::FILTER_DEFAULT,

        'email'     => Thing::FILTER_DEFAULT,
        'faxNumber' => Thing::FILTER_DEFAULT,
        'telephone' => Thing::FILTER_DEFAULT,

        // postal address

        'address' => Thing::FILTER_DEFAULT
    ];

    const ARGUMENTS_GET_DEFAULT =
    [
        'conditions'  => [],
        'facets'      => NULL,
        'key'         => '_key',
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'search'      => NULL,
        'sort'        => NULL
    ] ;

    public function getFavorites( $value , $init = [] )
    {
        [
            'conditions'  => $conditions,
            'facets'      => $facets,
            'key'         => $key,
            'lang'        => $lang,
            'limit'       => $limit,
            'offset'      => $offset,
            'search'      => $search,
            'sort'        => $sort
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $init ) ;

        $model = $this->container->get( 'things' ) ;

        // $this->logger->warning( $this . '::getFavorites #1 id:' . $value . ' init:' . json_encode($init)) ;

        $binds = [ "value" => $value ] ;

        // -------- FILTER

        $this->prepareSearch( $search , $conditions , $binds , 'doc_favorites' ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        $filter = '' ;

        if( count($conditions) > 0 )
        {
            $filter = ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value '
               . 'LET total = LENGTH( FOR doc_favorites IN INBOUND doc users_favorites ' . $filter . ' RETURN 1 ) '
               . 'LET favorites = '
               . '( '
               . 'FOR doc_favorites , edge_favorites IN INBOUND doc users_favorites '
               . 'OPTIONS { bfs : true , uniqueVertices : "global" } '
               . $filter
        ;

        // -------- ORDER BY

        if( !$this->prepareOrders( $sort , $query , 'doc_favorites' ) )
        {
            $query .= 'SORT edge_favorites.created DESC ' ;
        }

        // -------- LIMIT / OFFSET

        $this->prepareLimit( $query , $binds , $limit , $offset ) ;

        // -------- FAVORITES

        $thingFields = $this->container
                            ->get('thingsController')
                            ->getFields() ;

        $returnThing = $model->getFields( NULL , $thingFields , $lang , 'favorites' ) ;

        $query .= $returnThing . ' ) ' ;

        // -------- RETURN

        $query .= 'RETURN { favorites : favorites , total : total }' ;

        // -------- PREPARE

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds ,
            'fullCount' => (bool) $limit
        ]);

        // -------- EXECUTE

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Insert a new item into the table.
     * @param array $init
     * @return NULL|integer
     * @throws
     */
    public function insert( array $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add default team and dates
            if( !array_key_exists( 'team' , $init ) )
            {
                $values[] = 'team: "guest"' ;
            }
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table ;

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->arangodb->execute() ;

            return TRUE ;

        }

        return null ;
    }
}


