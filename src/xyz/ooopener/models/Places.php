<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Places model.
 */
class Places extends Collections
{
    /**
     * Creates a new Places instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'         => Thing::FILTER_INT,
        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,
        'status'     => Thing::FILTER_DEFAULT,

        'geo'       => Thing::FILTER_DEFAULT,
        'elevation' => Thing::FILTER_DEFAULT,

        'address'           => Thing::FILTER_DEFAULT,
        'services'          => Thing::FILTER_DEFAULT,
        'capacity'          => Thing::FILTER_INT,
        'numAttendee'       => Thing::FILTER_INT,
        'remainingAttendee' => Thing::FILTER_INT,
        'publicAccess'      => Thing::FILTER_INT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'slogan'        => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT
    ];

    /**
     * The default 'near' method options.
     */
    protected const ARGUMENTS_NEAR_DEFAULT =
    [
        'active'      => TRUE,
        'binds'       => [],
        'conditions'  => [],
        'distance'    => 0,
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'lang'        => NULL,
        'latitude'    => 0,
        'longitude'   => 0,
        'limit'       => 0,
        'order'       => 'ASC',
        'offset'      => 0,
        'queryFields' => NULL ,
        'search'      => NULL,
        'fields'      => '*' ,
        'values'      => NULL,
        'words'       => NULL
    ] ;

    /**
     * Returns all elements from a specific date.
     * @param array $init
     * @return array all elements from a specific date.
     * @throws Exception
     */
    public function near( array $init = [] )
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'distance'    => $distance,
            'facets'      => $facets,
            'fetchStyle'  => $fetchStyle,
            'fields'      => $fields ,
            'lang'        => $lang,
            'latitude'    => $latitude,
            'longitude'   => $longitude,
            'limit'       => $limit,
            'order'       => $order,
            'offset'      => $offset,
            'queryFields' => $queryFields ,
            'search'      => $search,
        ]
        = array_merge( self::ARGUMENTS_NEAR_DEFAULT , $init ) ;

        $binds['latitude']  = $latitude ;
        $binds['longitude'] = $longitude ;

        $query = 'FOR doc IN ' . $this->table . ' ' ; // INDEX

        $query .= 'LET distance = DISTANCE( doc.geo.latitude , doc.geo.longitude , @latitude , @longitude ) ' ;

        if( $distance > 0 )
        {
            $binds['radius'] = $distance ;
            $query .= ' FILTER distance <= @radius ' ;
        }

        $query .= ' SORT distance ASC ' ;

        if( $queryFields && is_array($queryFields) )
        {
            $queryFields[Thing::FILTER_DISTANCE] = [ 'filter' => Thing::FILTER_DISTANCE ];
        }

        $this->prepareLimit( $query , $binds , $limit , $offset ) ;

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * The default 'sameGeoExists' method options.
     */
    protected const ARGUMENTS_SAME_GEO_EXISTS_DEFAULT =
    [
        'active'      => TRUE,
        'lang'        => NULL,
        'latitude'    => 0,
        'longitude'   => 0,
        'queryFields' => NULL ,
        'fields'      => '*' ,
        'values'      => NULL,
        'words'       => NULL
    ] ;

    public function sameGeoExists( $init = [] )
    {
        [
            'lang'        => $lang,
            'latitude'    => $latitude,
            'longitude'   => $longitude,
            'queryFields' => $queryFields ,
            'fields'      => $fields,
        ]
        = array_merge( self::ARGUMENTS_SAME_GEO_EXISTS_DEFAULT , $init ) ;

        // -------- BINDS

        $binds = [] ;

        $binds['latitude']  = $latitude ;
        $binds['longitude'] = $longitude ;

        // -------- SELECT

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.geo.latitude == @latitude && doc.geo.longitude == @longitude ' ;

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        // -------- PREPARE

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
        ]);

        // -------- EXECUTE

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}

