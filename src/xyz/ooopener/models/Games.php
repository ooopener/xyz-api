<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Games model.
 */
class Games extends Collections
{
    /**
     * Creates a new Games instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct(ContainerInterface $container , ?string $table, array $init = [])
    {
        parent::__construct($container, $table, $init);
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'image' => Thing::FILTER_DEFAULT,
        'path'  => Thing::FILTER_DEFAULT,

        'about' => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,

        'item'  => Thing::FILTER_DEFAULT,
        'quest' => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT

    ];

    /**
     * Indicates if the item exist.
     * @param integer $about
     * @param string $id
     * @param string $edge
     * @return bool
     * @throws Exception
     */
    public function existAbout( int $about , string $id , string $edge ):bool
    {
        $query = 'FOR doc IN ' . $this->table .
                ' FILTER doc._key == @id ' .
                ' RETURN COUNT( FOR doc_quest IN INBOUND doc @@edge ' .
                ' OPTIONS { bfs : true , uniqueVertices : \'global\' } ' .
                ' FILTER doc_quest.about == @about ' .
                ' RETURN 1 )';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "about" => $about ,
                "@edge" => $edge,
                "id"    => $id
            ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }
}
