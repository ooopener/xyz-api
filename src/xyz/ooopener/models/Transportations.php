<?php

namespace xyz\ooopener\models;

use Psr\Container\ContainerInterface;

use xyz\ooopener\things\Thing;

class Transportations extends Things
{
    /**
     * Creates a new Transportations instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct($container , $table , $init);
    }

    public array $fillable =
    [
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'name'           => Thing::FILTER_DEFAULT,
        'duration'       => Thing::FILTER_DEFAULT,
        'comment'        => Thing::FILTER_DEFAULT
    ];
}
