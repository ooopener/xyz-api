<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Organizations model.
 */
class Organizations extends Collections
{
    /**
     * Creates a new Organizations instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'              => Thing::FILTER_INT,
        'identifier'      => Thing::FILTER_INT,
        'path'            => Thing::FILTER_DEFAULT,
        'name'            => Thing::FILTER_DEFAULT,
        'legalName'       => Thing::FILTER_DEFAULT,
        'active'          => Thing::FILTER_INT,
        'withStatus'      => Thing::FILTER_DEFAULT,
        'dissolutionDate' => Thing::FILTER_DEFAULT,
        'foundingDate'    => Thing::FILTER_DEFAULT,
        'taxID'           => Thing::FILTER_DEFAULT,
        'vatID'           => Thing::FILTER_DEFAULT,
        'address'         => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'slogan'        => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT
    ];

}

