<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class Photos extends Things
{
    /**
     * Creates a new Photos instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'owner' => Thing::FILTER_INT,
        'media' => Thing::FILTER_INT,
    ];
}
