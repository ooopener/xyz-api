<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Offers model.
 */
class Offers extends Things
{
    /**
     * Creates a new Offers instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'category'      => Thing::FILTER_DEFAULT,
        'price'         => Thing::FILTER_DEFAULT,
        'priceCurrency' => Thing::FILTER_DEFAULT,
        'validFrom'     => Thing::FILTER_DEFAULT,
        'validThrough'  => Thing::FILTER_DEFAULT
    ];


}


