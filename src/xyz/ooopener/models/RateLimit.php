<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\helpers\UserInfos;

class RateLimit
{
    /**
     * @param $expire
     * @return array
     */
    public function get( $expire )
    {
        $ip = $this->container->get( UserInfos::class )->getIp() ;

        $ip = preg_replace("/[.:]/" , "_", $ip ) ;

        $jti = preg_replace("/-/" , "_", $this->container->get('jwt')->jti ) ;

        if( $this->documentHandler->has( 'rateLimit' , sprintf( "d-%s-%s" , $ip , $jti ) ) )
        {
            $a = $this->documentHandler->get( 'rateLimit' , sprintf( "d-%s-%s" , $ip , $jti ) ) ;

            $date = $a->get( 'date' ) ;
            $now = round( microtime( true ) * 1000 ) ;
            if( $date + ( $expire * 1000 * 60 ) < $now )
            {
                // remove all value for this user
                $query = 'FOR rl IN rateLimit FILTER REGEX_TEST( rl._key , @key ) REMOVE { _key: rl._key } IN rateLimit' ;

                $this->statement = new ArangoStatement
                (
                    $this->connection ,
                    [
                        'query' => $query ,
                        'bindVars' => [ "key" => sprintf( "rl-%s-%s-*" , $ip , $jti ) ]
                    ]
                );

                $this->statement->execute() ;

                $this->documentHandler->remove( $a ) ;
            }

        }

        $query = 'FOR rl IN rateLimit FILTER REGEX_TEST( rl._key , @key ) SORT rl.date RETURN rl.date' ;

        $this->statement = new ArangoStatement
        (
            $this->connection ,
            [
                'query' => $query ,
                'bindVars' => [ "key" => sprintf( "rl-%s-%s-*" , $ip , $jti ) ]
            ]
        );

        $cursor = $this->statement->execute() ;

        return $cursor->getAll() ;
    }

    /**
     * @param $response
     * @return bool
     * @throws ArangoException
     */
    public function set( $response )
    {
        try
        {
            $ip = $this->container->get( UserInfos::class )->getIp() ;
            $ip = preg_replace("/[.:]/" , "_", $ip ) ;

            $jti = preg_replace("/-/" , "_", $this->container->get('jwt')->jti ) ;

            if( $jti )
            {
                if( !$this->documentHandler->has( 'rateLimit' , sprintf( "d-%s-%s" , $ip , $jti ) ) )
                {
                    $a = new ArangoDocument();
                    $a->setInternalKey( sprintf( "d-%s-%s" , $ip , $jti ) ) ;
                    $a->set( 'date' , round( microtime( true ) * 1000 ) ) ;
                    $this->documentHandler->save( 'rateLimit' , $a ) ;
                }
                // create a new document

                $query = 'INSERT { _key:@key , date:DATE_NOW() } IN rateLimit' ;

                $this->statement = new ArangoStatement
                (
                    $this->connection ,
                    [
                        'query' => $query ,
                        'bindVars' => [ "key" => sprintf( "rl-%s-%s-%s" , $ip , $jti , microtime( true ) ) ]
                    ]
                );

                $cursor = $this->statement->execute() ;

                return TRUE ;
            }
            else
            {
                return FALSE ;
            }

        }
        catch( ArangoConnectException $e )
        {
            return $this->formatError( $response , '500', [ 'setRateLimit()' , $e->getMessage() ] , NULL , 500 );
        }
        catch( ArangoClientException $e )
        {
            return $this->formatError( $response , '500', [ 'setRateLimit()' , $e->getMessage() ] , NULL , 500 );
        }
        catch( ArangoServerException $e )
        {
            return $this->formatError( $response , '500', [ 'setRateLimit()' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
