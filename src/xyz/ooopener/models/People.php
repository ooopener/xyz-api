<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class People extends Collections
{
    /**
     * Creates a new People instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container  , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'         => Thing::FILTER_INT,
        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'path' => Thing::FILTER_DEFAULT,

        'givenName'     => Thing::FILTER_DEFAULT,
        'familyName'    => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'gender'        => Thing::FILTER_INT,

        'honorificPrefix' => Thing::FILTER_INT,
        'honorificSuffix' => Thing::FILTER_DEFAULT,

        'nationality' => Thing::FILTER_DEFAULT,
        'birthDate'   => Thing::FILTER_DEFAULT,
        'birthPlace'  => Thing::FILTER_DEFAULT,
        'deathDate'   => Thing::FILTER_DEFAULT,
        'deathPlace'  => Thing::FILTER_DEFAULT,

        'address' => Thing::FILTER_DEFAULT,

        'taxID' => Thing::FILTER_DEFAULT,
        'vatID' => Thing::FILTER_DEFAULT,

        'description' => Thing::FILTER_DEFAULT,
        'text'        => Thing::FILTER_DEFAULT,
        'notes'       => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT
    ];
}
