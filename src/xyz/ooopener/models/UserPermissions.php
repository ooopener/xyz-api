<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing ;

use Psr\Container\ContainerInterface ;

/**
 * The UserPermissions model.
 */
class UserPermissions extends Model
{
    /**
     * Creates a new UserPermissions instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'user'       => Thing::FILTER_DEFAULT,
        'module'     => Thing::FILTER_DEFAULT,
        'resource'   => Thing::FILTER_INT,
        'permission' => Thing::FILTER_DEFAULT
    ];
}


