<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Stages model.
 */
class Stages extends Collections
{
    /**
     * Creates a new Stages instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct(ContainerInterface $container, ?string $table, array $init = [])
    {
        parent::__construct($container, $table, $init);
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT,
        'discover'  => Thing::FILTER_DEFAULT
    ];

    public function deleteDiscoverReverse( $ref )
    {
        $binds = [ 'ref' => $ref ];

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER POSITION( doc.discover , @ref ) '
               . 'UPDATE doc '
               . 'WITH { discover : REMOVE_VALUE( doc.discover , @ref ) , modified : DATE_ISO8601( DATE_NOW() ) } '
               . 'IN ' . $this->table . ' '
               . 'RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}
