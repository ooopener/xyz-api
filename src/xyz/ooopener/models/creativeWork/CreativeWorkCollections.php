<?php

namespace xyz\ooopener\models\creativeWork;

use xyz\ooopener\models\Collections;
use xyz\ooopener\things\Thing;
use Psr\Container\ContainerInterface;

class CreativeWorkCollections extends Collections
{
    /**
     * Creates a new ConceptualObjectNumbers instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'name'  => Thing::FILTER_DEFAULT,
        'path'  => Thing::FILTER_DEFAULT,
        'image' => Thing::FILTER_INT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'endDate'   => Thing::FILTER_DEFAULT,
        'startDate' => Thing::FILTER_DEFAULT
    ];
}
