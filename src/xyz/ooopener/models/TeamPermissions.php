<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The TeamPermissions model.
 */
class TeamPermissions extends Model
{
    /**
     * Creates a new TeamPermissions instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'team'       => Thing::FILTER_INT,
        'module'     => Thing::FILTER_DEFAULT,
        'resource'   => Thing::FILTER_INT,
        'permission' => Thing::FILTER_DEFAULT,
        'visible'    => Thing::FILTER_BOOL
    ];
}


