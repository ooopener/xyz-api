<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class Observations extends Collections
{
    /**
     * Creates a new Observations instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'endDate'     => Thing::FILTER_DEFAULT,
        'eventStatus' => Thing::FILTER_DEFAULT,
        'startDate'   => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,

        'review'     => Thing::FILTER_DEFAULT,
        'image'      => Thing::FILTER_INT,
        'firstOwner' => Thing::FILTER_INT,
        'isBasedOn'  => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT
    ];
}
