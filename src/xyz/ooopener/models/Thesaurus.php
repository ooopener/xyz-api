<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The thesaurus model.
 */
class Thesaurus extends Model
{
    /**
     * Creates a new Model instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct
        (
            $container ,
            $table ,
            array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] )
        );
    }

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        Model::FACETABLE  =>
        [
            'alternateName' =>
            [
                'alternateName' => 'field' ,
                'type'          => Thing::FILTER_DEFAULT
            ],
            'name' =>
            [
                'name' => 'field' ,
                'type' => Thing::FILTER_DEFAULT
            ]
        ],
        Model::SEARCHABLE => [ 'name.fr', 'name.en' , 'name.it' , 'name.es' , 'name.de' , 'alternateName' ],
        Model::SORTABLE   =>
        [
            'id'            => 'CAST(id as SIGNED INTEGER)',
            'fr'            => 'name.fr',
            'en'            => 'name.en',
            'it'            => 'name.it',
            'es'            => 'name.es',
            'de'            => 'name.de',
            'alternateName' => 'alternateName',
            'active'        => 'CAST(active as SIGNED INTEGER)',
            'created'       => 'created',
            'modified'      => 'modified'
        ]
    ] ;

    // -----

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'active'        => Thing::FILTER_BOOL,
        'image'         => Thing::FILTER_BOOL,
        'path'          => Thing::FILTER_DEFAULT,
        'name'          => Thing::FILTER_DEFAULT,
        'pattern'       => Thing::FILTER_DEFAULT,
        'format'        => Thing::FILTER_DEFAULT,
        'validator'     => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'color'         => Thing::FILTER_DEFAULT,
        'bgcolor'       => Thing::FILTER_DEFAULT
    ];

    // -----

    /**
     * The default 'all' methods options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'      => TRUE ,
        'binds'       => [] ,
        'conditions'  => [] ,
        'facets'      => NULL ,
        'limit'       => 0 ,
        'offset'      => 0 ,
        'sort'        => NULL ,
        'search'      => NULL ,
        'fields'      => '*'  ,
        'lang'        => NULL ,
        'queryFields' => NULL
    ] ;

    /**
     * Return all elements.
     * @param array $init
     * @return array All the elements
     * @throws Exception
     */
    public function all( array $init = [] ):array
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'facets'      => $facets,
            'limit'       => $limit,
            'offset'      => $offset,
            'sort'        => $sort,
            'search'      => $search,
            'fields'      => $fields,
            'lang'        => $lang,
            'queryFields' => $queryFields
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table ;

        $this->prepareSearch( $search , $conditions , $binds ) ;
        $this->prepareActive( $active , $conditions ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        $this->prepareConditions( $query , $conditions );           // FILTER
        $this->prepareOrders( $sort , $query ) ;                    // SORT
        $this->prepareLimit( $query , $binds , $limit , $offset ) ; // LIMIT / OFFSET

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'key' => '_key'
    ] ;

    /**
     * Delete the specific item
     * @param string|integer $value
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function delete( $value , array $init = [] )
    {
        [ 'key' => $key ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $query = 'FOR doc IN '
               . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value '
               . 'REMOVE { _key: doc._key } IN ' . $this->table . ' '
               . 'RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Delete items
     * @param $items
     * @param array $init
     * @return bool
     * @throws Exception
     */
    public function deleteAll( $items , array $init = [] ):bool
    {
        [ 'key' => $key ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $in    = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        //
        $query .= ' REMOVE { _key : doc._key } IN ' . $this->table . ' RETURN OLD';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getAll() ;
    }

    /**
     * Indicates if the specific item exist (by alternateName only).
     *
     * @param string $value
     *
     * @return bool
     *
     * @throws Exception
     */
    public function existByAlternateName( string $value )
    {
        return $this->exist( $value , [ 'key' => 'alternateName' ] ) ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'fields' => '*' ,
        'key'    => 'id'
    ] ;

    /**
     * Find the specific service by the alternateName.
     * @param string $value
     * @return object the specific service by the alternateName.
     * @throws Exception
     */
    public function getByAlternateName( string $value ):object
    {
        return $this->get( $value , [ 'key' => 'alternateName' ] ) ;
    }

    /**
     * Insert a new item into the table
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function insert( array $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->arangodb->execute() ;

            return $this->arangodb->getObject() ;
        }
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     * @param mixed $item
     * @param string $keyValue
     * @param array $init
     *
     * @return object
     *
     * @throws Exception
     */
    public function update( $item , string $keyValue , array $init = [] ) : object
    {
        [
            'active' => $active,
            'key'    => $key
        ]
        = array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if
            (
                is_array( $item ) && array_key_exists( $property , $item )
            )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}


