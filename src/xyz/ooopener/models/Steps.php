<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Steps model.
 */
class Steps extends Collections
{
    /**
     * Creates a new Steps instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct($container, $table, $init);
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'position'   => Thing::FILTER_INT,
        'path'       => Thing::FILTER_DEFAULT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'audible'    => Thing::FILTER_BOOL,
        'geofencing' => Thing::FILTER_BOOL,
        'geoRadius'  => Thing::FILTER_INT,
        'listable'   => Thing::FILTER_BOOL,
        'mappable'   => Thing::FILTER_BOOL,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT

    ];
}
