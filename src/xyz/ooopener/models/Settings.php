<?php

namespace xyz\ooopener\models;

use Psr\Container\ContainerInterface;

class Settings extends Edges
{
    /**
     * Settings constructor.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct
        (
            $container ,
            $table ,
            array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] )
        );
    }

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        Model::FACETABLE  => NULL,
        Model::SEARCHABLE => [ 'fr', 'en' , 'it' , 'es' , 'de' , 'alternateName' ],
        Model::SORTABLE   =>
        [
            'id'            => 'CAST(id as SIGNED INTEGER)',
            'fr'            => 'name.fr',
            'en'            => 'name.en',
            'it'            => 'name.it',
            'es'            => 'name.es',
            'de'            => 'name.de',
            'alternateName' => 'alternateName',
            'active'        => 'CAST(active as SIGNED INTEGER)',
            'created'       => 'created',
            'modified'      => 'modified'
        ]
    ] ;
}
