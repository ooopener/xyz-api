<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The LivestocksNumbers model.
 */
class LivestockNumbers extends Model
{
    /**
     * Creates a new Livestocks instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,
        'name'           => Thing::FILTER_DEFAULT,
        'value'          => Thing::FILTER_DEFAULT
    ];


}


