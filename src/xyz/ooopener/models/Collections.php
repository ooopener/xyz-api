<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\helpers\Status;
use Psr\Container\ContainerInterface ;

/**
 * The collections model.
 */
class Collections extends Model
{
    /**
     * Creates a new Collections instance.
     * @param ContainerInterface $container
     * @param ?string            $table
     * @param array              $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'      => TRUE ,
        'binds'       => [] ,
        'conditions'  => [] ,
        'facets'      => NULL ,
        'fetchStyle'  => NULL ,
        'limit'       => 0 ,
        'offset'      => 0 ,
        'sort'        => NULL ,
        'search'      => NULL ,
        'fields'      => '*' ,
        'lang'        => NULL ,
        'queryFields' => NULL ,
        'withStatus'  => Status::PUBLISHED
    ] ;

    /**
     * Return all elements.
     * @param array $init
     * @return array All the elements
     * @throws Exception
     */
    public function all( array $init = [] ):array
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'facets'      => $facets,
            'fetchStyle'  => $fetchStyle,
            'limit'       => $limit,
            'offset'      => $offset,
            'sort'        => $sort,
            'search'      => $search,
            'fields'      => $fields ,
            'lang'        => $lang ,
            'queryFields' => $queryFields ,
            'withStatus'  => $withStatus
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ;

        // $this->logger->debug( $this . ' all' ) ;

        $query = 'FOR doc IN ' . $this->table ; // INDEX

        $this->prepareSearch ( $search , $conditions , $binds ) ;
        $this->prepareActive ( $active , $conditions          ) ;
        $this->prepareFacets ( $facets , $conditions , $binds ) ;

        $this->prepareConditions ( $query , $conditions               ) ; // FILTER
        $this->prepareOrders     ( $sort , $query                     ) ; // SORT
        $this->prepareLimit      ( $query , $binds , $limit , $offset ) ; // LIMIT / OFFSET

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        // $this->debugQuery( $query , $binds ) ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * The default 'count' methods options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active'     => TRUE,
        'binds'      => [],
        'conditions' => [],
        'facets'     => NULL,
        'search'     => NULL,
        'values'     => NULL,
        'withStatus' => Status::PUBLISHED,
        'words'      => NULL
    ] ;

    /**
     * Returns the number of elements registered in the specific table.
     * @param array $init
     * @return int The number of elements registered in the specific table.
     * @throws Exception
     */
    public function count( array $init = [] ):int
    {
        [
            'active'     => $active,
            'binds'      => $binds,
            'conditions' => $conditions,
            'facets'     => $facets,
            'search'     => $search,
            'withStatus' => $withStatus
        ]
        = array_merge( self::ARGUMENTS_COUNT_DEFAULT , $init ) ;

        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        $this->prepareSearch( $search , $conditions , $binds ) ;
        $this->prepareActive( $active , $conditions ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        $this->prepareConditions( $query , $conditions ); // FILTER

        $query .= ' RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getFirstResult() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'      => NULL,
        'conditions'  => [],
        'extraQuery'  => '',
        'fields'      => '*' ,
        'key'         => '_key',
        'lang'        => NULL,
        'queryFields' => NULL,
        'withStatus'  => Status::PUBLISHED
    ] ;

    /**
     * Returns the specific item.
     * @param mixed $value
     * @param array $init
     * @return mixed the specific item.
     * @throws Exception
     */
    public function get( $value , array $init = [] )
    {
        [
            'active'      => $active,
            'conditions'  => $conditions,
            'extraQuery'  => $extraQuery,
            'fields'      => $fields,
            'key'         => $key,
            'lang'        => $lang,
            'queryFields' => $queryFields,
            'withStatus'  => $withStatus
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $init ) ;

        // $this->logger->info( $this . ' get(' . $value . ')' ) ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value ' ;

        if( !is_null( $active ) )
        {
            $query .= ' && doc.active == ' . ((bool) $active ? 1 : 0) . ' ' ;
        }

        // extra query
        if( is_string($extraQuery) && !empty($extraQuery) )
        {
            $query .= $extraQuery ;
        }

        if( is_array($conditions) && count($conditions) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "value" => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    const ARGUMENTS_GET_ARRAY_ALL_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'field'       => 'array' ,
        'key'         => '_key',
        'groupBy'     => NULL,
        'groups'      => NULL,
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'orders'      => NULL ,
        'queryFields' => NULL ,
        'search'      => NULL,
        'sort'        => NULL,
        'values'      => NULL,
        'withStatus'  => Status::PUBLISHED,
        'words'       => NULL
    ] ;

    public function getArrayJoinAll( $value , $modelJoin , $joinFields , $init = [] )
    {
        [
            'active'      => $active,
            'conditions'  => $conditions,
            'facets'      => $facets,
            'fetchStyle'  => $fetchStyle,
            'field'       => $field ,
            'groupBy'     => $groupBy,
            'groups'      => $groups,
            'key'         => $key,
            'lang'        => $lang,
            'limit'       => $limit,
            'offset'      => $offset,
            'orders'      => $orders ,
            'queryFields' => $queryFields ,
            'search'      => $search,
            'sort'        => $sort,
            'values'      => $values,
            'withStatus'  => $withStatus,
            'words'       => $words
        ]
        = array_merge( self::ARGUMENTS_GET_ARRAY_ALL_DEFAULT , $init ) ;

        $binds = [ "value" => $value ] ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        if( !is_null($active) )
        {
            $query .= ' && doc.active == ' . ((bool) $active ? 1 : 0) ;
        }

        // -------- LIMIT / OFFSET

        $queryLimit = '' ;

        $this->prepareLimit( $queryLimit , $binds , $limit , $offset ) ;

        ////

        $ref = 'ref_' . $field ;
        $docJoin = 'doc_' . $field ;

        // retrieve position name
        $position = $joinFields['position']['unique'] ;

        // num
        $num = 'num' . ucfirst( $field ) ;

        $filterJoinFields = $this->getFilterFields( $joinFields , $field , $lang ) ;

        $query .= ' LET array_join = ( ' .
            'FOR ' . $ref . ' IN doc.' . $field . ' ' .
            $queryLimit .
            'LET ' . $position . ' = POSITION( doc.' . $field . ' , ' . $ref . ' , true ) ' .
            'FOR ' . $docJoin . ' IN ' . $modelJoin . ' ' .
            'FILTER ' . $docJoin . '._key == TO_STRING( ' . $ref . ' ) ' .
            'RETURN { ' . $filterJoinFields . ' } ' .
            ' )' ;

        if( count($conditions) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- EDGES && FIELDS

        $query .= 'RETURN { ' . $field . ' : array_join , ' . $num . ': LENGTH( doc.' . $field . ' ) } ' ;

        // $this->logger->warning( $this . ' getArrayJoinAll query: ' . json_encode($query)  ) ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    /**
     * Delete the specific item
     * @param string|integer $value
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function delete( $value , array $init = [] )
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value ';

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Delete all the specific items
     * @param $items
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function deleteAll( $items , array $init = [] ):bool
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        //
        $query .= ' REMOVE { _key : doc._key } IN ' . $this->table . ' RETURN OLD';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT =
    [
        'field'  => NULL,
        'key'    => '_key',
        'num'    => NULL,
    ] ;

    public function deleteInMultiField( $id , $idField , $init = [] )
    {
        [
            'field'  => $field,
            'key'    => $key,
            'num'    => $num
        ]
        = array_merge( self::ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT , $init ) ;

        $fields = [] ;
        $binds =
        [
            'key' => $id ,
            $field => $idField
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = 'LET newMultiField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) ' ;
            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key '
               . $q . ' '
               . 'UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' '
               . 'RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function deleteReverseInMultiField( $id , $init = [] )
    {
        [
            'field'  => $field,
            'key'    => $key,
            'num'    => $num
        ]
        = array_merge( self::ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT , $init ) ;

        $fields = [] ;
        $binds  = [ $field => $id ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = 'LET newMultiField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) ' ;
            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER POSITION( doc.hasPart , @' . $field . ') ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;

    }

    /**
     * The default 'deleteListItem' method options.
     */
    const ARGUMENTS_DELETE_LIST_ITEM_DEFAULT =
    [
        'conditions' => [], // FIXME must be removed ?
        'key'        => '_key'
    ] ;

    public function deleteListItem( $owner , $value , $keyList , $init = [] )
    {
        [ 'key' => $key ]
        = array_merge( self::ARGUMENTS_DELETE_LIST_ITEM_DEFAULT , $init ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @owner ' ;

        $fields = [] ;

        // update

        $fields[] = '@keyList: REMOVE_VALUE( doc.@keyList , @value )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query .= ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'value'   => $value ,
                'keyList' => $keyList
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function deleteListItemAll( $owner , $items , $keyList , $init = [] )
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $fields = [] ;

        $fields[] = '@keyList: REMOVE_VALUES( doc.@keyList , TO_ARRAY( @items ) )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query  = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @owner '
                . 'UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' '
                . 'RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'items'   => $items ,
                'keyList' => $keyList
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_EXISTS_MULTI_FIELD_DEFAULT =
    [
        'field'  => NULL,
        'key'    => '_key'
    ] ;

    public function existsInMultiField( $value , $idField , $init = [] ) :bool
    {
        [
            'field'  => $field,
            'key'    => $key
        ]
        = array_merge( self::ARGUMENTS_EXISTS_MULTI_FIELD_DEFAULT , $init ) ;

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value && POSITION( doc.' . $field . ' , @field ) '
               . 'RETURN 1 )';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "value" => $value,
                "field" => $idField
            ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    /**
     * Insert a new item into the table
     * @param array $init The datas to insert in the model
     * @return mixed
     * @throws Exception
     */
    public function insert( array $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->arangodb->execute() ;

            return $this->arangodb->getObject() ;
        }
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT =
    [
        'active' => NULL,
        'field'  => NULL,
        'key'    => '_key',
        'num'    => NULL,
        'side'   => 'left'
    ] ;

    public function insertInMultiField( $id , $idField , $init = [] )
    {
        [
            'active' => $active,
            'field'  => $field,
            'key'    => $key,
            'num'    => $num,
            'side'   => $side
        ]
        = array_merge( self::ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT , $init ) ;

        $fields = [] ;
        $binds =
        [
            'key'  => $id ,
            $field => $idField
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            if( $side == 'right' )
            {
                $s = 'PUSH' ;
            }
            else
            {
                $s = 'UNSHIFT' ;
            }

            $q = 'LET newMultiField = ' . $s . '( doc.' . $field . ' , @' . $field.' , true ) ' ;

            $fields[] = $field . ': newMultiField ' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'insertListItems' method options.
     */
    const ARGUMENTS_INSERT_LIST_ITEMS_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    public function insertListItems( $owner , $items , $keyList , $init = [] )
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_INSERT_LIST_ITEMS_DEFAULT , $init ) ;

        // ---- QUERY

        $fields = [] ;
        $fields[] = '@keyList: APPEND( doc.@keyList , TO_ARRAY( @items ) , true )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @owner '
               . 'UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' '
               . 'RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'items'   => $items ,
                'keyList' => $keyList
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'updateDateParentMultiField' method options.
     */
    const ARGUMENTS_UPDATE_DATE_PARENT_MULTI_FIELD_DEFAULT =
    [
        'key' => '_key'
    ] ;

    public function updateDateParentMultiField( $id , $init = [] )
    {
        [ 'key' => $key ]
        = array_merge( self::ARGUMENTS_UPDATE_DATE_PARENT_MULTI_FIELD_DEFAULT , $init ) ;

        $binds =
        [
            'value' => $id
        ] ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER POSITION( doc.' . $key . ' , @value ) ' .
            ' UPDATE doc WITH { modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_IN_MULTI_FIELD_DEFAULT =
    [
        'active'   => NULL,
        'field'    => NULL,
        'key'      => '_key',
        'num'      => NULL ,
        'position' => 0
    ] ;

    public function updateInMultiField( $id , $idField , $init = [] )
    {
        [
            'field'    => $field,
            'key'      => $key ,
            'num'      => $num ,
            'position' => $position
        ]
        = array_merge( self::ARGUMENTS_UPDATE_IN_MULTI_FIELD_DEFAULT , $init ) ;

        $fields = [] ;
        $binds =
        [
            'key'      => $id ,
            $field     => $idField,
            'position' => $position
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = ' '
               . 'LET newField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) '
               . 'LET startField = SLICE( newField , 0 , @position ) '
               . 'LET endField = SLICE( newField , @position ) '
               . 'LET newMultiField = UNION( PUSH( startField , @' . $field . ' , true ) , endField ) ' ;

            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}


