<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Workshops model.
 */
class Workshops extends Collections
{
    /**
     * Creates a new Workshops instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,
        'identifier' => Thing::FILTER_INT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'isBasedOn'  => Thing::FILTER_DEFAULT
    ];
}


