<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * This service to manage the invitation_codes table.
 */
class InvitationCodes extends Model
{
    /**
     * Creates a new InvitationCodes instance.
     *
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable = [
        'id'       => Thing::FILTER_INT,
        'user'     => Thing::FILTER_DEFAULT,
        'team'     => Thing::FILTER_INT,
        'email'    => Thing::FILTER_DEFAULT,
        'person'   => Thing::FILTER_INT,
        'code'     => Thing::FILTER_DEFAULT,
        'redirect' => Thing::FILTER_DEFAULT,
        'ip'       => Thing::FILTER_DEFAULT,
        'used'     => Thing::FILTER_INT,
        'expired'  => Thing::FILTER_INT
    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public ?array $sortable =
    [
        'id'        => 'CAST(id as SIGNED INTEGER)',
        'created'   => 'created'
    ];

    ///////////////////////////

    public function check( $code )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.code == @code && doc.used == 0 && doc.expired > DATE_ISO8601( DATE_NOW() ) RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "code" => $code
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function used( $id )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc._key == @id ' .
            ' UPDATE doc WITH { used: @used , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "id"   => $id,
                "used" => 1
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}


