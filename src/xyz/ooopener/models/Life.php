<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing;
use Psr\Container\ContainerInterface;

class Life extends Collections
{
    /**
     * Creates a new Courses instance.
     *
     * @param ContainerInterface $container
     * @param string    $table
     * @param array     $init
     */
    public function __construct(ContainerInterface $container = null, $table = null, array $init = [])
    {
        parent::__construct($container, $table, $init);
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [

        'name'       => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'path'    => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,

        'taxon'         => Thing::FILTER_DEFAULT,

        'birthDate'     => Thing::FILTER_DEFAULT,
        'deathDate'     => Thing::FILTER_DEFAULT,
        'weight'        => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT

    ];
}
