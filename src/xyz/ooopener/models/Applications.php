<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * This service to manage the applications table.
 */
class Applications extends Collections
{
    /**
     * Creates a new Applications instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'   => Thing::FILTER_INT,
        'name' => Thing::FILTER_DEFAULT,

        'active' => Thing::FILTER_INT,
        'path'   => Thing::FILTER_DEFAULT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'slogan'              => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'version' => Thing::FILTER_DEFAULT,

        'oAuth'   => Thing::FILTER_DEFAULT,
        'setting' => Thing::FILTER_DEFAULT,

        'owner' => Thing::FILTER_INT,

        'image'  => Thing::FILTER_INT ,
        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT
    ];

    /**
     * Returns the specific application representation.
     * @param string $client_id
     * @param string $client_secret
     * @return object The specific application representation.
     * @throws Exception
     */
    public function getByClientCredentials( string $client_id , string $client_secret )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.oAuth.client_id == @client_id && doc.oAuth.client_secret == @client_secret RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "client_id"     => $client_id ,
                "client_secret" => $client_secret
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Returns the specific application representation by property.
     * @param string $name
     * @param string $value
     * @param string|NULL $filter
     *
     * @return object The specific application representation by property.
     *
     * @throws Exception
     */
    public function getByProperty( string $name , string $value , string $filter = NULL  )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $name . ' == @value RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "value" => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Check if application is whitelisted
     * @param string $client_id
     * @return bool
     * @throws Exception
     */
    public function isWhitelisted( string $client_id )
    {
        $query = 'RETURN COUNT( FOR doc IN whitelist_applications FILTER doc.client_id == @value RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "value" => $client_id ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }
}


