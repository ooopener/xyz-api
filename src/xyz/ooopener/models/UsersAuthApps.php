<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * This service to manage the users_auth_apps table.
 */
class UsersAuthApps extends Model
{
    /**
     * Creates a new UsersAuthApps instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'user_id'   => Thing::FILTER_INT,
        'client_id' => Thing::FILTER_INT,
        'scope'     => Thing::FILTER_DEFAULT
    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public ?array $sortable =
    [
        'user_id'   => 'CAST(user_id as SIGNED INTEGER)',
        'client_id' => 'CAST(client_id as SIGNED INTEGER)',
        'created'   => 'created',
        'modified'  => 'modified'
    ];

    /**
     * Returns the specific user representation.
     * @param string $user_id
     * @param string $client_id
     * @param string $fields
     *
     * @return object the specific user representation.
     *
     * @throws Exception
     */
    public function getUserApp( string $user_id , string $client_id , $fields = '*' )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.user_id == @user && doc.client_id == @client_id RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "user"      => $user_id ,
                "client_id" => $client_id
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}


