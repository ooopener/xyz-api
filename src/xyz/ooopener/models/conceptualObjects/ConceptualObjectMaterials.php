<?php

namespace xyz\ooopener\models\conceptualObjects ;

use xyz\ooopener\models\Things;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class ConceptualObjectMaterials extends Things
{
    /**
     * Creates a new ConceptualObjectMaterials instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'material'      => Thing::FILTER_INT,
        'technique'     => Thing::FILTER_INT
    ];
}
