<?php

namespace xyz\ooopener\models\conceptualObjects ;

use xyz\ooopener\models\Things;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class ConceptualObjectNumbers extends Things
{
    /**
     * Creates a new ConceptualObjectNumbers instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'           => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'date'           => Thing::FILTER_DEFAULT,
        'value'          => Thing::FILTER_DEFAULT
    ];
}
