<?php

namespace xyz\ooopener\models\conceptualObjects ;

use xyz\ooopener\models\Things;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class ConceptualObjectMarks extends Things
{
    /**
     * Creates a new ConceptualObjectMarks instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'position'      => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,

        'additionalType' => Thing::FILTER_INT,
        'language'       => Thing::FILTER_INT,
        'technique'      => Thing::FILTER_INT
    ];
}
