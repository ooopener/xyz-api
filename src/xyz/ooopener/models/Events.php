<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Events model.
 */
class Events extends Collections
{
    /**
     * Creates a new Events instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name' => Thing::FILTER_DEFAULT,
        'path' => Thing::FILTER_DEFAULT,

        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'endDate'     => Thing::FILTER_DEFAULT,
        'eventStatus' => Thing::FILTER_DEFAULT,
        'startDate'   => Thing::FILTER_DEFAULT,

        'about' => Thing::FILTER_INT,

        'capacity'          => Thing::FILTER_INT,
        'numAttendee'       => Thing::FILTER_INT,
        'remainingAttendee' => Thing::FILTER_INT,

        'alternateName'       => Thing::FILTER_DEFAULT,
        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,

        'audios'    => Thing::FILTER_DEFAULT,
        'photos'    => Thing::FILTER_DEFAULT,
        'videos'    => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT
    ];

    /**
     * The default 'countFrom' methods options.
     */
    const ARGUMENTS_COUNT_FROM_DEFAULT =
    [
        'active'     => NULL,
        'binds'      => [] ,
        'conditions' => [] ,
        'facets'     => NULL,
        'from'       => NULL,
        'interval'   => 5,
        'search'     => NULL,
    ] ;

    /**
     * Returns the number of users.
     * @param array $init
     * @return integer The number of users.
     * @throws Exception
     */
    public function countFrom( array $init = [] ) :int
    {
        [
            'active'     => $active,
            'binds'      => $binds,
            'conditions' => $conditions,
            'facets'     => $facets,
            'from'       => $from,
            'interval'   => $interval,
            'search'     => $search
        ]
        = array_merge( self::ARGUMENTS_COUNT_FROM_DEFAULT , $init ) ;

        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        // -------- FILTER

        $this->prepareFrom( $from , $interval , $query , $conditions , $binds ) ;
        $this->prepareSearch( $search , $conditions , $binds ) ;
        $this->prepareActive( $active , $conditions ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- RETURN

        $query .= ' RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        $count = $this->arangodb->getFirstResult() ;

        return $count > 0 ? $count : 0 ;
    }

    /**
     * The default 'from' method options.
     */
    const ARGUMENTS_FROM_DEFAULT =
    [
        'active'      => TRUE,
        'binds'       => [],
        'conditions'  => [],
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'fields'      => '*' ,
        'from'        => 'now',
        'interval'    => 5,
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'queryFields' => NULL ,
        'orders'      => NULL ,
        'search'      => NULL,
        'sort'        => NULL,
    ] ;

    /**
     * Returns all elements from a specific date.
     * @param array $init
     * @return array all elements from a specific date.
     * @throws Exception
     */
    public function from( $init = [] ):array
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'facets'      => $facets,
            'fetchStyle'  => $fetchStyle,
            'fields'      => $fields ,
            'from'        => $from,
            'interval'    => $interval,
            'lang'        => $lang,
            'limit'       => $limit,
            'offset'      => $offset,
            'queryFields' => $queryFields ,
            'search'      => $search,
            'sort'        => $sort,
        ]
        = array_merge( self::ARGUMENTS_FROM_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table ; // INDEX

        $this->prepareFrom ( $from , $interval , $query , $conditions , $binds ) ;

        $this->prepareSearch ( $search , $conditions , $binds ) ;
        $this->prepareActive ( $active , $conditions ) ;
        $this->prepareFacets ( $facets , $conditions , $binds ) ;

        $this->prepareConditions( $query , $conditions ); // FILTER

        if( !$this->prepareOrders( $sort , $query ) ) // SORT
        {
             $query .= ' SORT duration, doc.startDate, endDate ASC ' ;
        }

        $this->prepareLimit( $query , $binds , $limit , $offset ) ;

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    protected function prepareFrom( $from , $interval , ?string &$query , ?array &$conditions , ?array &$binds )
    {
        if( !is_string( $query ) )
        {
            $query = '' ;
        }

        $query .= ' LET endDate = ( IS_DATESTRING( doc.endDate ) ? doc.endDate : DATE_ADD( DATE_NOW() , 10 , "y" ) ) ' ;
        $query .= ' LET duration = ( DATE_DIFF( doc.startDate , endDate , "d" ) + 1 ) ' ;
        $query .= ' LET interval = ( DATE_ADD( @start , @interval , "d" ) ) ' ;

        $binds['start'] = $from ;

        $binds['interval']   = $interval ;

        $conditions[] = ' ( doc.startDate <= endDate ) '
                        . 'AND ( doc.startDate <= interval ) '
                        . 'AND ( @start <= endDate ) ' ;

    }
}


