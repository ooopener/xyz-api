<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

class MedicalAnalysis extends Collections
{
    /**
     * Creates a new MedicalAnalysis instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct($container, $table, $init);
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'           => Thing::FILTER_DEFAULT,
        'path'           => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT,
        'mandatory'      => Thing::FILTER_INT

    ];
}
