<?php

namespace xyz\ooopener\models;

use Exception;

use xyz\ooopener\things\Thing;
use Psr\Container\ContainerInterface;

use Closure;

/**
 * This class is the generic Edge class.
 */
class Edges extends Model
{
    /**
     * Creates a new Edge instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_EDGES_DEFAULT =
    [
        'active'      => TRUE,
        'binds'       => [],
        'conditions'  => [],
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'orders'      => NULL ,
        'skin'        => NULL,
        'skinFrom'    => NULL,
        'sort'        => NULL
    ] ;

    public function allEdges( array $init = [] ) :array
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'lang'        => $lang,
            'limit'       => $limit,
            'offset'      => $offset,
            'orders'      => $orders,
            'skin'        => $skin,
            'skinFrom'    => $skinFrom,
            'sort'        => $sort
        ]
        = array_merge( self::ARGUMENTS_ALL_EDGES_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->to[ Model::NAME ] ;

        $this->prepareActive( $active , $conditions ) ;

        $this->prepareConditions ( $query , $conditions               ) ; // FILTER
        $this->prepareOrders     ( $sort , $query                     ) ; // SORT
        $this->prepareLimit      ( $query , $binds , $limit , $offset ) ; // LIMIT / OFFSET

        $queryFrom  = '' ;
        $returnFrom = '' ;

        // query from
        $fieldsFrom = $this->container
                           ->get( $this->from[ Model::CONTROLLER ] )
                           ->getFields( $skinFrom ) ;

        if( isset($active) )
        {
            $queryFrom .= ' FILTER doc_from.active == ' . ((bool) $active ? '1' : '0') ;
        }

        $this->prepareOrders( $sort , $queryFrom , 'doc_from' ) ;

        if( array_key_exists( Model::MODEL , $this->from ) )
        {
            $queryFrom .= $this->container
                               ->get( $this->from[ Model::MODEL ] )
                               ->getFields( NULL , $fieldsFrom , $lang , 'from' ) ;
        }
        else
        {
            if( $fieldsFrom == '*' )
            {
                $returnFrom = ' RETURN doc_from' ;
            }
            else
            {
                $returnFrom = ' RETURN { ' . $this->getFilterFields( $fieldsFrom , 'from' , $lang ) . '}' ;
            }
        }

        $query .= ' LET from = '
                . '( '
                . 'FOR doc_from IN INBOUND doc ' . $this->table . ' '
                . $queryFrom
                . $returnFrom
                . ' ) ';

        $fieldsTo = $this->container
                         ->get( $this->to[ Model::CONTROLLER ] )
                         ->getFields() ;

        if( $fieldsTo == '*' )
        {
            $returnTo = ' doc' ;
        }
        else
        {
            $returnTo = ' { ' . $this->getFilterFields( $fieldsTo , 'doc' , $lang ) . '}' ;
        }

        $query .= ' RETURN { from : from , to : ' . $returnTo . ' }';

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * Insert a new edge entry in the edge model.
     * @param $from
     * @param $to
     * @param ?array $init the optional definitions to inject in the edge document.
     * @return object|null
     * @throws \ArangoDBClient\Exception
     */
    public function insertEdge( $from , $to , ?array $init = NULL ) :?object
    {
        $binds  =
        [
            'from' => $from ,
            'to'   => $to
        ];

        $values =
        [
            '_from:@from' ,
            '_to:@to' ,
            'created: DATE_ISO8601( DATE_NOW() )'
        ] ;

        if( is_array( $init ) && is_array( $this->fillable ) )
        {
            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }
        }

        $values  = implode( ',' , $values );

        $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';


        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_from'
    ] ;

    public function delete( $value , $init = [] ) :?object
    {
        [
            'conditions' => $conditions ,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value ';

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function deleteEdge( $from , $to ) :?object
    {
        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' FILTER doc._from == @from && doc._to == @to ' ;

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "from" => $from ,
                "to"   => $to
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function deleteEdgeBoth( $id , $firstOnly = FALSE )
    {
        $query  = 'FOR doc IN ' . $this->table . ' '
                . 'FILTER doc._from == @id || doc._to == @id '
                . 'REMOVE { _key: doc._key } IN ' . $this->table . ' '
                . 'RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'id' => $id ]
        ]);

        $this->arangodb->execute() ;

        return $firstOnly
             ? $this->arangodb->getObject()
             : $this->arangodb->getResult() ;
    }

    public function deleteEdgeFrom( $from , $firstOnly = FALSE )
    {
        $query  = 'FOR doc IN ' . $this->table . ' '
                . 'FILTER doc._from == @from '
                . 'REMOVE { _key: doc._key } IN ' . $this->table . ' '
                . 'RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'from' => $from ]
        ]);

        $this->arangodb->execute() ;

        return $firstOnly
             ? $this->arangodb->getObject()
             : $this->arangodb->getResult() ;
    }

    public function deleteEdgeTo( $to , $firstOnly = FALSE )
    {
        $query  = 'FOR doc IN ' . $this->table . ' '
                . 'FILTER doc._to == @to '
                . 'REMOVE { _key: doc._key } IN ' . $this->table . ' '
                . 'RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'to'  => $to ]
        ]);

        $this->arangodb->execute() ;

        return $firstOnly
             ? $this->arangodb->getObject()
             : $this->arangodb->getResult() ;
    }

    /**
     * Indicates if the edge exist with the specific from and to vertex identifier.
     * @param $from
     * @param $to
     * @return bool
     * @throws \ArangoDBClient\Exception
     */
    public function existEdge( $from , $to ) :bool
    {
        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' '
               . 'FILTER doc._from == @from && doc._to == @to RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'from' => $from , 'to' => $to ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    public function existEdgeFrom( $from ) :bool
    {
        $query = 'RETURN COUNT( FOR doc IN '
               . $this->table . ' '
               . 'FILTER doc._from == @from RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'from' => $from ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    public function existEdgeTo( $to ) :bool
    {
        $query = 'RETURN COUNT( FOR doc IN '
               . $this->table . ' '
               . 'FILTER doc._to == @to RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'to' => $to ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_EDGE_DEFAULT =
    [
        'binds' => [] ,
        'lang'  => NULL ,
        'skin'  => NULL ,
        'sort'  => NULL
    ] ;

    /**
     * Returns the specific edge definition.
     * @param ?string $from
     * @param ?string $to
     * @param array $init
     * @param ?array $mock
     * @return ?object
     * @throws Exception
     */
    public function getEdge( ?string $from = NULL , ?string $to = NULL , $init = [] , $mock = NULL ) :?object
    {
        [
            'binds' => $binds ,
            'lang'  => $lang ,
            'skin'  => $skin ,
            'sort'  => $sort
        ]
        = array_merge( self::ARGUMENTS_GET_EDGE_DEFAULT , is_array($init) ? $init : [] ) ;

        if( $from )
        {
            $binds['from'] = $from ;
        }

        if( $to )
        {
            $binds['to'] = $to ;
        }

        $query = NULL ;

        try
        {
            if( $to && !$from )
            {
                $query = $this->getEdgeQueryTo( $skin , $lang , $sort , $mock ) ;
            }
            elseif( $from && !$to )
            {
                $query = $this->getEdgeQueryFrom( $skin , $lang , $sort , $mock ) ;
            }
        }
        catch( Exception $e)
        {
            throw $e ;
        }

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Returns the specific edge definition.
     * @param  string $to
     * @param  ?array $init
     * @param  ?array $mock
     * @param  bool   $firstOnly
     * @return ?object
     * @throws Exception
     */
    public function getEdgeTo( string $to , ?array $init = [] , $mock = NULL , bool $firstOnly = FALSE ) :?object
    {
        [
            'binds' => $binds ,
            'lang'  => $lang ,
            'skin'  => $skin ,
            'sort'  => $sort
        ]
        = array_merge( self::ARGUMENTS_GET_EDGE_DEFAULT , is_array($init) ? $init : [] ) ;

        $this->arangodb->prepare
        ([
            'query'    => $this->getEdgeQueryTo( $skin , $lang , $sort , $mock ) ,
            'bindVars' => [ 'to' => $to ]
        ]);

        $this->arangodb->execute() ;

        return $firstOnly
             ? $this->arangodb->getResult()
             : $this->arangodb->getObject() ;
    }

    protected function getEdgeQueryFrom( $skin , $lang , $sort , ?array $mock = NULL ) :string
    {
        $query = 'FOR doc '
               . 'IN ' . $this->from[ Model::NAME ] . ' '
               . 'FILTER doc._key == @from ' ;

        $to = is_array($mock) ? $mock : $this->to ;

        if( is_array( $to ) )
        {
            $controller = $this->container->get( $to[ Model::CONTROLLER ] ) ;

            $fields = $controller->getFields( $skin ) ;
            $edges  = $controller->model->edges ;
            $joins  = $controller->model->joins ;

            $resultQuery = '' ;
            $sortQuery   = '' ;

            foreach( $fields as $key => $field )
            {
                switch( $field[ Model::FILTER ] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                    {
                        $current = NULL ;
                        foreach( $edges as $edge )
                        {
                            if( is_array( $edge ) )
                            {
                                if( $edge[ Model::NAME ] == $key )
                                {
                                    $current = $edge ;
                                    break ;
                                }
                            }
                            else
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with edge, it's not an array with the type : " . gettype( $edge ) ) ;
                            }
                        }
                        $current[Model::UNIQUE] = $field[Model::UNIQUE] ;
                        $resultQuery .= $this->getEdges( [ $current ] , 'doc_to' , $lang ) ;
                        break ;
                    }

                    case Thing::FILTER_EDGE_COUNT :
                    {
                        $current = NULL ;
                        foreach( $edges as $edge )
                        {
                            if( is_array( $edge ) )
                            {
                                if( 'num' . ucfirst( $edge[Model::NAME] ) == $key )
                                {
                                    $current = $edge;
                                    break;
                                }
                            }
                            else
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with edge, it's not an array with the type : " . gettype( $edge ) ) ;
                            }
                        }
                        $resultQuery .= $this->getEdgeCount( $field[Model::UNIQUE] , $current , 'doc_to' ) ;
                        break ;
                    }

                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                    {
                        $current = NULL ;
                        foreach( $joins as $join )
                        {
                            if( $join instanceof Closure )
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with join, it's not an object with the type : " . gettype( $join ) ) ;
                            }
                            else
                            {
                                if( $join[ Model::NAME ] == $key )
                                {
                                    $current = $join;
                                    break;
                                }
                            }
                        }
                        $current[ Model::UNIQUE ] = $field[ Model::UNIQUE ] ;
                        $resultQuery .= $this->getJoin( [ $current ] , 'doc_to' , $lang ) ;
                        break ;
                    }
                }
            }

            if( $fields == '*' )
            {
                $resultQuery .= ' RETURN doc_to ' ;
            }
            else
            {
                $resultQuery .= ' RETURN { ' . $this->getFilterFields( $fields , 'to' , $lang ) . '} ' ;
            }

            if( $sort )
            {
                $sortQuery = ' SORT doc_from.' . $sort . ' ' ;
            }

            $query .= ' LET to = '
                    . '( '
                    . 'FOR doc_to IN OUTBOUND doc '
                    . $this->table
                    . $sortQuery
                    . $resultQuery
                    . ') '
                    . 'RETURN { edge : to }' ; // RETURN
        }
        else
        {
            $query .= ' RETURN doc' ;
        }

        return $query ;
    }

    /**
     * @param $skin
     * @param $lang
     * @param $sort
     * @param ?array $mock
     * @return string
     * @throws Exception
     */
    protected function getEdgeQueryTo( $skin , $lang , $sort , ?array $mock = NULL ) :string
    {
        $to = is_array($mock) ? $mock : $this->to ;

        if( is_array( $this->from ) )
        {
            $query = 'FOR doc '
                   . 'IN ' . $to[ Model::NAME ] . ' '
                   . 'FILTER doc._key == @to' ;

            $resultQuery = '' ;
            $sortQuery   = '' ;

            $controller = $this->container->get( $this->from[ Model::CONTROLLER ] ) ;

            $fields = $controller->getFields( $skin ) ;
            $joins  = $controller->model->joins ;
            $edges  = $controller->model->edges ;

            foreach( $fields as $key => $field )
            {
                switch( $field[ Model::FILTER ] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                    {
                        $current = NULL ;

                        foreach( $edges as $edge )
                        {
                            if( is_array( $edge ) )
                            {
                                if( $edge[ Model::NAME ] == $key )
                                {
                                    $current = $edge ;
                                    break ;
                                }
                            }
                            else
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with edge, it's not an array with the type : " . gettype( $edge ) ) ;
                            }
                        }
                        $current[ Model::UNIQUE ] = $field[ Model::UNIQUE ] ;
                        $resultQuery .= $this->getEdges( [ $current ] , 'doc_from' , $lang ) ;
                        break ;
                    }

                    case Thing::FILTER_EDGE_COUNT :
                    {
                        $current = NULL ;
                        foreach( $edges as $edge )
                        {
                            if( is_array( $edge ) )
                            {
                                if( 'num' . ucfirst( $edge[Model::NAME] ) == $key )
                                {
                                    $current = $edge;
                                    break;
                                }
                            }
                            else
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with edge, it's not an array with the type : " . gettype( $edge ) ) ;
                            }
                        }
                        $resultQuery .= $this->getEdgeCount( $field[ Model::UNIQUE ] , $current , 'doc_from' ) ;
                        break ;
                    }
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                    {
                        $current = NULL ;
                        foreach( $joins as $join )
                        {
                            if( $join instanceof Closure )
                            {
                                throw new Exception( $this . ' ' . __FUNCTION__ . " failed, there is an error with join, it's not an object with the type : " . gettype( $join ) ) ;
                            }
                            else
                            {
                                if( $join[ Model::NAME ] == $key )
                                {
                                    $current = $join;
                                    break;
                                }
                            }
                        }
                        $current[ Model::UNIQUE ] = $field[ Model::UNIQUE ] ;
                        $resultQuery .= $this->getJoin( [ $current ] , 'doc_from' , $lang ) ;
                        break ;
                    }
                }
            }

            if( $fields == '*' )
            {
                $resultQuery .= ' RETURN doc_from ' ;
            }
            else
            {
                $resultQuery .= ' RETURN { ' . $this->getFilterFields( $fields , 'from' , $lang ) . '} ' ;
            }

            if( $sort )
            {
                $sortQuery = ' SORT doc_from.' . $sort . ' ' ;
            }

            $query .= ' LET from = '
                    . '( '
                    . 'FOR doc_from IN INBOUND doc '
                    . $this->table
                    . $sortQuery
                    . $resultQuery
                    . ') '
                    . 'RETURN { edge : from } ' ;
        }
        else
        {
            $to = $to[ Model::NAME ] ;

            $query = <<<EOD
                FOR vertex, edge, path IN $to
                FILTER doc._key == @to 
                RETURN doc
            EOD;
        }

        return $query ;
    }
}
