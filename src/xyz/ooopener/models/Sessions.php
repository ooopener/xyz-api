<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Session;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * This service to manage the sessions table.
 */
class Sessions extends Collections
{
    /**
     * Creates a new Sessions instance.
     *
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'type'      => Thing::FILTER_DEFAULT,
        'user'      => Thing::FILTER_DEFAULT,
        'client_id' => Thing::FILTER_DEFAULT,
        'app'       => Thing::FILTER_DEFAULT,
        'token_id'  => Thing::FILTER_DEFAULT,
        'refresh'   => Thing::FILTER_DEFAULT,
        'ip'        => Thing::FILTER_DEFAULT,
        'path'      => Thing::FILTER_DEFAULT,
        'agent'     => Thing::FILTER_DEFAULT,
        'expired'   => Thing::FILTER_DEFAULT,
        'logout'    => Thing::FILTER_DEFAULT
    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public ?array $sortable =
    [
        'id'        => '_key',
        'type'      => 'type' ,
        'user'      => 'user' ,
        'client_id' => 'client_id' ,
        'created'   => 'created'
    ];

    ///////////////////////////

    /**
     * check if token exist and not revoked
     *
     * @param $token_id string
     * @param $agent string
     *
     * @return bool
     * @throws Exception
     */
    public function check( string $token_id , string $agent ) : bool
    {
        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' '
               . ' FILTER doc.token_id == @token_id && doc.agent == @agent && doc.revoked == 0 ' .
               'RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "agent"    => $agent ,
                "token_id" => $token_id
            ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    /**
     * check if refresh token exist and not revoked
     *
     * @param string $client_id
     * @param string $refresh
     *
     * @return object
     * @throws Exception
     */
    public function checkRefresh( string $client_id , string $refresh ) : object
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.client_id == @client_id && doc.refresh == @refresh && doc.revoked == 0 RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "client_id" => $client_id ,
                "refresh"   => $refresh
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Returns the specific token representation by property.
     *
     * @param string $name
     * @param mixed $value
     * @param string|NULL $filter
     *
     * @return object The specific token representation by property.
     * @throws Exception
     */
    public function getByProperty( string $name , $value , ?string $filter = NULL ) : object
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.' . $name . ' == @name RETURN doc' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "name" => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Insert a new item into the table.
     * @param array|Session $init
     * @return NULL|integer
     * @throws Exception
     */
    public function insert( $init )
    {
        if( $init && ( $init instanceof Session || is_array( $init ) ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( ( $init instanceof Session ) && property_exists( $init , $property ) )
                {
                    $binds[ $property ] = $init->{$property} ;
                    $values[] = $property . ':@' . $property ;
                }
                else if( ( is_array( $init ) && array_key_exists( $property , $init ) ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'path: "sessions"' ;
            $values[] = 'revoked: 0' ;
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table ;

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->arangodb->execute() ;

            return TRUE ;
        }

        return null ;
    }

    /**
     * Update token
     *
     * @param string $refresh
     * @param Session $token
     *
     * @return bool
     * @throws Exception
     */
    public function updateToken( string $refresh , Session $token ) : bool
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.refresh == @refresh ' .
            ' UPDATE doc WITH { token_id: @token_id , expired: @expired ,modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'token_id' => $token->token_id ,
                'expired'  => $token->expired ,
                'refresh'  => $refresh
            ]
        ]);

        $this->arangodb->execute() ;

        return TRUE ;
    }

    /**
     * @param $user
     * @param $client
     * @param $ip
     * @param $agent
     * @return mixed
     * @throws Exception
     */
    public function logoutAccessToken( $user , $client , $ip , $agent )
    {
        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.type == @type && doc.user == @user && doc.client_id == @client_id && doc.ip == @ip && doc.agent == @agent '
               . '&& doc.revoked == 0 && doc.logout == null && doc.expired > DATE_ISO8601( DATE_NOW() ) '
               . 'UPDATE doc '
               . 'WITH { revoked: @revoked , logout: DATE_ISO8601( DATE_NOW() ) , modified: DATE_ISO8601( DATE_NOW() ) } '
               . 'IN ' . $this->table ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "revoked"    => 1 ,
                "type"       => 'access_token' ,
                "user"       => $user ,
                "client_id"  => $client ,
                "ip"         => $ip ,
                "agent"      => $agent
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getResult() ;
    }

    /**
     * Logout user
     * @param $user string
     * @param $ip string
     * @param $agent string
     * @return mixed
     * @throws Exception
     */
    public function logoutIDToken( string $user , string $ip , string $agent )
    {
        $query = 'FOR doc IN ' . $this->table ;

        // filter
        $query .= ' FILTER doc.type == @type && doc.user == @user && doc.ip == @ip && doc.agent == @agent' .
            ' && doc.revoked == 0 && doc.logout == null && doc.expired > DATE_ISO8601( DATE_NOW() )' ;

        // update
        $query .= ' UPDATE doc WITH { revoked: @revoked , logout: DATE_ISO8601( DATE_NOW() ) , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "revoked"    => 1 ,
                "type"       => "id_token" ,
                "user"       => $user ,
                "ip"         => $ip ,
                "agent"      => $agent
            ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getResult() ;
    }

    /**
     * Revoke token
     * @param $token_id
     * @return bool
     * @throws Exception
     */
    public function revoke( $token_id ) : bool
    {
        $query = 'FOR doc IN '  . $this->table . ' '
               . 'FILTER doc.token_id == @token_id && doc.revoked == 0 '
               . 'UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' '
               . 'RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'token_id' => $token_id ,
                'revoked'  => 1
            ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getObject() ;
    }

    /**
     * Revoke token
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function revokeID( $id ) : bool
    {
        $query = 'FOR doc IN '  . $this->table . ' FILTER doc._key == @id' .
            ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'id'      => $id ,
                'revoked' => 1
            ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_REVOKE_DEFAULT =
    [
        'key' => '_key'
    ] ;

    public function revokeIDAll( $items , $init = [] ) : bool
    {
        [ 'key' => $key ] = array_merge( self::ARGUMENTS_REVOKE_DEFAULT , $init ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        //
        $query .= ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $binds['revoked'] = 1 ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return TRUE ;
    }


    /**
     * Revoke all sessions from the users list
     * @param array $list
     * @throws Exception
     */
    public function revokeAllByIds( array $list ) : void
    {
        foreach( $list as $user )
        {
            $this->revokeAll( $user ) ;
        }
    }

    /**
     * Revoke all sessions for the user
     * @param string $user
     * @return bool
     * @throws Exception
     */
    public function revokeAll( string $user ) : bool
    {
        $query = 'FOR doc IN '  . $this->table .
            ' FILTER doc.user == @user && doc.revoked == 0 ' .
            ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table .
            ' RETURN NEW' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'user'     => $user ,
                'revoked'  => 1
            ]
        ]);

        $this->arangodb->execute() ;

        return TRUE ;
    }
}


