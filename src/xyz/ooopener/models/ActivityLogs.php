<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class ActivityLogs extends Collections
{
    /**
     * ActivityLogs constructor.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'          => Thing::FILTER_INT,
        'user'        => Thing::FILTER_DEFAULT,
        'method'      => Thing::FILTER_DEFAULT,
        'resource'    => Thing::FILTER_DEFAULT,
        'description' => Thing::FILTER_DEFAULT,
        'ip'          => Thing::FILTER_DEFAULT,
        'agent'       => Thing::FILTER_DEFAULT
    ];
}
