<?php

namespace xyz\ooopener\models ;

use xyz\ooopener\things\Thing ;

use Psr\Container\ContainerInterface ;

/**
 * The Websites model.
 */
class Websites extends Things
{
    /**
     * Creates a new Websites instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'name'           => Thing::FILTER_DEFAULT,
        'href'           => Thing::FILTER_DEFAULT,
        'order'          => Thing::FILTER_INT
    ];
}


