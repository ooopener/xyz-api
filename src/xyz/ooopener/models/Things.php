<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Things model.
 */
class Things extends Model
{
    /**
     * Creates a new Things instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
//        [
//            'facetable'  => $facetable,
//            'searchable' => $searchable,
//            'sortable'   => $sortable
//        ]
//        = array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] ) ; // FIXME remove this lines
        parent::__construct( $container , $table , $init );
    }

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'facetable'  => NULL,
        'searchable' => NULL,
        'sortable'   =>
        [
            'id'       => 'CAST(id as SIGNED INTEGER)',
            'name'     => 'UPPER(name)',
            'created'  => 'created',
            'modified' => 'modified'
        ]
    ] ;

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'    => Thing::FILTER_INT,
        'owner' => Thing::FILTER_INT,
        'name'  => Thing::FILTER_DEFAULT
    ];



    ///////////////////////////

    /**
     * The owner identifier key.
     */
    public string $ownerKey = 'owner' ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'     => TRUE,
        'conditions' => [],
        'facets'     => NULL,
        'limit'      => 0,
        'offset'     => 0,
        'sort'       => NULL,
        'fields'     => '*' ,
        'orders'     => NULL
    ] ;

    /**
     * Returns the collection of all the things.
     * @param mixed $owner an optional owner identifier.
     * @param array $init
     * @return array the collection of all the things.
     * @throws Exception
     */
    public function all( $owner = NULL , array $init = [] ):array
    {
        $init['conditions'] = [ 'doc.owner == ' . $owner ] ;
        return parent::all( $init ) ;
    }

    /**
     * Delete the specific item
     * @param mixed $value
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function delete( $value , array $init = [] )
    {
        [ 'key' => $key ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value '
               . 'REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Indicates if the specific owner exist.
     * @param mixed $id
     * @return bool
     * @throws Exception
     */
    public function existOwner( $id )
    {
        return $this->exist( $id , $this->ownerKey ) ;
    }

    /**
     * Update the specific item.
     * @param mixed $item
     * @param string $keyValue
     * @param array $init
     *
     * @return object
     *
     * @throws Exception
     */
    public function update( $item , string $keyValue , array $init = [] ) : object
    {
        [
            'active' => $active,
            'key'    => $key
        ]
        = array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if( is_array( $item ) && array_key_exists( $property , $item ) )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key '
               . 'UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' '
               . 'RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }
}


