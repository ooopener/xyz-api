<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The OpeningHoursSpecifications model.
 */
class OpeningHoursSpecifications extends Things
{
    /**
     * Creates a new Things instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'dayOfWeek'     => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'opens'         => Thing::FILTER_DEFAULT,
        'closes'        => Thing::FILTER_DEFAULT,
        'validFrom'     => Thing::FILTER_DEFAULT,
        'validThrough'  => Thing::FILTER_DEFAULT
    ];
}


