<?php

namespace xyz\ooopener\models\organizations ;

use xyz\ooopener\models\Things;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface;

class OrganizationNumbers extends Things
{
    /**
     * Creates a new OrganizationNumbers instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container, ?string $table, array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'name'           => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT,
        'value'          => Thing::FILTER_DEFAULT
    ];
}
