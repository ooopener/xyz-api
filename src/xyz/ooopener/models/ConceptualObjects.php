<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The ConceptualObjects model.
 */
class ConceptualObjects extends Collections
{
    /**
     * Creates a new ConceptualObjects instance.
     * @param ContainerInterface $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'path'          => Thing::FILTER_DEFAULT,
        'active'        => Thing::FILTER_INT,
        'withStatus'    => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,
        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];
}


