<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;
use Psr\Container\ContainerInterface;

class Technicians extends Collections
{
    /**
     * Creates a new Technicians instance
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'path'       => Thing::FILTER_DEFAULT,
        'identifier' => Thing::FILTER_DEFAULT,
        'person'     => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'isBasedOn'  => Thing::FILTER_DEFAULT
    ];
}
