<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception as ArangoException;
use Closure;
use xyz\ooopener\helpers\Arango;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

use Exception ;
use Psr\Log\LoggerInterface;

/**
 * This class is the generic Model class.
 */
class Model
{
    /**
     * Creates a new Model instance.
     * @param ContainerInterface $container
     * @param ?string $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        [
            'edges'      => $edges,
            'facetable'  => $facetable,
            'fillable'   => $fillable,
            'from'       => $from,
            'joins'      => $joins,
            'searchable' => $searchable,
            'sortable'   => $sortable,
            'to'         => $to
        ]
        = array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , $init ) ;

        $this->container = $container  ;

        $this->arangodb = $container->get( Arango::class ) ;
        $this->logger   = $container->get( LoggerInterface::class ) ;

        if( $table )
        {
            $this->table = $table ;
        }

        $this->edges      = $edges  ;
        $this->facetable  = $facetable  ;
        $this->from       = $from  ;
        $this->joins      = $joins  ;
        $this->searchable = $searchable ;
        $this->sortable   = $sortable   ;
        $this->to         = $to   ;

        if( is_array( $fillable ) )
        {
            $this->fillable = $fillable ; // overrides only if exist
        }
    }

    const ARRAY          = 'array' ;
    const CONTROLLER     = 'controller' ;
    const DIRECTION      = 'direction' ;
    const EDGECONTROLLER = 'edgeController' ;
    const EDGE           = 'edge' ;
    const EDGES          = 'edges' ;
    const FACETABLE      = 'facetable' ;
    const FIELDS         = 'fields' ;
    const FILLABLE       = 'fillable' ;
    const FILTER         = 'filter' ;
    const FROM           = 'from' ;
    const JOINS          = 'joins' ;
    const KEY            = 'key' ;
    const MODEL          = 'model' ;
    const MULTIPLE       = 'multiple' ;
    const NAME           = 'name' ;
    const POSITION       = 'position' ;
    const REVERSE        = 'reverse' ;
    const SEARCHABLE     = 'searchable' ;
    const SKIN           = 'skin' ;
    const SORTABLE       = 'sortable' ;
    const TABLE          = 'table' ;
    const TO             = 'to' ;
    const UNIQUE         = 'unique' ;

    const FACET_ARRAY_COMPLEX     = 'facet_array_complex' ;
    const FACET_EDGE              = 'facet_edge' ;
    const FACET_EDGE_COMPLEX      = 'facet_edge_complex' ;
    const FACET_FIELD             = 'facet_field' ;
    const FACET_LIST              = 'facet_list' ;
    const FACET_LIST_FIELD        = 'facet_list_field' ;
    const FACET_LIST_FIELD_SORTED = 'facet_list_field_sorted' ;
    const FACET_PROPERTY          = 'facet_property' ;
    const FACET_THESAURUS         = 'facet_thesaurus' ;
    const FACET_TYPE              = 'facet_type' ;

    /**
     * The default 'constructor' default arguments.
     */
    protected const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'edges'      => NULL,
        'facetable'  => NULL,
        'fillable'   => NULL,
        'from'       => NULL,
        'joins'      => NULL,
        'searchable' => NULL,
        'sortable'   => NULL,
        'to'         => NULL
    ] ;

    /**
     * The ArangoDB database reference.
     */
    protected Arango $arangodb ;

    /**
     * The container reference.
     */
    protected ContainerInterface $container ;

    /**
     * The edges settings.
     */
    public ?array $edges ;

    /**
     * The facet settings.
     */
    public ?array $facetable ;

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'   => Thing::FILTER_INT,
        'name' => Thing::FILTER_DEFAULT
    ];

    /**
     * The from settings.
     */
    public ?array $from ;

    /**
     * The joins settings.
     */
    public ?array $joins ;

    /**
     * The Logger reference of the model.
     * @var LoggerInterface|mixed
     */
    protected LoggerInterface $logger ;

    /**
     * The searchable fields.
     */
    public ?array $searchable ;

    /**
     * The map of all the sortable field of this model.
     */
    public ?array $sortable ;

    /**
     * The default table name.
     * @var string|null
     */
    public ?string $table;

    /**
     * The to settings.
     */
    public ?array $to ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'binds'       => [] ,
        'conditions'  => [] ,
        'fields'      => '*' ,
        'lang'        => NULL,
        'queryFields' => NULL
    ] ;

    /**
     * Return all elements.
     * @param array $init
     * @return array All the elements
     * @throws Exception
     */
    public function all( array $init = [] ):array
    {
        [
            'binds'       => $binds,
            'conditions'  => $conditions,
            'fields'      => $fields,
            'lang'        => $lang,
            'queryFields' => $queryFields
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;

        $this->prepareConditions( $query , $conditions ); // FILTER

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active'     => NULL,
        'conditions' => [],
        'facets'     => NULL,
        'search'     => NULL
    ] ;

    /**
     * Check if collection exists
     * @param string $name The name of the collection
     * @return bool
     */
    public function collectionExists( string $name ) : bool
    {
        return $name !== NULL && $this->arangodb->collectionExists( $name ) ;
    }

    /**
     * Creates a new collection if not exist.
     * @param string $name The name of the new collection
     * @return bool Returns TRUE if the new collection is created.
     */
    public function collectionCreate( string $name ) :bool
    {
        return $this->arangodb->collectionCreate( $name ) ;
    }

    /**
     * Drops a collection if exist.
     * @param string $name The name of the new collection
     * @return bool Returns TRUE if the new collection is dropped.
     */
    public function collectionDrop( string $name ) :bool
    {
        return $this->arangodb->collectionDrop( $name ) ;
    }

    /**
     * Renames a collection if exist.
     * @param string $oldName The old name of the collection
     * @param string $name The new name of the collection
     * @return bool Returns TRUE if the collection is renamed.
     * @throws Exception
     */
    public function collectionRename( string $oldName , string $name ) :bool
    {
        return $this->arangodb->collectionRename( $oldName , $name ) ;
    }

    /**
     * Returns the number of elements registered in the specific table.
     * @param array $init
     * @return int The number of elements registered in the specific table.
     * @throws Exception
     */
    public function count( array $init = [] ):int
    {
        [
            'active'     => $active,
            'conditions' => $conditions,
            'facets'     => $facets,
            'search'     => $search
        ]
        = array_merge( self::ARGUMENTS_COUNT_DEFAULT , $init ) ;

        $binds = [] ;
        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        $this->prepareSearch( $search , $conditions , $binds ) ;
        $this->prepareActive( $active , $conditions ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        $this->prepareConditions( $query , $conditions ); // FILTER

        $query .= ' RETURN 1 )' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getFirstResult() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT = [ 'key' => '_key' ] ;

    /**
     * Delete the specific item
     * @param string|integer $value
     * @param array $init
     * @return mixed
     * @throws Exception
     */
    public function delete( $value , array $init = [] )
    {
        [ 'key' => $key ] = array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value '
               . 'REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    public function deleteAllArrayPropertyValues( string $name , ?array $values ) :?array
    {
        if( is_array($values) && count($values) > 0 )
        {
            //$values = implode( ',' , $values ) ;

            $query = 'FOR doc IN ' . $this->table . ' '
                   . 'FILTER IS_ARRAY( doc.' . $name . ' ) '
                   . 'LET value = REMOVE_VALUES( doc.' . $name . ' , @values ) '
                   . 'UPDATE doc WITH { '. $name . ' : value } IN ' . $this->table . ' '
                   . 'RETURN NEW' ;

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => [ 'values' => $values ]
            ]);

            $this->arangodb->execute() ;

            return $this->arangodb->getAll() ;
        }

        return NULL ;
    }

    /**
     * The default 'exist' method options.
     */
    const ARGUMENTS_EXIST_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    /**
     * Indicates if the item exist.
     * @param mixed $value The value to find in the model.
     * @param array $init The optional setting definition.
     * @return bool True of the value exist in the model.
     * @throws ArangoException
     */
    public function exist( $value , $init = []  ):bool
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_EXIST_DEFAULT , $init ) ;

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        $query .= ' RETURN 1 )';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getFirstResult() ;
    }

    /**
     * Indicates if the list of items exist (by id or the custom field parameter).
     *
     * @param array $items
     * @param array $init
     *
     * @return integer
     * @throws ArangoException
     */
    public function existAll( array $items , array $init = [] ):int
    {
        [
            'conditions' => $conditions,
            'key'        => $key
        ]
        = array_merge( self::ARGUMENTS_EXIST_DEFAULT , $init ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyI => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyI ;
            $binds['i' . $keyI] = $value ;
        }

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        $query .= ' RETURN 1 )';

        // $this->logger->info( $this . ' existAll binds: ' . json_encode($binds) ) ;
        // $this->logger->info( $this . ' existAll query: ' . $query ) ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getFirstResult() ;
    }

    /**
     * For a SELECT with a LIMIT clause, returns the number of rows that would be returned were there no LIMIT clause.
     * @return int
     */
    public function foundRows():int
    {
        return $this->arangodb->getFoundRows() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'fields'      => '*' ,
        'key'         => '_key',
        'lang'        => NULL,
        'queryFields' => NULL
    ] ;

    /**
     * Returns the specific item.
     * @param mixed $value
     * @param array $init
     * @return mixed the specific item.
     * @throws Exception
     */
    public function get( $value , array $init = [] )
    {
        [
            'fields'      => $fields ,
            'key'         => $key ,
            'lang'        => $lang,
            'queryFields' => $queryFields
        ]
        = array_merge( self::ARGUMENTS_GET_DEFAULT , $init ) ;

        $query = <<<EOD
        FOR doc IN $this->table 
        FILTER doc.$key == @value  
        EOD;

        // $this->logger->warning( $this . ' get(' . $value . ') QUERY_FIELDS::: ' . $queryFields ) ;

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * Returns Arango the database singleton reference.
     * @return Arango
     */
    public function getDatabase() :Arango
    {
        return $this->arangodb ;
    }

    /**
     * Return edges query
     * @param array $edges
     * @param string $docRef
     * @param ?string $lang
     * @return string
     * @throws Exception
     */
    public function getEdges( array $edges , string $docRef = 'doc' , ?string $lang = NULL ) : string
    {
        $edges_query = '' ;

        // $this->logger->debug( $this . ' getEdges ::: ' . json_encode( $edges , JSON_PRETTY_PRINT )  ) ;

        foreach( $edges as $edge )
        {
            // check controller exists
            if( !isset( $edge[ Model::CONTROLLER ] ) || !$this->container->has( $edge[ Model::CONTROLLER ] ) )
            {
                throw new Exception( "Controller doesn't exist on edge!" ) ;
            }

            $fields = $this->container
                           ->get( $edge[ Model::CONTROLLER ] )
                           ->getFields( $edge[ Model::SKIN ] ) ;

            $name = array_key_exists( Model::UNIQUE , $edge ) ? $edge[Model::UNIQUE] : $edge[Model::NAME] ;

            // $this->logger->info( $this . ' getEdges, name: ' . $name . ' controller:' . $edge[ Model::CONTROLLER ] );

            $return = '' ;

            $table = $this->container
                          ->get( $edge[ Model::EDGECONTROLLER ] )
                          ->edge
                          ->table ;

            $doc = $edge[ Model::NAME ] . '_' . $table ;

            foreach( $fields as $key => $field )
            {
                // $this->logger->warning( $this . ' getEdges >> ' . $key . ' -- ' . json_encode($field) );
                switch( $field[ Model::FILTER ] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                    {
                        if
                        (
                               array_key_exists( Model::EDGES , $edge )
                            && is_countable( $edge[Model::EDGES] )
                            && count( $edge[Model::EDGES] ) > 0
                        )
                        {
                            $currentEdge = null;
                            foreach( $edge[ Model::EDGES ] as $ed )
                            {
                                if( $ed[ Model::NAME ] == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $currentEdge[ Model::UNIQUE ] = $field[ Model::UNIQUE ];
                            $return .= self::getEdges( [ $currentEdge ] , 'doc_' . $doc , $lang);
                        }
                        break ;
                    }
                    case Thing::FILTER_EDGE_COUNT :
                    {
                        if( array_key_exists( Model::EDGES , $edge ) && is_countable( $edge[Model::EDGES] ) && count( $edge[Model::EDGES] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $edge[ Model::EDGES ] as $ed )
                            {
                                if( 'num' . ucfirst($ed[ Model::NAME ]) == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $return .= self::getEdgeCount( $field[ Model::UNIQUE ] , $currentEdge , 'doc_' . $doc);
                        }
                        break ;
                    }
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                    {
                        if(
                               array_key_exists( Model::JOINS , $edge )
                            && is_countable( $edge[Model::JOINS] )
                            && count( $edge[Model::JOINS] ) > 0 )
                        {
                            $current = null;
                            foreach( $edge[ Model::JOINS ] as $join )
                            {
                                // $this->logger->debug( $this . ' getEdges failed with the key:"' . $key . ', line:' . __LINE__ . ', ' . isset( $join[ Model::NAME ]) )  ;
                                if( isset( $join[ Model::NAME ] ) )
                                {
                                    if( $join[ Model::NAME ] == $key )
                                    {
                                        $current = $join ;
                                        break;
                                    }
                                }
                                else
                                {
                                    $this->logger->warning( $this . ' getEdges failed with the key:"' . $key . '" and the filter:"' . $field[ Model::FILTER ] . ' ", the join definition need a "name" property ' )  ;
                                }
                            }

                            if( $current != NULL )
                            {
                                $current[ Model::UNIQUE ] = $field[ Model::UNIQUE ];
                                $return .= self::getJoin([ $current ] , 'doc_' . $doc , $lang);
                            }
                            else
                            {
                                $this->logger->warning( $this . ' getEdges failed with the key:"' . $key . '" and the filter:"' . $field[ Model::FILTER ] . ' ", the join definition don\'t exist with this key value.' )  ;
                            }
                        }
                        break ;
                    }
                }
            }

            $return .= ' RETURN { ' . $this->getFilterFields( $fields , $doc , $lang ) . ' } ' ;

            $order     = array_key_exists( 'order' , $edge ) ? ' SORT doc_' . $doc . '.' . $edge['order'] . ' ' : ' SORT edge_' . $doc . '.created DESC ' ;
            $direction = (array_key_exists( 'direction' , $edge ) && $edge['direction'] == Model::REVERSE) ? 'OUTBOUND' : 'INBOUND' ;

            $edges_query .=
            ' LET ' . $name . ' = ' .
            '( ' .
                'FOR doc_' . $doc . ' , edge_' . $doc . ' ' .
                'IN ' . $direction . ' ' . $docRef . ' ' . $table . ' ' .
                'OPTIONS { bfs : true , uniqueVertices : \'global\' } ' .
                $order .
                $return .
            ') ';
        }

        return $edges_query ;
    }

    /**
     * Returns the AQL query RETURN NODE expression defines with a specific fields definitions.
     * @param $fields
     * @param $queryFields
     * @param null $lang
     * @param string $docRef
     * @param false $result
     * @return string
     * @throws Exception
     */
    public function getFields( $fields , $queryFields , $lang = NULL , $docRef = 'doc' , $result = FALSE ) :string
    {
        $query = '' ;

        $edges_fields = NULL ;

        // $this->logger->warning( $this . ' ' . __FUNCTION__ . ', line:' . __LINE__ . ', fields:' . json_encode($fields) ) ;
        // $this->logger->warning( $this . ' ' . __FUNCTION__ . ', line:' . __LINE__ . ', queryFields:' . json_encode($queryFields)) ;

        if( $fields == '*' && $queryFields == NULL )
        {
            if( $this->joins && is_countable($this->joins) && count( $this->joins ) > 0 )
            {
                foreach( $this->joins as $join )
                {
                    $query .= $this->getJoin( [ $join ] , $docRef , $lang ) ;
                }
            }

            if( $this->edges && is_countable($this->edges) && count( $this->edges ) > 0 )
            {
                $query .= $this->getEdges( $this->edges , $docRef , $lang ) ;
                $edges_fields = [] ;
                foreach( $this->edges as $edge )
                {
                    array_push( $edges_fields , '{ ' . $edge[Model::NAME] . ' : ' . $edge[Model::NAME] . ' }' ) ;
                }
            }

            if( $edges_fields )
            {
                $query .= ' RETURN MERGE ( doc , ' . implode( ',' , $edges_fields ) . ' )' ;
            }
            else
            {
                $query .= ' RETURN doc' ;
            }
        }
        else
        {
            if( $queryFields == NULL )
            {
                $r = explode( ',' , $fields );

                $fields = [] ;
                foreach( $r as $value )
                {
                    array_push( $fields , $value . ': doc.' . $value ) ;
                }

                $fields = implode( ' , ' , $fields ) ;
            }
            else
            {
                $doc = $docRef ;

                if( $docRef != 'doc' )
                {
                    $doc = 'doc_' . $docRef ;
                }

                foreach( $queryFields as $key => $field )
                {
                    if( is_array($field) && array_key_exists( Model::FILTER , $field ) )
                    {
                        switch( $field[ Model::FILTER ] )
                        {
                            case Thing::FILTER_EDGE :
                            case Thing::FILTER_EDGE_SINGLE :
                            {
                                $current = null ;

                                foreach( $this->edges as $edge )
                                {
                                    if( $edge[ Model::NAME ] == $key )
                                    {
                                        $current = $edge ;
                                        break ;
                                    }
                                }

                                if( $current != NULL )
                                {
                                    $current[Model::UNIQUE] = $field[Model::UNIQUE] ;
                                    $query .= $this->getEdges( [ $current ] , $doc , $lang ) ;
                                }
                                else
                                {
                                    $this->logger->warning( $this . ' ' . __FUNCTION__ . ' failed with the key:"' . $key . '", this FILTER_EDGE_SINGLE field don\'t find an edge reference with this key value.' ) ;
                                }
                                break ;
                            }
                            case Thing::FILTER_EDGE_COUNT :
                            {
                                $current = null ;
                                foreach( $this->edges as $edge )
                                {
                                    if( 'num' . ucfirst( $edge[ Model::NAME ] ) == $key )
                                    {
                                        $current = $edge ;
                                        break ;
                                    }
                                }
                                $query .= $this->getEdgeCount( $field[Model::UNIQUE] , $current , $doc ) ;
                                break ;
                            }
                            case Thing::FILTER_JOIN :
                            case Thing::FILTER_JOIN_ARRAY :
                            case Thing::FILTER_JOIN_MULTIPLE :
                            {
                                $current = null ;
                                foreach( $this->joins as $join )
                                {

                                    if( !( $join instanceof Closure ) )
                                    {
                                        if( $join[Model::NAME] == $key )
                                        {
                                            $current = $join ;
                                            break ;
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception( "There is an error with join, it's not an pure object!" . gettype( $join ) ) ;
                                    }

                                }

                                $current[ Model::UNIQUE ] = $field[ Model::UNIQUE ] ;

                                $query .= $this->getJoin( [ $current ] , $doc , $lang ) ;

                                // $this->logger->warning( $this . ' ' . __FUNCTION__ . ' --- ' . $key . ' :: ' . $this->getJoin( [ $current ] , $doc , $lang ) ) ;

                                break ;
                            }
                        }
                    }
                    else
                    {
                        $this->logger->warning( $this . ' ' . __FUNCTION__ . ' failed with the key:"' . $key . '", this definition need a "filter" property.' ) ;
                    }

                }

                $fields = $this->getFilterFields( $queryFields , $docRef , $lang ) ;
            }

            if( $result === FALSE )
            {
                $query .= ' RETURN { ' . $fields . ' }' ;
            }
            else
            {
                $query .= ' LET result = { ' . $fields . ' }' ;
            }
        }

        // $this->logger->debug( $query ) ; // DEBUG Query

        return $query ;
    }

    /**
     * Insert a new item into the table
     * @param array $init The new document definition to insert in the model.
     * @return mixed
     * @throws ArangoException
     */
    public function insert( array $init )
    {
        if( is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode( ',' , $values );

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->arangodb->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->arangodb->execute() ;

            return $this->arangodb->getObject() ;
        }
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     * @param mixed $item
     * @param string $keyValue
     * @param array $init
     *
     * @return object
     *
     * @throws ArangoException
     */
    public function update( $item , string $keyValue , array $init = [] ) : ?object
    {
        [
            'active' => $active,
            'key'    => $key
        ]
        = array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        if( is_array($this->fillable) && ( is_array($item) || is_object($item) ) )
        {
            $fillable = array_keys( $this->fillable ) ;

            foreach( $fillable as $property )
            {
                $exist = FALSE ;
                $value = NULL  ;

                if( is_array( $item ) )
                {
                    $exist = array_key_exists( $property , $item ) ;
                }
                else if( is_object( $item ) )
                {
                    $exist = property_exists( $item , $property ) ;
                }

                if( $exist )
                {
                    $fields[] = $property . ': @' . $property ;
                    $binds[ $property ] = is_array($item) ? $item[$property] : $item->{ $property } ;
                }
            }

        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @key '
               . 'UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' '
               . 'RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getObject() ;
    }

    /**
     * The default 'update date' method options.
     */
    const ARGUMENTS_UPDATE_DATE_DEFAULT = [ 'key' => '_key' ] ;

    /**
     * Update the specific date item.
     * @param string $value
     * @param array $init
     * @return bool
     * @throws ArangoException
     */
    public function updateDate( string $value , array $init = [] ) :bool
    {
        [ 'key' => $key ] = array_merge( self::ARGUMENTS_UPDATE_DATE_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table . ' '
               . 'FILTER doc.' . $key . ' == @value '
               . 'UPDATE doc WITH { modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' '
               . 'RETURN NEW';

        $this->arangodb->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->arangodb->execute() ;

        return (bool) $this->arangodb->getObject() ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString():string
    {
        return '[' . get_class($this) . ']' ;
    }

    // ---------- Queries helpers

    protected function debugQuery( $query , $binds )
    {
        $this->logger->debug( $this . ' ------------' ) ;
        $this->logger->debug( $query );
        $this->logger->debug( $this . ' ------------' ) ;
        $this->logger->debug( json_encode( $binds , JSON_PRETTY_PRINT ) );
        $this->logger->debug( $this . ' ------------' ) ;
    }

    protected function getEdgeCount( $name , $edge , $docRef = 'doc' ) :string
    {
        $table = $this->container->get( $edge[ Model::EDGECONTROLLER ] )->edge->table ;

        return ' LET ' . $name . ' = '
             . '( '
             . 'LENGTH( FOR v IN OUTBOUND ' . $docRef . ' ' . $table . ' RETURN v ) '
             . ') ' ;
    }

    protected function getFilterFields( $queryFields , $doc = 'doc' , $lang = NULL ): string
    {
        $config = $this->container->get( 'settings' ) ;

        $url      = $config['app']['url'] ;
        $mediaUrl = $config['app']['mediaUrl'] ;

        $fields = [] ;
        if( $doc != 'doc' )
        {
            $doc = 'doc_' . $doc ;
        }

        foreach( $queryFields as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options )      ? $options['filter']      : NULL ;
            $name   = array_key_exists( Model::UNIQUE , $options ) ? $options[Model::UNIQUE] : $key ;
            $type   = array_key_exists( 'type' , $options )        ? $options['type']        : NULL ;

            switch( $filter )
            {
                case Thing::FILTER_BOOL :
                {
                    $field = $key . ': TO_BOOL( ' . $doc . '.' . $key . ' )' ;
                    break ;
                }

                case Thing::FILTER_ID :
                {
                    $field = $key . ': TO_NUMBER( ' . $doc . '._key )' ;
                    break ;
                }

                case Thing::FILTER_IMAGE :
                {
                    $field = $key . ': ' . $doc . '.image ? CONCAT( "' . $url . 'media" , "/" , ' . $doc . '.image ) : null ' ;
                    break ;
                }

                case Thing::FILTER_INT :
                {
                    $field = $key . ': TO_NUMBER( ' . $doc . '.' . $key . ' )' ;
                    break ;
                }

                case Thing::FILTER_MEDIA_SOURCE :
                {
                    $field = $key . ': IS_ARRAY( ' . $doc . '.source ) ? ( FOR s IN IS_ARRAY( ' . $doc . '.source ) ? ' . $doc . '.source : [] RETURN MERGE( s , { contentUrl : CONCAT( "' . $mediaUrl . '" , s.contentUrl ) } ) ) : null ' ;
                    break;
                }

                case Thing::FILTER_MEDIA_THUMBNAIL :
                {
                    $field = $key . ': IS_OBJECT( ' . $doc . '.thumbnail ) ? MERGE( ' . $doc . '.thumbnail , { contentUrl : CONCAT( "' . $mediaUrl . '" , ' . $doc . '.thumbnail.contentUrl ) } ) : null ' ;
                    break;
                }

                case Thing::FILTER_MEDIA_URL :
                {
                    $field = $key . ': ' . $doc . '.contentUrl ? CONCAT( "' . $mediaUrl . '" , ' . $doc . '.contentUrl ) : null ' ;
                    break ;
                }

                case Thing::FILTER_URL :
                {
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.path , "/" , ' . $doc . '._key )' ;
                    break ;
                }

                case Thing::FILTER_URL_API :
                {
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.' . $key . ')' ;
                    break ;
                }

                case Thing::FILTER_THESAURUS_URL :
                {
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.path )' ;
                    break ;
                }

                case Thing::FILTER_THESAURUS_IMAGE :
                {
                    $field = $key . ': TO_BOOL( ' . $doc . '.' . $key . ' ) == true ? CONCAT( "' . $url . '" , ' . $doc . '.path , "/" , ' . $doc . '._key , "/image" ) : null ' ;
                    break ;
                }
                case Thing::FILTER_DATETIME :
                    $field = $key . ': IS_DATESTRING(' . $doc . '.' . $key . ') ? DATE_FORMAT( ' . $doc . '.' . $key . ' ? : 0 , "%yyyy-%mm-%ddT%hh:%ii:%ssZ" ) : null' ;
                    break ;

                case Thing::FILTER_DISTANCE :
                {
                    $field = $key . ': distance' ;
                    break ;
                }

                case Thing::FILTER_EDGE_SINGLE :
                case Thing::FILTER_JOIN        :
                {
                    $field = $key . ': IS_ARRAY( ' . $name . ' ) ? FIRST( ' . $name . ' ? : [] ) : null';
                    break ;
                }

                case Thing::FILTER_JOIN_COUNT :
                {
                    // get property
                    $prop = lcfirst( substr( $name , strlen( 'num' ) ) ) ;
                    $field = $key . ': IS_ARRAY( ' . $doc . '.' . $prop . ' ) ? LENGTH( ' . $doc . '.' . $prop . ' ) : 0';
                    break ;
                }

                case Thing::FILTER_EDGE_COUNT :
                case Thing::FILTER_EDGE :
                case Thing::FILTER_JOIN_ARRAY :
                case Thing::FILTER_JOIN_MULTIPLE :
                case Thing::FILTER_UNIQUE_NAME :
                {
                    $field = $key . ': ' . $name ;
                    break ;
                }

                case Thing::FILTER_TRANSLATE :
                {
                    if( $lang != NULL )
                    {
                        $field = $key . ': TRANSLATE( "' . $lang . '" , ' . $doc . '.' . $key . ' , "" ) ' ;
                        break ;
                    }
                }

                case Thing::FILTER_DEFAULT :
                default :
                {
                    $field = $key . ': ' . $doc . '.' . $key ;
                    break ;
                }
            }
            array_push( $fields , $field ) ;
        }

        return implode( ' , ' , $fields ) ;
    }

    /**
     * @param $joins
     * @param string $docRef
     * @param null $lang
     *
     * @return string
     *
     * @throws Exception
     */
    protected function getJoin( $joins , $docRef = 'doc' , $lang = NULL ) :string
    {
        $joins_query = '' ;

        foreach( $joins as $join )
        {
            $name = ( array_key_exists( Model::UNIQUE , $join ) && ( $join[Model::UNIQUE] != NULL || $join[Model::UNIQUE] != '' ) )
                    ? $join[ Model::UNIQUE ]
                    : $join[ Model::NAME   ] ;

            if( !isset( $join[ Model::CONTROLLER ] ) || !$this->container->has( $join[ Model::CONTROLLER ] ) )
            {
                throw new Exception( $this . ' getJoin failed, the controller is not registered on the join definition : ' . $name ) ;
            }

            $controller = $this->container->get( $join[ Model::CONTROLLER ] ) ;

            $joinKey = array_key_exists( Model::KEY , $join ) ? $join[ Model::KEY ] : '_key' ;
            $fields  = $controller->getFields( $join[ Model::SKIN ] ) ;
            $return  = '' ;

            foreach( $fields as $key => $field )
            {
                switch( $field[ Model::FILTER ] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                    {
                        if( array_key_exists( Model::EDGES , $join ) && count( $join[Model::EDGES] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $join[ Model::EDGES ] as $ed )
                            {
                                if( $ed[ Model::NAME ] == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $currentEdge[ Model::UNIQUE ] = $field[ Model::UNIQUE ];
                            $return .= self::getEdges( [ $currentEdge ] , 'doc_' . $join[Model::NAME] , $lang);
                        }
                        break ;
                    }

                    case Thing::FILTER_EDGE_COUNT :
                    {
                        if( array_key_exists( Model::EDGES , $join ) && count( $join[Model::EDGES] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $join[ Model::EDGES ] as $ed )
                            {
                                if( 'num' . ucfirst($ed[ Model::NAME ]) == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $return .= self::getEdgeCount( $field[ Model::UNIQUE ] , $currentEdge , 'doc_' . $join[Model::NAME] );
                        }
                        break ;
                    }

                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                    {
                        if( array_key_exists( Model::JOINS , $join ) && count( $join[Model::JOINS] ) > 0 )
                        {
                            $currentJoin = null;
                            foreach( $join[ Model::JOINS ] as $jo )
                            {
                                if( $jo[ Model::NAME ] == $key )
                                {
                                    $currentJoin = $jo;
                                    break;
                                }
                            }
                            $currentJoin[ Model::UNIQUE ] = $field[ Model::UNIQUE ];
                            $return .= self::getJoin([ $currentJoin ] , 'doc_' . $join[Model::NAME] , $lang);
                        }
                        break ;
                    }
                }
            }

            if( $fields == '*' )
            {
                $return .= ' RETURN doc_' . $join[ Model::NAME ] . ' ' ;
            }
            else
            {
                $return .= ' RETURN { ' . $this->getFilterFields( $fields , $join[ Model::NAME ] , $lang ) . ' } ' ;
            }

            if( array_key_exists( Model::MULTIPLE , $join ) && is_array( $join[ Model::MULTIPLE ] ) )
            {
                if( array_key_exists( Model::REVERSE , $join ) && $join[Model::REVERSE] != '' )
                {
                    $request = 'FOR doc_' . $join[Model::NAME] . '_multiple IN [' . implode( ',' , $join[Model::MULTIPLE] ) . '] '
                             . 'FOR doc_' . $join[Model::NAME] . ' IN doc_' . $join[Model::NAME] . '_multiple '
                             . 'FILTER doc_' . $join[Model::NAME] . '.' . $join[Model::REVERSE]
                             . ' ANY == ' . $docRef . '._id ' ;
                }
                else
                {
                    $request = 'FOR doc_' . $join[Model::NAME] . '_index IN ' . $docRef . '.' . $join[ Model::NAME     ]
                             . ' '
                             . 'LET doc_' . $join[Model::NAME] . '_multiple = UNION( ' . implode(',' , $join[ Model::MULTIPLE ] ) . ' ) '
                             . 'LET doc_' . $join[Model::NAME] . ' = NTH( doc_' . $join[Model::NAME] . '_multiple , POSITION( doc_' . $join[Model::NAME] . '_multiple[*]._id , doc_' . $join[Model::NAME] . '_index , true ) ) ' ;
                }

            }
            else if( array_key_exists( Model::ARRAY , $join ) && $join[Model::ARRAY] == true )
            {
                $table    = $this->container->get( $join[ Model::CONTROLLER ] )->model->table ;
                $refArray = 'ref_' . $join[ Model::NAME ] ;

                // position
                $position = '' ;
                if( array_key_exists( Model::POSITION , $fields ) )
                {
                    $position = 'LET ' . $fields[ Model::POSITION ][ Model::UNIQUE ]
                              . ' = POSITION( ' . $docRef . '.' . $join[Model::NAME] . ' , ' . $refArray . ' , true ) ' ;
                }

                // request
                $request = 'FOR ' . $refArray . ' IN ' . $docRef . '.' . $join[ Model::NAME ] . ' ' .
                    $position .
                    'FOR doc_' . $join[Model::NAME] . ' IN ' . $table . ' ' .
                    'FILTER doc_' . $join[Model::NAME] . '.' . $joinKey . ' == TO_STRING( ' . $refArray . ' ) ' ;

                // inject position in return query
            }
            else
            {
                $table   = $this->container->get( $join['controller'] )->model->table ;
                $request = 'FOR doc_' . $join[Model::NAME] . ' IN ' . $table . ' ' .
                    // remove 'LIMIT 1' to avoid bug with fullCount with arangodb
                    //'FILTER doc_' . $join[Model::NAME] . '.' . $joinKey . ' == TO_STRING( ' . $docRef . '.' . $join[Model::NAME] . ' ) LIMIT 1';
                    'FILTER doc_' . $join[Model::NAME] . '.' . $joinKey . ' == TO_STRING( ' . $docRef . '.' . $join[Model::NAME] . ' ) ';

            }

            $joins_query .= ' LET ' . $name . ' = '
                         .  '( '
                         . $request
                         . $return
                         . ') ';
        }

        return $joins_query ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     * @param array|NULL $facets
     * @param string $doc
     * @return array
     */
    protected function getQueryFacets( ?array $facets , string $doc = 'doc' ):array
    {
        $condition = '' ;
        $values    = [] ;

        if
        (
            isset($facets) && is_array($facets)
            && isset($this->facetable) && is_array($this->facetable)
        )
        {
            foreach( $facets as $key => $value )
            {
                if( array_key_exists( $key , $this->facetable ) )
                {
                    try
                    {
                        $set      = $this->facetable[$key] ;
                        $type     = $set[ Model::FACET_TYPE ] ;
                        $property = array_key_exists( Model::FACET_PROPERTY , $set ) ? $set[ Model::FACET_PROPERTY ] : '_key' ;

                        switch( $type )
                        {
                            case Model::FACET_ARRAY_COMPLEX :
                            {
                                $items  = NULL ;

                                $filter = [] ;

                                foreach( $value as $keyM => $s )
                                {
                                    $search = preg_replace( '/\./' , '_' , $key . '_' .$keyM ) ;

                                    // test negative and multiple
                                    if( is_array( $s ) && !empty( $s ) )
                                    {
                                        $subFilter = [] ;

                                        $negative = false ;
                                        $i = 0 ;
                                        foreach( $s as $si )
                                        {
                                            // test negative
                                            if( is_string( $si ) && $si[0] == '-' )
                                            {
                                                $si = substr( $si , 1 ) ;
                                                $negative = true ;
                                            }
                                            else if( is_int( $si ) && $si < 0 )
                                            {
                                                $si = abs( $si ) ;
                                                $negative = true ;
                                            }

                                            $subSearch = $search . $i ;

                                            $values[$subSearch] = [ 'value' => $si ] ;

                                            $eq = '=' ;
                                            if( $negative == true )
                                            {
                                                $eq = '!' ;
                                            }

                                            array_push( $subFilter , 'doc_' . $key . '.' . $keyM . ' ' . $eq . '= @' . $subSearch ) ;

                                            $i++ ;
                                        }

                                        $glue = ' || ' ;
                                        if( $negative == true )
                                        {
                                            $glue = ' && ' ;
                                        }
                                        array_push( $filter , implode( $glue , $subFilter ) ) ;
                                    }
                                    else
                                    {
                                        // test negative
                                        if( is_string( $s ) && $s[0] == '-' )
                                        {
                                            $s = substr( $s , 1 ) ;
                                            $eq = '!' ;
                                        }
                                        else if( is_int( $s ) && $s < 0 )
                                        {
                                            $s = abs( $s ) ;
                                            $eq = '!' ;
                                        }
                                        else
                                        {
                                            $eq = '=' ;
                                        }
                                        $values[$search] = [ 'value' => $s ] ;

                                        array_push( $filter , 'doc_' . $key . '.' . $keyM . ' ' . $eq . '= @' . $search ) ;
                                    }

                                }

                                $condition =
                                    ' LENGTH( '.
                                    ' FOR doc_'    . $key . ' IN result.' . $key .
                                    ' FILTER '     . implode( ' && ' , $filter ) .
                                    ' RETURN doc_' . $key . '._key ' .
                                    ') > 0';

                                break ;
                            }
                            case Model::FACET_EDGE :
                            {
                                $items  = NULL ;
                                $fields = array_key_exists( Model::FIELDS , $set ) ? $set[ Model::FIELDS ] : '_key' ;
                                $edge   = array_key_exists( Model::EDGE   , $set ) ? $set[ Model::EDGE   ] : NULL ;

                                $condition =
                                            ' LENGTH('.
                                            ' FOR doc_'    . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                            ' FILTER doc_' . $key . '.' . $fields . ' == @' . $key .
                                            ' RETURN doc_' . $key . '._key ' .
                                            ') > 0';

                                $values[$key] = [ 'value' => (string) $value ] ;

                                break ;
                            }

                            case Model::FACET_EDGE_COMPLEX :
                            {
                                $items  = NULL ;
                                $edge   = array_key_exists( 'edge' , $set ) ? $set['edge'] : NULL ;

                                $filter = [] ;

                                foreach( $value as $keyM => $s )
                                {
                                    $values[$key.$keyM] = [ 'value' => $s ] ;

                                    array_push( $filter , 'doc_' . $key . '.' . $keyM . ' == @' . $key . $keyM ) ;
                                }

                                $condition = ' LENGTH( '.
                                    ' FOR doc_' . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                    ' FILTER ' . implode( ' && ' , $filter ) .
                                    ' RETURN doc_' . $key . '._key ' .
                                    ') > 0';

                                break ;
                            }

                            case Model::FACET_THESAURUS :
                            {
                                $items  = NULL ;
                                $fields = array_key_exists( 'fields' , $set ) ? $set['fields'] : '_key,name,alternateName' ;
                                $edge   = array_key_exists( 'edge'   , $set ) ? $set['edge']   : NULL ;

                                $filter = [] ;

                                foreach( explode( ',' , $value ) as $keyM => $s )
                                {
                                    if( $s[0] == '-' )
                                    {
                                        $s = ltrim( $s , '-' );
                                        $contains = ' !CONTAINS' ;
                                        $glue = ' && ' ;
                                    }
                                    else
                                    {
                                        $contains = ' CONTAINS' ;
                                        $glue = ' || ' ;
                                    }

                                    $values[$key.$keyM] = [ 'value' => (string) $s ] ;

                                    $search = [] ;

                                    foreach( explode( ',' , $fields ) as $field )
                                    {
                                        array_push( $search , $contains . '( doc_' . $key . '.' . $field .' , @' . $key . $keyM . ' ) ' ) ;
                                    }

                                    array_push( $filter , '(' . implode( $glue , $search ) . ')' ) ;
                                }

                                $condition = ' LENGTH( '.
                                    ' FOR doc_' . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                    ' FILTER ( ' . implode( ' && ' , $filter ) . ' ) ' .
                                    ' RETURN doc_' . $key . '._key ' .
                                    ') > 0';

                                break ;
                            }

                            case Model::FACET_LIST :
                            {
                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                if( is_array( $value ) && count( $value ) == 1 && array_key_exists( 'length' , $value ) )
                                {
                                    $condition .= ' LENGTH( FOR doc_length IN ' . $this->table . ' FILTER ' . $doc . '._id IN doc_length.' . $property . ' LIMIT 1 RETURN 1 ) == ' . $value['length'] ;
                                }
                                else
                                {
                                    $multiple = explode( ',' , $value ) ;

                                    $arr = [] ;
                                    foreach( $multiple as $keyM => $valueM )
                                    {
                                        $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                        $arr[] = "@" . $key . $keyM ;
                                    }

                                    $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY IN ' . $doc . '.' . $property ;
                                }

                                break ;
                            }

                            case Model::FACET_LIST_FIELD :
                            {
                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                $arr = [] ;
                                foreach( $multiple as $keyM => $valueM )
                                {
                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                    $arr[] = "@" . $key . $keyM ;
                                }

                                $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY == ' . $doc . '.' . $property ;

                                break ;
                            }

                            case Model::FACET_LIST_FIELD_SORTED :
                            {
                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                $arr = [] ;
                                foreach( $multiple as $keyM => $valueM )
                                {
                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                    $arr[] = "@" . $key . $keyM ;
                                }

                                $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY == ' . $doc . '.' . $property ;
                                $condition .= ' SORT POSITION( [ ' . implode( ',' , $arr ) . ' ] , ' . $doc . '.' . $property . ' , true ) ';

                                break ;
                            }

                            default :
                            case Model::FACET_FIELD :
                            {
                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                foreach( $multiple as $keyM => $valueM )
                                {
                                    if( (!empty($valueM) && strlen($valueM) > 1 && $valueM[0] == '-' ) )
                                    {
                                        $valueM = ltrim($valueM,'-') ;
                                        $eq = ' !' ;

                                        if( $keyM > 0 )
                                        {
                                            $condition .= ' && ' ;
                                        }
                                    }
                                    else
                                    {
                                        $eq = ' =' ;

                                        if( $keyM > 0 )
                                        {
                                            $condition .= ' || ' ;
                                        }
                                    }

                                    $condition .= $doc . '.' . $property . $eq . '~ @' . $key . $keyM ;

                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;

                                }

                                if( strlen($condition) > 0 )
                                {
                                    $condition = ' ( ' . $condition . ' ) ' ;
                                }

                                break ;
                            }
                        }
                    }
                    catch( Exception $e )
                    {
                        $this->logger->warning( $this . " all failed, the '" . $key . "' facet failed, " . $e->getMessage() ) ;
                    }
                }
            }
        }

        return [ 'facets' => $condition , 'values' => $values ] ;
    }

    /**
     * Returns the collection of all wanted groupable fields.
     * @param string|NULL $expression
     * @param string $doc The name of the document reference in the AQL query.
     * @return NULL|array
     */
    protected function getQueryGroups( ?string $expression , string $doc = 'doc' ): ?array
    {
        if( empty( $expression ) )
        {
            return NULL ;
        }

        $groups = NULL ;

        $items = explode( ',' , $expression ) ;

        if( count($items) > 0 )
        {
            foreach( $items as $key )
            {
                if( array_key_exists( $key , $this->sortable ) )
                {
                    $groups[] = $key
                              . ' = '
                              . $doc
                              . '.'
                              . $this->sortable[ $key ] ;
                }
            }
        }

        return $groups ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     * @param string|NULL $expression
     * @param string $doc
     * @return NULL|array
     */
    protected function getQueryOrders( ?string $expression , string $doc = 'doc' ): ?array
    {
        if( empty( $expression ) )
        {
            return NULL ;
        }

        $sort = explode( ',' , $expression ) ;
        if( is_countable($sort) && count($sort) > 0 )
        {
            $orders = [] ;

            foreach( $sort as $key )
            {
                if( !empty( $key ) )
                {
                    $first = $key[ 0 ];

                    if( $first == "-" )
                    {
                        $order = 'DESC';
                        $key   = ltrim( $key , "-" ) ;
                    }
                    else
                    {
                        $order = 'ASC';
                    }

                    if( array_key_exists( $key , $this->sortable ) )
                    {
                        $orders[] = $doc
                                  . '.'
                                  . $this->sortable[ $key ]
                                  . ' '
                                  . $order ;
                    }
                }
            }

            return count( $orders ) > 0 ? $orders : NULL ;
        }

        return NULL ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     * @param string|null $expression
     * @param array $fields
     * @param string $doc
     * @return NULL|array
     */
    protected function getQuerySearch( ?string $expression , array $fields = [ 'name' ] , $doc = 'doc' ): ?array
    {
        if( isset( $expression ) && !empty($expression) && is_array( $fields ) && !empty($fields) )
        {
            $condition = '' ;
            $words = explode ( ' ' , $expression ) ;
            $i     = 0 ;
            if( count($words) > 0 )
            {
                foreach( $words as $word )
                {
                    $condition .= (($i == 0) ? '' : ' && ' ) ;
                    $j = 0 ;
                    foreach( $fields as $field )
                    {
                        if( $j > 0 )
                        {
                            $condition .= ' || ' ;
                        }
                        $condition .= "LIKE( $doc.$field , @search" . $i . ' , true ) ' ;
                        $j++ ;
                    }
                    $i++ ;
                }
            }
            return [ 'search' => ' ( ' . $condition . ' ) ' , 'words' => $words ] ;
        }

        return NULL ;
    }

    protected function prepareActive( $init , array &$conditions )
    {
        if( isset($init) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $init ? '1' : '0') ;
        }
    }

    protected function prepareConditions( string &$query , array &$conditions )
    {
        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }
    }

    protected function prepareFacets( ?array $init , array &$conditions , array &$binds )
    {
        [
            'facets' => $facets,
            'values' => $values
        ]
        = $this->getQueryFacets( $init ) ;

        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }
    }

    protected function prepareGroups( string &$query , ?string $groupBy )
    {
        $groups = $this->getQueryGroups( $groupBy ) ;
        if( is_countable($groups) && count($groups) > 0 )
        {
            $query .= ' COLLECT ' . implode( ', ' , $groups ) . ' ' ;
        }
    }

    protected function prepareLimit( string &$query , array &$binds , $limit , $offset = 0 )
    {
        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit ' ;

            $binds['offset'] = $offset ;
            $binds['limit']  = $limit ;
        }
    }

    protected function prepareOrders( $expression , string &$query , $doc = 'doc' ) :bool
    {
        $orders = $this->getQueryOrders( $expression , $doc ) ;

        if( is_countable($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' . implode( ', ' , $orders ) . ' ' ;
            return TRUE ;
        }

        return FALSE ;
    }

    protected function prepareSearch( $init , array &$conditions , array &$binds , $doc = 'doc' )
    {
        $init = $this->getQuerySearch( $init , $this->searchable , $doc ) ;

        if( isset( $init ) )
        {
            [
                'search' => $search,
                'words'  => $words
            ]
            = $init ;

            if( !empty($search) )
            {
                $conditions[] = $search ;
            }

            if( isset( $words ) )
            {
                $i = 0 ;
                foreach( $words as $word )
                {
                    $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
                }
            }
        }
    }
}
