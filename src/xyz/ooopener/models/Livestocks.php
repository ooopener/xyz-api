<?php

namespace xyz\ooopener\models;

use ArangoDBClient\Exception;
use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Livestocks model.
 */
class Livestocks extends Collections
{
    /**
     * Creates a new Model instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container ,  ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,
        'numbers'    => Thing::FILTER_DEFAULT,
        'workshops'  => Thing::FILTER_DEFAULT,
        'isBasedOn'  => Thing::FILTER_DEFAULT
    ];

    /**
     * Return all elements.
     * @param array $init
     * @return array All the elements
     * @throws Exception
     */
    public function all( array $init = [] ):array
    {
        [
            'active'      => $active,
            'binds'       => $binds,
            'conditions'  => $conditions,
            'facets'      => $facets,
            'fetchStyle'  => $fetchStyle,
            'limit'       => $limit,
            'offset'      => $offset,
            'sort'        => $sort,
            'search'      => $search,
            'fields'      => $fields ,
            'lang'        => $lang ,
            'queryFields' => $queryFields ,
            'withStatus'  => $withStatus
        ]
        = array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ;

        $query = 'FOR doc IN ' . $this->table ;

        $query .= $this->getFields( $fields , $queryFields , $lang , 'doc' , TRUE ) ;

        $this->prepareSearch( $search , $conditions , $binds , 'result' ) ;
        $this->prepareActive( $active , $conditions ) ;
        $this->prepareFacets( $facets , $conditions , $binds ) ;

        $this->prepareConditions ( $query , $conditions               ) ; // FILTER
        $this->prepareOrders     ( $sort , $query , 'result'          ) ; // SORT
        $this->prepareLimit      ( $query , $binds , $limit , $offset ) ; // LIMIT / OFFSET

        $query .= ' RETURN result ' ;

        $this->arangodb->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->arangodb->execute() ;

        return $this->arangodb->getAll() ;
    }
}


