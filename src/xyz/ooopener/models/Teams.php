<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The Teams model.
 */
class Teams extends Collections
{
    /**
     * Creates a new Teams instance.
     * @param ContainerInterface $container
     * @param string|null $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'active'        => Thing::FILTER_INT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'color'         => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'identifier'    => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'path'          => Thing::FILTER_DEFAULT,
        'permissions'   => Thing::FILTER_DEFAULT
    ];


}


