<?php

namespace xyz\ooopener\models;

use xyz\ooopener\things\Thing;

use Psr\Container\ContainerInterface ;

/**
 * The collection of brands used by an organization or business person for labeling a product, product group, or similar.
 */
class Brands extends Collections
{
    /**
     * Creates a new Brands instance.
     * @param ContainerInterface $container
     * @param ?string $table
     * @param array $init
     */
    public function __construct( ContainerInterface $container , ?string $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public array $fillable =
    [
        'id'              => Thing::FILTER_INT,
        'path'            => Thing::FILTER_DEFAULT,
        'name'            => Thing::FILTER_DEFAULT,
        'active'          => Thing::FILTER_INT,
        'withStatus'      => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'slogan'        => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,

        'audios' => Thing::FILTER_DEFAULT,
        'photos' => Thing::FILTER_DEFAULT,
        'videos' => Thing::FILTER_DEFAULT,

        'isBasedOn' => Thing::FILTER_DEFAULT
    ];

}

