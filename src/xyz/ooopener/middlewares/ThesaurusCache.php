<?php

namespace xyz\ooopener\middlewares ;

use Psr\Container\ContainerInterface;

use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

use Psr\Log\LoggerInterface;
use xyz\ooopener\controllers\ThesaurusCollectionController;
use xyz\ooopener\data\ThesaurusCollector;

class ThesaurusCache implements MiddlewareInterface
{
    /**
     * ThesaurusCache constructor.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container ;
    }

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @param Request $request
     * @param RequestHandler $handler
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $this->container->get( LoggerInterface::class )->warning( 'ThesaurusCache::process' );

        $collector = $this->container->get( ThesaurusCollector::class ) ;

        if( !$collector->exists() )
        {
            $this->container->get( ThesaurusCollectionController::class )->initCollector();
        }

        return $handler->handle( $request ) ;
    }
}
