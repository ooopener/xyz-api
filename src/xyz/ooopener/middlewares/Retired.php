<?php

namespace xyz\ooopener\middlewares ;

use Psr\Container\ContainerInterface ;

use Psr\Http\Message\ResponseFactoryInterface ;
use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

use xyz\ooopener\controllers\Controller ;


/**
 * Stops the application if is retired.
 */
class Retired extends Controller implements MiddlewareInterface
{
    /**
     * @var ResponseFactoryInterface
     */
    private ResponseFactoryInterface $responseFactory;

    /**
     * The constructor.
     *
     * @param ResponseFactoryInterface $responseFactory The response factory
     * @param ContainerInterface $container
     */
    public function __construct( ResponseFactoryInterface $responseFactory , ContainerInterface $container )
    {
        parent::__construct( $container ) ;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        if( (bool) $this->config['retired'] )
        {
            $response = $this->responseFactory->createResponse() ;
            return $this->formatError
            (
                $response , '301' ,
                [
                    $this->config[ 'version' ] ,
                    $this->config[ 'next'    ]
                ]
            );
        }

        return $handler->handle( $request ) ;
    }
}

