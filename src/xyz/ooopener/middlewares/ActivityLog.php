<?php

namespace xyz\ooopener\middlewares ;

use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\ActivityLogs;
use Psr\Container\ContainerInterface;

use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;


use Exception ;
use Psr\Log\LoggerInterface;
use Slim\App;

class ActivityLog implements MiddlewareInterface
{
    /**
     * ActivityLog constructor.
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container ;
        $this->logger = $this->container->get( LoggerInterface::class ) ;
    }

    /**
     * @var ContainerInterface
     */
    protected ContainerInterface $container ;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger ;

    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $response = $handler->handle( $request ) ;

        $code   = $response->getStatusCode() ;
        $method = $request->getMethod() ;

        $passthrough = $this->container->get('settings')['token']['passthrough'];

        $path  = $request->getUri()->getPath() ;

        if( $code == 200 && !$this->checkPassthroughPath( '/' . $path , $passthrough ) && ( $method == "POST" || $method == "PUT" || $method == "PATCH" || $method == "DELETE" ) )
        {
            $app      = $this->container->get( App::class ) ;
            $basePath = $app->getBasePath() ;
            $path     = substr( $path , strlen( $basePath ) + 1 ) ;

            switch( $method )
            {
                case "DELETE" :
                {
                    $list = $this->getParam( $request , 'list' ) ;
                    if( $list )
                    {
                        $list = explode( ',' , $list ) ;
                        if( $list && count( $list ) > 0 )
                        {
                            foreach( $list as $key => $value )
                            {
                                // $this->logger->info( 'ActivityLog::saveActivityLog ' . $method . ' path:' . $path . '/' . $value ) ;
                                $this->saveActivityLog( $request , $method , $path . '/' . $value ) ;
                            }
                        }
                    }
                    else
                    {
                        $this->saveActivityLog( $request , $method , $path ) ;
                    }
                    break ;
                }
                case "POST" :
                {
                    $body = json_decode( $response->getBody() , false ) ;
                    if( $body && property_exists( $body , 'result' ) )
                    {
                        if( is_array( $body->result ) )
                        {
                            foreach( $body->result as $thing )
                            {
                                if( isset( $thing->id ) )
                                {
                                    $this->saveActivityLog( $request , $method , $path . '/' . $thing->id ) ;
                                }
                            }
                        }
                        else if( isset( $body->result->id ) )
                        {
                            $this->saveActivityLog( $request , $method , $path . '/' . $body->result->id ) ;
                        }
                    }
                    break ;
                }
                default :
                {
                    $this->saveActivityLog( $request , $method , $path ) ;
                }
            }
        }

        return $response ;
    }

    /**
     * @param string $uri
     * @param array $list
     *
     * @return bool
     */
    private function checkPassthroughPath( string $uri , array $list ) : bool
    {
        foreach( $list as $passthrough )
        {
            $passthrough = rtrim($passthrough, "/") ;
            if( preg_match("@^{$passthrough}(/.*)?$@" , $uri ) )
            {
                return true ;
            }
        }
        return false ;
    }

    private function getParam( Request $request , string $name )
    {
        $value = NULL ;
        if( $request )
        {
            $params = $request->getQueryParams();
            if( isset( $params[ $name ] ) )
            {
                $value = $params[ $name ] ;
            }
            else
            {
                $params = $request->getParsedBody() ;
                if( isset( $params[ $name ] ) )
                {
                    $value = $params[ $name ] ;
                }
            }
        }
        return $value ;
    }

    /**
     * @param Request $request
     * @param string $method
     * @param string $resource
     */
    private function saveActivityLog( Request $request , string $method , string $resource ) : void
    {
        try
        {
            $userInfos = $this->container->get( UserInfos::class ) ;

            $ip    = $userInfos->getIp() ;
            $agent = $userInfos->getUserAgent() ;

            $jwt = $request->getAttribute( 'jwt' ) ;

            if( $jwt )
            {
                $item =
                [
                    'active'   => 1,
                    'path'     => 'activityLogs',
                    'user'     => $jwt->sub ,
                    'method'   => $method ,
                    'ip'       => $ip ,
                    'agent'    => $agent ,
                    'resource' => $resource
                ];

                $activityLogsModel = $this->container->get( ActivityLogs::class ) ;
                $activityLogsModel->insert( $item ) ;
            }
        }
        catch( Exception $e )
        {
            $this->logger->warning( "ActivityLogs::saveActivityLog failed, error: " . $e->getMessage() ) ;
        }
    }
}
