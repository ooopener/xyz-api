<?php

namespace xyz\ooopener\middlewares ;

use xyz\ooopener\helpers\UserInfos;
use xyz\ooopener\models\Sessions;
use Exception ;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

use xyz\ooopener\controllers\Controller ;

/**
 * Class CheckToken
 *
 * @package xyz\ooopener\middlewares
 */
class CheckToken extends Controller implements MiddlewareInterface
{
    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $jwt = $request->getAttribute('jwt') ;

        if( $jwt )
        {
            $errorResponse = $this->container->get( ResponseFactoryInterface::class )->createResponse() ;

            try
            {
                $agent    = $this->container->get( UserInfos::class )->getUserAgent() ;
                $sessions = $this->container->get( Sessions::class ) ;
                $check    = $sessions->check( $jwt->jti , $agent ) ;

                if( !$check )
                {
                    $sessions->revoke( $jwt->jti ) ; // revoke token if necessary

                    $path = $request->getUri()->getPath() ;

                    if( $path == 'oauth/authorize' )
                    {
                        $_SESSION[ "urlRedirect" ] = (string) $request->getUri() ; // save to redirect
                        return $this->redirectFor( $errorResponse , 'api.logout' , [] , 302 ) ;
                    }
                    else if( $path != "login" )
                    {
                        switch( $request->getAttribute('format') )
                        {
                            case 'json' :
                            {
                                return $this->error( $errorResponse, "token revoked", "401", null, 401 ) ;
                            }
                            default :
                            {
                                $uri = (string) $request->getUri() ;

                                if( $path == "/" )
                                {
                                    $uri = substr( $uri , 0 , -1 ) ;
                                }

                                $_SESSION[ "urlRedirect" ] = $uri ;

                                return $this->redirectFor( $errorResponse , 'api.logout' , [] , 302 ) ;
                            }
                        }
                    }
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $errorResponse, "500", [ $this , $e->getMessage() ], false, 500);
            }
        }

        return $handler->handle( $request ) ;
    }

}
