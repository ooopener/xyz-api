<?php

namespace xyz\ooopener\middlewares ;

use xyz\ooopener\controllers\Controller ;

use Psr\Http\Message\ResponseFactoryInterface ;
use Psr\Http\Message\ResponseInterface ;
use Psr\Http\Message\ServerRequestInterface ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface ;
use Slim\App;

class Authorized extends Controller implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process( ServerRequestInterface $request , RequestHandlerInterface $handler ) : ResponseInterface
    {
        // $this->logger->info( $this . ' ' . __FUNCTION__ . ' ' . $request->getUri() ) ;

        $authorized = FALSE ;

        $uri    = $request->getUri() ;
        $path   = $uri->getPath() ;
        $method = $request->getMethod() ;

        $passthrough = $this->config['token']['passthrough'] ;

        // passthrough 'OPTIONS' method
        if( $method == 'OPTIONS' )
        {
            $authorized = TRUE ;
        }

        $app      = $this->container->get( App::class ) ;
        $basePath = $app->getBasePath() ;

        if( $path == $basePath )
        {
            $authorized = TRUE ; // handle '/'
        }
        else if( $path == $basePath . '/info' && $this->config['debug'] == TRUE )
        {
            $authorized = TRUE ; // handle /info
        }
        else if( $this->checkPassthroughPath( $path , $passthrough ) )
        {
            $authorized = TRUE ;
        }

        // check permissions
        $jwt = $request->getAttribute( 'jwt' ) ;

        if( isset( $jwt ) && isset( $jwt->scope ) )
        {
            // remove basePath in path
            $path = substr( $path , strlen( $basePath ) ) ;

            $scope = json_decode( $jwt->scope ) ;

            // $this->logger->info( $this . ' ' . __FUNCTION__ . ' ' . $request->getUri() . ' :: ' . json_encode($scope, JSON_PRETTY_PRINT) ) ;

            // get main path and if exists get the resource's identifier
            preg_match( '/(?<name>\w+)(\/)?(?<digit>\d+)?/' , $path , $matches ) ;

            $resource = array_key_exists( 'digit' , $matches ) ? '/' . $matches['digit'] : '' ;

            $name = isset( $matches['name'] ) ? $matches['name'] : '' ;

            $path = $name . $resource ;

            if( $scope != null  && ( property_exists( $scope , $name ) || property_exists( $scope , $path ) ) )
            {
                $authorize = property_exists( $scope , $path )
                           ? $scope->{$path}
                           : $scope->{ $matches['name'] } ;

                switch( $authorize )
                {
                    case 'admin':
                    {
                        $authorized = TRUE ;
                        break;
                    }
                    case 'write':
                    {
                        if( $method == 'GET' || $method == 'HEAD' || $method == 'PATCH' || $method == 'POST' || $method == 'PUT' || $method == 'DELETE' )
                        {
                            $authorized = TRUE ;
                        }
                        break;
                    }
                    case 'read':
                    {
                        if( $method == 'GET' || $method == 'HEAD' )
                        {
                            $authorized = TRUE ;
                        }
                        break;
                    }
                }
            }
        }

        if( $authorized == FALSE )
        {
            $format = $request->getAttribute('format') ;

            $response = $this->container->get( ResponseFactoryInterface::class )->createResponse() ;

            switch( $format )
            {
                case 'json' :
                {
                    return $this->formatError( $response , "401" , NULL , NULL , 401 ) ;
                }
                case 'html' :
                default     :
                {
                    return $this->redirectFor( $response , 'api.index' );
                }
            }
        }

        return $handler->handle( $request ) ;

    }

    /**
     * @param string $uri
     * @param array $list
     *
     * @return bool
     */
    private function checkPassthroughPath( string $uri , array $list ) : bool
    {
        $app = $this->container->get( App::class ) ;
        $basePath = $app->getBasePath() ;

        foreach( $list as $passthrough )
        {
            $passthrough = rtrim($basePath . $passthrough, "/") ;
            if( preg_match("@^{$passthrough}(/.*)?$@" , $uri ) )
            {
                return true ;
            }
        }
        return false ;
    }
}
