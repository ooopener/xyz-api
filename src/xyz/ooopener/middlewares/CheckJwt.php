<?php

namespace xyz\ooopener\middlewares ;

use xyz\ooopener\controllers\Controller;
use DomainException ;

use Exception ;

use DI\Container ;

use Firebase\JWT\JWT ;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface ;
use Psr\Http\Message\ServerRequestInterface ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface ;

use RuntimeException ;

use SplStack ;

use xyz\ooopener\rules\RequestMethodRule ;
use xyz\ooopener\rules\RequestPathRule ;

class CheckJwt extends Controller implements MiddlewareInterface
{
    /**
     * Creates a new CheckJwt instance.
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );

        $this->responseFactory = $this->container->get( ResponseFactoryInterface::class ) ;

        /* Setup stack for rules */
        $this->rules = new SplStack();

        if( $this->config )
        {
            $settings = $this->config ;

            if( array_key_exists( 'token' , $settings )  )
            {
                $setToken = $settings['token'] ;

                $this->algorithm   = $setToken['algorithm'] ;
                $this->attribute   = 'jwt' ;
                $this->cookieName  = $setToken['cookie_name'] ;
                $this->path        = $setToken['path'] ;
                $this->ignore      = $setToken['passthrough'] ;
                $this->secure      = $setToken['secure'] ;
                $this->secret      = $setToken['key'] ;
            }

            if( array_key_exists( 'jwt' , $settings ) )
            {
                $setJwt = $settings['jwt'] ;
                $this->relaxed = $setJwt['relaxed'] ;
            }

            /* If nothing was passed in options add default rules. */
            /* This also means $options["rules"] overrides $options["path"] */
            /* and $options["ignore"] */
            if( $this->rules->isEmpty() )
            {
                $this->rules->push
                (
                    new RequestMethodRule
                    (
                        [ 'ignore' => ["OPTIONS"] ]
                    )
                ) ;

                $this->rules->push
                (
                    new RequestPathRule
                    (
                        $this->getBasePath() ,
                        [
                            'path'   => $this->path,
                            'ignore' => $this->ignore
                        ]
                    )
                ) ;
            }
        }
    }

    /**
     * @var array|mixed
     */
    private array $algorithm = [ "HS256", "HS512", "HS384" ] ;

    /**
     * @var string
     */
    private string $attribute = "token" ;

    /**
     * @var mixed|string
     */
    private string $cookieName = "token" ;

    /**
     * @var string
     */
    private string $header = "Authorization" ;

    /**
     * @var array|null
     */
    private $ignore = NULL ;

    /**
     * @var array
     */
    private $path = [ "/" ] ;

    /**
     * @var string
     */
    private string $regexp = "/Bearer\s+(.*)$/i" ;

    /**
     * @var array
     */
    private array $relaxed = ["localhost", "127.0.0.1"] ;

    /**
     * @var ResponseFactoryInterface
     */
    protected ResponseFactoryInterface $responseFactory;

    /**
     * @var SplStack
     */
    private SplStack $rules ;

    /**
     * @var bool
     */
    private bool $secure  = true ;

    /**
     * @var mixed
     */
    private $secret ;

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process( ServerRequestInterface $request , RequestHandlerInterface $handler ) : ResponseInterface
    {
        $uri = $request->getUri() ;

        // $this->logger->info( $this . ' ' . __FUNCTION__ . ' ' . $uri ) ;

        $scheme = $uri->getScheme() ;
        $host   = $uri->getHost() ;

        /* If rules say we should not authenticate call next and return. */
        if( false === $this->shouldAuthenticate( $request ) )
        {
            return $handler->handle( $request );
        }

        /* HTTP allowed only if secure is false or server is in relaxed array. */
        if( "https" !== $scheme && true === $this->secure )
        {
            if( !in_array( $host , $this->relaxed ) )
            {
                $message = sprintf
                (
                    "Insecure use of middleware over %s denied by configuration.",
                    strtoupper( $scheme )
                );
                throw new RuntimeException( $message ) ;
            }
        }

        /* If token cannot be found or decoded return with 401 Unauthorized. */

        try
        {
            $token   = $this->fetchToken( $request ) ;
            $decoded = $this->decodeToken( $token ) ;
        }
        catch( RuntimeException | DomainException | Exception $exception )
        {
            $response = $this->responseFactory->createResponse(401) ;
            return $this->errorToken
            (
                $request,
                $response,
                [
                    "message" => $exception->getMessage(),
                    "uri"     => (string) $request->getUri()
                ]
            );
        }

        /* Add decoded token to request as attribute when requested. */

        if( $this->attribute )
        {
            $request = $request->withAttribute( $this->attribute , $decoded ) ;
            $this->container->set( $this->attribute , $decoded ) ;
        }

        /* Modify $request before calling next middleware. */

        if( isset( $jwt->sub ) )
        {
            $settings = $this->container->get('settings') ;
            $_SESSION[ $settings['auth']['session'] ] = $jwt->sub ;
        }

        /* Everything ok, call next middleware. */
        $response = $handler->handle( $request ) ;

        /* Modify $response before returning. */

        return $response ;
    }

    /**
     * Check if middleware should authenticate.
     * If any of the rules in stack return false will not authenticate
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function shouldAuthenticate( ServerRequestInterface $request ) : bool
    {
        foreach( $this->rules as $callable )
        {
            if( false === $callable( $request ) )
            {
                return false ;
            }
        }
        return true ;
    }

    /**
     * Handle error
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param mixed[] $arguments
     *
     * @return mixed
     */
    private function errorToken( ServerRequestInterface $request , ResponseInterface $response , array $arguments )
    {
        $path = $request->getUri()->getPath() ;

        if( $path == "oauth/authorize" )
        {
            // save to redirect
            $fullUrl = (string)$request->getUri() ;

            $_SESSION[ "urlRedirect" ] = $fullUrl ;

            // go to login
            // $this->logger->debug( $this . ' redirect 1' ) ;

            return $this->redirectFor( $response , 'api.login' , [] , 302 ) ;
        }

        $format = $request->getAttribute('format') ;

        switch( $format )
        {
            case 'json' :
            {
                $data["status"]  = "error JWT";
                $data["message"] = $arguments["message"];
                return $this->jsonResponse( $response , $data ) ;
            }
            default :
            {
                // save to redirect
                $fullUrl = (string) $request->getUri() ;

                if( $path == "/" )
                {
                    $fullUrl = substr( $fullUrl , 0 , -1 ) ;
                }

                $_SESSION[ "urlRedirect" ] = $fullUrl ;

                // go to login
                // $this->logger->debug( $this . ' redirect 2' ) ;

                return $this->redirectFor( $response , 'api.login' , [] , 302 ) ;
            }
        }
    }

    /**
     * Fetch the access token.
     *
     * @param ServerRequestInterface $request
     *
     * @return string
     */
    private function fetchToken( ServerRequestInterface $request ) : string
    {
        /* Check for token in header. */
        $header = $request->getHeaderLine( $this->header ) ;

        if( false === empty( $header ) )
        {
            if( preg_match( $this->regexp , $header, $matches ) )
            {
                // $this->logger->debug($this . " Using token from request header" ) ;
                return $matches[1] ;
            }
        }

        /* Token not found in header try a cookie. */
        $cookieParams = $request->getCookieParams() ;

        if( isset( $cookieParams[ $this->cookieName ] ) )
        {
            // $this->logger->debug($this . " Using token from cookie") ;
            if( preg_match( $this->regexp , $cookieParams[ $this->cookieName ] , $matches ) )
            {
                return $matches[1] ;
            }
            return $cookieParams[ $this->cookieName ] ;
        }

        /* If everything fails log and throw. */

        $this->logger->warning( $this . ' token not found' ) ;

        throw new RuntimeException( 'Token not found' ) ;
    }

    /**
     * Decode the token.
     * @param string $token
     * @return mixed[]
     * @throws Exception
     */
    private function decodeToken( string $token ) : object
    {
        try
        {
            return Jwt::decode
            (
                $token,
                $this->secret,
                (array) $this->algorithm
            );
        }
        catch( Exception $exception )
        {
            $this->logger->warning( $this . ' decodeToken failed, ' . $exception->getMessage() , [ $token ] ) ;
            throw $exception ;
        }
    }

}


