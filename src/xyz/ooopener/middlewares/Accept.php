<?php

namespace xyz\ooopener\middlewares;

use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;


/**
 * This middleware manage the mime types accepted by the client, first with the request header and after with the format parameter.
 */
class Accept implements MiddlewareInterface
{
    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $format = NULL ;

        if( $request->hasHeader( 'Accept' ) ) // check header
        {
            $accept = $request->getHeader('Accept')[ 0 ];
            $accept = explode( ',' , $accept ) ;

            //// check content type - Be careful with order (json is preferred so it's the latest)

            if( in_array('text/csv' , $accept) )
            {
                $format = 'csv';
            }

            if( in_array('application/xml' , $accept) )
            {
                $format = 'xml';
            }

            if( in_array('application/json' , $accept) )
            {
                $format = 'json';
            }
        }

        $params = $request->getQueryParams();

        if( isset( $params[ 'format' ] ) )
        {
            $format = $params[ 'format' ];
        }

        $request = $request->withAttribute( 'format' , $format ) ;

        return $handler->handle( $request ) ;
    }
}

