<?php

namespace xyz\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

use Slim\Routing\RouteContext ;

use xyz\ooopener\controllers\Controller ;

class Cors extends Controller implements MiddlewareInterface
{
    const DEFAULT_ORIGIN  = '*' ;
    const DEFAULT_HEADERS = 'X-Requested-With, Content-Type, Accept, Origin, Authorization, Expires, Last-Modified, If-Modified-Since' ;
    const DEFAULT_MAX_AGE = 600 ;

    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $origin  = self::DEFAULT_ORIGIN  ;
        $headers = self::DEFAULT_HEADERS ;
        $maxAge  = self::DEFAULT_MAX_AGE ;

        if( isset( $this->config['cors'] ) )
        {
            $cors = $this->config['cors'] ;
            if( isset( $cors['origin'] ) )
            {
                $origin  = $cors['origin'] ;
            }
            if( isset( $cors['headers'] ) )
            {
                $headers = $cors['headers'] ;
            }
            if( isset( $cors['maxAge'] ) )
            {
                $maxAge = $cors['maxAge'] ;
            }
        }

        $methods = RouteContext::fromRequest( $request )
                 ->getRoutingResults()
                 ->getAllowedMethods() ;

        $response = $handler->handle( $request ) ;

        $response = $response->withHeader( 'Access-Control-Allow-Headers' , $headers )
                             ->withHeader( 'Access-Control-Allow-Origin'  , $origin )
                             ->withHeader( 'Access-Control-Allow-Methods' , implode(',' , $methods ) )
                             ->withHeader( 'Access-Control-Max-Age'       , $maxAge ) ;

        // Optional: Allow Ajax CORS requests with Authorization header
        // $response = $response->withHeader('Access-Control-Allow-Credentials', 'true') ;

        return $response ;
    }
}
