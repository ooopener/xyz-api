<?php

namespace xyz\ooopener\middlewares ;

use Psr\Container\ContainerInterface;

use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

class HttpCache implements MiddlewareInterface
{
    /**
     * HttpCache constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container ;
    }

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $response = $handler->handle( $request ) ;

        // add expires for png images
        if( $response->hasHeader( 'Content-Type' ) && ( $response->getHeader( 'Content-Type' )[0] == "image/png" ) )
        {
            $response = $response->withHeader( 'Expires' , gmdate('D, d M Y H:i:s T' , time() + 3600 ) ) ;
            $response = $response->withHeader( 'Vary' , 'Accept' ) ;
            $response = $response->withHeader( 'Cache-Control' , 'max-age=2592000' ) ;
        }

        return $response ;
    }
}
