<?php

namespace xyz\ooopener\middlewares;

use Psr\Http\Message\ResponseInterface as Response ;
use Psr\Http\Message\ServerRequestInterface as Request ;

use Psr\Http\Server\MiddlewareInterface ;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler ;

use xyz\ooopener\controllers\Controller ;

/**
 * The LogStatus class.
 */
class LogStatus extends Controller implements MiddlewareInterface
{
    /**
     * @param Request $request
     * @param RequestHandler $handler
     *
     * @return Response
     */
    public function process( Request $request , RequestHandler $handler ) : Response
    {
        $response = $handler->handle( $request ) ;

        $uri  = $request->getUri() ;
        $code = $response->getStatusCode();
        $set  = $this->config;

        $method = $request->getMethod() ;

        if( (bool) $set['useLogging'] )
        {
            /*
            getScheme()
            getAuthority()
            getUserInfo()
            getHost()
            getPort()
            getPath()
            getBasePath()
            getQuery() (returns the full query string, e.g. a=1&b=2)
            getFragment()
            getBaseUrl()
            */
            if( $code < 500 )
            {
                if( $method != 'OPTIONS' )
                {
                    $this->logger->info( 'HTTP ' . $code . ' ' . $uri->getPath() ) ;
                }
            }
            else
            {
                $this->logger->error( 'HTTP ' . $code . ' ' . $uri->getPath() ) ;
            }
        }

        if( $method == 'GET' && (bool) $set['useAnalytics'] )
        {
            $pre = (bool) $set['analytics']['useVersion'] ? '/' . $set['version'] : '' ;
            $this->container->get('tracker')->pageview( $pre . '/status/' . $code , $uri );
        }

        return $response ;
    }
}

