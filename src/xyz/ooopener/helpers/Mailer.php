<?php

namespace xyz\ooopener\helpers ;

use Exception ;

use PHPMailer\PHPMailer\PHPMailer ;
use PHPMailer\PHPMailer\Exception as MailerException ;

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface ;

use Slim\Views\Twig ;

class Mailer
{
    public function __construct( ContainerInterface $container , $config )
    {
        $this->container = $container ;
        $this->config    = $container->get( 'settings' ) ;
        $this->logger    = $container->get( LoggerInterface::class ) ;
        $this->mailer    = new PHPMailer( true ) ;

        $this->mailer->IsSMTP();

        $this->mailer->Host       = $config['host'] ;
        $this->mailer->SMTPAuth   = $config['smtp_auth'] ;
        $this->mailer->Username   = $config['username'] ;
        $this->mailer->Password   = $config['password'] ;
        $this->mailer->SMTPSecure = $config['smtp_secure'] ;
        $this->mailer->Port       = $config['port'] ;

        $this->mailer->SetFrom
        (
            $config['from'],
            $config['from_name'],
            FALSE
        );

        $this->mailer->isHTML( $config['html'] ) ;
        $this->mailer->CharSet = $config['charset'] ;
    }

    /**
     * @var array
     */
    protected array $config ;

    /**
     * @var ContainerInterface
     */
    protected ContainerInterface $container ;

    /**
     * @var mixed|LoggerInterface
     */
    protected LoggerInterface $logger ;

    /**
     * @var PHPMailer
     */
    protected PHPMailer $mailer ;

    /**
     * Send an invitation by email.
     * @param $invite
     * @return bool Returns TRUE if the mail is sending.
     */
    public function sendInvitation( $invite ):bool
    {
        try
        {
            $this->mailer->Subject = 'Invitation' ;

            $this->mailer->addAddress( $invite['email'] ) ;

            $view = $this->container->get( Twig::class ) ;
            $this->mailer->Body = $view->fetch
            (
                'emails/invite.twig' ,
                [
                    'app_name' => $this->config['name'] ,
                    'app_url'  => $this->config['app']['url'] ,
                    'code'     => $invite['code'] ,
                    'name'     => $invite['name'] ,
                    'redirect' => $invite['redirect']
                ]
            ) ;

            $this->mailer->send() ;

            return true ;
        }
        catch( MailerException $e )
        {
            $this->logger->warning( "Error with sending email : " . $this->mailer->ErrorInfo ) ;
            return false ;
        }
        catch( Exception $e )
        {
            $this->logger->warning( "Error email template : " . $e->getMessage() ) ;
            return false ;
        }
    }
}
