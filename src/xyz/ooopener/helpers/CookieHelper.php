<?php

namespace xyz\ooopener\helpers;

use Psr\Http\Message\RequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;


use Dflydev\FigCookies\FigResponseCookies ;
use Dflydev\FigCookies\FigRequestCookies ;
use Dflydev\FigCookies\Modifier\SameSite ;
use Dflydev\FigCookies\SetCookie ;

use DateTime ;

/**
 * Invoked to manage the cookies of the application. Check https://github.com/dflydev/dflydev-fig-cookies
 */
class CookieHelper
{
    /**
     * Creates a new CookieManager instance.
     */
    public function __construct()
    {

    }

    /**
     * get cookie
     *
     * @param $request Request
     * @param $name string
     *
     * @return string
     */
    public function get( Request $request , string $name )
    {
        $cookie = FigRequestCookies::get( $request , $name );
        return isset( $cookie ) ? $cookie->getValue() : NULL;
    }

    /**
     * Set cookie
     *
     * @param Response $response
     * @param string $name
     * @param string|null $value
     * @param null $expiresAt
     * @param null $path
     * @param null $domain
     * @param bool $secure
     * @param bool $httponly
     *
     * @return Response
     */
    public function set( Response $response , string $name , ?string $value , $expiresAt = null , $path = null , $domain = null , $secure = false , $httponly = true )
    {
        return FigResponseCookies::set( $response , SetCookie::create( $name , $value )
            ->rememberForever()
            ->withExpires( $expiresAt )
            ->withPath( $path )
            ->withDomain( $domain )
            ->withSecure( $secure )
            ->withHttpOnly( $httponly )
            ->withSameSite( SameSite::lax() )
        );
    }

    /**
     * delete cookie
     *
     * @param Response $response
     * @param string $name
     *
     * @return Response
     */
    public function delete( Response $response , string $name )
    {
        return FigResponseCookies::expire( $response , $name );
    }


}


