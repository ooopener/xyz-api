<?php

namespace xyz\ooopener\helpers ;

class Version
{
    static function fromString( string $value , string $separator = '.' )
    {
        $v = new \system\Version() ;

        if( $value == null || $value == "" )
        {
            return null ;
        }

        if( strpos( $value , $separator ) > -1 )
        {
            $values = explode( $separator , $value ) ;
            $len = count( $values ) ;

            if( $len > 0 )
            {
                $v->setMajor( (int) $values[0] ) ;
            }

            if( $len > 1 )
            {
                $v->setMinor( (int) $values[1] ) ;
            }

            if( $len > 2 )
            {
                $v->setBuild( (int) $values[2] ) ;
            }

            if( $len > 3 )
            {
                $v->setRevision( (int) $values[3] ) ;
            }
        }
        else
        {
            $vv = (int) $value ;
            if( $vv != 0 )
            {
                $v->setMajor( $vv ) ;
            }
            else
            {
                $v = NULL ;
            }
        }

        return (string) $v ;
    }
}
