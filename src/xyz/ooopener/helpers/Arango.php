<?php

namespace xyz\ooopener\helpers ;

use ArangoDBClient\Collection as ArangoCollection ;
use ArangoDBClient\CollectionHandler as ArangoCollectionHandler ;
use ArangoDBClient\Connection as ArangoConnection ;
use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions ;
use ArangoDBClient\Cursor;
use ArangoDBClient\DocumentHandler as ArangoDocumentHandler ;
use ArangoDBClient\Document as ArangoDocument ;
use ArangoDBClient\ConnectException as ArangoConnectException ;
use ArangoDBClient\Exception;
use ArangoDBClient\Exception as ArangoException ;
use ArangoDBClient\ClientException as ArangoClientException ;
use ArangoDBClient\ServerException as ArangoServerException ;
use ArangoDBClient\Statement as ArangoStatement ;
use ArangoDBClient\UpdatePolicy as ArangoUpdatePolicy ;

use Psr\Log\LoggerInterface ;

class Arango
{
    /**
     * Arango constructor.
     * @param $settings
     * @param ?LoggerInterface $logger
     * @throws ArangoException
     */
    public function __construct( $settings , ?LoggerInterface $logger  = NULL )
    {
        $this->logger = $logger ;

        $connectionOptions =
        [
            ArangoConnectionOptions::OPTION_DATABASE      => $settings['database']    , // database name
            ArangoConnectionOptions::OPTION_ENDPOINT      => $settings['endpoint']    , // server endpoint to connect to
            ArangoConnectionOptions::OPTION_AUTH_TYPE     => $settings['auth_type']   , // authorization type to use (currently supported: 'Basic')
            ArangoConnectionOptions::OPTION_AUTH_USER     => $settings['auth_user']   , // user for basic authorization
            ArangoConnectionOptions::OPTION_AUTH_PASSWD   => $settings['auth_passwd'] , // password for basic authorization
            ArangoConnectionOptions::OPTION_CONNECTION    => $settings['connection']  , // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ArangoConnectionOptions::OPTION_TIMEOUT       => $settings['timeout']     , // connect timeout in seconds
            ArangoConnectionOptions::OPTION_RECONNECT     => true                     , // whether or not to reconnect when a keep-alive connection has timed out on server
            ArangoConnectionOptions::OPTION_CREATE        => true                     , // optionally create new collections when inserting documents
            ArangoConnectionOptions::OPTION_UPDATE_POLICY => ArangoUpdatePolicy::LAST   // optionally create new collections when inserting documents
        ];

        if( array_key_exists( 'debug' , $settings ) && $settings['debug'] == TRUE )
        {
            ArangoException::enableLogging() ;
        }

        try
        {
            $this->connection        = new ArangoConnection( $connectionOptions ) ;
            $this->collection        = new ArangoCollection() ;
            $this->collectionHandler = new ArangoCollectionHandler( $this->connection ) ;
            $this->document          = new ArangoDocument() ;
            $this->documentHandler   = new ArangoDocumentHandler( $this->connection ) ;
            $this->statement         = new ArangoStatement( $this->connection , [] ) ;

            if( !$this->collectionHandler->has('rateLimit') )
            {
                $this->collection->setName( 'rateLimit' ) ;
                $this->collectionHandler->create( $this->collection , [ 'isVolatile' => true ] ) ;
            }
        }
        catch( ArangoConnectException $e )
        {
            $this->error( $this. ' Connection error: ' . $e->getMessage() . PHP_EOL ) ;
        }
        catch( ArangoClientException $e )
        {
            $this->error( $this. ' Client error: ' . $e->getMessage() . PHP_EOL ) ;
        }
        catch( ArangoServerException $e )
        {
            $this->error( $this.  'Server error: ' . $e->getServerCode() . ': ' . $e->getServerMessage() . ' - ' . $e->getMessage() . PHP_EOL ) ;
        }

        if( array_key_exists( 'batchSize' , $settings ) )
        {
            $this->batchSize = (int) $settings['batchSize'] ;
        }
    }

    /**
     * @var int
     */
    private int $batchSize = 10000 ;

    /**
     * @var ArangoCollection
     */
    private ArangoCollection $collection ;

    /**
     * @var ArangoCollectionHandler
     */
    private ArangoCollectionHandler $collectionHandler ;

    /**
     * @var ArangoConnection
     */
    private ArangoConnection $connection ;

    /**
     * @var Cursor
     */
    private Cursor $cursor ;

    /**
     * @var array
     */
    private array $data ;

    /**
     * @var ArangoDocument
     */
    private ArangoDocument $document ;

    /**
     * @var ArangoDocumentHandler
     */
    private ArangoDocumentHandler $documentHandler ;

    /**
     * @var mixed|LoggerInterface
     */
    protected ?LoggerInterface $logger ;

    /**
     * @var ArangoStatement
     */
    private ArangoStatement $statement ;

    /**
     * Creates a new collection if not exist.
     * @param string $name The name of the new collection
     * @return bool Returns TRUE if the new collection is created.
     */
    public function collectionCreate( string $name ) :bool
    {
        try
        {
            if( !$this->collectionHandler->has( $name ) )
            {
                $collection = new ArangoCollection( $name ) ;
                $this->collectionHandler->create( $collection ) ;
                return TRUE ;
            }
        }
        catch( ArangoException $e )
        {
            //
        }
        return FALSE ;
    }

    /**
     * Drop a collection if exist.
     * @param string $name The name of the new collection
     * @return bool Returns TRUE if the new collection is dropped.
     */
    public function collectionDrop( string $name ) :bool
    {
        try
        {
            if( $this->collectionHandler->has( $name ) )
            {
                $this->collectionHandler->drop( $name ) ;
                return TRUE ;
            }
        }
        catch( ArangoException $e )
        {
            //
        }
        return FALSE ;
    }

    /**
     * Check if collection exists
     * @param string $name The name of the collection to find.
     * @return bool
     */
    public function collectionExists( string $name ) :bool
    {
        try
        {
            return $this->collectionHandler->has( $name ) ;
        }
        catch( ArangoException $e )
        {
            return FALSE ;
        }
    }
    /**
     * Rename a collection if exist.
     * @param string $oldName The old name of the collection
     * @param string $name The new name of the collection
     * @return bool Returns TRUE if the collection is renamed.
     * @throws Exception
     */
    public function collectionRename( string $oldName , string $name ) :bool
    {
        if( $this->collectionHandler->has( $oldName ) )
        {
            return $this->collectionHandler->rename( $oldName , $name);
        }
        return FALSE ;
    }

    /**
     * Execute the query request.
     * @throws ArangoException
     */
    public function execute()
    {
        $this->statement = new ArangoStatement
        (
            $this->connection ,
            $this->data
        ) ;

        $this->cursor = $this->statement->execute() ;
    }

    /**
     * @return mixed
     */
    public function getAll(): array
    {
        $result = $this->getResult() ;

        if( $result && is_array( $result ) )
        {
            foreach( $result as $key => $value )
            {
                $result[$key] = (object) $value ;
            }
        }
        else
        {
            $result = [] ;
        }

        return $result ;
    }

    /**
     * @return mixed
     */
    public function getFirstResult()
    {
        $result = $this->getResult() ;
        return $result ? $this->getResult()[0] : null ;
    }

    /**
     * @return Cursor
     */
    public function getCursor() : Cursor
    {
        return $this->cursor ;
    }

    /**
     * @return array
     */
    public function getExtra() : array
    {
        return $this->cursor->getExtra() ;
    }

    /**
     * @return int
     */
    public function getFoundRows() : int
    {
        return $this->cursor->getFullCount() ;
    }

    /**
     * @return mixed
     */
    public function getObject(): ?object
    {
        $first = $this->getFirstResult() ;
        return $first ? (object) $first : null ;
    }

    /**
     * @return mixed
     */
    public function getResult(): ?array
    {
        $result = $this->cursor->getMetadata()['result'] ;
        return ( is_array( $result ) && count( $result ) > 0 )
               ? $result
               : null ;
    }

    /**
     * @return mixed|null
     */
    public function lastInsertId()
    {
        return $this->getFirstResult() ;
    }

    /**
     * @param array $data
     */
    public function prepare( $data = [] ) : void
    {
        $data['batchSize'] = $this->batchSize ;
        $this->data = $data ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() : string
    {
        return '[' . get_class($this) . ']' ;
    }

    protected function error( $message )
    {
        if( $this->logger )
        {
            $this->logger->error( $message ) ;
        }
    }
}
