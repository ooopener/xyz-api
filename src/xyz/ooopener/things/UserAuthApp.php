<?php

namespace xyz\ooopener\things;

/**
 * An user auth application.
 */
class UserAuthApp
{
    /**
     * Creates a new UserAuthApp instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The user id.
     */
    public string $user_id ;

    /**
     * The client id.
     */
    public string $client_id ;

    /**
     * The scope.
     * @var string
     */
    public $scope ;

    /**
     * Date of creation of the resource.
     */
    public $created ;

    /**
     * Date on which the resource was changed.
     */
    public $modified ;

    /**
     * The 'date' filter constant.
     */
    const FILTER_DATE = 'date' ;

    /**
     * The default filter constant (NULL).
     */
    const FILTER_DEFAULT = NULL ;

    /**
     * The 'url' filter constant.
     */
    const FILTER_URL = 'url' ;


    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'user_id'    => self::FILTER_DEFAULT ,
        'client_id'  => self::FILTER_DEFAULT ,
        'scope'      => self::FILTER_URL ,
        'created'    => self::FILTER_DATE ,
        'modified'   => self::FILTER_DATE
    ];

    /**
     * Creates a new UserAuthApp instance.
     * @param object $init An object to initialize a new UserAuthApp instance.
     * @param $iso8601
     *
     * @return UserAuthApp $application
     */
    public static function create( $init = NULL , $iso8601 = NULL )
    {
        $userAuthApp = NULL ;

        if( isset( $init ) )
        {
            $userAuthApp = new UserAuthApp() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DATE :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $userAuthApp->{ $property } = isset( $iso8601 )
                                ? $iso8601->formatTimeToISO8601($init->{ $property })
                                : $init->{ $property } ;
                        }
                        break ;
                    }
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $userAuthApp->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $userAuthApp ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return "[" . get_class($this) . "]" ;
    }
}

