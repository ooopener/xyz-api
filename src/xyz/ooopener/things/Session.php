<?php

namespace xyz\ooopener\things;

use JsonSerializable;

use core\Objects ;

/**
 * An Session code.
 */
class Session extends Thing
{
    /**
     * Creates a new Session instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The app.
     * @var string
     */
    public $app ;

    /**
     * The type of token.
     * @var string
     */
    public $type ;

    /**
     * The user uuid.
     * @var string
     */
    public $user ;

    /**
     * The client id.
     * @var string
     */
    public $client_id ;

    /**
     * The token id.
     * @var string
     */
    public $token_id ;

    /**
     * The refresh.
     * @var string
     */
    public $refresh ;

    /**
     * The ip of the user
     * @var string
     */
    public $ip ;

    /**
     * The user agent
     * @var string
     */
    public $agent ;

    /**
     * Date of refresh expired.
     */
    public $expired ;

    /**
     * Date of logout.
     */
    public $logout ;

    /**
     * If refresh or token is revoked
     * @var bool
     */
    public $revoked ;

    ///////////////////////////

    const FILTER_USER   = 'user' ;
    const FILTER_CLIENT = 'client_id' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'         ,
        'app'        ,
        'type'       ,
        'user'       ,
        'client_id'  ,
        'token_id'   ,
        'refresh'    ,
        'ip'         ,
        'agent'      ,
        'created'    ,
        'expired'    ,
        'logout'     ,
        'revoked'
    ] ;

    /**
     * A utility function for implementing the __toString() method.
     * return @string
     */
    public function formatToString()
    {
        $source = '[' ;

        $arguments = func_get_args() ;

        if ( count($arguments) > 0 )
        {
            $className = array_shift( $arguments ) ;

            $source .= ( isset($className) && !empty($className) )
                ? $className
                : get_class($this) ;

            if ( count( $arguments ) > 0 )
            {
                foreach( $arguments as $arg  )
                {
                    if ( is_string($arg) && property_exists( $this , $arg ) && !empty($this->{ $arg }) )
                    {
                        $source .= ' ' . $arg . ':'
                            . ( is_null($this->{ $arg } )
                                ? "null"
                                : (string) $this->{ $arg } )
                        ;
                    }
                }
            }
        }

        $source .= ']' ;

        return $source ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}

