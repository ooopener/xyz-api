<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Mark and inscription .
 */
class ConceptualObjectMark extends Thing
{
    /**
     * Creates a new ConceptualObjectMark instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_POSITION     = 'position' ;

    ///////////////////////////

    /**
     * The language of the mark
     */
    public $language ;

    /**
     *
     */
    public $position ;

    /**
     * All processes, methods, and techniques used in the creation of the mark.
     * Examples : carved, thrown, printed, embroidered
     */
    public $technique ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'technique'   => $this->technique,
            'name'        => $this->name,
            'description' => $this->description
        ];
        return Objects::compress( $object ) ;
    }
}


