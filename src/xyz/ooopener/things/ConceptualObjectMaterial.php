<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Material and technique information supports Security, Access, and an Historic archive.
 * It can be of primary importance in identifying high value, and therefore high risk, objects.
 * It is also a key information area for the research of man made objects.
 */
class ConceptualObjectMaterial extends Thing
{
    /**
     * Creates a new ConceptualObjectMaterial instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The Materials used in the creation, decoration, and any subsequent adaptations of the object.
     * Examples : Gold, chalk, oil, tempera
     */
    public $material ;

    /**
     * All processes, methods, and techniques used in the creation of the object.
     * Examples : carved, thrown, printed, embroidered
     */
    public $technique ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'material'    => $this->material,
            'technique'   => $this->technique,
            'name'        => $this->name,
            'description' => $this->description
        ];
        return Objects::compress( $object ) ;
    }
}


