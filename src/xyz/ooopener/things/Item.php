<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An Item object.
 */
class Item extends Thing
{
    /**
     * Creates a new Item instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * Indicates if the item is active.
     */
    public $active ;

    /**
     * The optional background color component value.
     */
    public $bgcolor ;

    /**
     * The optional color component value.
     */
    public $color ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'url'           => $this->url,
            'image'         => $this->image,
            'name'          => $this->name,
            'alternateName' => $this->alternateName,
            'description'   => $this->description,
            'created'       => $this->created,
            'modified'      => $this->modified,
            'active'        => $this->active,
            'color'         => $this->color,
            'bgcolor'       => $this->bgcolor
        ];

        return Objects::compress( $object ) ;
    }
}


