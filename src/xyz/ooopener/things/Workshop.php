<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An Workshop object.
 */
class Workshop extends Item
{
    /**
     * Creates a new Workshop instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $breeding ;

    public $production ;

    public $providers ;

    public $water ;

    ///////////////////////////


    ///////////////////////////

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'identifier'    => $this->identifier,
            'url'           => $this->url,
            'name'          => $this->name,
            'created'       => $this->created,
            'modified'      => $this->modified,
            'active'        => $this->active,
            'breeding'      => $this->breeding,
            'production'    => $this->production,
            'water'         => $this->water,
            'providers'     => $this->providers
        ];

        return Objects::compress( $object ) ;
    }
}
