<?php

namespace xyz\ooopener\things;

class Veterinarian extends Thing
{
    public function __construct( object $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_LIVESTOCKS          = 'livestocks' ;
    const FILTER_MEDICAL_SPECIALTIES = 'medicalSpecialties' ;
    const FILTER_AUTHORITY           = 'authority' ;

    ///////////////////////////
}
