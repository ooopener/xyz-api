<?php

namespace xyz\ooopener\things;

use core\Objects;

class Livestock extends Thing
{
    /**
     * Creates a new Livestock instance.
     * @param object|NULL $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $medicalLaboratories ;

    public $organization ;

    public $numbers ;

    public $technicians ;

    public $veterinarians ;

    public $workshops ;

    ///////////////////////////

    const FILTER_MEDICAL_LABORATORIES  = 'medicalLaboratories' ;
    const FILTER_ORGANIZATION          = 'organization' ;
    const FILTER_TECHNICIANS           = 'technicians' ;
    const FILTER_VETERINARIANS         = 'veterinarians' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'           ,
        'url'          ,
        'created'      ,
        'modified'     ,
        'organization' ,
        'numbers'      ,
        'workshops'
    ];

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}
