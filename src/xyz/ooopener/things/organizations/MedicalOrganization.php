<?php

namespace xyz\ooopener\things\organizations ;

use core\Objects;

use xyz\ooopener\things\Organization;

/**
 * Entities that have a somewhat fixed, physical extension.
 * @see http://schema.org/MedicalOrganization
 */
class MedicalOrganization extends Organization
{
    /**
     * Creates a new MedicalOrganization instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_MEDICAL_SPECIALTIES = 'medicalSpecialties' ;

    ///////////////////////////

    /**
     * The medical specialties of the organization
     * @var array
     */
    public $medicalSpecialties ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'identifier'  => $this->identifier,
            'name'        => $this->name,
            'legalName'   => $this->legalName,
            'url'         => $this->url,
            'active'      => $this->active,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'image'       => $this->image,
            'licence'     => $this->licence,
            'description' => $this->description,
            'publisher'   => $this->publisher,

            'about'                     => $this->about,
            'additional'                => $this->additional,
            'additionalType'            => $this->additionalType,
            'alternateName'             => $this->alternateName,
            'address'                   => $this->address,
            'areServed'                 => $this->areaServed,
            'employees'                 => $this->employees,
            'events'                    => $this->events,
            'images'                    => $this->images,
            'keywords'                  => $this->keywords,
            'location'                  => $this->location,
            'logo'                      => $this->logo,
            'members'                   => $this->members,
            'photos'                    => $this->photos,
            'providers'                 => $this->providers,
            'subOrganization'           => $this->subOrganization,
            'sponsor'                   => $this->sponsor,

            'medicalSpecialties'        => $this->medicalSpecialties,
        ];

        return Objects::compress( $object ) ;
    }
}
