<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An event happening at a certain time and location, such as a concert, lecture, or festival. Repeated events may be structured as separate Event objects.
 */
class Event extends Thing
{
    /**
     * Creates a new Event instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * Indicates if the thing is active.
     */
    public $active ;

    /**
     * A person or organization attending the event.
     */
    public $attendee ;

    public $capacity ;

    /**
     * The person or organization who wrote a composition, or who is the composer of a work performed at some event.
     */
    public $composer ;

    /**
     * A secondary contributor to the CreativeWork or Event.
     */
    public $contributor ;

    /**
     * The end date and time of the event or item (in ISO 8601 date format).
     * @var string
     */
    public $endDate ;

    /**
     * An eventStatus of an event represents its status; particularly useful when an event is cancelled or rescheduled.
     * @var string
     */
    public $eventStatus ;

    /**
     * The keywords collection of this object.
     * @var array
     */
    public $keywords ;

    /**
     * The location of the event, organization or action.
     * @var PostalAddress or Place.
     */
    public $location ;

    public $numAttendee ;

    /**
     * A collection of Offer items to provide this item - for example, an offer to sell a product, rent the DVD of a movie, or give away tickets to an event.
     * @var array
     */
    public $offers ;

    /**
     * An organizer of an Event.
     * @var array|Null|Person|Organization
     */
    public $organizer ;

    /**
     * Photographs of this place (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    public $remainingAttendee ;

    /**
     * The remarks about the resource.
     * @var array
     */
    public $remarks ;

    /**
     * The start date and time of the event or item (in ISO 8601 date format).
     * @var string
     */
    public $startDate ;

    /**
     * subEvent	An Event that is part of this event. For example, a conference event includes many presentations, each of which is a subEvent of the conference. Supersedes subEvents.
     * @var array or Event
     */
    public $subEvent ;

    /**
     * A work featured in some event, e.g. exhibited in an ExhibitionEvent.
     */
    public $workFeatured ;

    /**
     * A work performed in some event, for example a play performed in a TheaterEvent.
     */
    public $workPerformed ;

    ///////////////////////////

    const FILTER_ALTERNATIVE_HEADLINE = 'alternativeHeadline' ;
    const FILTER_HEADLINE             = 'headline' ;
    const FILTER_KEYWORDS             = 'keywords' ;
    const FILTER_LOCATION             = 'location' ;
    const FILTER_OFFERS               = 'offers' ;
    const FILTER_ORGANIZER            = 'organizer' ;
    const FILTER_PHOTOS               = 'photos' ;
    const FILTER_REMARKS              = 'remarks' ;
    const FILTER_STATUS               = 'eventStatus' ;
    const FILTER_SUB_EVENT            = 'subEvent' ;
    const FILTER_SUPER_EVENT          = 'superEvent' ;
    const FILTER_WORK_FEATURED        = 'workFeatured' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'             ,
        'name'           ,
        'url'            ,
        'image'          ,
        'startDate'      ,
        'endDate'        ,
        'eventStatus'    ,
        'additionalType' ,
        'active'         ,
        'created'        ,
        'modified'       ,
        'alternateName'  ,
        'remarks'        ,
        'description'    ,
        'location'       ,
        'keywords'       ,
        'photos'         ,
        'subEvent'       ,
        'contributor'    ,
        'offers'         ,
        'organizer'      ,
        'workFeatured'   ,
        'workPerformed'
    ];

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
    * Returns a String representation of the object.
    * @return string A string representation of the object.
    */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' , 'startDate' , 'endDate' ) ;
    }
}


