<?php

namespace xyz\ooopener\things;

class Observation extends Thing
{
    public function __construct( object $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_AUTHORITY   = 'authority' ;
    const FILTER_FIRST_OWNER = 'firstOwner' ;
    const FILTER_OWNER       = 'owner' ;

    ///////////////////////////

    public $livestock ;

    public $status ;

    public $type ;

    public $veterinarian ;

    public $workshop ;
}
