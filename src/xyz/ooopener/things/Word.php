<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An word object.
 */
class Word extends Item
{
    /**
     * Creates a new Word instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The enumeration of all langs used in the create static method.
     */
    public static $languages = [ 'fr' , 'en' , 'it' , 'es' , 'de' ]  ;

    public $path ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object $init
     * @param string $lang
     * @param string $url
     * @param $iso8601
     *
     * @return Word
     */
    public static function create
    (
        $init = NULL , $lang = NULL , $url = NULL , $iso8601 = NULL
    )
    {
        $word = NULL ;

        if( $init )
        {
            $word = new Word() ;

            if( property_exists( $init , 'id' ) )
            {
                $word->id = (int) $init->id ;
            }

            if( isset( $url ) )
            {
                $word->url = $url ;
            }

            if( is_null( $lang ) )
            {
                foreach ( self::$languages as $lan )
                {
                    if( property_exists( $init , $lan ) )
                    {
                        $word->name[ $lan ] = $init->{ $lan } ;
                    }
                }
            }
            else if( property_exists( $init , $lang ) )
            {
                $word->name = $init->{ $lang } ;
            }

            if( property_exists( $init , 'alternateName' ) )
            {
                $word->alternateName = $init->alternateName ;
            }

            if( property_exists( $init , 'created' ) )
            {
                $word->created  = $iso8601
                                ? $iso8601->formatTimeToISO8601($init->created)
                                : $init->created ;
            }

            if( property_exists( $init , 'modified' ) )
            {
                $word->modified = $iso8601
                                ? $iso8601->formatTimeToISO8601($init->modified)
                                : $init->modified ;
            }
        }

        return $word ;
    }

    public static function createBis
    (
        $init = NULL , $lang = NULL , $url = NULL , $iso8601 = NULL
    )
    {
        $word = NULL ;

        if( $init )
        {
            $word = new Word() ;

            /*if( property_exists( $init , 'id' ) )
            {
                $word->id = (int) $init->id ;
            }*/

            /*if( isset( $url ) )
            {
                $word->url = $url ;
            }*/

            if( is_null( $lang ) )
            {
                foreach ( self::$languages as $lan )
                {
                    if( property_exists( $init , $lan ) )
                    {
                        $word->name[ $lan ] = $init->{ $lan } ;
                    }
                }
            }
            else if( property_exists( $init , $lang ) )
            {
                $word->name = $init->{ $lang } ;
            }

            if( property_exists( $init , 'alternateName' ) )
            {
                $word->alternateName = $init->alternateName ;
            }

            if( property_exists( $init , 'created' ) )
            {
                /*$word->created  = $iso8601
                    ? $iso8601->formatTimeToISO8601($init->created)
                    : $init->created ;*/
                $date = new \DateTime( $init->created ) ;
                $word->created = $date->format( "Y-m-d\TH:i:s.v\Z" ) ;
            }

            if( property_exists( $init , 'modified' ) )
            {
                /*$word->modified = $iso8601
                    ? $iso8601->formatTimeToISO8601($init->modified)
                    : $init->modified ;*/
                $date = new \DateTime( $init->modified ) ;
                $word->modified = $date->format( "Y-m-d\TH:i:s.v\Z" ) ;
            }
        }

        return $word ;
    }

    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'path'          => $this->path,
            'url'           => $this->url,
            'image'         => $this->image,
            'name'          => $this->name,
            'alternateName' => $this->alternateName,
            'description'   => $this->description,
            'created'       => $this->created,
            'modified'      => $this->modified,
            'active'        => $this->active,
            'color'         => $this->color,
            'bgcolor'       => $this->bgcolor
        ];

        return Objects::compress( $object ) ;
    }
}


