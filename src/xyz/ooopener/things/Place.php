<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Entities that have a somewhat fixed, physical extension.
 * @see http://schema.org/Place
 */
class Place extends Thing
{
    /**
     * Creates a new Place instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_ADDITIONAL                  = 'additional' ;
    const FILTER_ADDRESS                     = 'address' ;
    const FILTER_BRAND                       = 'brand' ;
    const FILTER_EVENTS                      = 'events' ;
    const FILTER_GEO                         = 'geo' ;
    const FILTER_KEYWORDS                    = 'keywords' ;
    const FILTER_CONCEPTUAL_OBJECTS          = 'conceptualObjects' ;
    const FILTER_OFFERS                      = 'offers' ;
    const FILTER_OPENING_HOURS_SPECIFICATION = 'openingHoursSpecification' ;
    const FILTER_PHOTOS                      = 'photos' ;
    const FILTER_SERVICES                    = 'services' ;
    const FILTER_CONTAINS_PLACE              = 'containsPlace' ;
    const FILTER_CONTAINED_IN_PLACE          = 'containedInPlace' ;
    const FILTER_WEBSITES                    = 'websites' ;

    const FILTER_SLOGAN = 'slogan' ;

    ///////////////////////////

    /**
     * Indicates if the thing is active.
     */
    public $active ;

    /**
     * The additional description of the place.
     * Note : this property is a custom attributeof the original Place class defined in http://schema.org/Place.
     * @var string
     */
    public $additional ;

    /**
     * Physical address of the item (PostalAddress or any object to describe it).
     */
    public $address ;

    public $capacity ;

    /**
     * Upcoming or past events associated with this place or organization (legacy spelling; see singular form, event).
     * @var array
     */
    public $events ;

    /**
     * The geo coordinates of the place (GeoShape or GeoCoordinates).
     */
    public $geo ;

    /**
     * Photographs of this place (legacy spelling; see singular form, photo).
     * @var array
     */
    public $images ;

    /**
     * The keywords collection of this object.
     * @var array
     */
    public $keywords ;

    public $numAttendee ;

    /**
     * A collection of Offer items to provide this item - for example, an offer to sell a product, rent the DVD of a movie, or give away tickets to an event.
     * @var array
     */
    public $offers ;

    /**
     * The opening hours of a certain place.
     * @var array
     */
    public $openingHoursSpecification ;

    /**
     * Photographs of this place (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    public $remainingAttendee ;

    /**
     * The services provided by a place.
     * @var array
     */
    public $services ;

    /**
     * The basic containment relation between a place and another that it contains.
     * @var array or Place
     */
    public $containsPlace ;

    /**
     * The collection of all websites of this place.
     */
    public $websites ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'name'        => $this->name,
            'url'         => $this->url,
            'active'      => $this->active,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'image'       => $this->image,
            'licence'     => $this->licence,
            'description' => $this->description,
            'publisher'   => $this->publisher,

            'about'                     => $this->about,
            'additional'                => $this->additional,
            'additionalType'            => $this->additionalType,
            'address'                   => $this->address,
            'alternateName'             => $this->alternateName,
            'events'                    => $this->events,
            'geo'                       => $this->geo,
            'images'                    => $this->images,
            'keywords'                  => $this->keywords,
            'offers'                    => $this->offers,
            'openingHoursSpecification' => $this->openingHoursSpecification,
            'photos'                    => $this->photos,
            'services'                  => $this->services,
            'containsPlace'             => $this->containsPlace,
            'websites'                  => $this->websites
        ];

        return Objects::compress( $object ) ;
    }
}


