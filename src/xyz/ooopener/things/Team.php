<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Team.
 */
class Team extends Thing
{
    /**
     * Creates a new Team instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_COLOR        = 'color' ;
    const FILTER_PERMISSIONS  = 'permissions' ;

    ///////////////////////////

    const ADMIN       = 'admin' ;
    const CONTRIBUTOR = 'contributor' ;
    const DEVELOPER   = 'developer' ;
    const GUEST       = 'guest' ;
    const SUPER_ADMIN = 'superadmin' ;

    const DEFAULT =
    [
        self::ADMIN ,
        self::CONTRIBUTOR ,
        self::DEVELOPER ,
        self::GUEST ,
        self::SUPER_ADMIN ,
    ];

    ///////////////////////////
    ///
    ///

    /**
     * Indicates if the thing is active.
     */
    public $active ;

    /**
     * The color.
     * @var string
     */
    public $color ;

    /**
     * The permissions of the team.
     */
    public $permissions ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'active'        => $this->active,
            'name'          => $this->name,
            'alternateName' => $this->alternateName,
            'color'         => $this->color,
            'description'   => $this->description,
            'permissions'   => $this->permissions,
            'created'       => $this->created,
            'modified'      => $this->modified
        ];

        return Objects::compress( $object ) ;
    }
}
