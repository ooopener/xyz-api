<?php

namespace xyz\ooopener\things;

use core\Objects;

class MedicalLaboratory extends Thing
{
    /**
     * Creates a new Livestock instance.
     * @param object|NULL $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $identifier ;

    public $medicalSpecialties ;

    public $organization ;

    ///////////////////////////

    const FILTER_LIVESTOCKS          = 'livestocks' ;
    const FILTER_MEDICAL_SPECIALTIES = 'medicalSpecialties' ;
    const FILTER_ORGANIZATION        = 'organization' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'           ,
        'url'          ,
        'created'      ,
        'modified'     ,
        'identifier'   ,
        'organization' ,
        'medicalSpecialties'
    ];

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}
