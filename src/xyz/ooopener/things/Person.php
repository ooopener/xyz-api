<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * A person (alive, dead, undead, or fictional).
 */
class Person extends Thing
{
    /**
     * Creates a new Person instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The active flag.
     * @var boolean
     */
    public $active ;

    /**
     * Physical address of the item (PostalAddress or any object to describe it).
     */
    public $address ;

    /**
     * The Date of birth.
     */
    public $birthDate ;

    /**
     * The place where the person was born.
     */
    public $birthPlace ;

    /**
     * The Date of death.
     */
    public $deathDate ;

    /**
     * The place where the person died.
     */
    public $deathPlace ;

    /**
     * The email of the user.
     * @var string
     */
    public $email ;

    /**
     * The family name of the user.
     * @var string
     */
    public $familyName ;

    /**
     * The gender of the user.
     */
    public $gender ;

    /**
     * The given name of the user (first name).
     * @var string
     */
    public $givenName ;

    /**
     * The keywords collection of this object.
     * @var array
     */
    public $keywords ;

    /**
     * An Organization to which this Person or Organization belongs.
     */
    public $memberOf ;

    /**
     * Nationality of the person.
     */
    public $nationality ;

    /**
     * The telephone collection of this object.
     * @var array
     */
    public $telephone ;

    /**
     * Photographs of this person (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    /**
     * The remarks about the resource.
     * @var array
     */
    public $remarks ;

    /**
     * The job(s) of the person (for example, Painter, etc.).
     * @var null|array|string
     */
    public $job ;

    ///////////////////////////

    const FILTER_ADDRESS     = 'address' ;
    const FILTER_BRAND       = 'brand' ;
    const FILTER_GENDER      = 'gender' ;
    const FILTER_KEYWORDS    = 'keywords' ;
    const FILTER_TELEPHONE   = 'telephone' ;
    const FILTER_PHOTOS      = 'photos' ;
    const FILTER_REMARKS     = 'remarks' ;
    const FILTER_JOB         = 'job' ;
    const FILTER_OWNS        = 'owns' ;
    const FILTER_MEMBER_OF   = 'memberOf' ;
    const FILTER_WORKS_FOR   = 'worksFor' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'            , 'name'        ,
        'alternateName' , 'url'         ,
        'image'         , 'active'      ,
        'created'       , 'modified'    ,
        'job'           , 'gender'      ,
        'givenName'     , 'familyName'  ,
        'description'   , 'remarks'     ,
        'address'       , 'email'       ,
        'birthDate'     , 'birthPlace'  ,
        'deathDate'     , 'deathPlace'  ,
        'nationality'   , 'memberOf'    ,
        'photos'        , 'keywords'    ,
        'telephone'
    ];

    /**
     * Returns the full name of the current person.
     */
    public function getFullName() :?string
    {
        if( $this->name )
        {
            return $this->name ;
        }
        else if( $this->givenName || $this->familyName )
        {
            $name = '' ;

            if( strlen($this->givenName) > 0 )
            {
                $name .= $this->givenName ;
            }

            if( strlen($this->familyName) > 0 )
            {
                if( strlen($name) > 0 )
                {
                    $name .= ' ' ;
                }
                $name .= $this->familyName ;
            }

            return $name ;
        }
        else if( $this->alternateName )
        {
            return $this->alternateName ;
        }

        return NULL ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return $this->formatToString( NULL , 'id', 'name', 'givenName', 'familyName' ) ;
    }
}


