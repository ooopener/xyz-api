<?php

namespace xyz\ooopener\things;

use core\Objects;
use system\date\ISO8601;

/**
 * An event happening at a certain time and location, such as a concert, lecture, or festival. Repeated events may be structured as separate Event objects.
 */
class Offer extends Thing
{
    /**
     * Creates a new Offer instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * @var string
     */
    public $category ;

    /**
     * The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
     * @var string
     */
    public $price ;

    /**
     * The currency (in 3-letter ISO 4217 format) of the price or a price component, when attached to PriceSpecification and its subtypes.
     * @var string
     */
    public $priceCurrency ;

    /**
     * The date when the item becomes valid (DateTime).
     */
    public $validFrom ;

    /**
     * The end of the validity of offer, price specification, or opening hours data (DateTime).
     */
    public $validThrough ;

    // ---------

    /**
     * @private
     */
    const PROPERTIES =
    [
        'id'            => Thing::FILTER_INT ,
        'name'          => Thing::FILTER_DEFAULT ,
        'description'   => Thing::FILTER_DEFAULT ,
        'category'      => Thing::FILTER_DEFAULT ,
        'price'         => Thing::FILTER_FLOAT ,
        'priceCurrency' => Thing::FILTER_DEFAULT ,
        'validFrom'     => Thing::FILTER_DATETIME ,
        'validThrough'  => Thing::FILTER_DATETIME,
        'created'       => Thing::FILTER_DATETIME,
        'modified'      => Thing::FILTER_DATETIME
    ] ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object|array $init
     * @param $iso8601
     *
     * @return Offer
     */
    public static function create( $init = NULL , ISO8601 $iso8601 = NULL )
    {
        $item = NULL ;

        if( isset( $init ) )
        {
            $item = new Offer() ;

            $isArray  = is_array($init)  ;
            $isObject = is_object($init) ;

            foreach ( self::PROPERTIES as $key => $filter )
            {
                if( $isArray && array_key_exists( $key , $init ) )
                {
                    $item->{ $key } = $init[ $key ] ;
                }
                else if( $isObject && property_exists( $init , $key ) )
                {
                    $item->{ $key } = $init->{ $key } ;
                }

                if( property_exists( $item , $key ) )
                {
                    if( $filter == Thing::FILTER_FLOAT )
                    {
                        if( is_numeric( $item->{ $key } ) )
                        {
                            $item->{ $key } = floatval( $item->{ $key } ) ;
                        }
                    }
                    else if( $filter == Thing::FILTER_INT )
                    {
                        $item->{ $key } = (int) $item->{ $key } ;
                    }
                    else if( $filter == Thing::FILTER_DATETIME )
                    {
                        if( $item->{ $key } != NULL )
                        {
                            if( $iso8601 )
                            {
                                $item->{ $key } = $iso8601->formatTimeToISO8601( $item->{ $key } );
                            }
                        }
                    }
                }
            }
        }

        return $item ;
    }

    // ---------

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'created'       => $this->created,
            'modified'      => $this->modified,
            'name'          => $this->name,
            'image'         => $this->image,
            'licence'       => $this->licence,
            'description'   => $this->description,
            'publisher'     => $this->publisher,
            'url'           => $this->url,

            'category'      => $this->category,
            'price'         => $this->price,
            'priceCurrency' => $this->priceCurrency,
            'validFrom'     => $this->validFrom,
            'validThrough'  => $this->validThrough
        ];
        return Objects::compress( $object ) ;
    }
}


