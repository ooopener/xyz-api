<?php

namespace xyz\ooopener\things ;

use DateTime ;

/**
 * An authorization code.
 */
class AuthorizationCode
{
    /**
     * Creates a new AuthorizationCode instance.
     * @param object|null $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( object $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The id.
     * @var integer
     */
    public int $id;

    /**
     * The user id.
     * @var string
     */
    public string $user_id;

    /**
     * The client id.
     * @var string
     */
    public string $client_id;

    /**
     * The code.
     * @var string
     */
    public string $code;

    /**
     * @var string
     */
    public string $ip;

    /**
     * Date of creation of the resource.
     */
    public DateTime $created ;

    /**
     * Date of the expiration of the resource.
     */
    public DateTime $expired ;

    /**
     * The 'date' filter constant.
     */
    const FILTER_DATE = 'date' ;

    /**
     * The default filter constant (NULL).
     */
    const FILTER_DEFAULT = NULL ;


    /**
     * The enumeration of all object properties.
     */
    public static array $properties =
    [
        'id'        => self::FILTER_DEFAULT ,
        'user_id'   => self::FILTER_DEFAULT ,
        'client_id' => self::FILTER_DEFAULT ,
        'code'      => self::FILTER_DEFAULT ,
        'ip'        => self::FILTER_DEFAULT ,
        'created'   => self::FILTER_DATE ,
        'expired'   => self::FILTER_DATE
    ];

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return "[" . get_class($this) . "]" ;
    }
}

