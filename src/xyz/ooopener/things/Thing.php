<?php

namespace xyz\ooopener\things;

use JsonSerializable;

use core\Objects;

/**
 * The most generic type of item.
 */
class Thing implements JsonSerializable
{
    /**
     * Creates a new Thing instance.
     *
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    // ------------ filters

    /**
     * The 'about' filter constant
     */
    const FILTER_ABOUT = 'about' ;

    /**
     * The 'audio' filter constant.
     */
    const FILTER_AUDIO = 'audio' ;

    /**
     * The 'alternateName' filter constant.
     */
    const FILTER_ALTERNATE_NAME = 'alternateName' ;

    /**
     * The 'additionalType' filter constant.
     */
    const FILTER_ADDITIONAL_TYPE = 'additionalType' ;

    /**
     * The 'boolean' filter constant.
     */
    const FILTER_BOOL = 'bool' ;

    /**
     * The 'date' filter constant.
     */
    const FILTER_DATE = 'date' ;

    /**
     * The 'dateTime' filter constant.
     */
    const FILTER_DATETIME = 'dateTime' ;

    /**
     * The 'description' filter constant.
     */
    const FILTER_DESCRIPTION = 'description' ;

    /**
     * The default filter constant (NULL).
     */
    const FILTER_DEFAULT = NULL ;

    const FILTER_DISTANCE = 'distance' ;

    /**
     * The 'float' filter constant.
     */
    const FILTER_FLOAT = 'float' ;

    const FILTER_EDGE = 'edge' ;

    const FILTER_EDGE_COUNT = 'edgeCount' ;

    const FILTER_EDGE_SINGLE = 'edgeSingle' ;

    /**
     * The 'id' filter constant.
     */
    const FILTER_ID = 'id' ;

    /**
     * The 'image' filter constant.
     */
    const FILTER_IMAGE = 'image' ;

    /**
     * The 'int' filter constant.
     */
    const FILTER_INT = 'int' ;

    const FILTER_JOIN = 'join' ;

    const FILTER_JOIN_ARRAY = 'join_array' ;

    const FILTER_JOIN_COUNT = 'join_count' ;

    const FILTER_JOIN_MULTIPLE = 'join_multiple' ;

    const FILTER_MEDIA_SOURCE = 'media_source' ;

    const FILTER_MEDIA_THUMBNAIL = 'media_thumbnail' ;

    const FILTER_MEDIA_URL = 'media_url' ;

    /**
    * The 'notes' filter constant.
    */
    const FILTER_NOTES = 'notes' ;

    /**
     * The 'text' filter constant.
     */
    const FILTER_TEXT = 'text' ;

    const FILTER_THESAURUS_IMAGE = 'thesaurus_image' ;

    const FILTER_THESAURUS_URL = 'thesaurus_url' ;

    const FILTER_TRANSLATE = 'translate' ;

    const FILTER_UNIQUE_NAME = 'unique_name' ;

    /**
     * The 'url' filter constant.
     */
    const FILTER_URL = 'url' ;

    const FILTER_URL_API = 'url_api' ;

    /**
     * The 'video' filter constant.
     */
    const FILTER_VIDEO = 'video' ;

    // ------------

    /**
     * The subject matter of the content.
     * @var string
     */
    public $about ;

    /**
     * An alias for the item.
     */
    public $alternateName ;

    /**
     * An additionalType for the item.
     */
    public $additionalType ;

    /**
     * Date of creation of the resource.
     * @var string
     */
    public $created ;

    /**
     * A short description of the item.
     */
    public $description ;

    /**
     * The index of the item.
     */
    public $id ;

    /**
     * The identifier of the item.
     */
    public $identifier ;

    /**
     * The image reference of this ressource.
     * @var string
     */
    public $image ;

    /**
     * A legal document giving official permission to do something with the resource.
     * @var string
     */
    public $licence ;

    /**
     * Date on which the resource was changed.
     */
    public $modified ;

    /**
     * The name of the item.
     */
    public $name ;

    /**
     * @var string The publisher of the resource.
     */
    public $publisher ;

    /**
     * @var string $url The url of the item.
     */
    public $url ;

    /**
     * A utility function for implementing the __toString() method.
     * return @string
     */
    public function formatToString()
    {
        $source = '[' ;

        $arguments = func_get_args() ;

        if ( count($arguments) > 0 )
        {
            $className = array_shift( $arguments ) ;

            $source .= ( isset($className) && !empty($className) )
                ? $className
                : get_class($this) ;

            if ( count( $arguments ) > 0 )
            {
                foreach( $arguments as $arg  )
                {
                    if ( is_string($arg) && property_exists( $this , $arg ) && !empty($this->{ $arg }) )
                    {
                        $source .= ' ' . $arg . ':'
                            . ( is_null($this->{ $arg } )
                                ? "null"
                                : (string) $this->{ $arg } )
                        ;
                    }
                }
            }
        }

        $source .= ']' ;

        return $source ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'             => $this->id,
            'identifier'     => $this->identifier,
            'created'        => $this->created,
            'modified'       => $this->modified,
            'name'           => $this->name,
            'additionalType' => $this->additionalType,
            'alternateName'  => $this->alternateName,
            'image'          => $this->image,
            'licence'        => $this->licence,
            'description'    => $this->description,
            'publisher'      => $this->publisher,
            'url'            => $this->url
        );
        return Objects::compress( $object ) ;
    }

    /**
     * Returns string A string representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString()
    {
        return $this->formatToString( NULL , 'id' ) ;
    }
}


