<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An keyword information.
 */
class Keyword extends Thing
{
    /**
     * Creates a new Keyword instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * A link to the website.
     * @var string
     */
    public $href ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object $init
     *
     * @return Keyword
     */
    public static function create( $init = NULL )
    {
        $item = NULL ;

        if( isset( $init ) )
        {
            $item = new Keyword() ;

            if( property_exists( $init , 'id'  )  )
            {
                $item->id = (int) $init->id ;
            }

            if( property_exists( $init , 'name' ) && !empty($init->name) )
            {
                $item->name = $init->name ;
            }

            if( property_exists( $init , 'href' ) && !empty($init->href)  )
            {
                $item->href = $init->href ;
            }
        }

        return $item ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'   => $this->id,
            'name' => $this->name,
            'href' => $this->href
        ];

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' , 'name' , 'href' ) ;
    }
}


