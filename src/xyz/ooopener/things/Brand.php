<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * A brand is a name used by an organization or business person for labeling a product, product group, or similar.
 * @see http://schema.org/Brand
 */
class Brand extends Thing
{
    /**
     * Creates a new Brand instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_ADDITIONAL = 'additional' ;
    const FILTER_LOGO       = 'logo' ;
    const FILTER_PHOTOS     = 'photos' ;

    const FILTER_SLOGAN = 'slogan' ;

    ///////////////////////////

    /**
     * Indicates if the thing is active.
     */
    public $active ;

    /**
     * The additional description of the organization.
     * Note : this property is a custom attributeof the original Organization class defined in http://schema.org/Organization.
     * @var string
     */
    public $additional ;

    /**
     * Photographs of this organization (legacy spelling; see singular form, photo).
     * @var array
     */
    public $images ;

    /**
     * An associated logo
     */
    public $logo ;

    /**
     * Photographs of this organization (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    /**
     * A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event.
     * @var array|Person|Organization
     */
    public $sponsor ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'identifier'  => $this->identifier,
            'name'        => $this->name,
            'legalName'   => $this->legalName,
            'url'         => $this->url,
            'active'      => $this->active,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'image'       => $this->image,
            'licence'     => $this->licence,
            'description' => $this->description,
            'publisher'   => $this->publisher,

            'about'                     => $this->about,
            'additional'                => $this->additional,
            'additionalType'            => $this->additionalType,
            'alternateName'             => $this->alternateName,
            'address'                   => $this->address,
            'areServed'                 => $this->areaServed,
            'employees'                 => $this->employees,
            'events'                    => $this->events,
            'images'                    => $this->images,
            'keywords'                  => $this->keywords,
            'location'                  => $this->location,
            'logo'                      => $this->logo,
            'members'                   => $this->members,
            'photos'                    => $this->photos,
            'providers'                 => $this->providers,
            'subOrganization'           => $this->subOrganization,
            'sponsor'                   => $this->sponsor
        ];

        return Objects::compress( $object ) ;
    }
}


