<?php

namespace xyz\ooopener\things ;

use JsonSerializable ;

use core\Objects ;

/**
 * Class TokenPayload
 * @package xyz\ooopener\things
 */
class TokenPayload implements JsonSerializable
{
    /**
     * TokenPayload constructor.
     */
    public function __construct()
    {
    }

    /**
     * The internal id of the token
     * @var string|null
     */
    public ?string $id = null ;

    /**
     * A unique token identifier of the token (JWT id)
     * @var string
     */
    public string $jti ;

    /**
     * The id of the server who issued the token (Issuer)
     * @var string
     */
    public string $iss ;

    /**
     * The id of the client who requested the token (Audience)
     * @var string|null
     */
    public ?string $aud = null ;

    /**
     * The id of the user for which the token was released (Subject)
     * @var string
     */
    public string $sub ;

    /**
     * UNIX timestamp when the token expires (Expiration)
     * @var integer
     */
    public int $exp ;

    /**
     * UNIX timestamp when the token was created (Issued At)
     * @var integer
     */
    public int $iat ;

    /**
     * UNIX timestamp when the token is valid (Not Before)
     * @var integer|null
     */
    public ?int $nbf = null ;

    /**
     * @var string
     */
    public string $scope ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'    => $this->id,
            'jti'   => $this->jti,
            'iss'   => $this->iss,
            'aud'   => $this->aud,
            'sub'   => $this->sub,
            'exp'   => $this->exp,
            'iat'   => $this->iat,
            'nbf'   => $this->nbf,
            'scope' => $this->scope
        ];

        return Objects::compress( $object ) ;
    }
}
