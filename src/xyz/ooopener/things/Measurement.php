<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Object Measurement informations.
 */
class Measurement extends Thing
{
    /**
     * Creates a new Measurement instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The aspect of an object being measured.
     * Examples : height, width, depth, weight, volume, circumference, etc.
     */
    public $dimension ;

    /**
     * The unit of measurement used when measuring a dimension.
     * Examples : cm, metres, inches, g
     */
    public $unit ;

    /**
     * The numeric value of the Measurement of a dimension.
     * Examples : 23, 14.5
     */
    public $value ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'name'        => $this->name,
            'dimension'   => $this->dimension,
            'value'       => $this->value,
            'unit'        => $this->unit,
            'description' => $this->description
        ];
        return Objects::compress( $object ) ;
    }
}


