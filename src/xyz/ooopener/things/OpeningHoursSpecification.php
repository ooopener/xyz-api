<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * A structured value providing information about the opening hours of a place or a certain service inside a place.
 */
class OpeningHoursSpecification extends Thing
{
    /**
     * Creates a new OpeningHoursSpecification instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The closing hour of the place or service on the given day(s) of the week (Time).
     */
    public $closes ;

    /**
     * The day of the week for which these opening hours are valid (DayOfWeek).
     */
    public $dayOfWeek ;

    /**
     * The opening hour of the place or service on the given day(s) of the week (Time).
     */
    public $opens ;

    /**
     * The date when the item becomes valid (DateTime).
     */
    public $validFrom ;

    /**
     * The end of the validity of offer, price specification, or opening hours data (DateTime).
     */
    public $validThrough ;

    /**
     * @private
     */
    const PROPERTIES =
    [
        'id'           => 'int' ,
        'name'         => NULL ,
        'description'  => NULL ,
        'dayOfWeek'    => NULL ,
        'opens'        => NULL ,
        'closes'       => NULL ,
        'validFrom'    => 'date',
        'validThrough' => 'date',
        'created'      => 'date',
        'modified'     => 'date'
    ] ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object|array $init
     * @param $iso8601
     *
     * @return OpeningHoursSpecification
     */
    public static function create( $init = NULL , $iso8601 = NULL )
    {
        $item = NULL ;

        if( isset( $init ) )
        {
            $item = new OpeningHoursSpecification() ;

            $isArray  = is_array($init)  ;
            $isObject = is_object($init) ;

            foreach ( self::PROPERTIES as $key => $filter )
            {
                if( $isArray && array_key_exists( $key , $init ) )
                {
                    $item->{ $key } = $init[ $key ] ;
                }
                else if( $isObject && property_exists( $init , $key ) )
                {
                    $item->{ $key } = $init->{ $key } ;
                }

                if( property_exists( $item , $key ) )
                {
                    switch( $filter )
                    {
                        case 'int' :
                        {
                            $item->{ $key } = (int) $item->{ $key } ;
                            break ;
                        }
                        case 'date' :
                        {
                            if( $item->{ $key } != NULL )
                            {
                                if( $iso8601 )
                                {
                                    $item->{ $key } = $iso8601->formatTimeToISO8601( $item->{ $key } );
                                }
                            }
                            break ;
                        }
                    }

                }
            }
        }

        return $item ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'           => $this->id,
            'created'      => $this->created,
            'modified'     => $this->modified,
            'name'         => $this->name,
            'description'  => $this->description,
            'url'          => $this->url,
            'dayOfWeek'    => $this->dayOfWeek,
            'closes'       => $this->closes,
            'opens'        => $this->opens,
            'validFrom'    => $this->validFrom,
            'validThrough' => $this->validThrough
        );

        return Objects::compress( $object ) ;
    }
}


