<?php

namespace xyz\ooopener\things\creativeWork\mediaObject;

use xyz\ooopener\things\creativeWork\MediaObject;
use core\Objects;

/**
 * A video object file representation.
 */
class VideoObject extends MediaObject
{
    /**
     * Creates a new VideoObject instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The actor of the item
     * @var string|int
     */
    public $actor ;

    /**
     * The audio codec
     * @var string
     */
    public $audioCodec ;

    /**
     * The audio bit per sample
     * @var int
     */
    public $bitsPerSample ;

    /**
     * The caption of the item.
     * @var string
     */
    public $caption ;

    /**
     * The number of channel of the audio
     * @var string
     */
    public $channels ;

    /**
     * The director of the item.
     * @var string|int
     */
    public $director ;

    /**
     * The music by of the item
     * @var string
     */
    public $musicBy ;

    /**
     * The sample rate of the audio
     * @var int
     */
    public $sampleRate ;

    /**
     * The thumbnail of the item.
     * @var object
     */
    public $thumbnail ;

    /**
     * The transcript of the item
     * @var string
     */
    public $transcript ;

    /**
     * The video frame size of the item
     * @var string
     */
    public $videoFrameSize ;

    /**
     * The video quality of the item
     * @var string
     */
    public $videoQuality ;

    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'           => self::FILTER_ID ,
        'name'         => self::FILTER_DEFAULT ,
        'url'          => self::FILTER_URL ,
        'created'      => self::FILTER_DATETIME ,
        'modified'     => self::FILTER_DATETIME ,
        'description'  => self::FILTER_TRANSLATE ,

        'author'              => self::FILTER_DEFAULT ,
        'alternativeHeadline' => self::FILTER_TRANSLATE ,
        'encoding'            => self::FILTER_DEFAULT ,
        'encodingFormat'      => self::FILTER_DEFAULT ,
        'headline'            => self::FILTER_TRANSLATE ,
        'inLanguage'          => self::FILTER_DEFAULT ,
        'license'             => self::FILTER_DEFAULT ,
        'mentions'            => self::FILTER_TRANSLATE ,
        'publisher'           => self::FILTER_DEFAULT ,
        'review'              => self::FILTER_TRANSLATE ,
        'source'              => self::FILTER_DEFAULT ,
        'text'                => self::FILTER_TRANSLATE ,
        'thumbnailUrl'        => self::FILTER_DEFAULT ,

        'bitrate'        => self::FILTER_INT,
        'contentSize'    => self::FILTER_INT ,
        'contentUrl'     => self::FILTER_DEFAULT ,
        'duration'       => self::FILTER_DEFAULT ,
        'editor'         => self::FILTER_DEFAULT ,
        'embedUrl'       => self::FILTER_DEFAULT ,
        'playerType'     => self::FILTER_DEFAULT ,
        'width'          => self::FILTER_INT ,
        'height'         => self::FILTER_INT ,

        'actor'          => self::FILTER_DEFAULT ,
        'audioCodec'     => self::FILTER_DEFAULT,
        'bitsPerSample'  => self::FILTER_DEFAULT,
        'caption'        => self::FILTER_TRANSLATE ,
        'channels'       => self::FILTER_DEFAULT,
        'director'       => self::FILTER_DEFAULT ,
        'musicBy'        => self::FILTER_DEFAULT ,
        'sampleRate'     => self::FILTER_DEFAULT,
        'thumbnail'      => self::FILTER_DEFAULT ,
        'transcript'     => self::FILTER_TRANSLATE ,
        'videoFrameSize' => self::FILTER_DEFAULT ,
        'videoQuality'   => self::FILTER_DEFAULT
    ];

    /**
     * Creates an instance and initialize it.
     *
     * @param object $init
     * @param $iso8601
     * @param string $url
     *
     * @return VideoObject
     */
    public static function create( $init = NULL , $iso8601 = NULL , $url = NULL , $lang = NULL )
    {
        $media = NULL ;

        if( isset( $init ) )
        {
            $media = new VideoObject() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DATETIME :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $media->{ $property } = isset( $iso8601 )
                                ? $iso8601->formatTimeToISO8601($init->{ $property })
                                : $init->{ $property } ;
                        }
                        break ;
                    }
                    case self::FILTER_URL :
                    {
                        $media->{ $property } = $url ;
                        break ;
                    }
                    case self::FILTER_ID :
                    {
                        $media->{ $property } = (int) $init->{ '_key' } ;
                        break ;
                    }
                    case self::FILTER_TRANSLATE :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            if( is_array( $init->{$property} ) )
                            {
                                if( $lang != NULL )
                                {
                                    if( array_key_exists( $lang , $init->{ $property } ) )
                                    {
                                        $media->{ $property } = $init->{ $property }[$lang] ;
                                    }
                                    else
                                    {
                                        $media->{ $property } = "" ;
                                    }
                                }
                                else
                                {
                                    $media->{ $property } = $init->{ $property } ;
                                }
                            }
                            else if( is_string( $init->{ $property } ) )
                            {
                                $media->{ $property } = $init->{ $property } ;
                            }
                        }
                        break ;
                    }
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $media->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $media ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'          => $this->id,
            'name'        => $this->name,
            'url'         => $this->url,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'description' => $this->description,

            'author'              => $this->author,
            'alternativeHeadline' => $this->alternativeHeadline,
            'encoding'            => $this->encoding,
            'encodingFormat'      => $this->encodingFormat,
            'headline'            => $this->headline,
            'inLanguage'          => $this->inLanguage,
            'keywords'            => $this->keywords,
            'license'             => $this->license,
            'mentions'            => $this->mentions,
            'publisher'           => $this->publisher,
            'review'              => $this->review,
            'source'              => $this->source,
            'text'                => $this->text,
            'thumbnailUrl'        => $this->thumbnailUrl,

            'bitrate'        => $this->bitrate,
            'contentSize'    => $this->contentSize,
            'contentUrl'     => $this->contentUrl,
            'duration'       => $this->duration,
            'editor'         => $this->editor,
            'embedUrl'       => $this->embedUrl,
            'playerType'     => $this->playerType,
            'width'          => $this->width,
            'height'         => $this->height,

            'actor'          => $this->actor,
            'audioCodec'     => $this->audioCodec,
            'bitsPerSample'  => $this->bitsPerSample,
            'caption'        => $this->caption,
            'channels'       => $this->channels,
            'director'       => $this->director,
            'musicBy'        => $this->musicBy,
            'sampleRate'     => $this->sampleRate,
            'thumbnail'      => $this->thumbnail,
            'transcript'     => $this->transcript,
            'videoFrameSize' => $this->videoFrameSize,
            'videoQuality'   => $this->videoQuality
        );

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}


