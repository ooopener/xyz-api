<?php

namespace xyz\ooopener\things;


/**
 * A stage.
 */
class Stage extends Thing
{
    /**
     * Creates a new Stage instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_ALTERNATIVEHEADLINE = 'alternativeHeadline' ;
    const FILTER_HEADLINE            = 'headline' ;
    const FILTER_LOCATION            = 'location' ;
    const FILTER_MEDIA               = 'media' ;
    const FILTER_PHOTOS              = 'photos' ;

    const FILTER_DISCOVER            = 'discover' ;

    const FILTER_ARTICLES            = 'articles' ;
    const FILTER_CONCEPTUAL_OBJECTS  = 'conceptualObjects' ;
    const FILTER_COURSES             = 'courses' ;
    const FILTER_EVENTS              = 'events' ;
    const FILTER_ORGANIZATIONS       = 'organizations' ;
    const FILTER_PEOPLE              = 'people' ;
    const FILTER_PLACES              = 'places' ;
    const FILTER_STAGES              = 'stages' ;

    ///////////////////////////

    /**
     * Indicates if the stage is active.
     */
    public $active ;

    /**
     * activities associated with this stage.
     * @var array
     */
    public $activities ;

    /**
     * An alternative headline of the item.
     */
    public $alternativeHeadline ;

    public $courses ;

    public $events ;

    /**
     * An headline of the item.
     */
    public $headline ;

    /**
     * The location of the item.
     */
    public $location ;

    /**
     * media associated with this stage.
     * @var array
     */
    public $media ;

    public $objects ;

    public $organizations ;

    public $people ;

    /**
     * photos associated with this stage.
     * @var array
     */
    public $photos ;

    public $places ;

    public $stages ;

    /**
     * The status of the item.
     */
    public $status ;

    /**
     * A text of the item.
     */
    public $text ;

    /**
     * websites associated with this stage.
     * @var array
     */
    public $websites ;
}
