<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An website information. Based on https://developers.google.com/google-apps/contacts/v3/reference#gcWebsite.
 */
class Website extends Thing
{
    /**
     * Creates a new Website instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * A link to the website.
     * @var string
     */
    public $href ;

    /**
     * When multiple websites appear in an entry, indicates which is primary. At most one website may be primary. Default value is false.
     * @var boolean
     */
    public $primary ;

    /**
     * A programmatic value that identifies the type of a website (related website values).
     * Related values :
     * - home-page : "The home page of the contact."
     * - blog      : "Contact's blog."
     * - profile   : "Contact's profile."
     * - home      : "home-related site."
     * - work      : "work-related site."
     * - other     : "site of other type."
     * - ftp       : "FTP site."
     * @var string
     */
    public $rel ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object|array $init
     *
     * @return Website
     */
    public static function create( $init = NULL )
    {
        $web = NULL ;

        if( isset( $init ) )
        {
            $web = new Website() ;

            if( is_array($init) )
            {
                if( array_key_exists( 'id' , $init ) )
                {
                    $web->id = (int) $init['id'] ;
                }

                if( array_key_exists( 'name' , $init ) )
                {
                    $web->name = $init['name'] ;
                }

                if( array_key_exists( 'href' , $init ) )
                {
                    $web->href = $init['href'] ;
                }

                if( array_key_exists( 'rel' , $init ) )
                {
                    $web->rel = $init['rel'] ;
                }

                if( array_key_exists( 'primary' , $init ) )
                {
                    $web->primary = (bool) $init['primary'] ;
                }
                else if( array_key_exists( 'first' , $init ) )
                {
                    $web->primary = (bool) $init['first'] ;
                }
            }
            else if( is_object( $init ) )
            {
                if( property_exists( $init , 'id' ) )
                {
                    $web->id = (int) $init->id ;
                }

                if( property_exists( $init ,'name' ) )
                {
                    $web->name = $init->name ;
                }

                if( property_exists( $init, 'href' ) )
                {
                    $web->href = $init->href ;
                }

                if( property_exists( $init , 'rel'  ) )
                {
                    $web->rel = $init->rel ;
                }

                if( property_exists( $init , 'primary' ) )
                {
                    $web->primary = (bool) $init->primary ;
                }
                else if( property_exists( $init , 'first' ) )
                {
                    $web->primary = (bool) $init->first ;
                }
            }
        }

        return $web ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'      => $this->id,
            'name'    => $this->name,
            'href'    => $this->href,
            'primary' => $this->primary,
            'rel'     => $this->rel
        );

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() :string
    {
        return "[" . get_class($this) . " id:" . $this->id . " name:" . $this->name . " href:" . $this->href . "]" ;
    }
}


