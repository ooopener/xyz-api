<?php

namespace xyz\ooopener\things;

use core\Objects;

class Sector extends Place
{
    /**
     * Creates a new Sector instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serializableAttributes =
    [
        'id'             ,
        'url'            ,
        'additionalType' ,
        'created'        ,
        'modified'
    ];

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serializableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}
