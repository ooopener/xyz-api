<?php

namespace xyz\ooopener\things;

class Game extends CreativeWork
{
    /**
     * Creates a new Game instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_CHARACTER_ATTRIBUTE = 'characterAttribute' ;
    const FILTER_QUEST               = 'quest' ;
    const FILTER_ITEM                = 'item' ;
    const FILTER_LOCATION            = 'location' ;
    const FILTER_NUMBER_OF_PLAYERS   = 'numberOfPlayers' ;

    ///////////////////////////


    public $characterAttribute ;

    public $quest ;

    public $item ;

    public $location ;

    public $numberOfPlayers ;

}
