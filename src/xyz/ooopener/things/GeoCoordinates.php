<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * The geographic coordinates of a place or event.
 */
class GeoCoordinates extends Thing
{
    /**
     * Creates a new GeoCoordinates instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The distance of a location. For example 125 (double).
     * @var double
     */
    public $distance ;

    /**
     * The elevation of a location. For example 125 (double).
     * @var double
     */
    public $elevation ;

    /**
     * The latitude of a location. For example 37.42242 (double).
     * @var double
     */
    public $latitude ;

    /**
     * The longitude of a location. For example -122.08585 (double).
     * @var double
     */
    public $longitude ;

    /**
     * Creates an instance and initialize it.
     *
     * @param object|array $init
     *
     * @return GeoCoordinates
     */
    public static function create( $init = NULL )
    {
        $geo = NULL ;

        if( isset( $init ) )
        {
            $geo = new GeoCoordinates();

            if ( isset( $init->latitude ) )
            {
                $geo->latitude = (double) $init->latitude;
            }

            if ( isset( $init->longitude ) )
            {
                $geo->longitude = (double) $init->longitude;
            }

            if ( isset( $init->elevation ) )
            {
                $geo->elevation = (double) $init->elevation;
            }

            if ( isset( $init->distance ) )
            {
                $geo->distance = (double) $init->distance;
            }
        }

        return $geo ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
            'distance'  => $this->distance,
            'elevation' => $this->elevation
        ];
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' , 'latitude' , 'longitude' , 'distance' , 'elevation' ) ;
    }
}


