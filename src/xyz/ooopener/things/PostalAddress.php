<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * The mailing address.
 */
class PostalAddress extends Thing
{
    /**
     * Creates a new PostalAddress instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $additionalAddress ;

    /**
     * The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
     * @var string
     */
    public $addressCountry ;

    /**
     * In the administrative divisions of France, the department is one of the three levels
     * of government below the national level ("territorial collectivities"),
     * between the region and the commune.
     * @var string
     */
    public $addressDepartment ;

    /**
     * The locality. For example, Mountain View.
     * @var string
     */
    public $addressLocality ;

    /**
     * The region. For example, CA.
     * @var string
     */
    public $addressRegion ;

    /**
     * Email address.
     * @var string
     */
    public $email ;

    /**
     * The fax number.
     * @var string
     */
    public $faxNumber ;

    /**
     * The postal code. For example, 94043
     * @var string
     */
    public $postalCode ;

    public $postOfficeBoxNumber ;

    /**
     * The street address. For example, 1600 Amphitheatre Pkwy.
     * @var string
     */
    public $streetAddress ;

    /**
     * The telephone number.
     * @var string
     */
    public $telephone ;

    /**
     * The url of the item.
     * @var string
     */
    public $url ;

    /**
     * The enumeration of all postal address properties.
     */
    const PROPERTIES =
    [
        'additionalAddress',
        'addressCountry',
        'addressDepartment',
        'addressLocality',
        'addressRegion',
        'email',
        'postalCode',
        'postOfficeBoxNumber',
        'streetAddress',
        'telephone',
        'faxNumber'
    ];

    /**
     * Creates an instance and initialize it.
     *
     * @param array|object $init
     *
     * @return PostalAddress
     */
    public static function create( $init = NULL )
    {
        $address = NULL ;
        if( isset( $init ) )
        {
            foreach( self::PROPERTIES as $key )
            {
                if( is_array( $init ) )
                {
                    if( array_key_exists( $key , $init ) && !empty($init[$key]))
                    {
                        if( !isset($address) )
                        {
                            $address = new PostalAddress() ;
                        }
                        $address->{ $key } = $init[$key] ;
                    }
                }
                else
                {
                    if( property_exists( $init , $key ) && !empty($init->{$key}))
                    {
                        if( !isset($address) )
                        {
                            $address = new PostalAddress() ;
                        }
                        $address->{ $key } = $init->{ $key } ;
                    }
                }
            }
        }
        return $address ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'name'                => $this->name,
            'additionalAddress'   => $this->additionalAddress,
            'addressCountry'      => $this->addressCountry,
            'addressDepartment'   => $this->addressDepartment,
            'addressLocality'     => $this->addressLocality,
            'addressRegion'       => $this->addressRegion,
            'email'               => $this->email,
            'faxNumber'           => $this->faxNumber,
            'postalCode'          => $this->postalCode,
            'postOfficeBoxNumber' => $this->postOfficeBoxNumber,
            'streetAddress'       => $this->streetAddress,
            'telephone'           => $this->telephone,
            'created'             => $this->created,
            'modified'            => $this->modified
        ];
        return Objects::compress( $object ) ;
    }
}


