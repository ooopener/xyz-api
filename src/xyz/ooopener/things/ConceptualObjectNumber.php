<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * Without an Object number it is not possible either to uniquely identify an object or to link an object with its documentation.
 * The Object number should be marked on, or otherwise physically associated with the object.
 */
class ConceptualObjectNumber extends Thing
{
    /**
     * Creates a new ConceptualObjectNumber instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The date that the Object number was assigned to the object.
     * Examples : 1945, 2010-04, 1933-01-21
     */
    public $date ;

    /**
     * Identity number type
     * Examples: accession number / previous accession number / previous loan number
     */
    public $type ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'       => $this->id,
            'created'  => $this->created,
            'modified' => $this->modified,
            'name'     => $this->name,
            'type'     => $this->type,
            'date'     => $this->date
        ];
        return Objects::compress( $object ) ;
    }
}


