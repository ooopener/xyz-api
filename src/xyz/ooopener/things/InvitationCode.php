<?php

namespace xyz\ooopener\things ;

use DateTime ;

/**
 * An invitation code.
 */
class InvitationCode
{
    /**
     * Creates a new InvitationCode instance.
     * @param object|null $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( object $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The id.
     * @var integer
     */
    public int $id;

    /**
     * The user.
     * @var string
     */
    public string $user;

    /**
     * The team.
     * @var integer
     */
    public int $team;

    /**
     * The email.
     * @var string
     */
    public string $email;

    /**
     * The person.
     * @var integer
     */
    public int $person;

    /**
     * The code.
     * @var integer
     */
    public int $code;

    /**
     * @var string
     */
    public string $ip;

    /**
     * @var string
     */
    public string $redirect;

    /**
     * Date of creation of the resource.
     */
    public DateTime $created ;

    /**
     * Date of the expiration of the resource.
     */
    public DateTime $expired ;

    /**
     * Date of the used of the resource.
     */
    public DateTime $used ;

    /**
     * The 'date' filter constant.
     */
    const FILTER_DATE = 'date' ;

    /**
     * The default filter constant (NULL).
     */
    const FILTER_DEFAULT = NULL ;


    /**
     * The enumeration of all object properties.
     */
    public static array $properties =
    [
        'id'      => self::FILTER_DEFAULT ,
        'user'    => self::FILTER_DEFAULT ,
        'team'    => self::FILTER_DEFAULT ,
        'email'   => self::FILTER_DEFAULT ,
        'person'  => self::FILTER_DEFAULT ,
        'code'    => self::FILTER_DEFAULT ,
        'ip'      => self::FILTER_DEFAULT ,
        'created' => self::FILTER_DATE ,
        'expired' => self::FILTER_DATE ,
        'used'    => self::FILTER_DATE
    ];

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return "[" . get_class($this) . "]" ;
    }
}

