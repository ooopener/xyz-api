<?php

namespace xyz\ooopener\things;

use core\Objects;

/**
 * An application.
 */
class Application extends CreativeWork
{
    /**
     * Creates a new Application instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    // ------------ filters

    const FILTER_OAUTH   = 'oAuth' ;
    const FILTER_OWNER   = 'owner' ;
    const FILTER_SETTING = 'setting' ;

    const FILTER_SLOGAN = 'slogan' ;

    // ------------

    /**
     * The client uid.
     * @var string
     */
    public $client_id ;

    /**
     * The client secret.
     * @var string
     */
    public $client_secret ;

    /**
     * The url redirect.
     * @var string
     */
    public $url_redirect ;


    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'            => self::FILTER_DEFAULT ,
        'name'          => self::FILTER_DEFAULT ,
        'url'           => self::FILTER_URL ,
        'created'       => self::FILTER_DATE ,
        'modified'      => self::FILTER_DATE ,
        'client_id'     => self::FILTER_DEFAULT ,
        'client_secret' => self::FILTER_DEFAULT,
        'url_redirect'  => self::FILTER_URL
    ];

    /**
     * Creates a new Application instance.
     * @param object $init An object to initialize a new Application instance.
     * @param $iso8601
     * @param $url
     * @param $lang
     *
     * @return Application $application
     */
    public static function create( $init = NULL , $iso8601 = NULL , $url = NULL , $lang = NULL )
    {
        $application = NULL ;

        if( isset( $init ) )
        {
            $application = new Application() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DATE :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $application->{ $property } = isset( $iso8601 )
                                ? $iso8601->formatTimeToISO8601($init->{ $property })
                                : $init->{ $property } ;
                        }
                        break ;
                    }
                    case self::FILTER_URL :
                    {
                        $application->{ $property } = $url ;
                        break ;
                    }
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $application->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $application ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'            => $this->id,
            'name'          => $this->name,
            'url'           => $this->url,
            'created'       => $this->created,
            'modified'      => $this->modified,
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'url_redirect'  => $this->url_redirect
        ];

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return "[" . get_class($this) . " id:" . $this->id . " name:" . $this->name . "]" ;
    }
}


