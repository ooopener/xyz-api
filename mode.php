<?php

return function( string $root , string $defaultMode = 'dev' )
{
    if( file_exists ( $root .'/user.ini' ) )
    {
        return file_get_contents( $root .'/user.ini' ) ;
    }
    else if( file_exists ( __ROOT__ .'/mode.ini' ) )
    {
        return file_get_contents( $root .'/mode.ini' ) ;
    }
    else
    {
        return 'dev' ;
    }
};
