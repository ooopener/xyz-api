<?php /** @noinspection PhpIncludeInspection */

use xyz\ooopener\data\ThesaurusCollector;
use xyz\ooopener\helpers\Arango;

use system\logging\Logger;

$getControllers = require 'controllers/thesaurus.php' ;
$getModels      = require 'models/thesaurus.php'      ;

return function
(
    $cache = NULL ,
    ?array $config = NULL ,
    string $root = ''
)
use (
    $getControllers,
    $getModels
)
{
    $definitions = NULL ;
    $routes      = NULL ;

    if( !file_exists( $cache ) )
    {
        $logger    = new Logger( $root . $config['logger'][ 'path' ] , $config['logger'][ 'level' ] ) ;
        $arango    = new Arango( $config['arangoDB'] , $logger ) ;
        $collector = new ThesaurusCollector( $cache , $logger , __CACHE__ . '/routes.php' ) ;

        set_error_handler     ( [ $logger , 'onError'     ] ) ;
        set_exception_handler ( [ $logger , 'onException' ] ) ;

        try
        {
            $arango->prepare
            ([
                'query'    => 'FOR doc in @@table RETURN { identifier:doc.identifier , path:doc.path }' ,
                'bindVars' => [ '@table' => 'thesaurus' ]
            ]);

            $arango->execute();

            $collector->build( $arango->getAll() ) ;
        }
        catch( Exception $exception )
        {
            $logger->warning( __FILE__ . ' failed : ' . $exception->getMessage() ) ;
        }
    }

    if( file_exists( $cache ) )
    {
        $init = require( $cache ) ;

        $definitions = array_merge
        (
            $getControllers ( $init['CONTROLLERS'] ) ,
            $getModels      ( $init['MODELS']      )
        );

        $routes = $init['ROUTES'] ;
    }

    return
    [
        'thesaurusDefinitions' => $definitions ,
        'thesaurusRoutes'      => $routes
    ];
} ;
