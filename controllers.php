<?php

return ( require 'controllers/commons.php'                 )
     + ( require 'controllers/microAdventure.php'          )
     + ( require 'controllers/animalHealth.php'            )
     + ( require 'controllers/settings/animal-health.php'  )
;
