# XYZ API 

XYZ API for Slim with ArangoDB

### Licences ###

Under tree licences : **MPL 1.1/GPL 2.0/LGPL 2.1**

 * Licence MPL 1.1 : http://www.mozilla.org/MPL/MPL-1.1.html
 * Licence GPL 2 : http://www.gnu.org/licenses/gpl-2.0.html
 * Licence LGPL 2.1 : http://www.gnu.org/licenses/lgpl-2.1.html
