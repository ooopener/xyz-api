<?php

use system\logging\Logger;

use xyz\ooopener\display\Image;

$getLogger = function( $app , $setting )
{
    $logger = new Logger( $app . $setting[ 'path' ] , $setting[ 'level' ] );

    set_error_handler     ( [ $logger , 'onError'     ]) ;
    set_exception_handler ( [ $logger , 'onException' ]) ;

    return $logger ;
};

return function( string $app , string $mode , string $xyz ) use ( $getLogger )
{
    error_reporting( E_ALL );

    require $app . 'vendor/autoload.php';

    $iconPath = $app  . 'images/icons/' ;

    $config = array_merge
    (
        parse_ini_file( $app  . 'config/commons.ini'   , TRUE ) ,
        parse_ini_file( $xyz  . 'config/thesaurus.ini' , TRUE ) ,
        parse_ini_file( $app  . 'config/config.' . $mode . '.ini' , TRUE )
    ) ;

    ini_set( 'display_errors'         , $config['display_errors']         ) ;
    ini_set( 'display_startup_errors' , $config['display_startup_errors'] ) ;
    ini_set( 'error_log'              , $app . 'logs/php-error.log'    ) ;

    $logger = $getLogger( $app , $config['logger'] ) ;

    if( isset( $_GET['thesaurus'] ) && isset( $_GET['id'] ) )
    {
        $id         = $_GET['id'] ;
        $thesaurus  = $_GET['thesaurus'] ;

        $iconPrefix = '' ;
        $iconSuffix = '.png' ;

        if( isset( $config[ $thesaurus ] ) )
        {
            $set = $config[ $thesaurus ] ;

            if( isset( $set['iconPrefix'] ) )
            {
                $iconPrefix = $set['iconPrefix'] ;
            }

            if( isset( $set['iconSuffix'] ) )
            {
                $iconSuffix = $set['iconSuffix'] ;
            }
        }

        $filename  = $iconPrefix . $id . $iconSuffix ;
        $file      = $iconPath . $thesaurus . '/' . $filename ;

        if( file_exists( $file ) )
        {
            $if_modified_since = isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] )
                               ? $_SERVER['HTTP_IF_MODIFIED_SINCE']
                               : false ;

            $time = gmdate('D, d M Y H:i:s ', filemtime( $file ) ) . 'GMT' ;

            if( $if_modified_since && $if_modified_since == $time )
            {
                http_response_code(304) ;
                die() ;
            }

            header( "Content-Type: " . mime_content_type( $file ) ) ;
            header( "Last-Modified: $time" ) ;

            $render = isset( $_GET['render'] ) ? (string) $_GET['render'] : 'icon' ;

            if( $render )
            {
                $pin   = $config['pin'] ;
                $image = new Image([ 'logger' => $logger ]) ;
                $image->icon
                (
                    $file ,
                    [
                        'bgcolor'  => isset( $_GET['bgcolor'] ) ? (string) $_GET['bgcolor'] : NULL ,
                        'color'    => isset( $_GET['color']   ) ? (string) $_GET['color']   : NULL ,
                        'format'   => isset( $_GET['format']  ) ? (string) $_GET['format']  : 'png' ,
                        'quality'  => isset( $_GET['quality'] ) ? (int)    $_GET['quality'] : 70 ,
                        'gray'     => isset( $_GET['gray']    ) ? (string) $_GET['gray']    : FALSE ,
                        'margin'   => isset( $_GET['margin']  ) ? (int)    $_GET['margin']  : 2 ,
                        'pinColor' => $pin['color'] ,
                        'pinPath'  => $iconPath . $pin['path'] ,
                        'render'   => $render ,
                        'shadow'   => ( isset( $_GET['shadow'] ) && $_GET['shadow'] == 'true' ) ? $config['icons']['shadow'] : NULL ,
                        'strip'    => isset( $_GET['strip']   ) ? (string) $_GET['strip']   : FALSE ,
                        'w'        => isset( $_GET['w']       ) ? (int)    $_GET['w']       : 30 ,
                        'h'        => isset( $_GET['h']       ) ? (int)    $_GET['h']       : 30
                    ]
                );
            }
            else
            {
                header( "Content-Length: " . filesize( $file ) ) ;
                readfile( $file ) ;
            }
        }
        else
        {
            $logger->error( __FILE__ . " failed, file doesn't exist!" ) ;
            http_response_code( 404 ) ;
            die() ;
        }
    }
    else
    {
        $logger->error( __FILE__ . " failed, missing params or unknown thesaurus settings" ) ;
        http_response_code( 404 ) ;
        die() ;
    }
};
