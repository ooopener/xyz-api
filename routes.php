<?php

use Slim\App ;

require 'routes/init.php' ;

$commons        = ( require 'routes/commons.php'                ) ;
$microAdventure = ( require 'routes/microAdventure.php'         ) ;
$animalHealth   = ( require 'routes/animalHealth.php'           ) ;

$settings = require 'routes/settings/animal-health.php' ;

$getRoutes   = ( require 'routes/thesaurus/builder.php' ) ;
$getSettings = ( require 'routes/settings/builder.php'  ) ;

return function
(
    App   $app ,
    array $thesaurus = []
)
use
(
    $animalHealth ,
    $commons ,
    $microAdventure ,

    $settings,

    $getRoutes,
    $getSettings
)
{
    $commons( $app ) ;
    $microAdventure( $app ) ;
    $animalHealth( $app ) ;

    $getSettings( $settings )( $app ) ;

    if( is_array( $thesaurus ) && count( $thesaurus ) > 0 )
    {
        try
        {
            $getRoutes( $thesaurus )( $app ) ;
        }
        catch( Exception $exception )
        {
            error_log( 'xyz-api/routes :: ' . $exception->getMessage() ) ;
        }
    }
};