<?php

require 'models/init.php' ;

return  ( require 'models/commons.php'                )
      + ( require 'models/microAdventure.php'         )
      + ( require 'models/animalHealth.php'           )
      + ( require 'models/settings/animal-health.php' )
;