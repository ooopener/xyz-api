<?php

use Slim\App ;

use xyz\ooopener\controllers\teams\TeamsController ;

return function( App $app )
{
    $controller = TeamsController::class ;

    $all = '/teams' ;
    $id  = '/teams/{id}' ;

    $app->options ( $all , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $all , $controller . ':all' )->setName( 'api.teams.all'  );
    $app->post    ( $all , $controller . ':post')->setName( 'api.teams.post' );

    $app->options ( $id , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $id , $controller . ':getTeam' )->setName('api.teams.get');
    $app->delete  ( $id , $controller . ':delete'  )->setName('api.teams.delete');
    $app->patch   ( $id , $controller . ':patch'   )->setName('api.teams.patch');
};

/**
 * @OA\Get(
 *     path="/teams",
 *     tags={"teams"},
 *     description="List teams",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Post(
 *     path="/teams",
 *     tags={"teams"},
 *     description="Create a new team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postTeam"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Get team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Delete(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Delete team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Patch a team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchTeam"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
