<?php

use Slim\App ;

use xyz\ooopener\controllers\teams\TeamPermissionsController ;

return function( App $app )
{
    $controller = TeamPermissionsController::class ;

    $route = '/teams/{name}/permissions' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $route , $controller . ':all')->setName('api.teams.permissions.all');
};
