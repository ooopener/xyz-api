<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="people",
 *     description="People paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'people/people.php' ) ( $app ) ;

    ( require 'people/active.php'      ) ( $app ) ;
    ( require 'people/brand.php'       ) ( $app ) ;
    ( require 'people/address.php'     ) ( $app ) ;
    ( require 'people/audio.php'       ) ( $app ) ;
    ( require 'people/audios.php'      ) ( $app ) ;
    ( require 'people/email.php'       ) ( $app ) ;
    ( require 'people/image.php'       ) ( $app ) ;
    ( require 'people/keywords.php'    ) ( $app ) ;
    ( require 'people/memberOf.php'    ) ( $app ) ;
    ( require 'people/photos.php'      ) ( $app ) ;
    ( require 'people/telephone.php'   ) ( $app ) ;
    ( require 'people/job.php'         ) ( $app ) ;
    ( require 'people/translation.php' ) ( $app ) ;
    ( require 'people/video.php'       ) ( $app ) ;
    ( require 'people/videos.php'      ) ( $app ) ;
    ( require 'people/websites.php'    ) ( $app ) ;
    ( require 'people/withStatus.php'  ) ( $app ) ;
    ( require 'people/worksFor.php'    ) ( $app ) ;
};
