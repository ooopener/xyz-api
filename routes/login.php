<?php

use Slim\App ;

return function ( App $app )
{
    ( require "login/invite.php" )( $app ) ;
    ( require "login/login.php"  )( $app ) ;
    ( require "login/logout.php" )( $app ) ;
};
