<?php

use xyz\ooopener\controllers\games\CourseGamesController;

return ( require __DIR__ . '/../helpers/games/quest.php' )
(
    'games',
    CourseGamesController::class
) ;
