<?php

use Slim\App ;

use xyz\ooopener\controllers\games\ApplicationGamesController;

return function( App $app )
{
    ( require 'questions/questions.php'   ) ( $app ) ;
    ( require 'questions/item.php'        ) ( $app ) ;
    ( require 'questions/translation.php' ) ( $app ) ;
};
