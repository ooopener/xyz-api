<?php

use Slim\App ;

use xyz\ooopener\controllers\games\QuestionGamesController;

return function( App $app )
{
    $controller = QuestionGamesController::class ;

    $path    = '/games/questions' ;
    $thing   = $path . '/{id:[0-9]+}' ;
    $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $app->options ( $path , $options );
    $app->get     ( $path , $controller . ':all'  )->setName( 'api.games.questions.all'  ); // TODO verify this route ?
    $app->post    ( $path , $controller . ':post' )->setName( 'api.games.questions.post' );

    $app->options ( $thing , $options );
    $app->get     ( $thing , $controller . ':get'    )->setName( 'api.games.questions.get'    );
    $app->delete  ( $thing , $controller . ':delete' )->setName( 'api.games.questions.delete' );
    $app->patch   ( $thing , $controller . ':patch'  )->setName( 'api.games.questions.patch'   );
};
