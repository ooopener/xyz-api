<?php

use Slim\App ;

return function( App $app )
{
   $translation = ( require __DIR__ . '/../../helpers/translation.php' )
   (
       'games/questions',
       'questionGamesTranslationController' ,
       [ 'useIndex' => TRUE ]
   );

   $translation( 'alternateName' )( $app );
   $translation( 'description'   )( $app );
   $translation( 'notes'         )( $app );
   $translation( 'text'          )( $app );
};