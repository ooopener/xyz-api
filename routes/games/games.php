<?php

use Slim\App ;

use xyz\ooopener\controllers\games\ApplicationGamesController;

return function( App $app )
{
    $controller = ApplicationGamesController::class ;

    $path    = '/games' ;
    $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $thing   = $path . '/{id:[0-9]+}' ;

    $app->options ( $path , $options )  ;
    $app->get     ( $path , $controller . ':all'  )->setName( 'api.games.all'  ) ;
    $app->post    ( $path , $controller . ':post' )->setName( 'api.games.post' ) ;

    $app->options ( $thing , $options )  ;
    $app->get     ( $thing , $controller . ':get'    )->setName( 'api.games.get'    ) ;
    $app->delete  ( $thing , $controller . ':delete' )->setName( 'api.games.delete' ) ;
    $app->patch   ( $thing , $controller . ':patch'  )->setName( 'api.game.patch'   ) ;
};
