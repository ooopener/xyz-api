<?php

use Slim\App ;

use xyz\ooopener\controllers\games\ApplicationGamesController;

return function( App $app )
{
    ( require 'courses/courses.php'     ) ( $app ) ;
    ( require 'courses/item.php'        ) ( $app ) ;
    ( require 'courses/quest.php'       ) ( $app ) ;
    ( require 'courses/translation.php' ) ( $app ) ;
};
