<?php

use xyz\ooopener\controllers\games\QuestionGamesController;

return ( require __DIR__ . '/../../helpers/games/quest.php' )
(
    'games/courses',
    QuestionGamesController::class
) ;
