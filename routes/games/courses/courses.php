<?php

use Slim\App ;

use xyz\ooopener\controllers\games\CourseGamesController;

return function( App $app )
{
    $controller = CourseGamesController::class ;

    $route = '/games/courses/{id:[0-9]+}' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ );
    $app->get     ( $route , $controller . ':get'    )->setName( 'api.games.courses.get'    );
    $app->delete  ( $route , $controller . ':delete' )->setName( 'api.games.courses.delete' );
    $app->patch   ( $route , $controller . ':patch'  )->setName( 'api.games.courses.patch'  );
};
