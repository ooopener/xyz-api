<?php

use core\Strings ;

use Slim\App ;
use Slim\Routing\RouteCollectorProxy ;

use xyz\ooopener\controllers\SettingsController;

/**
 * Register a new application route's group with a specific thesaurus controller reference.
 * @param SettingsController $controller
 * @return callable
 */
$populate = fn( SettingsController $controller ):callable => function( RouteCollectorProxy $group ) use ( $controller )
{
    $ref = str_replace( '/' , '.' , $controller->fullPath );

    // all
    $group->options( $controller->fullPath , [ $controller , __NOBODY_RESPONSE__ ] ) ;

    $group->get( $controller->fullPath , [ $controller , 'all' ] )
          ->setName( 'api.settings.' . $ref . '.all' );

    // getTo
    $group->options( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '' ) , [ $controller , __NOBODY_RESPONSE__ ] ) ;

    $group->get( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '' ) , [ $controller , 'get' ] )
          ->setName( 'api.settings.' . $ref . '.getTo' );

    // getFrom
    $group->get( Strings::fastformat( $controller->pattern , '' , '/{from:[0-9]+}' ) , [ $controller , 'get' ] )
          ->setName( 'api.settings.' . $ref . '.getFrom' );

    // delete
    $group->delete( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '/{from:[0-9]+}' ) , [ $controller , 'delete' ] )
          ->setName( 'api.settings.' . $ref . '.delete' );

    // delete all
    $group->delete( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '' ) , [ $controller , 'deleteAll' ] )
          ->setName( 'api.settings.' . $ref . '.deleteAll' );

    // patch
    $group->patch( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '/{from:[0-9]+}' ) , [ $controller , 'patch' ] )
          ->setName( 'api.settings.' . $ref . '.patch' );

    // put
    $group->put( Strings::fastformat( $controller->pattern , '/{to:[0-9]+}' , '' ) , [ $controller , 'put' ] )
          ->setName( 'api.settings.' . $ref . '.put' );

};

/**
 * Register all the route's groups with a specific array of thesaurus controllers.
 * @param array $controllers
 * @return callable
 */
return fn( array $controllers ) => function( App $app ) use ( $controllers , $populate )
{
    $id = NULL ;
    $container = $app->getContainer() ;
    foreach( $controllers as $id )
    {
        $controller = $container->get( $id ) ;
        $app->group( $controller->path , $populate( $controller ) );
    }
};
