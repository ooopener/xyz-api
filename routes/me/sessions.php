<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserSessionsController ;

return function( App $app )
{
    $controller = UserSessionsController::class ;
    $active     = '/me/sessions/{active:true|false|TRUE|FALSE}' ;
    $session    = '/me/sessions/{session}' ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $app->options ( $active , $options ) ;
    $app->get     ( $active , $controller. ':allForAccount' )->setName('api.me.sessions.active');

    $app->options ( '/me/sessions/all' , $options ) ;
    $app->patch   ( '/me/sessions/all' , $controller . ':revokeAllForAccount' )->setName('api.me.sessions.revoke.all');

    $app->options ( '/me/sessions' , $options ) ;
    $app->patch   ( '/me/sessions' , $controller . ':revokeAllListForAccount' )->setName('api.me.sessions.revoke.allList');

    $app->options ( $session , $options ) ;
    $app->patch   ( $session , $controller . ':revokeForAccount' )->setName('api.me.sessions.revoke');
};

/**
 * @OA\Get( path="/me/sessions/{active}",
 *     tags={"me"},
 *     description="List sessions me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W",
 *             "me:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/activeSession"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of sessions",type="array",items=@OA\Items(ref="#/components/schemas/UserSession"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch( path="/me/sessions/all",
 *     tags={"me"},
 *     description="Revoke all sessions me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch( path="/me/sessions",
 *     tags={"me"},
 *     description="Revoke sessions me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch( path="/me/sessions/{id}",
 *     tags={"me"},
 *     description="Revoke session me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
