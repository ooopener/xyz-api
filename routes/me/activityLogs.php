<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserActivityLogsController ;

return function( App $app )
{
    $controller = UserActivityLogsController::class ;
    $route      = '/me/activityLogs' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $route , $controller . ':allForAccount' )->setName('api.me.activityLogs');
};

/**
 * @OA\Get( path="/me/activityLogs",
 *     tags={"me"},
 *     description="List activityLogs me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W",
 *             "me:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/ActivityLog"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
