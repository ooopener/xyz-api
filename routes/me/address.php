<?php

use xyz\ooopener\controllers\users\UserPostalAddressController ;

return ( require __DIR__ . '/../helpers/address.php' )
(
    'me',
    UserPostalAddressController::class ,
    [
        'useIndex' => FALSE ,
        'get'      => 'getForAccount' ,
        'patch'    => 'patchForAccount',
        'path'     => '/address'
    ]
) ;

/**
 * @OA\Get( path="/me/address",
 *     tags={"me"},
 *     description="Get address me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W",
 *             "me:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Patch( path="/me/address",
 *     tags={"me"},
 *     description="Patch address me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/patchAddress"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
