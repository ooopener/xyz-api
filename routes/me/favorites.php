<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UsersController ;

return function( App $app )
{
    $controller = UsersController::class ;
    $route      = '/me/favorites' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $route , $controller . ':getFavorites'    )->setName('api.me.favorites');
    $app->delete  ( $route , $controller . ':deleteFavorites' )->setName('api.me.favorites.delete');
    $app->post    ( $route , $controller . ':postFavorites'   )->setName('api.me.favorites.post');
};

/**
 * @OA\Get( path="/me/favorites",
 *     tags={"me"},
 *     description="Get favorites me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W",
 *             "me:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/me/favorites",
 *     tags={"me"},
 *     description="Delete favorites to me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Post( path="/me/favorites",
 *     tags={"me"},
 *     description="Add favorites to me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
