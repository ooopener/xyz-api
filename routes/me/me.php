<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UsersController ;

return function( App $app )
{
    $controller = UsersController::class ;
    $route      = '/me' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $route , $controller . ':getForAccount' )->setName('api.me');
    $app->put     ( $route , $controller . ':putForAccount' )->setName('api.me.put');
};

/**
 * @OA\Get(
 *     path="/me",
 *     tags={"me"},
 *     description="Get the current user account informations",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W",
 *             "me:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Put(
 *     path="/me",
 *     tags={"me"},
 *     description="Edit me",
 *     security={
 *         {"OAuth2"={
 *             "me:A",
 *             "me:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/putUser"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
