<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="organizations",
 *     description="The /organizations API routes"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'organizations/organizations.php' ) ( $app ) ;

    ( require 'organizations/active.php'             ) ( $app ) ;
    ( require 'organizations/additionalType.php'     ) ( $app ) ;
    ( require 'organizations/address.php'            ) ( $app ) ;
    ( require 'organizations/audio.php'              ) ( $app ) ;
    ( require 'organizations/audios.php'             ) ( $app ) ;
    ( require 'organizations/brand.php'              ) ( $app ) ;
    ( require 'organizations/department.php'         ) ( $app ) ;
    ( require 'organizations/email.php'              ) ( $app ) ;
    ( require 'organizations/employees.php'          ) ( $app ) ;
    ( require 'organizations/founder.php'            ) ( $app ) ;
    ( require 'organizations/image.php'              ) ( $app ) ;
    ( require 'organizations/keywords.php'           ) ( $app ) ;
    ( require 'organizations/logo.php'               ) ( $app ) ;
    ( require 'organizations/memberOf.php'           ) ( $app ) ;
    ( require 'organizations/members.php'            ) ( $app ) ;
    ( require 'organizations/numbers.php'            ) ( $app ) ;
    ( require 'organizations/parentOrganization.php' ) ( $app ) ;
    ( require 'organizations/photos.php'             ) ( $app ) ;
    ( require 'organizations/providers.php'          ) ( $app ) ;
    ( require 'organizations/subOrganization.php'    ) ( $app ) ;
    ( require 'organizations/telephone.php'          ) ( $app ) ;
    ( require 'organizations/translation.php'        ) ( $app ) ;
    ( require 'organizations/video.php'              ) ( $app ) ;
    ( require 'organizations/videos.php'             ) ( $app ) ;
    ( require 'organizations/websites.php'           ) ( $app ) ;
    ( require 'organizations/withStatus.php'         ) ( $app ) ;
};
