<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="teams",
 *     description="Teams paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require "teams/permissions.php" )($app);
    ( require "teams/teams.php" )($app);
    ( require "teams/translation.php" )($app);
};
