<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserActivityLogsController ;

return function( App $app )
{
    $controller = UserActivityLogsController::class ;

    $all = '/users/activityLogs' ;
    $id = '/users/{id}/activityLogs' ;

    $app->options( $all , $controller . __CLAZZ_NOBODY_RESPONSE__) ;

    $app->get( $all , $controller . ':all')
        ->setName('api.users.activityLogs.all');


    $app->options( $id , $controller . __CLAZZ_NOBODY_RESPONSE__) ;

    $app->get( $id , $controller . ':allByUser')
        ->setName('api.users.activityLogs');
};

/**
 * @OA\Get(
 *     path="/users/{uuid}/activityLogs",
 *     tags={"users"},
 *     description="List activityLogs user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/ActivityLog"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
