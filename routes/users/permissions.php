<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserPermissionsController ;

return function( App $app )
{
    $controller = UserPermissionsController::class ;

    $route = '/users/{id}/permissions' ;

    $app->options( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;

    $app->get( $route , $controller . ':all' )
        ->setName('api.users.permissions.all');
};
