<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserSessionsController ;

return function( App $app )
{
    $controller = UserSessionsController::class ;

    $active  = '/users/{id}/sessions/{active:true|false|TRUE|FALSE}' ;
    $session = '/users/{id}/sessions/{session}' ;

    $app->options( $active , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;

    $app->get( $active , $controller . ':allByUser')
        ->setName('api.users.sessions.active');

    $app->patch('/users/{id}/sessions/all', $controller . ':revokeAll')
        ->setName('api.users.sessions.revoke.all');

    $app->patch('/users/{id}/sessions', $controller . ':revokeAllList')
        ->setName('api.users.sessions.revoke.allList');


    $app->options( $session , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;

    $app->patch($session , $controller . ':revoke')
        ->setName('api.users.sessions.revoke');
};

/**
 * @OA\Get(
 *     path="/users/{uuid}/sessions/{active}",
 *     tags={"users"},
 *     description="List sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Parameter(ref="#/components/parameters/activeSession"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of sessions",type="array",items=@OA\Items(ref="#/components/schemas/UserSession"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/users/{uuid}/sessions/all",
 *     tags={"users"},
 *     description="Revoke all sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/users/{uuid}/sessions",
 *     tags={"users"},
 *     description="Revoke sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/users/{uuid}/sessions/{id}",
 *     tags={"users"},
 *     description="Revoke session user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
