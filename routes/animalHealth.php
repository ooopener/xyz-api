<?php

use Slim\App ;

return function( App $app )
{
    ( require 'diseases.php'            ) ( $app ) ;
    ( require 'livestocks.php'          ) ( $app ) ;
    ( require 'medicalLaboratories.php' ) ( $app ) ;
    ( require 'technicians.php'         ) ( $app ) ;
    ( require 'veterinarians.php'       ) ( $app ) ;
};
