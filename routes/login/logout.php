<?php

use Slim\App ;

use xyz\ooopener\controllers\login\LogoutController ;

return function( App $app )
{
    $controller = LogoutController::class ;

    $app->options ( '/logout' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( '/logout' , $controller . ':get' )->setName('api.logout') ;
};
