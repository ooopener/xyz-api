<?php

use Slim\App ;

use xyz\ooopener\controllers\login\LoginController ;

return function( App $app )
{
    $controller = LoginController::class ;

    $app->options ( '/login' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( '/login' , $controller . ':get' )->setName('api.login');

    $app->options ( '/login/register' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->post    ( '/login/register' , $controller . ':post' )->setName('api.login.register.post') ; // //->add('csrf')
};
