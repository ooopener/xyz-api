<?php

use Slim\App ;

use xyz\ooopener\controllers\login\InviteController ;

return function( App $app )
{
    $controller = InviteController::class ;

    $app->options( '/login/invite'  , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get    ( '/login/invite'  , $controller . ':get' )->setName('api.login.invite') ;

    $app->options ( '/users/invite' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->post    ( '/users/invite' , $controller . ':post' )->setName('api.users.invite.post') ;
};
