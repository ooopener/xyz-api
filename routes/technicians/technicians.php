<?php

use xyz\ooopener\controllers\technicians\TechniciansController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'technicians' ,
    TechniciansController::class
) ;
