<?php

$app->get
(
    '/technicians/{id:[0-9]+}/withStatus' ,
    [ $container->technicianWithStatusController , 'get'  ]
)
->setName('api.technicians.withStatus.get') ;

$app->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->technicianWithStatusController , 'patch' ]
)
->setName('api.technicians.withStatus.patch') ;

