<?php

use Slim\App ;

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'games/games.php' ) ( $app ) ;

    ( require 'games/courses.php'     ) ( $app ) ;
    ( require 'games/quest.php'       ) ( $app ) ;
    ( require 'games/questions.php'   ) ( $app ) ;
    ( require 'games/item.php'        ) ( $app ) ;
    ( require 'games/translation.php' ) ( $app ) ;
};
