<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="me",
 *     description="Me paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'me/me.php' )( $app ) ;

    ( require 'me/activityLogs.php' )( $app ) ;
    ( require 'me/address.php'      )( $app ) ;
    ( require 'me/favorites.php'    )( $app ) ;
    ( require 'me/sessions.php'     )( $app ) ;
};
