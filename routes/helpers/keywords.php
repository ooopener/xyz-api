<?php

/**
 * Return the keywords route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/keywords.php' )
 * (
 *     'places',
 *     'placeKeywordsController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return ( require 'things.php' )( 'keywords' ) ;