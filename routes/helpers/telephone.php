<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the telephone route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/telephone.php' )
 * (
 *     'places',
 *     'placeWebsitesController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);
        $options    = [ $controller , __NOBODY_RESPONSE__ ] ;

        $all   = '/' . $route . '/{id:[0-9]+}/telephone';
        $count = $all . '/count';
        $thing = '/' . $route . '/{owner:[0-9]+}/telephone/{id:[0-9]+}';

        $app->options ( $all , $options );
        $app->get     ( $all , [ $controller , 'all' ] )->setName( 'api.' . $route . '.telephone.all'  );
        $app->post    ( $all , [ $controller , 'post'] )->setName( 'api.' . $route . '.telephone.post' );

        $app->options ( $count , $options );
        $app->get     ( $count , [ $controller, 'count' ] )->setName( 'api.' . $route . '.telephone.count' );

        $app->options ( $thing , $options );
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.telephone.delete' );
        $app->put     ( $thing , [ $controller , 'put'    ] )->setName( 'api.' . $route . '.telephone.put'    );
    }
    else
    {
        $container->get( LoggerInterface::class )
                  ->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};