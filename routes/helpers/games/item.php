<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the game's item route helper function.
 * @param string $route The route name.
 * @param string $controllerID The controller ID definition.
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID ) ;
        $path       = '/' . $route . '/{id:[0-9]+}/item' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $path , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->get     ( $path , [ $controller , 'get'   ] )->setName( 'api.' . $route . '.item.all'   );
        $app->patch   ( $path , [ $controller , 'patch' ] )->setName( 'api.' . $route . '.item.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};