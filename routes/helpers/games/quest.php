<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the game's quest route helper function.
 * @param string $route The route name.
 * @param string $controller The controller ID definition.
 * @return Closure
 */
return fn( string $route , string $controller ):Closure => function( App $app ) use ( $controller , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $path = '/' . $route . '/{id:[0-9]+}/quest' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $path , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
        $app->get     ( $path , $controller . ':all'   )->setName( 'api.' . $route . '.quest.all'  );
        $app->post    ( $path , $controller . ':post'  )->setName( 'api.' . $route . '.quest.post' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};