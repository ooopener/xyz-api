<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the 'video' route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/video.php' )
 * (
 *     'places',
 *     'placeVideoController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID ) ;

        $thing = '/' . $route . '/{id:[0-9]+}/video' ;
        $patch = '/' . $route . '/{owner:[0-9]+}/video/{id:[0-9]+}' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $thing , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->get     ( $thing , [ $controller , 'get'    ] )->setName( 'api.' . $route . '.video.get'    );
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.video.delete' );

        $app->options ( $patch , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->patch   ( $patch , [ $controller , 'patch' ] ) ->setName( 'api.' . $route . '.video.patch' );
    }
    else
    {
        $container->get( LoggerInterface::class )
                  ->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
