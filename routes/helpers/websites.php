<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the websites route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/websites.php' )
 * (
 *     'places',
 *     'placeWebsitesController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);

        $all     = '/' . $route . '/{id:[0-9]+}/websites';
        $count   = $all . '/count';
        $thing   = '/' . $route . '/{owner:[0-9]+}/websites/{id:[0-9]+}';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $all ,$options );
        $app->get     ( $all , [ $controller , 'all'  ] )->setName( 'api.' . $route . '.websites.all'  );
        $app->post    ( $all , [ $controller , 'post' ] )->setName( 'api.' . $route . '.websites.post' );

        $app->options ( $count , $options );
        $app->get     ( $count , [ $controller , 'count'])->setName( 'api.' . $route . '.websites.count' );

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.websites.delete' );
        $app->put     ( $thing , [ $controller , 'put'    ] )->setName( 'api.' . $route . '.websites.put'    );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};