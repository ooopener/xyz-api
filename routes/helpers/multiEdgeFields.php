<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the prohibitions route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/prohibitions.php' )
 * (
 *     'places',
 *     'placeProhibitionsController'
 * ) ;
 * </code>
 * @param string $name
 * @return Closure
 */
return fn( string $name ) =>
       fn( string $route , string $controllerID ):Closure =>
       function( App $app ) use ( $controllerID , $name , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);

        $all     = '/' . $route . '/{id:[0-9]+}/' . $name ;
        $count   = $all . '/count';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $all , $options );
        $app->get     ( $all , [ $controller , 'all' ] )->setName( 'api.' . $route . '.' . $name . '.all' );
        $app->put     ( $all , [ $controller , 'put' ] )->setName( 'api.' . $route . '.' . $name . '.put' );

        $app->options ( $count , $options );
        $app->get     ( $count , [ $controller , 'count' ] )->setName( 'api.' . $route . '.' . $name . '.count' );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};