<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the address route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/address.php' )
 * (
 *     'places',
 *     'placePostalAddressController'
 * ) ;
 * </code>
 * @param string $route The route name.
 * @param string $controller
 * @param array $init
 * @return Closure
 */
return fn
(
    string $route ,
    string $controller ,
    array  $init = []
):Closure
=> function( App $app ) use ( $controller , $init , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $useID = isset( $init[ 'useIndex' ] ) ? (bool) $init[ 'useIndex' ] : TRUE ;
        $get   = isset( $init[ 'get'      ] ) ? $init[ 'get'   ] : 'get'   ;
        $patch = isset( $init[ 'patch'    ] ) ? $init[ 'patch' ] : 'patch' ;
        $path  = isset( $init[ 'path'     ] ) ? $init[ 'path'  ] : '/{id}/address' ;

        $path = '/' . $route . $path ;

        if( $useID )
        {
            $controller = $container->get( $controller ) ;
            $get        = [ $controller , $get ];
            $options    = [ $controller , __NOBODY_RESPONSE__ ] ;
            $patch      = [ $controller , $patch ] ;
        }
        else
        {
            $get     = $controller . ':' . $get ;
            $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
            $patch   = $controller . ':' . $patch ;
        }

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $path , $options );
        $app->get     ( $path , $get   )->setName( 'api.' . $route . '.address.get'   );
        $app->patch   ( $path , $patch )->setName( 'api.' . $route . '.address.patch' );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};
