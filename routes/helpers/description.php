<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the description route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/description.php' )
 * (
 *     'places',
 *      PlaceTranslationController::class
 * )( $app ) ;
 * </code>
 * @param string $route
 * @param string $controller
 * @return Closure
 */
return fn( string $route , string $controller ):Closure => function( App $app ) use ( $controller , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
        $route   = '/' . $route . '/{id:[0-9]+}/description';

        $app->options ( $route , $options );
        $app->get     ( $route , $controller . ':description'      )->setName( 'api.' . $route . '.description.all'  );
        $app->patch   ( $route , $controller . ':patchDescription' )->setName( 'api.' . $route . '.description.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};