<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the alternateName route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/alternateName.php' )
 * (
 *     'places',
 *      PlaceTranslationController::class
 * )( $app ) ;
 * </code>
 * @param string $route
 * @param string $controller
 * @return Closure
 */
return fn( string $route , string $controller ):Closure => function( App $app ) use ( $controller , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
        $route   = '/' . $route . '/{id:[0-9]+}/alternateName';

        $app->options ( $route , $options );
        $app->get     ( $route , $controller . ':alternateName'      )->setName( 'api.' . $route . '.alternateName.all'  );
        $app->patch   ( $route , $controller . ':patchAlternateName' )->setName( 'api.' . $route . '.alternateName.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};