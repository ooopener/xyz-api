<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the audios route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/audios.php' )
 * (
 *     'places',
 *     'placeAudiosController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID ) ;

        $all      = '/'  . $route . '/{id:[0-9]+}/audios' ;
        $count    = $all . '/count' ;
        $thing    = '/'  . $route . '/{owner:[0-9]+}/audios/{id:[0-9]+}' ;
        $position = $thing . '/position/{position:[0-9]+}' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $all , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->get     ( $all , [ $controller , 'all'       ] )->setName( 'api.' . $route . '.audios.all'  ) ;
        $app->delete  ( $all , [ $controller , 'deleteAll' ] )->setName( 'api.' . $route . '.audios.delete.all' ) ;
        $app->post    ( $all , [ $controller , 'post'      ] )->setName( 'api.' . $route . '.audios.post' ) ;

        $app->options ( $count , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->get     ( $count , [ $controller , 'count' ] )->setName( 'api.' . $route . '.audios.count' ) ;

        $app->options ( $thing, [ $controller , __NOBODY_RESPONSE__ ] );
        $app->get     ( $thing, [ $controller , 'get'    ] )->setName( 'api.' . $route . '.audios.get'    ) ;
        $app->delete  ( $thing, [ $controller , 'delete' ] )->setName( 'api.' . $route . '.audios.delete' ) ;

        $app->options ( $position , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->patch   ( $position , [ $controller , 'patchPosition'  ] )->setName( 'api.' . $route . '.audios.position' ) ;
    }
    else
    {
        $container->get( LoggerInterface::class )
                  ->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
