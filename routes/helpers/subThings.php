<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the subThings route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/subThings.php' )
 * (
 *     'containsPlace',
 *     'places',
 *     'placeContainsPlacesController'
 * ) ;
 * </code>
 * @param string $name
 * @param string $route
 * @param string $controllerID
 * @param array $options The options to activate/deactivate some routes.
 * @return Closure
 */
return fn( string $name , string $route , string $controllerID , array $options = []  ):Closure
       => function( App $app ) use ( $controllerID, $name , $options , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;

    if( $container->has( $controllerID ) )
    {
        $hasCount     = isset( $options[ 'hasCount'     ] ) ? (bool) $options[ 'hasCount'     ] : FALSE ;
        $hasDeleteAll = isset( $options[ 'hasDeleteAll' ] ) ? (bool) $options[ 'hasDeleteAll' ] : FALSE ;

        $controller = $container->get( $controllerID );

        $things  = '/' . $route . '/{id:[0-9]+}/'    . $name ;
        $thing   = '/' . $route . '/{owner:[0-9]+}/' . $name . '/{id:[0-9]+}';

        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $things , $options );
        $app->get     ( $things , [ $controller , 'all' ] )->setName( 'api.' . $route . '.' . $name . '.all'  );

        if( $hasDeleteAll )
        {
            $app->delete( $things , [ $controller , 'deleteAll' ] )->setName( 'api.' . $route . '.' . $name . '.deleteAll'  );
        }

        if( $hasCount )
        {
            $count = $things . '/count';
            $app->options ( $count , $options );
            $app->get     ( $count , [ $controller , 'count'])->setName( 'api.' . $route . '.'. $name . '.count' );
        }

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.'. $name . '.delete' );
        $app->post    ( $thing , [ $controller , 'post'   ] )->setName( 'api.' . $route . '.'. $name . '.post'    );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};