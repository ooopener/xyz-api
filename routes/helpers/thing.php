<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the basic thing route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/thing.php' )
 * (
 *     'places',
 *     PlacesController::class
 * ) ;
 * </code>
 * @param string $route
 * @param string $controller
 * @param array $options The options to activate/deactivate some routes.
 * @return Closure
 */
return fn( string $route , string $controller , array $options = [] ):Closure => function( App $app ) use ( $controller , $route , $options )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;

    $hasAll       = isset( $options[ 'hasAll'       ] ) ? (bool) $options[ 'hasAll'       ] : TRUE ;
    $hasCount     = isset( $options[ 'hasCount'     ] ) ? (bool) $options[ 'hasCount'     ] : TRUE ;
    $hasDelete    = isset( $options[ 'hasDelete'    ] ) ? (bool) $options[ 'hasDelete'    ] : TRUE ;
    $hasDeleteAll = isset( $options[ 'hasDeleteAll' ] ) ? (bool) $options[ 'hasDeleteAll' ] : TRUE ;
    $hasGet       = isset( $options[ 'hasGet'       ] ) ? (bool) $options[ 'hasGet'       ] : TRUE ;
    $hasPatch     = isset( $options[ 'hasPatch'     ] ) ? (bool) $options[ 'hasPatch'     ] : TRUE ;
    $hasPost      = isset( $options[ 'hasPost'      ] ) ? (bool) $options[ 'hasPost'      ] : TRUE ;
    $hasPut       = isset( $options[ 'hasPut'       ] ) ? (bool) $options[ 'hasPut'       ] : TRUE ;

    if( $container->has( $controller ) )
    {
        // ----------------------

        $all     = '/' . $route ;
        $count   = $all . '/count' ;
        $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
        $thing   = $all . '/{id:[0-9]+}' ;

        // ----------------------

        if( $hasAll || $hasDeleteAll || $hasPost )
        {
            $app->options( $all, $options);
        }

        if( $hasAll )
        {
            $app->get( $all , $controller . ':all' )->setName('api.' . $route . '.all') ;
        }

        if( $hasDeleteAll )
        {
            $app->delete( $all , $controller . ':deleteAll' )->setName('api.' . $route . '.delete.all') ;
        }

        if( $hasPost )
        {
            $app->post( $all , $controller . ':post' )->setName('api.' . $route . '.post') ;
        }

        // ----------------------

        if( $hasCount )
        {
            $app->options ( $count , $options ) ;
            $app->get     ( $count , $controller . ':count' )->setName('api.' . $route . '.count') ;
        }

        // ----------------------

        if( $hasGet || $hasDelete || $hasPatch || $hasPut )
        {
            $app->options ( $thing , $options );
        }

        if( $hasGet )
        {
            $app->get( $thing , $controller . ':get' )->setName('api.' . $route . '.get');
        }

        if( $hasDelete )
        {
            $app->delete( $thing , $controller . ':delete' )->setName('api.' . $route . '.delete');
        }

        if( $hasPatch )
        {
            $app->patch( $thing , $controller . ':patch' )->setName('api.' . $route . '.patch');
        }

        if( $hasPut )
        {
            $app->put( $thing , $controller . ':put' )->setName('api.' . $route . '.put');
        }

        // ----------------------
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};