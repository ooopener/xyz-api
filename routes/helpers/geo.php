<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the geo route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/geo.php' )
 * (
 *     'places',
 *     'placeGeoCoordinatesController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID );
        $thing      = '/' . $route . '/{id:[0-9]+}/geo';

        $app->options ( $thing , [ $controller , __NOBODY_RESPONSE__ ] );
        $app->get     ( $thing , [ $controller, 'get'  ] )->setName( 'api.' . $route . '.geo.get'   );
        $app->patch   ( $thing , [ $controller, 'patch'] )->setName( 'api.' . $route . '.geo.patch' );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};