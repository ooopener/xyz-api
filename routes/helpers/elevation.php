<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the elevation route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/elevation.php' )
 * (
 *     'courses',
 *     'courseElevationController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID );
        $thing      = '/' . $route . '/{id:[0-9]+}/elevation';

        $app->options ( $thing , [ $controller , __NOBODY_RESPONSE__ ] );
        $app->get     ( $thing , [ $controller, 'get'  ] )->setName( 'api.' . $route . '.elevation.get'   );
        $app->patch   ( $thing , [ $controller, 'patch'] )->setName( 'api.' . $route . '.elevation.patch' );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};