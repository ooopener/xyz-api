<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the superThings route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/superThings.php' )
 * (
 *     'containedInPlace',
 *     'places',
 *     'placeContainsPlacesController'
 * ) ;
 * </code>
 * @param string $name
 * @param string $route
 * @param string $controllerID
 * @param bool $countable Indicates if the /count route is registered or not (default FALSE).
 * @param bool $peopleAuthority
 *
 * @return Closure
 */
return fn( string $name , string $route , string $controllerID , bool $countable = FALSE , bool $peopleAuthority = FALSE ):Closure
       => function( App $app ) use ( $peopleAuthority , $controllerID , $countable , $name , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID );

        $things  = '/' . $route . '/{id:[0-9]+}/' . $name ;
        $thing   = '/' . $route . '/{owner:[0-9]+}/' . $name . '/{id:[0-9]+}';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $things , $options );
        $app->get     ( $things , [ $controller , 'allReverse' ] )->setName( 'api.' . $route . '.' . $name . '.all'  );

        if( $countable )
        {
            $count = $things . '/count';
            $app->options ( $count , $options );
            $app->get     ( $count , [ $controller , 'countReverse' ])->setName( 'api.' . $route . '.'. $name . '.count' );
        }

        if( $peopleAuthority == TRUE )
        {
            $deleteReverse = 'deleteReversePeople' ;
            $postReverse = 'postReversePeople' ;
        }
        else
        {
            $deleteReverse = 'deleteReverse' ;
            $postReverse = 'postReverse' ;
        }

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , $deleteReverse ] )->setName( 'api.' . $route . '.'. $name . '.delete' );
        $app->post    ( $thing , [ $controller , $postReverse   ] )->setName( 'api.' . $route . '.'. $name . '.put'    );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
