<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the job route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/job.php' )
 * (
 *     'people',
 *     'peopleJobsController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID );

        $all     = '/' . $route . '/{id:[0-9]+}/job';
        $count   = $all . '/count';
        $thing   = '/' . $route . '/{owner:[0-9]+}/job/{id:[0-9]+}';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $all ,$options );
        $app->get     ( $all , [ $controller , 'get' ] )->setName( 'api.' . $route . '.job.get'  );

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.job.delete' );
        $app->post    ( $thing , [ $controller , 'post'    ] )->setName( 'api.' . $route . '.job.post'   );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
