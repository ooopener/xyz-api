<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the 'logo' route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/logo.php' )
 * (
 *     'places',
 *     'placeLogoController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get( $controllerID ) ;

        $thing = '/' . $route . '/{id:[0-9]+}/logo' ;
        $patch = '/' . $route . '/{owner:[0-9]+}/logo/{id:[0-9]+}' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $thing , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->get     ( $thing , [ $controller , 'get'    ] )->setName( 'api.' . $route . '.logo.get'    );
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.logo.delete' );

        $app->options ( $patch , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->patch   ( $patch , [ $controller , 'patch' ] ) ->setName( 'api.' . $route . '.logo.patch' );
    }
    else
    {
        $container->get( LoggerInterface::class )
                  ->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
