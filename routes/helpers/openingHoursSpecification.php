<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the openingHoursSpecification route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/openingHoursSpecification.php' )
 * (
 *     'places',
 *     'placeOpeningHoursController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);

        $all     = '/' . $route . '/{id:[0-9]+}/openingHoursSpecification';
        $count   = $all . '/count';
        $thing   = '/' . $route . '/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $all ,$options );
        $app->get     ( $all , [ $controller , 'all'  ] )->setName( 'api.' . $route . '.openingHoursSpecification.all'  );
        $app->post    ( $all , [ $controller , 'post' ] )->setName( 'api.' . $route . '.openingHoursSpecification.post' );

        $app->options ( $count , $options );
        $app->get     ( $count , [ $controller , 'count'])->setName( 'api.' . $route . '.openingHoursSpecification.count' );

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName( 'api.' . $route . '.openingHoursSpecification.delete' );
        $app->put     ( $thing , [ $controller , 'put'    ] )->setName( 'api.' . $route . '.openingHoursSpecification.put'    );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};