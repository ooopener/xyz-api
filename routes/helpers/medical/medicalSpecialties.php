<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the active route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/medical/medicalSpecialties.php' )
 * (
 *     'technicians',
 *     'technicianMedicalSpecialtiesController'
 * )( $app ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @param string $name
 * @return Closure
 */
return fn( string $route , string $controllerID , $name = 'medicalSpecialties' ):Closure => function( App $app ) use ( $controllerID , $name , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);

        $all     = '/' . $route . '/{id:[0-9]+}/' . $name ;
        $count   = $all . '/count';
        $thing   = '/' . $route . '/{owner:[0-9]+}/' . $name . '/{id:[0-9]+}';
        $options = [ $controller , __NOBODY_RESPONSE__ ] ;

        $app->options ( $all , $options ) ;
        $app->get     ( $all , [ $controller , 'all' ] )->setName('api.' . $route . '.' . $name . '.all') ;

        $app->options ( $count , $options ) ;
        $app->get     ( $count , [ $controller , 'count' ] )->setName('api.' . $route . '.' . $name . '.count') ;

        $app->options ( $thing , $options ) ;
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName('api.' . $route . '.' . $name . '.delete') ;
        $app->post    ( $thing , [ $controller , 'post'   ] )->setName('api.' . $route . '.' . $name . '.post') ;
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};