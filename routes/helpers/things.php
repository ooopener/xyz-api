<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the specific things route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/things.php' )( 'keywords' )
 * (
 *     'places',
 *     'placeKeywordsController'
 * ) ;
 * </code>
 * @param string $name
 * @param array $options The options to activate/deactivate some routes.
 * @return Closure
 */
return fn( string $name , array $options = [] )
    => fn( string $route , string $controller ):Closure =>
    function( App $app ) use ( $controller , $name , $options , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;

    if( $container->has( $controller ) )
    {
        $hasCount  = isset( $options[ 'hasCount'  ] ) ? (bool) $options[ 'hasCount' ] : TRUE ;
        $hasDelete = isset( $options[ 'hasDelete' ] ) ? (bool) $options[ 'hasDelete'] : TRUE ;
        $hasPost   = isset( $options[ 'hasPost'   ] ) ? (bool) $options[ 'hasPost'  ] : TRUE ;
        $hasPut    = isset( $options[ 'hasPut'    ] ) ? (bool) $options[ 'hasPut'   ] : TRUE ;
        $useID     = isset( $options[ 'useIndex'  ] ) ? (bool) $options[ 'useIndex' ] : TRUE ;

        if( $useID )
        {
            $controller = $container->get( $controller );
            $options = [ $controller , __NOBODY_RESPONSE__ ];
            $all     = [ $controller , 'all'    ] ;
            $delete  = [ $controller , 'delete' ] ;
            $count   = [ $controller , 'count'  ] ;
            $post    = [ $controller , 'post'   ] ;
            $put     = [ $controller , 'put'    ] ;
        }
        else
        {
            $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
            $all     = $controller . ':all'    ;
            $delete  = $controller . ':delete' ;
            $count   = $controller . ':count'  ;
            $post    = $controller . ':post'   ;
            $put     = $controller . ':put'   ;
        }

        $thing      = '/' . $route . '/{owner:[0-9]+}/' . $name . '/{id:[0-9]+}';
        $things     = '/' . $route . '/{id:[0-9]+}/'    . $name ;
        $thingCount = $things . '/count' ;

        $app->options ( $things , $options );
        $app->get     ( $things , $all  )->setName( 'api.' . $route . '.' . $name . '.all'  );
        $app->post    ( $things , $post )->setName( 'api.' . $route . '.' . $name . '.post' );

        if( $hasCount )
        {
            $app->options ( $thingCount , $options );
            $app->get     ( $thingCount , $count )->setName( 'api.' . $route . '.' . $name . '.count' );
        }

        if( $hasDelete || $hasPut )
        {
            $app->options ( $thing , $options ) ;
        }

        if( $hasDelete )
        {
            $app->delete( $thing , $delete )->setName( 'api.' . $route . '.' . $name . '.delete' );
        }

        if( $hasPut )
        {
            $app->put( $thing , $put )->setName( 'api.' . $route . '.' . $name . '.put'    );
        }
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};