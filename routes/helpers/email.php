<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the email route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/email.php' )
 * (
 *     'places',
 *     'placeEmailsController'
 * ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);

        $all   = '/' . $route . '/{id:[0-9]+}/email';
        $count = $all . '/count';
        $thing = '/' . $route . '/{owner:[0-9]+}/email/{id:[0-9]+}';

        $app->options ( $all , [ $controller , __NOBODY_RESPONSE__]);
        $app->get     ( $all , [ $controller , 'all'  ] )->setName('api.' . $route . '.email.all'  );
        $app->post    ( $all , [ $controller , 'post' ] )->setName('api.' . $route . '.email.post' );

        $app->options ( $count , [ $controller , __NOBODY_RESPONSE__]);
        $app->get     ( $count , [ $controller , 'count' ] )->setName('api.' . $route . '.email.count' );

        $app->options ( $thing , [ $controller , __NOBODY_RESPONSE__]);
        $app->delete  ( $thing , [ $controller , 'delete' ] )->setName('api.' . $route . '.email.delete' );
        $app->put     ( $thing , [ $controller , 'put'    ] )->setName('api.' . $route . '.email.put'    );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};