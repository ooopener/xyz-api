<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the address route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/discover.php' )
 * (
 *     'stages',
 *      StageDiscoverController::class
 * ) ;
 * </code>
 * @param string $route
 * @param string $controller
 * @return Closure
 */
return fn( string $route , string $controller ):Closure => function( App $app ) use ( $controller , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $route = '/' . $route . '/{id:[0-9]+}/discover';

        $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ );
        $app->get     ( $route , $controller . ':get'    )->setName( 'api.' . $route . '.discover.all'    );
        $app->delete  ( $route , $controller . ':delete' )->setName( 'api.' . $route . '.discover.delete' );
        // TODO add the deleteAll method + hasDeleteAll
        $app->patch   ( $route , $controller . ':patch'  )->setName( 'api.' . $route . '.discover.patch'  );
        $app->post    ( $route , $controller . ':post'   )->setName( 'api.' . $route . '.discover.post'   );
    }
    else
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};
