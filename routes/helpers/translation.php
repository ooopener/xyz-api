<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the translation route helper function.
 * Here is an inline example:
 * <code>
 * $translation = ( require __DIR__ . '/../helpers/translation.php' )
 * (
 *     'places',
 *     PlaceTranslationController::class
 * );
 *
 * $translation( 'alternateName' )( $app );
 * </code>
 * @param string $route The route name.
 * @param string $controller
 * @param array $init
 * @return Closure
 */
return fn
(
    string $route ,
    string $controller ,
    array  $init = []
):Closure
=>
fn( string $name ) => function( App $app )
                      use ( $name , $controller , $init , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $useID = isset( $init[ 'useIndex' ] )
                         ? (bool) $init[ 'useIndex' ]
                         : FALSE ;

        $i18n = '/' . $route . '/{id}/' . $name ;

        if( $useID )
        {
            $controller = $container->get( $controller ) ;
            $get     = [ $controller , $name ];
            $options = [ $controller , __NOBODY_RESPONSE__ ] ;
            $patch   = [ $controller , 'patch' . ucfirst( $name ) ] ;
        }
        else
        {
            $get     = $controller . ':' . $name ;
            $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
            $patch   = $controller . ':patch' . ucfirst( $name ) ;
        }

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $i18n , $options );
        $app->get     ( $i18n , $get     )->setName( 'api.' . $route . '.' . $name . '.all'  );
        $app->patch   ( $i18n , $patch   )->setName( 'api.' . $route . '.' . $name . '.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};