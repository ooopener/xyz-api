<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the notes route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/notes.php' )
 * (
 *     'places',
 *      PlaceTranslationController::class
 * )( $app ) ;
 * </code>
 * @param string $route
 * @param string $controller
 * @return Closure
 */
return fn( string $route , string $controller ):Closure => function( App $app ) use ( $controller , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controller ) )
    {
        $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
        $route   = '/' . $route . '/{id:[0-9]+}/notes';

        $app->options ( $route , $options );
        $app->get     ( $route , $controller . ':notes'      )->setName( 'api.' . $route . '.notes.all'  );
        $app->patch   ( $route , $controller . ':patchNotes' )->setName( 'api.' . $route . '.notes.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controller . '\' is not registered in the DI container.' ) ;
    }
};