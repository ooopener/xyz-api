<?php

use Psr\Log\LoggerInterface;
use Slim\App ;

/**
 * Return the active route helper function.
 * Here is an inline example:
 * <code>
 * return ( require __DIR__ . '/../helpers/active.php' )
 * (
 *     'places',
 *     'placeActiveController'
 * )( $app ) ;
 * </code>
 * @param string $route
 * @param string $controllerID
 * @return Closure
 */
return fn( string $route , string $controllerID ):Closure => function( App $app ) use ( $controllerID , $route )
{
    $container = $app->getContainer() ;
    $logger    = $container->get( LoggerInterface::class ) ;
    if( $container->has( $controllerID ) )
    {
        $controller = $container->get($controllerID);
        $active     = '/' . $route . '/{id:[0-9]+}/active';
        $patch      = $active . '/{bool:true|false|TRUE|FALSE}' ;

        if( strpos( $route , '/' ) !== FALSE )
        {
            $route = str_replace( '/' , '.' , $route ) ; // Transform the 'foo/bar' route path in 'foo.bar'
        }

        $app->options ( $active , [ $controller , __NOBODY_RESPONSE__ ] );
        $app->get     ( $active , [ $controller , 'get'   ] )->setName( 'api.' . $route . '.active.get'  );

        $app->options ( $patch , [ $controller , __NOBODY_RESPONSE__ ] ) ;
        $app->patch   ( $patch , [ $controller , 'patch' ] )->setName( 'api.' . $route . '.active.patch' );
    }
    else if( $logger )
    {
        $logger->warning( __FILE__ . ' failed, the controller \'' . $controllerID . '\' is not registered in the DI container.' ) ;
    }
};
