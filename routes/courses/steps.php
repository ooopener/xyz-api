<?php

use Slim\App ;

return function( App $app )
{
    ( require "steps/steps.php"       ) ( $app ) ;

    ( require "steps/active.php"      ) ( $app ) ;
    ( require "steps/audio.php"       ) ( $app ) ;
    ( require "steps/audios.php"      ) ( $app ) ;
    ( require "steps/image.php"       ) ( $app ) ;
    ( require "steps/photos.php"      ) ( $app ) ;
    ( require "steps/translation.php" ) ( $app ) ;
    ( require "steps/video.php"       ) ( $app ) ;
    ( require "steps/videos.php"      ) ( $app ) ;
    ( require "steps/withStatus.php"  ) ( $app ) ;
};
