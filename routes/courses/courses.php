<?php

use xyz\ooopener\controllers\courses\CoursesController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'courses' ,
    CoursesController::class ,
    [
        'hasPatch' => FALSE
    ]
) ;

/**
 * @OA\Get( path="/courses",
 *     tags={"courses"},
 *     description="List courses",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/courseListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/courses",
 *     tags={"courses"},
 *     description="Delete courses from list",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/NotValidList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Post(
 *     path="/courses",
 *     tags={"courses"},
 *     description="Create a new course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postCourse"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/courseResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/courses/count",
 *     tags={"courses"},
 *     description="Count courses",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of courses",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of courses",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/courses/{id}",
 *     tags={"courses"},
 *     description="Get course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/courseResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/courses/{id}",
 *     tags={"courses"},
 *     description="Delete course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Patch(
 *     path="/courses/{id}",
 *     tags={"courses"},
 *     description="Patch a course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchCourse"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/courseResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Put(
 *     path="/courses/{id}",
 *     tags={"courses"},
 *     description="Edit a course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putCourse"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/courseResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
