<?php

use Slim\App ;

use xyz\ooopener\controllers\steps\StepsController ;

return function( App $app )
{
    $controller = StepsController::class ;
    $path       = '/courses/{id:[0-9]+}/steps' ;
    $count      = $path . '/count' ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $position   = '/courses/{owner:[0-9]+}/steps/{id:[0-9]+}/position/{position:[0-9]+}' ;
    $thing      = '/courses/steps/{id:[0-9]+}' ;

    $app->options( $path , $options ) ;
    $app->get    ( $path , $controller . ':all' )->setName( 'api.courses.steps.all'  ) ;
    $app->post   ( $path , $controller . ':post')->setName( 'api.courses.steps.post' ) ;

    $app->options ( $count , $options ) ;
    $app->get     ( $count , $controller . ':count')->setName( 'api.courses.steps.count' ) ;

    $app->options ( $thing , $options ) ;
    $app->get     ( $thing , $controller . ':get'    )->setName( 'api.courses.steps.get'    ) ;
    $app->delete  ( $thing , $controller . ':delete' )->setName( 'api.courses.steps.delete' ) ;
    $app->put     ( $thing , $controller . ':put'    )->setName( 'api.courses.steps.put'    ) ;

    $app->options ( $position , $options ) ;
    $app->patch   ( $position , $controller . ':patchPosition')->setName('api.course.steps.position.patch');
};

/**
 * @OA\Get(
 *     path="/courses/{id}/steps",
 *     tags={"courses"},
 *     description="List course steps",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stepListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Post(
 *     path="/courses/{id}/steps",
 *     tags={"courses"},
 *     description="Create a new course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postStep"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stepResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/courses/{id}/steps/count",
 *     tags={"courses"},
 *     description="Count course steps",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of course steps",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of course steps",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/courses/{owner}/steps/{id}/position/{position}",
 *     tags={"courses"},
 *     description="Patch course step position",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="position",in="path",description="",required=true,@OA\Schema(type="integer")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stepResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Delete(
 *     path="/courses/steps/{id}",
 *     tags={"courses"},
 *     description="Delete course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/courses/steps/{id}",
 *     tags={"courses"},
 *     description="Get course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stepResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Put(
 *     path="/courses/steps/{id}",
 *     tags={"courses"},
 *     description="Edit a course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putStep"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stepResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
