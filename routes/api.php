<?php

use Slim\App ;

use xyz\ooopener\controllers\APIController ;

return function( App $app )
{
    $controller = APIController::class ;

    $app->get( ''         , $controller . ':index'   )->setName( 'api.index'   );
    $app->get( '/info'    , $controller . ':info'    )->setName( 'api.info'    );
    $app->get( '/version' , $controller . ':version' )->setName( 'api.version' );
};
