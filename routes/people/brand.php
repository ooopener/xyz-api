<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'brand' ,
    'people',
    'personHasBrandController' ,
    [
        'hasCount'     => TRUE ,
        'hasDeleteAll' => TRUE
    ]
) ;
