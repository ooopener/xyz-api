<?php

return ( require __DIR__ . '/../helpers/superThings.php' )
(
    'memberOf' ,
    'people',
    'organizationMembersController',
    false,
    true
) ;

/**
     * @OA\Get(
     *     path="/people/{id}/memberOf",
     *     tags={"people"},
     *     description="Get memberOf person",
     *     security={
     *         {"OAuth2"={
     *             "people:A",
     *             "people:W",
     *             "people:R"
     *         }}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         ref="#/components/responses/personListResponse"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         ref="#/components/responses/Unauthorized"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         ref="#/components/responses/Unexpected"
     *     )
     * )
     */
/**
     * @OA\Get(
     *     path="/people/{id}/memberOf/count",
     *     tags={"people"},
     *     description="Count person memberOf",
     *     security={
     *         {"OAuth2"={
     *             "people:A",
     *             "people:W",
     *             "people:R"
     *         }}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         description="",
     *         @OA\JsonContent(
     *             type="object",
     *             allOf={@OA\Schema(ref="#/components/schemas/success")},
     *             @OA\Property(property="result",description="Number of memberOf",type="integer")
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         ref="#/components/responses/Unauthorized"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         ref="#/components/responses/Unexpected"
     *     )
     * )
     */
/**
     * @OA\Delete(
     *     path="/people/{owner}/memberOf/{id}",
     *     tags={"people"},
     *     description="Delete memberOf person",
     *     security={
     *         {"OAuth2"={
     *             "people:A",
     *             "people:W"
     *         }}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/owner"),
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         ref="#/components/responses/personListResponse"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         ref="#/components/responses/Unauthorized"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         ref="#/components/responses/Unexpected"
     *     )
     * )
     */
/**
     * @OA\Post(
     *     path="/people/{owner}/memberOf/{id}",
     *     tags={"people"},
     *     description="Create memberOf person",
     *     security={
     *         {"OAuth2"={
     *             "people:A",
     *             "people:W"
     *         }}
     *     },
     *     @OA\Parameter(ref="#/components/parameters/owner"),
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         ref="#/components/responses/personListResponse"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         ref="#/components/responses/Unauthorized"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         ref="#/components/responses/Unexpected"
     *     )
     * )
     */
