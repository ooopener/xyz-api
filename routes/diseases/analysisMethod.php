<?php

$things = ( require __DIR__ . '/../helpers/things.php' ) ;

return $things('analysisMethod')
(
    'diseases',
    'diseaseAnalysisMethodController'
);