<?php

use xyz\ooopener\controllers\diseases\DiseasesController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'diseases' ,
    DiseasesController::class
) ;
