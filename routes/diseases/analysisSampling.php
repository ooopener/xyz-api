<?php

$things = ( require __DIR__ . '/../helpers/things.php' ) ;

return $things('analysisSampling')
(
    'diseases',
    'diseaseAnalysisSamplingController'
);

