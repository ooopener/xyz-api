<?php

use Slim\App ;

use xyz\ooopener\controllers\diseases\DiseasesTranslationController;

return function( App $app )
{
   $translation = ( require __DIR__ . '/../helpers/translation.php' )
   (
       'diseases',
       DiseasesTranslationController::class
   );

   $translation( 'alternateName' )( $app );
   $translation( 'description'   )( $app );
   $translation( 'notes'         )( $app );
   $translation( 'text'          )( $app );
};

