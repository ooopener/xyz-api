<?php

use Slim\App ;

use xyz\ooopener\controllers\places\PlacesController ;

return function( App $app )
{
    $controller = PlacesController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $near  = '/places/near[/{id:[0-9]+}]' ;

    $app->options ( $near , $options ) ;
    $app->get     ( $near , $controller . ':near'  )->setName('api.places.near');
};

/**
*
 * @OA\Get(
 *     path="/places/near",
 *     tags={"places"},
 *     description="Returns all the elements near the default position",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(name="latitude",in="query",description="The latitude of a location",@OA\Schema(type="number",minimum=-90,maximum=90)),
 *     @OA\Parameter(name="longitude",in="query",description="The longitude of a location",@OA\Schema(type="number",minimum=-180,maximum=180)),
 *     @OA\Parameter(name="distance",in="query",description="The distance in meter",@OA\Schema(type="number",minimum=0)),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/places/near/{id}",
 *     tags={"places"},
 *     description="Returns all the elements near the place ID",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="distance",in="query",description="The distance in meter",@OA\Schema(type="number",minimum=0)),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
