<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'brand' ,
    'places',
    'placeHasBrandController' ,
    [
        'hasCount'     => TRUE ,
        'hasDeleteAll' => TRUE
    ]
) ;
