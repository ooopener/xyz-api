<?php

use Slim\App ;

use xyz\ooopener\controllers\applications\ApplicationTranslationController ;

return function( App $app )
{
    $translation = ( require __DIR__ . '/../helpers/translation.php' )
    (
        'applications',
        ApplicationTranslationController::class
    );

    $translation( 'alternativeHeadline' )( $app );
    $translation( 'description'         )( $app );
    $translation( 'headline'            )( $app );
    $translation( 'notes'               )( $app );
    $translation( 'text'                )( $app );
    $translation( 'slogan'              )( $app );
};

/**
 * @OA\Get(
 *     path="/applications/{id}/alternativeHeadline",
 *     tags={"applications"},
 *     description="Get application alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/alternativeHeadline",
 *     tags={"applications"},
 *     description="Patch application alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}/notes",
 *     tags={"applications"},
 *     description="Get application notes",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/notes",
 *     tags={"applications"},
 *     description="Patch application notes",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}/headline",
 *     tags={"applications"},
 *     description="Get application headline",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/headline",
 *     tags={"applications"},
 *     description="Patch application headline",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}/description",
 *     tags={"applications"},
 *     description="Get application description",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/description",
 *     tags={"applications"},
 *     description="Patch application description",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}/slogan",
 *     tags={"applications"},
 *     description="Get application slogan",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/slogan",
 *     tags={"applications"},
 *     description="Patch application slogan",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}/text",
 *     tags={"applications"},
 *     description="Get application text",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/text",
 *     tags={"applications"},
 *     description="Patch application text",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
