<?php

return ( require __DIR__ . '/../helpers/video.php' )
(
    'applications',
    'applicationVideoController'
) ;


/**
 * @OA\Get(
 *     path="/applications/{id}/video",
 *     tags={"applications"},
 *     description="Get application video",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Delete(
 *     path="/applications/{id}/video",
 *     tags={"applications"},
 *     description="Delete resource video",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{owner}/video/{id}",
 *     tags={"applications"},
 *     description="Patch resource video",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
