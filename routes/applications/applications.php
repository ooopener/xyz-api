<?php

use Slim\App ;

use xyz\ooopener\controllers\applications\ApplicationsController ;


return function( App $app )
{
    $controller = ApplicationsController::class ;

    $all   = '/applications' ;
    $count = $all . '/count' ;
    $thing = '/applications/{id}' ;

    $app->options ( $all , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $all , $controller . ':all'  )->setName('api.applications.all') ;
    $app->post    ( $all , $controller . ':post' )->setName('api.applications.post');

    $app->options ( $count , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $count , $controller . ':count')->setName('api.applications.count');

    $app->options ( $thing , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $thing , $controller . ':get'    )->setName( 'api.applications.get'    ) ;
    $app->delete  ( $thing , $controller . ':delete' )->setName( 'api.applications.delete' );
    $app->patch   ( $thing , $controller . ':patch'  )->setName( 'api.applications.patch'  );

    $basic   = '/app';
    $oauth   = '/applications/{id}/oAuth' ;
    $setting = '/applications/{id}/setting' ;
    $secret  = '/applications/{id}/oAuth/secret' ;

    $app->options ( $basic    , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $basic    , $controller . ':getApp' )->setName('api.applications.get.app') ;
    $app->options ( $oauth    , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->patch   ( $oauth    , $controller . ':patchOAuth' )->setName('api.applications.patch.oauth');
    $app->options ( $secret   , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->patch   ( $secret   , $controller . ':patchSecret' )->setName('api.applications.patch.secret');
    $app->options ( $setting  , $controller . __CLAZZ_NOBODY_RESPONSE__) ;
    $app->patch   ( $setting  , $controller . ':patchSetting' )->setName('api.applications.patch.setting');
};

/**
 * @OA\Get(
 *     path="/applications",
 *     tags={"applications"},
 *     description="List applications",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Post(
 *     path="/applications",
 *     tags={"applications"},
 *     description="Create a new application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postApplication"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/count",
 *     tags={"applications"},
 *     description="Count applications",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of applications",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of applications",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/app",
 *     tags={"applications"},
 *     description="Get current application from access token",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse",
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/applications/{id}",
 *     tags={"applications"},
 *     description="Get application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse",
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Delete(
 *     path="/applications/{id}",
 *     tags={"applications"},
 *     description="Delete application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}",
 *     tags={"applications"},
 *     description="Patch a application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchApplication"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/oAuth",
 *     tags={"applications"},
 *     description="Patch application oAuth url redirect",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchApplicationOAuth"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/setting",
 *     tags={"applications"},
 *     description="Patch application setting",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchApplicationSetting"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Patch(
 *     path="/applications/{id}/oAuth/secret",
 *     tags={"applications"},
 *     description="Patch to regenerate application oAuth secret",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/applicationResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
