<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="technicians",
 *     description="Technicians paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'technicians/technicians.php' ) ( $app ) ;

    ( require 'technicians/active.php'             ) ( $app ) ;
    ( require 'technicians/livestocks.php'         ) ( $app ) ;
    ( require 'technicians/medicalSpecialties.php' ) ( $app ) ;
};
