<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="courses",
 *     description="The API /courses route path"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'courses/courses.php' ) ( $app ) ;

    ( require 'courses/active.php'          ) ( $app ) ;
    ( require 'courses/additionalType.php'  ) ( $app ) ;
    ( require 'courses/audio.php'           ) ( $app ) ;
    ( require 'courses/audios.php'          ) ( $app ) ;
    ( require 'courses/discover.php'        ) ( $app ) ;
    ( require 'courses/elevation.php'       ) ( $app ) ;
    ( require 'courses/image.php'           ) ( $app ) ;
    ( require 'courses/openingHours.php'    ) ( $app ) ;
    ( require 'courses/photos.php'          ) ( $app ) ;
    ( require 'courses/steps.php'           ) ( $app ) ;
    ( require 'courses/translation.php'     ) ( $app ) ;
    ( require 'courses/transportations.php' ) ( $app ) ;
    ( require 'courses/video.php'           ) ( $app ) ;
    ( require 'courses/videos.php'          ) ( $app ) ;
    ( require 'courses/withStatus.php'      ) ( $app ) ;
};
