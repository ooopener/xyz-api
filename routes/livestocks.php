<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="livestocks",
 *     description="Livestocks paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'livestocks/livestocks.php' ) ( $app ) ;

    ( require 'livestocks/active.php' ) ( $app ) ;
    ( require 'livestocks/numbers.php') ( $app ) ;

    ( require 'livestocks/observations/actor.php'        ) ( $app ) ;
    ( require 'livestocks/observations/attendee.php'     ) ( $app ) ;
    ( require 'livestocks/observations/authority.php'    ) ( $app ) ;
    ( require 'livestocks/observations/observations.php' ) ( $app ) ;
    ( require 'livestocks/observations/translation.php'  ) ( $app ) ;

    ( require 'livestocks/observations/audio.php'   ) ( $app ) ;
    ( require 'livestocks/observations/audios.php'  ) ( $app ) ;
    ( require 'livestocks/observations/image.php'   ) ( $app ) ;
    ( require 'livestocks/observations/photos.php'  ) ( $app ) ;
    ( require 'livestocks/observations/video.php'   ) ( $app ) ;
    ( require 'livestocks/observations/videos.php'  ) ( $app ) ;

    ( require 'livestocks/medicalLaboratories.php'       ) ( $app ) ;
    ( require 'livestocks/sectors.php'                   ) ( $app ) ;
    ( require 'livestocks/technicians.php'               ) ( $app ) ;
    ( require 'livestocks/veterinarians.php'             ) ( $app ) ;
    ( require 'livestocks/workplaces.php'                ) ( $app ) ;
    ( require 'livestocks/workshops.php'                 ) ( $app ) ;
};
