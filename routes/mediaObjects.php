<?php

use Slim\App ;

use xyz\ooopener\controllers\creativeWork\MediaObjectsController ;


return function( App $app )
{
    $controller = MediaObjectsController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $app->options ( '/mediaObjects' , $options ) ;
    $app->get     ( '/mediaObjects' , $controller . ':all'       )->setName( 'api.mediaObjects.all' );
    $app->delete  ( '/mediaObjects' , $controller . ':deleteAll' )->setName( 'api.mediaObjects.delete.all' );
    $app->post    ( '/mediaObjects' , $controller . ':post'      )->setName( 'api.mediaObjects.post' );

    $app->options ( '/mediaObjects/exists' , $options ) ;
    $app->get     ( '/mediaObjects/exists' , $controller . ':exists' )->setName( 'api.mediaObjects.exists' );

    $app->options ( '/mediaObjects/count' , $options ) ;
    $app->get     ( '/mediaObjects/count' , $controller . ':count' )->setName( 'api.mediaObjects.count' );

    $app->options ( '/mediaObjects/{id}' , $options ) ;
    $app->get     ( '/mediaObjects/{id}' , $controller . ':get'    )->setName( 'api.mediaObjects.get' );
    $app->delete  ( '/mediaObjects/{id}' , $controller . ':delete' )->setName( 'api.mediaObjects.delete' );
    $app->patch   ( '/mediaObjects/{id}' , $controller . ':patch'  )->setName( 'api.mediaObjects.patch' );
};

/**
 * @OA\Get(
 *     path="/mediaObjects",
 *     tags={"mediaObjects"},
 *     description="List media objects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W",
 *             "mediaObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         description="Result with mediaObjects",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of mediaObjects",type="array",items=@OA\Items(ref="#/components/schemas/MediaObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get(
 *     path="/mediaObjects/count",
 *     tags={"mediaObjects"},
 *     description="Count mediaObjects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W",
 *             "mediaObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of mediaObjects",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of mediaObjects",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get(
 *     path="/mediaObjects/{id}",
 *     tags={"mediaObjects"},
 *     description="Get mediaObjects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W",
 *             "mediaObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/mediaObject"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete(
 *     path="/mediaObjects",
 *     tags={"mediaObjects"},
 *     description="Delete mediaObjects from list",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/NotValidList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete(
 *     path="/mediaObjects/{id}",
 *     tags={"mediaObjects"},
 *     description="Delete mediaObjects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Patch(
 *     path="/mediaObjects/{id}",
 *     tags={"mediaObjects"},
 *     description="Patch a mediaObjects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchMediaObject"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/mediaObject"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Post(
 *     path="/mediaObjects",
 *     tags={"mediaObjects"},
 *     description="Create a new mediaObjects",
 *     security={
 *         {"OAuth2"={
 *             "mediaObjects:A",
 *             "mediaObjects:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postMediaObject"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/mediaObject"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
