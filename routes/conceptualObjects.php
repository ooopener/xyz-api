<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="conceptualObjects",
 *     description="The ConceptualObjects API paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require "conceptualObjects/conceptualObjects.php" ) ( $app ) ;

    ( require "conceptualObjects/active.php"            ) ( $app ) ;
    ( require "conceptualObjects/audio.php"             ) ( $app ) ;
    ( require "conceptualObjects/audios.php"            ) ( $app ) ;
    ( require "conceptualObjects/brand.php"             ) ( $app ) ;
    ( require "conceptualObjects/image.php"             ) ( $app ) ;
    ( require "conceptualObjects/keywords.php"          ) ( $app ) ;
    ( require "conceptualObjects/marks.php"             ) ( $app ) ;
    ( require "conceptualObjects/materials.php"         ) ( $app ) ;
    ( require "conceptualObjects/measurements.php"      ) ( $app ) ;
    ( require "conceptualObjects/numbers.php"           ) ( $app ) ;
    ( require "conceptualObjects/photos.php"            ) ( $app ) ;
    ( require "conceptualObjects/productions.php"       ) ( $app ) ;
    ( require "conceptualObjects/translation.php"       ) ( $app ) ;
    ( require "conceptualObjects/video.php"             ) ( $app ) ;
    ( require "conceptualObjects/videos.php"            ) ( $app ) ;
    ( require "conceptualObjects/websites.php"          ) ( $app ) ;
    ( require "conceptualObjects/withStatus.php"        ) ( $app ) ;
};
