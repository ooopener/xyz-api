<?php

use xyz\ooopener\controllers\stages\StagesController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'stages' ,
    StagesController::class ,
    [ 'hasPatch' => FALSE ]
) ;

/**
 * @OA\Get( path="/stages",
 *     tags={"stages"},
 *     description="List stages",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stageListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/stages",
 *     tags={"stages"},
 *     description="Delete stages from list",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/NotValidList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Post( path="/stages",
 *     tags={"stages"},
 *     description="Create a new stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postStage"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stageResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/count",
 *     tags={"stages"},
 *     description="Count stages",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of stages",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of stages",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/{id}",
 *     tags={"stages"},
 *     description="Get stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stageResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/stages/{id}",
 *     tags={"stages"},
 *     description="Delete stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Put( path="/stages/{id}",
 *     tags={"stages"},
 *     description="Edit a stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putStage"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/stageResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
