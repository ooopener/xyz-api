<?php

use xyz\ooopener\controllers\stages\StageDiscoverController;

return ( require __DIR__ . '/../helpers/discover.php' )
(
    'stages',
    StageDiscoverController::class
) ;

/**
* @OA\Get( path="/stages/{id}/discover",
*     tags={"stages"},
*     description="List stage discover",
*     security={
*         {"OAuth2"={
*             "stages:A",
*             "stages:W",
*             "stages:R"
*         }}
*     },
*     @OA\Parameter(ref="#/components/parameters/id"),
*     @OA\Response(
*         response="200",
*         ref="#/components/responses/discoverResponse"
*     ),
*     @OA\Response(
*         response="401",
*         ref="#/components/responses/Unauthorized"
*     ),
*     @OA\Response(
*         response="404",
*         ref="#/components/responses/NotFound"
*     ),
*     @OA\Response(
*         response="500",
*         ref="#/components/responses/Unexpected"
*     )
* )
*/
/**
* @OA\Delete( path="/stages/{id}/discover",
*     tags={"stages"},
*     description="Delete stage discover",
*     security={
*         {"OAuth2"={
*             "stages:A",
*             "stages:W"
*         }}
*     },
*     @OA\Parameter(ref="#/components/parameters/id"),
*     @OA\RequestBody(ref="#/components/requestBodies/deleteStageDiscover"),
*     @OA\Response(
*         response="200",
*         ref="#/components/responses/DeleteList"
*     ),
*     @OA\Response(
*         response="401",
*         ref="#/components/responses/Unauthorized"
*     ),
*     @OA\Response(
*         response="404",
*         ref="#/components/responses/NotFound"
*     ),
*     @OA\Response(
*         response="500",
*         ref="#/components/responses/Unexpected"
*     )
* )
*/
/**
* @OA\Patch( path="/stages/{id}/discover",
*     tags={"stages"},
*     description="Patch stage discover",
*     security={
*         {"OAuth2"={
*             "stages:A",
*             "stages:W"
*         }}
*     },
*     @OA\Parameter(ref="#/components/parameters/id"),
*     @OA\RequestBody(ref="#/components/requestBodies/patchStageDiscover"),
*     @OA\Response(
*         response="200",
*         ref="#/components/responses/discoverResponse"
*     ),
*     @OA\Response(
*         response="401",
*         ref="#/components/responses/Unauthorized"
*     ),
*     @OA\Response(
*         response="404",
*         ref="#/components/responses/NotFound"
*     ),
*     @OA\Response(
*         response="500",
*         ref="#/components/responses/Unexpected"
*     )
* )
*/
/**
* @OA\Post( path="/stages/{id}/discover",
*     tags={"stages"},
*     description="Create stage discover",
*     security={
*         {"OAuth2"={
*             "stages:A",
*             "stages:W"
*         }}
*     },
*     @OA\Parameter(ref="#/components/parameters/id"),
*     @OA\RequestBody(ref="#/components/requestBodies/postStageDiscover"),
*     @OA\Response(
*         response="200",
*         ref="#/components/responses/discoverResponse"
*     ),
*     @OA\Response(
*         response="401",
*         ref="#/components/responses/Unauthorized"
*     ),
*     @OA\Response(
*         response="404",
*         ref="#/components/responses/NotFound"
*     ),
*     @OA\Response(
*         response="500",
*         ref="#/components/responses/Unexpected"
*     )
* )
*/
