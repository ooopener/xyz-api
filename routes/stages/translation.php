<?php

use Slim\App ;

use xyz\ooopener\controllers\stages\StageTranslationController;

return function( App $app )
{
   $translation = ( require __DIR__ . '/../helpers/translation.php' )
   (
       'stages',
       StageTranslationController::class
   );

   $translation( 'alternativeHeadline' )( $app );
   $translation( 'description'         )( $app );
   $translation( 'headline'            )( $app );
   $translation( 'notes'               )( $app );
   $translation( 'text'                )( $app );
};

/**
 * @OA\Get( path="/stages/{id}/alternativeHeadline",
 *     tags={"stages"},
 *     description="Get stage alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch( path="/stages/{id}/alternativeHeadline",
 *     tags={"stages"},
 *     description="Patch stage alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/{id}/description",
 *     tags={"stages"},
 *     description="Get stage description",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch(
 *     path="/stages/{id}/description",
 *     tags={"stages"},
 *     description="Patch stage description",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/{id}/headline",
 *     tags={"stages"},
 *     description="Get stage headline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch(
 *     path="/stages/{id}/headline",
 *     tags={"stages"},
 *     description="Patch stage headline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/{id}/notes",
 *     tags={"stages"},
 *     description="Get stage notes",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch(
 *     path="/stages/{id}/notes",
 *     tags={"stages"},
 *     description="Patch stage notes",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/stages/{id}/text",
 *     tags={"stages"},
 *     description="Get stage text",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch(
 *     path="/stages/{id}/text",
 *     tags={"stages"},
 *     description="Patch stage text",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

