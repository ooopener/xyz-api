<?php

$app->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/keywords',
    [ $container->stageKeywordsController , 'all' ]
)
->setName('api.stages.keywords.all') ;

$app->post
(
    '/stages/{id:[0-9]+}/keywords',
    [ $container->stageKeywordsController , 'post' ]
)
->setName('api.stages.keywords.post') ;

$app->get
(
    '/stages/{id:[0-9]+}/keywords/count' ,
    [ $container->stageKeywordsController , 'count' ]
)
->setName('api.stages.keywords.count') ;

$app->options
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$app->delete
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->stageKeywordsController , 'delete' ]
)
->setName('api.stages.keywords.delete') ;

$app->put
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->stageKeywordsController , 'put' ]
)
->setName('api.stages.keywords.put') ;