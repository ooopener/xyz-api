<?php

use Slim\App ;

/**
 * The commons routes definitions
 * @param App $app
 */
return function ( App $app )
{
    ( require 'api.php' ) ( $app ) ;

    ( require 'login.php'        ) ( $app ) ;
    ( require 'oauth.php'        ) ( $app ) ;
    ( require 'me.php'           ) ( $app ) ;
    ( require 'activityLogs.php' ) ( $app ) ;

    ( require 'applications.php'      ) ( $app ) ;
    ( require 'articles.php'          ) ( $app ) ;
    ( require 'brands.php'            ) ( $app ) ;
    ( require 'conceptualObjects.php' ) ( $app ) ;
    ( require 'events.php'            ) ( $app ) ;
    ( require 'mediaObjects.php'      ) ( $app ) ;
    ( require 'organizations.php'     ) ( $app ) ;
    ( require 'people.php'            ) ( $app ) ;
    ( require 'places.php'            ) ( $app ) ;
    ( require 'users.php'             ) ( $app ) ;
    ( require 'teams.php'             ) ( $app ) ;

    ( require 'settings.php'  ) ( $app ) ;
    ( require 'thesaurus.php' ) ( $app ) ;
};
