<?php

use xyz\ooopener\controllers\livestocks\LivestocksController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'livestocks' ,
    LivestocksController::class
) ;
