<?php

use xyz\ooopener\controllers\livestocks\WorkplacesController;
use Slim\App ;

return function( App $app )
{
    $controller = WorkplacesController::class ;

    $list    = '/livestocks/workshops/{id:[0-9]+}/workplaces' ;
    $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $thing   = '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}' ;

    $app->options ( $list , $options ) ;
    $app->get     ( $list , $controller . ':all' )->setName('api.workshops.workplaces.all');
    $app->post    ( $list , $controller . ':post' )->setName('api.workshops.workplaces.post') ;

    $app->options ( $thing , $options ) ;
    $app->get     ( $thing , $controller . ':get'    )->setName('api.workshops.workplaces.get');
    $app->delete  ( $thing , $controller . ':delete' )->setName('api.workshops.workplaces.delete');
    $app->put     ( $thing , $controller . ':put'    )->setName('api.workshops.workplaces.put');



};
