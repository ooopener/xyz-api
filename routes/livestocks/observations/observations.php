<?php

use Slim\App ;

use xyz\ooopener\controllers\livestocks\observations\ObservationsController;

return function( App $app )
{
    $controller = ObservationsController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    // all observations of a specific livestock

    $app->options ( '/livestocks/{id:[0-9]+}/observations' , $options ) ;
    $app->get     ( '/livestocks/{id:[0-9]+}/observations' , $controller . ':allLivestock' )->setName('api.livestocks.observations.allLivestock');

    // list observations of a specific workshop (all) and create a new observation (post)

    $app->options ( '/livestocks/workshops/{id:[0-9]+}/observations' , $options ) ;
    $app->get     ( '/livestocks/workshops/{id:[0-9]+}/observations' , $controller . ':allWorkshop' )->setName('api.livestocks.workshops.observations.allWorkshop');
    $app->post    ( '/livestocks/workshops/{id:[0-9]+}/observations' , $controller . ':post'        )->setName('api.livestocks.workshops.observations.post');

    // get/delete/patch a specific observation

    $app->options ( '/livestocks/observations/{id:[0-9]+}' , $options ) ;
    $app->get     ( '/livestocks/observations/{id:[0-9]+}' , $controller . ':get'    )->setName('api.livestocks.observations.get');
    $app->delete  ( '/livestocks/observations/{id:[0-9]+}' , $controller . ':delete' )->setName('api.livestocks.observations.delete');
    $app->patch   ( '/livestocks/observations/{id:[0-9]+}' , $controller . ':patch'  )->setName('api.livestocks.observations.patch');

    // delete a list of observations

    $app->options ( '/livestocks/observations' , $options ) ;
    $app->delete  ( '/livestocks/observations' , $controller . ':deleteAll' )->setName('api.livestocks.observations.deleteAll');
};
