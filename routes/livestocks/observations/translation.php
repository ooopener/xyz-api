<?php

use Slim\App ;

use xyz\ooopener\controllers\livestocks\observations\ObservationTranslationController ;


return function( App $app )
{
   $translation = ( require __DIR__ . '/../../helpers/translation.php' )
   (
       'livestocks/observations',
       ObservationTranslationController::class
   );

   $translation( 'alternateName' )( $app );
   $translation( 'description'   )( $app );
   $translation( 'notes'         )( $app );
   $translation( 'text'          )( $app );
};
