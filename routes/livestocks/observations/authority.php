<?php

return ( require __DIR__ . '/../../helpers/subThings.php' )
(
    'authority' ,
    'livestocks/observations',
    'observationAuthorityController' ,
    [
        'hasCount'     => TRUE ,
        'hasDeleteAll' => TRUE
    ]
) ;
