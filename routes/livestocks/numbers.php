<?php

use Slim\App ;

use xyz\ooopener\controllers\livestocks\LivestockNumbersController;

return function( App $app ) // TODO create an helper
{
    $controller = LivestockNumbersController::class ;
    $route      = '/livestocks/{id:[0-9]+}/numbers' ;
    $count      = $route . '/count' ;
    $options    = LivestockNumbersController::class . __CLAZZ_NOBODY_RESPONSE__ ;
    $thing      = '/livestocks/{owner:[0-9]+}/numbers/{id:[0-9]+}' ;


    $app->options  ( $route , $options ) ;
    $app->get      ( $route , $controller . ':all'  )->setName('api.livestocks.numbers.all') ;
    $app->post     ( $route , $controller . ':post' )->setName('api.livestocks.numbers.post') ;

    $app->get( $count , $controller . ':count' )->setName('api.livestocks.numbers.count') ;

    $app->options ( $thing , $options ) ;
    $app->delete  ( $thing , $controller . ':delete' )->setName('api.livestocks.numbers.delete') ;
    $app->put     ( $thing , $controller . ':put'    )->setName('api.livestocks.numbers.put') ;
};
