<?php

$app->get
(
    '/livestocks/{id:[0-9]+}/withStatus' ,
    [ $container->livestockWithStatusController , 'get'  ]
)
->setName('api.livestocks.withStatus.get') ;

$app->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->livestockWithStatusController , 'patch' ]
)
->setName('api.livestocks.withStatus.patch') ;

