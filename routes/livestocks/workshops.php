<?php

use xyz\ooopener\controllers\livestocks\WorkshopsController;
use Slim\App ;

return function( App $app )
{
    $controller = WorkshopsController::class ;

    $list    = '/livestocks/{id:[0-9]+}/workshops' ;
    $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $get     = '/livestocks/workshops/{id:[0-9]+}' ;
    $edit    = '/livestocks/{owner:[0-9]+}/workshops/{id:[0-9]+}' ;

    $app->options ( $list , $options ) ;
    $app->get     ( $list , $controller . ':all'  )->setName( 'api.livestocks.workshops.all'  ) ;
    $app->post    ( $list , $controller . ':post' )->setName( 'api.livestocks.workshops.post' ) ;

    $app->options ( $get , $options ) ;
    $app->get     ( $get , $controller . ':get' ) ->setName('api.livestocks.workshops.get');

    $app->options ( $edit , $options );
    $app->delete  ( $edit , $controller . ':delete' )->setName( 'api.livestocks.workshops.delete' );
    $app->put     ( $edit , $controller . ':put'    )->setName( 'api.livestocks.workshops.put'    );
};
