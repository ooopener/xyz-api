<?php

use Slim\App ;
use xyz\ooopener\controllers\livestocks\SectorsController;

return function( App $app )
{
    $controller = SectorsController::class ;

    $list    = '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}/sectors' ;
    $options = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $thing   = '/livestocks/workshops/{workshop:[0-9]+}/workplaces/{owner:[0-9]+}/sectors/{id:[0-9]+}' ;

    $app->options ( $list , $options ) ;
    $app->get     ( $list , $controller . ':all'  )->setName('api.workshops.workplaces.sectors.all');
    $app->post    ( $list , $controller . ':post' )->setName('api.workshops.workplaces.sectors.post') ;

    $app->options ( $thing , $options ) ;
    $app->get     ( $thing , $controller . ':get'    )->setName('api.workshops.workplaces.sectors.get');
    $app->delete  ( $thing , $controller . ':delete' )->setName('api.workshops.workplaces.sectors.delete');
    $app->put     ( $thing , $controller . ':put'    )->setName('api.workshops.workplaces.sectors.put');
};
