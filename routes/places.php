<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="places",
 *     description="The places API paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'places/places.php' ) ( $app ) ;

    ( require 'places/active.php'           ) ( $app ) ;
    ( require 'places/activities.php'       ) ( $app ) ;
    ( require 'places/additionalType.php'   ) ( $app ) ;
    ( require 'places/address.php'          ) ( $app ) ;
    ( require 'places/audio.php'            ) ( $app ) ;
    ( require 'places/audios.php'           ) ( $app ) ;
    ( require 'places/brand.php'            ) ( $app ) ;
    ( require 'places/email.php'            ) ( $app ) ;
    ( require 'places/geo.php'              ) ( $app ) ;
    ( require 'places/image.php'            ) ( $app ) ;
    ( require 'places/keywords.php'         ) ( $app ) ;
    ( require 'places/logo.php'             ) ( $app ) ;
    ( require 'places/near.php'             ) ( $app ) ;
    ( require 'places/offers.php'           ) ( $app ) ;
    ( require 'places/openingHours.php'     ) ( $app ) ;
    ( require 'places/permits.php'          ) ( $app ) ;
    ( require 'places/photos.php'           ) ( $app ) ;
    ( require 'places/prohibitions.php'     ) ( $app ) ;
    ( require 'places/services.php'         ) ( $app ) ;
    ( require 'places/containedInPlace.php' ) ( $app ) ;
    ( require 'places/containsPlace.php'    ) ( $app ) ;
    ( require 'places/telephone.php'        ) ( $app ) ;
    ( require 'places/translation.php'      ) ( $app ) ;
    ( require 'places/video.php'            ) ( $app ) ;
    ( require 'places/videos.php'           ) ( $app ) ;
    ( require 'places/websites.php'         ) ( $app ) ;
    ( require 'places/withStatus.php'       ) ( $app ) ;

};
