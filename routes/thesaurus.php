<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="thesaurus",
 *     description="The places API paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'thesaurus/thesaurus.php' ) ( $app ) ;
    ( require 'thesaurus/active.php'    ) ( $app ) ;
};
