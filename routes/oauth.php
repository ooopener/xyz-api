<?php

use Slim\App ;

return function ( App $app )
{
    ( require "oauth/allowApplication.php" ) ( $app ) ;
    ( require "oauth/authorize.php"        ) ( $app ) ;
    ( require "oauth/revoke.php"           ) ( $app ) ;
    ( require "oauth/token.php"            ) ( $app ) ;
};
