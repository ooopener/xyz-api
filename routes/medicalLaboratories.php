<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="medicalLaboratories",
 *     description="The medical laboratories paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'medicalLaboratories/medicalLaboratories.php' ) ( $app ) ;
    ( require 'medicalLaboratories/active.php'              ) ( $app ) ;
    ( require 'medicalLaboratories/livestocks.php'          ) ( $app ) ;
    ( require 'medicalLaboratories/medicalSpecialties.php'  ) ( $app ) ;
};
