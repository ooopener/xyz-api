<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="users",
 *     description="Users paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require "users/activityLogs.php" ) ( $app );
    ( require "users/address.php"      ) ( $app );
    ( require "users/permissions.php"  ) ( $app );
    ( require "users/sessions.php"     ) ( $app );
    ( require "users/users.php"        ) ( $app );
};
