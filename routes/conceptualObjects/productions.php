<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'productions' ,
    'conceptualObjects',
    'conceptualObjectProductionsController'
) ;

/**
 * @OA\Get( path="/conceptualObjects/{id}/productions",
 *     tags={"conceptualObjects"},
 *     description="Get productions conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Get( path="/conceptualObjects/{id}/productions/count",
 *     tags={"conceptualObjects"},
 *     description="Count conceptualObject productions",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of productions",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Delete( path="/conceptualObjects/{owner}/productions/{id}",
 *     tags={"conceptualObjects"},
 *     description="Delete productions conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/deleteAuthority"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Post( path="/conceptualObjects/{owner}/productions/{id}",
 *     tags={"conceptualObjects"},
 *     description="Create productions conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postAuthority"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */