<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'brand' ,
    'conceptualObjects',
    'conceptualObjectHasBrandController' ,
    [
        'hasCount'     => TRUE ,
        'hasDeleteAll' => TRUE
    ]
) ;
