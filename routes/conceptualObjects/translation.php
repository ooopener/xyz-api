<?php

use Slim\App ;

use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectTranslationController ;

return function( App $app )
{
   $translation = ( require __DIR__ . '/../helpers/translation.php' )
   (
       'conceptualObjects',
       ConceptualObjectTranslationController::class
   );

   $translation( 'alternateName' )( $app );
   $translation( 'description'   )( $app );
   $translation( 'notes'         )( $app );
   $translation( 'text'          )( $app );
};

/**
 * @OA\Get( path="/conceptualObjects/{id}/alternateName",
 *     tags={"conceptualObjects"},
 *     description="Get conceptualObject alternateName",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch( path="/conceptualObjects/{id}/alternateName",
 *     tags={"conceptualObjects"},
 *     description="Patch conceptualObject alternateName",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/conceptualObjects/{id}/description",
 *     tags={"conceptualObjects"},
 *     description="Get conceptualObject description",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch( path="/conceptualObjects/{id}/description",
 *     tags={"conceptualObjects"},
 *     description="Patch conceptualObject description",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/conceptualObjects/{id}/notes",
 *     tags={"conceptualObjects"},
 *     description="Get conceptualObject notes",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch( path="/conceptualObjects/{id}/notes",
 *     tags={"conceptualObjects"},
 *     description="Patch conceptualObject notes",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/conceptualObjects/{id}/text",
 *     tags={"conceptualObjects"},
 *     description="Get conceptualObject text",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 * @OA\Patch( path="/conceptualObjects/{id}/text",
 *     tags={"conceptualObjects"},
 *     description="Patch conceptualObject text",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
