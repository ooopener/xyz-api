<?php

use Slim\App ;

use xyz\ooopener\controllers\users\UserActivityLogsController ;

/**
 * @OA\Tag(
 *     name="activityLogs",
 *     description="ActivityLogs paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    $controller = UserActivityLogsController::class ;
    $app->options ( '/activityLogs' , $controller .__CLAZZ_NOBODY_RESPONSE__ );
    $app->get     ( '/activityLogs' , $controller . ':all' )->setName('api.activityLogs.all');
};
