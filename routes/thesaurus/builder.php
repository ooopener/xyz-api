<?php

use Slim\App ;
use Slim\Routing\RouteCollectorProxy ;

use xyz\ooopener\controllers\ThesaurusController;

/**
 * Register a new application route's group with a specific thesaurus controller reference.
 * @param ThesaurusController $controller
 * @return callable
 */
$populate = fn( ThesaurusController $controller ):callable
          => function( RouteCollectorProxy $group ) use ( $controller )
{
    $ref = str_replace( '/' , '.' , $controller->path ) ;

    $options = [ $controller , __NOBODY_RESPONSE__ ] ;

    $group->options( '' , $options ) ;

    $group->get( ''  , [ $controller , 'all' ] )
          ->setName( 'api.thesaurus.' . $ref . '.all' ) ;

    $group->delete( ''  , [ $controller , 'deleteAll' ] )
          ->setName( 'api.thesaurus.' . $ref . '.delete.all' ) ;

    $group->get( '/' , [ $controller , 'all' ] ) ;

    $group->get( '/count' , [ $controller , 'count' ] )
          ->setName( 'api.thesaurus.' . $ref . '.count' ) ;

    $group->post( '' , [ $controller , 'post' ] )
          ->setName( 'api.' . $ref . '.post' );

    // --------- alternateName

    $group->get( '/{name:[A-Za-z\/]+}' , [ $controller , 'getByAlternateName' ] )
          ->setName( 'api.' . $ref . '.name.get' ) ;

    $group->options('/{id:[0-9]+}' , $options ) ;

    $group->get('/{id:[0-9]+}' , [ $controller , 'get' ] )
          ->setName( 'api.thesaurus.' . $ref . '.get' ) ;

    $group->put( '/{id:[0-9]+}' , [ $controller , 'put' ] )
          ->setName( 'api.thesaurus.' . $ref . '.put' ) ;

    $group->delete( '/{id:[0-9]+}' , [ $controller , 'delete' ] )
          ->setName( 'api.thesaurus.' . $ref . '.delete' ) ;

    // --------- activable

    $group->options('/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' , $options ) ;

    $group->patch( '/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' , [ $controller , 'patchActive' ] )
          ->setName('api.thesaurus.' . $ref . '.active') ;

    // --------- iconifiable

    $group->options( '/{id:[0-9]+}/image' , $options ) ;

    $group->delete( '/{id:[0-9]+}/image' , [ $controller , 'deleteImage' ] )
          ->setName( 'api.thesaurus.' . $ref . '.image.delete') ;

    $group->post( '/{id:[0-9]+}/image' , [ $controller , 'postImage' ] )
          ->setName( 'api.thesaurus.' . $ref . '.image.post') ;

};

/**
 * Register all the route's groups with a specific array of thesaurus controllers.
 * @param array $definitions
 * @return callable
 */
return fn( array $definitions ) => function( App $app ) use ( $definitions , $populate )
{
    $id = NULL ;
    $container = $app->getContainer() ;

    foreach( $definitions as $id )
    {
        $controller = $container->get( $id . 'Controller' ) ;
        $app->group( $controller->fullPath , $populate( $controller ) );
    }
};
