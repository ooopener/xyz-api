<?php

return
[
    'badgeItemsTypes'  ,
    'coursesAudiences' ,
    'coursesLevels'    ,
    'coursesPaths'     ,
    'coursesStatus'    ,
    'coursesTypes'     ,
    'gamesTypes'       ,
    'questionsTypes'   ,
    'transportations'
];
