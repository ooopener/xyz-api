<?php

use Slim\App ;

use xyz\ooopener\controllers\ThesaurusCollectionController ;

return function( App $app )
{
    $controller = ThesaurusCollectionController::class ;

    $app->options ( '/thesaurus' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( '/thesaurus' , $controller . ':all'  )->setName( 'api.thesaurus.all'  );
    $app->post    ( '/thesaurus' , $controller . ':post' )->setName( 'api.thesaurus.post' );

    $app->options ( '/thesaurus/{id:[0-9]+}' , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( '/thesaurus/{id:[0-9]+}' , $controller . ':get'   )->setName('api.thesaurus.get');
    $app->delete  ( '/thesaurus/{id:[0-9]+}' , $controller . ':delete')->setName('api.thesaurus.delete');
    $app->put     ( '/thesaurus/{id:[0-9]+}' , $controller . ':put'   )->setName('api.thesaurus.put');
};

/**
 *
 * @OA\Tag(
 *     name="thesaurus",
 *     description="Thesaurus paths"
 * )
 *
 * @OA\Get(
 *     path="/thesaurus",
 *     tags={"thesaurus"},
 *     description="List of available thesauri",
 *     security={
 *         {"OAuth2"={
 *             "thesaurus:A",
 *             "thesaurus:W",
 *             "thesaurus:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/thesauriResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
