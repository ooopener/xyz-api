<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="stages",
 *     description="Stages paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'stages/stages.php' ) ( $app ) ;

    ( require 'stages/active.php'      ) ( $app ) ;
    ( require 'stages/activities.php'  ) ( $app ) ;
    ( require 'stages/audio.php'       ) ( $app ) ;
    ( require 'stages/audios.php'      ) ( $app ) ;
    ( require 'stages/discover.php'    ) ( $app ) ;
    ( require 'stages/image.php'       ) ( $app ) ;
    ( require 'stages/photos.php'      ) ( $app ) ;
    ( require 'stages/translation.php' ) ( $app ) ;
    ( require 'stages/video.php'       ) ( $app ) ;
    ( require 'stages/videos.php'      ) ( $app ) ;
    ( require 'stages/websites.php'    ) ( $app ) ;
    ( require 'stages/withStatus.php'  ) ( $app ) ;
};
