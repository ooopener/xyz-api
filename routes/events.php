<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="events",
 *     description="The event API routes. An event happening at a certain time and location, such as a concert, lecture, or festival."
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'events/events.php' ) ( $app ) ;
    ( require 'events/from.php'   ) ( $app ) ;

    ( require 'events/active.php'         ) ( $app ) ;
    ( require 'events/additionalType.php' ) ( $app ) ;
    ( require 'events/audio.php'          ) ( $app ) ;
    ( require 'events/audios.php'         ) ( $app ) ;
    ( require 'events/image.php'          ) ( $app ) ;
    ( require 'events/keywords.php'       ) ( $app ) ;
    ( require 'events/offers.php'         ) ( $app ) ;
    ( require 'events/organizer.php'      ) ( $app ) ;
    ( require 'events/photos.php'         ) ( $app ) ;
    ( require 'events/subEvent.php'       ) ( $app ) ;
    ( require 'events/superEvent.php'     ) ( $app ) ;
    ( require 'events/translation.php'    ) ( $app ) ;
    ( require 'events/video.php'          ) ( $app ) ;
    ( require 'events/videos.php'         ) ( $app ) ;
    ( require 'events/websites.php'       ) ( $app ) ;
    ( require 'events/withStatus.php'     ) ( $app ) ;
    ( require 'events/workFeatured.php'   ) ( $app ) ;
};
