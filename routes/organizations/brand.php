<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'brand' ,
    'organizations',
    'organizationHasBrandController' ,
    [
        'hasCount'     => TRUE ,
        'hasDeleteAll' => TRUE
    ]
) ;
