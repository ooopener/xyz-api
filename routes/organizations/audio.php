<?php

return ( require __DIR__ . '/../helpers/audio.php' )
(
    'organizations',
    'organizationAudioController'
) ;

/**
 * @OA\Get(
 *     path="/organizations/{id}/audio",
 *     tags={"organizations"},
 *     description="Get organization audio",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/organizations/{id}/audio",
 *     tags={"organizations"},
 *     description="Delete resource audio",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Patch(
 *     path="/organizations/{owner}/audio/{id}",
 *     tags={"organizations"},
 *     description="Patch resource audio",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */