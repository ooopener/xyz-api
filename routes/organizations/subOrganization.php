<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'subOrganization' ,
    'organizations',
    'organizationSubOrganizationsController'
) ;

/**
 * @OA\Get(
 *     path="/organizations/{id}/subOrganization",
 *     tags={"organizations"},
 *     description="Get subOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/organizations/{id}/subOrganization/count",
 *     tags={"organizations"},
 *     description="Count organization subOrganization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of subOrganization",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Delete(
 *     path="/organizations/{owner}/subOrganization/{id}",
 *     tags={"organizations"},
 *     description="Delete subOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Post(
 *     path="/organizations/{owner}/subOrganization/{id}",
 *     tags={"organizations"},
 *     description="Create subOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */