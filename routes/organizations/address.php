<?php

return ( require __DIR__ . '/../helpers/address.php' )
(
    'organizations',
    'organizationPostalAddressController'
) ;

/**
 * @OA\Get(
 *     path="/organizations/{id}/address",
 *     tags={"organizations"},
 *     description="Get address organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Patch(
 *     path="/organizations/{id}/address",
 *     tags={"organizations"},
 *     description="Patch address organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchAddress"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
