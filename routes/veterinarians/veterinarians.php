<?php

use xyz\ooopener\controllers\veterinarians\VeterinariansController ;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'veterinarians' ,
    VeterinariansController::class
) ;
