<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="brands",
 *     description="The /brands API routes"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'brands/brands.php' ) ( $app ) ;

    ( require 'brands/active.php'         ) ( $app ) ;
    ( require 'brands/additionalType.php' ) ( $app ) ;
    ( require 'brands/audio.php'          ) ( $app ) ;
    ( require 'brands/audios.php'         ) ( $app ) ;
    ( require 'brands/image.php'          ) ( $app ) ;
    ( require 'brands/logo.php'           ) ( $app ) ;
    ( require 'brands/photos.php'         ) ( $app ) ;
    ( require 'brands/translation.php'    ) ( $app ) ;
    ( require 'brands/video.php'          ) ( $app ) ;
    ( require 'brands/videos.php'         ) ( $app ) ;
    ( require 'brands/websites.php'       ) ( $app ) ;
    ( require 'brands/withStatus.php'     ) ( $app ) ;
};
