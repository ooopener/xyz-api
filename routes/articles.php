<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="articles",
 *     description="The articles API route paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'articles/articles.php' ) ( $app ) ;

    ( require 'articles/active.php'         ) ( $app ) ;
    ( require 'articles/additionalType.php' ) ( $app ) ;
    ( require 'articles/audio.php'          ) ( $app ) ;
    ( require 'articles/audios.php'         ) ( $app ) ;
    ( require 'articles/hasPart.php'        ) ( $app ) ;
    ( require 'articles/image.php'          ) ( $app ) ;
    ( require 'articles/isPartOf.php'       ) ( $app ) ;
    ( require 'articles/isRelatedTo.php'    ) ( $app ) ;
    ( require 'articles/isSimilarTo.php'    ) ( $app ) ;
    ( require 'articles/photos.php'         ) ( $app ) ;
    ( require 'articles/translation.php'    ) ( $app ) ;
    ( require 'articles/video.php'          ) ( $app ) ;
    ( require 'articles/videos.php'         ) ( $app ) ;
    ( require 'articles/websites.php'       ) ( $app ) ;
};
