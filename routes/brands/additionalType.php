<?php

return ( require __DIR__ . '/../helpers/additionalType.php' )
(
    'brands',
    'brandAdditionalTypeController'
) ;

/**
 * @OA\Get(
 *     path="/brands/{id}/additionalType",
 *     tags={"brands"},
 *     description="Get additionalType brand",
 *     security={
 *         {"OAuth2"={
 *             "brands:A",
 *             "brands:W",
 *             "brands:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/thesaurusResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

