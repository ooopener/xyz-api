<?php

return ( require __DIR__ . '/../helpers/logo.php' )
(
    'brands',
    'brandLogoController'
) ;

/**
 * @OA\Delete(
 *     path="/brands/{id}/logo",
 *     tags={"brands"},
 *     description="Delete resource logo",
 *     security={
 *         {"OAuth2"={
 *             "brands:A",
 *             "brands:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Patch(
 *     path="/brands/{owner}/logo/{id}",
 *     tags={"brands"},
 *     description="Patch resource logo",
 *     security={
 *         {"OAuth2"={
 *             "brands:A",
 *             "brands:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */