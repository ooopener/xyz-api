<?php

use Slim\App ;

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'diseases/diseases.php' ) ( $app ) ;

    ( require 'diseases/active.php'             ) ( $app ) ;
    ( require 'diseases/analysisMethod.php'     ) ( $app ) ;
    ( require 'diseases/analysisSampling.php'   ) ( $app ) ;
    ( require 'diseases/translation.php'        ) ( $app ) ;
    ( require 'diseases/transmissionMethod.php' ) ( $app ) ;
    ( require 'diseases/withStatus.php'         ) ( $app ) ;
};
