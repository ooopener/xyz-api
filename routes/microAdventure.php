<?php

use Slim\App ;

// micro-adventure routes

return function( App $app )
{
    ( require 'courses.php') ( $app ) ;
    ( require 'games.php'  ) ( $app ) ;
    ( require 'stages.php' ) ( $app ) ;
};
