<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="applications",
 *     description="Applications paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'applications/applications.php' ) ( $app );

    ( require 'applications/audio.php'       ) ( $app );
    ( require 'applications/audios.php'      ) ( $app );
    ( require 'applications/image.php'       ) ( $app );
    ( require 'applications/photos.php'      ) ( $app );
    ( require 'applications/producer.php'    ) ( $app );
    ( require 'applications/publisher.php'   ) ( $app );
    ( require 'applications/sponsor.php'     ) ( $app );
    ( require 'applications/translation.php' ) ( $app );
    ( require 'applications/video.php'       ) ( $app );
    ( require 'applications/videos.php'      ) ( $app );
    ( require 'applications/websites.php'    ) ( $app );
};
