<?php

use Slim\App ;

/**
 * @OA\Tag(
 *     name="veterinarians",
 *     description="Veterinarians paths"
 * )
 */

/**
 * @param App $app
 */
return function( App $app )
{
    ( require 'veterinarians/veterinarians.php' ) ( $app ) ;

    ( require 'veterinarians/active.php'             ) ( $app ) ;
    ( require 'veterinarians/livestocks.php'         ) ( $app ) ;
    ( require 'veterinarians/medicalSpecialties.php' ) ( $app ) ;
};
