<?php

return ( require __DIR__ . '/../helpers/subThings.php' )
(
    'isSimilarTo' ,
    'articles',
    'articleIsSimilarToController'
) ;

/**
 * @OA\Get( path="/articles/{id}/isSimilarTo",
 *     tags={"articles"},
 *     description="Get isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Get( path="/articles/{id}/isSimilarTo/count",
 *     tags={"articles"},
 *     description="Count article isSimilarTo",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of isSimilarTo",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Delete( path="/articles/{owner}/isSimilarTo/{id}",
 *     tags={"articles"},
 *     description="Delete isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
/**
 * @OA\Post( path="/articles/{owner}/isSimilarTo/{id}",
 *     tags={"articles"},
 *     description="Create isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
