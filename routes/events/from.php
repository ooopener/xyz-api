<?php

use Slim\App ;

use xyz\ooopener\controllers\events\EventsController ;

return function( App $app )
{
    $controller = EventsController::class ;
    $route      = '/events/from[/{from}[/to/{to}]]' ;

    $app->options ( $route , $controller . __CLAZZ_NOBODY_RESPONSE__ ) ;
    $app->get     ( $route , $controller . ':from' )->setName('api.events.from');
};


/**
 * @OA\Get(
 *     path="/events/from",
 *     tags={"events"},
 *     description="Return all the elements from a specific date",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/eventListResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/BadRequest"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/events/from/{from}",
 *     tags={"events"},
 *     description="Return all the elements from a specific date",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(name="from",in="path",description="Date from",required=true,@OA\Schema(type="string",format="date")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/eventListResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/BadRequest"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/events/from/{from}/to/{to}",
 *     tags={"events"},
 *     description="Return all the elements between dates",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(name="from",in="path",description="Date from",required=true,@OA\Schema(type="string",format="date")),
 *     @OA\Parameter(name="to",in="path",description="Date to",required=true,@OA\Schema(type="string",format="date")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/eventListResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/BadRequest"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
