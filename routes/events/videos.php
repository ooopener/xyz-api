<?php

return ( require __DIR__ . '/../helpers/videos.php' )
(
    'events',
    'eventVideosController'
) ;

/**
 * @OA\Get( path="/events/{id}/videos",
 *     tags={"events"},
 *     description="List event videos",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of video objects",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Post( path="/events/{id}/videos",
 *     tags={"events"},
 *     description="Create event videos",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of video objects",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Get( path="/events/{id}/videos/count",
 *     tags={"events"},
 *     description="Count event videos",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of video objects",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Delete( path="/events/{id}/videos",
 *     tags={"events"},
 *     description="Delete event videos",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Get( path="/events/{owner}/videos/{id}",
 *     tags={"events"},
 *     description="Get event video",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Delete( path="/events/{owner}/videos/{id}",
 *     tags={"events"},
 *     description="Delete event video",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */

/**
 * @OA\Patch( path="/events/{owner}/videos/{id}/position/{position}",
 *     tags={"events"},
 *     description="Patch event video position",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="position",in="path",description="",required=true,@OA\Schema(type="integer")),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */