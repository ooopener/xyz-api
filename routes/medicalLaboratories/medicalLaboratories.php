<?php

use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController;

return ( require __DIR__ . '/../helpers/thing.php' )
(
    'medicalLaboratories' ,
    MedicalLaboratoriesController::class
) ;
