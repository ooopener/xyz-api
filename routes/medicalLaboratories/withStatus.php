<?php

return ( require __DIR__ . '/../helpers/withStatus.php' )
(
    'medicalLaboratories',
    'medicalLaboratoryWithStatusController'
) ;