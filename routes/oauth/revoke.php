<?php

use Slim\App ;

use xyz\ooopener\controllers\oauth\RevokeController ;

return function( App $app )
{
    $controller = RevokeController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;

    $route = '/oauth/revoke' ;

    $app->options ( $route , $options ) ;
    $app->get     ( $route , $controller . ':revoke' )->setName( 'oauth.revoke' ) ;
    $app->post    ( $route , $controller . ':revoke' )->setName( 'oauth.revoke.post' ) ;
};
