<?php

use Slim\App ;

use xyz\ooopener\controllers\oauth\TokenController ;

return function( App $app )
{
    $controller = TokenController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__;
    $route      = '/oauth/token' ;

    $app->options ( $route , $options ) ;
    $app->get     ( $route , $controller . ':token' )->setName('oauth.token') ;
    $app->post    ( $route , $controller . ':token' )->setName('oauth.token.post') ;
};
