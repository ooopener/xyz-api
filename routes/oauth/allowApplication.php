<?php

use Slim\App ;

use xyz\ooopener\controllers\oauth\AuthorizationCodeController ;

return function( App $app )
{
    $controller = AuthorizationCodeController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $route      = '/oauth/allowApplication' ;

    $app->options( $route , $options ) ;
    $app->post( $route , $controller . ':allowApplication' )
        ->add('csrf')
        ->setName('oauth.allowApplication.post') ;
};
