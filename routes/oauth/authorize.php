<?php

use Slim\App ;

use xyz\ooopener\controllers\oauth\AuthorizationCodeController ;

return function( App $app )
{
    $controller = AuthorizationCodeController::class ;
    $options    = $controller . __CLAZZ_NOBODY_RESPONSE__ ;
    $route      = '/oauth/authorize' ;

    $app->options ( $route , $options ) ;
    $app->get     ( $route , $controller . ':authorize' )->setName( 'oauth.authorize' ) ;
    $app->post    ( $route , $controller . ':authorize' )->setName( 'oauth.authorize.post' ) ;
};
