<?php

use core\Strings;

$content =
"[csrf]
key = \"{0}\"

[token]
key = \"{1}\"
" ;

return function( $path ) use ( $content )
{
    if( !file_exists( $path ) )
    {
        $generateCSRFKey  = bin2hex( random_bytes( 16 ) ) ;
        $generateTokenKey = bin2hex( random_bytes( 16 ) ) ;

        $content = Strings::fastformat( $content , $generateCSRFKey , $generateTokenKey ) ;

        file_put_contents( $path , $content ) ;
    }
    return parse_ini_file( $path , TRUE ) ;
};