<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\applications\ApplicationsController;
use xyz\ooopener\controllers\articles\ArticlesController ;
use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;
use xyz\ooopener\controllers\courses\CoursesController ;
use xyz\ooopener\controllers\events\EventsController ;
use xyz\ooopener\controllers\livestocks\observations\ObservationsController;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;
use xyz\ooopener\controllers\stages\StagesController ;
use xyz\ooopener\controllers\steps\StepsController ;

use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;

$core =
[
    MediaObjects::class => fn( ContainerInterface $container ) => new MediaObjects
    (
        $container ,
        "mediaObjects",
        "mediaObjects_things_audio",
        "mediaObjects_things_audios",
        "mediaObjects_things_image",
        "mediaObjects_things_logo",
        "mediaObjects_things_photos",
        "mediaObjects_things_video",
        "mediaObjects_things_videos",
        [
            'applications',
            'articles',
            'conceptualObjects',
            'courses',
            'events',
            'observations',
            'organizations',
            'people',
            'places',
            'stages',
            'steps'
        ],
        [
            Model::FACETABLE =>
            [
                'id'             => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids'            => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'encodingFormat' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'encodingFormat' ],
                'keywords'       => [ Model::FACET_TYPE => Model::FACET_LIST  , Model::FACET_PROPERTY => 'keywords'       ]
            ],
            Model::SEARCHABLE =>
            [
                'headline' ,
                'alternativeHeadline',
                'name'
            ],
            Model::SORTABLE =>
            [
                'id'             => '_key',
                'name'           => 'name',
                'encodingFormat' => 'encodingFormat',
                'created'        => 'created',
                'modified'       => 'modified'
            ]
        ]
    ),

    'mediaObjectsThingsAudio' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_audio',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => AudioObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsImage' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_image',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => ImageObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsLogo' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_logo',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => ImageObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsVideo' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_video',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => VideoObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsAudios' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => AudioObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsPhotos' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => ImageObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    ),

    'mediaObjectsThingsVideos' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            Model::FROM =>
            [
                Model::NAME       => 'mediaObjects',
                Model::CONTROLLER => VideoObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ]
        ]
    )
];

$init = function( string $name , string $controller )
{
    $upper = ucFirst( $name ) ;
    return
    [
        'mediaObjects' . $upper . 'Audios' => fn( ContainerInterface $container ) => new Edges
        (
            $container ,
            'mediaObjects_things_audios',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'mediaObjects',
                    Model::CONTROLLER => AudioObjectsController::class
                ],
                Model::TO =>
                [
                    Model::NAME       => $name,
                    Model::CONTROLLER => $controller
                ]
            ]
        ),
        'mediaObjects' . $upper . 'Photos' => fn( ContainerInterface $container ) => new Edges
        (
            $container ,
            'mediaObjects_things_photos',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'mediaObjects',
                    Model::CONTROLLER => ImageObjectsController::class
                ],
                Model::TO =>
                [
                    Model::NAME       => $name,
                    Model::CONTROLLER => $controller
                ]
            ]
        ),
        'mediaObjects' . $upper . 'Videos' => fn( ContainerInterface $container ) => new Edges
        (
            $container ,
            'mediaObjects_things_videos',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'mediaObjects',
                    Model::CONTROLLER => VideoObjectsController::class
                ],
                Model::TO =>
                [
                    Model::NAME       => $name,
                    Model::CONTROLLER => $controller
                ]
            ]
        )
    ];
};

// register all mediaObjects controllers

return $core
     + $init( 'applications'      , ApplicationsController::class      )
     + $init( 'articles'          , ArticlesController::class          )
     + $init( 'brands'            , BrandsController::class            )
     + $init( 'conceptualObjects' , ConceptualObjectsController::class )
     + $init( 'courses'           , CoursesController::class           )
     + $init( 'events'            , EventsController::class            )
     + $init( 'observations'      , ObservationsController::class      )
     + $init( 'organizations'     , OrganizationsController::class     )
     + $init( 'people'            , PeopleController::class            )
     + $init( 'places'            , PlacesController::class            )
     + $init( 'stages'            , StagesController::class            )
     + $init( 'steps'             , StepsController::class             )
;
