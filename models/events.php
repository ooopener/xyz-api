<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\events\EventsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Events ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Offers ;
use xyz\ooopener\models\Photos ;
use xyz\ooopener\models\Websites ;

return
[
    Events::class => fn( ContainerInterface $container ) => new Events
    (
        $container ,
        'events' ,
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ],
                'ids' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD
                ],
                'idsSorted' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD_SORTED
                ],
                'location' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE ,
                    Model::EDGE       => 'events_places'
                ],
                'additionalType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS ,
                    Model::EDGE       => 'events_eventsTypes'
                ],
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ]
            ,
            Model::SEARCHABLE => [ 'name' ] ,
            Model::SORTABLE   =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'startDate' => 'startDate',
                'endDate'   => 'endDate'
            ],
            Model::EDGES => ( require __MODELS__ . 'event/edges.php')( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'event/joins.php')( 'full' )
        ]
    ),

    'eventKeywords' => fn( ContainerInterface $container ) => new Keywords
    (
        $container ,
        'events_keywords'
    ),

    'eventEventsKeywords' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_eventsKeywords',
        [
            Model::FROM =>
            [
                Model::NAME       => 'events_keywords',
                Model::CONTROLLER => 'eventKeywordsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventEventsOffers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_eventsOffers',
        [
            Model::FROM =>
            [
                Model::NAME       => 'events_offers',
                Model::CONTROLLER => 'eventOffersController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventEventsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_eventsTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'events_types',
                Model::CONTROLLER => 'eventsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventOffers' => fn( ContainerInterface $container ) => new Offers
    (
        $container ,
        'events_offers'  ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'category',
                    Model::CONTROLLER => 'offersCategoriesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'eventOrganizersOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_organizers_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventOrganizersPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_organizers_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventPhotos' => fn( ContainerInterface $container ) => new Photos
    (
        $container ,
        'events_photos' ,
        [
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ),

    'eventPlaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_places',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventSubEvents' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_subEvents' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'events' ,
                Model::CONTROLLER => EventsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'events' ,
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ),

    'eventWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'events_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ) ,

    'eventEventsWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_eventsWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'events_websites',
                Model::CONTROLLER => 'eventWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'events',
                Model::CONTROLLER => EventsController::class
            ]
        ]
    ) ,

    'eventWorksFeatured' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'events_works_featured'  ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects' ,
                Model::CONTROLLER => ConceptualObjectsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'events' ,
                Model::CONTROLLER => EventsController::class
            ]
        ]
    )
];
