<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Collections ;
use xyz\ooopener\models\Model ;

return
[
    'settingsList' => fn( ContainerInterface $container ) => new Collections
    (
        $container ,
        "settings" ,
        [
            Model::FACETABLE =>
            [
                'id'       => [ Model::FACET_TYPE => Model::FACET_FIELD ] ,
                'category' => [ Model::FACET_TYPE => Model::FACET_FIELD ]
            ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE   =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'category' => 'category',
            ]
        ]
    )
];
