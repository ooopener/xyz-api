<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\diseases\DiseasesController ;

use xyz\ooopener\models\Diseases ;
use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\MedicalAnalysis ;
use xyz\ooopener\models\Model ;

return
[
    Diseases::class => fn( ContainerInterface $container ) => new Diseases
    (
        $container ,
        'diseases',
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ],
                'ids' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD
                ],
                'additionalType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS,
                    Model::EDGE       => 'diseases_diseasesTypes'
                ],
                'level' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS,
                    Model::EDGE       => 'diseases_diseasesLevels'
                ],
                'observationType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS,
                    Model::EDGE       => 'diseases_observationsTypes'
                ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ]
            ,
            Model::SEARCHABLE =>
            [
                'name' , 'alternateName'
            ]
            ,
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'disease/edges.php')( 'full' )
        ]
    ),

    'analysisMethod' => fn( ContainerInterface $container ) => new MedicalAnalysis
    (
        $container ,
        'diseases_analysisMethod' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'analysisMethodsController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'diseaseDiseasesAnalysisMethod' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'diseases_diseasesAnalysisMethod',
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases_analysisMethod',
                Model::CONTROLLER => 'diseaseAnalysisMethodController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class
            ]
        ]
    ),

    'analysisSampling' => fn( ContainerInterface $container ) => new MedicalAnalysis
    (
        $container ,
        'diseases_analysisSampling' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'medicalSamplingsController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'diseaseDiseasesAnalysisSampling' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'diseases_diseasesAnalysisSampling',
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases_analysisSampling',
                Model::CONTROLLER => 'diseaseAnalysisSamplingController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class
            ]
        ]
    ),

    'diseaseDiseasesLevels' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'diseases_diseasesLevels',
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases_levels',
                Model::CONTROLLER => 'diseasesLevelsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class
            ]
        ]
    ),

    'diseaseDiseasesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'diseases_diseasesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases_types',
                Model::CONTROLLER => 'diseasesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class
            ]
        ]
    ),

    'diseaseTransmissionsMethods' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'diseases_transmissionsMethods',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_transmissions_methods',
                Model::CONTROLLER => 'medicalTransmissionsMethodsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class
            ]
        ]
    )
];
