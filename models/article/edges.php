<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'articlesTypesController',
            Model::EDGECONTROLLER => 'articleArticlesTypesController',
            Model::SKIN           => 'list'
        ],
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php'
    ];

    // compact skin
    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'articleWebsitesController',
                Model::EDGECONTROLLER => 'articleArticlesWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            ( require 'article.php' )( 'hasPart'     , 'list' , 'articleHasPartController'     ),
            ( require 'article.php' )( 'isPartOf'    , 'list' , 'articleHasPartController'     , 'reverse' ),
            ( require 'article.php' )( 'isRelatedTo' , 'list' , 'articleIsRelatedToController' ),
            ( require 'article.php' )( 'isSimilarTo' , 'list' , 'articleIsSimilarToController' )
        ];
    }

    return $edges ;
};
