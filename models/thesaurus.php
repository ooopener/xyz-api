<?php

$create = require( 'thesaurus/create.php' ) ;

return function( $init = NULL ) use ( $create )
{
    $definitions = [] ;
    if( is_array( $init )  )
    {
        foreach( $init as $key => $value )
        {
            $definitions[ $key ] = $create( $value ) ;
        }
    }
    return $definitions ;
};

