<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Teams ;

return
[
    Teams::class => function( ContainerInterface $container )
    {
        return new Teams
        (
            $container ,
            "teams",
            [
                Model::FACETABLE =>
                [
                    'id' =>
                    [
                        Model::FACET_TYPE   => Model::FACET_FIELD
                    ],
                    'ids' =>
                    [
                        Model::FACET_TYPE   => Model::FACET_LIST_FIELD
                    ]

                ],
                Model::SEARCHABLE =>
                [
                    'name'
                ],
                Model::SORTABLE =>
                [
                    'id'         => '_key',
                    'active'     => 'active',
                    'fr'         => 'alternateName.fr',
                    'identifier' => 'identifier',
                    'name'       => 'name',
                    'created'    => 'created',
                    'modified'   => 'modified'
                ]
            ]
        ) ;
    }
];
