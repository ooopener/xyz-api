<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' ) : array
{
    return
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'observationsTypesController',
            Model::EDGECONTROLLER => 'observationObservationsTypesController',
            Model::SKIN           => 'list'
        ],
        [
            Model::NAME           => 'eventStatus',
            Model::CONTROLLER     => 'observationsStatusController',
            Model::EDGECONTROLLER => 'observationObservationsStatusController',
            Model::SKIN           => 'list'
        ],

        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php' ,

        ( require __MODELS__ . 'livestock/livestock.php' )( 'about'    , 'normal' , 'observationLivestocksController' ) ,
        ( require __MODELS__ . 'person/person.php'       )( 'actor'    , 'list'   , 'observationActorsController'     ) ,
        ( require __MODELS__ . 'person/person.php'       )( 'attendee' , 'list'   , 'observationAttendeesController'  ) ,
        ( require __MODELS__ . 'place/place.php'         )( 'location' , 'list'   , 'observationPlacesController'     ) ,
        ( require __MODELS__ . 'user/user.php'           )( 'owner'    , 'list'   , 'observationOwnersController'     ) ,
        ( require __MODELS__ . 'workshop/workshop.php'   )( 'subject'  , 'full'   , 'observationWorkshopsController'  )
    ] ;
};
