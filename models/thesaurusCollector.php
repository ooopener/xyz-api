<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Collections ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\things\Thing;

return
[
    'thesaurusCollection' => fn( ContainerInterface $container ) => new Collections
    (
        $container ,
        "thesaurus" ,
        [
            Model::FACETABLE =>
            [
                'id'         => [ Model::FACET_TYPE => Model::FACET_FIELD ],
                'category'   => [ Model::FACET_TYPE => Model::FACET_FIELD ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::FILLABLE =>
            [
                'active'     => Thing::FILTER_INT,
                'category'   => Thing::FILTER_DEFAULT,
                'identifier' => Thing::FILTER_DEFAULT,
                'name'       => Thing::FILTER_DEFAULT,
                'path'       => Thing::FILTER_DEFAULT
            ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE   =>
            [
                'id'       => '_key' ,
                'name'     => 'name' ,
                'category' => 'category'
            ]
        ]
    )
];

