<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges =
    [
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php' ,
        ( require __MODELS__ . 'place/place.php')( 'location' , 'normal' , 'stageLocationsController' ),
        [
            Model::NAME           => 'status',
            Model::CONTROLLER     => 'coursesStatusController',
            Model::EDGECONTROLLER => 'stageCoursesStatusController',
            Model::SKIN           => 'list'
        ]
    ] ;

    // compact skin
    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'activities',
                Model::CONTROLLER     => 'activitiesController',
                Model::EDGECONTROLLER => 'stageActivitiesController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'stageWebsitesController',
                Model::EDGECONTROLLER => 'stageStagesWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    // full skin
    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
        ];
    }

    return $edges ;
};
