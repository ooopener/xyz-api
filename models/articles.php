<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\articles\ArticlesController ;

use xyz\ooopener\models\Articles ;
use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Websites ;

return
[
    Articles::class => fn( ContainerInterface $container ) => new Articles
    (
        $container ,
        'articles' ,
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ] ,
                'ids' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD
                ] ,
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ]
            ,
            Model::SEARCHABLE =>
            [
                'name'
            ],
            Model::SORTABLE   =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'created'  => 'created',
                'modified' => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'article/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'article/joins.php')( 'full' )
        ]
    ),

    'articleArticlesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'articles_articlesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'articles_types',
                Model::CONTROLLER => 'articlesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ]
        ]
    ),

    'articleHasPart' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'articles_hasPart' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ]
        ]
    ),

    'articleIsRelatedTo' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'articles_isRelatedTo' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ]
        ]
    ),

    'articleIsSimilarTo' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'articles_isSimilarTo' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ]
        ]
    ),

    'articleWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'articles_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'articleArticlesWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'articles_articlesWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'articles_websites',
                Model::CONTROLLER => 'articleWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'articles',
                Model::CONTROLLER => ArticlesController::class
            ]
        ]
    )
];
