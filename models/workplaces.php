<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\livestocks\SectorsController;
use xyz\ooopener\controllers\livestocks\WorkplacesController;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Workplaces ;

return
[
    Workplaces::class => fn( ContainerInterface $container ) => new Workplaces
    (
        $container ,
        'workplaces' ,
        [
            Model::FACETABLE  => [ 'id' => [ Model::FACET_TYPE => Model::FACET_FIELD ] ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE   =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'workplace/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'workplace/joins.php')( 'full' )
        ]
    ),

    'workplacesSectors' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'workplaces_sectors' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'sectors',
                Model::CONTROLLER => SectorsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'workplaces',
                Model::CONTROLLER => WorkplacesController::class
            ]
        ]
    )
];
