<?php

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges =
    [
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php' ,

    ] ;

    // compact skin
    if( $skin == 'compact' ||$skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            ( require __MODELS__ . 'place/place.php' )( 'location' , 'normal' , 'stepLocationsController' ) ,
            ( require __MODELS__ . 'stage/stage.php' )( 'stage'    , 'full'   , 'stepStagesController'    )
        ];
    }

    return $edges ;
};
