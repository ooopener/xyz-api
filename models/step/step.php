<?php

use xyz\ooopener\controllers\steps\StepsController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param string|null $edge
 * @param string|null $direction
 * @param bool $array
 * @param bool $lock
 *
 * @return array
 */
return fn( string $property = 'steps' , string $skin = '' , string $edge = null , string $direction = null , bool $array = false , bool $lock = false ) : array =>
[
    Model::NAME       => $property,
    Model::CONTROLLER => StepsController::class,
    Model::SKIN       => $skin,
    Model::ARRAY      => $array,
    Model::EDGES      => $lock ? NULL : (require 'edges.php')( $skin ) ,
    Model::JOINS      => $lock ? NULL : (require 'joins.php')( $skin ) ,
    !( $edge )        ?: Model::EDGECONTROLLER => $edge,
    !( $direction )   ?: Model::DIRECTION      => $direction
];

