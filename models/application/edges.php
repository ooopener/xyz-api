<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'applicationsTypesController',
            Model::EDGECONTROLLER => 'applicationApplicationsTypesController',
            Model::SKIN           => 'list'
        ],
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php'
    ];

    // compact skin
    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            (require __MODELS__ . 'user/user.php')( 'owner' , 'list' , 'applicationUsersController' ),
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'applicationWebsitesController',
                Model::EDGECONTROLLER => 'applicationApplicationsWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    // full skin
    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
        ];
    }

    return $edges ;
};
