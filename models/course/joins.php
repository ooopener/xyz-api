<?php

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $joins =
    [
        ( require __MODELS__ . 'step/step.php')( 'steps' , 'full' , null , null , true )
    ] ;

    // steps skin
    /*if( $skin == 'steps' )
    {
        $joins =
        [
            ...$joins,
            ( require __MODELS__ . 'step/step.php')( 'steps' , 'full' , null , null , true )
        ];
    }*/

    // full skin
    if( $skin == 'full' )
    {
        $joins =
        [
            ...$joins,
            require __MODELS__ . 'mediaObjects/audios.php',
            require __MODELS__ . 'mediaObjects/photos.php',
            require __MODELS__ . 'mediaObjects/videos.php'
        ];
    }

    return $joins ;
};
