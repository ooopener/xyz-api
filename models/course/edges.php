<?php

use xyz\ooopener\controllers\courses\CourseTransportationsController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'coursesTypesController',
            Model::EDGECONTROLLER => 'courseCoursesTypesController',
            Model::SKIN           => 'list'
        ],
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        [
            Model::NAME           => 'level',
            Model::CONTROLLER     => 'coursesLevelsController',
            Model::EDGECONTROLLER => 'courseCoursesLevelsController',
            Model::SKIN           => 'list'
        ],
        [
            Model::NAME           => 'openingHoursSpecification',
            Model::CONTROLLER     => 'courseOpeningHoursController',
            Model::EDGECONTROLLER => 'courseCoursesOpeningHoursController',
            Model::SKIN           => 'list'
        ],
        [
            Model::NAME           => 'status',
            Model::CONTROLLER     => 'coursesStatusController',
            Model::EDGECONTROLLER => 'courseCoursesStatusController',
            Model::SKIN           => 'list'
        ],
        require __MODELS__ . 'mediaObjects/video.php' ,
    ] ;

    // compact skin
    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'audience',
                Model::CONTROLLER     => 'coursesAudiencesController',
                Model::EDGECONTROLLER => 'courseCoursesAudiencesController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'path',
                Model::CONTROLLER     => 'coursesPathsController',
                Model::EDGECONTROLLER => 'courseCoursesPathsController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'transportations',
                Model::CONTROLLER     => CourseTransportationsController::class,
                Model::EDGECONTROLLER => 'courseCoursesTransportationsController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'transportationsController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    return $edges ;
};
