<?php

use xyz\ooopener\controllers\organizations\OrganizationNumbersController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'organizationsTypesController',
            Model::EDGECONTROLLER => 'organizationOrganizationsTypesController',
            Model::SKIN           => 'list'
        ],

        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/logo.php'  ,
        require __MODELS__ . 'mediaObjects/video.php' ,

        [
            Model::NAME           => 'email',
            Model::CONTROLLER     => 'organizationEmailsController',
            Model::EDGECONTROLLER => 'organizationOrganizationsEmailsController',
            Model::SKIN           => 'normal',
            Model::JOINS          =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'emailsTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],
        [
            Model::NAME           => 'telephone',
            Model::CONTROLLER     => 'organizationPhoneNumbersController',
            Model::EDGECONTROLLER => 'organizationOrganizationsPhoneNumbersController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],

        ( require __MODELS__ . 'place/place.php' )( 'location' , 'list' , 'organizationPlacesController' )
    ] ;

//    if( $skin == 'list' || $skin == 'compact' || $skin == 'full' )
//    {
//        $edges =
//        [
//            ...$edges
//        ];
//    }

    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,

            [
                Model::NAME           => 'ape',
                Model::CONTROLLER     => 'organizationsNafController',
                Model::EDGECONTROLLER => 'organizationApeController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'legalForm',
                Model::CONTROLLER     => 'businessEntityTypesController',
                Model::EDGECONTROLLER => 'organizationLegalFormController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'organizationWebsitesController',
                Model::EDGECONTROLLER => 'organizationOrganizationsWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],

            ( require 'organization.php' )( 'department'         , 'list' , 'organizationDepartmentController'        ),
            ( require 'organization.php' )( 'subOrganization'    , 'list' , 'organizationSubOrganizationsController'  ),
            ( require 'organization.php' )( 'parentOrganization' , 'list' , 'organizationSubOrganizationsController' , 'reverse' ),

            ( require __MODELS__ . 'person/person.php' ) ( 'employees'        , 'list' , 'organizationEmployeesController' ),
            ( require __MODELS__ . 'person/person.php' ) ( 'founder'          , 'list' , 'organizationFounderController' ),
            ( require __MODELS__ . 'place/place.php'   ) ( 'foundingLocation' , 'list' , 'organizationFoundingLocationController' ),
            ( require __MODELS__ . 'person/person.php' ) ( 'memberOf'         , 'list' , 'organizationMembersOrganizationsController' , 'reverse' ),

            ( require __MODELS__ . 'conceptualObject/conceptualObject.php' )( 'owns' , '' , 'conceptualObjectProductionsOrganizationsController' , 'reverse' ),
        ];
    }

    // skin full
    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'numbers',
                Model::CONTROLLER     => OrganizationNumbersController::class,
                Model::EDGECONTROLLER => 'organizationOrganizationsNumbersController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'organizationsNumbersTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    return $edges ;
};
