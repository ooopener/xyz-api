<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\ActivityLogs ;
use xyz\ooopener\models\Model ;

return
[
    ActivityLogs::class => fn( ContainerInterface  $container ) => new ActivityLogs
    (
        $container ,
        'users_activity_logs',
        [
            Model::FACETABLE =>
            [
                'resource' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'resource'
                ],
                'user' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'user'
                ]
            ],
            Model::SEARCHABLE => [ 'user' , 'method' , 'resource' ],
            Model::SORTABLE   =>
            [
                'id'      => '_key',
                'created' => 'created'
            ],
            Model::JOINS => ( require __MODELS__ . 'activityLog/joins.php' )( 'full' )
        ]
    )
];
