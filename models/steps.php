<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\places\PlacesController ;
use xyz\ooopener\controllers\stages\StagesController ;
use xyz\ooopener\controllers\steps\StepsController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Steps ;

return
[
    Steps::class => fn( ContainerInterface $container ) => new Steps
    (
        $container ,
        "steps",
        [
            Model::FACETABLE =>
            [
                'id'  => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids' => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ]
            ]
            ,
            Model::SEARCHABLE => [ 'name' ] ,
            Model::SORTABLE   =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'step/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'step/joins.php' )( 'full' )
        ]
    ),

    'stepLocations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'steps_locations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'steps',
                Model::CONTROLLER => StepsController::class
            ]
        ]
    ),

    'stepStages' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'steps_stages',
        [
            Model::FROM =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'steps',
                Model::CONTROLLER => StepsController::class
            ]
        ]
    )
];
