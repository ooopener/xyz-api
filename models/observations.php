<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\livestocks\observations\ObservationsController;
use xyz\ooopener\controllers\livestocks\LivestocksController ;
use xyz\ooopener\controllers\livestocks\WorkshopsController;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;
use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController ;
use xyz\ooopener\controllers\technicians\TechniciansController ;
use xyz\ooopener\controllers\users\UsersController ;
use xyz\ooopener\controllers\veterinarians\VeterinariansController;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Observations ;

return
[
    Observations::class => fn( ContainerInterface $container ) => new Observations
    (
        $container ,
        'observations',
        [
            Model::FACETABLE =>
            [
                'livestock' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE,
                    Model::EDGE       => 'observations_livestocks'
                ],
                'withStatus' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ],
                'workshop' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE,
                    Model::EDGE       => 'observations_workshops'
                ]
            ],
            Model::SEARCHABLE =>
            [
                'name'
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'observation/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'observation/joins.php' )( 'full' )
        ]
    ),

    'observationActors' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'observations_actors' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'people' ,
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations' ,
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationAttendees' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'observations_attendees' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'people' ,
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations' ,
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationAuthorityMedicalLaboratories' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'observations_authority_medicalLaboratories',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medicalLaboratories',
                Model::CONTROLLER => MedicalLaboratoriesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationAuthorityTechnicians' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'observations_authority_technicians',
        [
            Model::FROM =>
            [
                Model::NAME       => 'technicians',
                Model::CONTROLLER => TechniciansController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationAuthorityVeterinarians' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'observations_authority_veterinarians',
        [
            Model::FROM =>
            [
                Model::NAME       => 'veterinarians',
                Model::CONTROLLER => VeterinariansController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationLivestocks' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_livestocks',
        [
            Model::FROM =>
            [
                Model::NAME       => 'livestocks',
                Model::CONTROLLER => LivestocksController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationObservationsStatus' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_observationsStatus',
        [
            Model::FROM =>
            [
                Model::NAME       => 'observations_status',
                Model::CONTROLLER => 'observationsStatusController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationObservationsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_observationsTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationWorkshops' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_workshops',
        [
            Model::FROM =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationPlaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_places',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    ),

    'observationOwners' => fn( ContainerInterface $container ) => new Edges
    (
        $container,
        'observations_owners',
        [
            Model::FROM =>
            [
                Model::NAME       => 'users',
                Model::CONTROLLER => UsersController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations',
                Model::CONTROLLER => ObservationsController::class
            ]
        ]
    )
];
