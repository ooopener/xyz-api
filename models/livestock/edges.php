<?php

$getMedicalLaboratory = require __MODELS__ . 'medicalLaboratory/medicalLaboratory.php' ;
$getOrganization      = require __MODELS__ . 'organization/organization.php' ;
$getNumbers           = require __MODELS__ . 'livestockNumbers/livestockNumbers.php' ;
$getTechnician        = require __MODELS__ . 'technician/technician.php' ;
$getVeterinarian      = require __MODELS__ . 'veterinarian/veterinarian.php' ;
$getWorkshop          = require __MODELS__ . 'workshop/workshop.php' ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' )
       use
       (
           $getMedicalLaboratory,
           $getOrganization ,
           $getNumbers ,
           $getTechnician ,
           $getVeterinarian ,
           $getWorkshop
       )
{
    $edges =
    [
        $getOrganization ( 'organization' , 'normal' , 'livestockOrganizationsController' ) ,
        $getNumbers      ( 'numbers'      , 'livestocksLivestocksNumbersController' )
    ] ;

    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            $getMedicalLaboratory ( 'medicalLaboratories' , 'list' , 'livestockMedicalLaboratoriesController' ),
            $getTechnician        ( 'technicians'         , 'list' , 'livestockTechniciansController'         ),
            $getVeterinarian      ( 'veterinarians'       , 'list' , 'livestockVeterinariansController'       ),
            $getWorkshop          ( 'workshops'           , 'list' , 'livestocksWorkshopsController'          ) // FIXME the bug in this definition
        ];
    }

    return $edges ;
};
