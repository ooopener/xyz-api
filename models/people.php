<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\people\PeopleController ;

use xyz\ooopener\models\People ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Emails ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\PhoneNumbers ;
use xyz\ooopener\models\Websites ;

use xyz\ooopener\things\Thing ;

return
[
    People::class => function( ContainerInterface $container )
    {
        return new People
        (
            $container ,
            'people',
            [
                Model::FACETABLE =>
                [
                    'gender' =>
                    [
                        Model::FACET_TYPE => Model::FACET_THESAURUS ,
                        Model::EDGE       => 'people_genders'
                    ],
                    'id' =>
                    [
                        Model::FACET_TYPE   => Model::FACET_FIELD
                    ],
                    'ids' =>
                    [
                        Model::FACET_TYPE   => Model::FACET_LIST_FIELD
                    ],
                    'withStatus' =>
                    [
                        Model::FACET_TYPE     => Model::FACET_FIELD ,
                        Model::FACET_PROPERTY => 'withStatus'
                    ]
                ]
                ,
                Model::SEARCHABLE =>
                [
                    'name', 'givenName', 'familyName', 'alternateName'
                ]
                ,
                Model::SORTABLE =>
                [
                    'id'                => '_key',
                    'addressCountry'    => 'address.addressCountry',
                    'addressDepartment' => 'address.addressDepartment',
                    'addressLocality'   => 'address.addressLocality',
                    'addressRegion'     => 'address.addressRegion',
                    'postalCode'        => 'address.postalCode',
                    'streetAddress'     => 'address.streetAddress',
                    'name'              => 'name',
                    'nationality'       => 'nationality',
                    'alternateName'     => 'alternateName',
                    'familyName'        => 'familyName',
                    'givenName'         => 'givenName',
                    'created'           => 'created',
                    'modified'          => 'modified',
                    'birthDate'         => 'birthDate',
                    'birthPlace'        => 'birthPlace',
                    'deathDate'         => 'deathDate',
                    'deathPlace'        => 'deathPlace'
                ],
                Model::EDGES => (require __MODELS__ . 'person/edges.php')( 'full' ),
                Model::JOINS => (require __MODELS__ . 'person/joins.php')( 'full' )
            ]
        ) ;
    },

    'peopleEmails' => function( ContainerInterface $container )
    {
        return new Emails
        (
            $container ,
            'people_emails',
            [
                Model::JOINS =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'emailsTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ) ;
    },

    'peopleKeywords' => function( ContainerInterface $container )
    {
        return new Keywords( $container , 'people_keywords' ) ;
    },

    'peoplePeopleEmails' => function( ContainerInterface $container )
    {
        return new Edges
        (
            $container ,
            'people_peopleEmails',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'people_emails',
                    Model::CONTROLLER => 'peopleEmailsController'
                ],
                Model::TO =>
                [
                    Model::NAME       => 'people',
                    Model::CONTROLLER => PeopleController::class
                ]
            ]
        ) ;
    },

    'peoplePeopleKeywords' => function( ContainerInterface $container )
    {
        return new Edges
        (
            $container ,
            'people_peopleKeywords',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'people_keywords',
                    Model::CONTROLLER => 'peopleKeywordsController'
                ],
                Model::TO =>
                [
                    Model::NAME       => 'people',
                    Model::CONTROLLER => PeopleController::class
                ]
            ]
        ) ;
    },

    'peoplePeoplePhoneNumbers' => function( ContainerInterface $container )
    {
        return new Edges
        (
            $container ,
            'people_peoplePhoneNumbers',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'people_phoneNumbers',
                    Model::CONTROLLER => 'peoplePhoneNumbersController'
                ],
                Model::TO =>
                [
                    Model::NAME       => 'people',
                    Model::CONTROLLER => PeopleController::class
                ]
            ]
        ) ;
    },

    'peoplePeopleWebsites' => function( ContainerInterface $container )
    {
        return new Edges
        (
            $container ,
            'people_peopleWebsites',
            [
                Model::FROM =>
                [
                    Model::NAME       => 'people_websites',
                    Model::CONTROLLER => 'peopleWebsitesController'
                ],
                Model::TO =>
                [
                    Model::NAME       => 'people',
                    Model::CONTROLLER => PeopleController::class
                ]
            ]
        ) ;
    },

    'peoplePhoneNumbers' => function( ContainerInterface $container )
    {
        return new PhoneNumbers
        (
            $container ,
            'people_phoneNumbers',
            [
                Model::JOINS =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'phoneNumbersTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ) ;
    },

    'peopleJobs' => function( ContainerInterface $container )
    {
        return new Edges
        (
            $container ,
            'people_jobs' ,
            [
                Model::FROM =>
                [
                    Model::NAME       => 'jobs',
                    Model::CONTROLLER => 'jobsController'
                ],
                Model::TO =>
                [
                    Model::NAME       => 'people',
                    Model::CONTROLLER => PeopleController::class
                ]
            ]
        ) ;
    },

    'peopleWebsites' => function( ContainerInterface $container )
    {
        return new Websites
        (
            $container ,
            'people_websites' ,
            [
                Model::JOINS =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ) ;
    },

    'personHasBrand' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'hasBrand',
        [
            Model::TO =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::FILLABLE =>
            [
                'type' => Thing::FILTER_DEFAULT
            ]
        ]
    )
];
