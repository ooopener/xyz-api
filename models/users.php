<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\users\UsersController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Users ;
use xyz\ooopener\models\UserPermissions ;

return
[
    Users::class => fn( ContainerInterface $container ) => new Users
    (
        $container ,
        "users" ,
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ],
                'ids' =>
                [
                    Model::FACET_TYPE   => Model::FACET_LIST_FIELD
                ]
                ,
                'provider' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'provider'
                ]
            ],
            Model::SEARCHABLE =>
            [
                'name' , 'givenName' , 'familyName' , 'email'
            ],
            Model::SORTABLE =>
            [
                'id'         => '_key',
                'givenName'  => 'givenName',
                'familyName' => 'familyName',
                'name'       => 'name',
                'created'    => 'created',
                'modified'   => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'user/edges.php')('full'),
            Model::JOINS => (require __MODELS__ . 'user/joins.php')('full')
        ]
    ),

    UserPermissions::class => fn( ContainerInterface  $container )
                           => new UserPermissions( $container , 'users' ) ,

    'userFavorites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'users_favorites' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'things',
                Model::CONTROLLER => 'thingsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'users' ,
                Model::CONTROLLER => UsersController::class
            ]
        ]
    )
];
