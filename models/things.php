<?php

use xyz\ooopener\models\Collections ;
use xyz\ooopener\models\Model ;

use xyz\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;

return
[
    'things' => fn( $container ) => new Collections
    (
        $container ,
        NULL,
        [
            Model::FACETABLE =>
            [
                'type' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'path' ]
            ],
            Model::SEARCHABLE =>
            [
                'name' ,
                'headline' ,
                'alternativeHeadline' ,
                'alternateName'
            ],
            Model::SORTABLE =>
            [
                'name',
                'created' ,
                'modified'
            ],
            Model::EDGES =>
            [
                [
                    Model::NAME           => 'audio',
                    Model::CONTROLLER     => AudioObjectsController::class,
                    Model::EDGECONTROLLER => 'mediaObjectsThingsAudioController',
                    Model::SKIN           => 'list'
                ],
                [
                    Model::NAME           => 'image',
                    Model::CONTROLLER     => ImageObjectsController::class,
                    Model::EDGECONTROLLER => 'mediaObjectsThingsImageController',
                    Model::SKIN           => 'list'
                ],
                [
                    Model::NAME           => 'video',
                    Model::CONTROLLER     => VideoObjectsController::class,
                    Model::EDGECONTROLLER => 'mediaObjectsThingsVideoController',
                    Model::SKIN           => 'list'
                ]
            ]
        ]
    )
];
