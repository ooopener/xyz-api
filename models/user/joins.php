<?php

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    return
    [
        require __MODELS__ . 'thesaurus/gender.php',
        ( require __MODELS__ . 'person/person.php' )()
    ];
};
