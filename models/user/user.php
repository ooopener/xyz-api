<?php

use xyz\ooopener\controllers\users\UsersController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param string|null $edge
 * @param string|null $direction
 *
 * @return array
 */
return fn( string $property , string $skin , string $edge = null , string $direction = null ) : array =>
[
    Model::NAME       => $property,
    Model::CONTROLLER => UsersController::class,
    Model::KEY        => 'uuid',
    Model::SKIN       => $skin,
    Model::EDGES      => ( require 'edges.php' )($skin),
    Model::JOINS      => ( require 'joins.php' )($skin),
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION => $direction
];

