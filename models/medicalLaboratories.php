<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoryIdentifierController;
use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\MedicalLaboratories ;
use xyz\ooopener\models\MedicalLaboratoryIdentifier ;
use xyz\ooopener\models\Model ;

return
[
    MedicalLaboratories::class => fn( ContainerInterface $container ) => new MedicalLaboratories
    (
        $container ,
        'medicalLaboratories' ,
        [
            Model::FACETABLE =>
            [
                'id'         => [ Model::FACET_TYPE => Model::FACET_FIELD      ] ,
                'ids'        => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ] ,
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ],
            Model::SEARCHABLE => [ 'organization.name' ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'organization.name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'medicalLaboratory/edges.php' )( 'full' ) ,
            Model::JOINS => ( require __MODELS__ . 'medicalLaboratory/joins.php' )( 'full' )
        ]
    ),

    MedicalLaboratoryIdentifier::class => fn( ContainerInterface $container ) => new MedicalLaboratoryIdentifier
    (
        $container ,
        'medicalLaboratories_identifier' ,
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE   => Model::FACET_FIELD
                ]
            ],
            Model::SEARCHABLE =>
            [
                Model::NAME
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'medicalLaboratoriesIdentifierTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'medicalLaboratoriesIdentifierEdge' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'medicalLaboratories_identifier_edge' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_laboratories_identifier',
                Model::CONTROLLER => MedicalLaboratoryIdentifierController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'medicalLaboratories',
                Model::CONTROLLER => MedicalLaboratoriesController::class
            ]
        ]
    ),

    'medicalLaboratoriesMedicalSpecialties' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'medicalLaboratories_medical_specialties_edge',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medicalSpecialties',
                Model::CONTROLLER => 'medicalSpecialtiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'medicalLaboratories',
                Model::CONTROLLER => MedicalLaboratoriesController::class
            ]
        ]
    ),

    'medicalLaboratoryOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'medicalLaboratories_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations' ,
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'medicalLaboratories' ,
                Model::CONTROLLER => MedicalLaboratoriesController::class
            ]
        ]
    ),
];
