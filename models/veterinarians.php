<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\veterinarians\VeterinariansController;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Veterinarians ;

return
[
    Veterinarians::class => fn( ContainerInterface $container ) => new Veterinarians
    (
        $container,
        'veterinarians',
        [
            Model::FACETABLE =>
            [
                'id'  => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids' => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE =>
            [
                'name'
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'authority.name',
                'created'   => 'created',
                'modified'  => 'modified',
                'after'     => 'authority.name'
            ],
            Model::EDGES => (require __MODELS__ . 'veterinarian/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'veterinarian/joins.php')( 'full' )
        ]
    ),

    'veterinarianMedicalSpecialties' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'veterinarians_medicalSpecialties',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medicalSpecialties',
                Model::CONTROLLER => 'medicalSpecialtiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'veterinarians',
                Model::CONTROLLER => VeterinariansController::class
            ]
        ]
    ),

    'veterinarianOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'veterinarians_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'veterinarians',
                Model::CONTROLLER => VeterinariansController::class
            ]
        ]
    ),

    'veterinarianPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'veterinarians_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'veterinarians',
                Model::CONTROLLER => VeterinariansController::class
            ]
        ]
    )
];
