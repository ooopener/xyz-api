<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\livestocks\SectorsController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Sectors ;

return
[
    Sectors::class => fn( ContainerInterface $container ) => new Sectors
    (
        $container ,
        'sectors' ,
        [
            Model::FACETABLE  => [ 'id' => [ Model::FACET_TYPE => Model::FACET_FIELD ] ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE   =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name     ' => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'sector/edges.php')( 'full' ) ,
            Model::JOINS => (require __MODELS__ . 'sector/joins.php')( 'full' )
        ]
    ),

    'sectorsSectorsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'sectors_sectorsTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'sectors' ,
                Model::CONTROLLER => SectorsController::class
            ]
        ]
    )
];
