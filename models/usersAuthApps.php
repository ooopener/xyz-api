<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\UsersAuthApps ;

return
[
    UsersAuthApps::class => fn( ContainerInterface $container )
                         => new UsersAuthApps( $container , 'users_auth_apps' )
];

