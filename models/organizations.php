<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\organizations\OrganizationNumbersController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Emails ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\PhoneNumbers ;
use xyz\ooopener\models\Photos ;
use xyz\ooopener\models\Websites ;

use xyz\ooopener\things\Thing ;

use xyz\ooopener\models\organizations\OrganizationNumbers ;

return
[
    Organizations::class => fn( ContainerInterface $container ) => new Organizations
    (
        $container ,
        'organizations' ,
        [
            Model::FACETABLE =>
            [
                'addressCountry' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'address.addressCountry'
                ],
                'addressDepartment' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'address.addressDepartment'
                ],
                'addressLocality' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'address.addressLocality'
                ],
                'addressRegion' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'address.addressRegion'
                ],
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ],
                'ids' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD
                ],
                'additionalType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS,
                    Model::EDGE       => 'organizations_organizationsTypes'
                ] ,
                'numbers' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE_COMPLEX ,
                    Model::EDGE       => 'organizations_organizationsNumbers'
                ] ,
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ],
            Model::SEARCHABLE => [ 'name' , 'addressCountry', 'addressDepartment' ],
            Model::SORTABLE =>
            [
                'id'                => '_key',
                'addressCountry'    => 'address.addressCountry',
                'addressDepartment' => 'address.addressDepartment',
                'addressLocality'   => 'address.addressLocality',
                'addressRegion'     => 'address.addressRegion',
                'postalCode'        => 'address.postalCode',
                'streetAddress'     => 'address.streetAddress',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'organization/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'organization/joins.php' )( 'full' )
        ]
    ),

    OrganizationNumbers::class => fn( ContainerInterface $container ) => new OrganizationNumbers
    (
        $container ,
        'organizations_numbers' ,
        [
            Model::FACETABLE =>
            [
                'id' => [ Model::FACET_TYPE => Model::FACET_FIELD ]
            ],
            Model::SEARCHABLE =>
            [
                Model::NAME
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'organizationsNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'organizationApe' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_ape',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_naf',
                Model::CONTROLLER => 'organizationsNafController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationDepartment' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_department' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationEmails' => fn( ContainerInterface $container ) => new Emails
    (
        $container ,
        'organizations_emails',
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'emailsTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'organizationEmployees' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_employees' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'people' ,
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations' ,
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationFounder' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_founder' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'people' ,
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations' ,
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationFoundingLocation' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_foundingLocation',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationHasBrand' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'hasBrand',
        [
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::FILLABLE =>
            [
                'type' => Thing::FILTER_DEFAULT
            ]
        ]
    ),

    'organizationKeywords' => fn( ContainerInterface $container ) => new Keywords
    (
        $container ,
        'organizations_keywords'
    ) ,

    'organizationLegalForm' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_legalForm',
        [
            Model::FROM =>
            [
                Model::NAME       => 'business_entity_types',
                Model::CONTROLLER => 'businessEntityTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationMembersOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_members_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationMembersPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_members_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationPhoneNumbers' => fn( ContainerInterface $container ) => new PhoneNumbers
    (
        $container ,
        'organizations_phoneNumbers',
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'organizationProvidersOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_providers_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationProvidersPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_providers_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationOrganizationsEmails' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsEmails',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_emails',
                Model::CONTROLLER => 'organizationEmailsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationOrganizationsKeywords' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsKeywords',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_keywords',
                Model::CONTROLLER => 'organizationKeywordsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationOrganizationsNumbers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsNumbers' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'organization_numbers',
                Model::CONTROLLER => OrganizationNumbersController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationOrganizationsPhoneNumbers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsPhoneNumbers',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_phoneNumbers',
                Model::CONTROLLER => 'organizationPhoneNumbersController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationOrganizationsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_types',
                Model::CONTROLLER => 'organizationsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationPhotos' => fn( ContainerInterface $container ) => new Photos
    (
        $container ,
        'organizations_photos' ,
        [
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ),

    'organizationPlaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_places',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationSubOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_subOrganizations' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    ),

    'organizationWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'organizations_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'organizationOrganizationsWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'organizations_organizationsWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations_websites',
                Model::CONTROLLER => 'organizationWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ]
        ]
    )
];
