<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' ) : array
{
    return [

        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'brandsTypesController',
            Model::EDGECONTROLLER => 'brandHasTypeController',
            Model::SKIN           => 'list'
        ],

        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/logo.php'  ,
        require __MODELS__ . 'mediaObjects/video.php' ,

        [
            Model::NAME           => 'websites',
            Model::CONTROLLER     => 'brandWebsitesController',
            Model::EDGECONTROLLER => 'placePlacesWebsitesController',
            Model::SKIN           => 'list',
            Model::JOINS          =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ] ;
};
