<?php

use xyz\ooopener\controllers\organizations\BrandsController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param ?string $edge
 * @param ?string $direction
 * @param bool $lock
 *
 * @return array
 */
return fn( string $property = 'brand' , string $skin = '' , ?string $edge = null , ?string $direction = null , bool $lock = false ) : array =>
[
    Model::NAME       => $property,
    Model::CONTROLLER => BrandsController::class,
    Model::SKIN       => $skin,
    Model::EDGES      => $lock ? NULL : (require 'edges.php')( $skin ) ,
    Model::JOINS      => $lock ? NULL : (require 'joins.php')( $skin ) ,
    !( $edge )      ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION      => $direction
];

