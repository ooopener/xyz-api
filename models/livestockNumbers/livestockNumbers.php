<?php

use xyz\ooopener\controllers\livestocks\LivestockNumbersController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $name
 * @param string|null $edge
 *
 * @return array
 */
return fn( string $name = 'identifier' , string $edge = null ) : array =>
[
    Model::NAME       => $name,
    Model::CONTROLLER => LivestockNumbersController::class,
    Model::SKIN       => 'list',
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    Model::JOINS =>
    [
        [
            Model::NAME       => 'additionalType',
            Model::CONTROLLER => 'livestocksNumbersTypesController',
            Model::SKIN       => 'list'
        ]
    ]
];
