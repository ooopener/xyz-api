<?php

$getLivestocks   = require __MODELS__ . 'livestock/livestock.php' ;
$getOrganization = require __MODELS__ . 'organization/organization.php' ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' ) use
(
   $getLivestocks,
   $getOrganization
)
{
    return
    [
        $getOrganization ( 'organization' , 'normal' , 'medicalLaboratoryOrganizationsController' ) ,
        $getLivestocks   ( 'livestocks'   , ''       , 'livestockMedicalLaboratoriesController' , 'reverse' )
    ] ;
};
