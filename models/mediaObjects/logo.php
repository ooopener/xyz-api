<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME           => 'logo',
    Model::CONTROLLER     => ImageObjectsController::class,
    Model::EDGECONTROLLER => 'mediaObjectsThingsLogoController',
    Model::SKIN           => 'list'
];
