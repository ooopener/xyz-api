<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME           => 'video',
    Model::CONTROLLER     => VideoObjectsController::class,
    Model::EDGECONTROLLER => 'mediaObjectsThingsVideoController',
    Model::SKIN           => 'list'
];
