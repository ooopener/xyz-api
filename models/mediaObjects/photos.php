<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME       => 'photos',
    Model::CONTROLLER => ImageObjectsController::class,
    Model::ARRAY      => true,
    Model::SKIN       => 'extend'
];
