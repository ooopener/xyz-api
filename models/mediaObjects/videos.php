<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME       => 'videos',
    Model::CONTROLLER => VideoObjectsController::class,
    Model::ARRAY      => true,
    Model::SKIN       => 'extend'
];
