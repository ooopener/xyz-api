<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME           => 'audio',
    Model::CONTROLLER     => AudioObjectsController::class,
    Model::EDGECONTROLLER => 'mediaObjectsThingsAudioController',
    Model::SKIN           => 'list'
];
