<?php

use xyz\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;

use xyz\ooopener\models\Model ;

return
[
    Model::NAME       => 'audios',
    Model::CONTROLLER => AudioObjectsController::class,
    Model::ARRAY      => true,
    Model::SKIN       => 'extend'
];
