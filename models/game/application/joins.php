<?php

use xyz\ooopener\models\Model ;

/**
 *
 */
return function( string $skin = '' ) : array
{
    return
    [
        [
            Model::NAME       => 'additionalType' ,
            Model::CONTROLLER => 'gamesTypesController',
            Model::SKIN       => 'list'
        ]
    ];
};
