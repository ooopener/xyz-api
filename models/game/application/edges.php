<?php

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    return
    [
        (require __MODELS__ . 'game/course/course.php')( 'quest' , 'full' , 'applicationsGamesCoursesGamesController' )
    ] ;
};
