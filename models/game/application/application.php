<?php

use xyz\ooopener\controllers\games\ApplicationGamesController ;

use xyz\ooopener\models\Model ;

return fn( string $property = 'application' , string $skin = '' , string $edge = null , string $direction = null , bool $lock = false ) : array =>
[
    Model::NAME       => $property,
    Model::CONTROLLER => ApplicationGamesController::class,
    Model::SKIN       => $skin,
    Model::EDGES      => $lock ? NULL : (require 'edges.php')( $skin ) ,
    Model::JOINS      => $lock ? NULL : (require 'joins.php')( $skin ) ,
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION => $direction
];
