<?php

use xyz\ooopener\models\Model ;

/**
 *
 */
return function( string $skin = '' ) : array
{
    return
    [
        (require __MODELS__ . 'course/course.php' )( 'about' , 'game'),
        [
            Model::NAME       => 'additionalType' ,
            Model::CONTROLLER => 'gamesTypesController',
            Model::SKIN       => 'list'
        ]
    ];
};
