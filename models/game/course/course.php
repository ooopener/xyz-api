<?php

use xyz\ooopener\controllers\games\CourseGamesController ;

use xyz\ooopener\models\Model ;

return fn( string $property = 'course' , string $skin = '' , string $edge = null , string $direction = null , bool $lock = false ) : array =>
[
    Model::NAME       => $property,
    Model::CONTROLLER => CourseGamesController::class,
    Model::SKIN       => $skin,
    Model::EDGES      => $lock ? NULL : ( require 'edges.php' )( $skin ) ,
    Model::JOINS      => $lock ? NULL : ( require 'joins.php' )( $skin ) ,
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION => $direction
];
