<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType' ,
            Model::CONTROLLER     => 'diseasesTypesController',
            Model::EDGECONTROLLER => 'diseaseDiseasesTypesController',
            Model::SKIN           => 'list',
        ],
        [
            Model::NAME           => 'level' ,
            Model::CONTROLLER     => 'diseasesLevelsController',
            Model::EDGECONTROLLER => 'diseaseDiseasesLevelsController',
            Model::SKIN           => 'list'
        ]
    ] ;

    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'analysisMethod' ,
                Model::CONTROLLER     => 'diseaseAnalysisMethodController',
                Model::EDGECONTROLLER => 'diseaseDiseasesAnalysisMethodController',
                Model::SKIN           => 'list',
                Model::JOINS =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'analysisMethodsController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'analysisSampling' ,
                Model::CONTROLLER     => 'diseaseAnalysisSamplingController',
                Model::EDGECONTROLLER => 'diseaseDiseasesAnalysisSamplingController',
                Model::SKIN           => 'list',
                Model::JOINS =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'medicalSamplingsController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'transmissionMethod' ,
                Model::CONTROLLER     => 'medicalTransmissionsMethodsController',
                Model::EDGECONTROLLER => 'diseaseTransmissionsMethodsController',
                Model::SKIN           => 'list'
            ]
        ];
    }

    return $edges ;
};
