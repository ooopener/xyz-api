<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' ) : array
{
    $joins =
    [
        [
            Model::NAME       => 'status' ,
            Model::CONTROLLER => 'placesStatusTypesController',
            Model::SKIN       => 'list'
        ]
    ];

    if( $skin == 'full' )
    {
        $joins =
        [
            ...$joins,
            require __MODELS__ . 'mediaObjects/audios.php',
            require __MODELS__ . 'mediaObjects/photos.php',
            require __MODELS__ . 'mediaObjects/videos.php'
        ];
    }

    return $joins ;
};
