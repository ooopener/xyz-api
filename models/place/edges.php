<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 * @return array
 */
return function( string $skin = '' ) : array
{
    $conceptualObjects = require __MODELS__ . 'conceptualObject/conceptualObject.php' ;
    $events            = require __MODELS__ . 'event/event.php' ;

    // default skin

    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'placesTypesController',
            Model::EDGECONTROLLER => 'placePlacesTypesController',
            Model::SKIN           => 'list'
        ],

        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/logo.php' ,
        require __MODELS__ . 'mediaObjects/video.php',

        $conceptualObjects // numConceptualObjects
        (
            'conceptualObjects' ,
            'list' ,
            'conceptualObjectPlacesController' ,
            'reverse' ,
            true
        ),

        $events // numEvents
        (
            'events' ,
            'list' ,
            'eventPlacesController' ,
            'reverse' ,
            true
        ),

        // activities

        [
            Model::NAME           => 'activities',
            Model::CONTROLLER     => 'activitiesController',
            Model::EDGECONTROLLER => 'placeActivitiesController',
            Model::SKIN           => 'list'
        ],

        // email
        [
            Model::NAME           => 'email',
            Model::CONTROLLER     => 'placeEmailsController',
            Model::EDGECONTROLLER => 'placePlacesEmailsController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'emailsTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],

        // offers
        [
            Model::NAME           => 'offers',
            Model::CONTROLLER     => 'placeOffersController',
            Model::EDGECONTROLLER => 'placePlacesOffersController',
            Model::SKIN           => 'list',
            Model::JOINS          =>
            [
                [
                    Model::NAME       => 'category',
                    Model::CONTROLLER => 'offersCategoriesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],

        // openingHoursSpecification
        [
            Model::NAME           => 'openingHoursSpecification',
            Model::CONTROLLER     => 'placeOpeningHoursController',
            Model::EDGECONTROLLER => 'placePlacesOpeningHoursController',
            Model::SKIN           => 'list'
        ],
        // permits
        [
            Model::NAME           => 'permits',
            Model::CONTROLLER     => 'placesRegulationsController',
            Model::EDGECONTROLLER => 'placePermitsController',
            Model::SKIN           => 'list'
        ],
        // prohibitions
        [
            Model::NAME           => 'prohibitions',
            Model::CONTROLLER     => 'placesRegulationsController',
            Model::EDGECONTROLLER => 'placeProhibitionsController',
            Model::SKIN           => 'list'
        ],
        // services
        [
            Model::NAME           => 'services',
            Model::CONTROLLER     => 'servicesController',
            Model::EDGECONTROLLER => 'placeServicesController',
            Model::SKIN           => 'list'
        ],
        // telephone
        [
            Model::NAME           => 'telephone',
            Model::CONTROLLER     => 'placePhoneNumbersController',
            Model::EDGECONTROLLER => 'placePlacesPhoneNumbersController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],
        // websites
        [
            Model::NAME           => 'websites',
            Model::CONTROLLER     => 'placeWebsitesController',
            Model::EDGECONTROLLER => 'placePlacesWebsitesController',
            Model::SKIN           => 'list',
            Model::JOINS          =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ];

    // full skin
    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            ( require 'place.php' )( 'containsPlace'    , 'list' , 'placeContainsPlacesController' ),
            ( require 'place.php' )( 'containedInPlace' , 'list' , 'placeContainsPlacesController' , 'reverse' ),
            [
                Model::NAME           => 'keywords',
                Model::CONTROLLER     => 'placeKeywordsController',
                Model::EDGECONTROLLER => 'placePlacesKeywordsController',
                Model::SKIN           => 'list'
            ] // keywords
        ];
    }

    return $edges ;
};
