<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\games\ApplicationGamesController;
use xyz\ooopener\controllers\games\CourseGamesController;
use xyz\ooopener\controllers\games\QuestionGamesController;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Games ;
use xyz\ooopener\models\Model ;

return
[
    'applicationsGames' => fn( ContainerInterface $container ) => new Games
    (
        $container ,
        'applications_games',
        [
            Model::FACETABLE =>
            [
                'id'  => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids' => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'game/application/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'game/application/joins.php')( 'full' )
        ]
    ),

    'applicationsGamesCoursesGames' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applicationsGames_coursesGames',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_games',
                Model::CONTROLLER => CourseGamesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications_games',
                Model::CONTROLLER => ApplicationGamesController::class
            ]
        ]
    ),

    'applicationsGamesGamesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applicationGames_gamesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'games_types',
                Model::CONTROLLER => 'gamesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications_games',
                Model::CONTROLLER => ApplicationGamesController::class
            ]
        ]
    ),

    'coursesGames' => fn( ContainerInterface $container ) => new Games
    (
        $container ,
        'courses_games',
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ]
            ]
            ,
            Model::SEARCHABLE =>
            [
                Model::NAME
            ]
            ,
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require 'game/course/edges.php')( 'full' ),
            Model::JOINS => (require 'game/course/joins.php')( 'full' )
        ]
    ),

    'coursesGamesGamesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courseGames_gamesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'games_types',
                Model::CONTROLLER => 'gamesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses_games',
                Model::CONTROLLER => CourseGamesController::class
            ]
        ]
    ),

    'coursesGamesQuestionsGames' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'coursesGames_questionsGames',
        [
            Model::FROM =>
            [
                Model::NAME       => 'questions_games',
                Model::CONTROLLER => QuestionGamesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses_games',
                Model::CONTROLLER => CourseGamesController::class
            ]
        ]
    ),

    'questionsGames' => fn( ContainerInterface $container ) => new Games
    (
        $container ,
        'questions_games',
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ]
            ]
            ,
            Model::SEARCHABLE =>
            [
                Model::NAME
            ]
            ,
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require 'game/question/edges.php')( 'full' ),
            Model::JOINS => (require 'game/question/joins.php')( 'full' )
        ]
    ),

    'questionGamesGamesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'questionGames_gamesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'games_types',
                Model::CONTROLLER => 'gamesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'questions_games',
                Model::CONTROLLER => QuestionGamesController::class
            ]
        ]
    )
];
