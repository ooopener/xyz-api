<?php

/**
 * The animal health models definitions.
 */
return ( require 'diseases.php'            )
     + ( require 'livestocks.php'          )
     + ( require 'medicalLaboratories.php' )
     + ( require 'observations.php'        )
     + ( require 'sectors.php'             )
     + ( require 'technicians.php'         )
     + ( require 'veterinarians.php'       )
     + ( require 'workplaces.php'          )
     + ( require 'workshops.php'           )
;
