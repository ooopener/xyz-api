<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\places\PlacesController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Emails ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Offers ;
use xyz\ooopener\models\OpeningHoursSpecifications ;
use xyz\ooopener\models\PhoneNumbers ;
use xyz\ooopener\models\Places ;
use xyz\ooopener\models\Photos ;
use xyz\ooopener\models\Websites ;

use xyz\ooopener\things\Thing ;

return
[
    Places::class => fn( ContainerInterface $container ) => new Places
    (
        $container ,
        'places' ,
        [
            Model::FACETABLE =>
            [
                'addressCountry'    => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'address.addressCountry'    ],
                'addressDepartment' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'address.addressDepartment' ],
                'addressLocality'   => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'address.addressLocality'   ],
                'addressRegion'     => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'address.addressRegion'     ],
                'id'                => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids'               => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'additionalType'    => [ Model::FACET_TYPE => Model::FACET_THESAURUS , Model::EDGE  => 'places_placesTypes' ],
                'services'          => [ Model::FACET_TYPE => Model::FACET_THESAURUS , Model::EDGE  => 'places_services'    ],
                'withStatus'        => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE => [ 'name' , 'addressCountry', 'addressDepartment' ],
            Model::SORTABLE =>
            [
                'id'                => '_key',
                'addressCountry'    => 'address.addressCountry',
                'addressDepartment' => 'address.addressDepartment',
                'addressLocality'   => 'address.addressLocality',
                'addressRegion'     => 'address.addressRegion',
                'postalCode'        => 'address.postalCode',
                'streetAddress'     => 'address.streetAddress',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'place/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'place/joins.php')( 'full' )
        ]
    ),

    'placeActivities' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_activities',
        [
            Model::FROM =>
            [
                Model::NAME       => 'activities',
                Model::CONTROLLER => 'activitiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placeEmails' => fn( ContainerInterface $container ) => new Emails
    (
        $container ,
        'places_emails',
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'emailsTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'placeHasBrand' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'hasBrand',
        [
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::FILLABLE =>
            [
                'type' => Thing::FILTER_DEFAULT
            ]
        ]
    ),

    'placeKeywords' => fn( ContainerInterface $container )
                    => new Keywords( $container , 'places_keywords' ) ,

    'placeOffers' => fn( ContainerInterface $container ) => new Offers
    (
        $container ,
        'places_offers' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'category',
                    Model::CONTROLLER => 'offersCategoriesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'placeOpeningHours' => fn( ContainerInterface $container ) => new OpeningHoursSpecifications
    (
        $container ,
        'places_opening_hours'
    ),

    'placePermits' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_permits',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_regulations',
                Model::CONTROLLER => 'placesRegulationsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePhoneNumbers' => fn( ContainerInterface $container ) => new PhoneNumbers
    (
        $container ,
        'places_phoneNumbers',
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'placePhotos' => fn( ContainerInterface $container ) => new Photos
    (
        $container ,
        'places_photos' ,
        [
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ),

    'placeContainsPlaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_subPlaces' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesEmails' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesEmails',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_emails',
                Model::CONTROLLER => 'placeEmailsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesKeywords' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesKeywords',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_keywords',
                Model::CONTROLLER => 'placeKeywordsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesOffers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesOffers',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_offers',
                Model::CONTROLLER => 'placeOffersController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesOpeningHours' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesOpeningHours',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_openingHours',
                Model::CONTROLLER => 'placeOpeningHoursController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesPhoneNumbers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesPhoneNumbers',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_phoneNumbers',
                Model::CONTROLLER => 'placePhoneNumbersController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_types',
                Model::CONTROLLER => 'placesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placePlacesWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_placesWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_websites',
                Model::CONTROLLER => 'placeWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placeProhibitions' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_prohibitions',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places_regulations',
                Model::CONTROLLER => 'placesRegulationsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placeServices' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'places_services',
        [
            Model::FROM =>
            [
                Model::NAME       => 'services',
                Model::CONTROLLER => 'servicesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ]
        ]
    ),

    'placeWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'places_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    )
];
