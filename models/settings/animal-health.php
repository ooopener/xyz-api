<?php

use xyz\ooopener\controllers\diseases\DiseasesController ;

use xyz\ooopener\models\Model ;

use xyz\ooopener\models\Diseases ;

$create = require( 'create.php' ) ;

return
[
    'breedingsTypesMedicalCauses' => $create
    (
        'breedingsTypes_medicalCauses' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_causes',
                Model::CONTROLLER => 'medicalCausesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesMorbidityRates' => $create
    (
        'breedingsTypes_morbidityRates' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'morbidity_rates',
                Model::CONTROLLER => 'morbidityRatesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesMortalityRates' => $create
    (
        'breedingsTypes_mortalityRates' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'mortality_rates',
                Model::CONTROLLER => 'mortalityRatesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ) ,

    'breedingsTypesProductionsTypes' => $create
    (
        'breedingsTypes_productionsTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'productions_types',
                Model::CONTROLLER => 'productionsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesObservationsTypes' => $create
    (
        'breedingsTypes_observationsTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesRiskFactors' => $create
    (
        'breedingsTypes_riskFactors' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'riskFactors',
                Model::CONTROLLER => 'riskFactorsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesSectorsTypes' => $create
    (
        'breedingsTypes_sectorsTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesSpecies' => $create
    (
        'breedingsTypes_species' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'taxonomy_species',
                Model::CONTROLLER => 'taxonomySpeciesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesVaccines' => $create
    (
        'breedingsTypes_vaccines' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'vaccines',
                Model::CONTROLLER => 'vaccinesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'breedingsTypesWorkplacesTypes' => $create
    (
        'breedingsTypes_workplacesTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'workplaces_types',
                Model::CONTROLLER => 'workplacesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ]
        ]
    ),

    'observationsTypesAgeGroups' => $create
    (
        'observationsTypes_ageGroups' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'ageGroups',
                Model::CONTROLLER => 'ageGroupsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ),

    'observationsTypesDiseases' => $create
    (
        'observationsTypes_diseases' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class,
                Model::MODEL      => Diseases::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ),

    'observationsTypesDiseasesMandatory' => $create
    (
        'observationsTypes_diseasesMandatory' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'diseases',
                Model::CONTROLLER => DiseasesController::class,
                Model::MODEL      => Diseases::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ),

    'observationsTypesMedicalCauses' => $create
    (
        'observationsTypes_medicalCauses' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_causes',
                Model::CONTROLLER => 'medicalCausesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ),

    'observationsTypesMedicalConclusions' => $create
    (
        'observationsTypes_medicalConclusions' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_conclusions',
                Model::CONTROLLER => 'medicalConclusionsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ) ,

    'observationsTypesMedicalFrequencies' => $create
    (
        'observationsTypes_medicalFrequencies' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'medical_frequencies',
                Model::CONTROLLER => 'medicalFrequenciesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ) ,

    'observationsTypesRiskFactors' => $create
    (
        'observationsTypes_riskFactors' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'riskFactors',
                Model::CONTROLLER => 'riskFactorsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'observations_types',
                Model::CONTROLLER => 'observationsTypesController'
            ]
        ]
    ),

    'sectorsTypesAgeGroups' => $create
    (
        'sectorsTypes_ageGroups' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'ageGroups',
                Model::CONTROLLER => 'ageGroupsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ]
        ]
    ),

    'sectorsTypesAmbiences' => $create
    (
        'sectorsTypes_ambiences' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'ambiences',
                Model::CONTROLLER => 'ambiencesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ]
        ]
    ) ,

    'sectorsTypesPreventions' => $create
    (
        'sectorsTypes_preventions' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'preventions',
                Model::CONTROLLER => 'preventionsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ]
        ]
    ) ,

    'sectorsTypesSymptoms' => $create
    (
        'sectorsTypes_symptoms' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'symptoms',
                Model::CONTROLLER => 'symptomsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'sectors_types',
                Model::CONTROLLER => 'sectorsTypesController'
            ]
        ]
    )
];
