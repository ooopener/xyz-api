<?php

use DI\Container ;

use xyz\ooopener\models\Settings ;

return fn( string $table = NULL , array $init = [] )
    => fn( Container $container )
    => new Settings( $container , $table , $init ) ;
