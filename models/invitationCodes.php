<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\InvitationCodes ;

return
[
    InvitationCodes::class => fn( ContainerInterface $container )
                           => new InvitationCodes( $container , "invitation_codes" )
];

