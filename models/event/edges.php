<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    $edges =
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'eventsTypesController',
            Model::EDGECONTROLLER => 'eventEventsTypesController',
            Model::SKIN           => 'list'
        ],

        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php' ,

        ( require __MODELS__ . 'place/place.php' )( 'location' , 'list' , 'eventPlacesController' ),

        [
            Model::NAME           => 'offers',
            Model::CONTROLLER     => 'eventOffersController',
            Model::EDGECONTROLLER => 'eventEventsOffersController',
            Model::SKIN           => 'list',
            Model::JOINS          =>
            [
                [
                    Model::NAME       => 'category',
                    Model::CONTROLLER => 'offersCategoriesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],

    ];

    // extend skin
    if( $skin == 'extend' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'eventWebsitesController',
                Model::EDGECONTROLLER => 'eventEventsWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    // full skin
    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'keywords',
                Model::CONTROLLER     => 'eventKeywordsController',
                Model::EDGECONTROLLER => 'eventEventsKeywordsController',
                Model::SKIN           => 'list'
            ],
            ( require 'event.php' ) ( 'subEvent'   , '' , 'eventSubEventsController' ),
            ( require 'event.php' ) ( 'superEvent' , '' , 'eventSubEventsController' , 'reverse' ),
            ( require __MODELS__ . 'conceptualObject/conceptualObject.php')( 'workFeatured' , '' , 'eventWorksFeaturedController' )
        ];
    }

    return $edges ;
};
