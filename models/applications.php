<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\users\UsersController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Applications ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Websites ;

return
[
    Applications::class => function( ContainerInterface $container )
    {
        return new Applications
        (
            $container ,
            "applications" ,
            [
                Model::FACETABLE  =>
                [
                    'withStatus' =>
                    [
                        Model::FACET_TYPE     => Model::FACET_FIELD ,
                        Model::FACET_PROPERTY => 'withStatus'
                    ]
                ],
                Model::SEARCHABLE => [ 'name' ],
                Model::SORTABLE   =>
                [
                    'id'       => '_key',
                    'name'     => 'name',
                    'created'  => 'created',
                    'modified' => 'modified'
                ],
                Model::EDGES => ( require __MODELS__ . 'application/edges.php' )( 'full' ) ,
                Model::JOINS => ( require __MODELS__ . 'application/joins.php' )( 'full' )
            ]
        ) ;
    },

    'applicationApplicationsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_applicationsTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'applications_types',
                Model::CONTROLLER => 'applicationsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationProducerOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_producer_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationProducerPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_producer_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationPublisherOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_publisher_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationPublisherPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_publisher_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationSponsorOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_sponsor_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationSponsorPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_sponsor_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationUsers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_users',
        [
            Model::FROM =>
            [
                Model::NAME       => 'users',
                Model::CONTROLLER => UsersController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    ),

    'applicationWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'applications_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'applicationApplicationsWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'applications_applicationsWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'applications_websites',
                Model::CONTROLLER => 'applicationWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'applications',
                Model::CONTROLLER => Applications::class
            ]
        ]
    )
];
