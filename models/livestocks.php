<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\livestocks\LivestockNumbersController;
use xyz\ooopener\controllers\livestocks\LivestocksController ;
use xyz\ooopener\controllers\livestocks\WorkshopsController;
use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\technicians\TechniciansController ;
use xyz\ooopener\controllers\veterinarians\VeterinariansController;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Livestocks ;
use xyz\ooopener\models\LivestockNumbers ;
use xyz\ooopener\models\Model ;

return
[
    Livestocks::class => fn( ContainerInterface $container ) => new Livestocks
    (
        $container ,
        'livestocks' ,
        [
            Model::FACETABLE =>
            [
                'id'      => [ Model::FACET_TYPE => Model::FACET_FIELD      ] ,
                'ids'     => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ] ,
                'numbers' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE_COMPLEX ,
                    Model::EDGE       => 'livestocks_livestocksNumbers'
                ],
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ],
                'workshops'  =>
                [
                    Model::FACET_TYPE => Model::FACET_ARRAY_COMPLEX
                ]
            ],
            Model::SEARCHABLE => [ 'organization.name' ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'organization.name',
                'created'   => 'created',
                'modified'  => 'modified',
                //'after'     => 'organization.name'
            ],
            Model::EDGES => (require __MODELS__ . 'livestock/edges.php')( 'full' ) ,
            Model::JOINS => (require __MODELS__ . 'livestock/joins.php')( 'full' )
        ]
    ),

    LivestockNumbers::class => fn( ContainerInterface $container ) => new LivestockNumbers
    (
        $container ,
        'livestocks_numbers' ,
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE   => Model::FACET_FIELD
                ]
            ],
            Model::SEARCHABLE =>
            [
                Model::NAME
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'livestocksNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'livestockMedicalLaboratories' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_medicalLaboratories',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medicalLaboratories' ,
                Model::CONTROLLER => MedicalLaboratoriesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks' ,
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    ),

    'livestockOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations' ,
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks' ,
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    ),

    'livestockTechnicians' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_technicians',
        [
            Model::FROM =>
            [
                Model::NAME       => 'technicians' ,
                Model::CONTROLLER => TechniciansController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks' ,
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    ),

    'livestockVeterinarians' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_veterinarians',
        [
            Model::FROM =>
            [
                Model::NAME       => 'veterinarians' ,
                Model::CONTROLLER => VeterinariansController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks' ,
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    ),

    'livestocksLivestocksNumbers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_livestocksNumbers' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'livestock_numbers',
                Model::CONTROLLER => LivestockNumbersController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks',
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    ),

    'livestocksWorkshops' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'livestocks_workshops' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'livestocks',
                Model::CONTROLLER => LivestocksController::class
            ]
        ]
    )
];
