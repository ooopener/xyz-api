<?php

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $joins = [] ;

    if( $skin == 'full' )
    {
        $joins =
        [
            ...$joins,
            (require __MODELS__ . 'application/application.php' )( 'app' , 'list' )
        ];
    }

    return $joins ;
};
