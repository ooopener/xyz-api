<?php

use xyz\ooopener\controllers\veterinarians\VeterinariansController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param string|null $edge
 * @param string|null $direction
 * @param bool $lock
 *
 * @return array
 */
return fn
(
    string $property  = 'veterinarian' ,
    string $skin      = '' ,
    string $edge      = null ,
    string $direction = null ,
    bool   $lock      = false
):array
=>
[
    Model::NAME       => $property,
    Model::CONTROLLER => VeterinariansController::class,
    Model::SKIN       => $skin,
    Model::EDGES      => $lock ? NULL : ( require 'edges.php' )( $skin ) ,
    Model::JOINS      => $lock ? NULL : ( require 'joins.php' )( $skin ) ,

    !( $edge )      ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION      => $direction
];

