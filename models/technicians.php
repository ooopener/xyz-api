<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\technicians\TechniciansController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Technicians ;

return
[
    Technicians::class => fn( ContainerInterface $container ) => new Technicians
    (
        $container,
        'technicians',
        [
            Model::FACETABLE =>
            [
                'id'         => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids'        => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE =>
            [
                'name'
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'person.name',
                'created'   => 'created',
                'modified'  => 'modified',
                'after'     => 'person.name'
            ],
            Model::EDGES => ( require __MODELS__ . 'technician/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'technician/joins.php' )( 'full' )
        ]
    ),

    'technicianMedicalSpecialties' => fn( ContainerInterface $container ) =>new Edges
    (
        $container ,
        'technicians_medicalSpecialties',
        [
            Model::FROM =>
            [
                Model::NAME       => 'medicalSpecialties',
                Model::CONTROLLER => 'medicalSpecialtiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'technicians',
                Model::CONTROLLER => TechniciansController::class
            ]
        ]
    )
];
