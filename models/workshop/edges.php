<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    return
    [
        [
            Model::NAME           => 'breeding',
            Model::CONTROLLER     => 'breedingsTypesController',
            Model::EDGECONTROLLER => 'workshopsBreedingsTypesController',
            Model::SKIN           => 'full'
        ],
        [
            Model::NAME           => 'production',
            Model::CONTROLLER     => 'productionsTypesController',
            Model::EDGECONTROLLER => 'workshopsProductionsTypesController',
            Model::SKIN           => 'full'
        ],
        [
            Model::NAME           => 'water',
            Model::CONTROLLER     => 'waterOriginsController',
            Model::EDGECONTROLLER => 'workshopsWaterOriginsController',
            Model::SKIN           => 'full'
        ],
        (require __MODELS__ . 'workplace/workplace.php')( 'workplaces' , 'list' , 'workshopsWorkplacesController' )
    ] ;
};
