<?php

$create = require( 'create.php' ) ;

return
[
    'activities'                    => $create( 'activities' ) ,
    'answers'                       => $create( 'answers' )  ,
    'applicationsTypes'             => $create( 'applications_types' )  ,
    'artMovements'                  => $create( 'art_movements' )  ,
    'artTemporalEntities'           => $create( 'art_temporal_entities' )  ,
    'articlesTypes'                 => $create( 'articles_types' ) ,
    'brandsTypes'                   => $create( 'brands_types' ) ,
    'businessEntityTypes'           => $create( 'business_entity_types' ) ,
    'conceptualObjectsCategories'   => $create( 'conceptualObjects_categories' ) ,
    'conceptualObjectsNumbersTypes' => $create( 'conceptualObjects_numbers_types' ) ,
    'conceptualObjectsTechniques'   => $create( 'conceptualObjects_techniques' ) ,
    'emailsTypes'                   => $create( 'emails_types' ) ,
    'eventsStatusTypes'             => $create( 'events_status_types' ) ,
    'eventsTypes'                   => $create( 'events_types' ) ,
    'genders'                       => $create( 'genders' ),
    'honorificPrefix'               => $create( 'honorificPrefix' ) ,
    'jobs'                          => $create( 'jobs' ) ,
    'languages'                     => $create( 'languages' ) ,
    'marksTypes'                    => $create( 'marks_types' ) ,
    'materials'                     => $create( 'materials' ) ,
    'measurementsDimensions'        => $create( 'measurements_dimensions' ) ,
    'measurementsUnits'             => $create( 'measurements_units' ) ,
    'offersCategories'              => $create( 'offers_categories' ) ,
    'organizationsNaf'              => $create( 'organizations_naf' ) ,
    'organizationsNumbersTypes'     => $create( 'organizations_numbers_types' ) ,
    'organizationsTypes'            => $create( 'organizations_types' ) ,
    'phoneNumbersTypes'             => $create( 'phoneNumbers_types' ) ,
    'placesRegulations'             => $create( 'places_regulations' ) ,
    'placesStatusTypes'             => $create( 'places_status_types' ) ,
    'placesTypes'                   => $create( 'places_types' ) ,
    'services'                      => $create( 'services' ) ,
    'thesaurusTypes'                => $create( 'thesaurus_types' ) ,
    'websitesTypes'                 => $create( 'websites_types' )
];
