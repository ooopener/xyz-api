<?php

$create = require( 'create.php' ) ;

return
[
    'ageGroups'                          => $create( 'ageGroups'                            ) ,
    'ambiences'                          => $create( 'ambiences'                            ) ,
    'analysisMethods'                    => $create( 'analysis_methods'                     ) ,
    'analysisResultStatusTypes'          => $create( 'analysis_result_status_types'         ) ,
    'breedingsTypes'                     => $create( 'breedings_types'                      ) ,
    'diseasesLevels'                     => $create( 'diseases_levels'                      ) ,
    'diseasesTypes'                      => $create( 'diseases_types'                       ) ,
    'livestocksNumbersTypes'             => $create( 'livestocks_numbers_types'             ) ,
    'medicalAutopsyStatus'               => $create( 'medical_autopsy_status'               ) ,
    'medicalBodySystems'                 => $create( 'medical_body_systems'                 ) ,
    'medicalCauses'                      => $create( 'medical_causes'                       ) ,
    'medicalConclusions'                 => $create( 'medical_conclusions'                  ) ,
    'medicalFrequencies'                 => $create( 'medical_frequencies'                  ) ,
    'medicalPriorities'                  => $create( 'medical_priorities'                   ) ,
    'medicalSamplings'                   => $create( 'medical_samplings'                    ) ,
    'medicalSpecialties'                 => $create( 'medical_specialties'                  ) ,
    'medicalTransmissionsMethods'        => $create( 'medical_transmissions_methods'        ) ,
    'medicalLaboratoriesIdentifierTypes' => $create( 'medicalLaboratories_identifier_types' ) ,
    'morbidityRates'                     => $create( 'morbidity_rates'                      ) ,
    'mortalityRates'                     => $create( 'mortality_rates'                      ) ,
    'observationsCauses'                 => $create( 'observations_causes'                  ) ,
    'observationsStatus'                 => $create( 'observations_status'                  ) ,
    'observationsTypes'                  => $create( 'observations_types'                   ) ,
    'preventions'                        => $create( 'preventions'                          ) ,
    'productionsTypes'                   => $create( 'productions_types'                    ) ,
    'riskFactors'                        => $create( 'riskFactors'                          ) ,
    'sectorsTypes'                       => $create( 'sectors_types'                        ) ,
    'symptoms'                           => $create( 'symptoms'                             ) ,
    'taxonomyBreeds'                     => $create( 'taxonomy_breeds'                      ) ,
    'taxonomySpecies'                    => $create( 'taxonomy_species'                     ) ,
    'vaccines'                           => $create( 'vaccines'                             ) ,
    'waterOrigins'                       => $create( 'water_origins'                        ) ,
    'workplacesTypes'                    => $create( 'workplaces_types'                     )
];
