<?php

$create = require( 'create.php' ) ;

return
[

    'badgeItemsTypes'  => $create( 'badgeItems_types' ) ,
    'coursesAudiences' => $create( 'courses_audiences' ) ,
    'coursesLevels'    => $create( 'courses_levels' ) ,
    'coursesPaths'     => $create( 'courses_paths' ) ,
    'coursesStatus'    => $create( 'courses_status' ) ,
    'coursesTypes'     => $create( 'courses_types' ) ,
    'gamesTypes'       => $create( 'games_types' ) ,
    'questionsTypes'   => $create( 'questions_types' ) ,
    'transportations'  => $create( 'transportations' )
];
