<?php

use DI\Container ;

use xyz\ooopener\models\Thesaurus ;

return fn( $table ) => fn( Container $container )
                    => new Thesaurus( $container , $table ) ;
