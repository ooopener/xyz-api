<?php

use xyz\ooopener\models\Model ;

return
[
    Model::NAME       => 'gender',
    Model::CONTROLLER => 'gendersController',
    Model::SKIN       => 'normal'
];
