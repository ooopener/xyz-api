<?php

/**
 * The commons application's controllers.
 */
return
    // ------------------------------------
       ( require 'authorizationCodes.php' )
     + ( require 'activityLogs.php'       )
     + ( require 'invitationCodes.php'    )
     + ( require 'usersAuthApps.php'      )
     // -----------------------------------
     + ( require 'applications.php'       )
     + ( require 'articles.php'           )
     + ( require 'brands.php'             )
     + ( require 'conceptualObjects.php'  )
     + ( require 'mediaObjects.php'       )
     + ( require 'events.php'             )
     + ( require 'organizations.php'      )
     + ( require 'people.php'             )
     + ( require 'places.php'             )
     + ( require 'sessions.php'           )
     + ( require 'teams.php'              )
     + ( require 'things.php'             )
     + ( require 'users.php'              )
     + ( require 'thesaurusCollector.php' )
     + ( require 'settings.php'           )
    // -----------------------------------
;
