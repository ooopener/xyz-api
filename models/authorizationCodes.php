<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\AuthorizationCodes ;

return
[
    AuthorizationCodes::class => fn( ContainerInterface $container )
                              => new AuthorizationCodes( $container , "authorization_codes" )
];

