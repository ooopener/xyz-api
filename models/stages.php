<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\articles\ArticlesController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use xyz\ooopener\controllers\courses\CoursesController ;
use xyz\ooopener\controllers\events\EventsController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;
use xyz\ooopener\controllers\stages\StagesController ;

use xyz\ooopener\models\Stages ;
use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Websites ;

return
[
    Stages::class => fn( ContainerInterface $container ) => new Stages
    (
        $container ,
        'stages' ,
        [
            Model::FACETABLE =>
            [
                'id'  => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids' => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE   =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'created'  => 'created',
                'modified' => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'stage/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'stage/joins.php')( 'full' )
        ]
    ),

    'stageLocations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'stages_locations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ]
        ]
    ),

    'stageCoursesStatus' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'stages_coursesStatus',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_status',
                Model::CONTROLLER => 'coursesStatusController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ]
        ]
    ),

    'stageActivities' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'stages_activities',
        [
            Model::FROM =>
            [
                Model::NAME       => 'activities',
                Model::CONTROLLER => 'activitiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ]
        ]
    ),

    'stageKeywords' => fn( ContainerInterface $container ) => new Keywords( $container , 'stages_keywords' ) ,

    'stageStagesKeywords' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'stages_stagesKeywords',
        [
            Model::FROM =>
            [
                Model::NAME       => 'stages_keywords',
                Model::CONTROLLER => 'stageKeywordsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ]
        ]
    ),

    'stageWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'stages_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'stageStagesWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'stages_stagesWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'stages_websites',
                Model::CONTROLLER => 'stageWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'stages',
                Model::CONTROLLER => StagesController::class
            ]
        ]
    )
];
