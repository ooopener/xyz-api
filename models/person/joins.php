<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $joins =
    [
        require __MODELS__ . 'thesaurus/gender.php' ,
        [
            Model::NAME       => 'honorificPrefix',
            Model::CONTROLLER => 'honorificPrefixController',
            Model::SKIN       => 'normal'
        ]
    ];

    // skin full
    if( $skin == 'full' )
    {
        $joins =
        [
            ...$joins,
            require __MODELS__ . 'mediaObjects/audios.php',
            require __MODELS__ . 'mediaObjects/photos.php',
            require __MODELS__ . 'mediaObjects/videos.php'
        ];
    }

    return $joins ;
};
