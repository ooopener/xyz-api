<?php

use xyz\ooopener\controllers\people\PeopleController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param string|null $edge
 * @param string|null $direction
 * @param bool $lock
 *
 * @return array
 */
return fn( string $property = 'person' , string $skin = '' , string $edge = null , string $direction = null , bool $lock = false ) : array =>
[
    Model::NAME       => $property ,
    Model::CONTROLLER => PeopleController::class ,
    Model::SKIN       => $skin ,
    Model::EDGES      => $lock ? NULL : (require __MODELS__ . 'person/edges.php')( $skin ) ,
    Model::JOINS      => $lock ? NULL : (require __MODELS__ . 'person/joins.php')( $skin ) ,
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION => $direction
];

