<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    $edges =
    [
        require __MODELS__ . 'mediaObjects/audio.php' ,
        [
            Model::NAME           => 'email',
            Model::CONTROLLER     => 'peopleEmailsController',
            Model::EDGECONTROLLER => 'peoplePeopleEmailsController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'emailsTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],
        require __MODELS__ . 'mediaObjects/image.php' ,
        [
            Model::NAME           => 'telephone',
            Model::CONTROLLER     => 'peoplePhoneNumbersController',
            Model::EDGECONTROLLER => 'peoplePeoplePhoneNumbersController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],
        [
            Model::NAME           => 'telephone',
            Model::CONTROLLER     => 'peoplePhoneNumbersController',
            Model::EDGECONTROLLER => 'peoplePeoplePhoneNumbersController',
            Model::SKIN           => 'normal',
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'phoneNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ],
        require __MODELS__ . 'mediaObjects/video.php'
    ];

    // skin list
    if( $skin == 'list' || $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'job',
                Model::CONTROLLER     => 'jobsController',
                Model::EDGECONTROLLER => 'peopleJobsController',
                Model::SKIN           => 'normal'
            ],
            [
                Model::NAME           => 'keywords',
                Model::CONTROLLER     => 'peopleKeywordsController',
                Model::EDGECONTROLLER => 'peoplePeopleKeywordsController',
                Model::SKIN           => 'list'
            ]
        ];
    }

    // skin compact
    if( $skin == 'compact' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'peopleWebsitesController',
                Model::EDGECONTROLLER => 'peoplePeopleWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            (require __MODELS__ . 'conceptualObject/conceptualObject.php')( 'owns' , '' , 'conceptualObjectProductionsPeopleController' , 'reverse' ),
            (require __MODELS__ . 'organization/organization.php')( 'memberOf' , 'list' , 'organizationMembersPeopleController' , 'reverse' ),
            (require __MODELS__ . 'organization/organization.php')( 'worksFor' , 'list' , 'organizationEmployeesController' , 'reverse' )
        ];
    }

    // skin full
    if( $skin == 'full' )
    {}

    return $edges ;
};
