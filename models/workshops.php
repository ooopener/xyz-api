<?php

use xyz\ooopener\controllers\livestocks\WorkplacesController;
use xyz\ooopener\controllers\livestocks\WorkshopsController;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Workshops ;

return
[
    Workshops::class => fn( ContainerInterface $container ) => new Workshops
    (
        $container ,
        "workshops",
        [
            Model::FACETABLE =>
            [
                'id'         => [ Model::FACET_TYPE => Model::FACET_FIELD ],
                'breeding'   => [ Model::FACET_TYPE => Model::FACET_THESAURUS , Model::EDGE  => 'workshops_breedingsTypes'   ] ,
                'production' => [ Model::FACET_TYPE => Model::FACET_THESAURUS , Model::EDGE  => 'workshops_productionsTypes' ] ,
                'water'      => [ Model::FACET_TYPE => Model::FACET_THESAURUS , Model::EDGE  => 'workshops_waterOrigins'     ] ,
                'withStatus' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'withStatus' ]
            ],
            Model::SEARCHABLE =>
            [
                'name'
            ],
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'workshop/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'workshop/joins.php')( 'full' )
        ]
    ),

    'workshopsBreedingsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'workshops_breedingsTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'breedings_types',
                Model::CONTROLLER => 'breedingsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ]
        ]
    ),

    'workshopsProductionsTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'workshops_productionsTypes' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'productions_types',
                Model::CONTROLLER => 'productionsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ]
        ]
    ),

    'workshopsWaterOrigins' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'workshops_waterOrigins' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'water_origins',
                Model::CONTROLLER => 'waterOriginsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ]
        ]
    ),

    'workshopsWorkplaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'workshops_workplaces' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'workplaces',
                Model::CONTROLLER => WorkplacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'workshops',
                Model::CONTROLLER => WorkshopsController::class
            ]
        ]
    ),
];
