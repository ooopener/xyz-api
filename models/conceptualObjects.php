<?php

use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMaterialsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMeasurementsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectNumbersController;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMarksController;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\places\PlacesController ;

use xyz\ooopener\models\conceptualObjects\ConceptualObjectMarks ;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectMaterials ;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectMeasurements ;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectNumbers ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Keywords ;
use xyz\ooopener\models\ConceptualObjects ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Photos ;
use xyz\ooopener\models\Websites ;

use xyz\ooopener\things\Thing ;

return
[
    ConceptualObjects::class => fn( ContainerInterface $container ) => new ConceptualObjects
    (
        $container ,
        'conceptualObjects',
        [
            Model::FACETABLE =>
            [
                'id'       => [ Model::FACET_TYPE => Model::FACET_FIELD      ],
                'ids'      => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'location' =>
                [
                    Model::FACET_TYPE => Model::FACET_EDGE ,
                    Model::EDGE       => 'conceptualObjects_places'
                ]
                ,
                'category' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS ,
                    Model::EDGE       => 'conceptualObjects_conceptualObjectsCategories'
                ]
                ,
                'movement' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS ,
                    Model::EDGE       => 'conceptualObjects_artMovements'
                ]
                ,
                'temporal' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS ,
                    Model::EDGE       => 'conceptualObjects_artTemporalEntities'
                ],
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ]
            ,
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE =>
            [
                'id'       => '_key',
                'active'   => 'active',
                'name'     => 'name',
                'created'  => 'created',
                'modified' => 'modified'
            ],
            Model::EDGES => (require __MODELS__ . 'conceptualObject/edges.php')( 'full' ),
            Model::JOINS => (require __MODELS__ . 'conceptualObject/joins.php')( 'full' )
        ]

    ),

    ConceptualObjectMarks::class => fn( ContainerInterface $container ) => new ConceptualObjectMarks
    (
        $container ,
        'conceptualObjects_marks' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType' ,
                    Model::CONTROLLER => 'marksTypesController',
                    Model::SKIN       => 'list'
                ],
                [
                    Model::NAME       => 'language' ,
                    Model::CONTROLLER => 'languagesController',
                    Model::SKIN       => 'list'
                ],
                [
                    Model::NAME       => 'technique',
                    Model::CONTROLLER => 'conceptualObjectsTechniquesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    ConceptualObjectMaterials::class => fn( ContainerInterface $container ) => new ConceptualObjectMaterials
    (
        $container ,
        'conceptualObjects_materials' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'material' ,
                    Model::CONTROLLER => 'materialsController',
                    Model::SKIN       => 'list'
                ],
                [
                    Model::NAME       => 'technique',
                    Model::CONTROLLER => 'conceptualObjectsTechniquesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    ConceptualObjectMeasurements::class => fn( ContainerInterface $container ) => new ConceptualObjectMeasurements
    (
        $container ,
        'conceptualObjects_measurements' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'dimension' ,
                    Model::CONTROLLER => 'measurementsDimensionsController',
                    Model::SKIN       => 'list'
                ],
                [
                    Model::NAME       => 'unit',
                    Model::CONTROLLER => 'measurementsUnitsController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    ConceptualObjectNumbers::class => fn( ContainerInterface $container ) => new ConceptualObjectNumbers
    (
        $container ,
        'conceptualObjects_numbers' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType' ,
                    Model::CONTROLLER => 'conceptualObjectsNumbersTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'conceptualObjectArtMovements' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_artMovements',
        [
            Model::FROM =>
            [
                Model::NAME       => 'art_movements',
                Model::CONTROLLER => 'artMovementsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectArtTemporalEntities' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_artTemporalEntities',
        [
            Model::FROM =>
            [
                Model::NAME       => 'art_temporal_entities',
                Model::CONTROLLER => 'artTemporalEntitiesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),


    'conceptualObjectHasBrand' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'hasBrand',
        [
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ],
            Model::FILLABLE =>
            [
                'type' => Thing::FILTER_DEFAULT
            ]
        ]
    ),

    'conceptualObjectKeywords' => fn( ContainerInterface $container ) => new Keywords
    (
        $container ,
        'conceptualObjects_keywords'
    ),

    'conceptualObjectConceptualObjectsKeywords' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsKeywords',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_keywords',
                Model::CONTROLLER => 'conceptualObjectKeywordsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsCategories' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsCategories',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_categories',
                Model::CONTROLLER => 'conceptualObjectsCategoriesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsMarks' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMarks',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_marks',
                Model::CONTROLLER => ConceptualObjectMarksController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsMaterials' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMaterials',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_materials',
                Model::CONTROLLER => ConceptualObjectMaterialsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsMeasurements' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMeasurements',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_measurements',
                Model::CONTROLLER => ConceptualObjectMeasurementsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsNumbers' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsNumbers',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_numbers',
                Model::CONTROLLER => ConceptualObjectNumbersController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),


    'conceptualObjectPlaces' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_places',
        [
            Model::FROM =>
            [
                Model::NAME       => 'places',
                Model::CONTROLLER => PlacesController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectProductionsOrganizations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_productions_organizations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'organizations',
                Model::CONTROLLER => OrganizationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectProductionsPeople' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_productions_people',
        [
            Model::FROM =>
            [
                Model::NAME       => 'people',
                Model::CONTROLLER => PeopleController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    ),

    'conceptualObjectPhotos' => fn( ContainerInterface $container ) => new Photos
    (
        $container ,
        'conceptualObjects_photos' ,
        [
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ),

    'conceptualObjectWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'conceptualObjects_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'conceptualObjects_websites',
                Model::CONTROLLER => 'conceptualObjectWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'conceptualObjects',
                Model::CONTROLLER => ConceptualObjectsController::class
            ]
        ]
    )
];
