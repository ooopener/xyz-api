<?php

use xyz\ooopener\controllers\courses\CourseTransportationsController;
use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\courses\CoursesController ;
use xyz\ooopener\controllers\steps\StepsController ;

use xyz\ooopener\models\Courses ;
use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\OpeningHoursSpecifications ;
use xyz\ooopener\models\Transportations ;

return
[
    Courses::class => fn( ContainerInterface $container ) => new Courses
    (
        $container ,
        "courses",
        [
            Model::FACETABLE =>
            [
                'id' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD
                ],
                'ids' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD
                ],
                'idsSorted' =>
                [
                    Model::FACET_TYPE => Model::FACET_LIST_FIELD_SORTED
                ],
                'additionalType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS ,
                    Model::EDGE       => 'courses_coursesTypes'
                ],
                'withStatus' =>
                [
                    Model::FACET_TYPE => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ]
            ,
            Model::SEARCHABLE => [ 'name' ] ,
            Model::SORTABLE =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'course/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'course/joins.php' )( 'full' )
        ]
    ),

    'courseCoursesAudiences' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesAudiences',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_audiences',
                Model::CONTROLLER => 'coursesAudiencesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesLevels' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesLevels',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_levels',
                Model::CONTROLLER => 'coursesLevelsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesOpeningHours' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesOpeningHours',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_openingHours',
                Model::CONTROLLER => 'courseOpeningHoursController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesPaths' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesPaths',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_paths',
                Model::CONTROLLER => 'coursesPathsController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesStatus' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesStatus',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_status',
                Model::CONTROLLER => 'coursesStatusController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesTransportations' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesTransportations',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_transportations',
                Model::CONTROLLER => CourseTransportationsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseCoursesTypes' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesTypes',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_types',
                Model::CONTROLLER => 'coursesTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'coursePlacesOpeningHours' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_coursesOpeningHours',
        [
            Model::FROM =>
            [
                Model::NAME       => 'courses_openingHours',
                Model::CONTROLLER => 'courseOpeningHoursController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseOpeningHours' => fn( ContainerInterface $container ) => new OpeningHoursSpecifications
    (
        $container ,
        'courses_opening_hours'
    ),

    'courseSteps' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'courses_steps',
        [
            Model::FROM =>
            [
                Model::NAME       => 'steps',
                Model::CONTROLLER => StepsController::class
            ],
            Model::TO =>
            [
                Model::NAME       => 'courses',
                Model::CONTROLLER => CoursesController::class
            ]
        ]
    ),

    'courseTransportations' => fn( ContainerInterface $container ) => new Transportations
    (
        $container ,
        'courses_transportations',
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'transportationsController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    )
];
