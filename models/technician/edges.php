<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    $edges = [] ;

    if( $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'medicalSpecialties' ,
                Model::CONTROLLER     => 'medicalSpecialtiesController',
                Model::EDGECONTROLLER => 'technicianMedicalSpecialtiesController',
                Model::SKIN           => 'list'
            ],
            (require __MODELS__ . 'livestock/livestock.php')( 'livestocks' , '' , 'livestockTechniciansController' , 'reverse' )
        ];
    }

    return $edges ;
};
