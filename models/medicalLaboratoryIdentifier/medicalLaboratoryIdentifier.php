<?php

use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoryIdentifierController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $name
 * @param string|null $edge
 *
 * @return array
 */
return fn( string $name = 'identifier' , string $edge = null ) : array =>
[
    Model::NAME       => $name,
    Model::CONTROLLER => MedicalLaboratoryIdentifierController::class,
    Model::SKIN       => 'list',
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    Model::JOINS =>
    [
        [
            Model::NAME       => 'additionalType',
            Model::CONTROLLER => 'medicalLaboratoriesIdentifierTypesController',
            Model::SKIN       => 'list'
        ]
    ]
];
