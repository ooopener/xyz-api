<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\brands\BrandsController ;

use xyz\ooopener\models\Edges ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Brands ;
use xyz\ooopener\models\Websites ;

return
[
    Brands::class => fn( ContainerInterface $container ) => new Brands
    (
        $container ,
        'brands' ,
        [
            Model::FACETABLE =>
            [
                'id'  => [ Model::FACET_TYPE => Model::FACET_FIELD ],
                'ids' => [ Model::FACET_TYPE => Model::FACET_LIST_FIELD ],
                'additionalType' =>
                [
                    Model::FACET_TYPE => Model::FACET_THESAURUS,
                    Model::EDGE       => 'brand_hasType'
                ] ,
                'withStatus' =>
                [
                    Model::FACET_TYPE     => Model::FACET_FIELD ,
                    Model::FACET_PROPERTY => 'withStatus'
                ]
            ],
            Model::SEARCHABLE => [ 'name' ],
            Model::SORTABLE =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'created'  => 'created',
                'modified' => 'modified'
            ],
            Model::EDGES => ( require __MODELS__ . 'brand/edges.php' )( 'full' ),
            Model::JOINS => ( require __MODELS__ . 'brand/joins.php' )( 'full' )
        ]
    ),

    'brandHasType' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'brand_hasType' ,
        [
            Model::FROM =>
            [
                Model::NAME       => 'brands_types',
                Model::CONTROLLER => 'brandsTypesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'brands',
                Model::CONTROLLER => BrandsController::class
            ]
        ]
    ),

    'brandHasWebsites' => fn( ContainerInterface $container ) => new Edges
    (
        $container ,
        'brand_hasWebsites',
        [
            Model::FROM =>
            [
                Model::NAME       => 'brands_websites',
                Model::CONTROLLER => 'brandHasWebsitesController'
            ],
            Model::TO =>
            [
                Model::NAME       => 'brands',
                Model::CONTROLLER => BrandsController::class
            ]
        ]
    ),

    'brandWebsites' => fn( ContainerInterface $container ) => new Websites
    (
        $container ,
        'brands_websites' ,
        [
            Model::JOINS =>
            [
                [
                    Model::NAME       => 'additionalType',
                    Model::CONTROLLER => 'websitesTypesController',
                    Model::SKIN       => 'list'
                ]
            ]
        ]
    )
];
