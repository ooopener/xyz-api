<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\applications\ApplicationsController ;

use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Sessions ;

return
[
    Sessions::class => fn( ContainerInterface $container ) => new Sessions
    (
        $container ,
        "sessions" ,
        [
            Model::FACETABLE =>
            [
                'type' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'type' ],
                'user' => [ Model::FACET_TYPE => Model::FACET_FIELD , Model::FACET_PROPERTY => 'user' ]
            ],
            Model::SEARCHABLE => [ 'user' , 'type' ],
            Model::SORTABLE   =>
            [
                'id'      => '_key',
                'created' => 'created',
                'type'    => 'type'
            ],
            Model::JOINS => (require __MODELS__ . 'session/joins.php')( 'full' )
        ]
    )
];

