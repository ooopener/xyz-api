<?php

use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMarksController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMaterialsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMeasurementsController;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectNumbersController;
use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function ( string $skin = '' ) : array
{
    $edges =
    [
        require __MODELS__ . 'mediaObjects/audio.php' ,
        require __MODELS__ . 'mediaObjects/image.php' ,
        require __MODELS__ . 'mediaObjects/video.php' ,
        [
            Model::NAME           => 'category',
            Model::CONTROLLER     => 'conceptualObjectsCategoriesController',
            Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsCategoriesController',
            Model::SKIN           => 'list'
        ],
        (require __MODELS__ . 'place/place.php')( 'location' , 'list' , 'conceptualObjectPlacesController' )
    ];

    // list skin
    if( $skin == 'list' || $skin == 'extend' ||$skin = 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'temporal',
                Model::CONTROLLER     => 'artTemporalEntitiesController',
                Model::EDGECONTROLLER => 'conceptualObjectArtTemporalEntitiesController',
                Model::SKIN           => 'list'
            ]
        ];
    }

    // extend skin
    if( $skin == 'extend' || $skin == 'full' )
    {
        $edges =
        [
            ...$edges,
            [
                Model::NAME           => 'keywords',
                Model::CONTROLLER     => 'conceptualObjectKeywordsController',
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsKeywordsController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'marks',
                Model::CONTROLLER     => ConceptualObjectMarksController::class,
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsMarksController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType' ,
                        Model::CONTROLLER => 'marksTypesController',
                        Model::SKIN       => 'list'
                    ],
                    [
                        Model::NAME       => 'language' ,
                        Model::CONTROLLER => 'languagesController',
                        Model::SKIN       => 'list'
                    ],
                    [
                        Model::NAME       => 'technique',
                        Model::CONTROLLER => 'conceptualObjectsTechniquesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'materials',
                Model::CONTROLLER     => ConceptualObjectMaterialsController::class,
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsMaterialsController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'material' ,
                        Model::CONTROLLER => 'materialsController',
                        Model::SKIN       => 'list'
                    ],
                    [
                        Model::NAME       => 'technique',
                        Model::CONTROLLER => 'conceptualObjectsTechniquesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'measurements',
                Model::CONTROLLER     => ConceptualObjectMeasurementsController::class,
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsMeasurementsController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'dimension' ,
                        Model::CONTROLLER => 'measurementsDimensionsController',
                        Model::SKIN       => 'list'
                    ],
                    [
                        Model::NAME       => 'unit',
                        Model::CONTROLLER => 'measurementsUnitsController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'movement',
                Model::CONTROLLER     => 'artMovementsController',
                Model::EDGECONTROLLER => 'conceptualObjectArtMovementsController',
                Model::SKIN           => 'list'
            ],
            [
                Model::NAME           => 'numbers',
                Model::CONTROLLER     => ConceptualObjectNumbersController::class,
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsNumbersController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType' ,
                        Model::CONTROLLER => 'conceptualObjectsNumbersTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ],
            [
                Model::NAME           => 'websites',
                Model::CONTROLLER     => 'conceptualObjectWebsitesController',
                Model::EDGECONTROLLER => 'conceptualObjectConceptualObjectsWebsitesController',
                Model::SKIN           => 'list',
                Model::JOINS          =>
                [
                    [
                        Model::NAME       => 'additionalType',
                        Model::CONTROLLER => 'websitesTypesController',
                        Model::SKIN       => 'list'
                    ]
                ]
            ]
        ];
    }

    return $edges ;
};
