<?php

/**
 * The micro-adventure models definitions.
 */
return ( require 'courses.php' )
     + ( require 'games.php'   )
     + ( require 'stages.php'  )
     + ( require 'steps.php'   )
;
