<?php

use xyz\ooopener\controllers\livestocks\SectorsController ;

use xyz\ooopener\models\Model ;

/**
 * @param string $property
 * @param string $skin
 * @param string|null $edge
 * @param string|null $direction
 * @param bool $lock
 *
 * @return array
 */
return fn
(
    string $property  = 'sector' ,
    string $skin      = '' ,
    string $edge      = null ,
    string $direction = null ,
    bool   $lock      = false
):array
=>
[
    Model::NAME       => $property,
    Model::CONTROLLER => SectorsController::class,
    Model::SKIN       => $skin,
    Model::EDGES      => $lock ? NULL : ( require 'edges.php' )( $skin ) ,
    Model::JOINS      => $lock ? NULL : ( require 'joins.php' )( $skin ) ,
    !( $edge ) ?: Model::EDGECONTROLLER => $edge,
    !( $direction ) ?: Model::DIRECTION => $direction
];

