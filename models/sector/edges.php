<?php

use xyz\ooopener\models\Model ;

/**
 * @param string $skin
 *
 * @return array
 */
return function( string $skin = '' ) : array
{
    return
    [
        [
            Model::NAME           => 'additionalType',
            Model::CONTROLLER     => 'sectorsTypesController',
            Model::EDGECONTROLLER => 'sectorSectorsTypesController',
            Model::SKIN           => 'list'
        ]
    ] ;
};
