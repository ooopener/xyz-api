<?php

use Slim\App ;

use Slim\HttpCache\Cache ;

use Slim\Middleware\ErrorMiddleware ;

use Slim\Views\Twig ;
use Slim\Views\TwigMiddleware ;

use Middlewares\TrailingSlash;

use xyz\ooopener\middlewares\Accept ;
use xyz\ooopener\middlewares\ActivityLog ;
use xyz\ooopener\middlewares\Authorized ;
use xyz\ooopener\middlewares\CheckToken ;
use xyz\ooopener\middlewares\Cors ;
use xyz\ooopener\middlewares\HttpCache ;
use xyz\ooopener\middlewares\CheckJwt ;
use xyz\ooopener\middlewares\LogStatus ;
use xyz\ooopener\middlewares\UnderMaintenance ;
use xyz\ooopener\middlewares\Retired ;

return function( App $app ) // be careful - FILO
{
    $app->add( LogStatus::class ) ;
    $app->add( ActivityLog::class ) ;
    $app->add( HttpCache::class ) ;

    $app->addBodyParsingMiddleware() ; // Parse json, form data and xml

    $app->add( Cache::class      ) ;
    $app->add( Authorized::class ) ;
    $app->add( CheckToken::class ) ;
    $app->add( CheckJwt::class   ) ;
    $app->add( Accept::class     ) ;
    $app->add( Cors::class       ) ;

    $app->add( UnderMaintenance::class ) ;
    $app->add( Retired::class          ) ;

    $app->add( TwigMiddleware::createFromContainer( $app , Twig::class ) ) ;

    $app->addRoutingMiddleware() ;

    $app->add( ( new TrailingSlash() )->redirect() );

    $app->add( ErrorMiddleware::class ) ;
};
