<?php

use Psr\Container\ContainerInterface ;

use Psr\Log\LoggerInterface;
use xyz\ooopener\helpers\Arango ;

return
[
    Arango::class => fn( ContainerInterface $container )
                  => new Arango
                  (
                      $container->get('settings')['arangoDB'] ,
                      $container->get( LoggerInterface::class )
                  )
];
