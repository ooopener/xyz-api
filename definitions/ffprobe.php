<?php

use Psr\Container\ContainerInterface;

use FFMpeg\FFProbe ;

return
[
    FFProbe::class => fn( ContainerInterface $container ) => FFProbe::create
    (
        $container->get('settings')['ffmpeg']
    )
];