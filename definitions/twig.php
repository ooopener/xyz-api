<?php

use Psr\Container\ContainerInterface ;

use Slim\Views\Twig ;

use xyz\ooopener\twig\extension\CsrfExtension ;

return
[
    Twig::class => function( ContainerInterface $container )
    {
        $set = $container->get('settings')[ 'twig' ] ;

        $init = [ 'debug' => (bool) $set[ 'debug' ] ] ;

        if( (bool) $set[ 'cache_active' ] )
        {
            $init[ 'cache' ] = __APP__ . $set[ 'cache_path' ] ;
        }

        $view = Twig::create(__APP__ . $set[ 'views_path' ] , $init) ;

        $csrf = $container->get( 'csrf' ) ;
        $view->addExtension( new CsrfExtension( $csrf ) ) ;
        return $view ;
    }
];
