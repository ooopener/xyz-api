<?php

use Psr\Container\ContainerInterface ;

use Slim\App ;
use Slim\Factory\AppFactory ;
use Slim\Interfaces\RouteCollectorInterface;

return
[
    App::class => function( ContainerInterface $container )
    {
        AppFactory::setContainer( $container ) ;
        return AppFactory::create() ;
    }
];
