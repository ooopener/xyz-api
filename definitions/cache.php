<?php

use Slim\HttpCache\Cache ;
use Slim\HttpCache\CacheProvider ;

return
[
    Cache::class         => fn() => new Cache( 'no-store' , 0 ) ,
    CacheProvider::class => fn() => new CacheProvider()
];
