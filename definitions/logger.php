<?php

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface ;

use system\logging\Logger;

return
[
    LoggerInterface::class => function( ContainerInterface $container )
    {
        $setting = $container->get('settings')['logger'] ;
        $logger  = new Logger( __APP__ . $setting[ 'path' ] , $setting[ 'level' ] );

        set_error_handler([ $logger , 'onError' ]) ;
        set_exception_handler([ $logger , 'onException' ]) ;

        return $logger ;
    }
];