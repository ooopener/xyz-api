<?php

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface ;

use Slim\App ;

use Slim\Middleware\ErrorMiddleware ;

use Zeuxisoo\Whoops\Slim\WhoopsMiddleware ;
return
[
    ErrorMiddleware::class => function( ContainerInterface $container )
    {
        $app    = $container->get( App::class ) ;
        $config  = $container->get( 'settings' ) ;
        $logger  = $container->get( LoggerInterface::class ) ;

        if( $config['mode'] == 'prod' ) // || $config['mode'] == 'preprod'
        {
            return $app->addErrorMiddleware( false , false , false , $logger ) ;
        }
        else
        {
            return new WhoopsMiddleware() ;
        }
    }
];
