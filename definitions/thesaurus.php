<?php

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface;

use xyz\ooopener\data\ThesaurusCollector;

return
[
    ThesaurusCollector::class => fn( ContainerInterface $container )
                              => new ThesaurusCollector
                                 (
                                     __CACHE__ . '/thesaurus.php' ,
                                     $container->get( LoggerInterface::class ) ,
                                     __CACHE__ . '/routes.php'
                                 )
];