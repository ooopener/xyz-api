<?php

use xyz\ooopener\helpers\CookieHelper ;

return
[
    CookieHelper::class => fn() => new CookieHelper()
];
