<?php

use Psr\Container\ContainerInterface ;

use Slim\App ;

use Slim\Interfaces\RouteCollectorInterface ;
use Slim\Interfaces\RouteParserInterface ;

return
[
    RouteCollectorInterface::class => fn (ContainerInterface $container)
                                   => $container->get( App::class )->getRouteCollector() ,

    RouteParserInterface::class => fn( ContainerInterface $container )
                                => $container->get( RouteCollectorInterface::class )
                                             ->getRouteParser()
];