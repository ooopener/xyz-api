<?php

use libphonenumber\PhoneNumberUtil ;

return
[
    PhoneNumberUtil::class => fn() => PhoneNumberUtil::getInstance()
];
