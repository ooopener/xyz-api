<?php

use Psr\Container\ContainerInterface;

use FFMpeg\FFMpeg ;

return
[
    FFMpeg::class => fn( ContainerInterface $container ) => FFMpeg::create
    (
        $container->get('settings')['ffmpeg']
    )
];