<?php

use Psr\Container\ContainerInterface ;

use system\net\GoogleAnalytics;

return
[
    'tracker' => function( ContainerInterface $container )
    {
        $set = $container->get('settings');
        if( $set[ 'useAnalytics' ] )
        {
            return new GoogleAnalytics (
                $set[ "analytics" ][ "trackingID" ] ,
                $set[ "analytics" ][ "useSSL" ] ,
                $set[ "analytics" ][ "useCacheBuster" ]
            );
        }
        return NULL ;
    }
];