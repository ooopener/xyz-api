<?php

use system\date\ISO8601 ;

return
[
    ISO8601::class => fn() => new ISO8601()
];
