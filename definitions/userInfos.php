<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\helpers\UserInfos ;

return
[
    UserInfos::class => fn( ContainerInterface $container ) => new UserInfos( $container )
];
