<?php

use Kreait\Firebase\Factory ;

use Psr\Container\ContainerInterface ;

return
[
    'firebase' => fn( ContainerInterface $container )
               => ( new Factory() )
               ->withServiceAccount( $container->get('serviceAccount') )
               ->createAuth()
];