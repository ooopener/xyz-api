<?php

use Psr\Container\ContainerInterface ;

use Slim\App ;

use Psr\Http\Message\ResponseFactoryInterface ;

return
[
    ResponseFactoryInterface::class => fn( ContainerInterface $container )
                                    => $container->get( App::class )->getResponseFactory()
];