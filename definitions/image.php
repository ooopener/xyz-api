<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\display\Image ;

return
[
    'image' => function( ContainerInterface $container )
    {
        $image = new Image($container); // TODO initialize the properties of the container in the constructor ?!

        $settings = $container->get('settings')[ 'images' ];

        $image->root      = $settings[ 'root' ];
        $image->maxheight = $settings[ 'maxheight' ];
        $image->maxwidth  = $settings[ 'maxwidth'  ];

        return $image;
    }
];
