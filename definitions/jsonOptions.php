<?php

use Psr\Container\ContainerInterface ;

return
[
    'jsonOptions' => function( ContainerInterface $container )
    {
        $options = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

        $prettyprint = $container->get('settings')[ 'json_prettyprint' ] == TRUE ;

        $params = filter_input_array(INPUT_GET , [ "prettyprint" => FILTER_SANITIZE_STRING ] , FALSE);

        if( $params && array_key_exists('prettyprint' , $params) )
        {
            $prettyprint = strtolower($params[ "prettyprint" ]) == "true";
        }

        if( $prettyprint )
        {
            $options |= JSON_PRETTY_PRINT;
        }

        return $options;
    }
];