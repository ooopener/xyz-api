<?php

use Psr\Container\ContainerInterface ;
use xyz\ooopener\handlers\NotFoundHandler ;

return
[
    'notFoundHandler' => fn( ContainerInterface $container )
                      => new NotFoundHandler( $container ) // option: 'errors/404.twig'
];
