<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\helpers\Mailer ;

return
[
    Mailer::class => fn( ContainerInterface $container ) => new Mailer
    (
        $container ,
        $container->get('settings')[ 'mail' ]
    )
];
