<?php

use Psr\Container\ContainerInterface ;
use Psr\Log\LoggerInterface ;

use xyz\ooopener\controllers\users\UsersController ;

use xyz\ooopener\models\Users ;

return
[
    'auth' => function( ContainerInterface $container )
    {
        $auth = NULL;
        try
        {
            $jwt = $container->get('jwt') ;
            if( $jwt && $jwt->sub )
            {
                $usersController = $container->get( UsersController::class ) ;

                $auth = $container
                      ->get( Users::class )
                      ->get
                      (
                          $jwt->sub ,
                          [
                              'key'         => 'uuid' ,
                              'skin'        => 'full' ,
                              'queryFields' => $usersController->getFields()
                          ]
                      ) ;

                if( $auth )
                {
                    $auth = $usersController->create( $auth ) ;
                }
            }
        }
        catch( Exception $e )
        {
            $container->get( LoggerInterface::class )->warning( 'Failed to generate the current auth reference in the container, error: \n' . $e->getMessage() ) ;
        }
        return $auth ;
    }
];
