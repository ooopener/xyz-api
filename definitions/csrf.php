<?php

use Psr\Container\ContainerInterface ;
use Psr\Http\Message\ResponseFactoryInterface ;
use Slim\Csrf\Guard ;

return
[
    'csrf' => fn( ContainerInterface $container )
           => new Guard( $container->get( ResponseFactoryInterface::class ) )
];