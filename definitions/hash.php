<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\helpers\Hash ;

return
[
    Hash::class => fn( ContainerInterface $container ) => new Hash( $container->get('settings') )
];
