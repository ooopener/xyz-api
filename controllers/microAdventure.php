<?php

/**
 * The micro-adventure controllers.
 */
return ( require 'courses.php' )
     + ( require 'stages.php'  )
     + ( require 'steps.php'   )
;
