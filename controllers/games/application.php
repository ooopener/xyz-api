<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\games\ApplicationGamesController;
use xyz\ooopener\controllers\games\BadgeItemController;
use xyz\ooopener\controllers\games\GameTranslationController;

use xyz\ooopener\controllers\EdgesController;

return
[
    ApplicationGamesController::class => fn( ContainerInterface $container ) => new ApplicationGamesController
    (
        $container ,
        $container->get( 'applicationsGames' ) ,
        'games'
    ),

    'applicationsGamesCoursesGamesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'coursesGames' ),
        $container->get( 'applicationsGames' ),
        $container->get( 'applicationsGamesCoursesGames' )
    ),

    'applicationsGamesGamesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'gamesTypes' ),
        $container->get( 'applicationsGames' ),
        $container->get( 'applicationsGamesGamesTypes' )
    ),

    'applicationGamesTranslationController' => fn( ContainerInterface $container ) => new GameTranslationController
    (
        $container,
        $container->get( 'applicationsGames' ),
        'games',
        'alternateName,description,notes,text'
    ),

    'applicationBadgeItemController' => fn( ContainerInterface $container ) => new BadgeItemController
    (
        $container ,
        $container->get( 'applicationsGames' ) ,
        $container->get( ApplicationGamesController::class )
    )
];
