<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\games\BadgeItemController;
use xyz\ooopener\controllers\games\CourseGamesController;
use xyz\ooopener\controllers\games\QuestionGamesController;
use xyz\ooopener\controllers\games\GameTranslationController;

use xyz\ooopener\controllers\EdgesController;

return
[
    QuestionGamesController::class => fn( ContainerInterface $container ) => new QuestionGamesController
    (
        $container ,
        $container->get( 'questionsGames' ) ,
        $container->get( 'coursesGames' ) ,
        $container->get( 'coursesGamesQuestionsGames' ) ,
        $container->get( CourseGamesController::class ),
        'games/questions'
    ),

    'questionBadgeItemController' => fn( ContainerInterface $container ) => new BadgeItemController
    (
        $container ,
        $container->get( 'questionsGames' ) ,
        $container->get( QuestionGamesController::class )
    ),

    'questionsGamesGamesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'gamesTypes' ),
        $container->get( 'questionsGames' ),
        $container->get( 'questionsGamesGamesTypes' )
    ),

    'questionGamesTranslationController' => fn( ContainerInterface $container ) => new GameTranslationController
    (
        $container,
        $container->get( 'questionsGames' ),
        'games/questions',
        'alternateName,description,notes,text'
    )
];
