<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\games\ApplicationGamesController;
use xyz\ooopener\controllers\games\BadgeItemController;
use xyz\ooopener\controllers\games\CourseGamesController;
use xyz\ooopener\controllers\games\GameTranslationController;

use xyz\ooopener\controllers\EdgesController;

return
[
    CourseGamesController::class => fn( ContainerInterface $container ) => new CourseGamesController
    (
        $container ,
        $container->get( 'coursesGames' ) ,
        $container->get( 'applicationsGames' ) ,
        $container->get( 'applicationsGamesCoursesGames' ) ,
        $container->get( ApplicationGamesController::class ),
        'games/courses'
    ),

    'courseGamesTranslationController' => fn( ContainerInterface $container ) => new GameTranslationController
    (
        $container,
        $container->get( 'coursesGames' ),
        'games/courses',
        'alternateName,description,notes,text'
    ),

    'courseBadgeItemController' => fn( ContainerInterface $container ) => new BadgeItemController
    (
        $container ,
        $container->get( 'coursesGames' ) ,
        $container->get( CourseGamesController::class )
    ),

    'coursesGamesGamesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'gamesTypes' ),
        $container->get( 'coursesGames' ),
        $container->get( 'coursesGamesGamesTypes' )
    ),

    'coursesGamesQuestionsGamesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'questionsGames' ),
        $container->get( 'coursesGames' ),
        $container->get( 'coursesGamesQuestionsGames' )
    )
];
