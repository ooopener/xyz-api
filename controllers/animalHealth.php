<?php

/**
 * The animal health controllers.
 */
return ( require 'diseases.php'            )
     + ( require 'livestocks.php'          )
     + ( require 'medicalLaboratories.php' )
     + ( require 'technicians.php'         )
     + ( require 'veterinarians.php'       )
;
