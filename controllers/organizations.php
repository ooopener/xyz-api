<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesAuthoritiesController ;
use xyz\ooopener\controllers\EdgesFieldController ;
use xyz\ooopener\controllers\EdgesMultipleThingsController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\EdgesSubController ;
use xyz\ooopener\controllers\EmailsController ;
use xyz\ooopener\controllers\GeoCoordinatesController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\PhoneNumbersController ;
use xyz\ooopener\controllers\PostalAddressController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\organizations\OrganizationNumbersController ;
use xyz\ooopener\controllers\organizations\OrganizationsController ;
use xyz\ooopener\controllers\organizations\OrganizationTranslationController ;

use xyz\ooopener\models\Brands ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;

use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\organizations\OrganizationNumbers ;

return
[
    OrganizationsController::class => fn( ContainerInterface $container ) => new OrganizationsController
    (
        $container ,
        $container->get( Organizations::class ) ,
        'organizations'
    ),

    OrganizationTranslationController::class => fn( ContainerInterface $container ) => new OrganizationTranslationController
    (
        $container,
        $container->get( Organizations::class ),
        'organizations',
        'about,additional,alternateName,description'
    ),

    'organizationActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Organizations::class ) ,
        'organizations'
    ),

    'organizationApeController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'organizationsNaf' ) ,
        $container->get( Organizations::class ) ,
        $container->get( 'organizationApe' )
    ),

    'organizationAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'organizationAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsOrganizationsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'organizationDepartmentController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Organizations::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationDepartment' ) ,
        FALSE // not strict
    ),

    'organizationEmailsController' => fn( ContainerInterface $container ) => new EmailsController
    (
        $container,
        $container->get( 'organizationEmails' ),
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsEmails' ),
        'email'
    ),

    'organizationEmployeesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationEmployees' )
    ),

    'organizationFounderController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationFounder' )
    ),

    'organizationFoundingLocationController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( Organizations::class ) ,
        $container->get( 'organizationFoundingLocation' )
    ),

    'organizationGeoCoordinatesController' => fn( ContainerInterface $container ) => new GeoCoordinatesController
    (
        $container ,
        $container->get( Organizations::class )
    ),

    'organizationHasBrandController' => fn( ContainerInterface $container ) => new EdgesMultipleThingsController
    (
        $container ,
        $container->get( Organizations::class ) ,
        $container->get( 'organizationHasBrand' ) ,
        [
            'brand' =>
            [
                Model::CONTROLLER => $container->get( BrandsController::class ) ,
                Model::MODEL      => $container->get( Brands::class           ) ,
                Model::NAME       => 'brands'
            ]
        ]
    ),

    'organizationImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'organizationKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container,
        $container->get( 'organizationKeywords' ),
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsKeywords' ),
        'keywords'
    ),

    'organizationLegalFormController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'businessEntityTypes' ) ,
        $container->get( Organizations::class ) ,
        $container->get( 'organizationLegalForm' )
    ),

    'organizationLogoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsThingsLogo' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    OrganizationNumbersController::class => fn( ContainerInterface $container ) => new OrganizationNumbersController
    (
        $container ,
        $container->get( OrganizationNumbers::class ) ,
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsNumbers' ),
        'numbers'
    ),

    'organizationOrganizationsEmailsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationOrganizationsEmails' )
    ),

    'organizationOrganizationsKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationOrganizationsKeywords' )
    ),

    'organizationOrganizationsNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationOrganizationsNumbers' )
    ),

    'organizationOrganizationsPhoneNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationOrganizationsPhoneNumbers' )
    ),

    'organizationOrganizationsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'organizationsTypes' ),
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsTypes' )
    ),

    'organizationPhoneNumbersController' => fn( ContainerInterface $container ) => new PhoneNumbersController
    (
        $container,
        $container->get( 'organizationPhoneNumbers' ),
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsPhoneNumbers' ),
        'telephone'
    ),

    'organizationPlacesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( Organizations::class ) ,
        $container->get( 'organizationPlaces' )
    ),

    'organizationMembersController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationMembersPeople' ),
        $container->get( 'organizationMembersOrganizations' ),
        'members'
    ),

    'organizationMembersOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationMembersOrganizations' )
    ),

    'organizationMembersPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationMembersPeople' )
    ),

    'organizationPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsOrganizationsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'organizationPostalAddressController' => fn( ContainerInterface $container ) => new PostalAddressController
    (
        $container ,
        $container->get( Organizations::class )
    ),

    'organizationProvidersController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationProvidersPeople' ),
        $container->get( 'organizationProvidersOrganizations' ),
        'providers'
    ),

    'organizationSubOrganizationsController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Organizations::class ),
        $container->get( Organizations::class ),
        $container->get( 'organizationSubOrganizations' )
    ),

    'organizationAdditionalTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Organizations::class ),
        'additionalType',
        TRUE
    ),

    'organizationVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'organizationVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Organizations::class ),
        $container->get( 'mediaObjectsOrganizationsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'organizationWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'organizationWebsites' ),
        $container->get( Organizations::class ),
        $container->get( 'organizationOrganizationsWebsites' ),
        'websites'
    ),

    'organizationWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Organizations::class ) ,
        'organizations'
    ),

    'organizationOrganizationsWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'organizationOrganizationsWebsites' )
    )
];
