<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesAuthoritiesController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesFieldController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\EdgesSubController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\OffersController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\events\EventsController ;
use xyz\ooopener\controllers\events\EventTranslationController ;

use xyz\ooopener\models\ConceptualObjects ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Events ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;

return
[
    EventsController::class => fn( ContainerInterface $container ) => new EventsController
    (
        $container ,
        $container->get( Events::class ) ,
        'events'
    ),

    EventTranslationController::class => fn( ContainerInterface $container ) => new EventTranslationController
    (
        $container,
        $container->get( Events::class ) ,
        'events' ,
        'alternateName,alternativeHeadline,description,headline,notes,text'
    ),

    'eventActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Events::class ) ,
        'events'
    ),

    'eventAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'eventAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsEventsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'eventImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'eventKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container ,
        $container->get( 'eventKeywords' ),
        $container->get( Events::class ),
        $container->get( 'eventEventsKeywords' ),
        'keywords'
    ),


    'eventOffersController' => fn( ContainerInterface $container ) => new OffersController
    (
        $container ,
        $container->get( 'eventOffers' ),
        $container->get( Events::class ),
        $container->get( 'eventEventsOffers' ),
        'offers'
    ),

    'eventOrganizersController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Events::class ),
        $container->get( 'eventOrganizersPeople' ),
        $container->get( 'eventOrganizersOrganizations' ),
        'organizer'
    ),

    'eventPlacesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( Events::class ) ,
        $container->get( 'eventPlaces' )
    ),

    'eventPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsEventsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'eventSubEventsController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Events::class ),
        $container->get( Events::class ),
        $container->get( 'eventSubEvents' )
    ),

    'eventTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Events::class ),
        'additionalType',
        TRUE
    ),

    'eventWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'eventWebsites' ),
        $container->get( Events::class ),
        $container->get( 'eventEventsWebsites' ),
        'websites'
    ),

    'eventVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'eventVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( Events::class ),
        $container->get( 'mediaObjectsEventsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'eventWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container,
        $container->get( Events::class ),
        'events'
    ),

    'eventWorksFeaturedController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( ConceptualObjects::class ),
        $container->get( Events::class ),
        $container->get( 'eventWorksFeatured' )
    ),

    // --------

    'eventEventsKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'eventEventsKeywords' )
    ),

    'eventEventsOffersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'eventEventsOffers' )
    ),

    'eventEventsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'eventsTypes' ),
        $container->get( Events::class ),
        $container->get( 'eventEventsTypes' )
    ),

    'eventEventsWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'eventEventsWebsites' )
    ),

];
