<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\SettingsListController ;

return
[
    SettingsListController::class => fn( ContainerInterface $container ) => new SettingsListController
    (
        $container ,
        $container->get( 'settingsList' ) ,
        'settings'
    )
];
