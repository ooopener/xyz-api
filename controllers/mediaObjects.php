<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;

use xyz\ooopener\controllers\creativeWork\MediaObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;
use xyz\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;

use xyz\ooopener\models\creativeWork\MediaObjects ;

$init = fn( $name ) => fn( ContainerInterface $container ) => new EdgesController
(
    $container ,
    $container->get( MediaObjects::class ),
    null,
    $container->get( $name )
);

return
[
    MediaObjectsController::class => fn( ContainerInterface $container ) => new MediaObjectsController
    (
        $container ,
        $container->get( MediaObjects::class ) ,
        'mediaObjects'
    ),

    AudioObjectsController::class => fn( ContainerInterface $container ) => new AudioObjectsController
    (
        $container ,
        $container->get( MediaObjects::class ) ,
        'audioObjects'
    ),

    ImageObjectsController::class => fn( ContainerInterface $container ) => new ImageObjectsController
    (
        $container ,
        $container->get( MediaObjects::class ) ,
        'imageObjects'
    ),

    VideoObjectsController::class => fn( ContainerInterface $container ) => new VideoObjectsController
    (
        $container ,
        $container->get( MediaObjects::class ) ,
        'videoObjects'
    ),

    'mediaObjectsThingsAudioController'  => $init( 'mediaObjectsThingsAudio'  ),
    'mediaObjectsThingsAudiosController' => $init( 'mediaObjectsThingsAudios' ),
    'mediaObjectsThingsImageController'  => $init( 'mediaObjectsThingsImage'  ),
    'mediaObjectsThingsLogoController'   => $init( 'mediaObjectsThingsLogo'   ),
    'mediaObjectsThingsPhotosController' => $init( 'mediaObjectsThingsPhotos' ),
    'mediaObjectsThingsVideoController'  => $init( 'mediaObjectsThingsVideo'  ),
    'mediaObjectsThingsVideosController' => $init( 'mediaObjectsThingsVideos' )
];
