<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesAuthoritiesController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\applications\ApplicationsController ;
use xyz\ooopener\controllers\applications\ApplicationTranslationController ;

use xyz\ooopener\models\Applications ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Users ;

return
[
    ApplicationsController::class => fn( ContainerInterface $container ) => new ApplicationsController
    (
        $container ,
        $container->get( Applications::class ) ,
        'applications'
    ),

    ApplicationTranslationController::class => fn( ContainerInterface $container ) => new ApplicationTranslationController
    (
            $container,
            $container->get( Applications::class ),
            'applications',
            'alternativeHeadline,headline,description,notes,text'
    ),

    'applicationApplicationsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'applicationsTypes'),
        $container->get( Applications::class ),
        $container->get( 'applicationApplicationsTypes' )
    ),

    'applicationAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'applicationAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsApplicationsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'applicationImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'applicationPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsApplicationsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'applicationProducerController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Applications::class ),
        $container->get( 'applicationProducerPeople' ),
        $container->get( 'applicationProducerOrganizations' ),
        'producer'
    ),

    'applicationProducerOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationProducerOrganizations' )
    ),

    'applicationProducerPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationProducerPeople' )
    ),

    'applicationPublisherController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Applications::class ),
        $container->get( 'applicationPublisherPeople' ),
        $container->get( 'applicationPublisherOrganizations' ),
        'publisher'
    ),

    'applicationPublisherOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationPublisherOrganizations' )
    ),

    'applicationPublisherPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationPublisherPeople' )
    ),

    'applicationSponsorController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( Applications::class ),
        $container->get( 'applicationSponsorPeople' ),
        $container->get( 'applicationSponsorOrganizations' ),
        'sponsor'
    ),

    'applicationSponsorOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationSponsorOrganizations' )
    ),

    'applicationSponsorPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationSponsorPeople' )
    ),

    'applicationUsersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Users::class ),
        $container->get( Applications::class ),
        $container->get( 'applicationUsers' )
    ),

    'applicationVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'applicationVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Applications::class ),
        $container->get( 'mediaObjectsApplicationsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'applicationWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'applicationWebsites' ),
        $container->get( Applications::class ),
        $container->get( 'applicationApplicationsWebsites' ),
        'websites'
    ),

    'applicationApplicationsWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'applicationApplicationsWebsites' )
    )
];
