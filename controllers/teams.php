<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\teams\TeamPermissionsController ;
use xyz\ooopener\controllers\teams\TeamsController ;

use xyz\ooopener\controllers\teams\TeamTranslationController ;

use xyz\ooopener\models\Teams ;

return
[
    TeamsController::class => fn( ContainerInterface $container ) => new TeamsController
    (
        $container ,
        $container->get( Teams::class )
    ),

    TeamPermissionsController::class => fn( ContainerInterface $container ) => new TeamPermissionsController
    (
        $container ,
        $container->get( Teams::class )
    ),

    TeamTranslationController::class => fn( ContainerInterface $container ) => new TeamTranslationController
    (
        $container,
        $container->get( Teams::class ) ,
        'teams',
        'alternateName,description'
    )
];
