<?php

$create = require( 'create.php' ) ;

return
[
    // ------ breeding types

    'breedingsTypesMedicalCausesController'     => $create( 'breedingsTypesMedicalCauses'     , 'breedingsTypes' , 'medicalCauses'     ) ,
    'breedingsTypesMorbidityRatesController'    => $create( 'breedingsTypesMorbidityRates'    , 'breedingsTypes' , 'morbidityRates'    ) ,
    'breedingsTypesMortalityRatesController'    => $create( 'breedingsTypesMortalityRates'    , 'breedingsTypes' , 'mortalityRates'    ) ,
    'breedingsTypesProductionsTypesController'  => $create( 'breedingsTypesProductionsTypes'  , 'breedingsTypes' , 'productionsTypes'  ) ,
    'breedingsTypesRiskFactorsController'       => $create( 'breedingsTypesRiskFactors'       , 'breedingsTypes' , 'riskFactors'       ) ,
    'breedingsTypesObservationsTypesController' => $create( 'breedingsTypesObservationsTypes' , 'breedingsTypes' , 'observationsTypes' ) ,
    'breedingsTypesSectorsTypesController'      => $create( 'breedingsTypesSectorsTypes'      , 'breedingsTypes' , 'sectorsTypes'      ) ,
    'breedingsTypesSpeciesController'           => $create( 'breedingsTypesSpecies'           , 'breedingsTypes' , 'species'           ) ,
    'breedingsTypesVaccinesController'          => $create( 'breedingsTypesVaccines'          , 'breedingsTypes' , 'vaccines'          ) ,
    'breedingsTypesWorkplacesTypesController'   => $create( 'breedingsTypesWorkplacesTypes'   , 'breedingsTypes' , 'workplacesTypes'   ) ,

    // ------ observation types

    'observationsTypesAgeGroupsController'          => $create( 'observationsTypesAgeGroups'          , 'observationsTypes' , 'ageGroups'          ) ,
    'observationsTypesDiseasesController'           => $create( 'observationsTypesDiseases'           , 'observationsTypes' , 'diseases'           ) ,
    'observationsTypesDiseasesMandatoryController'  => $create( 'observationsTypesDiseasesMandatory'  , 'observationsTypes' , 'diseasesMandatory'  ) ,
    'observationsTypesMedicalCausesController'      => $create( 'observationsTypesMedicalCauses'      , 'observationsTypes' , 'medicalCauses'      ) ,
    'observationsTypesMedicalConclusionsController' => $create( 'observationsTypesMedicalConclusions' , 'observationsTypes' , 'medicalConclusions' ) ,
    'observationsTypesMedicalFrequenciesController' => $create( 'observationsTypesMedicalFrequencies' , 'observationsTypes' , 'medicalFrequencies' ) ,
    'observationsTypesRiskFactorsController'        => $create( 'observationsTypesRiskFactors'        , 'observationsTypes' , 'riskFactors'        ) ,

    // ------ sectors types

    'sectorsTypesAgeGroupsController'   => $create( 'sectorsTypesAgeGroups'   , 'sectorsTypes' , 'ageGroups'   ) ,
    'sectorsTypesAmbiencesController'   => $create( 'sectorsTypesAmbiences'   , 'sectorsTypes' , 'ambiences'   ) ,
    'sectorsTypesPreventionsController' => $create( 'sectorsTypesPreventions' , 'sectorsTypes' , 'preventions' ) ,
    'sectorsTypesSymptomsController'    => $create( 'sectorsTypesSymptoms'    , 'sectorsTypes' , 'symptoms'    )
];
