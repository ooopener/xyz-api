<?php

use DI\Container;

use xyz\ooopener\controllers\SettingsController ;

return fn( string $ref , string $to , string $from )
       => fn( Container $container )
       => new SettingsController( $container , $container->get($ref) , $to , $from ) ;