<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController;
use xyz\ooopener\controllers\ThingStatusController;

use xyz\ooopener\controllers\technicians\TechniciansController ;

use xyz\ooopener\models\People ;
use xyz\ooopener\models\Technicians ;

return
[
    TechniciansController::class => fn( ContainerInterface $container ) => new TechniciansController
    (
        $container ,
        $container->get( Technicians::class ) ,
        'technicians'
    ),

    'technicianActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Technicians::class ) ,
        'technicians'
    ),

    'technicianWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Technicians::class ) ,
        'technicians'
    ),

    'technicianMedicalSpecialtiesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'medicalSpecialties' ) ,
        $container->get( Technicians::class ) ,
        $container->get( 'technicianMedicalSpecialties' )
    )
];
