<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\login\InviteController ;
use xyz\ooopener\controllers\login\LoginController ;
use xyz\ooopener\controllers\login\LogoutController ;

return
[
    InviteController::class => fn( ContainerInterface $container ) => new InviteController( $container ) ,
    LoginController::class  => fn( ContainerInterface $container ) => new LoginController( $container ) ,
    LogoutController::class => fn( ContainerInterface $container ) => new LogoutController( $container )
];
