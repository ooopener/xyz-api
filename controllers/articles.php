<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController;
use xyz\ooopener\controllers\EdgesController;
use xyz\ooopener\controllers\EdgesFieldController;
use xyz\ooopener\controllers\EdgesSingleController;
use xyz\ooopener\controllers\EdgesSubController;
use xyz\ooopener\controllers\MediaObjectsOrderController;
use xyz\ooopener\controllers\WebsitesController;

use xyz\ooopener\controllers\articles\ArticlesController;
use xyz\ooopener\controllers\articles\ArticleTranslationController;

use xyz\ooopener\models\Articles ;
use xyz\ooopener\models\creativeWork\MediaObjects ;

return
[
    ArticlesController::class => fn( ContainerInterface $container ) => new ArticlesController
    (
        $container ,
        $container->get( Articles::class ) ,
        'articles'
    ),

    ArticleTranslationController::class => fn( ContainerInterface $container ) => new ArticleTranslationController
    (
        $container,
        $container->get( Articles::class ),
        'articles',
        'alternateHeadline,description,headline,text,notes'
    ),

    'articleActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Articles::class ) ,
        'articles'
    ),

    'articleArticlesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'articlesTypes' ),
        $container->get( Articles::class ),
        $container->get( 'articleArticlesTypes' )
    ),

    'articleAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'articleAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsArticlesAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'articleHasPartController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Articles::class ),
        $container->get( Articles::class ),
        $container->get( 'articleHasPart' )
    ),

    'articleImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'articleIsRelatedToController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Articles::class ),
        $container->get( Articles::class ),
        $container->get( 'articleIsRelatedTo' ) ,
        FALSE // not strict
    ),

    'articleIsSimilarToController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Articles::class ),
        $container->get( Articles::class ),
        $container->get( 'articleIsSimilarTo' ) ,
        FALSE // not strict
    ),

    'articlePhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsArticlesPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'articleTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Articles::class ),
        'additionalType',
        TRUE
    ),

    'articleVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'articleVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Articles::class ),
        $container->get( 'mediaObjectsArticlesVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'articleWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'articleWebsites' ),
        $container->get( Articles::class ),
        $container->get( 'articleArticlesWebsites' ),
        'websites'
    ),

    'articleArticlesWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'articleArticlesWebsites' )
    )
];
