<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\ThesaurusCollectionController ;

return
[
    ThesaurusCollectionController::class => fn(ContainerInterface $container ) => new ThesaurusCollectionController
    (
        $container ,
        $container->get( 'thesaurusCollection' ) ,
        'thesaurus'
    ),

    'thesaurusCollectionActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( 'thesaurusCollection' ) ,
        'thesaurus'
    )
];

