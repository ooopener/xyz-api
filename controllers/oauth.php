<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\oauth\AuthorizationCodeController ;
use xyz\ooopener\controllers\oauth\RevokeController ;
use xyz\ooopener\controllers\oauth\TokenController ;

return
[
    AuthorizationCodeController::class => fn( ContainerInterface $container ) => new AuthorizationCodeController( $container ) ,
    RevokeController::class            => fn( ContainerInterface $container ) => new RevokeController( $container ) ,
    TokenController::class             => fn( ContainerInterface $container ) => new TokenController( $container )
];

