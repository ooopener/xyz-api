<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesAuthoritiesController ;
use xyz\ooopener\controllers\EdgesMultipleThingsController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMarksController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMaterialsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectMeasurementsController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectNumbersController ;
use xyz\ooopener\controllers\conceptualObjects\ConceptualObjectTranslationController ;

use xyz\ooopener\models\Brands;
use xyz\ooopener\models\ConceptualObjects ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;

use xyz\ooopener\models\conceptualObjects\ConceptualObjectMarks;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectMaterials;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectMeasurements;
use xyz\ooopener\models\conceptualObjects\ConceptualObjectNumbers;

return
[
    ConceptualObjectsController::class => fn( ContainerInterface $container ) => new ConceptualObjectsController
    (
        $container ,
        $container->get( ConceptualObjects::class ) ,
        'conceptualObjects'
    ),

    ConceptualObjectTranslationController::class => fn( ContainerInterface $container ) => new ConceptualObjectTranslationController
    (
        $container,
        $container->get( ConceptualObjects::class ),
        'conceptualObjects',
        'about,alternateName,description,remarks'
    ),

    ConceptualObjectMarksController::class => fn( ContainerInterface $container ) => new ConceptualObjectMarksController
    (
        $container ,
        $container->get( ConceptualObjectMarks::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsMarks' ),
        'marks'
    ),

    ConceptualObjectMaterialsController::class => fn( ContainerInterface $container ) => new ConceptualObjectMaterialsController
    (
        $container ,
        $container->get( ConceptualObjectMaterials::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsMaterials' ),
        'materials'
    ),

    ConceptualObjectMeasurementsController::class  => fn( ContainerInterface $container ) => new ConceptualObjectMeasurementsController
    (
        $container ,
        $container->get( ConceptualObjectMeasurements::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsMeasurements' ),
        'measurements'
    ),

    ConceptualObjectNumbersController::class => fn( ContainerInterface $container ) => new ConceptualObjectNumbersController
    (
        $container ,
        $container->get( ConceptualObjectNumbers::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsNumbers' ),
        'numbers'
    ),

    'conceptualObjectActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( ConceptualObjects::class ) ,
        'conceptualObjects'
    ),

    'conceptualObjectWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( ConceptualObjects::class ) ,
        'conceptualObjects'
    ),

    'conceptualObjectAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'conceptualObjectAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsConceptualObjectsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'conceptualObjectArtMovementsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'artMovements' ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectArtMovements' )
    ),

    'conceptualObjectArtTemporalEntitiesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'artTemporalEntities' ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectArtTemporalEntities' )
    ),

    'conceptualObjectHasBrandController' => fn( ContainerInterface $container ) => new EdgesMultipleThingsController
    (
        $container ,
        $container->get( ConceptualObjects::class ) ,
        $container->get( 'conceptualObjectHasBrand' ) ,
        [
            'brand' =>
            [
                Model::CONTROLLER => $container->get( BrandsController::class ) ,
                Model::MODEL      => $container->get( Brands::class           ) ,
                Model::NAME       => 'brands'
            ]
        ]
    ),

    'conceptualObjectConceptualObjectsCategoriesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'conceptualObjectsCategories' ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsCategories' )
    ),

    'conceptualObjectConceptualObjectsMarksController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsMarks' )
    ),

    'conceptualObjectConceptualObjectsMaterialsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsMaterials' )
    ),

    'conceptualObjectConceptualObjectsMeasurementsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsMeasurements' )
    ),

    'conceptualObjectConceptualObjectsNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsNumbers' )
    ),

    'conceptualObjectImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'conceptualObjectKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container ,
        $container->get( 'conceptualObjectKeywords' ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsKeywords' ),
        'keywords'
    ),

    'conceptualObjectConceptualObjectsKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsKeywords' )
    ),

    'conceptualObjectPlacesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( ConceptualObjects::class ) ,
        $container->get( 'conceptualObjectPlaces' )
    ),

    'conceptualObjectProductionsOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Organizations::class ) ,
        $container->get( ConceptualObjects::class ) ,
        $container->get( 'conceptualObjectProductionsOrganizations' )
    ),

    'conceptualObjectProductionsPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( People::class ) ,
        $container->get( ConceptualObjects::class ) ,
        $container->get( 'conceptualObjectProductionsPeople' )
    ),

    'conceptualObjectPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsConceptualObjectsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'conceptualObjectProductionsController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class ),
        $container->get( Organizations::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectProductionsPeople' ),
        $container->get( 'conceptualObjectProductionsOrganizations' ),
        'productions'
    ),

    'conceptualObjectVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'conceptualObjectVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'mediaObjectsConceptualObjectsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'conceptualObjectWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'conceptualObjectWebsites' ),
        $container->get( ConceptualObjects::class ),
        $container->get( 'conceptualObjectConceptualObjectsWebsites' ),
        'websites'
    ),

    'conceptualObjectConceptualObjectsWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'conceptualObjectConceptualObjectsWebsites' )
    )
];
