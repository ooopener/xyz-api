<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesFieldController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\brands\BrandTranslationController ;

use xyz\ooopener\models\Brands ;
use xyz\ooopener\models\creativeWork\MediaObjects ;

return
[
    BrandsController::class => fn( ContainerInterface $container ) => new BrandsController
    (
        $container ,
        $container->get( Brands::class ) ,
        'brands'
    ),

    BrandTranslationController::class => fn( ContainerInterface $container ) => new BrandTranslationController
    (
        $container,
        $container->get( Brands::class ),
        'brands',
        'about,additional,alternateName,description,slogan'
    ),

    'brandActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Brands::class ) ,
        'brands'
    ),

    'brandAdditionalTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Brands::class ),
        'additionalType',
        TRUE
    ),

    'brandAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'brandAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsBrandsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'brandWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Brands::class ) ,
        'brands'
    ),

    'brandImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'brandLogoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsThingsLogo' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'brandHasTypeController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'brandsTypes' ),
        $container->get( Brands::class ),
        $container->get( 'brandHasType' )
    ),

    'brandHasWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'brandHasWebsites' )
    ),

    'brandPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsBrandsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'brandVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'brandVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Brands::class ),
        $container->get( 'mediaObjectsBrandsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'brandWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'brandWebsites' ),
        $container->get( Brands::class ),
        $container->get( 'brandHasWebsites' ),
        'websites'
    ),
];
