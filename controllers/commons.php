<?php

/**
 * The commons application controllers.
 */
return ( require 'api.php'                 )
     + ( require 'applications.php'        )
     + ( require 'articles.php'            )
     + ( require 'brands.php'              )
     + ( require 'conceptualObjects.php'   )
     + ( require 'events.php'              )
     + ( require 'games.php'               )
     + ( require 'login.php'               )
     + ( require 'mediaObjects.php'        )
     + ( require 'oauth.php'               )
     + ( require 'organizations.php'       )
     + ( require 'people.php'              )
     + ( require 'places.php'              )
     + ( require 'settings.php'            )
     + ( require 'teams.php'               )
     + ( require 'thesaurusCollection.php' )
     + ( require 'things.php'              )
     + ( require 'users.php'               )
;
