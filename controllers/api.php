<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\APIController ;

return
[
    APIController::class => fn(ContainerInterface $container ) => new APIController( $container )
];
