<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;

use xyz\ooopener\controllers\users\UserActivityLogsController ;
use xyz\ooopener\controllers\users\UserPermissionsController ;
use xyz\ooopener\controllers\users\UserPostalAddressController ;
use xyz\ooopener\controllers\users\UsersController ;
use xyz\ooopener\controllers\users\UserSessionsController ;

use xyz\ooopener\models\ActivityLogs ;
use xyz\ooopener\models\Sessions ;
use xyz\ooopener\models\Users ;

return
[
    UsersController::class => fn( ContainerInterface $container ) => new UsersController
    (
        $container ,
        $container->get( Users::class ) ,
        'users'
    ) ,

    UserActivityLogsController::class => fn( ContainerInterface $container ) => new UserActivityLogsController
    (
        $container ,
        $container->get( ActivityLogs::class ) ,
        'users/activityLogs'
    ) ,

    'userFavoritesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null ,//$container->things ,
        $container->get( Users::class ) ,
        $container->get( 'userFavorites' )
    ),

    UserPermissionsController::class => fn( ContainerInterface $container ) => new UserPermissionsController
    (
         $container ,
         $container->get( Users::class )
    ),

    UserPostalAddressController::class => fn( ContainerInterface $container ) => new UserPostalAddressController
    (
        $container ,
        $container->get( Users::class )
    ) ,

    UserSessionsController::class => fn( ContainerInterface $container ) => new UserSessionsController
    (
        $container ,
        $container->get( Sessions::class ) ,
        'users/sessions'
    )
];
