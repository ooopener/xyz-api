<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\models\MedicalLaboratories ;

return
[
    'medicalLaboratoryActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( MedicalLaboratories::class ) ,
        'medicalLaboratories'
    )
];
