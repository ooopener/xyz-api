<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoryIdentifierController;
use xyz\ooopener\models\MedicalLaboratories ;
use xyz\ooopener\models\MedicalLaboratoryIdentifier ;

return
[
    'medicalLaboratoriesIdentifierEdgeController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'medicalLaboratoriesIdentifierEdge' )
    ),

    MedicalLaboratoryIdentifierController::class => fn( ContainerInterface $container ) => new MedicalLaboratoryIdentifierController
    (
        $container ,
        $container->get( MedicalLaboratoryIdentifier::class ) ,
        $container->get( MedicalLaboratories::class ),
        $container->get( 'medicalLaboratoriesIdentifierEdge' ),
        'identifier'
    )
];
