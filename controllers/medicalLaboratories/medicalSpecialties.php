<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\models\MedicalLaboratories ;

return
[
    'medicalLaboratoriesMedicalSpecialtiesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'medicalSpecialties' ) ,
        $container->get( MedicalLaboratories::class ) ,
        $container->get( 'medicalLaboratoriesMedicalSpecialties' )
    ),
];
