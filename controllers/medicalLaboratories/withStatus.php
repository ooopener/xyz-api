<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\models\MedicalLaboratories ;

return
[
    'medicalLaboratoryWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( MedicalLaboratories::class ) ,
        'medicalLaboratories'
    )
];
