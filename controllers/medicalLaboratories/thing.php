<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\medicalLaboratories\MedicalLaboratoriesController ;
use xyz\ooopener\models\MedicalLaboratories ;

return
[
    MedicalLaboratoriesController::class => fn( ContainerInterface $container ) => new MedicalLaboratoriesController
    (
        $container ,
        $container->get( MedicalLaboratories::class ) ,
        'medicalLaboratories'
    )
];
