<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\models\MedicalLaboratories ;
use xyz\ooopener\models\Organizations ;

return
[
    'medicalLaboratoryOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Organizations::class ),
        $container->get( MedicalLaboratories::class ),
        $container->get( 'medicalLaboratoryOrganizations' )
    )
];
