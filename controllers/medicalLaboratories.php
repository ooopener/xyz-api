<?php

return
       ( require( 'medicalLaboratories/thing.php'              ) )
     + ( require( 'medicalLaboratories/active.php'             ) )
     // + ( require( 'medicalLaboratories/identifier.php'         ) )
     + ( require( 'medicalLaboratories/medicalSpecialties.php' ) )
     + ( require( 'medicalLaboratories/organization.php'       ) )
     + ( require( 'medicalLaboratories/withStatus.php'         ) )
     ;
