<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesMultipleThingsController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\EdgesSubController ;
use xyz\ooopener\controllers\EmailsController ;
use xyz\ooopener\controllers\GeoCoordinatesController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\MultiEdgeFieldController ;
use xyz\ooopener\controllers\OffersController ;
use xyz\ooopener\controllers\OpeningHoursController ;
use xyz\ooopener\controllers\PhoneNumbersController ;
use xyz\ooopener\controllers\PostalAddressController ;
use xyz\ooopener\controllers\EdgesFieldController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\places\PlacesController ;
use xyz\ooopener\controllers\places\PlaceTranslationController ;

use xyz\ooopener\models\Brands;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\Places ;

return
[
    PlacesController::class => fn( ContainerInterface $container ) => new PlacesController
    (
        $container ,
        $container->get( Places::class ) ,
        'places'
    ),

    PlaceTranslationController::class => fn( ContainerInterface $container ) => new PlaceTranslationController
    (
        $container,
        $container->get( Places::class ),
        'places',
        'about,additional,alternateName,description'
    ),

    'placeActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Places::class ) ,
        'places'
    ),

    'placeAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'placeAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsPlacesAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'placeWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Places::class ) ,
        'places'
    ),

    'placeActivitiesController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container,
        'activities',
        $container->get( 'activities' ),
        $container->get( Places::class ),
        $container->get( 'placeActivities' ),
        'places/activities'
    ),

    'placeEmailsController' => fn( ContainerInterface $container ) => new EmailsController
    (
        $container,
        $container->get( 'placeEmails' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesEmails' ),
        'email'
    ),

    'placeHasBrandController' => fn( ContainerInterface $container ) => new EdgesMultipleThingsController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( 'placeHasBrand' ) ,
        [
            'brand' =>
            [
                Model::CONTROLLER => $container->get( BrandsController::class ) ,
                Model::MODEL      => $container->get( Brands::class           ) ,
                Model::NAME       => 'brands'
            ]
        ]
    ),

    'placeGeoCoordinatesController' => fn( ContainerInterface $container ) => new GeoCoordinatesController
    (
        $container ,
        $container->get( Places::class )
    ),

    'placeImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container                                   , // container
        $container->get( MediaObjects::class )       , // model
        $container->get( Places::class )             , // owner
        $container->get( 'mediaObjectsThingsImage' ) , // edge
        [ 'doc.encodingFormat =~ "image"' ]          , // conditions
        'mediaObjects'                                 // path
    ),

    'placeKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container,
        $container->get( 'placeKeywords' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesKeywords' ),
        'keywords'
    ),

    'placeLogoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsThingsLogo' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'placePermitsController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container,
        'permits',
        $container->get( 'placesRegulations' ),
        $container->get( Places::class ),
        $container->get( 'placePermits' ),
        'places/permits'
    ),

    'placePhoneNumbersController' => fn( ContainerInterface $container ) => new PhoneNumbersController
    (
        $container,
        $container->get( 'placePhoneNumbers' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesPhoneNumbers' ),
        'telephone'
    ),

    'placePlacesEmailsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesEmails' )
    ),

    'placePlacesKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesKeywords' )
    ),

    'placePlacesOffersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesOffers' )
    ),

    'placePlacesPhoneNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesPhoneNumbers' )
    ),

    'placePlacesOpeningHoursController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesOpeningHours' )
    ),

    'placePlacesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'placesTypes' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesTypes' )
    ),

    'placeProhibitionsController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container,
        'prohibitions',
        $container->get( 'placesRegulations' ),
        $container->get( Places::class ),
        $container->get( 'placeProhibitions' ),
        'places/prohibitions'
    ),

    'placeOffersController' => fn( ContainerInterface $container ) => new OffersController
    (
        $container ,
        $container->get( 'placeOffers' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesOffers' ),
        'offers'
    ),

    'placeOpeningHoursController' => fn( ContainerInterface $container ) => new OpeningHoursController
    (
        $container ,
        $container->get( 'placeOpeningHours' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesOpeningHours' ),
        'openingHoursSpecification'
    ),

    'placePhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsPlacesPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'placePostalAddressController' => fn( ContainerInterface $container ) => new PostalAddressController
    (
        $container ,
        $container->get( Places::class )
    ),

    'placeServicesController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container,
        'services',
        $container->get( 'services' ),
        $container->get( Places::class ),
        $container->get( 'placeServices' ),
        'places/services'
    ),

    'placeContainsPlacesController' => fn( ContainerInterface $container ) => new EdgesSubController
    (
        $container,
        $container->get( Places::class ),
        $container->get( Places::class ),
        $container->get( 'placeContainsPlaces' )
    ),

    'placeAdditionalTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Places::class ),
        'additionalType',
        TRUE
    ),

    'placeVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'placeVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Places::class ),
        $container->get( 'mediaObjectsPlacesVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'placeWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'placeWebsites' ),
        $container->get( Places::class ),
        $container->get( 'placePlacesWebsites' ),
        'websites'
    ),

    'placePlacesWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'placePlacesWebsites' )
    )
];
