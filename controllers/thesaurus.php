<?php

$create = require( 'thesaurus/create.php' ) ;

return function( $init ) use ( $create )
{
    $definitions = [] ;
    if( is_array( $init ) )
    {
        foreach( $init as $key => $value )
        {
            $definitions[ $key ] = $create( $value['MODEL'] , $value['PATH'] ) ;
        }
    }
    return $definitions ;
};

