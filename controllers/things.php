<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\CollectionsController ;

return
[
    'thingsController' => fn( ContainerInterface $container ) => new CollectionsController
    (
        $container ,
        $container->get( 'things' ) ,
        'things'
    )
];
