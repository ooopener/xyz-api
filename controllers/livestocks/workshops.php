<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\livestocks\WorkshopsController ;
use xyz\ooopener\models\Livestocks ;
use xyz\ooopener\models\Workplaces ;
use xyz\ooopener\models\Workshops ;

return
[
    WorkshopsController::class => fn( ContainerInterface $container ) => new WorkshopsController
    (
        $container ,
        $container->get( Workshops::class ) ,
        $container->get( Livestocks::class ),
        $container->get( 'livestocksWorkshops' ),
        'workshops'
    ),

    'workshopsBreedingsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'breedingsTypes' ) ,
        $container->get( Workshops::class ) ,
        $container->get( 'workshopsBreedingsTypes' )
    ),

    'workshopsProductionsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'productionsTypes' ) ,
        $container->get( Workshops::class ) ,
        $container->get( 'workshopsProductionsTypes' )
    ),

    'workshopsWaterOriginsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'waterOrigins' ) ,
        $container->get( Workshops::class ) ,
        $container->get( 'workshopsWaterOrigins' )
    ),

    'workshopsWorkplacesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Workplaces::class ) ,
        $container->get( Workshops::class ) ,
        $container->get( 'workshopsWorkplaces' )
    )
];
