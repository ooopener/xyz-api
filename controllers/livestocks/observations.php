<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesMedicalAuthoritiesController ;
use xyz\ooopener\controllers\EdgesSingleController;
use xyz\ooopener\controllers\livestocks\observations\ObservationsController ;
use xyz\ooopener\controllers\livestocks\observations\ObservationTranslationController ;
use xyz\ooopener\controllers\MediaObjectsOrderController;

use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Livestocks ;
use xyz\ooopener\models\Observations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;
use xyz\ooopener\models\Users ;
use xyz\ooopener\models\Workshops ;

return
[
    ObservationsController::class => fn( ContainerInterface $container ) => new ObservationsController
    (
        $container ,
        $container->get( Observations::class ) ,
        'observations'
    ),

    ObservationTranslationController::class => fn( ContainerInterface $container ) => new ObservationTranslationController
    (
        $container,
        $container->get( Observations::class ),
        'observations',
        'alternateName,description,notes'
    ),

    'observationActorsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Observations::class ),
        $container->get( 'observationActors' )
    ),

    'observationAttendeesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Observations::class ),
        $container->get( 'observationAttendees' )
    ),

    'observationAuthorityController' => fn( ContainerInterface $container ) => new EdgesMedicalAuthoritiesController
    (
        $container ,
        $container->get( Observations::class ) ,
        [
            'veterinarian'      => 'observationAuthorityVeterinarians',
            'technician'        => 'observationAuthorityTechnicians',
            'medicalLaboratory' => 'observationAuthorityMedicalLaboratories'
        ]
    ),

    // ------

    'observationAuthorityMedicalLaboratoriesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'observationAuthorityMedicalLaboratories' )
    ),

    'observationAuthorityTechniciansController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'observationAuthorityTechnicians' )
    ),

    'observationAuthorityVeterinariansController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'observationAuthorityVeterinarians' )
    ),

    // ------

    'observationLivestocksController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Livestocks::class ),
        $container->get( Observations::class ),
        $container->get( 'observationLivestocks' )
    ),

    'observationObservationsStatusController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'observationsStatus' ),
        $container->get( Observations::class ),
        $container->get( 'observationObservationsStatus' )
    ),

    'observationObservationsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( 'observationsTypes' ),
        $container->get( Observations::class ),
        $container->get( 'observationObservationsTypes' )
    ),

    'observationOwnersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Users::class ),
        $container->get( Observations::class ),
        $container->get( 'observationOwners' )
    ),

    'observationPeopleController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( People::class ),
        $container->get( Observations::class ),
        $container->get( 'observationPeople' )
    ),

    'observationPlacesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Places::class ),
        $container->get( Observations::class ),
        $container->get( 'observationPlaces' )
    ),

    'observationWorkshopsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Workshops::class ),
        $container->get( Observations::class ),
        $container->get( 'observationWorkshops' )
    ),

    // ------ Medias

    'observationAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),
    'observationAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsObservationsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),
    'observationImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),
    'observationPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsObservationsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),
    'observationVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),
    'observationVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Observations::class ),
        $container->get( 'mediaObjectsObservationsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    )
];
