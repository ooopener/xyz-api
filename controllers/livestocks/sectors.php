<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\livestocks\SectorsController ;
use xyz\ooopener\models\Sectors ;
use xyz\ooopener\models\Workplaces ;

return
[
    SectorsController::class => fn( ContainerInterface $container ) => new SectorsController
    (
        $container                             , // container
        $container->get( Sectors::class      ) , // model
        $container->get( Workplaces::class   ) , // owner
        $container->get( 'workplacesSectors' ) , // edge
        'sectors'                                // path
    ),

    'sectorSectorsTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'sectorsTypes' ) ,
        $container->get( Sectors::class ) ,
        $container->get( 'sectorsSectorsTypes' )
    )
];
