<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\livestocks\WorkplacesController ;
use xyz\ooopener\models\Sectors ;
use xyz\ooopener\models\Workplaces ;
use xyz\ooopener\models\Workshops ;

return
[
    WorkplacesController::class => fn( ContainerInterface $container ) => new WorkplacesController
    (
        $container ,
        $container->get( Workplaces::class ) ,
        $container->get( Workshops::class ),
        $container->get( 'workshopsWorkplaces' ),
        'workplaces'
    ),

    'workplacesSectorsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Sectors::class ) ,
        $container->get( Workplaces::class ) ,
        $container->get( 'workplacesSectors' )
    )
];
