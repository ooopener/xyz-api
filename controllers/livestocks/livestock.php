<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\livestocks\LivestockNumbersController ;
use xyz\ooopener\models\LivestockNumbers ;
use xyz\ooopener\models\Livestocks ;
use xyz\ooopener\models\MedicalLaboratories ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\Technicians ;
use xyz\ooopener\models\Veterinarians ;

return
[
    'livestockActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Livestocks::class ) ,
        'livestocks'
    ),

    LivestockNumbersController::class => fn( ContainerInterface $container ) => new LivestockNumbersController
    (
        $container ,
        $container->get( LivestockNumbers::class ) ,
        $container->get( Livestocks::class ),
        $container->get( 'livestocksLivestocksNumbers' ),
        'numbers'
    ),

    'livestockMedicalLaboratoriesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( MedicalLaboratories::class ),
        $container->get( Livestocks::class ),
        $container->get( 'livestockMedicalLaboratories' )
    ),

    'livestockOrganizationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Organizations::class ),
        $container->get( Livestocks::class ),
        $container->get( 'livestockOrganizations' )
    ),

    'livestockTechniciansController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Technicians::class ),
        $container->get( Livestocks::class ),
        $container->get( 'livestockTechnicians' )
    ),

    'livestockVeterinariansController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container,
        $container->get( Veterinarians::class ),
        $container->get( Livestocks::class ),
        $container->get( 'livestockVeterinarians' )
    ),

    'livestockWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Livestocks::class ) ,
        'livestocks'
    )
];
