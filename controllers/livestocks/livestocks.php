<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\livestocks\LivestocksController ;
use xyz\ooopener\models\Livestocks ;

return
[
    LivestocksController::class => fn( ContainerInterface $container ) => new LivestocksController
    (
        $container ,
        $container->get( Livestocks::class ) ,
        'livestocks'
    ),

    'livestocksLivestocksNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'livestocksLivestocksNumbers' )
    ),

    'livestocksWorkshopsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null ,
        null ,
        $container->get( 'livestocksWorkshops' )
    )
];
