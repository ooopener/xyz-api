<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesMultipleThingsController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\EmailsController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\PhoneNumbersController ;
use xyz\ooopener\controllers\PostalAddressController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\brands\BrandsController ;
use xyz\ooopener\controllers\people\PeopleController ;
use xyz\ooopener\controllers\people\PeopleTranslationController ;

use xyz\ooopener\models\Brands;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Model ;
use xyz\ooopener\models\People ;

return
[
    PeopleController::class => fn( ContainerInterface $container ) => new PeopleController
    (
        $container ,
        $container->get( People::class ) ,
        'people'
    ),

    PeopleTranslationController::class => fn( ContainerInterface $container ) => new PeopleTranslationController
    (
        $container,
        $container->get( People::class ),
        'people',
        'about,description,remarks'
    ),

    'peopleActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( People::class ) ,
        'people'
    ),

    'peopleAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'peopleAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsPeopleAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'peopleWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( People::class ) ,
        'people'
    ) ,

    'peopleEmailsController' => fn( ContainerInterface $container ) => new EmailsController
    (
        $container,
        $container->get( 'peopleEmails' ),
        $container->get( People::class ),
        $container->get( 'peoplePeopleEmails' ),
        'email'
    ),

    'peopleImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'peopleKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container,
        $container->get( 'peopleKeywords' ),
        $container->get( People::class ),
        $container->get( 'peoplePeopleKeywords' ),
        'keywords'
    ),

    'peoplePeopleEmailsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'peoplePeopleEmails' )
    ),

    'peoplePeopleKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'peoplePeopleKeywords' )
    ),

    'peoplePeoplePhoneNumbersController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'peoplePeoplePhoneNumbers' )
    ),

    'peoplePeopleWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'peoplePeopleWebsites' )
    ),

    'peopleJobsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'jobs' ) ,
        $container->get( People::class ),
        $container->get( 'peopleJobs' )
    ),

    'peoplePhoneNumbersController' => fn( ContainerInterface $container ) => new PhoneNumbersController
    (
        $container,
        $container->get( 'peoplePhoneNumbers' ),
        $container->get( People::class ),
        $container->get( 'peoplePeoplePhoneNumbers' ),
        'telephone'
    ),

    'peoplePhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsPeoplePhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'peoplePostalAddressController' => fn( ContainerInterface $container ) => new PostalAddressController
    (
        $container ,
        $container->get( People::class )
    ) ,

    'peopleVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'peopleVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container ,
        $container->get( MediaObjects::class ),
        $container->get( People::class ),
        $container->get( 'mediaObjectsPeopleVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'peopleWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container,
        $container->get( 'peopleWebsites' ),
        $container->get( People::class ),
        $container->get( 'peoplePeopleWebsites' ),
        'websites'
    ),

    'personHasBrandController' => fn( ContainerInterface $container ) => new EdgesMultipleThingsController
    (
        $container ,
        $container->get( People::class ) ,
        $container->get( 'personHasBrand' ) ,
        [
            'brand' =>
            [
                Model::CONTROLLER => $container->get( BrandsController::class ) ,
                Model::MODEL      => $container->get( Brands::class           ) ,
                Model::NAME       => 'brands'
            ]
        ]
    )
];
