<?php

$create = require( 'create.php' ) ;

return
[

    'badgeItemsTypesController'  => $create( 'badgeItemsTypes'  , 'badgeItems/types'  ),
    'coursesAudiencesController' => $create( 'coursesAudiences' , 'courses/audiences' ),
    'coursesLevelsController'    => $create( 'coursesLevels'    , 'courses/levels'    ),
    'coursesPathsController'     => $create( 'coursesPaths'     , 'courses/paths'     ),
    'coursesStatusController'    => $create( 'coursesStatus'    , 'courses/status'    ),
    'coursesTypesController'     => $create( 'coursesTypes'     , 'courses/types'     ),
    'gamesTypesController'       => $create( 'gamesTypes'       , 'games/types'       ),
    'questionsTypesController'   => $create( 'questionsTypes'   , 'questions/types'   ),
    'transportationsController'  => $create( 'transportations'  , 'transportations'   )
];
