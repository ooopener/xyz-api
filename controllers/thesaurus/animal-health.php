<?php

$create = require( 'create.php' ) ;

return
[
    'ageGroupsController'                          => $create( 'ageGroups'                          , 'ageGroups'                             ),
    'ambiencesController'                          => $create( 'ambiences'                          , 'ambiences'                             ),
    'analysisMethodsController'                    => $create( 'analysisMethods'                    , 'analysis/methods'                      ),
    'analysisResultStatusTypesController'          => $create( 'analysisResultStatusTypes'          , 'analysis/result/status/types'          ),
    'breedingsTypesController'                     => $create( 'breedingsTypes'                     , 'breedings/types'                       ),
    'diseasesLevelsController'                     => $create( 'diseasesLevels'                     , 'medical/levels'                        ),
    'diseasesTypesController'                      => $create( 'diseasesTypes'                      , 'diseases/types'                        ),
    'livestocksNumbersTypesController'             => $create( 'livestocksNumbersTypes'             , 'livestocks/numbers/types'              ),
    'medicalAutopsyStatusController'               => $create( 'medicalAutopsyStatus'               , 'medical/autopsy/status'                ),
    'medicalBodySystemsController'                 => $create( 'medicalBodySystems'                 , 'medical/body/systems'                  ),
    'medicalCausesController'                      => $create( 'medicalCauses'                      , 'medical/causes'                        ),
    'medicalConclusionsController'                 => $create( 'medicalConclusions'                 , 'medical/conclusions'                   ),
    'medicalFrequenciesController'                 => $create( 'medicalFrequencies'                 , 'medical/frequencies'                   ),
    'medicalLaboratoriesIdentifierTypesController' => $create( 'medicalLaboratoriesIdentifierTypes' , 'medical/laboratories/identifier/types' ),
    'medicalPrioritiesController'                  => $create( 'medicalPriorities'                  , 'medical/priorities'                    ),
    'medicalSamplingsController'                   => $create( 'medicalSamplings'                   , 'medical/samplings'                     ),
    'medicalSpecialtiesController'                 => $create( 'medicalSpecialties'                 , 'medical/specialties'                   ),
    'medicalTransmissionsMethodsController'        => $create( 'medicalTransmissionsMethods'        , 'medical/transmissions/methods'         ),
    'morbidityRatesController'                     => $create( 'morbidityRates'                     , 'morbidity/rates'                       ),
    'mortalityRatesController'                     => $create( 'mortalityRates'                     , 'mortality/rates'                       ),
    'observationsCausesController'                 => $create( 'observationsCauses'                 , 'observations/causes'                   ),
    'observationsStatusController'                 => $create( 'observationsStatus'                 , 'observations/status'                   ),
    'observationsTypesController'                  => $create( 'observationsTypes'                  , 'observations/types'                    ),
    'preventionsController'                        => $create( 'preventions'                        , 'medical/preventions'                   ),
    'productionsTypesController'                   => $create( 'productionsTypes'                   , 'productions/types'                     ),
    'riskFactorsController'                        => $create( 'riskFactors'                        , 'riskFactors'                           ),
    'sectorsTypesController'                       => $create( 'sectorsTypes'                       , 'sectors/types'                         ),
    'symptomsController'                           => $create( 'symptoms'                           , 'medical/symptoms'                      ),
    'taxonomyBreedsController'                     => $create( 'taxonomyBreeds'                     , 'taxonomy/breeds'                       ),
    'taxonomySpeciesController'                    => $create( 'taxonomySpecies'                    , 'taxonomy/species'                      ),
    'vaccinesController'                           => $create( 'vaccines'                           , 'vaccines'                              ),
    'waterOriginsController'                       => $create( 'waterOrigins'                       , 'water/origins'                         ),
    'workplacesTypesController'                    => $create( 'workplacesTypes'                    , 'workplaces/types'                      )
];
