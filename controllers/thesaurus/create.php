<?php

use xyz\ooopener\controllers\ThesaurusCollectionController ;
use xyz\ooopener\controllers\ThesaurusController ;

use DI\Container ;

return fn( string $ref , string $path , string $ownerRef = ThesaurusCollectionController::class )
       => fn( Container $container )
       => new ThesaurusController( $container , $container->get($ref) , $path , $container->get($ownerRef) ) ;