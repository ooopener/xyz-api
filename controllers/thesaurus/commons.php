<?php

$create = require( 'create.php' ) ;

return
[
    'activitiesController'                    => $create( 'activities'                    , 'activities'                      ),
    'answersController'                       => $create( 'answers'                       , 'answers/generics'                ),
    'applicationsTypesController'             => $create( 'applicationsTypes'             , 'applications/types'              ),
    'artMovementsController'                  => $create( 'artMovements'                  , 'art/movements'                   ),
    'artTemporalEntitiesController'           => $create( 'artTemporalEntities'           , 'art/temporal/entities'           ),
    'articlesTypesController'                 => $create( 'articlesTypes'                 , 'articles/types'                  ),
    'brandsTypesController'                   => $create( 'brandsTypes'                   , 'brands/types'                    ),
    'businessEntityTypesController'           => $create( 'businessEntityTypes'           , 'business/entity/types'           ),
    'conceptualObjectsCategoriesController'   => $create( 'conceptualObjectsCategories'   , 'conceptualObjects/categories'    ),
    'conceptualObjectsNumbersTypesController' => $create( 'conceptualObjectsNumbersTypes' , 'conceptualObjects/numbers/types' ),
    'conceptualObjectsTechniquesController'   => $create( 'conceptualObjectsTechniques'   , 'conceptualObjects/techniques'    ),
    'emailsTypesController'                   => $create( 'emailsTypes'                   , 'emails/types'                    ),
    'eventsStatusTypesController'             => $create( 'eventsStatusTypes'             , 'events/statusTypes'              ),
    'eventsTypesController'                   => $create( 'eventsTypes'                   , 'events/types'                    ),
    'gendersController'                       => $create( 'genders'                       , 'genders'                         ),
    'honorificPrefixController'               => $create( 'honorificPrefix'               , 'honorificPrefix'                 ),
    'jobsController'                          => $create( 'jobs'                          , 'people/jobs'                     ),
    'languagesController'                     => $create( 'languages'                     , 'languages'                       ),
    'marksTypesController'                    => $create( 'marksTypes'                    , 'marks/types'                     ),
    'materialsController'                     => $create( 'materials'                     , 'materials'                       ),
    'measurementsDimensionsController'        => $create( 'measurementsDimensions'        , 'measurements/dimensions'         ),
    'measurementsUnitsController'             => $create( 'measurementsUnits'             , 'measurements/units'              ),
    'offersCategoriesController'              => $create( 'offersCategories'              , 'offers/categories'               ),
    'organizationsNafController'              => $create( 'organizationsNaf'              , 'organizations/naf'               ),
    'organizationsNumbersTypesController'     => $create( 'organizationsNumbersTypes'     , 'organizations/numbers/types'     ),
    'organizationsTypesController'            => $create( 'organizationsTypes'            , 'organizations/types'             ),
    'phoneNumbersTypesController'             => $create( 'phoneNumbersTypes'             , 'phoneNumbers/types'              ),
    'placesRegulationsController'             => $create( 'placesRegulations'             , 'places/regulations'              ),
    'placesStatusTypesController'             => $create( 'placesStatusTypes'             , 'places/statusTypes'              ),
    'placesTypesController'                   => $create( 'placesTypes'                   , 'places/types'                    ),
    'servicesController'                      => $create( 'services'                      , 'services'                        ),
    'thesaurusTypesController'                => $create( 'thesaurusTypes'                , 'types'                           ),
    'websitesTypesController'                 => $create( 'websitesTypes'                 , 'websites/types'                  )
];
