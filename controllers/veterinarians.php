<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController;
use xyz\ooopener\controllers\EdgesAuthoritiesController ;
use xyz\ooopener\controllers\ThingStatusController ;

use xyz\ooopener\controllers\veterinarians\VeterinariansController ;

use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Veterinarians ;

return
[
    VeterinariansController::class => fn( ContainerInterface $container ) => new VeterinariansController
    (
        $container ,
        $container->get( Veterinarians::class ) ,
        'veterinarians'
    ),

    'veterinarianActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Veterinarians::class ) ,
        'veterinarians'
    ),

    'veterinarianAuthorityController' => fn( ContainerInterface $container ) => new EdgesAuthoritiesController
    (
        $container ,
        $container->get( People::class               ) ,
        $container->get( Organizations::class        ) ,
        $container->get( Veterinarians::class        ) ,
        $container->get( 'veterinarianPeople'        ) ,
        $container->get( 'veterinarianOrganizations' ) ,
        'authority'
    ),

    'veterinarianMedicalSpecialtiesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'medicalSpecialties'             ) ,
        $container->get( Veterinarians::class             ) ,
        $container->get( 'veterinarianMedicalSpecialties' )
    ),

    'veterinarianWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Veterinarians::class ) ,
        'veterinarians'
    )
];
