<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesFieldController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\ElevationController;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\OpeningHoursController ;
use xyz\ooopener\controllers\ThingStatusController ;

use xyz\ooopener\controllers\courses\CoursesController ;
use xyz\ooopener\controllers\courses\CourseDiscoverController;
use xyz\ooopener\controllers\courses\CourseTranslationController ;
use xyz\ooopener\controllers\courses\CourseTransportationsController ;

use xyz\ooopener\models\Articles;
use xyz\ooopener\models\ConceptualObjects;
use xyz\ooopener\models\Courses ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Events;
use xyz\ooopener\models\Organizations;
use xyz\ooopener\models\People;
use xyz\ooopener\models\Places;
use xyz\ooopener\models\Stages;

return
[
    CoursesController::class => fn( ContainerInterface $container ) => new CoursesController
    (
        $container ,
        $container->get( Courses::class ) ,
        'courses'
    ),

    CourseDiscoverController::class => fn( ContainerInterface $container ) => new CourseDiscoverController
    (
        $container ,
        $container->get( Courses::class ) ,
        'discover',
        CoursesController::class,
        [
            'articles'          => Articles::class ,
            'conceptualObjects' => ConceptualObjects::class ,
            'courses'           => Courses::class ,
            'events'            => Events::class ,
            'organizations'     => Organizations::class ,
            'people'            => People::class ,
            'places'            => Places::class ,
            'stages'            => Stages::class
        ]
    ),

    CourseTranslationController::class => fn( ContainerInterface $container ) => new CourseTranslationController
    (
        $container,
        $container->get( Courses::class ),
        'courses',
        'about,alternativeHeadline,cautions,headline,description'
    ),

    CourseTransportationsController::class => fn( ContainerInterface $container ) => new CourseTransportationsController
    (
        $container,
        $container->get( 'courseTransportations' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesTransportations' ),
        'transportations'
    ),

    'courseActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Courses::class ) ,
        'courses'
    ),

    'courseElevationController' => fn( ContainerInterface $container ) => new ElevationController
    (
        $container ,
        $container->get( Courses::class )
    ),

    'courseWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Courses::class ) ,
        'courses'
    ),

    'courseAdditionalTypeController' => fn( ContainerInterface $container ) => new EdgesFieldController
    (
        $container,
        $container->get( Courses::class ),
        'additionalType',
        TRUE
    ),

    'courseAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'courseAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsCoursesAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'courseCoursesAudiencesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesAudiences' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesAudiences' )
    ),

    'courseCoursesLevelsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesLevels' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesLevels' )
    ),

    'courseCoursesPathsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesPaths' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesPaths' )
    ),

    'courseCoursesStatusController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesStatus' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesStatus' )
    ),

    'courseCoursesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesTypes' ),
        $container->get( Courses::class ),
        $container->get( 'courseCoursesTypes' )
    ),

    'courseCoursesOpeningHoursController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'courseCoursesOpeningHours' )
    ),

    'courseCoursesTransportationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'courseCoursesTransportations' )
    ),

    'courseImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'courseOpeningHoursController' => fn( ContainerInterface $container ) => new OpeningHoursController
    (
        $container ,
        $container->get( 'courseOpeningHours' ),
        $container->get( Courses::class ),
        $container->get( 'coursePlacesOpeningHours' ),
        'openingHoursSpecification'
    ),

    'coursePhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsCoursesPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'courseVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'courseVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Courses::class ),
        $container->get( 'mediaObjectsCoursesVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    )
];
