<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController;
use xyz\ooopener\controllers\EdgesController;
use xyz\ooopener\controllers\MultiEdgeFieldController;
use xyz\ooopener\controllers\ThingStatusController;

use xyz\ooopener\controllers\diseases\DiseasesController;
use xyz\ooopener\controllers\diseases\DiseasesTranslationController;
use xyz\ooopener\controllers\diseases\AnalysisMethodController;
use xyz\ooopener\controllers\diseases\AnalysisSamplingController;

use xyz\ooopener\models\Diseases ;

return
[
    DiseasesController::class => fn( ContainerInterface $container ) => new DiseasesController
    (
        $container ,
        $container->get( Diseases::class ) ,
        'diseases'
    ),

    DiseasesTranslationController::class => fn( ContainerInterface $container ) => new DiseasesTranslationController
    (
        $container,
        $container->get( Diseases::class ),
        'diseases',
        'alternateName,description,notes,text'
    ),

    'diseaseActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Diseases::class ) ,
        'diseases'
    ),

    'diseaseWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Diseases::class ) ,
        'diseases'
    ),

    'diseaseAnalysisMethodController' => fn( ContainerInterface $container ) => new AnalysisMethodController
    (
        $container ,
        $container->get( 'analysisMethod' ),
        $container->get( Diseases::class ),
        $container->get( 'diseaseDiseasesAnalysisMethod' ),
        'analysisMethod'
    ),

    'diseaseDiseasesAnalysisMethodController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'diseaseDiseasesAnalysisMethod' )
    ),

    'diseaseAnalysisSamplingController' => fn( ContainerInterface $container ) => new AnalysisSamplingController
    (
        $container ,
        $container->get( 'analysisSampling' ),
        $container->get( Diseases::class ),
        $container->get( 'diseaseDiseasesAnalysisSampling' ),
        'analysisSampling'
    ),

    'diseaseDiseasesAnalysisSamplingController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'diseaseDiseasesAnalysisSampling' )
    ),

    'diseaseDiseasesLevelsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'diseasesLevels' ),
        $container->get( Diseases::class ),
        $container->get( 'diseaseDiseasesLevels' )
    ),

    'diseaseDiseasesTypesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'diseasesTypes' ),
        $container->get( Diseases::class ),
        $container->get( 'diseaseDiseasesTypes' )
    ),

    'diseaseTransmissionsMethodsController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container ,
        'transmissionMethod',
        $container->get( 'medicalTransmissionsMethods' ),
        $container->get( Diseases::class ),
        $container->get( 'diseaseTransmissionsMethods' ),
        'diseases/transmissionMethod'
    )
];
