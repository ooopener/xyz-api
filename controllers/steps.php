<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\courses\CoursesController;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\steps\StepsController;
use xyz\ooopener\controllers\steps\StepTranslationController;

use xyz\ooopener\models\Courses ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Places ;
use xyz\ooopener\models\Stages ;
use xyz\ooopener\models\Steps ;

return
[
    StepsController::class => fn( ContainerInterface $container ) => new StepsController
    (
        $container ,
        $container->get( Steps::class ) ,
        $container->get( Courses::class ) ,
        $container->get( CoursesController::class ) ,
        'steps'
    ),

    StepTranslationController::class => fn( ContainerInterface $container ) => new StepTranslationController
    (
        $container,
        $container->get( Steps::class ),
        'steps',
        'alternativeHeadline,description,headline,notes,text'
    ),

    'stepActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Steps::class ) ,
        'steps'
    ),

    'stepAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'stepAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsStepsAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'stepWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Steps::class ) ,
        'steps'
    ),

    'stepImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'stepLocationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( Steps::class  ) ,
        $container->get( 'stepLocations' )
    ),

    'stepPhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsStepsPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'stepStagesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Stages::class ),
        $container->get( Steps::class ),
        $container->get( 'stepStages' )
    ),

    'stepVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'stepVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Steps::class ),
        $container->get( 'mediaObjectsStepsVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    )
];
