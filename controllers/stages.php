<?php

use Psr\Container\ContainerInterface ;

use xyz\ooopener\controllers\ActiveController ;
use xyz\ooopener\controllers\EdgesController ;
use xyz\ooopener\controllers\EdgesSingleController ;
use xyz\ooopener\controllers\KeywordsController ;
use xyz\ooopener\controllers\MediaObjectsOrderController ;
use xyz\ooopener\controllers\MultiEdgeFieldController ;
use xyz\ooopener\controllers\ThingStatusController ;
use xyz\ooopener\controllers\WebsitesController ;

use xyz\ooopener\controllers\stages\StagesController ;
use xyz\ooopener\controllers\stages\StageDiscoverController;
use xyz\ooopener\controllers\stages\StageTranslationController;

use xyz\ooopener\models\Articles ;
use xyz\ooopener\models\Courses ;
use xyz\ooopener\models\ConceptualObjects ;
use xyz\ooopener\models\creativeWork\MediaObjects ;
use xyz\ooopener\models\Events ;
use xyz\ooopener\models\Organizations ;
use xyz\ooopener\models\People ;
use xyz\ooopener\models\Places ;
use xyz\ooopener\models\Stages ;

return
[
    StagesController::class => fn( ContainerInterface $container ) => new StagesController
    (
        $container ,
        $container->get( Stages::class ) ,
        'stages'
    ),

    StageDiscoverController::class => fn( ContainerInterface $container ) => new StageDiscoverController
    (
        $container ,
        $container->get( Stages::class ) ,
        'discover',
        StagesController::class,
        [
            'articles'          => Articles::class ,
            'conceptualObjects' => ConceptualObjects::class ,
            'courses'           => Courses::class ,
            'events'            => Events::class ,
            'organizations'     => Organizations::class ,
            'people'            => People::class ,
            'places'            => Places::class ,
            'stages'            => Stages::class
        ]
    ),

    StageTranslationController::class => fn( ContainerInterface $container ) => new StageTranslationController
    (
        $container,
        $container->get( Stages::class ),
        'stages',
        'alternativeHeadline,headline,text,description'
    ),

    'stageActiveController' => fn( ContainerInterface $container ) => new ActiveController
    (
        $container ,
        $container->get( Stages::class ) ,
        'stages'
    ),

    'stageWithStatusController' => fn( ContainerInterface $container ) => new ThingStatusController
    (
        $container ,
        $container->get( Stages::class ) ,
        'stages'
    ),

    'stageAudioController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsThingsAudio' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    ),

    'stageAudiosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsStagesAudios' ),
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ),

    'stageImageController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsThingsImage' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    ),

    'stageKeywordsController' => fn( ContainerInterface $container ) => new KeywordsController
    (
        $container,
        $container->get( 'stageKeywords' ),
        $container->get( Stages::class ),
        $container->get( 'stagePlacesKeywords' ),
        'keywords'
    ),

    'stageStagesKeywordsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'stageStagesKeywords' )
    ),

    'stagePhotosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsStagesPhotos' ),
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ),

    'stageActivitiesController' => fn( ContainerInterface $container ) => new MultiEdgeFieldController
    (
        $container ,
        'activities',
        $container->get( 'activities' ),
        $container->get( Stages::class ),
        $container->get( 'stageActivities' )
    ),

    'stageVideoController' => fn( ContainerInterface $container ) => new EdgesSingleController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsThingsVideo' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    ),

    'stageVideosController' => fn( ContainerInterface $container ) => new MediaObjectsOrderController
    (
        $container,
        $container->get( MediaObjects::class ),
        $container->get( Stages::class ),
        $container->get( 'mediaObjectsStagesVideos' ),
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ),

    'stageWebsitesController' => fn( ContainerInterface $container ) => new WebsitesController
    (
        $container ,
        $container->get( 'stageWebsites' ),
        $container->get( Stages::class ),
        $container->get( 'stageStagesWebsites' ),
        'websites'
    ),

    'stageStagesWebsitesController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        null,
        null,
        $container->get( 'stageStagesWebsites' )
    ),

    'stageLocationsController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( Places::class ) ,
        $container->get( Stages::class ) ,
        $container->get( 'stageLocations' )
    ),

    'stageCoursesStatusController' => fn( ContainerInterface $container ) => new EdgesController
    (
        $container ,
        $container->get( 'coursesStatus' ),
        $container->get( Stages::class ),
        $container->get( 'stageCoursesStatus' )
    )
];
